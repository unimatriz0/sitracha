run:
	echo "Iniciando servidor ..."
	php -S localhost:8000&
	echo "Lanzando navegador ..."
	firefox localhost:8000

doc:
	echo "Generando documentación Javascript ..."
	jsdoc /home/iceman/Dropbox/HTML/Diagnostico/ -r -d=/home/iceman/Documentos/SiTraCha/Js
	echo "Generando documentación PHP ..."
	/home/iceman/Aplicaciones/phpDocumentor/bin/phpdoc.php run -d /home/iceman/Dropbox/HTML/Diagnostico -t /home/iceman/Documentos/SiTraCha/php
