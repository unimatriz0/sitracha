<?php

/**
 *
 * Class Disautonomia | disautonomia/disautonomia.class.php
 *
 * @package     Diagnostico
 * @subpackage  Disautonomia
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * disautonomia
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Disautonomia {

    // declaraciòn de variables de clase
    protected $Link;                   // puntero a la base de datos
    protected $Id;                     // clave del registro
    protected $Paciente;               // clave del paciente
    protected $Hipotension;            // 0 No 1 Si
    protected $Bradicardia;            // 0 no 1 Si
    protected $Astenia;                // 0 No 1 Si
    protected $NoTiene;                // 0 No 1 Si
    protected $IdUsuario;              // clave del usuario
    protected $Usuario;                // nombre del usuario
    protected $Fecha;                  // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initDisautonomia();

    }

    // método que inicializa las variables de la clase
    private function initDisautonomia(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Hipotension = 0;
        $this->Bradicardia = 0;
        $this->Astenia = 0;
        $this->NoTiene = 0;
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // mètodos de asignaciòn de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setHipotension($hipotension){
        $this->Hipotension = $hipotension;
    }
    public function setBradicardia($bradicardia){
        $this->Bradicardia = $bradicardia;
    }
    public function setAstenia($astenia){
        $this->Astenia = $astenia;
    }
    public function setNoTiene($notiene){
        $this->NoTiene = $notiene;
    }

    // mètodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getHipotension(){
        return $this->Hipotension;
    }
    public function getBradicardia(){
        return $this->Bradicardia;
    }
    public function getAstenia(){
        return $this->Astenia;
    }
    public function getNoTiene(){
        return $this->NoTiene;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param paciente entero con la clave del registro
     * Mètodo que recibe como paràmetro la clave de un paciente
     * y asigna en las variables de clase los valores del
     * registro
     */
    public function getDatosDisautonomia($paciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_disautonomia.id AS id,
                            diagnostico.v_disautonomia.paciente AS paciente,
                            diagnostico.v_disautonomia.hipotension AS hipotension,
                            diagnostico.v_disautonomia.bradicardia AS bradicardia,
                            diagnostico.v_disautonomia.astenia AS astenia,
                            diagnostico.v_disautonomia.notiene AS notiene,
                            diagnostico.v_disautonomia.usuario AS usuario,
                            diagnostico.v_disautonomia.fecha AS fecha
                     FROM diagnostico.v_disautonomia
                     WHERE diagnostico.v_disautonomia.paciente = '$paciente'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // asignamos los valores del registro
        $this->Id = $id;
        $this->Paciente = $paciente;
        $this->Hipotension = $hipotension;
        $this->Bradicardia = $bradicardia;
        $this->Astenia = $astenia;
        $this->NoTiene = $notiene;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero clave del registro
     * Mètodo que ejecuta la consulta de inserciòn o ediciòn
     * segùn corresponda y retorna la clave del registro
     * afectado
     */
    public function grabaDisautonia() {

    	// si está insertando
    	if ($this->Id == 0) {
    		$this->nuevaDisautonomia();
    	} else {
    		$this->editaDisautonomia();
    	}

    	// retorna la id
    	return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserciòn
     */
    protected function nuevaDisautonomia() {

        // compone la consulta
        $consulta = "INSERT INTO diagnostico.disautonomia
                                (paciente,
       		                     hipotension,
                                 bradicardia,
       		                     astenia,
                                 notiene,
       		                     usuario)
                                VALUES
       		                    (:paciente,
                                 :hipotension,
                                 :bradicardia,
                                 :astenia,
                                 :notiene,
                                 :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",       $this->Paciente);
        $psInsertar->bindParam(":hipotension",    $this->Hipotension);
        $psInsertar->bindParam(":bradicardia",    $this->Bradicardia);
        $psInsertar->bindParam(":astenia",        $this->Astenia);
        $psInsertar->bindParam(":notiene",        $this->NoTiene);
        $psInsertar->bindParam(":usuario",        $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que ejecuta la consulta de edición
     */
    protected function editaDisautonomia() {

        // componemos la consulta
        $consulta = "UPDATE diagnostico.disautonomia SET
                            hipotension = :hipotension,
                            bradicardia = :bradicardia,
                            astenia = :astenia,
                            notiene = :notiene,
                            usuario = :usuario
                     WHERE diagnostico.disautonomia.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":hipotension",    $this->Hipotension);
        $psInsertar->bindParam(":bradicardia",    $this->Bradicardia);
        $psInsertar->bindParam(":astenia",        $this->Astenia);
        $psInsertar->bindParam(":notiene",        $this->NoTiene);
        $psInsertar->bindParam(":usuario",        $this->IdUsuario);
        $psInsertar->bindParam(":id",             $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}
