<?php

/**
 *
 * borrar | enfermedades/borrar.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave del registro de la tabla
 * pivot de enfermedades del paciente y ejecuta la consulta
 * de eliminación
*/

// incluimos e instanciamos las clases
require_once("enfermedades.class.php");
$enfermedad = new Enfermedades();

// eliminamos el registro
$enfermedad->borraEnfPaciente($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>