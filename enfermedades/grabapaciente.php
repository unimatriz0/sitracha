<?php

/**
 *
 * grabapaciente | enfermedades/grabapaciente.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que ejecuta la consulta de grabación en la tabla pivot
 * de enfermedades del paciente a partir de los valores que
 * recibe por post
*/

// incluimos e instanciamos las clases
require_once("enfermedades.class.php");
$enfermedad = new Enfermedades();

// asignamos los valores recibidos
$enfermedad->setProtocolo($_POST["Protocolo"]);
$enfermedad->setIdEnfermedad($_POST["Enfermedad"]);
$enfermedad->setFechaEnfermedad($_POST["Fecha"]);

// ejecutamos la consulta
$id = $enfermedad->grabaEnfPaciente($_POST["Protocolo"]);

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>