<?php

/**
 *
 * enfermedades/graba_enfermedade.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de una enfermedad, 
 * ejecuta la consulta actualizando el diccionario , retorna
 * el resultado de la operación 
 * 
*/

// incluimos e instanciamos las clases
require_once("enfermedades.class.php");
$enfermedad = new Enfermedades();

// si está editando
if (!empty($_POST["Id"])){
    $enfermedad->setIdEnfermedad($_POST["Id"]);
}

// fija el nombre
$enfermedad->setEnfermedad($_POST["Enfermedad"]);

// grabamos y obtenemos el resultado
$error = $enfermedad->grabaEnfermedad();

// retornamos el resultado
echo json_encode(array("Error" => $error));

?>