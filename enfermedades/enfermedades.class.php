<?php

/**
 *
 * Class Enfermedades | enfermedades/enfermedades.class.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (03/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre el diccionario de
 * enfermedades
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Enfermedades{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $IdEnfermedad;          // clave del registro
    protected $Enfermedad;            // nombre de la enfermedad
    protected $FechaAlta;             // fecha de alta
    protected $Link;                  // puntero a la base de datos

    // las variables específicas de la tabla de enfermedades
    // del paciente
    protected $Protocolo;             // clave del paciente
    protected $FechaEnfermedad;       // fecha en que sufrió la enfermedad

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Usuario = "";
        $this->FechaAlta = "";
        $this->IdEnfermedad = 0;
        $this->Enfermedad = "";
        $this->Protocolo = 0;
        $this->FechaEnfermedad = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdEnfermedad($idenfermedad){
        $this->IdEnfermedad = $idenfermedad;
    }
    public function setEnfermedad($enfermedad){
        $this->Enfermedad = $enfermedad;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setFechaEnfermedad($fecha){
        $this->FechaEnfermedad = $fecha;
    }

    // métodos de obtención de valores
    public function getIdEnfermedad(){
        return $this->IdEnfermedad;
    }
    public function getEnfermedad(){
        return $this->Enfermedad;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que retorna el array con las enfermedades del diccionario
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaEnfermedades(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_enfermedades.id_enfermedad AS id_enfermedad,
                            diagnostico.v_enfermedades.enfermedad AS enfermedad,
                            diagnostico.v_enfermedades.id_usuario AS id_usuario,
                            diagnostico.v_enfermedades.usuario AS usuario,
                            diagnostico.v_enfermedades.fecha_alta AS fecha_alta
                     FROM diagnostico.v_enfermedades
                     ORDER BY diagnostico.v_enfermedades.enfermedad;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método llamado al grabar, ejecuta la consulta de inserción o
     * edición según el caso, retorna la id del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / editado
     */
    public function grabaEnfermedad(){

        // si está insertando
        if ($this->IdEnfermedad == 0){
            $this->nuevaEnfermedad();
        } else {
            $this->editaEnfermedad();
        }

        // retorna la id del registro
        return $this->IdEnfermedad;

    }

    /**
     * Método protegido llamado al insertar un nuevo registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaEnfermedad(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.enfermedades
                            (enfermedad,
                             id_usuario)
                            VALUES
                            (:enfermedad,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":enfermedad", $this->Enfermedad);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asignamos la clave
        $this->IdEnfermedad = $this->Link->lastInsertId();

    }

    /**
     * Método llamado al editar un registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaEnfermedad(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.enfermedades SET
                            enfermedad = :enfermedad,
                            id_usuario = :id_usuario
                     WHERE diagnostico.enfermedades.id = :id_enfermedad;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":enfermedad", $this->Enfermedad);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_enfermedad", $this->IdEnfermedad);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que retorna un array con las enfermedades del paciente,
     * recibe como parámetro la clave del protocolo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int - $protocolo
     * @return array
     */
    public function enfermedadesPaciente($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_otras_enfermedades.id_registro AS id,
                            diagnostico.v_otras_enfermedades.enfermedad AS enfermedad,
                            diagnostico.v_otras_enfermedades.fecha AS fecha,
                            diagnostico.v_otras_enfermedades.fecha_alta AS fecha_alta,
                            diagnostico.v_otras_enfermedades.usuario AS usuario
                     FROM diagnostico.v_otras_enfermedades
                     WHERE diagnostico.v_otras_enfermedades.protocolo = '$protocolo'
                     ORDER BY STR_TO_DATE(diagnostico.v_otras_enfermedades.fecha, '%d/%m/%Y');";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que recibe el protocolo de un paciente y la clave de la
     * enfermedad, actualiza el registro del paciente, insertando la
     * enfermedad o removiéndola si está marcada
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function grabaEnfPaciente(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.otras_enfermedades
                            (id_protocolo,
                             id_enfermedad,
                             fecha,
                             id_usuario)
                            VALUES
                            (:id_protocolo,
                             :id_enfermedad,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_enfermedad", $this->IdEnfermedad);
        $psInsertar->bindParam(":fecha", $this->FechaEnfermedad);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave del registro de la
     * tabla pivot de enfermedades del paciente y elimina la
     * entrada
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $id - clave del registro
     */
    public function borraEnfPaciente($id){

        // componemos la consulta
        $consulta = "DELETE FROM diagnostico.otras_enfermedades
                     WHERE diagnostico.otras_enfermedades.id = '$id';";
        $this->Link->exec($consulta);

    }

}