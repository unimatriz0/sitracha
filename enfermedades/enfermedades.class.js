/*
 * Nombre: enfermedades.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 06/03/2018
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de enfermedades
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de enfermedades
 */
class Enfermedades {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initEnfermedades();

        // declaramos el layer emergente
        this.layerEnfermedades = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initEnfermedades(){

        // inicialización de variables de la tabla enfermedades
        this.IdEnfermedad = 0;         // clave de la enfermedad
        this.Enfermedad = "";          // nombre de la enfermedad
        this.IdUsuario = 0;            // clave del usuario
        this.Usuario = "";             // nombre del usuario
        this.FechaAlta = "";           // fecha de alta del registro

        // las variables de la tabla de enfermedades del paciente
        this.Fecha = "";               // fecha de la enfermedad
        this.Protocolo = 0;            // protocolo del paciente

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de enfermedades
     */
    muestraEnfermedades(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("enfermedades/form_enfermedades.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idenfermedad - clave del registro
     * Método que recibe como parámetro la clave del registro y a partir
     * de ella construye la id del elemento del formulario, si no
     * recibe la id asume que es un alta
     */
    verificaEnfermedad(idenfermedad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no recibió la id
        if (typeof(idenfermedad) == "undefined"){

            // asigna
            idenfermedad = "nuevo";

        // si recibió
        } else {

            // asigna en la variable de clase
            this.IdEnfermedad = idenfermedad;

        }

        // verifica que halla ingresado el texto
        if (document.getElementById("enfermedad_" + idenfermedad).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la enfermedad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("enfermedad_" + idenfermedad).focus();
            return false;

        }

        // si llegó hasta aquí asigna en la variable de clase y graba
        this.Enfermedad = document.getElementById("enfermedad_" + idenfermedad).value;
        this.grabaEnfermedad();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta en el servidor
     */
    grabaEnfermedad(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosEnfermedad = new FormData();

        // si está editando
        if (this.IdEnfermedad != 0){
            datosEnfermedad.append("Id", this.IdEnfermedad);
        }

        // agrega la enfermedad
        datosEnfermedad.append("Enfermedad", this.Enfermedad);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "enfermedades/graba_enfermedad.php",
            type: "POST",
            data: datosEnfermedad,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    enfermedades.initEnfermedades();

                    // recarga el formulario para reflejar los cambios
                    enfermedades.muestraEnfermedades();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el menú de pacientes, abre el cuadro de diálogo y presenta
     * la grilla de enfermedades
     */
    grillaEnfermedades(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // verifica que exista un paciente activo
        if (document.getElementById("protocolo_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe tener en pantalla un paciente antes<br>";
            mensaje += "de declarar sus enfermedades";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // setea la variable control de enfermedades
        pacientes.setEnfermedades();

        // cargamos el formulario en una ventana emergente
        this.layerEnfermedades = new jBox('Modal', {
                                  animation: 'flip',
                                  closeOnEsc: true,
                                  closeOnClick: false,
                                  closeOnMouseleave: false,
                                  closeButton: true,
                                  repositionOnContent: true,
                                  overlay: false,
                                  title: 'Enfermedades del Paciente',
                                  draggable: 'title',
                                  theme: 'TooltipBorder',
                                  width:500,
                                  onCloseComplete: function(){
                                    this.destroy();
                                  },
                                  ajax: {
                                      url: 'enfermedades/grilla_enfermedades.html',
                                      reload: 'strict'
                                  }
                            });
        this.layerEnfermedades.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de datos de enfermedades
     * del paciente antes de enviarlo al servidor
     */
    validaEnfPaciente(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no seleccionó la enfermedad
        if (document.getElementById("lista_enf_paciente").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione la enfermedad de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("lista_enf_paciente").focus();
            return false;

        }

        // si no seleccionó la fecha
        if (document.getElementById("fecha_enfermedad").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de la enfermedad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_enfermedad").focus();
            return false;

        }

        // asignamos los valores en las variables de clase
        this.Protocolo = document.getElementById("protocolo_paciente").value;
        this.IdEnfermedad = document.getElementById("lista_enf_paciente").value;
        this.Fecha = document.getElementById("fecha_enfermedad").value;

        // grabamos el registro
        this.grabaEnfermedadPaciente();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idenfermedad - clave de la patología
     * Método que recibe como parámetro la clave de una enfermedad
     * y a partir del protocolo del paciente, ejecuta la consulta
     * en la base de datos
     */
    grabaEnfermedadPaciente(idenfermedad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el formulario
        var datosEnfermedad = new FormData();

        // agregamos los valores
        datosEnfermedad.append("Enfermedad", this.IdEnfermedad);
        datosEnfermedad.append("Protocolo", this.Protocolo);
        datosEnfermedad.append("Fecha", this.Fecha);

        // llamamos por ajax
        $.ajax({
            url: "enfermedades/grabapaciente.php",
            type: "POST",
            data: datosEnfermedad,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {

                // presentamos el mensaje
                new jBox('Notice', {content: "Registro actualizado", color: 'green'});

                // limpiamos el formulario
                enfermedades.limpiaEnfermedades();

                // recargamos la grilla
                enfermedades.cargaGrillaEnfermedades();

            }

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario de enfermedades
     * del paciente
     */
    limpiaEnfermedades(){

        // inicializamos las variables de clase
        this.initEnfermedades();

        // inicializamos el formulario
        document.getElementById("lista_enf_paciente").value = 0;
        document.getElementById("fecha_enfermedad").value = "";
        document.getElementById("usuario_enfermedad").value = sessionStorage.getItem("Usuario");
        document.getElementById("alta_enfermedad").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir del protocolo obtiene la nómina
     * de enfermedades del paciente
     */
    cargaGrillaEnfermedades(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // lo llamamos sincrónico
        $.ajax({
            url: "enfermedades/enfermedades_paciente.php?protocolo=" + document.getElementById("protocolo_paciente").value,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // cargamos la grilla
                enfermedades.listaEnfermedades(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} - vector con la nómina
     * Método que recibe como parámetro la nómina de enfermedades
     * del paciente, limpia la grilla y la recarga dinámicamente
     */
    listaEnfermedades(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var texto;

        // limpia el combo
        $("#grilla_enf_paciente").html('');

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // definimos la fila
            texto = "<tr>";

            // agregamos la enfermedad
            texto += "<td>";
            texto += datos[i].enfermedad;
            texto += "</td>";

            // agregamos la fecha
            texto += "<td>";
            texto += datos[i].fecha;
            texto += "</td>";

            // agregamos el usuario
            texto += "<td>";
            texto += datos[i].usuario;
            texto += "</td>";

            // agrega la fecha de alta
            texto += "<td>";
            texto += datos[i].fecha_alta;
            texto += "</td>";

            // implementa el botón borrar
            texto += "<td>";
            texto += "<input type='button' ";
            texto += "       name='btnBorraEnfermedad' ";
            texto += "       id='btnBorraEnfermedad' ";
            texto += "       title='Elimina el registro' ";
            texto += "       onClick='enfermedades.borraEnfermedadPaciente(" + datos[i].id + ")' ";
            texto += "       class='botonborrar' >";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos al dom del navegador
            $("#grilla_enf_paciente").append(texto);

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - select del formulario
     * @param {string} idenfermedad - clave de la enfermedad
     * Método que recibe como parámetro la id del select de un formulario, la clave
     * de la enfermedad (optativo) preseleccionada, carga en el select recibido como
     * argumento la nómina de enfermedades y en todo caso preselecciona la recibida
     */
    nominaEnfermedades(idelemento, idenfermedad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto al combo
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos sincrónico
        $.ajax({
            url: "enfermedades/nomina_enfermedades.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la localidad
                    if (typeof(idenfermedad) != "undefined"){

                        // verifica y si es igual preselecciona
                        if (data[i].Id == idenfermedad){
                            $("#" + idelemento).append("<option selected value=" + data[i].id_enfermedad + ">" + data[i].enfermedad + "</option>");
                        } else {
                            $("#" + idelemento).append("<option value=" + data[i].id_enfermedad + ">" + data[i].enfermedad + "</option>");
                        }

                    // si no recibió nada
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].id_enfermedad + ">" + data[i].enfermedad + "</option>");
                    }

                }

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y luego de pedir confirmación ejecuta la consulta de
     * eliminación
     */
    borraEnfermedadPaciente(clave){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Enfermedad',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('enfermedades/borrar.php?id='+clave,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // inicializamos las variables
                            enfermedades.initEnfermedades();

                            // inicializamos el formulario
                            enfermedades.limpiaEnfermedades();

                            // recargamos la grilla
                            enfermedades.cargaGrillaEnfermedades();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}