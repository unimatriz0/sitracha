<?php

/**
 *
 * enfermedades_paciente | enfermedades/enfermedades_paciente.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el protocolo de un paciente y retorna
 * un array json con la nómina de las enfermedades declaradas
*/

// incluimos e instanciamos la clase
require_once("enfermedades.class.php");
$enfermedades = new Enfermedades();

// obtenemos la nómina
$nomina = $enfermedades->enfermedadesPaciente($_GET["protocolo"]);

// retornamos la nómina
echo json_encode($nomina);

?>