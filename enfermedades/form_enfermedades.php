<?php

/**
 *
 * enfermedades/form_enfermedades.class.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario para el ABM del diccionario 
 * de enfermedades
 * 
*/

// incluimos e instanciamos las clases
require_once("enfermedades.class.php");
$enfermedades = new Enfermedades();

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["ID"])){
    $usuario_actual = $_SESSION["Usuario"];
}
session_write_close();

// presentamos el título
echo "<h2>Diccionario de Enfermedades</h2>";

// definimos la tabla
echo "<table width='60%' align='center' border'2'>";

// definimos el header
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Enfermedad</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la nómina de estados civiles
$nomina = $enfermedades->nominaEnfermedades();

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos la enfermedad
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre completo de la enfermedad'>";
    echo "<input type='text'
                 size='30'
                 name='enfermedad_$id_enfermedad'
                 id='enfermedad_$id_enfermedad'
                 value='$enfermedad'>";
    echo "</span>";
    echo "</td>";

    // presenta la fecha de alta y el usuario
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // arma el enlace
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaEnfermedad'
           id='btnGrabaEnfermedad'
           title='Pulse para grabar el registro'
           onClick='enfermedades.verificaEnfermedad($id_enfermedad)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// presentamos la fila para el alta
echo "<tr>";

// presentamos la enfermedad
echo "<td>";
echo "<span class='tooltip'
            title='Nombre completo de la enfermedad'>";
echo "<input type='text'
                size='30'
                name='enfermedad_nuevo'
                id='enfermedad_nuevo'>";
echo "</span>";
echo "</td>";

// obtenemos la fecha actual
$fecha_alta = date('d/m/Y');

// presenta la fecha de alta y el usuario
echo "<td align='center'>$usuario_actual</td>";
echo "<td align='center'>$fecha_alta</td>";

// arma el enlace
echo "<td align='center'>";
echo "<input type='button'
        name='btnGrabaEnfermedad'
        id='btnGrabaEnfermedad'
        title='Pulse para grabar el registro'
        onClick='enfermedades.verificaEnfermedad()'
        class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos el cuerpo y la tabla
echo "</body>";
echo "</table>";

?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>