<?php

/**
 *
 * nomina_enfermedades | enfermedades/nomina_enfermedades.php
 *
 * @package     Diagnostico
 * @subpackage  Enfermedades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna el array json con la nómina de enfermedades
 * declaradas
*/

// incluimos e instanciamos las clases
require_once("enfermedades.class.php");
$enfermedades = new Enfermedades();

// obtenemos el vector
$nomina = $enfermedades->nominaEnfermedades();

// encodeamos y retornamos
echo json_encode($nomina);

?>
