<?php

/*
 * Nombre: informenacional.class.php
 * Proyecto: CCE
 * Fecha: 08/05/2017
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Comentarios: Clase que contiene los metodos para generar las estadisticas
 *              de los reportes nacionales
 *
 */

// inclusion de archivos
require_once ("conexion.class.php");
require_once ("herramientas.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definicion de la clase
class InformeNacional {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Pais;                  // pais del usuario activo
    protected $Funciones;             // clase de herramientas

    // constructor de la clase
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesion y obtenemos el pais
        session_start();
        $this->Pais = $_SESSION["Pais"];

        // iniciamos la clase de herramientas
        $this->Funciones = new Herramientas();

    }

    // destructor de la clase
    function __destruct() {

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @param operativo string con el número de operativo
     * @param jurisdiccion
     * @return resultset con los laboratorios que han cargado determinaciones
     * para el operativo indicado
     * Método que recibe como parámetro un número de operativo y retorna
     * un vector con los datos de los laboratorios que tienen determinaciones
     * cargadas en ese operativo, en caso de recibir como parametro una
     * jurisdiccion, retorna los laboratorios de esa jurisdiccion
     */
    public function determinacionesCargadas($operativo, $jurisdiccion = null){

        // si no recibio jurisdiccion
        if ($jurisdiccion == null){

            // compone la consulta
            $consulta = "SELECT COUNT(cce.chag_datos.ID) AS determinaciones,
                                 diccionarios.provincias.NOM_PROV AS jurisdiccion,
                                 cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                             INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                             INNER JOIN cce.diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo'
                         GROUP BY cce.laboratorios.NOMBRE
                         ORDER BY diccionarios.provincias.NOM_PROV,
                                  cce.laboratorios.NOMBRE; ";

            // obtenemos el vector, lo convertimos a minusculas y retornamos
            $resultado = $this->Link->query($consulta);
            $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $registros = array_change_key_case($fila, CASE_LOWER);

        // si recibio la jurisdiccion
        } else {

            // compone la consulta
            $consulta = "SELECT COUNT(cce.chag_datos.ID) AS determinaciones,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                             INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                             INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                             INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo' AND
                               diccionarios.provincias.NOM_PROV = '$jurisdiccion'
                         GROUP BY cce.laboratorios.NOMBRE
                         ORDER BY cce.laboratorios.NOMBRE; ";

            // obtenemos el vector, lo convertimos a minusculas y retornamos
            $resultado = $this->Link->query($consulta);
            $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $registros = array_change_key_case($fila, CASE_LOWER);

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * @param operativo string con el número de operativo
     * @param jurisdiccion
     * @return resultset cn los laboratorios sin cargar determinaciones
     * Método que recibe como parámetro un número de operativo y retorna
     * un resultset con los laboratorios que no tienen determinaciones
     * cargadas, si recibe la jurisdiccion solo selecciona los laboratorios
     * correspondientes a esa jurisdiccion
     */
    public function determinacionesSinCargar($operativo, $jurisdiccion = null){

        // si no recibio jurisdiccion
        if ($jurisdiccion == null){

            // componemos la consulta
            $consulta = "SELECT cce.laboratorios.ID AS id,
                                cce.laboratorios.NOMBRE AS laboratorio,
                                diccionarios.provincias.NOM_PROV AS jurisdiccion
                         FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE cce.laboratorios.ACTIVO = 'Si' AND
                               cce.laboratorios.ID NOT IN (SELECT existentes.ID AS id_laboratorio
                                                           FROM cce.chag_datos INNER JOIN cce.laboratorios AS existentes ON cce.chag_datos.LABORATORIO = existentes.ID
                                                                               INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                           WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo')
                         ORDER BY diccionarios.provincias.NOM_PROV,
                                  cce.laboratorios.NOMBRE;";

            // obtenemos el vector, lo convertimos a minusculas y retornamos
            $resultado = $this->Link->query($consulta);
            $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $registros = array_change_key_case($fila, CASE_LOWER);

        // si recibio la jurisdiccion
        } else {

            // componemos la consulta
            $consulta = "SELECT cce.laboratorios.ID AS id,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades_censales.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE diccionarios.provincias.NOM_PROV = '$jurisdiccion' AND
                               cce.laboratorios.ACTIVO = 'Si'
                               cce.laboratorios.ID NOT IN (SELECT existentes.ID AS id_laboratorio
                                                           FROM cce.chag_datos INNER JOIN cce.laboratorios AS existentes ON cce.chag_datos.LABORATORIO = existentes.ID
                                                                               INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                                                               INNER JOIN diccionarios.localidades ON existentes.LOCALIDAD = diccionarios.localidades.CODLOC
                                                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                           WHERE cce.operativos_chagas.OPERATIVO_NRO = '$operativo' AND
                                                                 diccionarios.provincias.NOM_PROV = '$jurisdiccion')
                         ORDER BY cce.laboratorios.NOMBRE;";

            // obtenemos el vector, lo convertimos a minusculas y retornamos
            $resultado = $this->Link->query($consulta);
            $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $registros = array_change_key_case($fila, CASE_LOWER);

        }

        // retornamos el vector
        return $registros;

    }

    /**
     * @param anio string con el año a informar
     * @return paneles string con el número de paneles enviados
     * Método que recibe como parámetro un string con el año a informar
     * y retorna el número de paneles enviados a las jurisdicciones
     * en ese año
     */
    public function panelesEnviados ($anio){

        // Componemos y ejecutamos la consulta
        $consulta = "SELECT COUNT(cce.etiquetas.ETIQUETA_NRO) DIV 6 AS paneles
                     FROM cce.etiquetas
                     WHERE cce.etiquetas.OPERATIVO LIKE '%$anio';";

        // obtenemos el vector, lo convertimos a minusculas y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetch(PDO::FETCH_ASSOC);
        $registro = array_change_key_case($fila, CASE_LOWER);
        extract($registro);
        return $paneles;

    }

    /**
     * @return vector con la nomina de jurisdicciones
     * Metodo que retorna un vector con la nomina de jurisdicciones
     * del pais del usuario
     */
    protected function nominaJurisdicciones(){

        // componemos la consulta con la nomina de provincias
        $sql = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                WHERE diccionarios.paises.NOMBRE = '$this->Pais'
                ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($sql);

        // obtenemos la nomina de jurisdicciones
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $registros = array_change_key_case($fila, CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * @return array con la nomina de laboratorios
     * Metodo que retorna un array con la nomina de jurisdicciones y el
     * numero de laboratorios por fuente de financiamiento usamos un array
     * para devolver la tabla al reporte lista para ser presentada
     */
    public function laboratoriosParticipantes(){

        // obtenemos la nomina total de jurisdicciones
        $registro = $this->nominaJurisdicciones();

        // recorremos el vector
        foreach($registro AS $record){

            // obtenemos el registro
            extract($record);

            // declaramos el array y lo agregamos
            $nominaParticipantes[$jurisdiccion] = array();

        }

        // componemos la consulta con todos los laboratorios relevados
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            diccionarios.dependencias.DEPENDENCIA AS dependencia,
                            COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorios
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.dependencias ON cce.laboratorios.DEPENDENCIA = diccionarios.dependencias.ID
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE diccionarios.paises.NOMBRE = '$this->Pais'
                     GROUP BY diccionarios.provincias.NOM_PROV,
                              diccionarios.dependencias.DEPENDENCIA
                     ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $registros = array_change_key_case($$fila, CASE_LOWER);

        // recorremos el vector
        foreach($registros AS $record){

            // obtenemos el registro
            extract($record);

            // según la dependencia
            switch ($dependencia) {

                case "Nacional":
                    $nominaParticipantes[$jurisdiccion]["Nacional"] = $laboratorios;
                    $TotalNacionales += $laboratorios;
                    break;

                case "Provincial":
                    $nominaParticipantes[$jurisdiccion]["Provincial"] = $laboratorios;
                    $TotalProvinciales += $laboratorios;
                    break;

                case "Municipal":
                    $nominaParticipantes[$jurisdiccion]["Municipal"] = $laboratorios;
                    $TotalMunicipales += $laboratorios;
                    break;

                case "Privado":
                    $nominaParticipantes[$jurisdiccion]["Privado"] = $laboratorios;
                    $TotalMunicipales += $laboratorios;
                    break;

                case "Universitario":
                    $nominaParticipantes[$jurisdiccion]["Universitario"] = $laboratorios;
                    $TotalUniversitario += $laboratorios;
                    break;

                default:
                    $nominaParticipantes[$jurisdiccion]["Otra"] = $laboratorios;
                    $TotalOtros += $laboratorios;
                    break;

            }

        }

        // ahora recorremos la matriz calculando los totales por provincia
        foreach($nominaParticipantes AS $provincia => $datos){

            // agregamos el total
            $nominaParticipantes[$provincia]["General"] = $datos["Nacional"] +
                                                          $datos["Provincial"] +
                                                          $datos["Municipal"] +
                                                          $datos["Privado"] +
                                                          $datos["Universitario"] +
                                                          $datos["Otra"];

        }

        // presentamos el total general
        $nominaParticipantes["Total"] = array();
        $nominaParticipantes["Total"]["Nacional"] = $TotalNacionales;
        $nominaParticipantes["Total"]["Provincial"] = $TotalProvinciales;
        $nominaParticipantes["Total"]["Municipall"] = $TotalMunicipales;
        $nominaParticipantes["Total"]["Privado"] = $TotalPrivado;
        $nominaParticipantes["Total"]["Universitario"] = $TotalUniversitario;
        $nominaParticipantes["Total"]["Otra"] = $TotalOtros;
        $nominaParticipantes["Total"]["General"] = $TotalNacionales +
                                                   $TotalProvinciales +
                                                   $TotalMunicipales +
                                                   $TotalPrivado +
                                                   $TotalUniversitario +
                                                   $TotalOtros;

        // retornamos el vector
        return Nomina;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la nomina de jurisdicciones, el numero de
     * paneles enviados a cada jurisdiccion, el numero de laboratorios
     * participantes, el numero de determinaciones cargadas y el
     * porcentaje de respuesta
     */
    public function panelesProcesados($anio){

        // componemos la consulta de los laboratorios activos
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            COUNT(cce.laboratorios.ID) AS laboratorios
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE diccionarios.paises.NOMBRE = '$this->Pais' AND
                           cce.laboratorios.ACTIVO = 'Si'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $registros = array_change_key_case($$fila, CASE_LOWER);

        // recorremos el vector cargando las provincias y los laboratorios
        foreach($registros AS $record){

            // obtenemos el registro
            extract($record);

            // lo agregamos
            $panelesEnviados[$jurisdiccion] = array();

            // ahora agregamos la cantidad de laboratorios activos
            $panelesEnviados[$jurisdiccion]["Activos"] = $laboratorios;

            // incrementamos el total de laboratorios
            $totalLaboratorios += $laboratorios;

        }

        // agregamos la ultima fila
        $panelesEnviados["Total"] = array();
        $panelesEnviados["Total"]["Activos"] = $totalLaboratorios;

        // componemos la consulta de los laboratorios a los que se enviaron
        // muestras
        $sql = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                       COUNT(DISTINCT(cce.laboratorios.ID)) AS laboratorios
                FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                      INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                      INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                      INNER JOIN cce.etiquetas ON cce.laboratorios.ID = cce.etiquetas.ID_LABORATORIO
                WHERE cce.laboratorios.activo = 'Si' AND
                      cce.etiquetas.OPERATIVO LIKE '%$anio' AND
                      diccionarios.paises.NOMBRE = '$this->Pais'
                GROUP BY diccionarios.provincias.NOM_PROV
                ORDER BY diccionarios.provincias.NOM_PROV;";
        $muestras = $this->Link->query($sql);
        $enviados = $muestras->fetchAll(PDO::FETCH_ASSOC);
        $sueros = array_change_key_case($enviados, CASE_LOWER);

        // recorremos el vector
        foreach ($sueros AS $record){

            // obtenemos el registro
            extract($record);

            // agregamos los paneles enviados
            $panelesEnviados[$jurisdiccion]["Paneles"] = $laboratorios;

            // incrementa el total de paneles
            $totalPanelesEnviados += $laboratorios;

        }

        // agregamos el total de paneles enviados
        $panelesEnviados["Total"]["Paneles"] = $totalPanelesEnviados;

        // componemos la consulta de los laboratorios que cargaron muestras
        $query = "SELECT calidad.provincias.NOM_PROV AS jurisdiccion,
                         COUNT(DISTINCT(laboratorios.ID)) AS laboratorios
                  FROM laboratorios INNER JOIN calidad.localidades_censales ON laboratorios.LOCALIDAD = calidad.localidades_censales.CODLOC
                                    INNER JOIN calidad.provincias ON calidad.localidades_censales.CODPCIA = calidad.provincias.COD_PROV
                                    INNER JOIN calidad.paises ON calidad.provincias.PAIS = calidad.paises.ID
                                    INNER JOIN chag_datos ON laboratorios.ID = chag_datos.LABORATORIO
                                    INNER JOIN operativos_chagas ON chag_datos.OPERATIVO = operativos_chagas.ID
                  WHERE laboratorios.activo = 'Si' AND
                        operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                        calidad.paises.NOMBRE = '$this->Pais'
                  GROUP BY calidad.provincias.NOM_PROV
                  ORDER BY calidad.provincias.NOM_PROV; ";
        $paneles = $this->Link->query($query);
        $row = $paneles->fetchAll(PDO::FETCH_ASSOC);
        $procesados = array_change_key_case($row, CASE_LOWER);

        // recorremos el vector de paneles
        foreach($procesados AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agregamos a la matriz
            $panelesEnviados[$jurisdiccion]["Procesados"] = $laboratorios;

            // incrementa el total de paneles
            $totalPanelesProcesados += $laboratorios;

        }

        // agregamos el total y el porcentaje
        $panelesEnviados["Total"]["Procesados"] = $totalPanelesProcesados;

        // ahora recorremos el array calculando los porcentajes
        foreach ($panelesEnviados AS $provincia => $datos){

            // calculamos el porcentaje dejando el punto decimal
            $respuesta = ($datos[$procesados]["Procesados"] / $datos["Enviados"]) * 100;
            $porcentaje = number_format($respuesta, 2);

            // lo asignamos en la matriz
            $panelesEnviados[$provincia]["Porcentaje"] = $porcentaje;

        }

        // retornamos el vector
        return $panelesEnviados;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la nomina de jurisdicciones y el numero de
     * determinaciones cargadas fuera de termino
     */
    public function fueraTermino($anio){

        // obtenemos las determinaciones cargadas a termino
        $consulta = "SELECT COUNT(cce.chag_datos.ID) AS termino
                     FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE cce.chag_datos.FECHA_ALTA <= cce.operativos_chagas.FECHA_FIN AND
                           cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio';";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetch(PDO::FETCH_ASSOC);
        $aTermino = array_change_key_case($fila, CASE_LOWER);
        extract($aTermino);

        // obtenemos las determinaciones cargadas fuera de termino
        $query = "SELECT COUNT(cce.chag_datos.ID) AS fuera
                  FROM cce.chag_datos INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                  WHERE cce.chag_datos.FECHA_ALTA > cce.operativos_chagas.FECHA_FIN AND
                        cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio';";
        $registros = $this->Link->query($query);
        $row = $registros->fetch(PDO::FETCH_ASSOC);
        $fueraTermino = array_change_key_case($row, CASE_LOWER);
        extract($fueraTermino);

        // creamos el array
        $Determinaciones = array("Termino" => $termino,
                                 "Fuera" => $fuera);

        // retornamos la matriz
        return $Determinaciones;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de la carga de muestras
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por jurisdiccion de las tecnicas
     * mas empleadas
     */
    public function distribucionMuestras($anio){

        // componemos y ejecutamos la consulta de las provincias que
        // remitieron muestras y el numero de determinaciones de cada
        // tecnica
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.tecnicas.TECNICA AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                         INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                    WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                          diccionarios.paises.NOMBRE = '$this.Pais'
                    GROUP BY diccionarios.provincias.NOM_PROV,
                             cce.tecnicas.TECNICA
                    ORDER BY diccionarios.provincias.NOM_PROV,
                             cce.chag_datos.TECNICA;";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $distribucion = array_change_key_case($$fila, CASE_LOWER);

        // recorremos el vector
        foreach ($distribucion AS $registro){

            // obtenemos el registro
            extract($registro);

            // si cambio la jurisdiccion
            if ($provinciaAnterior != $jurisdiccion){

                // dimensionamos la matriz
                $Muestras[$jurisdiccion] = array();

                // actualizamos la variable control
                $provinciaAnterior = $jurisdiccion;

            }

            // según la tecnica
            switch ($tecnica) {

                case "AP":
                    $Muestras[$jurisdiccion]["AP"] = $determinaciones;
                    $totalAp += $determinaciones;
                    break;

                case "ELISA":
                    $Muestras[$jurisdiccion]["ELISA"] = $determinaciones;
                    $totalElisa += $determinaciones;
                    break;

                case "HAI":
                    $Muestras[$jurisdiccion]["HAI"] = $determinaciones;
                    $totalHai += $determinaciones;
                    break;

                case "IFI":
                    $Muestras[$jurisdiccion]["IFI"] = $determinaciones;
                    $totalIfi += $determinaciones;
                    break;

                case "QUIMIOLUMINISCENCIA":
                    $Muestras[$jurisdiccion]["QUIMIO"] = $determinaciones;
                    $totalQuimio += $determinaciones;
                    break;

            }

        }

        // presenta el total general
        $Muestras["Total"] = array();
        $Muestras["Total"]["AP"] = $totalAp;
        $Muestras["Total"]["ELISA"] = $totalElisa;
        $Muestras["Total"]["HAI"] = $totalHai;
        $Muestras["Total"]["IFI"] = $totalIfi;
        $Muestras["Total"]["QUIMIO"] = $totalQuimio;

        // ahora recorremos el vector calculando las determinaciones totales
        foreach ($Muestras AS $provincia => $datos){

            // calculamos el total
            $Muestras[$provincia]["Total"] = $datos["AP"] +
                                             $datos["ELISA"] +
                                             $datos["HAI"] +
                                             $datos["IFI"] +
                                             $datos["QUIMIO"];

        }

        // retornamos el vector
        return $Muestras;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de lector / visual en elisa
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion de uso de lector de placas
     */
    public function lectorVisual($anio){

        // declaracion de variables
        $totalLector = 0;
        $totalVisual = 0;
        $jurisdiccionAnterior = "";
        $jurisdiccion = "";
        $metodo = "";
        $determinaciones = 0;
        $Lectores = array();

        // componemos la consulta
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.chag_datos.METODO AS metodo,
                            COUNT(cce.chag_datos.METODO) AS determinaciones
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                         INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = tecnicas.ID
                     WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this.Pais' AND
                           cce.tecnicas.TECNICA = 'ELISA'
                     GROUP BY diccionarios.provincias.NOM_PROV,
                              cce.chag_datos.METODO
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.chag_datos.METODO;";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $distribucion = array_change_key_case($$fila, CASE_LOWER);

        // recorremos el vector
        foreach($distribucion AS $registro){

           // obtenemos el registro
           extract($registro);

           // si cambio la jurisdiccion
           if ($jurisdiccionAnterior != $jurisdiccion){

               // definimos la matriz
               $Lectores[$jurisdiccion] = array();

               // asignamos la variable
               $jurisdiccionAnterior = $jurisdiccion;

            }

            // según la tecnica
            switch ($metodo) {

                case "Lector":
                    $Lectores[$jurisdiccion]["Lector"] = $determinaciones;
                    $totalLector += $determinaciones;
                    break;

                case "Visual":
                    $Lectores[$jurisdiccion]["Visual"] = $determinaciones;
                    $totalVisual += $determinaciones;
                    break;

            }

        }

        // presenta el total general
        $Lectores["Total"] = array();
        $Lectores["Total"]["Lector"] = $totalLector;
        $Lectores["Total"]["Visual"] = $totalVisual;

        // ahora recorremos el array calculando los totales
        foreach($Lectores AS $provincia => $datos){

            // lo agregamos
            $Lectores[$provincia]["Total"] = $datos["Lector"] + $datos["Visual"];

        }

        // retornamos la matriz
        return Lectores;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de tecnicas utilizadas
     * Metodo que recibe como parametro el año a reportar y genera la
     * matriz con el numero de tecnicas empleadas por cada jurisdiccion
     */
    public function nroTecnicas($anio){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT consulta.provincia AS jurisdiccion,
                            COUNT(consulta.laboratorios) AS laboratorios,
                            consulta.tecnicas AS nro_tecnicas
                     FROM (SELECT diccionarios.provincias.NOM_PROV AS provincia,
                                  cce.laboratorios.ID AS laboratorios,
                                  COUNT(DISTINCT(cce.chag_datos.TECNICA)) AS tecnicas
                           FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                               INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                               INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                               INNER JOIN cce.operativos_chagas ON cce.operativos_chagas.ID = cce.chag_datos.OPERATIVO
                           WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                                 diccionarios.paises.NOMBRE = '$this->Pais'
                           GROUP BY cce.laboratorios.ID) AS consulta
                     GROUP BY consulta.provincia,
                              consulta.tecnicas
                     ORDER BY consulta.provincia,
                     consulta.tecnicas;";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $distribucion = array_change_key_case($$fila, CASE_LOWER);

        // recorremos el vector
        foreach ($distribucion AS $registro){

            // obtenemos el registro
            extract($registro);

            // si cambio la jurisdiccion
            if ($provinciaAnterior != $jurisdiccion){

                // dimensionamos la matriz
                $Tecnicas[$jurisdiccion] = array();

                // asignamos la variable
                $provinciaAnterior = $jurisdiccion;

            }

            // según el numero de tecnicas
            switch ($nro_tecnicas) {

                case "1":
                    $Tecnicas[$jurisdiccion]["1_Tecnica"] = $laboratorios;
                    $totalUna += $laboratorios;
                    break;

                case "2":
                    // asignamos
                    $Tecnicas[$jurisdiccion]["2_Tecnnicas"] = $laboratorios;
                    $totalDos += $laboratorios;
                    break;

                case "3":
                    // asignamos
                    $Tecnicas[$jurisdiccion]["3_Tecnicas"] = $laboratorios;
                    $totalTres += $laboratorios;
                    break;

                case "4":
                    // asignamos
                    $Tecnicas[$jurisdiccion]["4_Tecnicas"] = $laboratorios;
                    $totalCuatro += $laboratorios;
                    break;

            }

        }

        // presenta el total general
        $Tecnicas["Total"] = array();
        $Tecnicas["Total"]["1_Tecnica"] = $totalUna;
        $Tecnicas["Total"]["2_Tecnicas"] = $totalDos;
        $Tecnicas["Total"]["3_Tecnicas"] = $totalTres;
        $Tecnicas["Total"]["4_Tecnicas"] = $totalCuatro;

        // retorna la matriz
        return Tecnicas;

    }

    /**
     * @param anio string con el año a reportar
     * @return array con la eficiencia por jurisdicción
     * Método que recibe como parámetro el año a calcular y retorna una
     * matriz con el número de determinaciones, las correctas y la
     * eficiencia de cada jurisdicción
     */
    public function obtenerConcordancia($anio){

        // declaración de variables
        $Concordantes = array();
        $totalDeterminaciones = 0;
        $totalCorrectas = 0;
        $jurisdiccion = "";
        $determinaciones = 0;

        // obtenemos el total de determinaciones por jurisdiccion
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                         INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                     WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this-Pais'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV; ";
        $resultado = $this->Link->query($consulta);
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);
        $distribucion = array_change_key_case($$fila, CASE_LOWER);

        // recorremos el array completando las jurisdicciones y el
        // numero de determinaciones realizadas
        foreach($distribucion AS $registro){

            // obtenemos el registro
            extract($registro);

            // inicializamos el array
            $Concordantes[$jurisdiccion] = array();

            // asignamos en la matriz
            $Concordantes[$jurisdiccion]["Determinaciones"] = $determinaciones;
            $totalDeterminaciones += $determinaciones;

        }

        // obtenemos las determinaciones correctas por jurisdicción
        $query = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                         COUNT(cce.chag_datos.ID) AS determinaciones
                  FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                      INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                      INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                      INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                      INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                  WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                        diccionarios.paises.NOMBRE = '$this->Pais' AND
                        (cce.chag_datos.DETERMINACION = cce.chag_datos.CORRECTO OR
                         (cce.chag_datos.DETERMINACION = 'Reactivo' AND cce.chag_datos.CORRECTO = 'Indeterminado') OR
                         (cce.chag_datos.DETERMINACION = 'Indeterminado' AND cce.chag_datos.CORRECTO = 'Reactivo'))
                  GROUP BY diccionarios.provincias.NOM_PROV
                  ORDER BY diccionarios.provincias.NOM_PROV; ";
        $sql = $this->Link->query($query);
        $row = $sql->fetchAll(PDO::FETCH_ASSOC);
        $correctas = array_change_key_case($row, CASE_LOWER);

        // recorremos el vector
        foreach($correctas AS $registro){

            // obtenemos el registro
            extract($registro);

            // asignamos en la matriz
            $Concordantes[$jurisdiccion]["Correctas"] = $determinaciones;

            // incrementamos los contadores
            $totalCorrectas += $determinaciones;

        }

        // incorporamos la ultima fila
        $Concordantes["Total"] = array();
        $Concordantes["Total"]["Determinaciones"] = $totalDeterminaciones;
        $Concordantes["Total"]["Correctas"] = $totalCorrectas;

        // ahora recorremos la matriz calculando la eficiencia
        foreach($Concordantes AS $provincia => $datos){

            // calculamos la eficiencia
            $eficiencia = ($datos["Correctas"] / $datos["Determinaciones"]) * 100;
            $porcentaje = number_format($eficiencia, 2) . " %";

            // asignamos en la matriz
            $Concordantes[$provincia]["Eficiencia"] = $porcentaje;

        }

        // retornamos el vector
        return Concordantes;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la eficiencia por tecnica
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la eficiencia por cada una de las tecnicas
     */
    public function eficienciaTecnicas($anio){

        /* usamos una matriz de 16 columnas para luego poder tomar la
           concordancia el orden de las columnas es
           0. Jurisdiccion
           1. Determinaciones AP
           2. Correctas AP
           3. Eficiencia AP
           4. Determinaciones ELISA
           5. Correctas ELISA
           6. Eficiencia ELISA
           7. Determinaciones HAI
           8. Correctas HAI
           9. Eficiencia HAI
           10. Determinaciones IFI
           11. Correctas IFI
           12. Eficiencia IFI
           13. Determinaciones Quimioluminiscencia
           14. Correctas Quimioluminiscencia
           15. Eficiencia Quimioluminiscencia

        */

        // obtenemos el numero de determinaciones por cada tecnica
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.tecnicas.TECNICA AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                         INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                     WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this->Pais'
                     GROUP BY diccionarios.provincias.NOM_PROV,
                              cce.chag_datos.TECNICA
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.chag_datos.TECNICA;";
        $sql = $this->Link->query($consulta);
        $fila = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = array_change_key_case($fila, CASE_LOWER);

        // recorremos el vector
        foreach ($resultado AS $registro){

            // si cambio la jurisdiccion
            if ($jurisdiccionAnterior != $jurisdiccion){

                // obtenemos el registro
                extract($registro);

                // dimensionamos la matriz
                $Concordancia[$jurisdiccion] = array();

                // asignamos el nuevo valor
                $jurisdiccionAnterior = $jurisdiccion;

            }

            // segun la tecnica
            switch ($tecnica){

                // si es ap
                case "AP":
                    $Concordancia[$jurisdiccion]["AP"] = $determinaciones;
                    $totalAp += $determinaciones;
                    break;

                // si es elisa
                case "ELISA";
                    $Concordancia[$jurisdiccion]["ELISA"] = $determinaciones;
                    $totalElisa += $determinaciones;
                    break;

                // si es hai
                case "HAI":
                    $Concordancia[$jurisdiccion]["HAI"] = $determinaciones;
                    $totalHai += $determinaciones;
                    break;

                // si es ifi
                case "IFI":
                    $Concordancia[$jurisdiccion]["IFI"] = $determinaciones;
                    $totalIfi += $determinaciones;
                    break;

                // si es quimioluminiscencia
                case "QUIMIOLUMINISCENCIA":
                    $Concordancia[$jurisdiccion]["QUIMIO"] = $determinaciones;
                    $totalQuimio += $determinaciones;
                    break;

            }

        }

        // obtenemos los resultados correctos por tecnica
        $query = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                         cce.tecnicas.TECNICA AS tecnica,
                         COUNT(cce.chag_datos.ID) AS determinaciones
                  FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                      INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                      INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                      INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                      INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                      INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                  WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                        diccionarios.paises.NOMBRE = '$this->Pais' AND
                        (cce.chag_datos.DETERMINACION = cce.chag_datos.CORRECTO OR
                         (cce.chag_datos.DETERMINACION = 'Reactivo' AND cce.chag_datos.CORRECTO = 'Indeterminado') OR
                         (cce.chag_datos.DETERMINACION = 'Indeterminado' AND cce.chag_datos.CORRECTO = 'Reactivo'))
                  GROUP BY diccionarios.provincias.NOM_PROV,
                           cce.chag_datos.TECNICA
                  ORDER BY diccionarios.provincias.NOM_PROV,
                           cce.chag_datos.TECNICA;";
        $vector = $this->Link->query($query);
        $row = $vector->fetchAll(PDO::FETCH_ASSOC);
        $correctas = array_change_key_case($row, CASE_LOWER);

        // recorremos el vector
        foreach ($correctas AS $registro){

            // obtenemos el registro
            extract($registro);

            // segun la tecnica
            switch (Resultado.getString("tecnica")){

                // si es ap
                case "AP":
                    $Concordancia[$jurisdiccion]["CorrectasAp"] = $determinaciones;
                    $correctasAp += $determinaciones;
                    break;

                // si es elisa
                case "ELISA":
                    $Concordancia[$jurisdiccion]["CorrectasElisa"] = $determinaciones;
                    $correctasElisa += $determinaciones;
                    break;

                // si es hai
                case "HAI":
                    $Concordancia[$jurisdiccion]["CorrectasHai"] = $determinaciones;
                    $correctasHai += $determinaciones;
                    break;

                // si es ifi
                case "IFI":
                    $Concordancia[$jurisdiccion]["CorrectsIfi"] = $determinaciones;
                    $correctasIfi += $determinaciones;
                    break;

                // si es quimio
                case "QUIMIOLUMINISCENCIA":
                    $Concordancia[$jurisdiccion]["CorrectasQuimio"] = $determinaciones;
                    $correctasQuimio += $determinaciones;
                    break;

            }

        }

        // ahora completamos la tabla
        $Concordancia["Total"] = array();
        $Concordancia["Total"]["AP"] = $totalAp;
        $Concordancia["Total"]["CorrectasAp"] = $correctasAp;
        $Concordancia["Total"]["ELISA"] = $totalElisa;
        $Concordancia["Total"]["CorrectasElisa"] = $correctasElisa;
        $Concordancia["Total"]["HAI"] = $totalHai;
        $Concordancia["Total"]["CorrectasHai"] = $correctasHai;
        $Concordancia["Total"]["IFI"] = $totalIfi;
        $Concordancia["Total"]["CorrectasIfi"] = $correctasIfi;
        $Concordancia["Total"]["QUIMIO"] = $totalQuimio;
        $Concordancia["Total"]["CorrectasQuimio"] = $correctasQuimio;

        // recorremos la tabla calculando la eficiencia de cada
        // tecnica pero verificando los valores nulos
        //
        // retornamos la matriz
        return $Concordancia;

    }

    /**
     * @param anio string con el año a reportar
     * @return resultset con la nómina de jurisdicciones
     * Método que recibe como parámetro el año a reportar y retorna un
     * vector con las provincias que no participaron del control de calidad
     */
    public function provinciasFaltantes($anio){

        // componemos la consulta
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           diccionarios.provincias.NOM_PROV NOT IN
                           (SELECT diccionarios.provincias.NOM_PROV AS provincia
                            FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                                INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                                INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                            WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio')
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $vector = $this->Link->query($consulta);
        $row = $vector->fetchAll(PDO::FETCH_ASSOC);
        $resultado = array_change_key_case($row, CASE_LOWER);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @return resultset con la nómina de técnicas registradas
     */
    public function nominaTecnicas(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT cce.tecnicas.TECNICA AS tecnica
                     FROM cce.tecnicas
                     ORDER BY cce.tecnicas.TECNICA;";
        $vector = $this->Link->query($consulta);
        $row = $vector->fetchAll(PDO::FETCH_ASSOC);
        $resultado = array_change_key_case($row, CASE_LOWER);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de valores mal informados
     * Metodo que recibe como parametro el año a reportar y retorna
     * una matriz con la distribucion de valores de corte y lectura
     * mal informados
     */
    public function valoresCorte($anio){

        // compone la consulta calculando los errores de corte
        // que son aquellos en donde se seleccionó otro o en
        // el caso elisa se seleccionó lector y el valor
        // ingresado es 0.000

        // primero obtenemos la nómina de jurisdicciones que enviaron muestras
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                         INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                     WHERE cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                           diccionarios.paises.NOMBRE = '$this->Pais'
                     GROUP BY diccionarios.provincias.NOM_PROV
                     ORDER BY diccionarios.provincias.NOM_PROV;";
        $vector = $this->Link->query($consulta);
        $row = $vector->fetchAll(PDO::FETCH_ASSOC);
        $resultado = array_change_key_case($row, CASE_LOWER);

        // recorremos el vector
        foreach ($resultado AS $registro){

            // obtenemos el registro
            extract($registro);

            // definimos la matriz
            $errores[$jurisdiccion] = array();

        }

        // obtenemos los valores incorrectos de corte
        $query = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                         COUNT(cce.chag_datos.ID) AS mal_corte
                  FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                        INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                        INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                        INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                        INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                  WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                        (cce.chag_datos.VALOR_CORTE = 'OTRO' OR (cce.chag_datos.METODO = 'Lector' AND cce.chag_datos.VALOR_CORTE = '0.000')) AND
                         cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                         diccionarios.paises.NOMBRE = '$this->Pais'
                  GROUP BY diccionarios.provincias.NOM_PROV
                  ORDER BY diccionarios.provincias.NOM_PROV;";
        $lista = $this->Link->query($query);
        $fila = $lista->fetchAll(PDO::FETCH_ASSOC);
        $registros = array_change_key_case($fila, CASE_LOWER);

        // recorremos el vector
        foreach ($registros AS $registro){

            // obtenemos el registro
            extract($registro);

            // lo agrega a la matriz e incrementa los totales
            $errores[$jurisdiccion]["Corte"] = $mal_corte;
            $totalCorte += $mal_corte;

        }

        // obtenemos los valores incorrectos de lectura pero aqui solo
        // se consideran las muestras reactivas o indeterminadas
        $sql = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                       COUNT(cce.chag_datos.ID) AS mal_lectura
                FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                      INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                      INNER JOIN cce.chag_datos ON cce.laboratorios.ID = cce.chag_datos.LABORATORIO
                                      INNER JOIN diccionarios.paises ON dicionarios.provincias.PAIS = diccionarios.paises.ID
                                      INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                      (cce.chag_datos.VALOR_LECTURA = 'OTRO' OR (cce.chag_datos.METODO = 'Lector' AND cce.chag_datos.VALOR_LECTURA = '0.000')) AND
                       cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                       (cce.chag_datos.DETERMINACION = 'Reactivo' OR cce.chag_datos.DETERMINACION = 'Indeterminada') AND
                       diccionarios.paises.NOMBRE = '$this->Pais'
                GROUP BY diccionarios.provincias.NOM_PROV
                ORDER BY diccionarios.provincias.NOM_PROV;";
        $resultset = $this->Link->query($sql);
        $solucion = $resultset->fetchAll(PDO::FETCH_ASSOC);
        $nomina = array_change_key_case($solucion, CASE_LOWER);

        // recorremos el vector
        foreach ($nomina AS $registro){

            // obtenemos el registro
            extract($registro);

            // agrega el valor e incrementa el contador
            $errores[$jurisdiccion]["Lectura"] = $mal_lectura;
            $totalLectura += $mal_lectura;

        }

        // agrega la última fila
        $errores["Total"] = array();
        $errores["Total"]["Corte"] = $totalCorte;
        $errores["Total"]["Lectura"] = $totalLectura;

        // si por casualidad no hubo errores
        if ($totalCorte == 0 && $totalLectura == 0){

            // retornamos nulo
            return null;

        // si encontró errores
        } else {

            // retornamos el vector
            return $errores;

        }

    }

    /**
     * @param anio string con el año a reportar
     * @return matriz con la distribucion de falsos negativos
     * Metodo que recibe como parametro el año a reportar y retorna una
     * matriz con la distribucion por provincia de los falsos negativos
     */
    public function falsosNegativos($anio){

        /*

          Dimensionamos la matriz, la estructura sera
          0. Nombre de la jurisdiccion
          1. Numero de determinaciones AP
          2. Numero de determinaciones falsas AP
          3. Porcentaje de acierto AP
          4. Numero de determinaciones ELISA
          5. Numero de determinaciones falsas ELISA
          6. Porcentaje de acierto ELISA
          7. Numero de determinaciones HAI
          8. Numero de determinaciones falsas HAI
          9. Porcentaje de acierto HAI
         10. Numero de determinaciones IFI
         11. Numero de determinaciones falsas IFI
         12. Porcentaje de acierto IFI
         13. Numero de determinaciones Quimioluminiscencia
         14. Numero de determinaciones falsas Quimiluminiscencia
         15. Porcentaje de acierto de determinaciones Quimioluminiscencia

        */

        // esta consulta me retorna el numero de determinaciones realizadas
        // sobre muestras negativas clasificadas por jurisdiccion y
        // tecnica
        $consulta = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                            cce.tecnicas.TECNICA AS tecnica,
                            COUNT(cce.chag_datos.ID) AS determinaciones
                     FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                         INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                         INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                         INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                         INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                         INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                     WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                           diccionarios.paises.NOMBRE = '$this->Pais' AND
                           cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                           (cce.chag_datos.CORRECTO = 'Reactivo' OR cce.chag_datos.CORRECTO = 'Indeterminado')
                     GROUP BY diccionarios.provincias.NOM_PROV,
                              cce.tecnicas.TECNICA
                     ORDER BY diccionarios.provincias.NOM_PROV,
                              cce.tecnicas.TECNICA;";
        $resultset = $this->Link->query($consulta);
        $resultado = $resultset->fetchAll(PDO::FETCH_ASSOC);
        $nroDeterminaciones = array_change_key_case($resultado, CASE_LOWER);

        // recorremos el vector
        foreach ($nroDeterminaciones AS $registro){

            // obtenemos el registro
            extract($registro);

            // si cambio la jurisdiccion
            if ($jurisdiccionAnterior != $jurisdiccion){

                // inicializamos el array
                $noConcordantes[$jurisdiccion] = array();

                // asignamos el nuevo valor
                $jurisdiccionAnterior = $jurisdiccion;

            }

            // segun la tecnica
            switch ($tecnica){

                // si es ap
                case "AP":
                    $noConcordantes[$jurisdiccion]["AP"] = $determinaciones;
                    $determinacionesAp += $determinaciones;
                    break;

                // si es elisa
                case "ELISA":
                    $noConcordantes[$jurisdiccion]["ELISA"] = $determinaciones;
                    $determinacionesElisa += $determinaciones;
                    break;

                // si es hai
                case "HAI":
                    $noConcordantes[$jurisdiccion]["HAI"] = $determinaciones;
                    $determinacionesHai += $determinaciones;
                    break;

                // si es ifi
                case "IFI":
                    $noConcordantes[$jurisdiccion]["IFI"] = $determinaciones;
                    $determinacionesIfi += $determinaciones;
                    break;

                // si es quimio
                default:
                    $noConcordantes[$jurisdiccion]["QUIMIO"] = $determinaciones;
                    $determinacionesQuimio += $determinaciones;
                    break;

            }

        }

        // esta consulta me retorna el numero de falsos negativos clasificados
        // por jurisdiccion y tecnica
        $query = "SELECT diccionarios.provincias.NOM_PROV AS jurisdiccion,
                         cce.tecnicas.TECNICA AS tecnica,
                         COUNT(cce.chag_datos.ID) AS determinaciones
                  FROM cce.chag_datos INNER JOIN cce.laboratorios ON cce.chag_datos.LABORATORIO = cce.laboratorios.ID
                                      INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                      INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                      INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                                      INNER JOIN cce.operativos_chagas ON cce.chag_datos.OPERATIVO = cce.operativos_chagas.ID
                                      INNER JOIN cce.tecnicas ON cce.chag_datos.TECNICA = cce.tecnicas.ID
                  WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                        diccionarios.paises.NOMBRE = '$this->Pais' AND
                        cce.operativos_chagas.OPERATIVO_NRO LIKE '%$anio' AND
                        (cce.chag_datos.CORRECTO = 'Reactivo' OR cce.chag_datos.CORRECTO = 'Indeterminado') AND
                        cce.chag_datos.DETERMINACION = 'No Reactivo'
                  GROUP BY diccionarios.provincias.NOM_PROV,
                           cce.tecnicas.TECNICA
                  ORDER BY diccionarios.provincias.NOM_PROV,
                           cce.tecnicas.TECNICA;";
        $sql = $this->Link->query($query);
        $vector = $sql->fetchAll(PDO::FETCH_ASSOC);
        $nroFalsos = array_change_key_case($vector, CASE_LOWER);

        // recorremos el vector
        foreach ($nroFalsos AS $registro){

            // obtenemos el registro
            extract($registro);

            // segun la tecnica
            switch ($tecnica){

                // si es ap
                case "AP":
                    $noConcordantes[$jurisdiccion]["FalsosAP"] = $determinaciones;
                    $erroresAp += $determinaciones;
                    break;

                // si es elisa
                case "ELISA":
                    $noConcordantes[$jurisdiccion]["FalsosELISA"] = $determinaciones;
                    $erroresElisa += $determinaciones;
                    break;

                // si es hai
                case "HAI":
                    $noConcordantes[$jurisdiccion]["FalsosHAI"] = $determinaciones;
                    $erroresHai += $determinaciones;
                    break;

                // si es ifi
                case "IFI":
                    $noConcordantes[$jurisdiccion]["FalsosIFI"] = $determinaciones;
                    $erroresIfi += $determinaciones;
                    break;

                // si es quimio
                case "QUIMIOLUMINISCENCIA":
                    $noConcordantes[$jurisdiccion]["FalsosQUIMIO"] = $determinaciones;
                    $erroresQuimio += $determinaciones;
                    break;

            }

        }

        // ahora agregamos la ultima fila
        $noConcordantes["Total"] = array();

        // calculamos los porcentajes totales y los asignamos verificando
        // que no se encuentren nulos

        // para ap
        if ($determinacionesAp != 0 && $erroresAp != 0){
            $numeroConFormato = $this->Funciones->Porcentaje($determinacionesAp, $erroresAp);
            $noConcordantes["Total"]["EficienciaAP"] = $numeroConFormato;
        }

        // para elisa
        if ($determinacionesElisa != 0 && $erroresElisa != 0){
            $numeroConFormato= $this->Funciones->Porcentaje($determinacionesElisa, $erroresElisa);
            $noConcordantes["Total"]["EficienciaELISA"] = $numeroConFormato;
        }

        // para hai
        if ($determinacionesHai != 0 && $erroresHai != 0){
            $numeroConFormato= $this->Funciones->Porcentaje($determinacionesHai, $erroresHai);
            $noConcordantes["Total"]["EficienciaHAI"] = $numeroConFormato;
        }

        // para ifi
        if ($determinacionesIfi != 0 && $erroresIfi != 0){
            $numeroConFormato= $this->Funciones->Porcentaje($determinacionesIfi, $erroresIfi);
            $noConcordantes["Total"]["EficienciaIFI"] = $numeroConFormato;
        }

        // para quimio
        if ($determinacionesQuimio != 0 && $erroresQuimio != 0){
            $numeroConFormato= $this->Funciones->Porcentaje($determinacionesOtras, $erroresOtras);
            $noConcordantes["Total"]["EficienciaQUIMIO"] = $numeroConFormato;
        }

        // retornamos el vector
        return $noConcordantes;

    }

}
