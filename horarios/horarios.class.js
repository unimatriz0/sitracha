/*
 * Nombre: horarios.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 17/07/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones del formulario de
 *              horarios de atención de los profesionales
 *
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de la tabla de horarios
 */
class Horarios {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initHorarios();

        // definimos el layer
        this.layerHorarios = "";

    }

    // método que inicializa las variables
    initHorarios(){

        // inicializamos las variables
        this.IdProfesional = 0;        // clave del profesional
        this.InicioLunes = "";         // fecha de inicio
        this.FinLunes = "";            // fecha de finalización
        this.InicioMartes = "";        // fecha de inicio
        this.FinMartes = "";           // fecha de finalización
        this.InicioMiercoles = "";     // fecha de inicio
        this.FinMiercoles = "";        // fecha de finalización
        this.InicioJueves = "";        // fecha de inicio
        this.FinJueves = "";           // fecha de finalización
        this.InicioViernes = "";       // fecha de inicio
        this.FinViernes = "";          // fecha de finalización
        this.Usuario = "";             // nombre del usuario
        this.Fecha = "";               // fecha de alta

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente y carga el formulario
     * con la nómina de profesionales
     */
    cargaHorarios(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // abrimos y mostramos el layer
        this.layerHorarios = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Horarios',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:500,
                    draggable: 'title',
                    ajax: {
                        url: 'horarios/horarios.html',
                        reload: 'strict'
                    }
                });
        this.layerHorarios.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del select de profesionales
     * obtiene la clave del profesional y llama la rutina php
     * para cargar los resultados en el cuerpo de la tabla
     */
    Actualizar(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la clave del profesional
        this.IdProfesional = document.getElementById("lista_profesionales").value;

        // cargamos los horarios asignados
        $("#cuerpo_horarios").load("horarios/grilla_horarios.php?profesional=" + this.IdProfesional);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar puede haber
     * días en blanco, verifica que si se ingresó un
     * horario de inicio o de finalización, el otro esté
     * también declarado
     */
    validaHorarios(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no seleccionó profesional
        if (this.IdProfesional == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar un profesional de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica los horarios del lunes
        if (document.getElementById("inicio_lunes").value > document.getElementById("fin_lunes").value){

            // presenta el mensaje y retorna
            mensaje = "El horario de inicio del Lunes no puede<br>";
            mensaje += "ser posterior al horario de finalización.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica los horarios del martes
        if (document.getElementById("inicio_martes").value > document.getElementById("fin_martes").value){

            // presenta el mensaje y retorna
            mensaje = "El horario de inicio del Martes no puede<br>";
            mensaje += "ser posterior al horario de finalización.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica los horarios del miércoles
        if (document.getElementById("inicio_miercoles").value > document.getElementById("fin_miercoles").value){

            // presenta el mensaje y retorna
            mensaje = "El horario de inicio del Miercoles no puede<br>";
            mensaje += "ser posterior al horario de finalización.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica los horarios del jueves
        if (document.getElementById("inicio_jueves").value > document.getElementById("fin_jueves").value){

            // presenta el mensaje y retorna
            mensaje = "El horario de inicio del Jueves no puede<br>";
            mensaje += "ser posterior al horario de finalización.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica los horarios del viernes
        if (document.getElementById("inicio_viernes").value > document.getElementById("fin_viernes").value){

            // presenta el mensaje y retorna
            mensaje = "El horario de inicio del Viernes no puede<br>";
            mensaje += "ser posterior al horario de finalización.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // grabamos el registro
        this.grabaHorarios();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario que
     * envía los datos al servidor
     */
    grabaHorarios(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosHorarios = new FormData();

        // asignamos los valores en la clase
        this.InicioLunes = document.getElementById("inicio_lunes").value;
        this.FinLunes = document.getElementById("fin_lunes").value;
        this.InicioMartes = document.getElementById("inicio_martes").value;
        this.FinMartes = document.getElementById("fin_martes").value;
        this.InicioMiercoles = document.getElementById("inicio_miercoles").value;
        this.FinMiercoles = document.getElementById("fin_miercoles").value;
        this.InicioJueves = document.getElementById("inicio_jueves").value;
        this.FinJueves = document.getElementById("fin_jueves").value;
        this.InicioViernes = document.getElementById("inicio_viernes").value;
        this.FinViernes = document.getElementById("fin_viernes").value;

        // asignamos en el formulario
        datosHorarios.append("Profesional", this.IdProfesional);
        datosHorarios.append("InicioLunes", this.InicioLunes);
        datosHorarios.append("FinLunes", this.FinLunes);
        datosHorarios.append("InicioMartes", this.InicioMartes);
        datosHorarios.append("FinMartes", this.FinMartes);
        datosHorarios.append("InicioMiercoles", this.InicioMiercoles);
        datosHorarios.append("FinMiercoles", this.FinMiercoles);
        datosHorarios.append("InicioJueves", this.InicioJueves);
        datosHorarios.append("FinJueves", this.FinJueves);
        datosHorarios.append("InicioViernes", this.InicioViernes);
        datosHorarios.append("FinViernes", this.FinViernes);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "horarios/grabahorario.php",
            type: "POST",
            data: datosHorarios,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

}