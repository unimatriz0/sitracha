<?php

/**
 *
 * grabahorario | horarios/grabahorario.php
 *
 * @package     Diagnostico
 * @subpackage  Horarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por post los datos de un registro y ejecuta
 * la consulta en el servidor
*/

// incluimos e instanciamos la clase
require_once("horarios.class.php");
$horarios = new Horarios();

// asignamos los datos
$horarios->setIdProfesional($_POST["Profesional"]);
$horarios->setInicioLunes($_POST["InicioLunes"]);
$horarios->setFinLunes($_POST["FinLunes"]);
$horarios->setInicioMartes($_POST["InicioMartes"]);
$horarios->setFinMartes($_POST["FinMartes"]);
$horarios->setInicioMiercoles($_POST["InicioMiercoles"]);
$horarios->setFinMiercoles($_POST["FinMiercoles"]);
$horarios->setInicioJueves($_POST["InicioJueves"]);
$horarios->setFinJueves($_POST["FinJueves"]);
$horarios->setInicioViernes($_POST["InicioViernes"]);
$horarios->setFinViernes($_POST["FinViernes"]);

// grabamos el registro
$id = $horarios->grabaHorario();

// retornamos el resultado
echo json_encode(array("Id", $id));

?>