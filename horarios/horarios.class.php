<?php

/**
 *
 * Class Horarios | horarios/horarios.class.php
 *
 * @package     Diagnostico
 * @subpackage  Horarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de
 * horarios de atención de los profesionales
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Horarios{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                 // puntero a la base de datos
    protected $Id;                   // clave del registro
    protected $Profesional;          // nombre usuario del profesional
    protected $Nombre;               // nombre del profesional
    protected $IdProfesional;        // clave del profesional
    protected $InicioLunes;          // horario de inicio
    protected $FinLunes;             // fin de la atención
    protected $InicioMartes;         // inicio de la atención
    protected $FinMartes;            // fin de la atención
    protected $InicioMiercoles;      // inicio de la atención
    protected $FinMiercoles;         // fin de la atención
    protected $InicioJueves;         // inicio de la atención
    protected $FinJueves;            // fin de la atención
    protected $InicioViernes;        // inicio de la atención
    protected $FinViernes;           // fin de la atención
    protected $Usuario;              // nombre del usuario
    protected $IdUsuario;            // clave del usuario actual
    protected $FechaAlta;            // fecha de alta del registro

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->initHorarios();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Método que inicializa las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function initHorarios(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Profesional = "";
        $this->IdProfesional = 0;
        $this->Nombre = "";
        $this->InicioLunes = "";
        $this->FinLunes = "";
        $this->InicioMartes = "";
        $this->FinMartes = "";
        $this->InicioMiercoles = "";
        $this->FinMiercoles = "";
        $this->InicioJueves = "";
        $this->FinJueves = "";
        $this->InicioViernes = "";
        $this->FinViernes = "";
        $this->FechaAlta = "";
        $this->Usuario = "";

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setIdProfesional($idprofesional){
        $this->IdProfesional = $idprofesional;
    }
    public function setInicioLunes($inicio){
        $this->InicioLunes = $inicio;
    }
    public function setFinLunes($fin){
        $this->FinLunes = $fin;
    }
    public function setInicioMartes($inicio){
        $this->InicioMartes = $inicio;
    }
    public function setFinMartes($fin){
        $this->FinMartes = $fin;
    }
    public function setInicioMiercoles($inicio){
        $this->InicioMiercoles = $inicio;
    }
    public function setFinMiercoles($fin){
        $this->FinMiercoles = $fin;
    }
    public function setInicioJueves($inicio){
        $this->InicioJueves = $inicio;
    }
    public function setFinJueves($fin){
        $this->FinJueves = $fin;
    }
    public function setInicioViernes($inicio){
        $this->InicioViernes = $inicio;
    }
    public function setFinViernes($fin){
        $this->FinViernes = $fin;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getProfesional(){
        return $this->Profesional;
    }
    public function getIdProfesional(){
        return $this->IdProfesional;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getInicioLunes(){
        return $this->InicioLunes;
    }
    public function getFinLunes(){
        return $this->FinLunes;
    }
    public function getInicioMartes(){
        return $this->InicioMartes;
    }
    public function getFinMartes(){
        return $this->FinMartes;
    }
    public function getInicioMiercoles(){
        return $this->InicioMiercoles;
    }
    public function getFinMiercoles(){
        return $this->FinMiercoles;
    }
    public function getInicioJueves(){
        return $this->InicioJueves;
    }
    public function getFinJueves(){
        return $this->FinJueves;
    }
    public function getInicioViernes(){
        return $this->InicioViernes;
    }
    public function getFinViernes(){
        return $this->FinViernes;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getUsuario(){
        return $this->Usuario;
    }

    /**
     *
     * Método que recibe como parámetro la id de un profesional
     * y asigna en la clase los valores del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idprofesional - clave del profesional
     *
     */
    public function getHorarios($idprofesional){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_horarios.id AS id,
                            diagnostico.v_horarios.idprofesional AS idprofesional,
                            diagnostico.v_horarios.profesional AS profesional,
                            diagnostico.v_horarios.nombre AS nombre,
                            diagnostico.v_horarios.inicio_lunes AS iniciolunes,
                            diagnostico.v_horarios.fin_lunes AS finlunes,
                            diagnostico.v_horarios.inicio_martes AS iniciomartes,
                            diagnostico.v_horarios.fin_martes AS finmartes,
                            diagnostico.v_horarios.inicio_miercoles AS iniciomiercoles,
                            diagnostico.v_horarios.fin_miercoles AS finmiercoles,
                            diagnostico.v_horarios.inicio_jueves AS iniciojueves,
                            diagnostico.v_horarios.fin_jueves AS finjueves,
                            diagnostico.v_horarios.inicio_viernes AS inicioviernes,
                            diagnostico.v_horarios.fin_viernes AS finviernes,
                            diagnostico.v_horarios.usuario AS usuario,
                            diagnostico.v_horarios.fecha_alta AS fecha
                     FROM diagnostico.v_horarios
                     WHERE diagnostico.v_horarios.idprofesional = '$idprofesional'; ";
        $resultado = $this->Link->query($consulta);

        // si hay registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro y lo asignamos en la clase
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Profesional = $profesional;
            $this->IdProfesional = $idprofesional;
            $this->Nombre = $nombre;
            $this->InicioLunes = $iniciolunes;
            $this->FinLunes = $finlunes;
            $this->InicioMartes = $iniciomartes;
            $this->FinMartes = $finmartes;
            $this->InicioMiercoles = $iniciomiercoles;
            $this->FinMiercoles = $finmiercoles;
            $this->InicioJueves = $iniciojueves;
            $this->FinJueves = $finjueves;
            $this->InicioViernes = $inicioviernes;
            $this->FinViernes = $finviernes;
            $this->FechaAlta = $fecha;
            $this->Usuario = $usuario;

        // si no hay registros
        } else {

            // inicializamos las variables
            $this->initHorarios();

        }

    }

    /**
     * Método que segun corresponda ejecuta la consulta
     * de actualización o inserción, retorna la clave
     * del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $id - clave del registro
     */
    public function grabaHorario(){

        // obtenemos la clave del registro
        $this->Id = $this->hallaClave();

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoHorario();
        } else {
            $this->editaHorario();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoHorario(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.horarios
                            (profesional,
                             inicio_lunes,
                             fin_lunes,
                             inicio_martes,
                             fin_martes,
                             inicio_miercoles,
                             fin_miercoles,
                             inicio_jueves,
                             fin_jueves,
                             inicio_viernes,
                             fin_viernes,
                             usuario)
                            VALUES
                            (:profesional,
                             :inicio_lunes,
                             :fin_lunes,
                             :inicio_martes,
                             :fin_martes,
                             :inicio_miercoles,
                             :fin_miercoles,
                             :inicio_jueves,
                             :fin_jueves,
                             :inicio_viernes,
                             :fin_viernes,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":profesional",      $this->IdProfesional);
        $psInsertar->bindParam(":inicio_lunes",     $this->InicioLunes);
        $psInsertar->bindParam(":fin_lunes",        $this->FinLunes);
        $psInsertar->bindParam(":inicio_martes",    $this->InicioMartes);
        $psInsertar->bindParam(":fin_martes",       $this->FinMartes);
        $psInsertar->bindParam(":inicio_miercoles", $this->InicioMiercoles);
        $psInsertar->bindParam(":fin_miercoles",    $this->FinMiercoles);
        $psInsertar->bindParam(":inicio_jueves",    $this->InicioJueves);
        $psInsertar->bindParam(":fin_jueves",       $this->FinJueves);
        $psInsertar->bindParam(":inicio_viernes",   $this->InicioViernes);
        $psInsertar->bindParam(":fin_viernes",      $this->FinViernes);
        $psInsertar->bindParam(":usuario",          $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaHorario(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.horarios SET
                            inicio_lunes = :inicio_lunes,
                            fin_lunes = :fin_lunes,
                            inicio_martes = :inicio_martes,
                            fin_martes = :fin_martes,
                            inicio_miercoles = :inicio_miercoles,
                            fin_miercoles = :fin_miercoles,
                            inicio_jueves = :inicio_jueves,
                            fin_jueves = :fin_jueves,
                            inicio_viernes = :inicio_viernes,
                            fin_viernes = :fin_viernes,
                            usuario = :usuario
                     WHERE diagnostico.horarios.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":inicio_lunes",     $this->InicioLunes);
        $psInsertar->bindParam(":fin_lunes",        $this->FinLunes);
        $psInsertar->bindParam(":inicio_martes",    $this->InicioMartes);
        $psInsertar->bindParam(":fin_martes",       $this->FinMartes);
        $psInsertar->bindParam(":inicio_miercoles", $this->InicioMiercoles);
        $psInsertar->bindParam(":fin_miercoles",    $this->FinMiercoles);
        $psInsertar->bindParam(":inicio_jueves",    $this->InicioJueves);
        $psInsertar->bindParam(":fin_jueves",       $this->FinJueves);
        $psInsertar->bindParam(":inicio_viernes",   $this->InicioViernes);
        $psInsertar->bindParam(":fin_viernes",      $this->FinViernes);
        $psInsertar->bindParam(":usuario",          $this->IdUsuario);
        $psInsertar->bindParam(":id",               $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que a partir de la clave del profesional encuentra la
     * clave del registro (o 0 en caso de no tener asignado un horario)
     * lo buscamos aquí porque es mas sencillo que obtener la clave
     * a través de javascript
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $id - entero con la clave
     */
    protected function hallaClave(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT diagnostico.horarios.id AS id
                     FROM diagnostico.horarios
                     WHERE diagnostico.horarios.profesional = '$this->IdProfesional';";
        $resultado = $this->Link->query($consulta);

        // si no hubo registros
        if ($resultado->rowCount() == 0){

            // retornamos
            return 0;

        // si lo encontró
        } else {

            // obtenemos el registro y retornamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            return $id;

        }

    }

}