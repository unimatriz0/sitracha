<?php

/**
 *
 * grilla_horarios | horarios/grilla_horarios.php
 *
 * @package     Diagnostico
 * @subpackage  Horarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por get la clave de un profesional, obtiene la
 * nómina de horarios de ese profesional y los presenta
*/

// incluimos e instanciamos la clase
require_once("horarios.class.php");
$horarios = new Horarios();

// obtenemos el registro
$horarios->getHorarios($_GET["profesional"]);

// presenta el lunes
echo "<tr>";
echo "<td>Lunes</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Inicio del Horario de Atención'>";
echo "<input type='text'
             name='inicio_lunes'
             id='inicio_lunes'
             value = '" . $horarios->getInicioLunes() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fin del Horario de Atención'>";
echo "<input type='text'
             name='fin_lunes'
             id='fin_lunes'
             value = '" . $horarios->getFinLunes() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>" . $horarios->getUsuario() . "</td>";
echo "<td align='center'align='center'>" . $horarios->getFechaAlta() . "</td>";
echo "<tr>";

// presenta el martes
echo "<tr>";
echo "<td>Martes</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Inicio del Horario de Atención'>";
echo "<input type='text'
             name='inicio_martes'
             id='inicio_martes'
             value = '" . $horarios->getInicioMartes() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fin del Horario de Atención'>";
echo "<input type='text'
             name='fin_martes'
             id='fin_martes'
             value = '" . $horarios->getFinMartes() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>" . $horarios->getUsuario() . "</td>";
echo "<td align='center'>" . $horarios->getFechaAlta() . "</td>";
echo "<tr>";

// presenta el miércoles
echo "<tr>";
echo "<td>Miércoles</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Inicio del Horario de Atención'>";
echo "<input type='text'
             name='inicio_miercoles'
             id='inicio_miercoles'
             value = '" . $horarios->getInicioMiercoles() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fin del Horario de Atención'>";
echo "<input type='text'
             name='fin_miercoles'
             id='fin_miercoles'
             value = '" . $horarios->getFinMiercoles() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>" . $horarios->getUsuario() . "</td>";
echo "<td align='center'>" . $horarios->getFechaAlta() . "</td>";
echo "<tr>";

// presenta el jueves
echo "<tr>";
echo "<td>Jueves</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Inicio del Horario de Atención'>";
echo "<input type='text'
             name='inicio_jueves'
             id='inicio_jueves'
             value = '" . $horarios->getInicioJueves() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fin del Horario de Atención'>";
echo "<input type='text'
             name='fin_jueves'
             id='fin_jueves'
             value = '" . $horarios->getFinJueves() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>" . $horarios->getUsuario() . "</td>";
echo "<td align='center'>" . $horarios->getFechaAlta() . "</td>";
echo "<tr>";

// presenta el viernes
echo "<tr>";
echo "<td>Viernes</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Inicio del Horario de Atención'>";
echo "<input type='text'
             name='inicio_viernes'
             id='inicio_viernes'
             value = '" . $horarios->getInicioViernes() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fin del Horario de Atención'>";
echo "<input type='text'
             name='fin_viernes'
             id='fin_viernes'
             value = '" . $horarios->getFinViernes() . "'
             size='8'>";
echo "</span>";
echo "</td>";
echo "<td align='center'>" . $horarios->getUsuario() . "</td>";
echo "<td align='center'>" . $horarios->getFechaAlta() . "</td>";
echo "<tr>";

// insertamos un separador
echo "<tr><td colspan='5'><br></td></tr>";

// agrega la fila y el botón grabar
echo "<tr>";
echo "<td colspan='5' align='center'>";
echo "<input type='button'
             name='btnGrabaHorario'
             id='btnGrabaHorario'
             class='boton_grabar'
             value='Grabar'
             title='Graba los horarios en la base'
             onClick='horarios.validaHorarios()'>";
echo "<td>";
echo "<tr>";

?>
<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

    // definimos los timepickers
    $('#inicio_lunes').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#fin_lunes').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#inicio_martes').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#fin_martes').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#inicio_miercoles').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#fin_miercoles').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#inicio_jueves').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#fin_jueves').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#inicio_viernes').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });
    $('#fin_viernes').timepicker({
    	'disableTimeRanges': [
	    	['12am', '8am'],
    		['6pm', '12pm']
	    ],
        'timeFormat': 'H:i',
        'step': 15
    });

</script>

<?
echo $culo;
?>