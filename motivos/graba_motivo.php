<?php

/**
 *
 * motivos/graba_motivo.php
 *
 * @package     Diagnostico
 * @subpackage  Motivos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de un motivo de consulta y
 * ejecuta la consulta en el servidor
 *
*/

// incluimos e instanciamos las clases
require_once("motivos.class.php");
$motivo = new Motivos();

// si está editando
if (!empty($_POST["Id"])){
    $motivo->setIdMotivo($_POST["Id"]);
}

// fija el nombre
$motivo->setMotivo($_POST["Motivo"]);

// grabamos y obtenemos el resultado
$error = $motivo->grabaMotivo();

// retornamos el resultado
echo json_encode(array("Error" => $error));

?>