<?php

/**
 *
 * motivos/lista_motivos.php
 *
 * @package     Diagnostico
 * @subpackage  Motivos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (03/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de motivos de consulta
*/

// incluimos e instanciamos la clase
require_once("motivos.class.php");
$motivos = new Motivos();

// obtenemos la nómina
$nomina = $motivos->nominaMotivos();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_motivo,
                        "Motivo" => $motivo);

}

// retornamos el vector
echo json_encode($jsondata);

?>