/*
 * Nombre: motivos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 02/03/2018
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de motivos de
 *              consulta
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el abm de motivos de consulta
 * Definición de la clase
 */
class Motivos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     * Constructor de la clase
     */
    constructor(){

        // inicializa las variables de clase
        this.initMotivos();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initMotivos(){

        // declaración de variables
        this.IdMotivo = 0;
        this.Motivo = "";
        this.IdUsuario = 0;
        this.Usuario = "";
        this.FechaAlta = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de motivos
     */
    muestraMotivos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("motivos/form_motivos.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idmotivo clave del motivo a verificar
     * Método que recibe como parámetro la clave de un motivo de consulta
     * verifica el formulario y lo graba, si no recibe la clave asume
     * que se trata de un alta
     */
    verificaMotivo(idmotivo){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió la id
        if (typeof(idmotivo) == "undefined"){
            idmotivo = "nuevo";
        } else {
            this.IdMotivo = idmotivo;
        }

        // verifica que halla ingresado un motivo
        if (document.getElementById("motivo_" + idmotivo).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el motivo de consulta";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("motivo_" + idmotivo).focus();
            return false;

        }

        // asignamos el elemento y grabamos el registro
        this.Motivo = document.getElementById("motivo_" + idmotivo).value;
        this.grabaMotivo();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta en el servidor
     */
    grabaMotivo(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosMotivo = new FormData();

        // si está editando
        if (this.IdMotivo != 0){
            datosMotivo.append("Id", this.IdMotivo);
        }

        // agregamos el motivo
        datosMotivo.append("Motivo", this.Motivo);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "motivos/graba_motivo.php",
            type: "POST",
            data: datosMotivo,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    motivos.initMotivos();

                    // recarga el formulario para reflejar los cambios
                    motivos.muestraMotivos();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un elemento html
     * @param {int} idmotivo clave del motivo preseleccionado
     * Método que recibe como parámetro la id de un elemento del formulario
     * carga en ese elemento la nómina de motivos de consulta, si además
     * recibe la clave de un motivo, lo predetermina
     */
    nominaMotivos(idelemento, idmotivo){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "motivos/lista_motivos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idmotivo) != "undefined"){

                        // si coincide
                        if (data[i].Id == idmotivo){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Motivo + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Motivo + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Motivo + "</option>");
                    }

                }

        }});

    }

}