<?php

/**
 *
 * Class Motivos | motivos/motivos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Motivos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla el diccionario de motivos de consulta
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Motivos{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Link;                  // puntero a la base de datos
    protected $IdMotivo;              // clave del registro
    protected $Motivo;                // descripción del motivo
    protected $Usuario;               // nombre del usuario
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdMotivo = 0;
        $this->Motivo = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdMotivo($idmotivo){
        $this->IdMotivo = $idmotivo;
    }
    public function setMotivo($motivo){
        $this->Motivo = $motivo;
    }

    // métodos de retorno de valores
    public function getIdMotivo(){
        return $this->IdMotivo;
    }
    public function getMotivo(){
        return $this->Motivo;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que retorna un array asociativo con la nómina
     * completa de motivos de consulta y sus claves
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaMotivos(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.motivos.id_motivo AS id_motivo,
                            diagnostico.motivos.motivo AS motivo,
                            diagnostico.motivos.id_usuario AS id_usuario,
                            cce.responsables.usuario AS usuario,
                            DATE_FORMAT(diagnostico.motivos.fecha, '%d/%m/%Y') AS fecha_alta
                     FROM diagnostico.motivos INNER JOIN cce.responsables ON diagnostico.motivos.id_usuario = cce.responsables.id
                     ORDER BY diagnostico.motivos.motivo;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que ejecuta la consulta de inserción o edición y
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idmotivo - clave del registro insertado / editado
     */
    public function grabaMotivo(){

        // si está insertando
        if ($this->IdMotivo == 0){
            $this->nuevoMotivo();
        } else {
            $this->editaMotivo();
        }

        // retornamos la id del registro
        return $this->IdMotivo;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoMotivo(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.motivos
                            (motivo,
                             id_usuario)
                            VALUES
                            (:motivo,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":motivo", $this->Motivo);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->IdMotivo = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaMotivo(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.motivos SET
                            motivo = :motivo,
                            id_usuario = :id_usuario
                     WHERE diagnostico.motivos.id_motivo = :id_motivo;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":motivo", $this->Motivo);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_motivo", $this->IdMotivo);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}