<?php

/**
 *
 * motivos/form_motivos.php
 *
 * @package     Diagnostico
 * @subpackage  Motivos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario de edición de motivos de consulta
 *
*/

// incluimos e instanciamos la clase
require_once("motivos.class.php");
$motivos = new Motivos();

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["ID"])){
    $usuario_actual = $_SESSION["Usuario"];
}
session_write_close();

// presentamos el título
echo "<h2>Nómina de Motivos de Consulta</h2>";

// definimos la tabla
echo "<table width='60%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Motivo</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la matriz de documentos
$nomina = $motivos->nominaMotivos();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el nombre de la compañia
    echo "<td>";
    echo "<span class='tooltip'
                title='Motivo de consulta'>";
    echo "<input type='text'
                 name='motivo_$id_motivo'
                 id='motivo_$id_motivo'
                 value='$motivo'
                 size='30'>";
    echo "</span>";
    echo "</td>";

    // presenta el usuario y la fecha de alta
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // arma el enlace
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaMotivo'
           id='btnGrabaMotivo'
           title='Pulse para grabar el registro'
           onClick='motivos.verificaMotivo($id_motivo)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// ahora agregamos la fila de altas
echo "<tr>";

// presentamos el tipo de documento
echo "<td>";
echo "<span class='tooltip'
            title='Motivo de consulta'>";
echo "<input type='text'
                name='motivo_nuevo'
                id='motivo_nuevo'
                size='30'>";
echo "</span>";
echo "</td>";

// obtenemos el usuario actual y la fecha de alta
$fecha_alta = date('d/m/Y');

// presenta el usuario y la fecha de alta
echo "<td align='center'>$usuario_actual</td>";
echo "<td align='center'>$fecha_alta</td>";

// arma el enlace
echo "<td align='center'>";
echo "<input type='button'
        name='btnGrabaMotivo'
        id='btnGrabaMotivo'
        title='Ingresa un nuevo registro'
        onClick='motivos.verificaMotivo()'
        class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>