<?php

/**
 *
 * Class Sintomas | sintomas/sintomas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Sintomas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * sintomas de cada visita del paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Sintomas{

    // declaración de variables
    protected $Link;                 // puntero a la base de datos
    protected $Id;                   // clave del registro
    protected $Paciente;             // clave del paciente
    protected $Visita;               // clave de la visita
    protected $Disnea;               // tipo de disnea
    protected $Palpitaciones;        // tipo de palpitaciones
    protected $Precordial;           // tipo de dolor
    protected $Conciencia;           // pérdida de conciencia 0 no 1 si
    protected $Presincope;           // 0 no 1 si
    protected $Edema;                // edema de m. inferiores 0 no 1 si
    protected $Usuario;              // nombre del usuario
    protected $IdUsuario;            // clave del usuario activo
    protected $Fecha;                // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // instanciamos la conexión
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->initSintomas();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que inicializa las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function initSintomas(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Visita = 0;
        $this->Disnea = "";
        $this->Palpitaciones = "";
        $this->Precordial = "";
        $this->Conciencia = 0;
        $this->Presincope = 0;
        $this->Edema = 0;
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setDisnea($disnea){
        $this->Disnea = $disnea;
    }
    public function setPalpitaciones($palpitaciones){
        $this->Palpitaciones = $palpitaciones;
    }
    public function setPrecordial($precordial){
        $this->Precordial = $precordial;
    }
    public function setConciencia($conciencia){
        $this->Conciencia = $conciencia;
    }
    public function setPresincope($presincope){
        $this->Presincope = $presincope;
    }
    public function setEdema($edema){
        $this->Edema = $edema;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getDisnea(){
        return $this->Disnea;
    }
    public function getPalpitaciones(){
        return $this->Palpitaciones;
    }
    public function getPrecordial(){
        return $this->Precordial;
    }
    public function getConciencia(){
        return $this->Conciencia;
    }
    public function getPresincope(){
        return $this->Presincope;
    }
    public function getEdema(){
        return $this->Edema;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha = "";
    }

    /**
     * Método que recibe como parámetro el protocolo de un paciente
     * y retorna un vector con el listado de síntomas por cada
     * visita
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $protocolo - clave del paciente
     */
    public function nominaSintomas($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_sintomas.fechavisita AS fecha,
                            diagnostico.v_sintomas.disnea AS disnea,
                            diagnostico.v_sintomas.palpitaciones AS palpitaciones,
                            diagnostico.v_sintomas.precordial AS precordial,
                            diagnostico.v_sintomas.conciencia AS conciencia,
                            diagnostico.v_sintomas.presincope AS presincope,
                            diagnostico.v_sintomas.edema AS edema,
                            diagnostico.v_sintomas.usuario AS usuario
                     FROM diagnostico.v_sintomas
                     WHERE diagnostico.v_sintomas.paciente = '$protocolo';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * Método que recibe como parámetro la clave de una visita y
     * asigna en las variables de clase los datos del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idvisita - clave del registro
     */
    public function getDatosSintoma($idvisita){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_sintomas.id AS id,
                            diagnostico.v_sintomas.paciente AS paciente,
                            diagnostico.v_sintomas.idvisita AS idvisita,
                            diagnostico.v_sintomas.disnea AS disnea,
                            diagnostico.v_sintomas.palpitaciones AS palpitaciones,
                            diagnostico.v_sintomas.precordial AS precordial,
                            diagnostico.v_sintomas.conciencia AS conciencia,
                            diagnostico.v_sintomas.presincope AS presincope,
                            diagnostico.v_sintomas.edema AS edema,
                            diagnostico.v_sintomas.usuario AS usuario,
                            diagnostico.v_sintomas.fecha AS fecha
                     FROM diagnostico.v_sintomas
                     WHERE diagnostico.v_sintomas.idvisita = '$idvisita'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y asignamos
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Paciente = $paciente;
        $this->Visita = $idvisita;
        $this->Disnea = $disnea;
        $this->Palpitaciones = $palpitaciones;
        $this->Precordial = $precordial;
        $this->Conciencia = $conciencia;
        $this->Presincope = $presincope;
        $this->Edema = $edema;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha;

    }

    /**
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda, retorna la clave del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $idvisita - clave del registro
     */
    public function grabaSintoma(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoSintoma();
        } else {
            $this->editaSintoma();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoSintoma(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.sintomas
                            (paciente,
                             visita,
                             disnea,
                             palpitaciones,
                             precordial,
                             conciencia,
                             presincope,
                             edema,
                             usuario)
                            VALUES
                            (:paciente,
                             :visita,
                             :disnea,
                             :palpitaciones,
                             :precordial,
                             :conciencia,
                             :presincope,
                             :edema,
                             :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":visita",        $this->Visita);
        $psInsertar->bindParam(":disnea",        $this->Disnea);
        $psInsertar->bindParam(":palpitaciones", $this->Palpitaciones);
        $psInsertar->bindParam(":precordial",    $this->Precordial);
        $psInsertar->bindParam(":conciencia",    $this->Conciencia);
        $psInsertar->bindParam(":presincope",    $this->Presincope);
        $psInsertar->bindParam(":edema",         $this->Edema);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaSintoma(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.sintomas SET
                            paciente = :paciente,
                            visita = :visita,
                            disnea = :disnea,
                            palpitaciones = :palpitaciones,
                            precordial = :precordial,
                            conciencia = :conciencia,
                            presincope = :presincope,
                            edema = :edema,
                            usuario = :usuario
                     WHERE diagnostico.visitas.id = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":visita",        $this->Visita);
        $psInsertar->bindParam(":disnea",        $this->Disnea);
        $psInsertar->bindParam(":palpitaciones", $this->Palpitaciones);
        $psInsertar->bindParam(":precordial",    $this->Precordial);
        $psInsertar->bindParam(":conciencia",    $this->Conciencia);
        $psInsertar->bindParam(":presincope",    $this->Presincope);
        $psInsertar->bindParam(":edema",         $this->Edema);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);
        $psInsertar->bindParam(":id",            $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}