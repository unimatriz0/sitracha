<?php

/**
 *
 * laboratorios/graba_laboratorio.class.php
 *
 * @package     Diagnostico
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta
 * la consulta en el servidor, retorna la id del registro 
 * afectado (o cero en caso de error)
 * 
*/

// incluye las clases y la instancia
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// si recibió la id
if (!empty($_POST["Id"])){
    $laboratorio->setIdLaboratorio($_POST["Id"]);
}

// asigna los valores
$laboratorio->setNombre($_POST["Nombre"]);
$laboratorio->setResponsable($_POST["Responsable"]);
$laboratorio->setEmail($_POST["Mail"]);
$laboratorio->setIdDependencia($_POST["Dependencia"]);
$laboratorio->setIdPais($_POST["Pais"]);
$laboratorio->setIdLocalidad($_POST["Localidad"]);
$laboratorio->setDireccion($_POST["Direccion"]);
$laboratorio->setCodigoPostal($_POST["CodigoPostal"]);

// si está activo
if ($_POST["Activo"] == "Si"){
    $laboratorio->setActivo("Si");
} else {
    $laboratorio->setActivo("No");
}

// si recibe muestras
if ($_POST["RecibeMuestras"] == "Si"){
    $laboratorio->setRecibeMuestras("Si");
} else {
    $laboratorio->setRecibeMuestras("No");
}

// si recibió un responsable de las muestras
if (!empty($_POST["ResponsableMuestras"])){
    $laboratorio->setIdRecibe($_POST["ResponsableMuestras"]);
}

// si recibió las coordenadas
if (!empty($_POST["Coordenadas"])){
    $laboratorio->setCoordenadas($_POST["Coordenadas"]);
}

// setea los comentarios
$laboratorio->setObservaciones($_POST["Comentarios"]);

// si recibió el logo
if (!empty($_POST["Logo"])){
    $laboratorio->setLogo($_POST["Logo"]);
}

// graba el registro y obtiene la id
$id = $laboratorio->grabarLaboratorio();

// retorna la id
echo json_encode(array("Id" => $id));

?>