<?php

/**
 *
 * laboratorios/verificamail.php
 *
 * @package     Diagnostico
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (13/06/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la dirección de correo
 * y verifica si se encuentra declarada en la base 
 * de datos (en cuyo caso retorna verdadero)
 * 
*/

// incluimos e instanciamos la clase
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// si recibió la id
if (!empty($_GET["id"])){

    // verificamos si no está repitiendo
    $resultado = $laboratorio->verificaMail($_GET["mail"], $_GET["id"]);

} else {

    // verificamos si se encuentra el laboratorio
    $resultado = $laboratorio->verificaMail($_GET["mail"]);

}

// retornamos el estado
echo json_encode(array("Encontrado" => $resultado));

?>