<?php

/**
 *
 * laboratorios/getlaboratorio.php
 *
 * @package     Diagnostico
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un laboratorio 
 * y retorna en un array json los datos del mismo
 * 
*/

// incluimos e instanciamos la clase
require_once ("laboratorios.class.php");
$laboratorio = new Laboratorios();

// obtenemos los datos del registro
$laboratorio->getDatosLaboratorio($_GET["id"]);

// armamos la matriz y retornamos
echo json_encode(array("ID" =>                $laboratorio->getIdLaboratorio(),
                       "Nombre" =>            $laboratorio->getNombre(),
                       "Responsable" =>       $laboratorio->getResponsable(),
                       "IdLocalidad" =>       $laboratorio->getIdLocalidad(),
                       "IdProvincia" =>       $laboratorio->getIdProvincia(),
                       "IdPais" =>            $laboratorio->getIdPais(),
                       "Direccion" =>         $laboratorio->getDireccion(),
                       "Coordenadas" =>       $laboratorio->getCoordenadas(),
                       "CodigoPostal" =>      $laboratorio->getCodigoPostal(),
                       "Dependencia" =>       $laboratorio->getIdDependencia(),
                       "Mail" =>              $laboratorio->getEmail(),
                       "FechaAlta" =>         $laboratorio->getFechaAlta(),
                       "Activo" =>            $laboratorio->getActivo(),
                       "RecibeMuestras" =>    $laboratorio->getRecibeMuestras(),
                       "IdRecibe" =>          $laboratorio->getIdRecibe(),
                       "Observaciones" =>     $laboratorio->getObservaciones(),
                       "Usuario" =>           $laboratorio->getUsuario(),
                       "Logo" =>              $laboratorio->getLogo()));

?>