/*
 * Nombre: laboratorios.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 19/12/2016
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones sobre el formulario
 *              de laboratorios
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre los laboratorios
 */
class Laboratorios {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // inicializa las variables de clase
        this.initLaboratorios();

        // el layer de los cuadros emergentes
        this.layerLaboratorio = "";

        // cargamos el formulario
        $("#form_laboratorios").load("laboratorios/form_laboratorios.html");

        // reiniciamos la sesión
        sesion.reiniciar();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initLaboratorios(){

        // inicializamos las variables
        this.IdLaboratorio = 0;
        this.NombreLaboratorio = "";
        this.Responsable = "";
        this.IdResponsable = "";
        this.Pais = "";
        this.IdPais = 0;
        this.Localidad = "";
        this.IdLocalidad = "";
        this.Jurisdiccion = "";
        this.IdJurisdiccion = "";
        this.Direccion = "";
        this.CodigoPostal = "";
        this.Coordenadas = "";
        this.Dependencia = "";
        this.IdDependencia = 0;
        this.EMail = "";
        this.FechaAlta = "";
        this.Activo = "";
        this.RecibeMuestras = "";
        this.IdRecibe = 0;
        this.Observaciones = "";
        this.Usuario = "";
        this.Logo = "";

    }

    // métodos de asignación de valores
    setIdLaboratorio(idlaboratorio){
        this.IdLaboratorio = idlaboratorio;
    }
    setNombreLaboratorio(nombre){
        this.NombreLaboratorio = nombre;
    }
    setResponsable(responsable){
        this.Responsable = responsable;
    }
    setIdResponsable(idresponsable){
        this.IdResponsable = idresponsable;
    }
    setPais(pais){
        this.Pais = pais;
    }
    setIdPais(idpais){
        this.IdPais = idpais;
    }
    setLocalidad(localidad){
        this.Localidad = localidad;
    }
    setIdLocalidad(idlocalidad){
        this.IdLocalidad = idlocalidad;
    }
    setJurisdiccion(jurisdiccion){
        this.Jurisdiccion = jurisdiccion;
    }
    setIdJurisdiccion(idjurisdiccion){
        this.IdJurisdiccion = idjurisdiccion;
    }
    setDireccion(direccion){
        this.Direccion = direccion;
    }
    setCodigoPostal(codigopostal){
        this.CodigoPostal = codigopostal;
    }
    setCoordenadas(coordenadas){
        this.Coordenadas = coordenadas;
    }
    setDependencia(dependencia){
        this.Dependencia = dependencia;
    }
    setIdDependencia(iddependencia){
        this.IdDependencia = iddependencia;
    }
    setMail(mail){
        this.EMail = mail;
    }
    setFechaAlta(fechaalta){
        this.FechaAlta = fechaalta;
    }
    setActivo(activo){
        this.Activo = activo;
    }
    setRecibeMuestras(recibemuestras){
        this.RecibeMuestras = recibemuestras;
    }
    setIdRecibe(idrecibe){
        this.IdRecibe = idrecibe;
    }
    setObservaciones(observaciones){
        this.Observaciones = observaciones;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setLogo(logo){

        // si es nulo
        if (logo != null){
            this.Logo = logo;
        } else {
           this.Logo = "imagenes/imagen_no_disponible.gif";
        }
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpais - clave del país
     * Método llamado en el constructor que carga el select de países, si
     * recibe la id de un país lo preselecciona
     */
    cargaPaises(idpais){

        // llama la clase de países
        paises.listaPaises("paislaboratorio", idpais);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpais - clave del país seleccionado
     * @param {string} idprovincia - clave de la provincia preseleccionada
     * Método llamado en el onchange del select de países o al cargar un
     * registro, si recibe la id del país carga en el select de provincias
     * las jurisdicciones, si no recibe nada, verifica el valor del
     * select de países y carga los valores correspondientes
     * Si recibe la clave de la provincia, asume que estamos presentando
     * un registro y lo selecciona
     */
    cargaProvincias(idpais, idprovincia){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió la id
        if (typeof(idpais) == "undefined"){

            // lee del formulario
            idpais = document.getElementById("paislaboratorio").value;

        }

        // llama la clase de provincias
        provincias.listaJurisdicciones("jurisdiccionlaboratorio", idpais, idprovincia);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idprovincia - clave de la provincia
     * @param {string} idlocalidad - clave de la localidad preseleccionada
     * Método llamado tanto en el onchange del select de provincias como
     * al cargar un registro, si recibe la id asume que esta presentando
     * un registro y si no la recibe, verifica el valor de provincia
     * en el formulario
     * Si recibe la id de localidad, asume que está presentando un registro
     * y lo preselecciona
     */
    cargaLocalidades(idprovincia, idlocalidad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió la clave de la provincia
        if (typeof(idprovincia) == "undefined"){

            // lee del formulario
            idprovincia = document.getElementById("jurisdiccionlaboratorio").value;

            // si fue llamado por el onchange y está en blanco
            if (idprovincia == ""){

                // simplemente retorna
                document.getElementById("jurisdiccionlaboratorio").focus();
                return false;

            }

        }

        // llama la clase de localidades
        ciudades.nominaLocalidades("localidadlaboratorio", idprovincia, idlocalidad);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga la nómina de dependencias en el laboratorio
     */
    cargaDependencias(){

        // llamamos la clase de dependencias
        dependencias.listaDependencias("dependencia");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idprovincia - clave de la provincia
     * @param {string} idresponsable - clave del responsable
     * Método llamado luego de cargar el combo de provincias que carga
     * la nómina de responsables de esa provincia
     * Si fue llamado en el evento onchange no recibe nada, si fue
     * llamado en la carga del formulario,recibe la clave de la provincia
     */
    cargaResponsables(idprovincia, idresponsable){

        // inicialización de variables
        var claveprovincia;

        // si fue llamado en el onchange
        if (typeof(idprovincia) == "undefined"){

            // obtiene el valor de la provincia
            claveprovincia = document.getElementById("jurisdiccionlaboratorio").value;

        // si lo recibió
        } else {

            // lo asigna
            claveprovincia = idprovincia;

        }

        // limpiamos el combo
        $("#responsablemuestras").html('');

        // obtenemos la nómina
        $.ajax({
            url: 'usuarios/lista_recibe.php?provincia='+claveprovincia,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#responsablemuestras").append("<option></option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave
                    if (idresponsable == data[i].Id){

                        // lo agrega seleccionado
                        $("#responsablemuestras").append("<option selected value='" + data[i].Id + "'>" + data[i].Nombre + "</option>");

                    // si no coinciden
                    } else {

                        // simplemente lo agrega
                        $("#responsablemuestras").append("<option value='" + data[i].Id + "'>" + data[i].Nombre + "</option>");

                    }

                }

            }});

        // setea el foco en el combo
        document.getElementById("localidadlaboratorio").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario e inicializa las variables de clase
     */
    nuevoLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("idlaboratorio").value = "";
        document.getElementById("nombrelaboratorio").value = "";
        document.getElementById("responsablelaboratorio").value = "";
        document.getElementById("maillaboratorio").value = "";

        // fijamos los combos
        document.getElementById("dependencia").selectedIndex = 0;
        document.getElementById("paislaboratorio").selectedIndex = 0;
        document.getElementById("jurisdiccionlaboratorio").selectedIndex = 0;
        document.getElementById("localidadlaboratorio").selectedIndex = 0;

        // continúa inicializando
        document.getElementById("direccionlaboratorio").value = "";
        document.getElementById("codigopostallaboratorio").value = "";

        // los checkbox
        document.getElementById("activolaboratorio").checked = false;
        document.getElementById("recibemuestras").checked = false;

        // el combo del responsable lo limpia
        $("#responsablemuestras").html('');
        $("#responsablemuestras").append('<option></option>');

        // la fecha de alta y el usuario
        document.getElementById("fechaalta").value = fechaActual();
        document.getElementById("usuariolaboratorio").value = sessionStorage.getItem("Usuario");

        // las observaciones y también el editor
        document.getElementById("observacioneslaboratorio").value = "";
        CKEDITOR.instances['observacioneslaboratorio'].setData();

        // ocultamos el botón mapa
        $("#mapalaboratorio").hide();

        // fijamos el logo
        document.getElementById("logo_laboratorio").src = "imagenes/imagen_no_disponible.gif";

        // inicializamos las variables
        this.initLaboratorios();

        // setea el foco en el primer elemento
        document.getElementById("nombrelaboratorio").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar, verifica los datos del
     * formulario antes de enviarlo por ajax
     */
    verificaLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("idlaboratorio").value != ""){
            this.IdLaboratorio = document.getElementById("idlaboratorio").value;
        }

        // verifica si ingresó el nombre del laboratorio
        if (document.getElementById("nombrelaboratorio").value === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombrelaboratorio").focus();
            return false;

        // si declaró
        } else {
            this.NombreLaboratorio = document.getElementById("nombrelaboratorio").value;
        }

        // si no ingresó el nombre del responsable
        if (document.getElementById("responsablelaboratorio").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique quien es el responsable del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("responsablelaboratorio").focus();
            return false;

        // si ingresó
        } else {
            this.Responsable = document.getElementById("responsablelaboratorio").value;
        }

        // si no ingresó el mail
        if (document.getElementById("maillaboratorio").value === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el mail institucional del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("maillaboratorio").focus();
            return false;

        // si cargó verifica si el mail es correcto
        } else if (!echeck(document.getElementById("maillaboratorio").value)){

            // presenta el mensaje y retorna
            mensaje = "El mail del laboratorio parece incorrecto, verifique";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("maillaboratorio").focus();
            return false;

        // verifica si el mail se encuentra repetido
        } else if (this.verificaMailLaboratorio(document.getElementById("maillaboratorio").value, document.getElementById("idlaboratorio").value)) {

            // presenta el mensaje y retorna
            mensaje = "Esa dirección de correo ya está declarada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("maillaboratorio").focus();
            return false;

        // si llegó hasta aquí
        } else {

            // lo agrega al formulario
            this.EMail = document.getElementById("maillaboratorio").value;
        }

        // si no indicó la dependencia
        if (document.getElementById("dependencia").selectedIndex == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la dependencia del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("dependencia").focus();
            return false;

        // si declaró
        } else {
            this.IdDependencia = document.getElementById("dependencia").value;
        }

        // si no seleccionó pais
        if (document.getElementById("paislaboratorio").selectedIndex == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el país donde se ubica el laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("paislaboratorio").focus();
            return false;

        // si declaró
        } else {
            this.IdPais = document.getElementById("paislaboratorio").value;
        }

        // si no seleccionó localidad (la clave en la base de datos es la
        // localidad)
        if (document.getElementById("localidadlaboratorio").selectedIndex === 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("localidadlaboratorio").focus();
            return false;

        // si seleccionó
        } else {
            this.IdLocalidad = document.getElementById("localidadlaboratorio").value;
        }

        // si no indicó la dirección
        if (document.getElementById("direccionlaboratorio").value === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la dirección postal del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("direccionlaboratorio").focus();
            return false;

        // si ingresó
        } else {
            this.Direccion = document.getElementById("direccionlaboratorio").value;
        }

        // si no ingresó el código postal
        if (document.getElementById("codigopostallaboratorio").value === ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el Código Postal de la Dirección";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("codigopostallaboratorio").focus();
            return false;

        // si ingresó
        } else {
            this.CodigoPostal = document.getElementById("codigopostallaboratorio").value;
        }

        // si está activo
        if (document.getElementById("activolaboratorio").checked){
            this.Activo = "Si";
        } else {
            this.Activo = "No";
        }

        // si no recibe muestras pide se seleccione el responsable
        if (document.getElementById("recibemuestras").checked == false){

            // verifica si seleccionó responsable
            if (document.getElementById("responsablemuestras").selectedIndex == 0){

                // presenta el mensaje y retorna
                mensaje = "Si el laboratorio no recibe muestras, indique<br>";
                mensaje += "que responsable las recibirá";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("responsablemuestras").focus();
                return false;

            // si indicó responsable de las muestras
            } else {
                this.IdRecibe = document.getElementById("responsablemuestras").value;
                this.RecibeMuestras = "No";
            }

        // si recibe muestras verifica que no halla seleccionado responsable
        } else {

            // verifica si hay un responsable seleccionado
            if (document.getElementById("responsablemuestras").selectedIndex != 0){

                // presenta el mensaje y retorna
                mensaje = "Si el laboratorio recibe muestras directamente<br>";
                mensaje += "deje en blanco el campo del responsable de las muestras";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("responsablemuestras").focus();
                return false;

            }

            // marcamos que recibe muestras
            this.RecibeMuestras = "Si";

        }

        // asigna los comentarios
        this.Observaciones = CKEDITOR.instances['observacioneslaboratorio'].getData();

        // antes de enviarlo, obtenemos por ajax las coordenadas gps
        var direccion = document.getElementById("direccionlaboratorio").value;
        direccion += " - ";
        direccion += $("#localidadlaboratorio option:selected").text();
        direccion += " - ";
        direccion += $("#jurisdiccionlaboratorio option:selected").text();
        direccion += " - ";
        direccion += $("#paislaboratorio option:selected").text();

        // obtenemos las coordenadas
        this.Coordenadas = document.getElementById("coordenadaslaboratorio").value;

        // si cargó la imagen el logo ya está en la variable global

        // llamamos la rutina de grabación
        this.grabaLaboratorio();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar los datos del formulario,
     * envia por ajax y recibe la id del registro afectado
     */
    grabaLaboratorio(){

        // declaramos el formulario html5
        var datosLaboratorio = new FormData();

        // si está editando el registro
        if (this.IdLaboratorio != 0){
            datosLaboratorio.append("Id", this.IdLaboratorio);
        }

        // agrega los valores al formdata
        datosLaboratorio.append("Nombre", this.NombreLaboratorio);
        datosLaboratorio.append("Responsable", this.Responsable);
        datosLaboratorio.append("Mail", this.EMail);
        datosLaboratorio.append("Dependencia", this.IdDependencia);
        datosLaboratorio.append("Pais", this.IdPais);
        datosLaboratorio.append("Localidad", this.IdLocalidad);
        datosLaboratorio.append("Direccion", this.Direccion);
        datosLaboratorio.append("CodigoPostal", this.CodigoPostal);
        datosLaboratorio.append("Activo", this.Activo);

        // si recibe muestras
        if (this.IdRecibe != 0){
            datosLaboratorio.append("ResponsableMuestras", this.IdRecibe);
        }

        // continúa cargando
        datosLaboratorio.append("RecibeMuestras", this.RecibeMuestras);
        datosLaboratorio.append("Comentarios", this.Observaciones);

        // si tenemos las coordenadas
        if (this.Coordenadas != ""){
            datosLaboratorio.append("Coordenadas", this.Coordenadas);
        }

        // agregamos el logo
        datosLaboratorio.append("Logo", this.Logo);

        // envía el formulario por ajax en forma sincrónica
        $.ajax({
            type: "POST",
            url: "laboratorios/graba_laboratorio.php",
            data: datosLaboratorio,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id === false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                } else {

                    // almacenamos la id en el formulario para poder
                    // agregar unidades inmediatamente
                    document.getElementById("idlaboratorio").value = data.Id;
                    this.Id = data.Id;

                    // muestra el mapa
                    $("#btnMapaLaboratorio").show();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // setea el foco
                    document.getElementById("nombrelaboratorio").focus();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Propósito: función que resetea el formulario de laboratorios y
     *            luego fija el foco en el primer elemento
     */
    cancelaLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // reinicia las variables de clase
        this.initLaboratorios();

        // reinicia el formulario y setea el foco
        document.form_laboratorios.reset();
        document.getElementById("nombrelaboratorio").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} mail - correo del laboratorio
     * @param {int} idlaboratorio - clave del registro
     * @return {boolean} - según si lo encontró
     * Método que recibe como parámetro una cadena con el mail del laboratorio
     * y la id del laboratorio y verifica que este no se encuentre repetido
     */
    verificaMailLaboratorio(mail, idlaboratorio){

        // declaración de variables
        var resultado;

        // pasamos el mail y verificamos de forma sincrónica
        $.ajax({
            url: 'laboratorios/verificamail.php?mail='+mail+'&id='+idlaboratorio,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.Encontrado == true){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idProvincia - clave de la provincia del usuario
     * Propósito: Método que recibe como parámetro la clave de la provincia
     *            y de acuerdo a las variables de sesión muestra u oculta
     *            los botones de acción
     */
    seguridadLaboratorios(idProvincia){

        // si es de nivel central
        if (sessionStorage.getItem("NivelCentral") === "Si"){

            // muestra la botonera
            $("#btnGrabaLaboratorio").show();
            $("#btnCancelaLaboratorio").show();

        // si es responsable de la misma provincia
        } else if (sessionStorage.getItem("Responsable") === "Si" && sessionStorage.getItem("CodProv") === idProvincia){

            // muestra la botonera
            $("#btnGrabaLaboratorio").show();
            $("#btnCancelaLaboratorio").show();

        // si es usuario del mismo laboratorio
        } else if (document.getElementById("idlaboratorio").value === document.getElementById("laboratorioresponsable").value){

            // muestra la botonera
            $("#btnGrabaLaboratorio").show();
            $("#btnCancelaLaboratorio").show();

        // en cualquier otro caso
        } else {

            // la oculta
            $("#btnGrabaLaboratorio").hide();
            $("#btnCancelaLaboratorio").hide();

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Propósito: Abre el cuadro de diálogo de búsqueda de laboratorios
     */
    buscaLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el contenido
        var contenido = "Ingrese parte del nombre del laboratorio a buscar.<br>";
        contenido += "<form name='busca_laboratorio'>";
        contenido += "<p><b>Texto:</b> ";
        contenido += "<input type='text' name='texto' size='25'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Buscar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='laboratorios.encuentraLaboratorio()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerLaboratorio = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    content: contenido,
                                    title: 'Buscar Laboratorio',
                                    theme: 'TooltipBorder',
                                    width: 300,
                            });
        this.layerLaboratorio.open();

        // fijamos el foco
        document.busca_laboratorio.texto.focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Propósito: verifica el formulario de búsqueda de laboratorios y luego
     *            envía la consulta al servidor
     */
    encuentraLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definición de variables
        var mensaje;
        var texto = document.busca_laboratorio.texto.value;

        // si no ingresó texto
        if (texto === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar un texto a buscar !!!";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.busca_laboratorio.texto.focus();
            return false;

        }

        // cierra el cuadro de diálogo
        this.layerLaboratorio.destroy();

        // abre el cuadro de resultados y le pasa por ajax
        // el texto a buscar
        this.layerLaboratorio = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    title: 'Resultados de la Búsqueda',
                                    theme: 'TooltipBorder',
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    ajax: {
                                    url: 'laboratorios/buscar.php?texto='+texto,
                                    reload: 'strict'
                                }
                            });
        this.layerLaboratorio.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idlaboratorio - clave del registro a obtener
     * Método que recibe como parámetro la clave de un laboratorio, ejecuta
     * la consulta y asigna los datos del registro en las variables de clase
     */
    getDatosLaboratorio(idlaboratorio){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la rutina php para obtener los datos del laboratorio
        $.get('laboratorios/getlaboratorio.php', 'id='+idlaboratorio,
            function(data){

                // asignamos los valores en las variables de clase
                laboratorios.setIdLaboratorio(data.ID);
                laboratorios.setNombreLaboratorio(data.Nombre);
                laboratorios.setResponsable(data.Responsable);
                laboratorios.setMail(data.Mail);
                laboratorios.setIdDependencia(data.Dependencia);
                laboratorios.setIdPais(data.IdPais);
                laboratorios.setIdJurisdiccion(data.IdProvincia);
                laboratorios.setIdLocalidad(data.IdLocalidad);
                laboratorios.setDireccion(data.Direccion);
                laboratorios.setCodigoPostal(data.CodigoPostal);
                laboratorios.setActivo(data.Activo);
                laboratorios.setRecibeMuestras(data.RecibeMuestras);
                laboratorios.setIdRecibe(data.IdRecibe);
                laboratorios.setCoordenadas(data.Coordenadas);
                laboratorios.setFechaAlta(data.FechaAlta);
                laboratorios.setUsuario(data.Usuario);
                laboratorios.setObservaciones(data.Observaciones);
                laboratorios.setLogo(data.Logo);

                // mostramos el registro
                laboratorios.muestraLaboratorio();

            }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase muestra el contenido
     * del registro en el formulario
     */
    muestraLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos los valores en el formulario
        document.getElementById("idlaboratorio").value = this.IdLaboratorio;
        document.getElementById("nombrelaboratorio").value = this.NombreLaboratorio;
        document.getElementById("responsablelaboratorio").value = this.Responsable;
        document.getElementById("maillaboratorio").value = this.EMail;

        // fijamos los combos
        document.getElementById("dependencia").value = this.IdDependencia;
        document.getElementById("paislaboratorio").value = this.IdPais;

        // cargamos la nómina de jurisdicciones
        this.cargaProvincias(this.IdPais, this.IdJurisdiccion);

        // cargamos la nómina de localidades
        this.cargaLocalidades(this.IdJurisdiccion, this.IdLocalidad);

        // seguimos cargando
        document.getElementById("direccionlaboratorio").value = this.Direccion;
        document.getElementById("codigopostallaboratorio").value = this.CodigoPostal;

        // si está activo
        if (this.Activo == "Si"){
            document.getElementById("activolaboratorio").checked = true;
        } else {
            document.getElementById("activolaboratorio").checked = false;
        }

        // si recibe muestras
        if (this.RecibeMuestras == "Si"){

            // marca el checkbox
            document.getElementById("recibemuestras").checked = true;

        // si no recibe muestras
        } else {

            // desmarca el checkbox
            document.getElementById("recibemuestras").checked = false;

        }

        // carga el select con los responsables de la provincia
        this.cargaResponsables(this.IdJurisdiccion, this.IdRecibe);

        // si recibió las coordenadas
        if (this.Coordenadas != ""){

            // las carga en el campo oculto y muestra el botón
            document.getElementById("coordenadaslaboratorio").value = this.Coordenadas;
            $("#btnMapaLaboratorio").show();

        // si no recibió las coordenadas
        } else {

            // carga el campo
            document.getElementById("coordenadaslaboratorio").value = "";

            // oculta el div
            $("#btnMapaLaboratorio").hide();

        }

        // terminamos de asignar
        document.getElementById("fechaalta").value = this.FechaAlta;
        document.getElementById("usuariolaboratorio").value = this.Usuario;
        document.getElementById("observacioneslaboratorio").value = this.Observaciones;
        CKEDITOR.instances['observacioneslaboratorio'].setData(this.Observaciones);

        // fija el nivel de acceso
        this.seguridadLaboratorios(this.IdProvincia);

        // si recibió el logo
        if (this.Logo != null){
            document.getElementById('logo_laboratorio').src = this.Logo;
        } else {
            document.getElementById('logo_laboratorio').src = 'imagenes/imagen_no_disponible.gif';
        }

        // si llamó desde el cuadro de diálogo
        if (this.layerLaboratorio != "undefined"){
            this.layerLaboratorio.destroy();
        }

        // setea el foco
        document.getElementById("nombrelaboratorio").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange de la subida de
     * logo del laboratorio, sube el archivo con la imagen y la muestra en el
     * formulario sin cargarla en la base
    */
    subirLogo(){

        // obtenemos el archivo seleccionado
        var file = $("#archivo_logo")[0].files[0];

        // si canceló
        if (file === undefined){

            // simplemente retornamos
            return false;

        }

        // usamos la nueva característica filereader que nos permite
        // cargar un archivo en el navegador y mostrarlo sin
        // subirlo al servidor, si es una imagen ya lo convierte
        // en base 64
        var reader = new FileReader();

        // evento llamado cuando termina la carga del archivo
        reader.onload = function (e) {

            // lo asignamos a la variable de clase
            var logo = e.target.result;

            // e.target.result contents the base64 data from the image uploaded
            document.getElementById('logo_laboratorio').src = logo;

            // asignamos en la variable de clase porque aquí estamos
            // en un espacio privado (dentro de function)
            laboratorios.setLogo(logo);

        };

        // definimos el elemento del formulario a leer
        var archivo = document.getElementById("archivo_logo");
        reader.readAsDataURL(archivo.files[0]);

    }

}
