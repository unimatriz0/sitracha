<?php

/**
 *
 * Class Laboratorios | laboratorios/laboratorios.class.php
 *
 * @package     Diagnostico
 * @subpackage  Laboratorios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de laboratorios
 * de la base de Control de Calidad
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Laboratorios {

        // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // definición de las variables de la base de datos
    protected $IdLaboratorio;             // clave del laboratorio
    protected $Nombre;                    // nombre del laboratorio
    protected $Responsable;               // responsable del laboratorio
    protected $Pais;                      // país donde queda
    protected $IdPais;                    // clave del país
    protected $Localidad;                 // nombre de la localidad
    protected $IdLocalidad;               // clave indec de la localidad
    protected $Provincia;                 // nombre de la provincia
    protected $IdProvincia;               // clave indec de la provincia
    protected $Direccion;                 // dirección postal
    protected $CodigoPostal;              // código postal correspondiente
    protected $Coordenadas;               // coordenadas GPS del laboratorio
    protected $Dependencia;               // nombre de la dependencia
    protected $IdDependencia;             // clave de la dependencia
    protected $EMail;                     // dirección de correo electrónico
    protected $FechaAlta;                 // fecha de alta del registro
    protected $Activo;                    // indica si participa de los controles
    protected $RecibeMuestras;            // indica si recibe las muestras
                                          // directamente
    protected $IdRecibe;                  // clave del responsable que
                                          // recibe las muestras
    protected $NombreRecibe;              // nombre del responsable que
                                          // recibe las muestras
    protected $Observaciones;             // observaciones y comentarios
    protected $Usuario;                   // usuario que actualizó / insertó
    protected $IdUsuario;                 // clave del responsable
    protected $Logo;                      // logo del laboratorio

    // declaración de variables usadas por la clase
    protected $NivelCentral;              // si el usuario es de nivel central
    protected $Jurisdiccion;              // jurisdicción del responsable

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdLaboratorio = 0;
        $this->Nombre = "";
        $this->Responsable = "";
        $this->Pais = "";
        $this->IdPais = 0;
        $this->Localidad = "";
        $this->IdLocalidad = "";
        $this->Provincia = "";
        $this->IdProvincia = "";
        $this->Direccion = "";
        $this->CodigoPostal = "";
        $this->Dependencia = "";
        $this->Coordenadas = "";
        $this->IdDependencia = 0;
        $this->EMail = "";
        $this->FechaAlta = "";
        $this->Activo = "";
        $this->RecibeMuestras = "";
        $this->IdRecibe = 0;
        $this->NombreRecibe = "";
        $this->Observaciones = "";
        $this->Usuario = "";
        $this->Logo = "";

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos los valores de la sesión
            $this->IdUsuario = $_SESSION["ID"];
            $this->NivelCentral = $_SESSION["NivelCentral"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdLaboratorio($idlaboratorio) {

        // verifica que sea un número
        if (!is_numeric($idlaboratorio)){

            // abandona por error
            echo "La clave del laboratorio debe ser un nùmero";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdLaboratorio = $idlaboratorio;

        }

    }
    public function setNombre($nombre){
        $this->Nombre = $nombre;
    }
    public function setResponsable($responsable){
        $this->Responsable = $responsable;
    }
    public function setIdPais($idpais){

        // si no es un número
        if (!is_numeric($idpais)){

            // abandona por error
            echo "La clave del país debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdPais = $idpais;

        }

    }
    public function setIdLocalidad($idlocalidad){
        $this->IdLocalidad = $idlocalidad;
    }
    public function setDireccion($direccion){
        $this->Direccion = $direccion;
    }
    public function setCodigoPostal($codigopostal){
        $this->CodigoPostal = $codigopostal;
    }
    public function setCoordenadas($coordenadas){
        $this->Coordenadas = $coordenadas;
    }
    public function setIdDependencia($iddependencia){

        // si no es un número
        if (!is_numeric($iddependencia)){

            // abandona por error
            echo "La clave de la dependencia debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdDependencia = $iddependencia;

        }

    }
    public function setEmail($email){
        $this->EMail = $email;
    }
    public function setActivo($activo){
        $this->Activo = $activo;
    }
    public function setRecibeMuestras($recibemuestras){
        $this->RecibeMuestras = $recibemuestras;
    }
    public function setIdRecibe($idrecibe){

        // si no es un número
        if (!is_numeric($idrecibe)){

            // abandona por error
            echo "La clave del Responsable que recibe debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdRecibe = $idrecibe;

        }

    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }
    public function setLogo($logo){
        $this->Logo = $logo;
    }

    // métodos de obtención de datos
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getResponsable(){
        return $this->Responsable;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getIdPais(){
        return $this->IdPais;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getIdLocalidad(){
        return $this->IdLocalidad;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getIdProvincia(){
        return $this->IdProvincia;
    }
    public function getDireccion(){
        return $this->Direccion;
    }
    public function getCodigoPostal(){
        return $this->CodigoPostal;
    }
    public function getCoordenadas(){
        return $this->Coordenadas;
    }
    public function getDependencia(){
        return $this->Dependencia;
    }
    public function getIdDependencia(){
        return $this->IdDependencia;
    }
    public function getEmail(){
        return $this->EMail;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getActivo(){
        return $this->Activo;
    }
    public function getRecibeMuestras(){
        return $this->RecibeMuestras;
    }
    public function getNombreRecibe(){
        return $this->NombreRecibe;
    }
    public function getIdRecibe(){
        return $this->IdRecibe;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getLogo(){
        return $this->Logo;
    }

    /**
     * Método que recibe un texto y lo busca en la tabla de laboratorios
     * retorna el vector con los registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto - cadena a buscar
     * @return array
     */
    public function buscaLaboratorio($texto){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                            UCASE(cce.laboratorios.NOMBRE) AS laboratorio,
                            cce.laboratorios.RESPONSABLE AS responsable,
                            diccionarios.localidades.NOMLOC AS localidad_laboratorio,
                            diccionarios.provincias.NOM_PROV AS provincia_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.NOMBRE LIKE '%$texto%' OR
                           cce.laboratorios.RESPONSABLE LIKE '%$texto%' OR
                           diccionarios.localidades.NOMLOC LIKE '%$texto%' OR
                           diccionarios.provincias.NOM_PROV LIKE '%$texto%'
                     ORDER BY cce.laboratorios.NOMBRE; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaLaboratorios = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nominaLaboratorios;

    }

    /**
     * Método utilizado en la selección de laboratorios de los usuarios, recibe
     * como parámetro parte del nombre del laboratorio, la clave del país del
     * usuario y la clave indec de la jurisdicción, retorna el array con los
     * registros coincidentes
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $pais - nombre del país
     * @param string $jurisdiccion - jurisdicción del país
     * @param string $laboratorio - nombre del laboratorio
     * @return array
     */
    public function buscaLaboratorioUsuario($pais, $jurisdiccion, $laboratorio){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                            UCASE(cce.laboratorios.NOMBRE) AS laboratorio,
                            cce.laboratorios.RESPONSABLE AS responsable,
                            diccionarios.localidades.NOMLOC AS localidad_laboratorio,
                            diccionarios.provincias.NOM_PROV AS provincia_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.NOMBRE LIKE '%$laboratorio%' AND
                           cce.laboratorios.PAIS = '$pais' AND
                           diccionarios.provincias.COD_PROV = '$jurisdiccion'
                     ORDER BY cce.laboratorios.NOMBRE; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // obtenemos el array con todos los registros
        $registros = $resultado->fetchAll(PDO::FETCH_ASSOC);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaLaboratorios = array_change_key_case($registros, CASE_LOWER);

        // retornamos el vector
        return $nominaLaboratorios;

    }

    /**
     * Método que recibe la clave del laboratorio e instancia las variables
     * de clase con los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idlaboratorio - clave del laboratorio
     */
    public function getDatosLaboratorio($idlaboratorio){

        // inicialización de variables
        $id_laboratorio = "";
        $nombre = "";
        $responsable = "";
        $pais = "";
        $idpais = 0;
        $localidad = "";
        $idlocalidad = "";
        $provincia = "";
        $idprovincia = "";
        $direccion = "";
        $codigo_postal = "";
        $coordenadas = "";
        $dependencia = "";
        $iddependencia = 0;
        $e_mail = "";
        $fecha_alta = "";
        $activo = "";
        $recibe_muestras = "";
        $responsable_muestras = "";
        $id_recibe = 0;
        $observaciones = "";
        $usuario = "";
        $logo = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_laboratorios.id AS id_laboratorio,
                            UCASE(diagnostico.v_laboratorios.laboratorio) AS nombre,
                            UCASE(diagnostico.v_laboratorios.responsable) AS responsable,
                            diagnostico.v_laboratorios.pais AS pais,
                            diagnostico.v_laboratorios.idpais AS idpais,
                            diagnostico.v_laboratorios.localidad AS localidad,
                            diagnostico.v_laboratorios.codloc AS idlocalidad,
                            diagnostico.v_laboratorios.provincia AS provincia,
                            diagnostico.v_laboratorios.codprov AS idprovincia,
                            UCASE(diagnostico.v_laboratorios.direccion) AS direccion,
                            diagnostico.v_laboratorios.codigo_postal AS codigo_postal,
                            diagnostico.v_laboratorios.coordenadas AS coordenadas,
                            diagnostico.v_laboratorios.dependencia AS dependencia,
                            diagnostico.v_laboratorios.iddependencia AS iddependencia,
                            diagnostico.v_laboratorios.email AS e_mail,
                            diagnostico.v_laboratorios.fecha_alta AS fecha_alta,
                            diagnostico.v_laboratorios.activo AS activo,
                            diagnostico.v_laboratorios.recibe_muestras AS recibe_muestras,
                            UCASE(diagnostico.v_laboratorios.nombre_recibe) AS responsable_muestras,
                            diagnostico.v_laboratorios.id_recibe AS id_recibe,
                            diagnostico.v_laboratorios.observaciones AS observaciones,
                            diagnostico.v_laboratorios.usuario AS usuario,
                            diagnostico.v_laboratorios.logo AS logo
                     FROM diagnostico.v_laboratorios
                     WHERE diagnostico.v_laboratorios.id = '$idlaboratorio';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($registro);

        // asignamos los valores a las variables de clase
        $this->IdLaboratorio = $id_laboratorio;
        $this->Nombre = $nombre;
        $this->Responsable = $responsable;
        $this->Pais = $pais;
        $this->IdPais = $idpais;
        $this->Localidad = $localidad;
        $this->IdLocalidad = $idlocalidad;
        $this->Provincia = $provincia;
        $this->IdProvincia = $idprovincia;
        $this->Direccion = $direccion;
        $this->CodigoPostal = $codigo_postal;
        $this->Coordenadas = $coordenadas;
        $this->Dependencia = $dependencia;
        $this->IdDependencia = $iddependencia;
        $this->EMail = $e_mail;
        $this->FechaAlta = $fecha_alta;
        $this->Activo = $activo;
        $this->RecibeMuestras = $recibe_muestras;
        $this->NombreRecibe = $responsable_muestras;
        $this->IdRecibe = $id_recibe;
        $this->Observaciones = $observaciones;
        $this->Usuario = $usuario;
        $this->Logo = $logo;

    }

    /**
     * Método que produce la consulta de inserción o edición según
     * el caso y retorna la id del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / editado
     */
    public function grabarLaboratorio(){

        // si está definida la clave
        if ($this->IdLaboratorio != 0){

            // edita el registro
            $this->editaLaboratorio();

        // si no está definida
        } else {

            // inserta un registro
            $this->nuevoLaboratorio();

        }

        // retorna la id
        return $this->IdLaboratorio;

    }

    /**
     * Método que inserta un nuevo registro en la tabla de laboratorios
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoLaboratorio(){

        // produce la consulta de inserción
        $consulta = "INSERT INTO cce.laboratorios
                            (NOMBRE,
                            RESPONSABLE,
                            PAIS,
                            LOCALIDAD,
                            DIRECCION,
                            CODIGO_POSTAL,
                            DEPENDENCIA,
                            E_MAIL,
                            ACTIVO,
                            RECIBE_MUESTRAS_CHAGAS,
                            ID_RECIBE,
                            OBSERVACIONES,
                            USUARIO,
                            LOGO)
                            VALUES
                            (:nombre,
                            :responsable,
                            :idpais,
                            :idlocalidad,
                            :direccion,
                            :codigo_postal,
                            :iddependencia,
                            :e_mail,
                            :activo,
                            :recibe_muestras,
                            :id_recibe,
                            :observaciones,
                            :idusuario,
                            :logo);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":responsable", $this->Responsable);
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":idlocalidad", $this->IdLocalidad);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":iddependencia", $this->IdDependencia);
        $psInsertar->bindParam(":e_mail", $this->EMail);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":recibe_muestras", $this->RecibeMuestras);
        $psInsertar->bindParam(":id_recibe", $this->IdRecibe);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":logo", $this->Logo);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdLaboratorio = $this->Link->lastInsertId();

    }

    /**
     * Método que edita el registro de la tabla de laboratorios
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaLaboratorio(){

        // produce la consulta de edición
        $consulta = "UPDATE cce.laboratorios SET
                            NOMBRE = :nombre,
                            RESPONSABLE = :responsable,
                            PAIS = :idpais,
                            LOCALIDAD = :idlocalidad,
                            DIRECCION = :direccion,
                            CODIGO_POSTAL = :codigo_postal,
                            DEPENDENCIA = :iddependencia,
                            E_MAIL = :e_mail,
                            ACTIVO = :activo,
                            RECIBE_MUESTRAS_CHAGAS = :recibe_muestras,
                            ID_RECIBE = :idrecibe,
                            OBSERVACIONES = :observaciones,
                            USUARIO = :idusuario,
                            LOGO = :logo
                     WHERE cce.laboratorios.ID = :idlaboratorio;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":responsable", $this->Responsable);
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":idlocalidad", $this->IdLocalidad);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":iddependencia", $this->IdDependencia);
        $psInsertar->bindParam(":e_mail", $this->EMail);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":recibe_muestras", $this->RecibeMuestras);
        $psInsertar->bindParam(":idrecibe", $this->IdRecibe);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":logo", $this->Logo);
        $psInsertar->bindParam(":idlaboratorio", $this->IdLaboratorio);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro el nombre de un laboratorio y la
     * provincia del mismo (puede haber mas de un laboratorio con el mismo
     * nombre) y retorna la clave del laboratorio
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la provincia
     * @param string $laboratorio - nombre del laboratorio
     * @return int $id_laboratorio - clave del laboratorio
     */
    public function getClaveLaboratorio($provincia, $laboratorio){

        // inicializamos las variables
        $id_laboratorio = 0;

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE cce.laboratorios.NOMBRE = '$laboratorio' AND
                           diccionarios.provincias.NOM_PROV = '$provincia'; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($registro);

        // retornamos la clave
        return $id_laboratorio;

    }

    /**
     * Método utilizado en el abm de usuarios para autorizar a cargar
     * específicamente un laboratorio, recibe parte del nombre del
     * laboratorio y la provincia y retorna los registros coincidentes
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $laboratorio - nombre del laboratorio
     * @param string $provincia - nombre de la provincia
     * @return array $registros
     */
    public function nominaLaboratorios($laboratorio, $provincia){

        // componemos la consulta
        $consulta = "SELECT cce.laboratorios.ID AS id_laboratorio,
                            cce.laboratorios.NOMBRE AS laboratorio
                     FROM cce.laboratorios INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE diccionarios.provincias.NOM_PROV = '$provincia' AND
                           cce.laboratorios.NOMBRE LIKE '$laboratorio'
                     ORDER BY cce.laboratorios.NOMBRE; ";
        $resultado = $this->Link->query($consulta);

        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método llamado en la edición de usuarios, recibe como parámetros
     * el pais, la jurisdicción y parte del nombre de un laboratorio
     * retorna un array con los registros que cumplen con el criterio
     * que luego el sistema muestra en una grilla
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $pais - nombre del país
     * @param string $provincia - nombre de la provincia
     * @param string $laboratorio - nombre del laboratorio
     * @return array $registros
     */
    public function listaLaboratorios($pais, $provincia = null, $laboratorio){

        // si la provincia es nulo
        if ($provincia == null){

            // Compone la consulta
            $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                                diccionarios.paises.NOMBRE AS pais,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.PAIS = diccionarios.paises.ID
                         WHERE cce.laboratorios.NOMBRE LIKE '%$laboratorio%' AND
                               diccionarios.paises.NOMBRE = '$pais'
                         ORDER BY cce.laboratorios.NOMBRE; ";

        // si recibió provincia
        } else {

            // Compone la consulta
            $consulta = "SELECT cce.laboratorios.ID AS idlaboratorio,
                                diccionarios.paises.NOMBRE AS pais,
                                diccionarios.provincias.NOM_PROV AS jurisdiccion,
                                cce.laboratorios.NOMBRE AS laboratorio
                         FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.PAIS = diccionarios.paises.ID
                                               INNER JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC
                                               INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                         WHERE cce.laboratorios.NOMBRE LIKE '%$laboratorio%' AND
                               diccionarios.paises.NOMBRE = '$pais' AND
                               diccionarios.provincias.NOM_PROV = '$provincia'
                         ORDER BY cce.laboratorios.NOMBRE; ";

        }

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // obtenemos el array
        $fila = $resultado->fetchAll(PDO::FETCH_ASSOC);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($fila, CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método utilizado en las altas y ediciones para evitar el duplicado
     * de direcciones de correo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - dirección de correo a verificar
     * @param int $id - clave del registro
     * @return boolean
     */
    public function verificaMail($mail, $id = false){

        // inicializamos las variables
        $registros = 0;

        // si no recibió la clave
        if ($id == false){

            // compone la consulta
            $consulta = "SELECT COUNT(*) AS registros
                         FROM cce.laboratorios
                         WHERE cce.laboratorios.E_MAIL = '$mail';";

        // si recibió la clave
        } else {

            // verificamos que no la tenga otro usuario
            $consulta = "SELECT COUNT(*) AS registros
                         FROM cce.laboratorios
                         WHERE cce.laboratorios.E_MAIL = '$mail' AND
                               cce.laboratorios.ID != '$id';";

        }

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // si hay registros
        if ($registros > 0){
            return true;
        } else {
            return false;
        }

    }

}
