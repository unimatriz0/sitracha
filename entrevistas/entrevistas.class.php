<?php

/**
 *
 * Class Entrevistas | entrevistas/entrevistas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Entrevistas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de entrevistas
 * realizadas a los pacientes y familiares
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Entrevistas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // de la tabla de entrevistas
    protected $IdEntrevista;           // clave del registro
    protected $Protocolo;              // clave del registro padre
    protected $IdLaboratorio;          // laboratorio que realizó la entrevista
    protected $Laboratorio;            // nombre del laboratorio
    protected $ResultadoEntrevista;    // resultado de la entrevista
    protected $Actitud;                // actitud durante la entrevista
    protected $Concurrio;              // si concurrió a la entrevista
    protected $TipoEntrevista;         // tipo de entrevista
    protected $FechaEntrevista;        // fecha de la entrevista
    protected $Comentarios;            // observaciones del entrevistador
    protected $Usuario;                // usuario que entrevistó
    protected $IdUsuario;              // id del usuario de la entrevista

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicialización de variables
        $this->IdEntrevista = 0;
        $this->Protocolo = 0;
        $this->ResultadoEntrevista = "";
        $this->Actitud = "";
        $this->Concurrio = "";
        $this->TipoEntrevista = "";
        $this->FechaEntrevista = "";
        $this->Comentarios = "";
        $this->Usuario = "";
        $this->Laboratorio = "";

        // si inició sesión
        session_start();

        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y la id del laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación de valores
    public function setIdEntrevista($identrevista){
        $this->IdEntrevista = $identrevista;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setResultado($resultado){
        $this->ResultadoEntrevista = $resultado;
    }
    public function setActitud($actitud){
        $this->Actitud = $actitud;
    }
    public function setConcurrio($concurrio){
        $this->Concurrio = $concurrio;
    }
    public function setTipoEntrevista($tipo){
        $this->TipoEntrevista = $tipo;
    }
    public function setFechaEntrevista($fecha){
        $this->FechaEntrevista = $fecha;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }

    // métodos públicos de retorno de datos
    public function getIdEntrevista(){
        return $this->IdEntrevista;
    }
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getResultadoEntrevista(){
        return $this->ResultadoEntrevista;
    }
    public function getActitud(){
        return $this->Actitud;
    }
    public function getConcurrio(){
        return $this->Concurrio;
    }
    public function getTipoEntrevista(){
        return $this->TipoEntrevista;
    }
    public function getFechaEntrevista(){
        return $this->FechaEntrevista;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getLaboratorio(){
        return $this->Laboratorio;
    }

    /**
     * Método que recibe como parámetro la clave de un protocolo y retorna un
     * array con todas las entrevistas realizadas a esa persona
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $protocolo - clave del protocolo a obtener
     * @return array
     */
    public function nominaEntrevistas($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_entrevistas.id_entrevista AS id_entrevista,
                            diagnostico.v_entrevistas.id_protocolo AS id_protocolo,
                            diagnostico.v_entrevistas.laboratorio AS laboratorio,
                            diagnostico.v_entrevistas.id_laboratorio AS id_laboratorio,
                            diagnostico.v_entrevistas.resultado AS resultado,
                            diagnostico.v_entrevistas.actitud AS actitud,
                            diagnostico.v_entrevistas.concurrio AS concurrio,
                            diagnostico.v_entrevistas.fecha AS fecha,
                            diagnostico.v_entrevistas.usuario AS usuario
                     FROM diagnostico.v_entrevistas
                     WHERE diagnostico.v_entrevistas.id_protocolo = '$protocolo'
                     ORDER BY STR_TO_DATE(diagnostico.v_entrevistas.fecha, '%d/%m/%Y') DESC; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el array
        return $fila;

    }

    /**
     * Método que recibe como parámetro la clave de una entrevista, encuentra
     * el registro y asigna en las variables de clase los datos del mismo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $clave - clave de la entrevista
     */
    public function getDatosEntrevista($clave){

        // inicializamos las variables
        $id_entrevista = 0;
        $id_protocolo = 0;
        $id_laboratorio = 0;
        $laboratorio = "";
        $opinion = "";
        $actitud = "";
        $concurrio = "";
        $tipo_entrevista = "";
        $fecha_entrevista = "";
        $comentarios = "";
        $usuario = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_entrevistas.id_entrevista AS id_entrevista,
                            diagnostico.v_entrevistas.id_protocolo AS id_protocolo,
                            diagnostico.v_entrevistas.id_laboratorio AS id_laboratorio,
                            diagnostico.v_entrevistas.laboratorio AS laboratorio,
                            diagnostico.v_entrevistas.resultado AS opinion,
                            diagnostico.v_entrevistas.actitud AS actitud,
                            diagnostico.v_entrevistas.concurrio AS concurrio,
                            diagnostico.v_entrevistas.tipo_entrevista AS tipo_entrevista,
                            diagnostico.v_entrevistas.fecha AS fecha_entrevista,
                            diagnostico.v_entrevistas.comentarios AS comentarios,
                            diagnostico.v_entrevistas.usuario AS usuario
                     FROM diagnostico.v_entrevistas
                     WHERE diagnostico.v_entrevistas.id_entrevista = '$clave'; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables y asignamos en la clase
        extract($fila);
        $this->IdEntrevista = $id_entrevista;
        $this->Protocolo = $id_protocolo;
        $this->IdLaboratorio = $id_laboratorio;
        $this->Laboratorio = $laboratorio;
        $this->ResultadoEntrevista = $opinion;
        $this->Actitud = $actitud;
        $this->Concurrio = $concurrio;
        $this->TipoEntrevista = $tipo_entrevista;
        $this->FechaEntrevista = $fecha_entrevista;
        $this->Comentarios = $comentarios;
        $this->Usuario = $usuario;

    }

    /**
     * Método público que efectúa la consulta de inserción o edición según
     * corresponda, retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $clave
     */
    public function grabaEntrevista(){

        // si está insertando
        if ($this->IdEntrevista == 0){
            $this->nuevaEntrevista();
        } else {
            $this->editaEntrevista();
        }

        // retorna la clave
        return $this->IdEntrevista;

    }

    /**
     * Método privado que efectúa la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaEntrevista(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.entrevistas
                                 (id_protocolo,
                                  id_laboratorio,
                                  resultado,
                                  actitud,
                                  concurrio,
                                  tipo,
                                  fecha,
                                  comentarios,
                                  id_usuario)
                                 VALUES
                                 (:id_protocolo,
                                  :id_laboratorio,
                                  :resultado,
                                  :actitud,
                                  :concurrio,
                                  :tipo,
                                  STR_TO_DATE(:fecha, '%d/%m/%Y'),
                                  :comentarios,
                                  :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":resultado", $this->ResultadoEntrevista);
        $psInsertar->bindParam(":actitud", $this->Actitud);
        $psInsertar->bindParam(":concurrio", $this->Concurrio);
        $psInsertar->bindParam(":tipo", $this->TipoEntrevista);
        $psInsertar->bindParam(":fecha", $this->FechaEntrevista);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdEntrevista = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que edita el registro actual
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaEntrevista(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.entrevistas SET
                            id_protocolo = :id_protocolo,
                            id_laboratorio = :id_laboratorio,
                            resultado = :resultado,
                            actitud = :actitud,
                            concurrio = :concurrio,
                            tipo = :tipo,
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            comentarios = :comentarios,
                            id_usuario = :id_usuario
                     WHERE diagnostico.entrevistas.id_entrevista = :id_entrevista;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":resultado", $this->ResultadoEntrevista);
        $psInsertar->bindParam(":actitud", $this->Actitud);
        $psInsertar->bindParam(":concurrio", $this->Concurrio);
        $psInsertar->bindParam(":tipo", $this->TipoEntrevista);
        $psInsertar->bindParam(":fecha", $this->FechaEntrevista);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_entrevista", $this->IdEntrevista);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método público que recibe como parámetro la clave de una entrevista
     * y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $identrevista - clave del registro
     */
    public function borraEntrevista($identrevista){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.entrevistas
                     WHERE diagnostico.entrevistas.id_entrevista = '$identrevista';";
        $this->Link->exec($consulta);

    }
}