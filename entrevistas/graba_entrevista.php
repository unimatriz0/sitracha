<?php

/**
 *
 * entrevistas/graba_entrevista.php
 *
 * @package     Diagnostico
 * @subpackage  Entrevistas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/07/2018)/
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta
 * la consulta en el servidor, retorna en un array json la
 * clave del registro afectado
 *
*/

// incluimos e instanciamos la clase
require_once ("entrevistas.class.php");
$entrevista = new Entrevistas();

// si recibió la id
if (!empty($_POST["Id"])){
    $entrevista->setIdEntrevista($_POST["Id"]);
}

// asignamos el resto de los valores
$entrevista->setProtocolo($_POST["Protocolo"]);
$entrevista->setResultado($_POST["Resultado"]);
$entrevista->setActitud($_POST["Actitud"]);
$entrevista->setConcurrio($_POST["Concurrio"]);
$entrevista->setTipoEntrevista($_POST["Tipo"]);
$entrevista->setFechaEntrevista($_POST["Fecha"]);
$entrevista->setComentarios($_POST["Comentarios"]);

// ejecutamos la consulta
$id = $entrevista->grabaEntrevista();

// retornamos
echo json_encode(array("Id" => $id));

?>