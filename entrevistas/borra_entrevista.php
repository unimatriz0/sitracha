<?php

/**
 *
 * entrevistas/borra_entrevista.php
 *
 * @package     Diagnostico
 * @subpackage  Entrevistas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/07//2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe como parámetro la clave de una entrevista y
 * ejecuta la consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once("entrevistas.class.php");
$entrevista = new Entrevistas();

// ejecutamos la consulta
$entrevista->borraEntrevista($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>