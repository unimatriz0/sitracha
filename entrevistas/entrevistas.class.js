/*

    Nombre: entrevistas.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 06/02/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de entrevistas

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las entrevistas
 */
class Entrevistas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initEntrevistas();

        // layer del formulario de entrevistas
        this.layerEntrevistas = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initEntrevistas(){

        // inicializamos las variables
        this.IdEntrevista = 0;              // clave del registro
        this.Resultado = "";                // resultado de la entrevista
        this.Actitud = "";                  // actitud del entrevistado
        this.Concurrio = "";                // si concurrió a la entrevista
        this.Tipo = "";                     // tipo de entrevista realizada
        this.Fecha = "";                    // fecha de realización
        this.Comentarios = "";              // comentarios del entrevistador
        this.Usuario = "";                  // nombre del usuario

    }

    // métodos de asignación de valores
    setIdEntrevista(identrevista){
        this.IdEntrevista = identrevista;
    }
    setResultado(resultado){
        this.Resultado = resultado;
    }
    setActitud(actitud){
        this.Actitud = actitud;
    }
    setConcurrio(concurrio){
        this.Concurrio = concurrio;
    }
    setTipo(tipo){
        this.Tipo = tipo;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setComentarios(comentarios){
        this.Comentarios = comentarios;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de entrada de datos
     * e inicializa las variables de clase
     */
    nuevaEntrevista(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("id_entrevista").value = "";
        document.getElementById("resultado_entrevista").value = "";
        document.getElementById("actitud_entrevista").value = "";
        document.getElementById("concurrio_entrevista").value = "";
        document.getElementById("tipo_entrevista").value = "";
        document.getElementById("fecha_entrevista").value = fechaActual();
        document.getElementById("usuario_entrevista").value = sessionStorage.getItem("Usuario");
        document.getElementById("observaciones_entrevista").value = "";
        CKEDITOR.instances['observaciones_entrevista'].setData();

        // inicializamos las variables
        this.initEntrevistas();

        // fijamos el foco
        document.getElementById("resultado_entrevista").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer y muestra el formulario con las entrevistas realizadas
     * al paciente
     */
    verEntrevistas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // seteamos el switch de apertura
        pacientes.setEntrevistas();

        // declaración de variables
        var mensaje;

        // verifica que exista un paciente activo
        if (document.getElementById("protocolo_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe tener en pantalla un paciente antes<br>";
            mensaje += "de declarar las entrevistas realizadas";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // el protocolo lo tomamos del formulario padre
        this.IdProtocolo = document.getElementById("protocolo_paciente").value;

        // cargamos el formulario en una ventana emergente
        this.layerEntrevistas = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Entrevistas del Paciente',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'entrevistas/entrevistas.html',
                        reload: 'strict'
                    }
        });
        this.layerEntrevistas.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en la grilla las entrevistas previas del paciente
     */
    grillaEntrevistas(){

        // ahora cargamos en el div las entrevistas previas
        $("#grilla_entrevistas").load("entrevistas/grilla_entrevistas.php?protocolo="+this.IdProtocolo);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de entrevistas
     * antes de enviarlo al servidor
     */
    verificaEntrevista(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está editando
        if (document.getElementById("id_entrevista").value != ""){

            // asigna en la variable de clase
            this.IdEntrevista = document.getElementById("id_entrevista").value;

        }

        // si no indicó el resultado
        if (document.getElementById("resultado_entrevista").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el resultado de la entrevista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("resultado_entrevista").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Resultado = document.getElementById("resultado_entrevista").value;

        }

        // si no indicó la actitud
        if (document.getElementById("actitud_entrevista").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione la actitud del entrevistado";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("actitud_entrevista").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Actitud = document.getElementById("actitud_entrevista").value;

        }

        // si no indicó si concurrió
        if (document.getElementById("concurrio_entrevista").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si concurrió a la entrevista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombrelaboratorio").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Concurrio = document.getElementById("concurrio_entrevista").value;

        }

        // si no indicó el tipo de entrevista
        if (document.getElementById("tipo_entrevista").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique cual ha sido el tipo de entrevista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tipo_entrevista").focus();
            return false;

        // si indicó
        } else {

            // asigna en la variable de clase
            this.Tipo = document.getElementById("tipo_entrevista").value;

        }

        // si no ingresó la fecha
        if (document.getElementById("fecha_entrevista").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de la entrevista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombrelaboratorio").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Fecha = document.getElementById("fecha_entrevista").value;

        }

        // agregamos los comentarios
        this.Comentarios = CKEDITOR.instances['observaciones_entrevista'].getData();

        // grabamos el registro
        this.grabaEntrevista();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos del formulario al servidor
     */
    grabaEntrevista(){

        // declaración de variables
        var datosEntrevista = new FormData();

        // si está editando
        if (this.IdEntrevista != 0){
            datosEntrevista.append("Id", this.IdEntrevista);
        }

        // agregamos el resto de las variables
        datosEntrevista.append("Protocolo", this.IdProtocolo);
        datosEntrevista.append("Resultado", this.Resultado);
        datosEntrevista.append("Actitud", this.Actitud);
        datosEntrevista.append("Concurrio", this.Concurrio);
        datosEntrevista.append("Tipo", this.Tipo);
        datosEntrevista.append("Fecha", this.Fecha);
        datosEntrevista.append("Comentarios", this.Comentarios);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "entrevistas/graba_entrevista.php",
            type: "POST",
            data: datosEntrevista,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // limpiamos el formulario y recargamos la grilla
                    entrevistas.nuevaEntrevista();
                    entrevistas.grillaEntrevistas();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} identrevista
     * Método que recibe como parámetro la id de una entrevista
     * obtiene el registro y asigna los datos a las variables de
     * clase
     */
    getDatosEntrevista(identrevista){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos el registro
        $.ajax({
            url: "entrevistas/get_entrevista.php?id=" + identrevista,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // asignamos en las variables de clase
                entrevistas.setIdEntrevista(data.Id);
                entrevistas.setResultado(data.Resultado);
                entrevistas.setActitud(data.Actitud);
                entrevistas.setConcurrio(data.Concurrio);
                entrevistas.setTipo(data.Tipo);
                entrevistas.setFecha(data.Fecha);
                entrevistas.setComentarios(data.Comentarios);
                entrevistas.setUsuario(data.Usuario);

                // mostramos el registro
                entrevistas.muestraEntrevista();

            }

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase muestra los
     * datos de la entrevista en el formulario
     */
    muestraEntrevista(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("id_entrevista").value = this.IdEntrevista;
        document.getElementById("resultado_entrevista").value = this.Resultado;
        document.getElementById("actitud_entrevista").value = this.Actitud;
        document.getElementById("concurrio_entrevista").value = this.Concurrio;
        document.getElementById("tipo_entrevista").value = this.Tipo;
        document.getElementById("fecha_entrevista").value = this.Fecha;
        document.getElementById("usuario_entrevista").value = this.Usuario;
        document.getElementById("observaciones_entrevista").value = this.Comentarios;
        CKEDITOR.instances['observaciones_entrevista'].setData(this.Comentarios);

        // fijamos el foco
        document.getElementById("resultado_entrevista").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que simplemente
     * cierra el layer
     */
    cancelaEntrevista(){

        // destruimos el layer y retornamos
        this.layerEntrevistas.destroy();
        return false;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} identrevista - clave del registro
     * Método que recibe como parámetro la id de una entrevista
     * y envía al servidor la consulta de eliminación
     */
    borraEntrevista(identrevista){

        // reiniciamos la sesion
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Entrevista',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('entrevistas/borra_entrevista.php?id='+identrevista,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // recargamos la grilla
                            entrevistas.grillaEntrevistas();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}