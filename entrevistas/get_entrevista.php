<?php

/**
 *
 * entrevistas/get_entrevista.php
 *
 * @package     Diagnostico
 * @subpackage  Entrevistas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/03//2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una entrevista y retorna
 * un array json con los datos del registro
 *
*/

// incluimos e instanciamos la clase
require_once("entrevistas.class.php");
$entrevista = new Entrevistas();

// obtenemos el registro
$entrevista->getDatosEntrevista($_GET["id"]);

// retornamos un array json
echo json_encode(array("Id" =>          $entrevista->getIdEntrevista(),
                       "Resultado" =>   $entrevista->getResultadoEntrevista(),
                       "Actitud" =>     $entrevista->getActitud(),
                       "Concurrio" =>   $entrevista->getConcurrio(),
                       "Tipo" =>        $entrevista->getTipoEntrevista(),
                       "Fecha" =>       $entrevista->getFechaEntrevista(),
                       "Comentarios" => $entrevista->getComentarios(),
                       "Usuario" =>     $entrevista->getUsuario()));

?>