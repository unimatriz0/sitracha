<?php

/**
 *
 * entrevistas/grilla_entrevistas.php
 *
 * @package     Diagnostico
 * @subpackage  Entrevistas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/03//2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma la tabla con las entrevistas realizadas al
 * paciente activo, recibe por get el protocolo
 *
*/

// incluimos e instanciamos la clase
require_once("entrevistas.class.php");
$entrevista = new Entrevistas();

// obtenemos el laboratorio del usuario actual
session_start();
$id_lab_usuario = $_SESSION["IdLaboratorio"];
session_write_close();

// define la tabla
echo "<table width='95%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Fecha</th>";
echo "<th>Actitud</th>";
echo "<th>Resultado</th>";
echo "<th>Entrevistador</th>";
echo "<th>";
echo "<span class='tooltip'
            title='Pulse para agregar una entrevista'>";
echo "<input type='button'
             name='btnNuevaEntrevista'
             id='btnNuevaEntrevista'
             class='botonagregar'
             onClick='entrevistas.nuevaEntrevista()'>";
echo "</span>";
echo "</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// define el cuerpo
echo "<tbody>";

// obtenemos las entrevistas previas
$nomina = $entrevista->nominaEntrevistas($_GET["protocolo"]);

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>$fecha</td>";
    echo "<td>$actitud</td>";
    echo "<td>$resultado</td>";
    echo "<td>$usuario</td>";

    // abrimos la columna
    echo "<td>";

    // si es del mismo laboratorio
    if ($id_lab_usuario == $id_laboratorio){

        // presenta el botón editar
        echo "<span class='tooltip'
                    title='Pulse para editar la entrevista'>";
        echo "<input type='button'
                     name='btnEditaEntrevista'
                     id='btnEditaEntrevista'
                     class='botoneditar'
                     onClick='entrevistas.getDatosEntrevista($id_entrevista)'>";
        echo "</span>";

    }

    // cerramos la columna
    echo "</td>";

    // abrimos la columna
    echo "<td>";

    // si es del mismo laboratorio
    if ($id_lab_usuario == $id_laboratorio){

        // presenta el botón eliminar
        echo "<span class='tooltip'
                    title='Pulse para borrar la entrevista'>";
        echo "<input type='button'
                     name='btnBorraEntrevista'
                     id='btnBorraEntrevista'
                     class='botonborrar'
                     onClick='entrevistas.borraEntrevista($id_entrevista)'>";
        echo "</span>";

    }

    // cerramos la columna
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

</script>