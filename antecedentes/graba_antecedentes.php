<?php

/**
 *
 * grabaantecedentes | antecedentes/grabaantecedentes.php
 *
 * @package     Diagnostico
 * @subpackage  Antecedentes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario, instancia las
 * clases y ejecuta las consulta de grabación, luego retorna la id de
 * cada una de las tablas (antecedentes tóxicos, chagas agudo,
 * digestivo, disautonomía y familiares)
 *
*/

// incluimos e instanciamos las librerías
require_once("antecedentes.class.php");
require_once("../agudo/agudo.class.php");
require_once("../digestivo/digestivo.class.php");
require_once("../disautonomia/disautonomia.class.php");
require_once("../familiares/familiares.class.php");

// instanciamos las clases
$antecedentes = new Antecedentes();
$agudo = new Agudo();
$digestivo = new Digestivo();
$disautonomia = new Disautonomia();
$familiares = new Familiares();

// fijamos las propiedades de la clase de antecedentes tóxicos
$antecedentes->setPaciente($_POST["Protocolo"]);
$antecedentes->setId($_POST["IdToxico"]);
$antecedentes->setFuma($_POST["Fuma"]);
$antecedentes->setCigarrillos(($_POST["Cigarrillos"]));
$antecedentes->setExFumador(($_POST["ExFumador"]));
$antecedentes->setAnios($_POST["Duracion"]);
$antecedentes->setDejo($_POST["DejoFumador"]);
$antecedentes->setAlcohol($_POST["Alcohol"]);
$antecedentes->setLitros($_POST["Litros"]);
$antecedentes->setBebida($_POST["Bebida"]);
$antecedentes->setAdicciones($_POST["Adicciones"]);

// las propiedades de la clase de chagas agudo
$agudo->setPaciente(($_POST["Protocolo"]));
$agudo->setId($_POST["IdAgudo"]);
$agudo->setSintomas($_POST["Sintomas"]);
$agudo->setObservaciones($_POST["Observaciones"]);

// las propiedades de la tabla de compromiso digestivo
$digestivo->setPaciente($_POST["Protocolo"]);
$digestivo->setId($_POST["IdDigestivo"]);
$digestivo->setDisfagia($_POST["DisfagiaDigestivo"]);
$digestivo->setPirosis($_POST["Pirosis"]);
$digestivo->setConstipacion($_POST["Constipacion"]);
$digestivo->setRegurgitacion($_POST["Regurgitacion"]);
$digestivo->setBolo($_POST["Bolo"]);
$digestivo->setNoTiene($_POST["NoTieneDigestivo"]);

// las propiedades de la tabla de disautonomía
$disautonomia->setPaciente($_POST["Protocolo"]);
$disautonomia->setId($_POST["IdDisautonomia"]);
$disautonomia->setHipotension($_POST["Hipotension"]);
$disautonomia->setBradicardia($_POST["Bradicardia"]);
$disautonomia->setAstenia($_POST["Astenia"]);
$disautonomia->setNoTiene($_POST["NoTieneDisauto"]);

// las propiedades de antecedentes familiares
$familiares->setPaciente($_POST["Protocolo"]);
$familiares->setId($_POST["IdFamiliar"]);
$familiares->setSubita($_POST["Subita"]);
$familiares->setCardiopatia($_POST["Cardiopatia"]);
$familiares->setDisfagia($_POST["Disfagia"]);
$familiares->setMarcapaso($_POST["Marcapaso"]);
$familiares->setNoSabe($_POST["NoSabeFamiliar"]);
$familiares->setNoTiene($_POST["NoTieneFamiliar"]);
$familiares->setOtra($_POST["OtraCronica"]);

// grabamos los registros
$idtoxico = $antecedentes->grabaAntecedente();
$idagudo = $agudo->grabaAgudo();
$idfamiliar = $familiares->grabaFamiliares();
$iddisautonomia = $disautonomia->grabaDisautonia();
$iddigestivo = $digestivo->grabaDigestivo();

// por ahora retorna siempre verdadero
$error = 1;

// retornamos las claves y el nivel de error
echo json_encode(array("Error" =>          $error,
                       "IdAgudo" =>        $idagudo,
                       "IdToxico" =>       $idtoxico,
                       "IdFamiliar" =>     $idfamiliar,
                       "IdDisautonomia" => $iddisautonomia,
                       "IdDigestivo" =>    $iddigestivo));

?>