<?php

/**
 *
 * Class Antecedentes | antecedentes/antecedentes.class.php
 *
 * @package     Diagnostico
 * @subpackage  Antecedentes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * antecedentes tóxicos del paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Antecedentes {

    // definición de las variables de clase
    protected $Link;                // puntero a la base de datos
    protected $Id;                  // clave del registro
    protected $Paciente;            // clave del paciente
    protected $Fuma;                // 0 falso - 1 verdadero
    protected $Cigarrillos;         // nro de cigarrillos diarios
    protected $ExFumador;           // 0 falso - 1 verdadero
    protected $Anios;               // años de fumador
    protected $Dejo;                // fecha en que dejó de fumar
    protected $Alcohol;             // 0 falso - 1 verdadero
    protected $Litros;              // litros de bebida a la semana
    protected $Bebida;              // tipo de bebida
    protected $Adicciones;          // adicciones del paciente
    protected $IdUsuario;           // clave del usuario
    protected $Usuario;             // nombre del usuario
    protected $Fecha;               // fecha de alta

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initAntecedentes();

    }

    // método que inicializa las variables de clase
    protected function initAntecedentes(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Fuma = 0;
        $this->Cigarrillos = 0;
        $this->ExFumador = 0;
        $this->Dejo = "";
        $this->Alcohol = 0;
        $this->Litros = 0;
        $this->Bebida = "";
        $this->Adicciones = "";
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setFuma($fuma){
        $this->Fuma = $fuma;
    }
    public function setCigarrillos($cigarrillos){
        $this->Cigarrillos = $cigarrillos;
    }
    public function setExFumador($exfumador){
        $this->ExFumador = $exfumador;
    }
    public function setAnios($anios){
        $this->Anios = $anios;
    }
    public function setDejo($dejo){
        $this->Dejo = $dejo;
    }
    public function setAlcohol($alcohol){
        $this->Alcohol = $alcohol;
    }
    public function setLitros($litros){
        $this->Litros = $litros;
    }
    public function setBebida($bebida){
        $this->Bebida = $bebida;
    }
    public function setAdicciones($adicciones){
        $this->Adicciones = $adicciones;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getFuma(){
        return $this->Fuma;
    }
    public function getCigarrillos(){
        return $this->Cigarrillos;
    }
    public function getExFumador(){
        return $this->ExFumador;
    }
    public function getAnios(){
        return $this->Anios;
    }
    public function getDejo(){
        return $this->Dejo;
    }
    public function getAlcohol(){
        return $this->Alcohol;
    }
    public function getLitros(){
        return $this->Litros;
    }
    public function getBebida(){
        return $this->Bebida;
    }
    public function getAdicciones(){
        return $this->Adicciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $protocolo - protocolo del paciente
     * Mëtodo que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los valores
     */
    public function getDatosAntecedente($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_antecedentes.id AS id,
                            diagnostico.v_antecedentes.paciente AS paciente,
                            diagnostico.v_antecedentes.fuma AS fuma,
                            diagnostico.v_antecedentes.cigarrillos AS cigarrillos,
                            diagnostico.v_antecedentes.exfumador AS exfumador,
                            diagnostico.v_antecedentes.anios AS anios,
                            diagnostico.v_antecedentes.dejo AS dejo,
                            diagnostico.v_antecedentes.alcohol AS alcohol,
                            diagnostico.v_antecedentes.litros AS litros,
                            diagnostico.v_antecedentes.bebida AS bebida,
                            diagnostico.v_antecedentes.adicciones AS adicciones,
                            diagnostico.v_antecedentes.usuario AS usuario,
                            diagnostico.v_antecedentes.fecha AS fecha
                     FROM diagnostico.v_antecedentes
                     WHERE diagnostico.v_antecedentes.id = '$protocolo'; ";
        $resultado = $this->Link->query($consulta);

        // si la consulta tuvo registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);

            // asignamos los valores
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Fuma = $fuma;
            $this->Cigarrillos = $cigarrillos;
            $this->ExFumador = $exfumador;
            $this->Anios = $anios;
            $this->Dejo = $dejo;
            $this->Alcohol = $alcohol;
            $this->Litros = $litros;
            $this->Bebida = $bebida;
            $this->Adicciones = $adicciones;
            $this->Usuario = $usuario;
            $this->Fecha = $fecha;

        // si no encontró el registro
        } else {

            // inicializamos las variables
            $this->initAntecedentes();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id - clave del registro afectado
     * Método que ejecuta la consulta de edición o
     * inserción según corresponda y retorna la clave
     * del registro afectado
     */
    public function grabaAntecedente(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoAntecedente();
        } else {
            $this->editaAntecedente();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevoAntecedente(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.antecedentes
                            (paciente,
                             fuma,
                             cigarrillos,
                             exfumador,
                             anios,
                             dejo,
                             alcohol,
                             litros,
                             bebida,
                             adicciones,
                             usuario)
                            VALUES
                            (:paciente,
                             :fuma,
                             :cigarrillos,
                             :exfumador,
                             :anios,
                             :dejo,
                             :alcohol,
                             :litros,
                             :bebida,
                             :adicciones,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",    $this->Paciente);
        $psInsertar->bindParam(":fuma",        $this->Fuma);
        $psInsertar->bindParam(":cigarrillos", $this->Cigarrillos);
        $psInsertar->bindParam(":exfumador",   $this->ExFumador);
        $psInsertar->bindParam(":anios",       $this->Anios);
        $psInsertar->bindParam(":dejo",        $this->Dejo);
        $psInsertar->bindParam(":alcohol",     $this->Alcohol);
        $psInsertar->bindParam(":litros",      $this->Litros);
        $psInsertar->bindParam(":bebida",      $this->Bebida);
        $psInsertar->bindParam(":adicciones",  $this->Adicciones);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaAntecedente(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.antecedentes SET
                            fuma = :fuma,
                            cigarrillos = :cigarrillos,
                            exfumador = :exfumador,
                            anios = :anios,
                            dejo = :dejo,
                            alcohol = :alcohol,
                            litros = :litros,
                            bebida = :bebida,
                            adicciones = :adicciones,
                            usuario = :usuario
                     WHERE diagnostico.antecedentes.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":fuma",        $this->Fuma);
        $psInsertar->bindParam(":cigarrillos", $this->Cigarrillos);
        $psInsertar->bindParam(":exfumador",   $this->ExFumador);
        $psInsertar->bindParam(":anios",       $this->Anios);
        $psInsertar->bindParam(":dejo",        $this->Dejo);
        $psInsertar->bindParam(":alcohol",     $this->Alcohol);
        $psInsertar->bindParam(":litros",      $this->Litros);
        $psInsertar->bindParam(":bebida",      $this->Bebida);
        $psInsertar->bindParam(":adicciones",  $this->Adicciones);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idantecedente clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     */
    public function borraAntecedente($idantecedente){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.antecedentes
                     WHERE diagnostico.antecedentes.id = '$idantecedente'; ";
        $this->Link->exec($consulta);

    }

}
?>