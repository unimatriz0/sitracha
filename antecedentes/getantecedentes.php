<?php

/**
 *
 * getantecedentes | antecedentes/getantecedentes.php
 *
 * @package     Diagnostico
 * @subpackage  Antecedentes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe como parámetro el protocolo de un paciente y
 * retorna los datos del formulario de antecedentes que comprende
 * la tabla de chagas agudo, la tabla de antecedentes tóxicos,
 * la tabla de antecedentes familiares, la tabla de antecedentes
 * digestivos, la tabla de disautonomía
 *
*/

// incluimos e instanciamos las librerías
require_once("antecedentes.class.php");
require_once("../agudo/agudo.class.php");
require_once("../digestivo/digestivo.class.php");
require_once("../disautonomia/disautonomia.class.php");
require_once("../familiares/familiares.class.php");

// obtenemos el protocolo
$protocolo = $_GET["protocolo"];

// instanciamos las clases
$antecedentes = new Antecedentes();
$agudo = new Agudo();
$digestivo = new Digestivo();
$disautonomia = new Disautonomia();
$familiares = new Familiares();

// obtenemos los registros
$antecedentes->getDatosAntecedente($protocolo);
$agudo->getDatosAgudo($protocolo);
$digestivo->getDatosDigestivo($protocolo);
$disautonomia->getDatosDisautonomia($protocolo);
$familiares->getDatosFamiliares($protocolo);

// ahora regornamos el vector
echo json_encode(array("Alta" =>              $antecedentes->getFecha(),
                       "Usuario" =>           $antecedentes->getUsuario(),
                       "IdAgudo" =>           $agudo->getId(),
                       "Sintomas" =>          $agudo->getSintomas(),
                       "Observaciones" =>     $agudo->getObservaciones(),
                       "IdToxico" =>          $antecedentes->getId(),
                       "Fuma" =>              $antecedentes->getFuma(),
                       "Cigarrillos" =>       $antecedentes->getCigarrillos(),
                       "ExFumador" =>         $antecedentes->getExFumador(),
                       "DuracFumador" =>      $antecedentes->getAnios(),
                       "DejoFumar" =>         $antecedentes->getDejo(),
                       "Alcohol" =>           $antecedentes->getAlcohol(),
                       "Litros" =>            $antecedentes->getLitros(),
                       "Bebida" =>            $antecedentes->getBebida(),
                       "Adicciones" =>        $antecedentes->getAdicciones(),
                       "IdFamiliar" =>        $familiares->getId(),
                       "Subita" =>            $familiares->getSubita(),
                       "Cardiopatia" =>       $familiares->getCardiopatia(),
                       "Disfagia" =>          $familiares->getDisfagia(),
                       "Marcapaso" =>         $familiares->getMarcapaso(),
                       "NoSabeFamiliar" =>    $familiares->getNoSabe(),
                       "NoTieneFamiliar" =>   $familiares->getNoTiene(),
                       "Otra" =>              $familiares->getOtra(),
                       "IdDisautonomia" =>    $disautonomia->getId(),
                       "Hipotension" =>       $disautonomia->getHipotension(),
                       "Bradicardia" =>       $disautonomia->getBradicardia(),
                       "Astenia" =>           $disautonomia->getAstenia(),
                       "NoTieneDisauto" =>    $disautonomia->getNoTiene(),
                       "IdDigestivo" =>       $digestivo->getId(),
                       "DisfagiaDigestivo" => $digestivo->getDisfagia(),
                       "Pirosis" =>           $digestivo->getPirosis(),
                       "Regurgitacion" =>     $digestivo->getRegurgitacion(),
                       "Constipacion" =>      $digestivo->getConstipacion(),
                       "Bolo" =>              $digestivo->getBolo(),
                       "NoTieneDigestivo" =>  $digestivo->getNoTiene()));

?>