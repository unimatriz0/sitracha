/*

    Nombre: antecedentes.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 10/06/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla el formulario de antecedentes
                 del paciente

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre el formulario de
 * antecedentes
 */
class Antecedentes {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initAntecedentes();

        // cargamos el formulario
        $("#form_antecedentes").load("antecedentes/form_antecedentes.html");

        // reiniciamos la sesión
        sesion.reiniciar();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initAntecedentes(){

        // variables comunes
        this.FechaAlta = "";                  // fecha de alta del registro
        this.Usuario = "";                    // usuario que grabó el registro
        this.Protocolo = 0;                   // protocolo del paciente

        // de la tabla de antecedentes de chagas agudo
        this.IdAgudo = 0;                     // clave del registro
        this.Sintomas = 0;                    // síntomas de chagas agudo (0 falso - 1 true)
        this.Observaciones = "";              // observaciones del usuario

        // de la tabla de antecedentes tóxicos
        this.IdToxico = 0;                    // clave del registro
        this.Fuma = 0;                        // 0 falso - 1 true
        this.Cigarrillos = 0;                 // número de cigarrillos diarios
        this.ExFumador = 0;                   // 0 falso - 1 true
        this.DuracFumador = 0;                // años de duración como fumador
        this.DejoFumador = "";                // fecha en que dejó de fumar
        this.Alcohol = 0;                     // 0 falso - 1 true
        this.Litros = 0;                      // litros a la semana de bebida
        this.Bebida = "";                     // tipo de bebida
        this.Adicciones = "";                 // descripción de las adicciones

        // de la tabla de antecedentes familiares
        this.IdFamiliar = 0;                  // clave del registro
        this.Subita = 0;                      // 0 falso - 1 true muerte súbita
        this.Cardiopatia = 0;                 // 0 falso - 1 true
        this.Disfagia = 0;                    // 0 falso - 1 true
        this.Marcapaso = 0;                   // 0 falso - 1 true
        this.NoSabeFamiliar = 0;              // 0 falso - 1 true
        this.NoTieneFamiliar = 0;             // 0 falso - 1 true
        this.Otra = "";                       // otra enfermedad crónica

        // de la tabla de disautonomía
        this.IdDisautonomia = 0;              // clave del registro
        this.Hipotension = 0;                 // 0 falso - 1 true
        this.Bradicardia = 0;                 // 0 falso - 1 true
        this.Astenia = 0;                     // 0 falso - 1 true
        this.NoTieneDisauto = 0;              // 0 falso - 1 true

        // de la tabla de compromiso digestivo
        this.IdDigestivo = 0;                 // clave del registro
        this.DisfagiaDigestivo = 0;           // 0 falso - 1 true
        this.Pirosis = 0;                     // 0 falso - 1 true
        this.Regurgitacion = 0;               // 0 falso - 1 true
        this.Constipacion = 0;                // 0 falso - 1 true
        this.Bolo = 0;                        // 0 falso - 1 true
        this.NoTieneDigestivo = 0;            // 0 falso - 1 true

    }

    // métodos de asignación de valores
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setIdAgudo(idagudo){
        this.IdAgudo = idagudo;
    }
    setSintomas(sintomas){
        this.Sintomas = sintomas;
    }
    setObservaciones (observaciones){
        this.Observaciones = observaciones;
    }
    setIdToxico(idtoxico){
        this.IdToxico = idtoxico;
    }
    setFuma(fuma){
        this.Fuma = fuma;
    }
    setCigarrillos(cigarrillos){
        this.Cigarrillos = cigarrillos;
    }
    setExFumador(exfumador){
        this.ExFumador = exfumador;
    }
    setDuracFumador(duracion){
        this.DuracFumador = duracion;
    }
    setDejoFumar(fecha){
        this.DejoFumador = fecha;
    }
    setAlcohol(alcohol){
        this.Alcohol = alcohol;
    }
    setLitros(litros){
        this.Litros = litros;
    }
    setBebida(bebida){
        this.Bebida = bebida;
    }
    setAdicciones(adicciones){
        this.Adicciones = adicciones;
    }
    setIdFamiliar(idfamiliar){
        this.IdFamiliar = idfamiliar;
    }
    setSubita(subita){
        this.Subita = subita;
    }
    setCardiopatia(cardiopatia){
        this.Cardiopatia = cardiopatia;
    }
    setDisfagia(disfagia){
        this.Disfagia = disfagia;
    }
    setMarcapaso(marcapaso){
        this.Marcapaso = marcapaso;
    }
    setNoSabeFamiliar(nosabe){
        this.NoSabeFamiliar = nosabe;
    }
    setNoTieneFamiliar(notiene){
        this.NoTieneFamiliar = notiene;
    }
    setOtra(otra){
        this.Otra = otra;
    }
    setIdDisautonomia(iddisautonomia){
        this.IdDisautonomia = iddisautonomia;
    }
    setHipotension(hipotension){
        this.Hipotension = hipotension;
    }
    setBradicardia(bradicardia){
        this.Bradicardia = bradicardia;
    }
    setAstenia(astenia){
        this.Astenia = astenia;
    }
    setNoTieneDisauto(notiene){
        this.NoTieneDisauto = notiene;
    }
    setIdDigestivo(iddigestivo){
        this.IdDigestivo = iddigestivo;
    }
    setDisfagiaDigestivo(disfagia){
        this.DisfagiaDigestivo = disfagia;
    }
    setPirosis(pirosis){
        this.Pirosis = pirosis;
    }
    setRegurgitacion(regurgitacion){
        this.Regurgitacion = regurgitacion;
    }
    setConstipacion(constipacion){
        this.Constipacion = constipacion;
    }
    setBolo(bolo){
        this.Bolo = bolo;
    }
    setNoTieneDigestivo(notiene){
        this.NoTieneDigestivo = notiene;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} protocolo - clave del registro
     * Método que recibe como parámetro la clave de un
     * paciente, asigna el valor en las variables de
     * clase y carga el registro en el formulario
     */
    getDatosAntecedentes(protocolo){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en la variable de clase
        this.Protocolo = protocolo;

        // obtenemos y asignamos los datos
        $.ajax({
            url: "antecedentes/getantecedentes.php?protocolo=" + this.Protocolo,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                antecedentes.cargaAntecedentes(data);

        }});

        // cargamos la grilla de visitas
        this.cargaVisitas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector con los datos del registro
     * Método llamado en la recuperación de datos que recibe
     * como parámetro el array con los datos del registro
     * y asigna estos en las variables de clase
     */
    cargaAntecedentes(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos los valores de las variables comunes
        this.setFechaAlta(datos.Alta);
        this.setUsuario(datos.Usuario);

        // de la tabla de chagas agudo
        this.setIdAgudo(datos.IdAgudo);
        this.setSintomas(datos.Sintomas);
        this.setObservaciones(datos.Observaciones);

        // de la tabla de antecedentes tóxicos
        this.setIdToxico(datos.IdToxico);
        this.setFuma(datos.Fuma);
        this.setCigarrillos(datos.Cigarrillos);
        this.setExFumador(datos.ExFumador);
        this.setDuracFumador(datos.DuracFumador);
        this.setDejoFumar(datos.DejoFumar);
        this.setAlcohol(datos.Alcohol);
        this.setLitros(datos.Litros);
        this.setBebida(datos.Bebida);
        this.setAdicciones(datos.Adicciones);

        // de la tabla de antecedentes familiares
        this.setIdFamiliar(datos.IdFamiliar);
        this.setSubita(datos.Subita);
        this.setCardiopatia(datos.Cardiopatia);
        this.setDisfagia(datos.Disfagia);
        this.setMarcapaso(datos.Marcapaso);
        this.setNoSabeFamiliar(datos.NoSabeFamiliar);
        this.setNoTieneFamiliar(datos.NoTieneFamiliar);
        this.setOtra(datos.Otra);

        // de la tabla de disautonomía
        this.setIdDisautonomia(datos.IdDisautonomia);
        this.setHipotension(datos.Hipotension);
        this.setBradicardia(datos.Bradicardia);
        this.setAstenia(datos.Astenia);
        this.setNoTieneDisauto(datos.NoTieneDisauto);

        // de la tabla de compromiso digestivo
        this.setIdDigestivo(datos.IdDigestivo);
        this.setDisfagiaDigestivo(datos.DisfagiaDigestivo);
        this.setPirosis(datos.Pirosis);
        this.setRegurgitacion(datos.Regurgitacion);
        this.setConstipacion(datos.Constipacion);
        this.setBolo(datos.Bolo);
        this.setNoTieneDigestivo(datos.NoTieneDigestivo);

        // mostramos el registro
        this.muestraAntecedentes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase carga
     * las mismas en el formulario
     */
    muestraAntecedentes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si tiene antecedentes de chagas agudo
        if (this.Sintomas == 0){
            document.getElementById("chagasagudo").checked = false;
        } else {
            document.getElementById("chagasagudo").checked = true;
        }

        // carga las observaciones
        CKEDITOR.instances['observaciones_antecedentes'].setData(this.Observaciones);

        // carga los antecedentes tóxicos
        if (this.Fuma == 0){
            document.getElementById("fuma_antecedentes").checked = false;
        } else {
            document.getElementById("fuma_antecedentes").checked = true;
        }
        document.getElementById("nro_cigarrillos").value = this.Cigarrillos;
        if (this.ExFumador == 0){
            document.getElementById("exfumador").checked = false;
        } else {
            document.getElementById("exfumador").checked = true;
        }
        document.getElementById("tiempo_fumador").value = this.DuracFumador;
        if (this.Alcohol == 0){
            document.getElementById("bebealcohol").checked = false;
        } else {
            document.getElementById("bebealcohol").checked = true;
        }
        document.getElementById("litros_bebida").value = this.Litros;
        document.getElementById("tipo_bebida").value = this.Bebida;
        document.getElementById("adicciones").value = this.Adicciones;

        // carga los antecedentes familiares
        if (this.Subita == 0){
            document.getElementById("muerte_subita").checked = false;
        } else {
            document.getElementById("muerte_subita").checked = true;
        }
        if (this.Cardiopatia == 0){
            document.getElementById("cardiopatia").checked = false;
        } else {
            document.getElementById("cardiopatia").checked = true;
        }
        if (this.Disfagia == 0){
            document.getElementById("disfagia_familiares").checked = false;
        } else {
            document.getElementById("disfagia_familiares").checked = true;
        }
        if (this.Marcapaso == 0){
            document.getElementById("marcapaso").checked = false;
        } else {
            document.getElementById("marcapaso").checked = true;
        }
        if (this.NoSabeFamiliar == 0){
            document.getElementById("nosabe_familiares").checked = false;
        } else {
            document.getElementById("nosabe_familiares").checked = true;
        }
        if (this.NoTieneFamiliar == 0){
            document.getElementById("notiene_familiares").checked = false;
        } else {
            document.getElementById("notiene_familiares").checked = true;
        }
        document.getElementById("enfermedad_cronica").value = this.Otra;

        // carga la disautonomía
        if (this.Hipotension == 0){
            document.getElementById("hipotension").checked = false;
        } else {
            document.getElementById("hipotension").checked = true;
        }
        if (this.Bradicardia == 0){
            document.getElementById("bradicardia").checked = false;
        } else {
            document.getElementById("bradicardia").checked = true;
        }
        if (this.Astenia == 0){
            document.getElementById("astenia").checked = false;
        } else {
            document.getElementById("astenia").checked = true;
        }
        if (this.NoTieneDisauto == 0){
            document.getElementById("notiene_disautonomia").checked = false;
        } else {
            document.getElementById("notiene_disautonomia").checked = true;
        }

        // carga los antecedentes digestivos
        if (this.DisfagiaDigestivo == 0){
            document.getElementById("disfagia").checked = false;
        } else {
            document.getElementById("disfagia").checked = true;
        }
        if (this.Pirosis == 0){
            document.getElementById("pirosis").checked = false;
        } else {
            document.getElementById("pirosis").checked = true;
        }
        if (this.Regurgitacion == 0){
            document.getElementById("regurgitacion").checked = false;
        } else {
            document.getElementById("regurgitacion").checked = true;
        }
        if (this.Constipacion == 0){
            document.getElementById("constipacion").checked = false;
        } else {
            document.getElementById("constipacion").checked = true;
        }
        if (this.Bolo == 0){
            document.getElementById("bolofecal").checked = false;
        } else {
            document.getElementById("bolofecal").checked = true;
        }
        if (this.NoTieneDigestivo == 0){
            document.getElementById("notiene_digestivo").checked = false;
        } else {
            document.getElementById("notiene_digestivo").checked = true;
        }

        // carga el usuario y la fecha de alta
        document.getElementById("usuario_antecedentes").value = this.Usuario;
        document.getElementById("fecha_antecedentes").value = this.FechaAlta;

        // cargamos las visitas
        this.listaVisitas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que verifica
     * los datos ingresados en el formulario
     */
    validaAntecedentes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // nos aseguramos de asignar el protocolo
        this.Protocolo = document.getElementById("protocolo_paciente").value;

        // declaración de variables
        var mensaje;
        var correcto = false;

        // fija si tiene síntomas de chagas agudo
        if (document.getElementById("chagasagudo").checked){
            this.Sintomas = 1;
        } else {
            this.Sintomas = 0;
        }

        // si declaró que fuma verifica que indique el número
        // de cigarrillos diarios
        if (document.getElementById("fuma_antecedentes").checked && document.getElementById("nro_cigarrillos").value == 0){

            // presenta el mensaje
            mensaje = "Si fuma debe indicar el número de cigarrillos";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asigna si fuma y el número de cigarrillos
        if (document.getElementById("fuma_antecedentes").checked){
            this.Fuma = 1;
        } else {
            this.Fuma = 0;
        }
        this.Cigarrillos = document.getElementById("nro_cigarrillos").value;

        // si declaró que es ex fumador verifica cuantos años
        // fumo y cuando dejó
        if (document.getElementById("exfumador").checked && document.getElementById("tiempo_fumador").value == 0){

            // presenta el mensaje
            mensaje = "Si es ex-fumador, debe indicar el tiempo<br>";
            mensaje += "que fumó y cuando dejó de fumar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asigna el tiempo y si es ex fumador
        if (document.getElementById("exfumador").checked){
            this.ExFumador = 1;
        } else {
            this.ExFumador = 0;
        }
        this.DuracFumador = document.getElementById("tiempo_fumador").value;
        this.DejoFumador = document.getElementById("fecha_fumador").value;

        // si declaró alcohol verifica que indique los litros
        // y el tipo de bebida
        if (document.getElementById("bebealcohol").checked && document.getElementById("litros_bebida").value == 0){

            // presenta el mensaje
            mensaje = "Si bebe, indique la cantidad de litros a la semana";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si bebe, verifica que seleccione el tipo de bebida
        if (document.getElementById("bebealcohol").checked && document.getElementById("tipo_bebida").value == ""){

            // presenta el mensaje
            mensaje = "Debe indicar el tipo de bebida";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asigna si bebe, los litros y el tipo de bebida
        if (document.getElementById("bebealcohol").checked){
            this.Alcohol = 1;
        } else {
            this.Alcohol = 0;
        }
        this.Litros = document.getElementById("litros_bebida").value;
        this.Bebida = document.getElementById("tipo_bebida").value;

        // adicciones lo permite en blanco
        this.Adicciones = document.getElementById("adicciones").value;

        // verifica que halla marcado al menos uno de los
        // antecedentes familiares
        correcto = false;

        // muerte súbita
        if (document.getElementById("muerte_subita").checked){
            this.Subita = 1;
            correcto = true;
        } else {
            this.Subita = 0;
        }

        // cardiopatía
        if (document.getElementById("cardiopatia").checked){
            this.Cardiopatia = 1;
            correcto = true;
        } else {
            this.Cardiopatia = 0;
        }

        // disfagia de familiares
        if (document.getElementById("disfagia_familiares").checked){
            this.Disfagia = 1;
            correcto = true;
        } else {
            this.Disfagia = 0;
        }

        // marcapaso
        if (document.getElementById("marcapaso").checked){
            this.Marcapaso = 1;
            correcto = true;
        } else {
            this.Marcapaso = 0;
        }

        // si no sabe
        if (document.getElementById("nosabe_familiares").checked){
            this.NoSabeFamiliar = 1;
            correcto = true;
        } else {
            this.NoSabeFamiliar = 0;
        }

        // si no tiene
        if (document.getElementById("notiene_familiares").checked){
            this.NoTieneFamiliar = 1;
            correcto = true;
        } else {
            this.NoTieneFamiliar = 0;
        }

        // si no marcó ninguno
        if (!correcto){

            // presenta el mensaje
            mensaje = "Debe marcar al menos un elemento de los <br>";
            mensaje += "antecedentes familiares (puede marcar No Sabe)";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // otra enfermedad crónica lo permite en blanco
        this.Otra = document.getElementById("enfermedad_cronica").value;

        // verifica que halla marcado al menos uno de los
        // antecedentes de disautonomía
        correcto = false;

        // hipotensión
        if (document.getElementById("hipotension").checked){
            this.Hipotension = 1;
            correcto = true;
        } else {
            this.Hipotension = 0;
        }

        // bradicardia
        if (document.getElementById("bradicardia").checked){
            this.Bradicardia = 1;
            correcto = true;
        } else {
            this.Bradicardia = 0;
        }

        // astenia
        if (document.getElementById("astenia").checked){
            this.Astenia = 1;
            correcto = true;
        } else {
            this.Astenia = 0;
        }

        // no tiene
        if (document.getElementById("notiene_disautonomia").checked){
            this.NoTieneDisauto = 1;
            correcto = true;
        } else {
            this.NoTieneDisauto = 0;
        }

        // si no marcó ninguno
        if (!correcto){

            // presenta el mensaje
            mensaje = "Debe marcar al menos un elemento de disautonomía<br>";
            mensaje += "(puede marcar No Tiene)";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica que halla marcado al menos uno de los
        // antecedentes digestivos
        correcto = false;

        // disfagia
        if (document.getElementById("disfagia").checked){
            this.DisfagiaDigestivo = 1;
            correcto = true;
        } else {
            this.DisfagiaDigestivo = 0;
        }

        // pirosis
        if (document.getElementById("pirosis").checked){
            this.Pirosis = 1;
            correcto = true;
        } else {
            this.Pirosis = 0;
        }

        // regurgitación
        if (document.getElementById("regurgitacion").checked){
            this.Regurgitacion = 1;
            correcto = true;
        } else {
            this.Regurgitacion = 0;
        }

        // constipación
        if (document.getElementById("constipacion").checked){
            this.Constipacion = 1;
            correcto = true;
        } else {
            this.Constipacion = 0;
        }

        // bolo fecal
        if (document.getElementById("bolofecal").checked){
            this.Bolo = 1;
            correcto = true;
        } else {
            this.Bolo = 0;
        }

        // no tiene
        if (document.getElementById("notiene_digestivo").checked){
            this.NoTieneDigestivo = 1;
            correcto = true;
        } else {
            this.NoTieneDigestivo = 0;
        }

        // si no marcó ninguno
        if (!correcto){

            // presenta el mensaje
            mensaje = "Debe marcar al menos un elemento de compromiso<br>";
            mensaje += "digestivo (puede marcar No Tiene)";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asigna las observaciones
        this.Observaciones = CKEDITOR.instances['observaciones_antecedentes'].getData();

        // grabamos el registro
        this.grabaAntecedentes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmmail.com>
     * Método llamado luego de validar el formulario que
     * ejecuta la consulta en el servidor
     */
    grabaAntecedentes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosAntecedentes = new FormData();

        // agregamos las variables comunes
        datosAntecedentes.append("Protocolo", this.Protocolo);

        // de la tabla de antecedentes de chagas agudo
        datosAntecedentes.append("IdAgudo",       this.IdAgudo);
        datosAntecedentes.append("Sintomas",      this.Sintomas);
        datosAntecedentes.append("Observaciones", this.Observaciones);

        // de la tabla de antecedentes tóxicos
        datosAntecedentes.append("IdToxico",    this.IdToxico);
        datosAntecedentes.append("Fuma",        this.Fuma);
        datosAntecedentes.append("Cigarrillos", this.Cigarrillos);
        datosAntecedentes.append("ExFumador",   this.ExFumador);
        datosAntecedentes.append("Duracion",    this.DuracFumador);
        datosAntecedentes.append("DejoFumador", this.DejoFumador);
        datosAntecedentes.append("Alcohol",     this.Alcohol);
        datosAntecedentes.append("Litros",      this.Litros);
        datosAntecedentes.append("Bebida",      this.Bebida);
        datosAntecedentes.append("Adicciones",  this.Adicciones);

        // de la tabla de antecedentes familiares
        datosAntecedentes.append("IdFamiliar",      this.IdFamiliar);
        datosAntecedentes.append("Subita",          this.Subita);
        datosAntecedentes.append("Cardiopatia",     this.Cardiopatia);
        datosAntecedentes.append("Disfagia",        this.Disfagia);
        datosAntecedentes.append("Marcapaso",       this.Marcapaso);
        datosAntecedentes.append("NoSabeFamiliar",  this.NoSabeFamiliar);
        datosAntecedentes.append("NoTieneFamiliar", this.NoTieneFamiliar);
        datosAntecedentes.append("OtraCronica",     this.Otra);

        // de la tabla de disautonomía
        datosAntecedentes.append("IdDisautonomia", this.IdDisautonomia);
        datosAntecedentes.append("Hipotension",    this.Hipotension);
        datosAntecedentes.append("Bradicardia",    this.Bradicardia);
        datosAntecedentes.append("Astenia",        this.Astenia);
        datosAntecedentes.append("NoTieneDisauto", this.NoTieneDisauto);

        // de la tabla de compromiso digestivo
        datosAntecedentes.append("IdDigestivo",       this.IdDigestivo);
        datosAntecedentes.append("DisfagiaDigestivo", this.DisfagiaDigestivo);
        datosAntecedentes.append("Pirosis",           this.Pirosis);
        datosAntecedentes.append("Regurgitacion",     this.Regurgitacion);
        datosAntecedentes.append("Constipacion",      this.Constipacion);
        datosAntecedentes.append("Bolo",              this.Bolo);
        datosAntecedentes.append("NoTieneDigestivo",  this.NoTieneDigestivo);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "antecedentes/graba_antecedentes.php",
            type: "POST",
            data: datosAntecedentes,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    // valores de las claves
                    antecedentes.setIdAgudo(data.IdAgudo);
                    antecedentes.setIdToxico(data.IdToxico);
                    antecedentes.setIdFamiliar(data.IdFamiliar);
                    antecedentes.setIdDisautonomia(data.IdDisautonomia);
                    antecedentes.setIdDigestivo(data.IdDigestivo);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de datos
     */
    limpiaFormulario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializa las variables de clase
        this.initAntecedentes();

        // seteamos las variables de chagas agudo
        document.getElementById("chagasagudo").checked = false;
        CKEDITOR.instances['observaciones_antecedentes'].setData();
        document.getElementById("fuma_antecedentes").checked = false;
        document.getElementById("nro_cigarrillos").value = 0;
        document.getElementById("exfumador").checked = false;
        document.getElementById("tiempo_fumador").value = 0;
        document.getElementById("fecha_fumador").value = "";
        document.getElementById("bebealcohol").checked = false;
        document.getElementById("litros_bebida").value = 0;
        document.getElementById("tipo_bebida").value = "";
        document.getElementById("adicciones").value = "";
        document.getElementById("muerte_subita").checked = false;
        document.getElementById("cardiopatia").checked = false;
        document.getElementById("disfagia_familiares").checked = false;
        document.getElementById("marcapaso").checked = false;
        document.getElementById("nosabe_familiares").checked = false;
        document.getElementById("notiene_familiares").checked = false;
        document.getElementById("enfermedad_cronica").value = "";
        document.getElementById("hipotension").checked = false;
        document.getElementById("bradicardia").checked = false;
        document.getElementById("astenia").checked = false;
        document.getElementById("notiene_disautonomia").checked = false;
        document.getElementById("disfagia").checked = false;
        document.getElementById("pirosis").checked = false;
        document.getElementById("regurgitacion").checked = false;
        document.getElementById("constipacion").checked = false;
        document.getElementById("bolofecal").checked = false;
        document.getElementById("notiene_digestivo").checked = false;

        // fijamos el usuario y la fecha de alta
        document.getElementById("usuario_antecedentes").value = sessionStorage.getItem("Usuario");
        document.getElementById("fecha_antecedentes").value = fechaActual();

        // limpiamos la grilla de visitas
        var tabla = $('#grilla_visitas').DataTable();
        tabla.clear();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de cargar los datos en el formulario
     * que completa la grilla de las visitas (puede ser llamado
     * también luego del alta de una visita)
     */
    listaVisitas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos el vector
        $.ajax({
            url: "visitas/nominavisitas.php?protocolo=" + this.Protocolo,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector
                antecedentes.cargaVisitas(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de obtener la nómina de visitas
     * que recibe como parámetro un array y completa la
     * grilla de las visitas
     */
    cargaVisitas(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // verificamos que halla recibido un vector
        if (typeof(datos) == "undefined"){
            return;
        }

        // declaración de variables
        var enlace = "";
        var elimina = "";
        var auscultacion = "";
        var soplos = "";

        // limpiamos la tabla y luego recargamos
        var tabla = $('#grilla_visitas').DataTable();
        tabla.clear();

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // armamos el enlace
            enlace =  "<input type='button' ";
            enlace += "       name='btnEditar' ";
            enlace += "       class='botoneditar' ";
            enlace += "       title='Pulse para ver la visita' ";
            enlace += "       onClick='visitas.editaVisita(" + datos[i].id + ")';>";

            // el enlace de eliminación
            elimina =  "<input type='button' ";
            elimina += "       name='btnBorrar' ";
            elimina += "       class='botonborrar' ";
            elimina += "       title='Pulse para eliminar la visita' ";
            elimina += "       onClick='visitas.borraVisita(" + datos[i].id + ")';>";

            // según el valor de soplos y auscultación
            if (datos[i].auscultacion == 0){
                auscultacion = "No";
            } else {
                auscultacion = "Si";
            }
            if (datos[i].soplos == 0){
                soplos = "No";
            } else {
                soplos = "Si";
            }

            // agregamos el registro
            tabla.row.add( [
                datos[i].fecha,
                datos[i].estadio,
                datos[i].peso,
                auscultacion,
                soplos,
                enlace,
                elimina
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * recarga el formulario
     */
    cancelaAntecedentes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // pide confirmación
        var Confirmacion = new jBox('Confirm', {
                animation: 'flip',
                title: 'Cancelar Cambios',
                closeOnEsc: false,
                closeOnClick: false,
                closeOnMouseleave: false,
                closeButton: true,
                onCloseComplete: function(){
                    this.destroy();
                },
                overlay: false,
                content: 'Se perderán los cambios, está seguro?',
                theme: 'TooltipBorder',
                confirm: function() {

                    // destruimos el diálogo y cargamos
                    antecedentes.getDatosAntecedentes(this.Protocolo);

                },
                cancel: function(){
                    Confirmacion.destroy();
                },
                confirmButton: 'Aceptar',
                cancelButton: 'Cancelar'

        });

    }

}