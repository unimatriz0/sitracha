<?php

/**
 *
 * localidades/verifica_localida.class.php
 *
 * @package     Diagnostico
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una provincia y una 
 * cadena con el nombre de la localidad, verifica que no se 
 * encuentre repetido, retorna el número de registros 
 * encontrados
 * 
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// verificamos si existe
$resultado = $localidades->existeLocalidad($_GET["provincia"], $_GET["localidad"]);

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));

?>