<?php

/**
 *
 * localidades/form_localidades.class.php
 *
 * @package     Diagnostico
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario de localidades presentando 
 * el combo con la nómina en la zona superior
 * 
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// obtenemos la nómina
$nomina = $localidades->listaLocalidades($_GET["jurisdiccion"]);

// definimos la tabla
echo "<table width='70%' align='center' border='0'>";

// presenta los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Cod.</th>";
echo "<th align='left'>Localidad</th>";
echo "<th>Población</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos la clave
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Código de Georreferenciación'>";
    echo "<input type='text'
           name='codloc'
           id='codloc_$idlocalidad'
           size='9'
           value='$idlocalidad'>";
    echo "</span>";
    echo "</td>";

    // presenta el nombre
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre de la Localidad'>";
    echo "<input type='text'
           name='nombrelocalidad'
           id='localidad_$idlocalidad'
           size='40'
           value='$nombre_localidad'>";
    echo "</span>";
    echo "</td>";

    // pide la población
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Cantidad de Habitantes'>";
    echo "<input type='number'
           name='poblacion'
           id='poblacion_$idlocalidad'
           class='numerogrande'
           value='$poblacion'>";
    echo "</span>";
    echo "</td>";

    // presenta el usuario
    echo "<td align='center'>$usuario</td>";

    // arma el enlace
    echo "<td>";
    echo "<input type='button' name='btnGrabaLocalidad'
           id='btnGrabaLocalidad'
           title='Pulse para grabar el registro'
           onClick='ciudades.verificaLocalidad(" . chr(34) . $idlocalidad . chr(34) . ")'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// agregamos la última fila
echo "<tr>";

// presentamos la clave
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Código de Georreferenciación'>";
echo "<input type='text'
        name='codloc'
        id='codloc_nueva'
        size='9'>";
echo "</span>";
echo "</td>";

// presenta el nombre
echo "<td>";
echo "<span class='tooltip'
            title='Nombre de la Localidad'>";
echo "<input type='text'
        name='nombrelocalidad'
        id='localidad_nueva'
        size='40'>";
echo "</span>";
echo "</td>";

// pide la población
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Cantidad de Habitantes'>";
echo "<input type='number'
        name='poblacion'
        id='poblacion_nueva'
        class='numerogrande'>";
echo "</span>";
echo "</td>";

// presenta el usuario
echo "<td></td>";

// arma el enlace
echo "<td>";
echo "<input type='button' name='btnGrabaLocalidad'
        id='btnGrabaLocalidad'
        title='Pulse para grabar el registro'
        onClick='ciudades.verificaLocalidad()'
        class='botongrabar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody></table>";

?>
<SCRIPT>

        // instanciamos los tooltips
        new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
        });

</SCRIPT>
