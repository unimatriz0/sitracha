/*
 * Nombre: localidades.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 10/01/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de localidades
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre localidades
 */
class Localidades {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initLocalidades();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicia las variables de clase
     */
    initLocalidades(){

        // declaración de variables
        this.Id = 0;
        this.CodLoc = "";
        this.Poblacion = 0;
        this.Localidad = "";
        this.Provincia = "";
        this.CodProv = "";
        this.Pais = "";
        this.IdPais = 0;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario en el contenido de la página
     */
    muestraLocalidades(){

        // carga el formulario
        $("#form_administracion").load("localidades/form_localidades.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Propósito: Método llamado en el evento onchange del combo de jurisdicciones
     *            obtiene la id de la jurisdicción y llama por ajax la rutina
     *            php para cargar en el div la nómina de localidades de esa
     *            jurisdicción
     */
    listaLocalidades(){

        // obtiene la id
        var provincia = document.getElementById("provincia_localidad").value;

        // si está seleccionado el primer elemento
        if (provincia === ""){
            return false;
        }

        // cargamos el formulario de localidades
        $("#localidades").load("localidades/nomina_localidades.php?provincia="+provincia);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * @return [boolean] - resultado de la operación
     * Método que verifica el formulario de localidades
     */
    verificaLocalidad(id){

        // declaración de variables
        var mensaje;

        // si está dando un alta
        if (id === undefined){
            id = "nueva";
        }

        // asignamos en la variable de clase
        this.Id = id;

        // obtenemos los valores del formulario
        this.CodLoc = document.getElementById("codloc_" + id).value;
        this.Localidad = document.getElementById("localidad_" + id).value;
        this.Poblacion = document.getElementById("poblacion_" + id).value;
        this.Provincia = document.getElementById("provincia_localidad").value;

        // verifica el código
        if (this.CodLoc == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Código Indec de la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("codloc_" + id).focus();
            return false;

        }

        // verifica el nombre
        if (this.Localidad == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("localidad_" + id).focus();
            return false;

        }

        // verifica la población
        if (this.Poblacion == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Código Indec de la localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("poblacion_" + id).focus();
            return false;

        }

        // si está dando un alta verifica que no esté repetida
        if (this.Id === "nueva"){

            // verifica que no esté repetida
            if (this.validaLocalidad()){

                // presenta el mensaje y retorna
                mensaje = "Esa Localidad ya se encuentra declarada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("valor_" + id).focus();
                return false;

            }

        }

        // grabamos el registro
        this.grabaLocalidad();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} - resultado de la operación
     * Método llamado en el alta de localidades, verifica que no se encuentre
     * ya declarada para evitar repeticiones
     */
    validaLocalidad(){

        // declaración de variables
        var resultado;

        // llamamos sincrónicamente
        $.ajax({
            url: 'localidades/verifica_localidad.php?provincia='+this.Povincia+'&localidad='+this.Localidad,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error === true){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario, compone el objeto
     * formdata y lo envía por ajax al servidor
     */
    grabaLocalidad(){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id === "nueva"){
            formulario.append("evento", "insertar");
        } else {
            formulario.append("evento", "editar");
        }

        // agrega los valores
        formulario.append("CodLoc", this.CodLoc);
        formulario.append("Provincia", this.Provincia);
        formulario.append("Localidad", this.Localidad);
        formulario.append("Poblacion", this.Poblacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "localidades/graba_localidad.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // inicializa las variables de clase
                    localidades.initLocalidades();

                    // recarga el formulario para reflejar los cambios
                    ciudades.listaLocalidades();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - select del formulario
     * @param {string} idprovincia - clave de la jurisdicción
     * @param {string} idlocalidad - clave de la localidad (puede ser nulo)
     * Método que recibe como parámetro la id del select de un formulario, la clave
     * indec de la jurisdicción y (optativo) la clave de la jurisdicción
     * preseleccionada, carga en el select recibido como argumento la nómina
     * de localidades correspondientes a la jurisdicción y en todo caso
     * preselecciona la recibida
     */
    nominaLocalidades(idelemento, idprovincia, idlocalidad){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto al combo
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // si hay alguna jurisdicción seleccionada
        if (idprovincia != 0){

            // lo llamamos sincrónico
            $.ajax({
                url: "localidades/lista_localidad.php?Provincia="+idprovincia,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        // si recibió la clave de la localidad
                        if (typeof(idlocalidad) != "undefined"){

                            // verifica y si es igual preselecciona
                            if (data[i].Id == idlocalidad){
                                $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Localidad + "</option>");
                            } else {
                                $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Localidad + "</option>");
                            }

                        // si no recibió nada
                        } else {
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Localidad + "</option>");
                        }
                    }

            }});

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el select de países del formulario de abm
     * de localidades
     */
    cargaPaises(){

        // llamamos la clase javascript
        paises.listaPaises("pais_localidad");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del select de países que carga
     * el select de jurisdicciones de ese país
     */
    cargaJurisdiccion(){

        // obtenemos la id de país
        var idpais = document.getElementById("pais_localidad").value;

        // si no seleccionó ninguno
        if (idpais == 0){
            return;
        }

        // llamamos la clase de provincias
        provincias.listaJurisdicciones("provincia_localidad", idpais);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del select de jurisdicciones en el
     * formulario de abm de localidades, verifica el valor seleccionado
     * y carga en el div la nómina de localidades
     */
    cargaCiudades(){

        // obtenemos el valor seleccionado
        var idjurisdiccion = document.getElementById("provincia_localidad").value;

        // si no seleccionó nada
        if (idjurisdiccion == 0 || idjurisdiccion == ""){
            return;
        }

        // cargamos en el div la nómina
        $("#nominaCiudades").load("localidades/form_localidades.php?jurisdiccion="+idjurisdiccion);

    }

}
