<?php

/**
 *
 * localidades/nomina_localidades.class.php
 *
 * @package     Diagnostico
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe como parámetro la clave de una jurisdicción 
 * y arma el formulario de abm de localidades de esa provincia
 * 
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// obtenemos la nómina
$nomina_localidades = $localidades->listaLocalidades($_GET["provincia"]);

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos los encabezados a dos columnas
echo "<thead>";
echo "<tr>";

// la primer columna
echo "<th>COD</th>";
echo "<th>Localidad</th>";
echo "<th>Población</th>";
echo "<th></th>";

// la segunda columna
echo "<th>COD</th>";
echo "<th>Localidad</th>";
echo "<th>Población</th>";
echo "<th></th>";

// cerramos la fila y el encabezado
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// iniciamos el contador de columnas
$columna = 1;

// recorremos el vector
foreach($nomina_localidades AS $registro){

    // obtenemos el registro
    extract($registro);

    // si estamos en la primer columna
    if ($columna == 1){

        // abrimos la fila
        echo "<tr>";

    }

    // presentamos el código de localidad
    echo "<td>";
    echo "<input type='text'
           name='codloc'
           id='codloc_$idlocalidad'
           value='$idlocalidad'
           title='Código de la Localidad'
           size='8'>";
    echo "</td>";

    // presentamos el nombre
    echo "<td>";
    echo "<input type='text'
           name='localidad'
           id='localidad_$idlocalidad'
           value='$nombre_localidad'
           title='Nombre de la Localidad'
           size='25'>";
    echo "</td>";

    // la población
    echo "<td>";
    echo "<input type='number'
           name='poblacion'
           id='poblacion_$idlocalidad'
           class='numerogrande'
           value='$poblacion'
           title='Número de Habitantes'
           size='10'>";
    echo "</td>";

    // presenta el botón grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaLocalidad'
           id='btnGrabaLocalidad'
           title='Pulse para grabar el registro'
           onClick='ciudades.verificaLocalidad(" . chr(34) . $idlocalidad . chr(34) . ")'
           class='botongrabar'>";
    echo "</td>";

    // incrementamos el contador de columnas
    $columna++;

    // si ya presentamos la segunda columna
    if ($columna > 2){

        // cerramos la fila y reiniciamos el contador
        echo "</tr>";
        $columna = 1;

    }

}

// agregamos el formulario para insertar una nueva localidad

// si estamos en la primer columna
if ($columna == 1){

    // abrimos la fila
    echo "<tr>";

}

// presentamos el código de localidad
echo "<td>";
echo "<input type='text'
       name='codloc'
       id='codloc_nueva'
       title='Código de la Localidad'
       size='8'>";
echo "</td>";

// presentamos el nombre
echo "<td>";
echo "<input type='text'
       name='localidad'
       id='localidad_nueva'
       title='Nombre de la Localidad'
       size='25'>";
echo "</td>";

// la población
echo "<td>";
echo "<input type='number'
       name='poblacion'
       id='poblacion_nueva'
       class='numerogrande'
       title='Número de Habitantes'
       size='10'>";
echo "</td>";

// presenta el botón agregar
echo "<td align='center'>";
echo "<input type='button' name='btnNuevaLocalidad'
       id='btnNuevaLocalidad'
       onClick='ciudades.verificaLocalidad()'
       class='botonagregar'>";
echo "</td>";

// siempre cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>