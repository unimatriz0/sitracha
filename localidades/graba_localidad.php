<?php

/**
 *
 * localidades/graba_localidad.php
 *
 * @package     Diagnostico
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta 
 * la consulta en el servidor, retorna el resultado de la 
 * operación 
 * 
*/

// incluimos e instanciamos las clases
require_once ("localidades.class.php");
$localidades = new Localidades();

// asignamos los valores en la clase
$localidades->setIdProvincia($_POST["Provincia"]);
$localidades->setIdLocalidad($_POST["CodLoc"]);
$localidades->setNombreLocalidad($_POST["Localidad"]);
$localidades->setPoblacionLocalidad($_POST["Poblacion"]);

// ejecutamos la consulta pasándole como argumento la acción
$resultado = $localidades->grabaLocalidad($_POST["evento"]);

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));

?>