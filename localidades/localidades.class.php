<?php

/**
 *
 * Class Localidades | localidades/localidades.class.php
 *
 * @package     Diagnostico
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de localidades
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Localidades{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $IdProvincia;           // clave indec de la provincia
    protected $NombreProvincia;       // nombre de la provincia
    protected $FechaAlta;             // fecha de alta del registro
    protected $NombreLocalidad;       // nombre de la lodiccionarios
    protected $IdLocalidad;           // clave indec de la localidad
    protected $PoblacionLocalidad;    // poblacion de la localidad
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdProvincia = "";
        $this->NombreProvincia = "";
        $this->IdLocalidad = "";
        $this->NombreLocalidad = "";
        $this->PoblacionLocalidad = 0;

        // inicializamos la fecha de alta en formato mysql
        $this->FechaAlta = date('Y/m/d');

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setNombreProvincia($provincia){
        $this->NombreProvincia = $provincia;
    }
    public function setIdProvincia($idprovincia){
        $this->IdProvincia = $idprovincia;
    }
    public function setNombreLocalidad($localidad){
        $this->NombreLocalidad = $localidad;
    }
    public function setIdLocalidad($idlocalidad){
        $this->IdLocalidad = $idlocalidad;
    }
    public function setPoblacionLocalidad($poblacion){

        // verifica que sea un número
        if (!is_numeric($poblacion)){

            // abandona por error
            echo "La poblacion de la lodiccionarios debe ser un número";
            exit;

            // si está correcto
        } else {

            // lo asigna
            $this->PoblacionLocalidad = $poblacion;

        }

    }

    /**
     * Método utilizado en el formulario de pacientes, recibe como parámetro
     * parte del nombre de la localidad y retorna un array con los ciudades
     * coincidentes, el país y la provinvia
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $localidad - nombre de la localidad a buscar
     * @return array
     */
    public function nominaLocalidades($localidad){

        // componemos la consulta
        $consulta = "SELECT diccionarios.v_localidades.pais AS pais,
                            diccionarios.v_localidades.provincia AS provincia,
                            diccionarios.v_localidades.localidad AS localidad,
                            diccionarios.v_localidades.codloc AS codloc
                     FROM diccionarios.v_localidades
                     WHERE diccionarios.v_localidades.localidad LIKE '%$localidad%'
                     ORDER BY diccionarios.v_localidades.pais,
                              diccionarios.v_localidades.provincia,
                              diccionarios.v_localidades.localidad;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que recibe como parámetro la id de una provincia o el nombre
     * y retorna la nómina de las localidades en esa provincia junto con su clave
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la provincia
     * @return array
     */
    public function listaLocalidades($provincia = false){

        // verifica haber recibido un valor
        if (!$provincia){

            // presenta el mensaje y abandona
            echo "No se ha recibido una jurisdicción";
            exit;

        }

        // componemos la consulta
        $consulta = "SELECT diccionarios.localidades.CODLOC AS idlocalidad,
                            UPPER(diccionarios.localidades.NOMLOC) AS nombre_localidad,
                            diccionarios.localidades.POBLACION AS poblacion,
                            cce.responsables.USUARIO AS usuario,
                            DATE_FORMAT(diccionarios.localidades.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta
                     FROM diccionarios.localidades INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                                                   INNER JOIN cce.responsables ON diccionarios.localidades.USUARIO = cce.responsables.ID
                     WHERE diccionarios.provincias.COD_PROV = '$provincia' OR
                           diccionarios.provincias.NOM_PROV = '$provincia'
                     ORDER BY diccionarios.localidades.NOMLOC; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaLocalidades = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nominaLocalidades;

    }

    /**
     * Método que recibe como parámetros la clave indec de la provincia y
     * el nombre de la localidad, retorna verdadero si existe esa localidad
     * en esa provincia, caso contrario falso, usado en el abm de localidad
     * para evitar registros duplicados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la provincia
     * @param string $localidad - nombre de la localidad
     * @return boolean
     */
    public function existeLocalidad($provincia, $localidad){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diccionarios.localidades.ID) AS registros
                     FROM diccionarios.localidades
                     WHERE diccionarios.localidades.CODPCIA = '$provincia' AND
                           diccionarios.localidades.NOMLOC = '$localidad';";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // si encontró
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método que recibe como parámetro la acción a realizar (insertar o
     * editar) compone la consulta y ejecuta sobre la tabla de los diccionarios
     * Retorna el resultado de la operación
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $evento - tipo de evento (inserción / edición)
     * @return boolean
     */
    public function grabaLocalidad($evento){

        // si está insertando
        if ($evento == "insertar"){

            // insertamos el registro
            $resultado = $this->nuevaLocalidad();

            // si está editando
        } else {

            // editamos el registro
            $resultado = $this->editaLocalidad();

        }

        // retornamos el estado de la operación
        return $resultado;

    }

    /**
     * Método protegido que inserta un nuevo registro en la tabla
     * de localidades, retorna el resultado de la operación
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaLocalidad(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diccionarios.localidades
                            (CODPCIA,
                             NOMLOC,
                             CODLOC,
                             POBLACION,
                             USUARIO,
                             FECHA_ALTA)
                            VALUES
                            (:codpcia,
                             :localidad,
                             :codloc,
                             :poblacion,
                             :usuario,
                             :fecha_alta);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":codpcia", $this->IdProvincia);
        $psInsertar->bindParam(":localidad", $this->NombreLocalidad);
        $psInsertar->bindParam(":codloc", $this->IdLocalidad);
        $psInsertar->bindParam(":poblacion", $this->PoblacionLocalidad);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // retornamos el número de registros
        return $resultado;

    }

    /**
     * Método protegido que edita el registro de localidades
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int el número de registros afectados
     */
    protected function editaLocalidad(){

        // compone la consulta de edición
        $consulta = "UPDATE diccionarios.localidades SET
                            CODPCIA = :codpcia,
                            NOMLOC = :localidad,
                            POBLACION = :poblacion,
                            USUARIO = :usuario,
                            FECHA_ALTA = :fecha_alta
                     WHERE diccionarios.localidades.CODLOC = :codloc;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":codpcia", $this->IdProvincia);
        $psInsertar->bindParam(":localidad", $this->NombreLocalidad);
        $psInsertar->bindParam(":poblacion", $this->PoblacionLocalidad);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);
        $psInsertar->bindParam(":codloc", $this->IdLocalidad);

        // ejecutamos la consulta
        $resultado = $psInsertar->execute();

        // retornamos el número de registros
        return $resultado;

    }

}
?>