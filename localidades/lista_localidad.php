<?php

/**
 *
 * localidades/lista_localidad.php
 *
 * @package     Diagnostico
 * @subpackage  Localidades
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/11/2011)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe la clave de una provincia y retorna un 
 * array json con las localidades de esa jurisdicción 
 * 
*/

// inclusión de archivos
require_once ("localidades.class.php");

// instancia la clase
$localidades = new Localidades();

// obtiene la nómina de localidades
$resultado = $localidades->listaLocalidades($_GET["Provincia"]);

// inicializa las variables
$jsondata = array();

// inicia un bucle recorriendo el vector
foreach ($resultado AS $registro){

    // obtiene el registro
    extract($registro);

    // lo agrega a la matriz
    $jsondata[] = array("Id" => $idlocalidad,
                        "Localidad" => $nombre_localidad);

}

// devuelve la cadena
echo json_encode($jsondata);

?>
