<?php

/**
 *
 * Class Congenito | congenito/congenito.class.php
 *
 * @package     Diagnostico
 * @subpackage  Congenito
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de 
 * Chagas Congénito
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Congenito {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // de la tabla de chagas congénito
    protected $IdCongenito;            // clave del registro
    protected $Protocolo;              // protocolo del registro padre
    protected $IdMadre;                // clave del registro de la madre
    protected $IdSivila;               // clave de la denuncia al sivila
    protected $Reportado;              // fecha en que fue reportado
    protected $Parto;                  // tipo de parto
    protected $Peso;                   // peso al nacer(en gramos)
    protected $Prematuro;              // si fue o no prematuro
    protected $IdInstitucion;          // clave de la institución de nacimiento
    protected $Institucion;            // nombre de la institucion
    protected $IdUsuario;              // usuario que ingresó el registro
    protected $Usuario;                // nombre del usuario
    protected $Fecha;                  // fecha de alta del registro
    protected $Comentarios;            // observaciones y comentarios

    /**
     * Constructor de la clase, establece la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicialización de variables
        $this->IdCongenito = 0;
        $this->Protocolo = 0;
        $this->IdMadre = 0;
        $this->IdSivila = "";
        $this->Reportado = "";
        $this->Parto = "";
        $this->Peso = "";
        $this->Prematuro = "";
        $this->IdInstitucion = 0;
        $this->Institucion = "";
        $this->Usuario = "";
        $this->Fecha = "";
        $this->Comentarios = "";

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación de valores
    public function setIdCongenito($idcongenito){
        $this->IdCongenito = $idcongenito;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setIdMadre($idmadre){
        $this->IdMadre = $idmadre;
    }
    public function setIdSivila($idsivila){
        $this->IdSivila = $idsivila;
    }
    public function setReportado($reportado){
        $this->Reportado = $reportado;
    }
    public function setParto($parto){
        $this->Parto = $parto;
    }
    public function setPeso($peso){
        $this->Peso = $peso;
    }
    public function setPrematuro($prematuro){
        $this->Prematuro = $prematuro;
    }
    public function setIdInstitucion($idinstitucion){
        $this->IdInstitucion = $idinstitucion;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }

    // métodos públicos de retorno de valores
    public function getIdCongenito(){
        return $this->IdCongenito;
    }
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getIdMadre(){
        return $this->IdMadre;
    }
    public function getIdSivila(){
        return $this->IdSivila;
    }
    public function getReportado(){
        return $this->Reportado;
    }
    public function getParto(){
        return $this->Parto;
    }
    public function getPeso(){
        return $this->Peso;
    }
    public function getPrematuro(){
        return $this->Prematuro;
    }
    public function getIdInstitucion(){
        return $this->IdInstitucion;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }

    /**
     * Método que recibe como parámetro la clave del registro y asigna
     * en la variable de clase los valores de la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $clave - clave del registro a obtener
     * @return array
     */
    public function getDatosCongenito($clave){

        // inicializamos las variables
        $id_congenito = 0;
        $id_protocolo = 0;
        $id_madre = 0;
        $id_sivila = 0;
        $reportado = "";
        $parto = "";
        $peso = 0;
        $prematuro = "";
        $id_institucion = 0;
        $institucion = "";
        $usuario = "";
        $fecha_alta = "";
        $comentarios = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_congenito.id_congenito AS id_congenito,
                            diagnostico.v_congenito.id_protocolo AS id_protocolo,
                            diagnostico.v_congenito.id_madre AS id_madre,
                            diagnostico.v_congenito.id_sivila AS id_sivila,
                            diagnostico.v_congenito.reportado AS reportado,
                            diagnostico.v_congenito.parto AS parto,
                            diagnostico.v_congenito.peso AS peso,
                            diagnostico.v_congenito.prematuro AS prematuro,
                            diagnostico.v_congenito.id_institucion AS id_institucion,
                            diagnostico.v_congenito.institucion AS institucion,
                            diagnostico.v_congenito.usuario AS usuario,
                            diagnostico.v_congenito.fecha_alta AS fecha_alta,
                            diagnostico.v_congenito.comentarios AS comentarios
                     FROM diagnostico.v_congenito
                     WHERE diagnostico.v_congenito.id_congenito = '$clave';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // asignamos en las variables de clase
        $this->IdCongenito = $id_congenito;
        $this->Protocolo = $id_protocolo;
        $this->IdMadre = $id_madre;
        $this->IdSivila = $id_sivila;
        $this->Reportado = $reportado;
        $this->Parto = $parto;
        $this->Peso = $peso;
        $this->Prematuro = $prematuro;
        $this->IdInstitucion = $id_institucion;
        $this->Institucion = $institucion;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha_alta;
        $this->Comentarios = $comentarios;

    }

    /**
     * Método público que ejecuta la consulta de inserción o edición
     * de la tabla según corresponda, retorna la clave del registro
     * afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idcongenito - clave del registro insertado / editado
     */
    public function grabaCongenito(){

        // si está dando un alta
        if ($this->IdCongenito == 0){
            $this->nuevoCongenito();
        } else {
            $this->editaCongenito();
        }

        // retornamos la id
        return $this->IdCongenito;

    }

    /**
     * Método protegido que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoCongenito(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.congenito
                            (id_protocolo,
                             id_madre,
                             id_sivila,
                             reportado,
                             parto,
                             peso,
                             prematuro,
                             institucion,
                             id_usuario,
                             comentarios)
                            VALUES
                            (:id_protocolo,
                             :id_madre,
                             :id_sivila,
                             STR_TO_DATE(:reportado, '%d/%m/%Y'),
                             :parto,
                             :peso,
                             :prematuro,
                             :institucion,
                             :id_usuario,
                             :comentarios);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_madre", $this->IdMadre);
        $psInsertar->bindParam(":id_sivila", $this->IdSivila);
        $psInsertar->bindParam(":reportado", $this->Reportado);
        $psInsertar->bindParam(":parto", $this->Parto);
        $psInsertar->bindParam(":peso", $this->Peso);
        $psInsertar->bindParam(":prematuro", $this->Prematuro);
        $psInsertar->bindParam(":institucion", $this->IdInstitucion);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdCongenito = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaCongenito(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.congenito SET
                            id_protocolo = :id_protocolo,
                            id_madre = :id_madre,
                            id_sivila = :id_sivila,
                            reportado = STR_TO_DATE(':reportado, '%d/%m/%Y'),
                            parto = :parto,
                            peso = :peso,
                            prematuro = :prematuro,
                            institucion = :institucion,
                            id_usuario = :id_usuario,
                            comentarios = :comentarios
                     WHERE diagnostico.congenito.id = :id_congenito;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_madre", $this->IdMadre);
        $psInsertar->bindParam(":id_sivila", $this->IdSivila);
        $psInsertar->bindParam(":reportado", $this->Reportado);
        $psInsertar->bindParam(":parto", $this->Parto);
        $psInsertar->bindParam(":peso", $this->Peso);
        $psInsertar->bindParam(":prematuro", $this->Prematuro);
        $psInsertar->bindParam(":institucion", $this->IdInstitucion);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":id_congenito", $this->IdCongenito);

        // ejecutamos la edición
        $psInsertar->execute();

    }

}