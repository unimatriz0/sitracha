/*

    Nombre: congenito.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 06/02/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de chagas congenito

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de chagas conténito
 */
class Congenito {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // layer emergente
        this.layerCongenito = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que muestra el formulario de chagas congénito
     */
    verCongenito(){

        // declaración de variables
        var mensaje;

        // verifica que exista un registro activo
        if (document.getElementById("protocolo_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe tener en pantalla un paciente antes<br>";
            mensaje += "de declarar datos de Chagas Congénito";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si hay un paciente en pantalla verifica la edad
        } else if (document.getElementById("edad_paciente").value > 3){

            // presenta el mensaje y retorna
            mensaje = "Verifique la edad del paciente antes de<br>";
            mensaje += "declarar datos de Chagas Congénito";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // cargamos el formulario en una ventana emergente
        this.layerCongenito = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Chagas Congénito',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'congenito/congenito.html',
                        reload: 'strict'
                    }
        });
        this.layerCongenito.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} Protocolo - clave del paciente
     * Método que recibe como parámetro la clave de un paciente y
     * obtiene los datos del mismo para después mostrarlo
     */
    getDatosCongenito(Protocolo){

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase muestra el
     * registro en el formulario
     */
    muestraCongenito(){

    }

}