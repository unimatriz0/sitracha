<?php

/**
 *
 * Class Derivacion | derivacion/derivacion.class.php
 *
 * @package     Diagnostico
 * @subpackage  Derivacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * tipos de derivacion
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Derivacion {

    // declaración de las variables de clase
    protected $Link;                  // puntero a la base de datos
    protected $Id;                    // clave del registro
    protected $Derivacion;            // tipo de derivación
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Fecha;                 // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();
        $this->Id = 0;
        $this->Derivacion = "";
        $this->Usuario = "";
        $this->Fecha = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    // métodos de asignación de variables
    public function setId($id){
        $this->Id = $id;
    }
    public function setDerivacion($derivacion){
        $this->Derivacion = $derivacion;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getDerivacion(){
        return $this->Derivacion;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idderivacion entero con la clave del registro
     * Método que compone la consulta y asigna a las variables
     * de clase los valores del registro
     */
    public function getDatosDerivacion($idderivacion){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_derivacion.id AS id,
                            diagnostico.v_derivacion.derivacion AS derivacion,
                            diagnostico.v_derivacion.usuario AS usuario,
                            diagnostico.v_derivacion.fecha AS fecha
                     FROM diagnostico.v_derivacion
                     WHERE diagnostico.v_derivacion.id = '$idderivacion'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y asignamos en las
        // variables de clase
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Derivacion = $derivacion;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con los registros
     * Método que retorna el vector con el diccionario de
     * tipos de derivación
     */
    public function nominaDerivacion(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_derivacion.id AS id,
                            diagnostico.v_derivacion.derivacion AS derivacion,
                            diagnostico.v_derivacion.usuario AS usuario,
                            diagnostico.v_derivacion.fecha AS fecha
                     FROM diagnostico.v_derivacion
                     ORDER BY diagnostico.v_derivacion.derivacion; ";

        // ejecutamos la consulta y retornamos el vector
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public function grabaDerivacion(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaDerivacion();
        } else {
            $this->editaDerivacion();
        }

        // retornamos la clave
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevaDerivacion(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.derivacion
                            (derivacion,
                             usuario)
                            VALUES
                            (:derivacion,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":derivacion", $this->Derivacion);
        $psInsertar->bindParam(":usuario",    $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaDerivacion(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.derivacion SET
                            derivacion = :derivacion,
                            usuario = :usuario
                     WHERE diagnostico.derivacion.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":derivacion", $this->Derivacion);
        $psInsertar->bindParam(":usuario",    $this->IdUsuario);
        $psInsertar->bindParam(":id",         $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idderivacion clave del registro
     * @return int número de registros encontrados
     * Método que verifica si se puede eliminar una fuente
     * de derivación
     */
    public function puedeBorrar($idderivacion){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros
                    FROM diagnostico.personas
                    WHERE diagnostico.personas.derivacion = '$idderivacion'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y retornamos
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idderivacion clave del registro
     * Método que recibe como parámetro la clave del registro
     * y ejecuta la consulta de eliminación
     */
    public function borraDerivacion($idderivacion){

        // compone y ejecuta la consulta
        $consulta = "DELETE FROM diagnostico.derivacion
                     WHERE diagnostico.derivacion.id = '$idderivacion'; ";
        $this->Link->exec($consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param derivacion cadena con el tipo de derivación
     * @return boolean si ya está declarada
     * Método que recibe como parámetro el nombre de una derivación
     * y retorna verdadero si ya está declarada en la base, utilizada
     * para evitar los registros duplicados
     */
    public function validaDerivacion($derivacion){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.derivacion.id) AS registros
                     FROM diagnostico.derivacion
                     WHERE diagnostico.derivacion.derivacion = '$derivacion'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // según si encontró
        if ($registros == 0){
            return true;
        } else {
            return false;
        }

    }

}
?>