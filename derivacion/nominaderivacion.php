<?php

/**
 *
 * borrar | derivacion/nominaderivacion.php
 *
 * @package     Diagnostico
 * @subpackage  Derivacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna el array json con la nómina de derivaciones
 * utilizado para cargar los combos
*/

// incluimos e instanciamos las clases
require_once("derivacion.class.php");
$derivacion = new Derivacion();

// ejecutamos la consulta
$nomina = $derivacion->nominaDerivacion();

// encodeamos el array y retornamos
echo json_encode($nomina);

?>