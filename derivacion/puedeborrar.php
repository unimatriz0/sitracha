<?php

/**
 *
 * puedeborrar | derivacion/puedeborrar.php
 *
 * @package     Diagnostico
 * @subpackage  Derivacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y verifica
 * que no esté asignado a algún paciente, retorna el número de
 * registros encontrados
*/

// incluimos e instanciamos las clases
require_once("derivacion.class.php");
$derivacion = new Derivacion();

// verificamos si tiene hijos
$registros = $derivacion->puedeBorrar($_GET["id"]);

// retornamos el número de registros
echo json_encode(array("Registros" => $registros));

?>