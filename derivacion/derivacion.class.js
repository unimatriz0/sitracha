/*

    Nombre: derivacion.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 07/06/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de tipos de derivación

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de efectos adversos
 */
class Derivacion {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initDerivacion();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDerivacion(){

        // inicializamos las variables
        this.Id = 0;                 // clave del registro
        this.Derivacion = "";        // descripción de la derivación

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de datos en el contenedor
     */
    muestraDerivacion(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("derivacion/listaderivacion.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que verifica los datos del formulario antes
     * de enviarlo al servidor
     */
    verificaDerivacion(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está dando un alta
        if (typeof(id) == "undefined"){
            id = "nueva";
        } else {
            this.Id = id;
        }

        // verifica que halla ingresado el nombre
        if (document.getElementById("derivacion_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar descripción de la derivación";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("derivacion_" + id).focus();
            return false;

        }

        // asigna en la variable de clase y graba
        this.Derivacion = document.getElementById("derivacion_" + id).value;
        this.grabaDerivacion();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que graba el registro en la base y luego
     * recarga la grilla
     */
    grabaDerivacion(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosDerivacion = new FormData();

        // agregamos al formulario
        datosDerivacion.append("Id", this.Id);
        datosDerivacion.append("Derivacion", this.Derivacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "derivacion/grabar.php",
            type: "POST",
            data: datosDerivacion,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    derivacion.initDerivacion();

                    // recarga el formulario para reflejar los cambios
                    derivacion.muestraDerivacion();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que llamado al pulsar el botón borrar que
     * verifica si puede borrar el registro
     */
    borraDerivacion(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la rutina php
        $.get('derivacion/puedeborrar.php?id='+id,
            function(data){

            // si retornó correcto
            if (data.Registros != 0){

                // llamamos la rutina de eliminación
                derivacion.eliminaDerivacion(id);

            // si no puede borrar
            } else {

                // presenta el mensaje
                new jBox('Notice', {content: "El registro tiene pacientes", color: 'red'});

            }

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método llamado luego de verificar que puede
     * borrar el registro y luego de pedir confirmación
     * elimina el registro y recarga la grilla
     */
    eliminaDerivacion(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Derivación',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('derivacion/borraderivacion.php?id='+id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // inicializamos las variables
                            derivacion.initDerivacion();

                            // recargamos la grilla
                            derivacion.muestraDerivacion();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un elemento html
     * @param {int} idderivacion - clave del registro preseleccionado
     * Método que recibe como parámetro la id de un elemento del
     * formulario y carga en ese elemento la nómina de tipos de
     * derivacion
     */
    nominaDerivacion(idelemento, idderivacion){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "derivacion/nominaderivacion.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la derivacion
                    if (typeof(idderivacion) != "undefined"){

                        // si coincide
                        if (data[i].Id == idderivacion){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].id + ">" + data[i].derivacion + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].derivacion + "</option>");

                        }

                    // si no recibió la clave
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].derivacion + "</option>");
                    }

                }

        }});

    }

}