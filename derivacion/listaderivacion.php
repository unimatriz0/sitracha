<?php

/**
 *
 * listaderivacion | derivacion/listaderivacion.php
 *
 * @package     Diagnostico
 * @subpackage  Derivacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (09/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta la grilla con los tipos de derivación y
 * permite el abm de ellos
*/

// incluimos e instanciamos las clases
require_once("derivacion.class.php");
$derivacion = new Derivacion();

// obtenemos la matriz
$nomina = $derivacion->nominaDerivacion();

// presentamos el título
echo "<h2>Tipos de Derivación</h2>";

// definimos la tabla
echo "<table id='derivaciones'
             whidth='60%'
             align='center'
             border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Derivación</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abre la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>";
    echo "<span class='tooltip'
                title='Tipo de derivacion'>";
    echo "<input type='text'
                 size='20'
                 name='derivacion_$id'
                 id='derivacion_$id'
                 value = '$derivacion'>";
    echo "</span>";
    echo "</td>";

    // presenta el usuario y la fecha
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha</td>";

    // presenta el enlace de edición
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaDerivacion'
           id='btnGrabaDerivacion'
           title='Pulse para grabar el registro'
           onClick='derivacion.verificaDerivacion($id)'
           class='botongrabar'>";
    echo "</td>";

    // presenta el enlace de borrar
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnBorraDerivacion'
           id='btnBorraDerivacion'
           title='Pulse para borrar el registro'
           onClick='derivacion.borraDerivacion($id)'
           class='botonborrar'>";
    echo "</td>";

    // cierra la fila
    echo "</tr>";

}

// agregamos la última fila
echo "<tr>";
echo "<td>";
echo "<span class='tooltip'
            title='Tipo de derivacion'>";
echo "<input type='text'
             size='20'
             name='derivacion_nueva'
             id='derivacion_nueva'>";
echo "</span>";
echo "</td>";

// presenta el usuario
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Usuario que ingresó el registro'>";
echo "<input type='text'
             size='10'
             name='usuario_derivacion'
             id='usuario_derivacion'>";
echo "</span>";
echo "</td>";

// presenta la fecha
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fecha de alta del registro'>";
echo "<input type='text'
             size='10'
             name='fecha_derivacion'
             id='fecha_derivacion'>";
echo "</span>";
echo "</td>";

// presenta el enlace de edición
echo "<td align='center'>";
echo "<input type='button'
       name='btnGrabaDerivacion'
       id='btnGrabaDerivacion'
       title='Pulse para grabar el registro'
       onClick='derivacion.verificaDerivacion()'
       class='botongrabar'>";
echo "</td>";

// presenta la última celda y cierra
echo "<td align='center'></td>";
echo "<tr>";

// cerramos la tabla
echo "</table>";

?>

<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

    // carga el usuario y la fecha de alta
    document.getElementById("usuario_derivacion").value = sessionStorage.getItem("Usuario");
    document.getElementById("fecha_derivacion").value = fechaActual();

</script>