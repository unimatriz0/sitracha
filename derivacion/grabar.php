<?php

/**
 *
 * grabar | derivacion/grabar.php
 *
 * @package     Diagnostico
 * @subpackage  Derivacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta de actualización en la base
*/

// incluimos e instanciamos las clases
require_once("derivacion.class.php");
$derivacion = new Derivacion();

// fijamos las variables
$derivacion->setId($_POST["Id"]);
$derivacion->setDerivacion($_POST["Derivacion"]);

// ejecutamos la consulta
$derivacion->grabaDerivacion();

?>