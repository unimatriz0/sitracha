<?php

/**
 *
 * dependencias/graba_dependencia.php
 *
 * @package     Diagnostico
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (09/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario 
 * y ejecuta la consulta en la base
 * 
*/

// incluimos e instanciamos la clase
require_once ("dependencias.class.php");
$dependencias = new Dependencias();

// si recibió la clave
if (!empty($_POST["Id"])){
    $dependencias->setIdDependencia($_POST["Id"]);
}

// asigna el nombre
$dependencias->setDependencia($_POST["Dependencia"]);

// grabamos el registro
$id = $dependencias->grabaDependencia();

// retornamos el resultado de la operación
echo json_encode(array("error" => $id));

?>