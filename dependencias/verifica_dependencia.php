<?php

/**
 *
 * dependencias/verifica_dependencia.php
 *
 * @package     Diagnostico
 * @subpackage  Dependencias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el nombre de una dependencia
 * verifica si la misma existe y retorna el número de 
 * registros
*/

/*

    Archivo: verifica_dependencia.php
    Autor: Claudio Invernizzi
    Fecha: 10/01/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: CCE
    Licencia: GPL
    Comentarios: Método que recibe como parámetro el nombre de una 
                 dependencia, instancia la clase y verifica si existe

*/

// incluimos e instanciamos la clase
require_once ("dependencias.class.php");
$dependencias = new Dependencias();

// verificamos si existe
$resultado = $dependencias->existeDependencia($_GET["dependencia"]);

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));