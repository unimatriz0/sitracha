/*
 * Nombre: dependencias.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 09/01/2017
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones del formulario de dependencias
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las dependencias 
 * de las instituciones
 */
class Dependencias {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initDependencias();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDependencias(){

        // inicialización de variables
        this.Id = 0;
        this.Dependencia = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el contenido del formulario de dependencias
     */
    muestraDependencias(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("dependencias/form_dependencias.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * @return {boolean} - resultado de la operación
     * Método que verifica el formulario de dependencias antes de grabar,
     * recibe como parámetro la clave de la dependencia
     */
    verificaDependencia(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está insertando
        if (id == undefined){

            // asignamos
            id = 'nueva';

        }

        // asignamos
        this.Id = id;

        // verifica el valor
        this.Dependencia = document.getElementById(id).value;

        // si no declaró
        if (this.Dependencia === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la dependencia";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById(id).focus();
            return false;

        }

        // si está dando un alta
        if (this.Id == 'nueva'){

            // verifica si no está declarada
            if (this.validaDependencia()){

                // presenta el mensaje y retorna
                mensaje = "Ese dependencia ya se encuentra declarada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById(id).focus();
                return false;

            }

        }

        // grabamos el registro
        this.grabaDependencia();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario, crea el objeto
     * formdata y lo envía por ajax al servidor
     */
    grabaDependencia(){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id != "nueva"){
            formulario.append("Id", this.Id);
        }

        // agrega la dependencia
        formulario.append("Dependencia", this.Dependencia);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "dependencias/graba_dependencia.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // inicializamos las variables de clase
                    dependencias.initDependencias();

                    // recarga el formulario para reflejar los cambios
                    dependencias.muestraDependencias();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} - si encontró el registro
     * Método llamado en la verificación del formulario para evitar que
     * se declaren dependencias repetidas, retorna verdadero si encontró
     * en la base de datos la dependencia que se quiere dar de alta
     */
    validaDependencia(){

        // declaración de variables
        var resultado;

        // llamamos la rutina php
        $.ajax({
            url: 'dependencias/verifica_dependencia.php?dependencia='+this.Dependencia,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error === true){
                    resultado = true;
                } else {
                    resultado = false;

                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * Método que recibe como parámetro la id de un select de un formulario
     * carga en ese elemento la nómina de dependencias
     */
    listaDependencias(idelemento){

        // obtenemos la nómina
        $.ajax({
            url: 'dependencias/lista_dependencias.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // limpia el combo
                $("#" + idelemento).html('');

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option></option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // simplemente lo agrega
                    $("#" + idelemento).append("<option value='" + data[i].Id + "'>" + data[i].Dependencia + "</option>");

                }

            }});

    }

}
