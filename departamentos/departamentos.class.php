<?php

/**
 *
 * Class Departamentos | departamentos/departamentos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Departamentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (30/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla el diccionario de departamentos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Departamentos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Id;                    // clave del registro
    protected $Institucion;           // nombre de la institución
    protected $IdInstitucion;         // clave de la institución
    protected $Departamento;          // nombre del departamento
    protected $FechaAlta;             // fecha de alta del registro
    protected $Usuario;               // nombre del usuario
    protected $IdUsuario;             // clave del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Id = 0;
        $this->Institucion = "";
        $this->Departamento = "";
        $this->FechaAlta = "";
        $this->Usuario = "";

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y de la institución
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdInstitucion = $_SESSION["IdLaboratorio"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setDepartamento($departamento){
        $this->Departamento = $departamento;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getDepartamento(){
        return $this->Departamento;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getUsuario(){
        return $this->Usuario;
    }

    /**
     * Método que puede recibir o no la clave de una institución, si no 
     * la recibe, utiliza la variable de sesión del usuario actual y 
     * retorna la nómina de departamentos para esa institución
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idinstitucion - clave de la institución
     * @return array - vector con los resultados
     */
    public function nominaDepartamentos($idinstitucion = false){

        // si no recibió la institución
        if ($idinstitucion == false){
            $idinstitucion = $this->IdInstitucion;
        }

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_departamentos.id AS id, 
                            diagnostico.v_departamentos.departamento AS departamento,
                            diagnostico.v_departamentos.fecha_alta AS fecha_alta, 
                            diagnostico.v_departamentos.usuario AS usuario
                     FROM diagnostico.v_departamentos 
                     WHERE diagnostico.v_departamentos.idinstitucion = '$idinstitucion' 
                     ORDER BY diagnostico.v_departamentos.departamento;";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método que recibe como parámetro la clave de un registro y 
     * asigna en las variables de clase los valores del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function getDatosDepartamento($iddepartamento){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_departamentos.id AS id,
                            diagnostico.v_departamentos.institucion AS institucion,
                            diagnostico.v_departamentos.departamento AS departamento,
                            diagnostico.v_departamentos.fecha_alta AS fecha_alta, 
                            diagnostico.v_departamentos.usuario AS usuario
                     FROM diagnostico.v_departamentos
                     WHERE diagnostico.v_departamentos.id = '$iddepartamento';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y asignamos en las variables
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Institucion = $institucion;
        $this->Departamento = $departamento;
        $this->FechaAlta = $fecha_alta;
        $this->Usuario = $usuario;

    }

    /**
     * Método que ejecuta la consulta de inserción o edición según 
     * corresponda, retorna la clave del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $iddepartamento - clave del registro
     */
    public function grabaDepartamento(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoDepartamento();
        } else {
            $this->editaDepartamento();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoDepartamento(){

        // compone la consulta
        $consulta = "INSERT INTO diagnostico.departamentos 
                            (institucion, 
                             departamento,
                             usuario)
                            VALUES 
                            (:idinstitucion, 
                             :departamento,
                             :idusuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":institucion",  $this->IdInstitucion);
        $psInsertar->bindParam(":departamento", $this->Departamento);
        $psInsertar->bindParam(":idusuario",    $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdMarca = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaDepartamento(){

        // compone la consulta
        $consulta = "UPDATE diagnostico.departamentos SET 
                            departamento = :departamento,
                            usuario = :idusuario
                     WHERE diagnostico.departamentos.id = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":departamento", $this->Departamento);
        $psInsertar->bindParam(":idusuario",    $this->IdUsuario);
        $psInsertar->bindParam(":id",           $this->Id);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro el nombre de un departamento y 
     * verifica que no esté declarado para la institución actual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $departamento - nombre del departamento
     */
    public function validaDepartamento($departamento){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.departamentos.id) AS registros
                     FROM diagnostico.departamentos 
                     WHERE diagnostico.departamentos.departamento = '$departamento' AND 
                           diagnostico.departamentos.institucion = '$this->IdInstitucion'; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }
    
}
