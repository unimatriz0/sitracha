<?php

/**
 *
 * getdepartamento | departamentos/getdepartamento.php
 *
 * @package     Diagnostico
 * @subpackage  Departamentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (31/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un departamento y 
 * retorna el array json con los datos del registro
 * 
*/

// incluimos e instanciamos las clases
require_once("departamentos.class.php");
$departamento = new Departamentos();

// obtenemos el registro
$departamento->getDatosDepartamento($_GET["departamento"]);

// retornamos los datos
echo json_encode(array("Id" => $departamento->getId(),
                       "Departamento" => $departamento->getDepartamento(),
                       "Usuario" => $departamento->getUsuario(),
                       "Alta" => $departamento->getFechaAlta()));
                       
?>