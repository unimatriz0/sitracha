<?php

/**
 *
 * graba_departamento | departamentos/graba_departamento.php
 *
 * @package     Diagnostico
 * @subpackage  Departamentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (31/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta la 
 * consulta en el servidor
 * 
*/

// incluimos e instanciamos las clases
require_once("departamentos.class.php");
$departamento = new Departamentos();

// asignamos en la clase
$departamento->setId($_POST["Id"]);
$departamento->setDepartamento($_POST["Departamento"]);

// grabamos el registro
$id = $departamento->grabaDepartamento();

// retornamos el resultado de la operación
echo json_encode(array("Id" => $id));

?>