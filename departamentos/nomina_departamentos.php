<?php

/**
 *
 * nomina_departamentos | departamentos/nomina_departamentos.php
 *
 * @package     Diagnostico
 * @subpackage  Departamentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (30/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con los departamentos de la 
 * institución del usuario activo
 * 
*/

// incluimos e instanciamos las clases
require_once("departamentos.class.php");
$departamentos = new Departamentos();

// obtenemos la nómina según si recibió o no la institución
if (empty($_GET["institucion"])){
    $nomina = $departamentos->nominaDepartamentos();
} else {
    $nomina = $departamentos->nominaDepartamentos($_GET["institucion"]);
}

// retornamos el array
echo json_encode($nomina);

?>