/*

    Nombre: departamentos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 30/07/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones sobre la tabla 
                 de departamentos

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de departamentos
 */
class Departamentos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initDepartamentos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDepartamentos(){

        // inicializamos las variables
        this.Id = 0;               // clave del registro
        this.IdInstitucion = 0;    // clave de la institución
        this.Institucion = "";     // nombre de la institución
        this.Departamento = "";    // nombre del departamento
        this.FechaAlta = "";       // fecha de alta del registro
        this.Usuario = "";         // nombre del usuario

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setIdInstitucion(idinstitucion){
        this.IdInstitucion = idinstitucion;
    }
    setInstitucion(institucion){
        this.Institucion = institucion;
    }
    setDepartamento(departamento){
        this.Departamento = departamento;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@ggmail.com>
     * Método que carga en el tabulador el formulario de 
     * departamentos
     */
    verDepartamentos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("departamentos/form_departamentos.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene la nómina de departamentos 
     */
    cargaDepartamentos(){

        // obtenemos la nómina
        $.ajax({
            url: 'departamentos/nomina_departamentos.php',
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // llamamos la función de carga
                departamentos.listarDepartamentos(data);

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos vector con los departamentos
     * Método que carga en el cuerpo de la tabla los departamentos
     */
    listarDepartamentos(datos){

        // declaración de variables
        var texto;

        // limpia el cuerpo
        $("#cuerpo_departamentos").html('');

        // recorremos el vector de datos
        for(var i=0; i < datos.length; i++){

            // abre la fila
            texto = "<tr>";

            // agregamos el nombre
            texto += "<td>";
            texto += datos[i].departamento;
            texto += "</td>";

            // agregamos el usuario
            texto += "<td align='center'>";
            texto += datos[i].usuario;
            texto += "</td>";

            // agregamos la fecha de alta
            texto += "<td align='center'>";
            texto += datos[i].fecha_alta;
            texto += "</td>";

            // arma el enlace
            texto += "<td align='center'>";
            texto += "<input type='button' ";
            texto += "       name='btnEditaDepartamento' ";
            texto += "       id='btnEditaDepartamento' ";
            texto += "       class='botongrabar' ";
            texto += "       title='Edita el registro' ";
            texto += "       onClick='departamentos.getDatosDepartamento(" + datos.id + ")'>";
            texto += "</td>";

            // cierra la fila
            texto += "</tr>";

            // la agregamos al dom 
            $("#cuerpo_departamentos").append(texto);

        }
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que valida
     * el formulario antes de enviarlo al servidor
     */
    validaDepartamento(){

        // declaración de variables
        var mensaje;

        // si no ingresó el departamento
        if (document.getElementById("nombre_departamento").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre del departamento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_departamento").focus();
            return false;

        // si ingresó 
        } else {

            // asigna en la clase
            this.Departamento = document.getElementById("nombre_departamento").value;

        }

        // verifica que no esté repetido
        this.verificaDepartamento();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica que el registro no se encuentre 
     * repetido
     */
    verificaDepartamento(){

        // verificamos si existe
        $.ajax({
            url: 'departamentos/valida_departamento.php?departamento='+this.Departamento,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si no hubo registros
                if (data.Registros == 0){

                    // grabamos 
                    departamentos.grabaDepartamento();

                // si hubo 
                } else {

                    // presenta el mensaje y retorna
                    var mensaje = "Ese departamento ya está declarado";
                    new jBox('Notice', {content: mensaje, color: 'red'});
                    document.getElementById("nombre_departamento").focus();
                    return false;

                }

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos al servidor
     */
    grabaDepartamento(){

        // declaración de variables
        var datosDepartamento = new FormData();

        // asignamos los valores
        datosDepartamento.append("Id", this.Id);
        datosDepartamento.append("Departamento", this.Departamento);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "departamentos/graba_departamento.php",
            type: "POST",
            data: datosDepartamento,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // limpia el formulario 
                    departamentos.limpiaDepartamento();

                    // inicializamos las variables de clase
                    departamentos.initDepartamentos();

                    // recarga el formulario para reflejar los cambios
                    departamentos.cargaDepartamentos();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} clave del departamento
     * Método que recibe como parámetro la clave de un 
     * departamento y obtiene los datos del registro
     */
    getDatosDepartamento(iddepartamento){

        // obtenemos el registro
        $.ajax({
            url: 'departamentos/getdepartamento.php?departamento='+iddepartamento,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // cargamos el registro
                departamentos.cargaDatosDepartamento(data);

            }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} vector con los datos del registro
     * Método que recibe como parámetro los datos del 
     * registro y asigna los valores en las variables de 
     * clase
     */
    cargaDatosDepartamento(vector){

        // cargamos los datos en la clase
        this.setId(vector.Id);
        this.setDepartento(vector.Departamento);
        this.setUsuario(vector.Usuario);
        this.setFechaAlta(vector.Alta);

        // mostramos el registro
        this.mostrarDatosDepartamento();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que a partir de las variables de clase las 
     * carga en el formulario
     */
    mostrarDatosDepartamento(){

        // cargamos en el formulario
        document.getElementById("nombre_departamento").value = this.Departamento;
        document.getElementById("usuario_departamento").value = this.Usuario;
        document.getElementById("alta_departamento").value = this.FechaAlta;

        // fijamos el foco
        document.getElementById("nombre_departamento").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de grabar que limpia el formulario
     * de datos
     */
    limpiaDepartamento(){

        // fijamos los valores por defecto
        document.getElementById("nombre_departamento").value = "";
        document.getElementById("usuario_departamento").vaue = sessionStorage.getItem("Usuario");
        document.getElementById("alta_departamento").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un elemento html
     * @param {int} idinstitucion clave de la institución
     * @param {int} departamento clave del departamento
     * Método que recibe como parámetro la id de un elemento html 
     * (normalmente un select) y la clave de la institución que 
     * puede ser nulo y la clave de un departamento que es el 
     * valor predefinido (si no recibe la institución utiliza 
     * la institución del usuario)
     */
    nominaDepartamentos(idelemento, idinstitucion, departamento){

        // si la institución está vacía
        if (typeof(idinstitucion) == "undefined"){

            // preasignamos la institución actual
            idinstitucion = sessionStorage.getItem("IdLaboratorio");

        }

        // obtenemos la nómina
        $.ajax({
            url: 'departamentos/nomina_departamentos.php?institucion='+idinstitucion,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // limpia el combo
                $("#" + idelemento).html('');

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value='0'>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si coincide con el valor preseleccionado
                    if (data[i].id == departamento){

                        // lo agregamos seleccionado
                        $("#" + idelemento).append("<option value='" + data[i].id + "' selected>" + data[i].departamento + "</option>");

                    // si no coincide
                    } else {

                        // simplemente lo agrega
                        $("#" + idelemento).append("<option value='" + data[i].id + "'>" + data[i].departamento + "</option>");

                    }

                }

            }});

    }

}