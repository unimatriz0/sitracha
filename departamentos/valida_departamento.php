<?php

/**
 *
 * valida_departamento | departamentos/valida_departamento.php
 *
 * @package     Diagnostico
 * @subpackage  Departamentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (31/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el nombre de un departamento y 
 * retorna el número de registros encontrados (utilizado 
 * para impedir la entrada repetida)
 * 
*/

// incluimos e instanciamos las clases
require_once("departamentos.class.php");
$departamento = new Departamentos();

// retornamos el número de registros
$registros = $departamento->validaDepartamento($_GET["departamento"]);
echo json_encode(array("Registros" => $registros));

?>