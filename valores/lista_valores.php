<?php

/**
 * lista_valores.php
 *
 * Método que recibe por get la id de una técnica y retorna un
 * array json con la nómina de valores aceptados
 *
 * @package     Diagnostico
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/01/2018)
 * @copyright   Copyright (c) 2017, INP
 * @param $_GET["tecnica"]
 *
*/

// incluimos e instanciamos la clase
require_once ("valores.class.php");
$valores = new Valores();

// obtenemos el array
$nomina = $valores->nominaValores($_GET["tecnica"]);

// declaramos el array
$jsondata = array();

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al vector
    $jsondata[] = array("Id" => $id_valor,
                        "Valor" => $valor);

}

// retornamos el vector
echo json_encode($jsondata);
?>