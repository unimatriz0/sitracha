<?php

/**
 * verifica_valor.php
 *
 * Procedimiento que recibe por get el valor de una técnica y
 * la clave de la técnica, verifica que no se encuentre
 * repetido en cuyo caso retorna verdadero
 *
 * @package     Diagnostico
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/01/2018)
 * @copyright   Copyright (c) 2017, INP
 * @param $_GET["tecnica"]
 * @param $_GET["valor"]
 *
*/

// incluimos e instanciamos la clase
require_once ("valores.class.php");
$tecnica = new Valores();

// verificamos si existe el valor
$resultado = $tecnica->verificaValor($_GET["tecnica"], $_GET["valor"]);

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));

?>