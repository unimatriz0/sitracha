<?php

/**
 * nomina_valores.php
 * 
 * Método que recibe por get la id de una técnica y arma el formulario
 * de ABM de los valores correspondientes a esa técnica
 *
 * @package     Diagnostico
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/01/2018)
 * @copyright   Copyright (c) 2017, INP
 * @param $_GET["tecnica"]
 *
*/

// incluimos e instanciamos las clases
require_once ("valores.class.php");
$tecnicas = new Valores();

// obtenemos la clave
$clave_tecnica = $_GET["tecnica"];

// obtenemos la nómina
$nomina_valores = $tecnicas->nominaValores($clave_tecnica);

// abrimos la sesión y obtenemos el usuario
session_start();
$usuario_sesion = $_SESSION["Usuario"];
session_write_close();

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th>Valor</th>";
echo "<th>Alta</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "<thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina_valores AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro
    echo "<tr>";
    echo "<td>";
    echo "<span class='tooltip'
                title='Valor aceptado'>";
    echo "<input type='text'
           name='valor_$id_valor'
           id='valor_$id_valor'
           size='25'
           value='$valor'>";
    echo "</span>";
    echo "</td>";

    // presentamos oculta la clave de la técnica
    echo "<input type='hidden'
           name='tecnica_$id_valor'
           id='tecnica_$id_valor'
           value='$id_tecnica'>";

    // presentamos la fecha y el usuario
    echo "<td>$fecha_alta</td>";
    echo "<td>$usuario</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaValor'
           id='btnGrabaValor'
           title='Pulse para grabar el registro'
           onClick='valores.verificaValor($id_valor)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// agregamos una fila para insertar un nuevo registro
echo "<tr>";

// presentamos el registro
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Valor Aceptado'>";
echo "<input type='text'
        name='valor_nueva'
        id='valor_nueva'
        size='25'>";
echo "</span>";
echo "</td>";

// presentamos oculta la clave de la técnica
echo "<input type='hidden'
        name='tecnica_nueva'
        id='tecnica_nueva'
        value='$clave_tecnica'>";

// obtenemos la fecha actual
$fecha_alta = date("d/m/Y");

// fecha de alta y usuario simplemente lo presentamos
echo "<td align='center'>$fecha_alta</td>";
echo "<td align='center'>$usuario_sesion</td>";

// agregamos el enlace para grabar
echo "<td align='center'>";
echo "<input type='button' name='btnNuevoValor'
        id='btnNuevoValor'
        title='Pulse para grabar el registro'
        onClick='valores.verificaValor()'
        class='botonagregar'>";
echo "</td>";

echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<SCRIPT>

        // instanciamos los tooltips
        new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
        });

</SCRIPT>