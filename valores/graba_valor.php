<?php

/**
 * graba_valor.php
 *
 * Procedimiento que recibe como parámetros los valores de un
 * registro de la tabla auxiliar de valores, llama la clase y
 * ejecuta la consulta
 *
 * @package     Diagnostico
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/01/2018)
 * @copyright   Copyright (c) 2017, INP
 * @param $_GET["tecnica"]
 *
*/

// inclusión de clases e instanciación
require_once ("valores.class.php");
$tecnica = new Valores();

// si recibió la id del registro
if (!empty($_POST["Id"])){

    // asigna en la clase
    $tecnica->setIdValor($_POST["Id"]);

}

// asignamos el resto de los valores
$tecnica->setIdTecnicaValor($_POST["Tecnica"]);
$tecnica->setValor($_POST["Valor"]);

// grabamos el registro
$resultado = $tecnica->grabaValores();

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));

?>