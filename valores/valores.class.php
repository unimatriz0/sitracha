<?php

/**
 *
 * Contiene la clase valores que implementa todos los métodos
 * para el control de la tabla auxiliar de valores admitidos
 * para cada técnica diagnóstica
 *
 * @package     Diagnostico
 * @subpackage  Valores
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de valores
 * admitidos para cada técnica diagnóstica
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Valores{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    // variables de la tabla de valores aceptados
    protected $IdValor;                  // clave del registro
    protected $IdTecnicaValor;           // clave de la técnica
    protected $Valor;                    // valor aceptado

    // definición de variables
    protected $Link;                     // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdValor = 0;
        $this->IdTecnicaValor = 0;
        $this->Valor = "";

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdValor($idvalor){

        // verifica que sea un número
        if (!is_numeric($idvalor)){

            // abandona por error
            echo "La clave del valor debe ser un número";
            exit;

            // si está correcto
        } else {

            // lo asigna
            $this->IdValor = $idvalor;

        }

    }
    public function setIdTecnicaValor($idtecnicavalor){

        // verifica que sea un número
        if (!is_numeric($idtecnicavalor)){

            // abandona por error
            echo "La clave de la técnica debe ser un número";
            exit;

            // si está correcto
        } else {

            // lo asigna
            $this->IdTecnicaValor = $idtecnicavalor;

        }

    }
    public function setValor($valor){
        $this->Valor = $valor;
    }

    /**
     * Método que recibe la id de una técnica y retorna el array
     * asociativo de valores aceptados para esa técnica
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $tecnica - clave de la técnica
     * @return array
     */
    public function nominaValores($tecnica){

        // armamos la consulta
        $consulta = "SELECT diagnostico.v_valores.id_valor AS id_valor,
                            diagnostico.v_valores.valor AS valor,
                            diagnostico.v_valores.id_tecnica AS id_tecnica,
                            diagnostico.v_valores.fecha_alta AS fecha_alta,
                            diagnostico.v_valores.usuario AS usuario
                     FROM diagnostico.v_valores
                     WHERE diagnostico.v_valores.id_tecnica = '$tecnica';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $listaValores = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $listaValores;

    }

    /**
     * Método que graba los datos del valor aceptado de la
     * técnica en la base de datos, retorna la id del
     * registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro editado / insertado
     */
    public function grabaValores(){

        // si no recibió la clave
        if ($this->IdValor == 0){

            // insertamos el registro
            $this->nuevoValor();

            // si está editando
        } else {

            // editamos el registro
            $this->editaValor();

        }

        // retornamos la id
        return $this->IdValor;

    }

    /**
     * Método que inserta un nuevo registro en la tabla de valores
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoValor(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diagnostico.valores_tecnicas
                                 (id_tecnica,
                                  id_usuario,
                                  valor)
                                 VALUES
                                 (:idtecnicavalor,
                                  :idusuario,
                                  :valor);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":idtecnicavalor", $this->IdTecnicaValor);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":valor", $this->Valor);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdValor = $this->Link->lastInsertId();

    }

    /**
     * Método que edita el registro de la tabla de valores
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaValor(){

        // compone la consulta de edición
        $consulta = "UPDATE diagnostico.valores_tecnicas SET
                            id_tecnica = :idtecnicavalor,
                            id_usuario = :idusuario,
                            valor = :valor
                     WHERE diagnostico.valores_tecnicas.id = :idvalor;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":idtecnicavalor", $this->IdTecnicaValor);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":valor", $this->Valor);
        $psInsertar->bindParam(":idvalor", $this->IdValor);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de una técnica y el
     * valor correcto, verifica si ese valor ya está declarado, en cuyo
     * caso retorna verdadero (utilizado para evitar el ingreso
     * duplicado de valores)
     * @param int $tecnica - clave de la técnica
     * @param string $valor - valor correcto de la técnica
     * @return boolean
     */
    public function verificaValor($tecnica, $valor){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.valores_tecnicas.ID) AS registros
                     FROM diagnostico.valores_tecnicas
                     WHERE diagnostico.tecnicas.id = '$tecnica' AND
                           diagnostico.valores_tecnicas.valor = '$valor';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // obtenemos el array con todos los registros
        $fila = $resultado->fetch(PDO::FETCH_ASSOC);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($fila, CASE_LOWER);

        // obtenemos el registro y retornamos el vector
        extract($registro);

        // si lo encontró
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

}

?>