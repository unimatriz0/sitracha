/*
 * Nombre: valores.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 06/01/2017
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones del abm de valores
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla los operaciones de la tabla de valores
 * de lectura aceptados
 */
class Valores {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initValores();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initValores(){

        // declaración de variables
        this.Id = 0;
        this.Valor = "";
        this.Tecnica = 0;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * método que carga el formulario principal
     */
    muestraValores(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el formulario de marcas
        $("#form_administracion").load("valores/form_valores.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del combo de técnicas, obtiene
     * la clave de la técnica y la pasa por ajax al servidor para
     * cargar la nómina en la segunda columna
     */
    nominaValores(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la clave
        var idtecnica = document.getElementById("lista_tecnicas").value;

        // si seleccionó el primer elemento
        if (idtecnica == "" || idtecnica == 0){

            // simplemente retorna
            return false;

        }

        // cargamos la nómina
        $("#columna2").load("valores/nomina_valores.php?tecnica="+idtecnica);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el select la nómina de técnicas
     */
    cargaTecnicas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llamamos la clase javascript
        tecnicas.listaTecnicas("lista_tecnicas");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id clave registro actual
     * Método que recibe como parámetro la id del registro o nulo si está
     * dando un alta, verifica los datos del formulario antes de grabar
     */
    verificaValor(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está dando un alta
        if (typeof(id) == "undefined"){
            id = "nueva";
        }

        // asignamos en la variable de clase
        this.Id = id;

        // obtenemos la clave de la técnica del formulario lateral
        this.Tecnica = document.getElementById("lista_tecnicas").value;

        // verifica se halla declarado nombre
        this.Valor = document.getElementById("valor_" + id).value;
        if (this.Valor == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el valor aceptado o en <br>";
            mensaje += "su defecto la máscara de entrada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("valor_" + id).focus();
            return false;

        }

        // si está dando un alta verifica que el valor no
        // esté declarado para esa técnica
        if (this.Id == "nueva"){

            // si está declarado
            if (this.validarValor()){

                // presenta el mensaje y retorna
                mensaje = "Ese valor ya se encuentra declarado";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // grabamos el registro
        this.grabaValor();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que crea el objeto formdata y actualiza la base de datos
     */
    grabaValor(){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id != "nueva"){
            formulario.append("Id", this.Id);
        }

        // agrega la marca y la técnica
        formulario.append("Valor", this.Valor);
        formulario.append("Tecnica", this.Tecnica);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "valores/graba_valor.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro Grabado ...", color: 'green'});

                    // recarga el formulario para reflejar los cambios
                    valores.nominaValores(valores.Tecnica);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean}
     * Método que verifica el valor no se encuentre declarado para la
     * misma ténica
     */
    validarValor(){

        // declaración de variables
        var resultado;

        // llamamos la rutina php
        $.ajax({
            url: 'valores/verifica_valor.php?valor='+this.Valor+'&tecnica='+this.Tecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error === true){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento
     * @param {int} idtecnica
     * @param {int} idvalor
     * Método que recibe como parámetro la id de un select y carga en el mismo
     * los valores de corte aceptados para la técnica que recibe como parámetro
     * si además recibe un valor preseleccionado lo marca por default
     */
    listaValores(idelemento, idtecnica, idvalor){

        // limpia el combo
        $("#" + idelemento).html('');

        // lo llamamos asincrónico
        $.ajax({
            url: "valores/lista_valores.php?tecnica="+idtecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si existe la clave y es igual
                    if (typeof(idvalor) != "undefined"){

                        // si es la misma
                        if (idvalor == data[i].Id){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Valor + "</option>");

                        // si son distintos
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Valor + "</option>");

                        }

                    // si no recibió la clave del valor recorre el bucle agregando
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Valor + "</option>");
                    }

                }

        }});

    }

}
