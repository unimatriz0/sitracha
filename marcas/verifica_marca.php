<?php

/**
 *
 * marcas/verifica_marca.php
 *
 * @package     Diagnostico
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el nombre de una marca y la 
 * clave de la técnica, verifica que no se encuentre 
 * repetido, en cuyo caso retorna verdadero 
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marca = new Marcas();

// verificamos si la marca existe
$resultado = $marca->getClaveMarca($_GET["tecnica"], $_GET["marca"]);

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));

?>