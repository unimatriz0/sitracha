<?php

/**
 *
 * marcas/lista_marcas.php
 *
 * @package     Diagnostico
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (20/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una técnica y 
 * retorna un array json con la nómina de marcas de 
 * reactivos de esa técnica 
 * 
*/

// incluimos e instanciamos la clase
require_once("marcas.class.php");
$marcas = new Marcas();

// obtenemos la matriz
$nomina = $marcas->nominaMarcas($_GET["idtecnica"]);

// inicializa las variables
$jsondata = array();

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agrega a la matriz
    $jsondata[] = array("Id" => $id_marca,
                        "Marca" => $marca);

}

// devuelve la cadena
echo json_encode($jsondata);
?>