<?php

/**
 *
 * marcas/nomina_marcas.php
 *
 * @package     Diagnostico
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una técnica y arma el 
 * formulario de abm de las marcas correspondientes a esa 
 * técnica
 * 
*/

// incluimos e instanciamos las clases
require_once ("marcas.class.php");
$marcas = new Marcas();

// obtenemos la nómina
$nomina_marcas = $marcas->nominaMarcas($_GET["tecnica"]);

// abrimos la sesión y obtenemos el usuario
session_start();
$usuario_sesion = $_SESSION["Usuario"];
session_write_close();

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th>Marca</th>";
echo "<th>Alta</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "<thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina_marcas AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro
    echo "<tr>";
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre de la marca'>";
    echo "<input type='text'
           name='marca_$id_marca'
           id='marca_$id_marca'
           size='25'
           value='$marca'>";
    echo "</span>";
    echo "</td>";

    // presentamos oculta la clave de la técnica
    echo "<input type='hidden'
           name='tecnica_$id_marca'
           id='tecnica_$id_marca'
           value='$id_tecnica'>";

    // presentamos la fecha y el usuario
    echo "<td>$fecha_alta</td>";
    echo "<td>$usuario</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaMarca'
           id='btnGrabaMarca'
           title='Pulse para grabar el registro'
           onClick='marcas.verificaMarca($id_marca)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// agregamos una fila para insertar un nuevo registro
echo "<tr>";

    // presentamos el registro
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Nombre de la marca'>";
    echo "<input type='text'
           name='marca_nueva'
           id='marca_nueva'
           size='25'>";
    echo "</span>";
    echo "</td>";

    // presentamos oculta la clave de la técnica
    echo "<input type='hidden'
           name='tecnica_nueva'
           id='tecnica_nueva'>";

    // obtenemos la fecha actual
    $fecha_alta = date("d/m/Y");

    // fecha de alta y usuario simplemente lo presentamos
    echo "<td align='center'>$fecha_alta</td>";
    echo "<td align='center'>$usuario_sesion</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnNuevaMarca'
           id='btnNuevaMarca'
           title='Pulse para grabar el registro'
           onClick='marcas.verificaMarca()'
           class='botonagregar'>";
    echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>