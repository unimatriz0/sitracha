<?php

/**
 *
 * Class Marcas | marcas/marcas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla el diccionario de marcas de reactivos 
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Marcas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // definición de las variables de la base de datos
    protected $IdMarca;                // clave de la marca
    protected $Marca;                  // nombre de la marca
    protected $IdTecnica;              // clave de la técnica
    protected $IdUsuario;              // clave del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdMarca = 0;
        $this->Marca = null;
        $this->IdTecnica = 0;

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setIdMarca($idmarca){

        // verifica que sea un número
        if (!is_numeric($idmarca)){

            // cierra con error
            echo "La clave de la marca debe ser un número";
            exit;

        // si es correcto
        } else {

            // asigna en la clase
            $this->IdMarca = $idmarca;

        }

    }
    public function setIdTecnica($idtecnica){

        // verifica que sea un número
        if (!is_numeric($idtecnica)){

            // cierra con error
            echo "La clave de la técnica debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdTecnica = $idtecnica;

        }

    }
    public function setMarca($marca){
        $this->Marca = $marca;
    }

    /**
     * Método que retorna el listado de las marcas, recibe como parámetro
     * la id de la técnica, retorna la id de la marca, es utilizada en el
     * abm de marcas
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $tecnica - clave de la técnica
     * @return array
     */
    public function nominaMarcas($tecnica){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_marcas.id_marca AS id_marca,
                            diagnostico.v_marcas.marca AS marca,
                            diagnostico.v_marcas.id_tecnica AS id_tecnica,
                            diagnostico.v_marcas.fecha_alta AS fecha_alta,
                            diagnostico.v_marcas.usuario AS usuario
                     FROM diagnostico.v_marcas
                     WHERE diagnostico.v_marcas.id_tecnica = '$tecnica'
                     ORDER BY diagnostico.v_marcas.marca;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $listaMarcas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $listaMarcas;

    }

    /**
     * método que retorna la clave de una marca, recibe como parámetro
     * la id de la técnica y la cadena con la marca
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $tecnica - nombre de la técnica
     * @param string $marca - nombre de la marca
     * @return [int]
     */
    public function getClaveMarca($tecnica, $marca){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.v_marcas.id_marca) AS registros
                     FROM diagnostico.v_marcas
                     WHERE diagnostico.v_marcas.id_tecnica = '$tecnica' AND
                           diagnostico.v_marcas.marca = '$marca'; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($fila);

        // retornamos el número de registros
        return $registros;

    }

    /**
     * Función que actualiza la base de datos de marcas, retorna
     * la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / editado
     */
    public function grabaMarca(){

        // componemos la consulta según si está editando o
        // insertando
        if ($this->IdMarca == 0){

            // insertamos el registro
            $this->nuevaMarca();

        // de otra forma
        } else {

            // editamos el registro
            $this->editaMarca();

        }

        // retorna la id del registro
        return $this->IdMarca;

    }

    /**
     * Método que inserta un nuevo registro en la tabla de marcas
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaMarca(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diagnostico.marcas
                            (MARCA,
                             TECNICA,
                             USUARIO)
                            VALUES
                            (:marca,
                             :idtecnica,
                             :idusuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":marca", $this->Marca);
        $psInsertar->bindParam(":idtecnica", $this->IdTecnica);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdMarca = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que edita el registro de marcas
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaMarca(){

        // compone la consulta de edición
        $consulta = "UPDATE diagnostico.marcas SET
                            MARCA = :marca,
                            TECNICA = :idtecnica,
                            USUARIO = :idusuario
                     WHERE diagnostico.marcas.ID = :idmarca; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":marca", $this->Marca);
        $psInsertar->bindParam(":idtecnica", $this->IdTecnica);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":idmarca", $this->IdMarca);

        // ejecutamos la edición
        $psInsertar->execute();

    }

}
