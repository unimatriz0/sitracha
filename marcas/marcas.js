/*
 * Nombre: marcas.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 25/12/2016
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de marcas de reactivos
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

 /**
  * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
  * @class Clase que controla las operaciones sobre la tabla de marcas
  */
 class Marcas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializa las variables de clase
        this.initMarcas();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initMarcas(){

        // declaración de variables
        this.Id = 0;
        this.Tecnica = 0;
        this.Marca = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenedor el formulario de marcas
     */
    mostrarMarcas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el formulario de marcas
        $("#form_administracion").load("marcas/form_marcas.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onclick del select de técnicas
     * según el valor carga en el div secundario la nómina de marcas
     */
    nominaMarcas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos el valor
        var idtecnica = document.getElementById("lista_tecnicas").value;

        // si está seleccionado el primer elemento
        if (idtecnica == "" || idtecnica == 0){

            // simplemente retorna
            return false;

        }

        // cargamos la nómina
        $("#columna2").load("marcas/nomina_marcas.php?tecnica="+idtecnica);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la clase de técnicas para cargar el select
     */
    cargaTecnicas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llamamos la clase javascript
        tecnicas.listaTecnicas("lista_tecnicas");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que verifica los datos del formulario de marcas
     */
    verificaMarca(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está dando un alta
        if (id == undefined){
            id = "nueva";
        }

        // asignamos en la variable de clase
        this.Id = id;

        // obtenemos la clave de la técnica del combo de técnicas
        this.Tecnica = document.getElementById("lista_tecnicas").value;

        // verifica se halla declarado nombre
        this.Marca = document.getElementById("marca_" + id).value;
        if (this.Marca === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la marca";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("marca_" + id).focus();
            return false;

        }

        // si está dando un alta
        if (this.Id === "nueva"){

            // verifica que la marca no se encuentre declarada
            if (this.validarMarca()){

                // presenta el mensaje y retorna
                mensaje = "Ese marca ya se encuentra declarada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // grabamos el registro
        this.grabaMarca();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} - si el registro está declarado o no 
     * Método que verifica la marca no se encuentre repetida para la misma
     * técnica utilizada en el alta de nuevos registros
     */
    validarMarca(){

        // declaración de variables
        var resultado;

        // llamamos sincrónicamente
        $.ajax({
            url: 'marcas/verifica_marca.php?marca='+this.Marca+'&tecnica='+this.Tecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error != 0){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario, arma el objeto formdata
     * y envía por ajax al servidor
     */
    grabaMarca (){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id !== "nueva"){
            formulario.append("Id", this.Id);
        }

        // agrega la marca y la técnica
        formulario.append("Marca", this.Marca);
        formulario.append("Tecnica", this.Tecnica);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "marcas/graba_marca.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // recarga el formulario para reflejar los cambios
                    marcas.nominaMarcas(marcas.Tecnica);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id del objeto html
     * @param {int} idtecnica - clave de la técnica
     * @param {int} idmarca - valor predeterminado
     * Método que recibe como parámetro la id de un select y carga en el mismo
     * las marcas correspondientes a la técnica que recibe como parámetro
     * si además recibe el valor predeterminado (optativo) lo marca
     */
    listaMarcas(idelemento, idtecnica, idmarca){

        // limpia el combo
        $("#" + idelemento).html('');

        // lo llamamos asincrónico
        $.ajax({
            url: "marcas/lista_marcas.php?idtecnica="+idtecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si existe la clave y es igual
                    if (typeof(idmarca) != "undefined"){

                        // si es la misma
                        if (idmarca == data[i].Id){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Marca + "</option>");

                        // si son distintos
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Marca + "</option>");

                        }

                    // si no recibió la clave de la tecnica recorre el
                    // bucle agregando
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Marca + "</option>");
                    }

                }

        }});

    }

 }
