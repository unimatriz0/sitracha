<?php

/**
 *
 * marcas/graba_marca.php
 *
 * @package     Diagnostico
 * @subpackage  Marcas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (29/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario de 
 * marcas, ejecuta la consulta en la base y retorna la 
 * clave del registro afectado
 * 
*/

// incluimos e instanciamos la clase
require_once ("marcas.class.php");
$marca = new Marcas();

// asignamos los valores recibidos por post
if (!empty($_POST["Id"])){
    $marca->setIdMarca($_POST["Id"]);
}
$marca->setMarca($_POST["Marca"]);
$marca->setIdTecnica($_POST["Tecnica"]);

// actualizamos el registro
$id = $marca->grabaMarca();

// retornamos el resultado de la operación
echo json_encode(array("error" => $id));

?>