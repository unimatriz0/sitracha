<?php

/**
 *
 * Class Fisico | fisico/fisico.class.php
 *
 * @package     Diagnostico
 * @subpackage  Fisico
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * datos antropométricos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Fisico {

    // declaración de las variables de clase
    protected $Link;                     // puntero a la base de datos
    protected $IdUsuario;                // clave del usuario
    protected $Id;                       // clave del registro
    protected $Protocolo;                // clave del protocolo del paciente
    protected $Visita;                   // clave de la visita
    protected $Ta;                       // tensión arterial
    protected $Peso;                     // peso del paciente
    protected $Fc;                       // frecuencia cardíaca
    protected $Spo;                      // saturación de oxígeno
    protected $Talla;                    // altura en metros
    protected $Bmi;                      // índice de masa corporal
    protected $Edema;                    // 0 falso - 1 verdadero
    protected $Sp;                       // 0 falso - 1 verdadero
    protected $Mvdisminuido;             // 0 falso - 1 verdadero
    protected $Crepitantes;              // 0 falso - 1 verdadero
    protected $Sibilancias;              // 0 falso - 1 verdadero
    protected $Usuario;                  // nombre del usuario
    protected $Fecha;                    // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializa las variables
        $this->initFisico();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase llamado
     * desde el constructor o desde la consulta cuando no
     * encuentra resultados
     */
    protected function initFisico(){

        // inicializa las variables
        $this->Id = 0;
        $this->Protocolo = 0;
        $this->Visita = 0;
        $this->Ta = "";
        $this->Peso = 0;
        $this->Fc = 0;
        $this->Spo = 0;
        $this->Talla = 0;
        $this->Bmi = 0;
        $this->Edema = 0;
        $this->Sp = 0;
        $this->Mvdisminuido = 0;
        $this->Crepitantes = 0;
        $this->Sibilancias = 0;
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // métodos de asignación de valores
    public function setId($id) {
        $this->Id = $id;
    }
    public function setProtocolo($protocolo) {
        $this->Protocolo = $protocolo;
    }
    public function setVisita($visita) {
        $this->Visita = $visita;
    }
    public function setTa($ta) {
        $this->Ta = $ta;
    }
    public function setPeso($peso) {
        $this->Peso = $peso;
    }
    public function setFc($fc) {
        $this->Fc = $fc;
    }
    public function setSpo($spo) {
        $this->Spo = $spo;
    }
    public function setTalla($talla) {
        $this->Talla = $talla;
    }
    public function setBmi($bmi) {
        $this->Bmi = $bmi;
    }
    public function setEdema($edema) {
        $this->Edema = $edema;
    }
    public function setSp($sp) {
        $this->Sp = $sp;
    }
    public function setMvdisminuido($mvdisminuido) {
        $this->Mvdisminuido = $mvdisminuido;
    }
    public function setCrepitantes($crepitantes) {
        $this->Crepitantes = $crepitantes;
    }
    public function setSibilancias($sibilancias) {
        $this->Sibilancias = $sibilancias;
    }

    // métodos de retorno de valores
    public function getId() {
        return $this->Id;
    }
    public function getProtocolo() {
        return $this->Protocolo;
    }
    public function getVisita() {
        return $this->Visita;
    }
    public function getTa() {
        return $this->Ta;
    }
    public function getPeso() {
        return $this->Peso;
    }
    public function getFc() {
        return $this->Fc;
    }
    public function getSpo() {
        return $this->Spo;
    }
    public function getTalla() {
        return $this->Talla;
    }
    public function getBmi() {
        return $this->Bmi;
    }
    public function getEdema() {
        return $this->Edema;
    }
    public function getSp() {
        return $this->Sp;
    }
    public function getMvdisminuido() {
        return $this->Mvdisminuido;
    }
    public function getCrepitantes() {
        return $this->Crepitantes;
    }
    public function getSibilancias() {
        return $this->Sibilancias;
    }
    public function getUsuario() {
        return $this->Usuario;
    }
    public function getFecha() {
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param visita - entero con la clave del registro
     * Método que recibe como parámetro la clave de una visita
     * y asigna los valores de la misma en las variables de clase
     */
    public function getDatosVisita($visita) {

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_fisico.id AS id,
                            diagnostico.v_fisico.protocolo AS protocolo,
                            diagnostico.v_fisico.idvisita AS visita,
                            diagnostico.v_fisico.fechavisita AS fecha,
                            diagnostico.v_fisico.ta AS ta,
                            diagnostico.v_fisico.peso AS peso,
                            diagnostico.v_fisico.fc AS fc,
                            diagnostico.v_fisico.spo AS spo,
                            diagnostico.v_fisico.talla AS talla,
                            diagnostico.v_fisico.bmi AS bmi,
                            diagnostico.v_fisico.edema AS edema,
                            diagnostico.v_fisico.sp AS sp,
                            diagnostico.v_fisico.mvdisminuido AS mvdisminuido,
                            diagnostico.v_fisico.crepitantes AS crepitantes,
                            diagnostico.v_fisico.sibilancias AS sibilancias,
                            diagnostico.v_fisico.usuario AS usuario
                     FROM diagnostico.v_fisico
                     WHERE diagnostico.v_fisico.idvisita = '$visita'; ";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);

        // si hubo resultados
        if ($resultado->rowCount() != 0) {

            // obtenemos el registro y asignamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Protocolo = $protocolo;
            $this->Visita = $visita;
            $this->Fecha = $fecha;
            $this->Ta = $ta;
            $this->Peso = $peso;
            $this->Fc = $fc;
            $this->Spo = $spo;
            $this->Talla = $talla;
            $this->Bmi = $bmi;
            $this->Edema = $edema;
            $this->Sp = $sp;
            $this->Mvdisminuido = $mvdisminuido;
            $this->Crepitantes = $crepitantes;
            $this->Sibilancias = $sibilancias;
            $this->Usuario = $usuario;

        // si no hubo registros
        } else {

            // inicializamos las variables
            $this->initFisico();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id - clave del registro afectado
     * Método público que ejecuta la consulta de edición
     * o eliminación según corresponda
     */
    public function grabaVisita(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaVisita();
        } else {
            $this->editaVisita();
        }

        // retornamos la clave
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de inserción
     */
    protected function nuevaVisita(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.fisico
                           (protocolo,
                            visita,
                            ta,
                            peso,
                            fc,
                            spo,
                            talla,
                            bmi,
                            edema,
                            sp,
                            mvdisminuido,
                            crepitantes,
                            sibilancias,
                            usuario)
                           VALUES
                           (:protocolo,
                            :visita,
                            :ta,
                            :peso,
                            :fc,
                            :spo,
                            :talla,
                            :bmi,
                            :edema,
                            :sp,
                            :mvdisminuido,
                            :crepitantes,
                            :sibilancias,
                            :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":protocolo",    $this->Protocolo);
        $psInsertar->bindParam(":visita",       $this->Visita);
        $psInsertar->bindParam(":ta",           $this->Ta);
        $psInsertar->bindParam(":peso",         $this->Peso);
        $psInsertar->bindParam(":fc",           $this->Fc);
        $psInsertar->bindParam(":spo",          $this->Spo);
        $psInsertar->bindParam(":talla",        $this->Talla);
        $psInsertar->bindParam(":bmi",          $this->Bmi);
        $psInsertar->bindParam(":edema",        $this->Edema);
        $psInsertar->bindParam(":sp",           $this->Sp);
        $psInsertar->bindParam(":mvdisminuido", $this->Mvdisminuido);
        $psInsertar->bindParam(":crepitantes",  $this->Crepitantes);
        $psInsertar->bindParam(":sibilancias",  $this->Sibilancias);
        $psInsertar->bindParam(":usuario",      $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de edición
     */
    protected function editaVisita(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.fisico SET
                            protocolo = :protocolo,
                            visita = :visita,
                            ta = :ta,
                            peso = :peso,
                            fc = :fc,
                            spo = :spo,
                            talla = :talla,
                            bmi = :bmi,
                            edema = :edema,
                            sp = :sp,
                            mvdisminuido = :mvdisminuido,
                            crepitantes = :crepitantes,
                            sibilancias = :sibilancias,
                            usuario = :usuario
                     WHERE diagnostico.fisico.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":protocolo",    $this->Protocolo);
        $psInsertar->bindParam(":visita",       $this->Visita);
        $psInsertar->bindParam(":ta",           $this->Ta);
        $psInsertar->bindParam(":peso",         $this->Peso);
        $psInsertar->bindParam(":fc",           $this->Fc);
        $psInsertar->bindParam(":spo",          $this->Spo);
        $psInsertar->bindParam(":talla",        $this->Talla);
        $psInsertar->bindParam(":bmi",          $this->Bmi);
        $psInsertar->bindParam(":edema",        $this->Edema);
        $psInsertar->bindParam(":sp",           $this->Sp);
        $psInsertar->bindParam(":mvdisminuido", $this->Mvdisminuido);
        $psInsertar->bindParam(":crepitantes",  $this->Crepitantes);
        $psInsertar->bindParam(":sibilancias",  $this->Sibilancias);
        $psInsertar->bindParam(":usuario",      $this->IdUsuario);
        $psInsertar->bindParam(":id",           $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo entero con el protocolo del paciente
     * @return resultset con los registros encontrados
     * Método protegido que retorna un vector con la nómina
     * de visitas del paciente, utilizado en la impresión
     * de historias agrupadas por estudio
     */
    public function nominaVisitas($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_fisico.id AS id,
                            diagnostico.v_fisico.protocolo AS protocolo,
                            diagnostico.v_fisico.idvisita AS idvisita,
                            diagnostico.v_fisico.fechavisita AS fecha,
                            diagnostico.v_fisico.tamax AS ta,
                            diagnostico.v_fisico.peso AS peso,
                            diagnostico.v_fisico.fc AS fc,
                            diagnostico.v_fisico.spo AS spo,
                            diagnostico.v_fisico.talla AS talla,
                            diagnostico.v_fisico.bmi AS bmi,
                            diagnostico.v_fisico.edema AS edema,
                            diagnostico.v_fisico.sp AS sp,
                            diagnostico.v_fisico.mvdisminuido AS mvdisminuido,
                            diagnostico.v_fisico.crepitantes AS crepitantes,
                            diagnostico.v_fisico.sibilancias AS sibilancias,
                            diagnostico.v_fisico.usuario AS usuario,
                            diagnostico.v_fisico.firma AS firma,
                            diagnostico.v_fisico.nombre AS nombre
                     FROM diagnostico.v_fisico
                     WHERE diagnostico.v_fisico.protocolo = '$protocolo'; ";

        // ejecutamos la consulta y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

}
?>