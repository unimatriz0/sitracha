/*

    Nombre: resultados.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 11/07/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones sobre la tabla
                 de resultados

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de resultados
 */
class Resultados {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initResultados();

        // el layer de los cuadros emergentes
        this.layerResultados = "";

        // reiniciamos la sesión
        sesion.reiniciar();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initResultados(){

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente presentando la nómina de
     * técnicas diagnósticas y sugiriendo las pruebas a efectuar
     * de acuerdo a las características del paciente
     */
    confirmaPruebas(){

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón aceptar de la grilla de
     * resultados, genera una entrada en la tabla de resultados
     * para cada técnica que se ha seleccionado
     */
    agregaPruebas(){

    }

}