<?php

/**
 *
 * Class Resultados | resultados/resultados.class.php
 *
 * @package     Diagnostico
 * @subpackage  Resultados
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/07/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que recibe como parámetro un número de protocolo,
 * obtiene los datos del registro y de la institución y
 * genera el protocolo para el paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Resultados {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct ($protocolo){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario activo y del laboratorio
            $this->IdUsuario = $_SESSION["Usuario"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que retorna el array con las pruebas a realizar,
     * utilizado en el ingreso de pacientes para verificar
     * las pruebas a realizar
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaPruebas(){

    }

    /**
     * Método que retorna verdadero si el paciente se
     * encuentra inmunocomprometido, recibe como parámetro
     * la clave del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idprotocolo - clave del registro
     * @return boolean
     */
    public function esInmuno($idprotocolo){

    }

    /**
     * Método protegido que retorna verdadero si el paciente
     * es transplantado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idprotocolo - clave del registro
     * @return boolean
     */
    protected function esTransplantado($idprotocolo){

    }

    /**
     * Método protegido que retorna verdadero si el paciente
     * ha recibido transfusiones
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idprotocolo - clave del registro
     * @return boolean
     */
    protected function esTransfundido($idprotocolo){

    }

    /**
     * Método que retorna verdader si el paciente tiene
     * alguna enfermedad del sistema inmunitario
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idprotocolo - clave del registro
     * @return boolean
     */
    protected function esEnfermo($idprotocolo){

    }

    /**
     * Método público que retorna verdadero si el paciente
     * es un bebe
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idprotocolo - clave del registro
     * @return boolean
     */
    public function esBebe($idprotocolo){

    }

}
?>
