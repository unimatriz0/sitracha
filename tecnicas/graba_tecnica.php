<?php

/**
 *
 * tecnicas/graba_tecnica.php
 *
 * @package     Diagnostico
 * @subpackage  Tecnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (23/12/2016)
 * @copyright   Copyright (c) 2017, INP/
 * 
 * Método que recibe por post los datos de la técnica y graba el 
 * registro en la base, retorna el resultado de la operación
 * 
*/

// inclusión e instanciación
require_once ("tecnicas.class.php");
$tecnica = new Tecnicas();

// si recibió la id
if (!empty($_POST["Id"])){
    $tecnica->setIdTecnica($_POST["Id"]);
}

// establecemos la técnica
$tecnica->setTecnica($_POST["Tecnica"]);
$tecnica->setNombreTecnica($_POST["Nombre"]);

// grabamos el registro
$id = $tecnica->grabaTecnica();

// retornamos el estado de la operación
echo json_encode(array("error" => $id));

?>