<?php

/**
 *
 * tecnicas/verifica_tecnica.class.php
 *
 * @package     Diagnostico
 * @subpackage  Tecnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (23/12/2016)
 * @copyright   Copyright (c) 2017, INP/
 * 
 * Método utilizado en el alta de las técnicas diagnósticas, recibe por 
 * get el nombre de una técnica y retorna verdadero si ya se encuentra
 * declarada
 * 
 * @param $_GET["tecnica"]
 * 
*/

// incluimos e instanciamos la clase
require_once ("tecnicas.class.php");
$tecnica = new Tecnicas();

// verificamos si existe
$resultado = $tecnica->verificaTecnica($_GET["tecnica"]);

// retornamos el estado de la operación
echo json_encode(array("error" => $resultado));

?>