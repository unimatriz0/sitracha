<?php

/**
 *
 * Class Tecnicas | tecnicas/tecnicas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Tecnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/12/2016)
 * @copyright   Copyright (c) 2017, INP/
 * 
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones sobre la tabla de 
 * Técnicas Diagnósticas reconocidas 
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Tecnicas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdTecnica;                // clave del registro
    protected $Tecnica;                  // nombre de la técnica
    protected $NombreTecnica;            // nombre completo de la técnica
    protected $IdUsuario;                // clave del usuario

    // definición de variables
    protected $Link;                     // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdTecnica = 0;
        $this->NombreTecnica = "";
        $this->Tecnica = "";

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdTecnica($idtecnica){

        // verifica que sea un número
        if (!is_numeric($idtecnica)){

            // abandona por error
            echo "La clave de la técnica debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdTecnica = $idtecnica;

        }

    }
    public function setTecnica($tecnica){
        $this->Tecnica = $tecnica;
    }
    public function setNombreTecnica($nombre){
        $this->NombreTecnica = $nombre;
    }

    /**
     * Método que retorna la nómina completa de las Técnicas 
     * Diagnósticas registradas, retorna un array asociativo 
     * con id y el nombre de cada una de las técnicas, 
     * utilizado en los combos y en las grillas de abm de técnicas
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaTecnicas(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_tecnicas.id_tecnica AS id_tecnica,
                            diagnostico.v_tecnicas.tecnica AS tecnica,
                            diagnostico.v_tecnicas.nombre AS nombre,
                            diagnostico.v_tecnicas.fecha AS fecha_alta,
                            diagnostico.v_tecnicas.usuario AS usuario
                     FROM diagnostico.v_tecnicas
                     ORDER BY diagnostico.v_tecnicas.tecnica;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $listaTecnicas = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $listaTecnicas;

    }

    /**
     * Método que recibe como parámetro la cadena con el
     * nombre de la técnica y retorna su clave
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $tecnica - nombre de la técnica
     * @return int - clave de la técnica
     */
    public function getClaveTecnica($tecnica){

        // componemos la consulta
        $consulta = "SELECT diagnostico.tecnicas.ID AS idtecnica
                     FROM diagnostico.tecnicas
                     WHERE diagnostico.tecnicas.TECNICA = '$tecnica';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos el vector
        return extract($registro);

    }

    /**
     * Método que graba los datos del registro en la base de datos,
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idtecnica - clave del registro insertado / editado
     */
    public function grabaTecnica(){

        // si es un alta
        if ($this->IdTecnica == 0) {

            // ejecutamos la consulta
            $this->nuevaTecnica();

        // si está editando
        } else {

            // ejecutamos la consulta
            $this->editaTecnica();

        }

        // retornamos la id
        return $this->IdTecnica;

    }

    /**
     * Método que inserta un nuevo registro en la tabla de técnicas
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaTecnica(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diagnostico.tecnicas
                            (tecnica,
                             nombre,
                             id_usuario)
                            VALUES
                            (:tecnica,
                             :nombre,
                             :idusuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":tecnica", $this->Tecnica);
        $psInsertar->bindParam(":nombre", $this->NombreTecnica);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdTecnica = $this->Link->lastInsertId();

    }

    /**
     * Método que edita el registro de la tabla de técnicas
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaTecnica(){

        // compone la consulta de actualización
        $consulta = "UPDATE diagnostico.tecnicas SET
                            tecnica = :tecnica,
                            nombre = :nombre,
                            id_usuario = :idusuario
                     WHERE tecnicas.ID = :idtecnica;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":tecnica", $this->Tecnica);
        $psInsertar->bindParam(":nombre", $this->NombreTecnica);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":idtecnica", $this->IdTecnica);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro el nombre de una técnica, la
     * busca en la base de datos y retorna el número de registros
     * encontrados.
     * Utilizado para evitar el ingreso de registros duplicados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $tecnica - nombre de la técnica
     * @return int $registros - número de registros encontrados
     */
    public function verificaTecnica($tecnica){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(*) AS registros
                     FROM diagnostico.tecnicas
                     WHERE diagnostico.tecnicas.TECNICA = '$tecnica';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // obtenemos el array con todos los registros
        $fila = $resultado->fetch(PDO::FETCH_ASSOC);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($fila, CASE_LOWER);

        // obtenemos el registro y verificamos el resultado
        extract($registro);

        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

}
