/*

 Nombre: tecnicas.js
 Fecha: 23/12/2016
 Autor: Lic. Claudio Invernizzi
 E-Mail: cinvernizzi@gmail.com
 Licencia: GPL
 Producido en: INP - Dr. Mario Fatala Chaben
 Buenos Aires - Argentina
 Comentarios: Clase que controla las operaciones sobre el formulario de técnicas

 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el abm de técnicas diagnósticas
 */
class Tecnicas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initTecnicas();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initTecnicas(){

        // declaración de variables
        this.Id = 0;
        this.Tecnica = "";
        this.Nombre = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario
     */
    muestraTecnicas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("tecnicas/form_tecnicas.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id clave del registro
     * Método que recibe como parámetro la id de una técnica o nada si
     * es un alta y verifica el formulario
     */
    verificaTecnica(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está dando un alta
        if (id === undefined){
            id = "nueva";
        }

        // asignamos en la variable de clase
        this.Id = id;

        // verifica se halla declarado nombre
        this.Tecnica = document.getElementById("tecnica_" + id).value;
        if (this.Tecnica == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la técnica";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tecnica_" + id).focus();
            return false;

        }

        // verifica se halla ingresado el nombre completo
        this.Nombre = document.getElementById("nombre_" + id).value;
        if (this.Nombre == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre completo de la técnica";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_" + id).focus();
            return false;

        }

        // si está dando un alta verifica que la técnica no
        // esté declarada
        if (this.Id == "nueva"){

            // si existe
            if (this.validarTecnica(this.Tecnica)){

                // presenta el mensaje y retorna
                mensaje = "Esa técnica ya se encuentra declarada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // grabamos el registro en la base
        this.grabaTecnica();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario, crea el objeto
     * formdata y lo envía por ajax al servidor
     */
    grabaTecnica(){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id != "nueva"){
            formulario.append("Id", this.Id);
        }

        // agrega la técnica convirtiéndola a mayúsculas
        formulario.append("Tecnica", this.Tecnica.toUpperCase());
        formulario.append("Nombre", this.Nombre);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "tecnicas/graba_tecnica.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // inicializamos las variables
                    tecnicas.initTecnicas();

                    // recarga el formulario para reflejar los cambios
                    tecnicas.muestraTecnicas();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean}
     * Método llamado en el alta de una técnica, verifica que esta no se
     * encuentre ya declarada en la base
     */
    validarTecnica(){

        // declaración de variables
        var resultado;

        // llamamos la rutina php
        $.ajax({
            url: 'tecnicas/verifica_tecnica.php?tecnica='+this.Tecnica,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: true,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error === true){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un objeto html
     * @param {int} idtecnica clave de la técnica preseleccionada
     * Método que recibe como parámetro la id de un objeto del formulario
     * y en todo caso (optativo) la clave del elemento preseleccionado
     * obtiene por ajax la nómina de técnicas y la carga en el
     * select que recibe como parámetro
     */
    listaTecnicas(idelemento, idtecnica){

        // limpia el combo
        $("#" + idelemento).html('');

        // lo llamamos asincrónico
        $.ajax({
            url: "tecnicas/lista_tecnicas.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si existe la clave y es igual
                    if (typeof(idtecnica) != "undefined"){

                        // si es la misma
                        if (idtecnica == data[i].Id){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Tecnica + "</option>");

                        // si son distintos
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Tecnica + "</option>");

                        }

                    // si no recibió la clave de la tecnica recorre el
                    // bucle agregando
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Tecnica + "</option>");
                    }

                }

        }});

    }

}
