<?php

/**
 *
 * tecnicas/lista_tecnicas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Tecnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (20/02/2018)
 * @copyright   Copyright (c) 2017, INP/
 * 
 * Método que retorna un array json con la nómina de las técnicas diagnósticas
 * registradas en la base
 * 
*/

// incluimos e instanciamos la clase
require_once("tecnicas.class.php");
$tecnicas = new Tecnicas();

// obtenemos la matriz
$nomina = $tecnicas->nominaTecnicas();

// declaramos el array
$jsondata = array();

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al vector
    $jsondata[] = array("Id" => $id_tecnica,
                        "Tecnica" => $tecnica);

}

// retornamos el vector
echo json_encode($jsondata);

?>