<?php

/**
 *
 * tecnicas/form_tecnicas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Tecnicas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (23/12/2016)
 * @copyright   Copyright (c) 2017, INP/
 * 
 * Método que arma el formulario para el abm de técnicas
 * 
*/

// inclusión e implementación de las clases
require_once ("tecnicas.class.php");
$tecnicas = new Tecnicas();

// obtenemos la nómina de las técnicas
$resultado = $tecnicas->nominaTecnicas();

// abrimos la sesión y obtenemos el usuario
session_start();
$usuario_sesion = $_SESSION["Usuario"];
session_write_close();

// definimos la tabla
echo "<table width='60%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th>Técnica</th>";
echo "<th>Nombre</th>";
echo "<th>Alta</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// iniciamos un bucle recorriendo el vector
foreach ($resultado AS $registro){

    // obtenemos el registro
    extract($registro);

    // si es distinto de otra
    if ($tecnica != "OTRA"){

        // abrimos la fila
        echo "<tr>";

        // presentamos el registro para edición
        echo "<td align='center'>";
        echo "<span class='tooltip'
                    title='Nombre de la Técnica'>";
        echo "<input type='text'
               name='tecnica_$id_tecnica'
               id='tecnica_$id_tecnica'
               size='20'
               value='$tecnica'>";
        echo "</span>";
        echo "</td>";

        // el nombre completo de la técnica
        echo "<td align='center'>";
        echo "<span class='tooltip'
                    title='Nombre completo de la técnica'>";
        echo "<input type='text'
               name='nombre_$id_tecnica'
               id='nombre_$id_tecnica'
               size='40'
               value='$nombre'>";
        echo "</span>";
        echo "</td>";

        // fecha de alta y usuario simplemente lo presentamos
        echo "<td align='center'>$fecha_alta</td>";
        echo "<td align='center'>$usuario</td>";

        // agregamos el enlace para grabar
        echo "<td align='center'>";
        echo "<input type='button'
                   name='btnGrabaTecnica'
                   id='btnGrabaTecnica'
                   title='Pulse para grabar el registro'
                   onClick='tecnicas.verificaTecnica($id_tecnica)'
                   class='botongrabar'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

}

// agregamos una fila para insertar un nuevo registro
echo "<tr>";

    // presentamos el registro
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Nombre de la técnica'>";
    echo "<input type='text'
           name='tecnica_nueva'
           id='tecnica_nueva'
           size='20'>";
    echo "</span>";
    echo "</td>";

    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Nombre completo de la técnica'>";
    echo "<input type='text'
           name='nombre_nueva'
           id='nombre_nueva'
           size='40'>";
    echo "</span>";
    echo "</td>";

    // obtenemos la fecha actual
    $fecha_alta = date("d/m/Y");

    // fecha de alta y usuario simplemente lo presentamos
    echo "<td align='center'>$fecha_alta</td>";
    echo "<td align='center'>$usuario_sesion</td>";

    // agregamos el enlace para grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnNuevaTecnica'
           id='btnNuevaTecnica'
           title='Inserta la Técnica en la base'
           onClick='tecnicas.verificaTecnica()'
           class='botonagregar'>";
    echo "</td>";

echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>