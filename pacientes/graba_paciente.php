<?php

/**
 *
 * pacientes/graba_paciente.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (26/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario de pacientes
 * y ejecuta la consulta en el servidor, retorna el protocolo y
 * el número de historia clínica
*/

// incluimos e instanciamos las clases
require_once ("pacientes.class.php");
$paciente = new Pacientes();

// si recibió el protocolo
if (!empty($_POST["Protocolo"])){
    $paciente->setProtocolo($_POST["Protocolo"]);
}

// si recibió la historia clínica
if (!empty($_POST["HistoriaClinica"])){
    $paciente->setHistoria($_POST["HistoriaClinica"]);
}

// fijamos el resto de las propiedades
$paciente->setApellido($_POST["Apellido"]);
$paciente->setNombre($_POST["Nombre"]);
$paciente->setDocumento($_POST["Documento"]);
$paciente->setIdDocumento($_POST["TipoDocumento"]);
$paciente->setFechaNacimiento($_POST["FechaNacimiento"]);
$paciente->setEdad($_POST["Edad"]);
$paciente->SetIdSexo($_POST["Sexo"]);
$paciente->setIdEstadoCivil($_POST["EstadoCivil"]);
$paciente->setHijos($_POST["Hijos"]);
$paciente->setDireccion($_POST["Direccion"]);
$paciente->setTelefono($_POST["Telefono"]);
$paciente->setCelular($_POST["Celular"]);
$paciente->setIdCompania($_POST["Compania"]);
$paciente->setMail($_POST["Mail"]);
$paciente->setIdLocNacimiento($_POST["LocNacimiento"]);
$paciente->setCoordenadasNac($_POST["CoordNacimiento"]);
$paciente->setIdLocResidencia($_POST["LocResidencia"]);
$paciente->setCoordenadasResid($_POST["CoordResidencia"]);
$paciente->setIdLocalidadMadre($_POST["LocMadre"]);
$paciente->setMadrePositiva($_POST["MadrePositiva"]);
$paciente->setOcupacion($_POST["Ocupacion"]);
$paciente->setObraSocial($_POST["ObraSocial"]);
$paciente->setIdMotivo($_POST["Motivo"]);
$paciente->setIdDerivacion($_POST["Derivacion"]);
$paciente->setTratamiento($_POST["Tratamiento"]);
$paciente->setComentarios($_POST["Comentarios"]);

// grabamos el registro
$paciente->grabaPaciente();

// retornamos el protocolo y la historia
echo json_encode(array("Protocolo" => $paciente->getProtocolo(),
                       "Historia" => $paciente->getHistoria()));

?>