<?php

/**
 *
 * Class Protocolos | pacientes/protocolos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Protocolos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// define la ruta a las fuentes pdf (lo llamamos desde
// el script de remitos y el path queda en stock)
define('FPDF_FONTPATH', '../clases/fpdf/font');

// la clase pdf
require_once ("../clases/fpdf/code39/code39.php");

// la clase qrcode
require_once('../clases/fpdf/qrcode/qrcode.class.php');

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que recibe como parámetro un número de protocolo,
 * obtiene los datos del registro y de la institución y
 * genera el protocolo para el paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Protocolos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // variables de la tabla de pacientes
    protected $Protocolo;             // número del protocolo
    protected $IdLaboratorio;         // clave del laboratorio propietario
    protected $HistoriaClinica;       // número de historia clínica
    protected $Nombre;                // apellido y nombre del paciente
    protected $Documento;             // número de documento
    protected $TipoDocumento;         // tipo de documento

    // datos del laboratorio propietario del registro
    protected $Laboratorio;           // nombre del laboratorio
    protected $Logo;                  // logo de la institución
    protected $FechaEntrega;          // fecha de entrega de resultados

    /**
     * Constructor de la clase, establece la conexión con la base
     * @param int $protocolo - número de protocolo a imprimir
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct ($protocolo){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // asignamos en la variable de clase
        $this->Protocolo = $protocolo;

        // inicializamos las variables de clase
        $this->IdLaboratorio = 0;
        $this->Laboratorio = "";
        $this->HistoriaClinica = "";
        $this->Nombre = "";
        $this->Documento = "";
        $this->TipoDocumento = "";
        $this->Logo = "";

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario activo y del laboratorio
            $this->IdUsuario = $_SESSION["Usuario"];

        }

        // cerramos sesión
        session_write_close();

        // obtenemos los datos del paciente
        $this->getDatosPaciente();

        // obtenemos los datos de la institución
        $this->getDatosInstitucion();

        // inicializamos el documento
        $this->initProtocolo();

        // imprimimos el texto
        $this->textoProtocolo();

        // lo cerramos y creamos en el directorio temporal
        $this->Documento->Output("../temp/protocolo.pdf", 'F');

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que a partir de la clave del protocolo obtiene los
     * datos del paciente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getDatosPaciente(){

        // inicializamos las variables
        $id_laboratorio = 0;
        $historia_clinica = "";
        $nombre = "";
        $documento = "";
        $tipo_documento = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_personas.id_laboratorio AS id_laboratorio,
                            diagnostico.v_personas.historia_clinica AS historia_clinica,
                            CONCAT(diagnostico.v_personas.apellido, ', ', diagnostico.v_personas.nombre) AS nombre,
                            diagnostico.v_personas.documento AS documento,
                            diagnostico.v_personas.tipo_documento AS tipo_documento
                     FROM diagnostico.v_personas
                     WHERE diagnostico.v_personas.protocolo = '$this->Protocolo';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y lo pasamos a variables
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($registro);

        // asignamos en las variables de clase
        $this->IdLaboratorio = $id_laboratorio;
        $this->HistoriaClinica = $historia_clinica;
        $this->Nombre = $nombre;
        $this->TipoDocumento = $tipo_documento;
        $this->Documento = number_format($documento, 0, ",", ".");


    }

    /**
     * Método que obtiene los datos de la institución correspondiente
     * al protocolo del paciente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getDatosInstitucion(){

        $consulta = "SELECT diagnostico.v_laboratorios.laboratorio AS laboratorio,
                            diagnostico.v_laboratorios.logo AS logo
                     FROM diagnostico.v_laboratorios
                     WHERE diagnostico.v_laboratorios.id = '$this->IdLaboratorio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro
        extract($registro);

        // asignamos en las variables de clase (la imagen la tenemos
        // que decodificar porque la tenemos como una cadena base64
        $this->Laboratorio = $laboratorio;
        $this->Logo = $logo;

    }

    /**
     * Método que inicializa el documento y configura sus parámetros
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function initProtocolo(){

        // instanciamos la clase pdf
        $this->Documento = new PDF_Code39();

        // establecemos las propiedades del documento
        $this->Documento->SetAuthor("Claudio Invernizzi");
        $this->Documento->SetCreator("INP - Mario Fatala Chaben");
        $this->Documento->SetSubject("Protocolo de Muestras", true);
        $this->Documento->SetTitle("Protocolo de Muestras", true);
        $this->Documento->SetAutoPageBreak(true, 10);

        // agrega una fuente unicode
        $this->Documento->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->Documento->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // fijamos el margen izquierdo y derecho
        $this->Documento->SetLeftMargin(15);
        $this->Documento->setRightMargin(15);

        // agregamos la página
        $this->Documento->AddPage("P", "A4");

    }

    /**
     * Método que arma el encabezado del protocolo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function encabezadoProtocolo(){

    }

    /**
     * Método que imprime el texto del protocolo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function textoProtocolo(){

    }

}