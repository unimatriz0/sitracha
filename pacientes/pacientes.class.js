/*

    Nombre: pacientes.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 06/02/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de pacientes

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de pacientes
 */
class Pacientes {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initPacientes();

        // el layer de los cuadros emergentes
        this.layerPacientes = "";

        // cargamos el formulario
        $("#form_pacientes").load("pacientes/form_pacientes.html");

        // reiniciamos la sesión
        sesion.reiniciar();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que reinicia las variables de clase, utilizado en varios
     * eventos de la clase
     */
    initPacientes(){

        // inicializamos las variables de clase
        this.Protocolo = 0;                    // clave del registro
        this.IdLaboratorio = 0;                // clave del laboratorio
        this.Laboratorio = "";                 // nombre del laboratorio
        this.HistoriaClinica = "";             // número de historia clínica
        this.Apellido = "";                    // apellido del paciente
        this.Nombre = "";                      // nombre del paciente
        this.Documento = "";                   // número de documento
        this.TipoDocumento = 0;                // clave del tipo de documento
        this.FechaNacimiento = "";             // fecha de nacimiento
        this.Edad = 0;                         // edad del paciente en años
        this.Sexo = 0;                         // clave del sexo
        this.EstadoCivil = 0;                  // clave del estado civil
        this.Hijos = 0;                        // número de hijos
        this.Direccion = "";                   // dirección postal
        this.Telefono = "";                    // número de teléfono
        this.Celular = "";                     // número de celular
        this.Compania = 0;                     // clave de la compañia de celular
        this.Mail = "";                        // dirección de correo electrónico
        this.PaisNacimiento = "";              // nombre del país de nacimiento
        this.ProvinciaNacimiento = "";         // nombre de la provincia de nacimiento
        this.IdLocalidadNacimiento = "";       // clave de la localidad de nacimiento
        this.LocalidadNacimiento = "";         // nombre de la localidad de nacimiento
        this.CoordenadasNacimiento = "";       // coordenadas gps de nacimiento
        this.PaisResidencia = "";              // nombre del país de residencia
        this.ProvinciaResidencia = "";         // nombre de la provincia de residencia
        this.IdLocalidadResidencia = "";       // clave de la localidad de residencia
        this.LocalidadResidencia = "";         // nombre de la localidad de residencia
        this.CoordenadasResidencia = "";       // coordenadas gps de la residencia
        this.PaisMadre = "";                   // pais de nacimiento de la madre
        this.ProvinciaMadre = "";              // la provincia de nacimiento de la madre
        this.IdLocalidadMadre = "";            // clave de la localidad de la madre
        this.LocalidadMadre = "";              // nombre de la localidad de la madre
        this.MadrePositiva = "";               // indica si la madre es seropositiva
        this.Ocupacion = "";                   // descripción de la ocupación
        this.ObraSocial = "";                  // nombre de la obra social
        this.Motivo = 0;                       // clave del motivo de consulta
        this.Derivacion = 0;                   // clave de la derivación
        this.Tratamiento = "";                 // indica si tuvo tratamiento
        this.IdUsuario = 0;                    // clave del usuario
        this.Usuario = "";                     // nombre del usuario
        this.FechaAlta = "";                   // fecha de alta del registro
        this.Comentarios = "";                 // observaciones del usuario

        // variables de control de declaración de tablas auxiliares
        this.Enfermedades = false;             // si declaró enfermedades
        this.Transfusiones = false;            // si declaró transfusiones
        this.Transplantes = false;             // si declaró transplantes
        this.Congenito = false;                // datos de chagas congenito
        this.Entrevistas = false;              // datos de entrevistas

    }

    // Métodos públicos de asignación de valores a las variables de clase
    setProtocolo(protocolo){
        this.Protocolo = protocolo;
    }
    setIdLaboratorio(idlaboratorio){
        this.IdLaboratorio = idlaboratorio;
    }
    setLaboratorio(laboratorio){
        this.Laboratorio = laboratorio;
    }
    setHistoriaClinica(historiaclinica){
        this.HistoriaClinica = historiaclinica;
    }
    setApellido(apellido){
        this.Apellido = apellido;
    }
    setNombre(nombre){
        this.Nombre = nombre;
    }
    setDocumento(documento){
        this.Documento = documento;
    }
    setTipoDocumento(tipodocumento){
        this.TipoDocumento = tipodocumento;
    }
    setFechaNacimiento(fechanacimiento){
        this.FechaNacimiento = fechanacimiento;
    }
    setEdad(edad){
        this.Edad = edad;
    }
    setSexo(sexo){
        this.Sexo = sexo;
    }
    setEstadoCivil(estadocivil){
        this.EstadoCivil = estadocivil;
    }
    setHijos(hijos){
        this.Hijos = hijos;
    }
    setDireccion(direccion){
        this.Direccion = direccion;
    }
    setTelefono(telefono){
        this.Telefono = telefono;
    }
    setCelular(celular){
        this.Celular = celular;
    }
    setCompania(compania){
        this.Compania = compania;
    }
    setMail(mail){
        this.Mail = mail;
    }
    setPaisNacimiento(pais){
        this.PaisNacimiento = pais;
    }
    setProvinciaNacimiento(provincia){
        this.ProvinciaNacimiento = provincia;
    }
    setLocalidadNacimiento(localidad){
        this.LocalidadNacimiento = localidad;
    }
    setIdLocalidadNacimiento(localidad){
        this.IdLocalidadNacimiento = localidad;
    }
    setCoordenadasNacimiento(coordenadas){
        this.CoordenadasNacimiento = coordenadas;
    }
    setPaisResidencia(pais){
        this.PaisResidencia = pais;
    }
    setProvinciaResidencia(provincia){
        this.ProvinciaResidencia = provincia;
    }
    setLocalidadResidencia(localidad){
        this.LocalidadResidencia = localidad;
    }
    setIdLocalidadResidencia(localidad){
        this.IdLocalidadResidencia = localidad;
    }
    setCoordenadasResidencia(coordenadas){
        this.CoordenadasResidencia = coordenadas;
    }
    setPaisMadre(pais){
        this.PaisMadre = pais;
    }
    setProvinciaMadre(provincia){
        this.ProvinciaMadre = provincia;
    }
    setLocalidadMadre(localidad){
        this.LocalidadMadre = localidad;
    }
    setIdLocalidadMadre(localidad){
        this.IdLocalidadMadre = localidad;
    }
    setMadrePositiva(positiva){
        this.MadrePositiva = positiva;
    }
    setOcupacion(ocupacion){
        this.Ocupacion = ocupacion;
    }
    setObraSocial(obrasocial){
        this.ObraSocial = obrasocial;
    }
    setMotivo(motivo){
        this.Motivo = motivo;
    }
    setDerivacion(derivacion){
        this.Derivacion = derivacion;
    }
    setTratamiento(tratamiento){
        this.Tratamiento = tratamiento;
    }
    setIdUsuario(idusuario){
        this.IdUsuario = idusuario;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setComentarios(comentarios){
        this.Comentarios = comentarios;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que setea la variable control de declaraciòn
     * de enfermedades
     */
    setEnfermedades(){
        this.Enfermedades = true;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que setea la variable control de declaración
     * de transfusiones
     */
    setTransfusiones(){
        this.Transfusiones = true;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que setea la variable control de declaración
     * de transplantes
     */
    setTransplantes(){
        this.Transplantes = true;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que setea la variable control de entrevistas
     */
    setEntrevistas(){
        this.Entrevistas = true;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que setea la variable control de declaración
     * de chagas congénito
     */
    setCongenito(){
        this.Congenito = true;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de pacientes
     */
    nuevoPaciente(){

        // reiniciamos el formulario
        this.initFormPacientes();

        // reiniciamos las variables de clase
        this.initPacientes();

        // presentamos el diálogo emergente solicitando
        // el número de documento
        var contenido = "Ingrese el número de documento.<br>";
        contenido += "<form name='nuevo_paciente'>";
        contenido += "<p><b>Documento:</b> ";
        contenido += "<input type='text' name='texto' size='30'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Aceptar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='pacientes.validaPaciente()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerPacientes = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    content: contenido,
                                    title: 'Nuevo Paciente',
                                    theme: 'TooltipBorder',
                                    width: 300,
                            });
        this.layerPacientes.open();

        // fijamos el foco
        document.nuevo_paciente.texto.focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el diálogo emergente de nuevo paciente, obtiene
     * el número de documento ingresado y verifica si existe en la base
     * según el caso, retorna (dejando el formulario en blanco) o carga
     * los datos del paciente en el formulario
     */
    validaPaciente(){

        // declaración de variables
        var mensaje;
        var documento = document.nuevo_paciente.texto.value;

        // si no ingresó el número de documento
        if (documento == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el número de documento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.nuevo_paciente.texto.focus();
            return false;

        }

        // destruimos el layer
        this.layerPacientes.destroy();

        // llamamos por ajax
        $.ajax({
            url: "pacientes/valida_paciente.php?Documento=" + documento,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // verificamos la seguridad
                pacientes.seguridadPacientes();

                // si recibió alto
                if (data.Protocolo != 0){

                    // obtenemos el registro y lo mostramos
                    pacientes.getDatosPaciente(data.Protocolo);

                // si no lo encontró
                } else {

                    // inicializamos las variables de clase
                    pacientes.initPacientes();

                    // fijamos a verdadero los switch de tablas auxiliares
                    // ya que en la ediciòn no se exige la apertura
                    pacientes.setEnfermedades();
                    pacientes.setTransfusiones();
                    pacientes.setTransplantes();
                    pacientes.setEntrevistas();
                    pacientes.setCongenito();

                    // fijamos el valor en el formulario
                    document.getElementById("documento_paciente").value = data.Documento;

                    // fijamos el foco
                    document.getElementById("apellido_paciente").focus();

                }

            }

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo de búsqueda de pacientes
     */
    buscaPaciente(){

        // definimos el contenido del layer
        var contenido = "Ingrese el texto a buscar, el sistema ";
        contenido += "buscará en el Nombre, el número de Documento, ";
        contenido += "el Protocolo y la Historia Clínica.<br>";
        contenido += "<form name='busca_paciente'>";
        contenido += "<p><b>Texto:</b> ";
        contenido += "<input type='text' name='texto' size='15'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Buscar' ";
        contenido += "       class='boton_buscar' ";
        contenido += "       onClick='pacientes.encuentraPaciente()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerPacientes = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    content: contenido,
                                    title: 'Buscar Paciente',
                                    theme: 'TooltipBorder',
                                    width: 300,
                            });
        this.layerPacientes.open();

        // fijamos el foco
        document.busca_paciente.texto.focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre el botón de búsqueda de pacientes
     * abre el layer con los resultados
     */
    encuentraPaciente(){

        // declaración de variables
        var texto = document.busca_paciente.texto.value;

        // si no ingresó nada
        if (texto == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar un texto a buscar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.busca_paciente.texto.focus();
            return false;

        }

        // destruimos el layer
        this.layerPacientes.destroy();

        // cargamos la grilla
        this.layerPacientes = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        title: 'Resultados de la Búsqueda',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        draggable: 'title',
                        ajax: {
                        url: 'pacientes/buscar.php?texto='+texto,
                        reload: 'strict'
                    }
                });
        this.layerPacientes.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario de pacientes
     */
    initFormPacientes(){

        // inicializamos los campos del formulario
        document.getElementById("laboratorio_paciente").value = "";
        document.getElementById("coord_nac_paciente").value = "";
        document.getElementById("coord_resid_paciente").value = "";
        document.getElementById("protocolo_paciente").value = "";
        document.getElementById("historia_paciente").value = "";
        document.getElementById("tipo_doc_paciente").value = 0;
        document.getElementById("documento_paciente").value = "";
        document.getElementById("mail_paciente").value = "";
        document.getElementById("apellido_paciente").value = "";
        document.getElementById("nombre_paciente").value = "";
        document.getElementById("fecha_nac_paciente").value = "";
        document.getElementById("edad_paciente").value = "";
        document.getElementById("sexo_paciente").value = 0;
        document.getElementById("estado_civil_paciente").value = 0;
        document.getElementById("hijos_paciente").value = "";
        document.getElementById("direccion_paciente").value = "";
        document.getElementById("telefono_paciente").value = "";
        document.getElementById("compania_paciente").value = 0;
        document.getElementById("celular_paciente").value = "";
        document.getElementById("pais_nac_paciente").value = "";
        document.getElementById("jur_nac_paciente").value = "";
        document.getElementById("loc_nac_paciente").value = "";
        document.getElementById("id_loc_nac_paciente").value = "";
        document.getElementById("pais_res_paciente").value = "";
        document.getElementById("jur_res_paciente").value = "";
        document.getElementById("loc_res_paciente").value = "";
        document.getElementById("id_loc_res_paciente").value = "";
        document.getElementById("pais_nac_madre").value = "";
        document.getElementById("jur_nac_madre").value = "";
        document.getElementById("loc_nac_madre").value = "";
        document.getElementById("id_loc_nac_madre").value = "";
        document.getElementById("madre_positiva").value = "";
        document.getElementById("ocupacion_paciente").value = "";
        document.getElementById("obra_soc_paciente").value = "";
        document.getElementById("motivo_paciente").value = 0;
        document.getElementById("derivo_paciente").value = 0;
        document.getElementById("tratamiento_paciente").value = "";
        document.getElementById("observaciones_paciente").value = "";
        CKEDITOR.instances['observaciones_paciente'].setData();

        // el usuario y la fecha de alta la seteamos por defecto
        document.getElementById("usuario_paciente").value = sessionStorage.getItem("Usuario");
        document.getElementById("alta_paciente").value = fechaActual();

        // inicializamos el formulario y las variables de antecedentes
        antecedentes.initAntecedentes();
        antecedentes.limpiaFormulario();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario antes de enviarlo por ajax
     */
    verificaPaciente(){

        // declaración de variables
        var mensaje;

        // el laboratorio lo asigna por la variable de sesión en php

        // si existen las coordenadas de nacimiento
        if (document.getElementById("coord_nac_paciente").value != ""){

            // asigna en la variable de clase
            this.CoordenadasNacimiento = document.getElementById("coord_nac_paciente").value;

        }

        // si existen las coordenadas de residencia
        if (document.getElementById("coord_resid_paciente").value != ""){

            // asigna en la variable de clase
            this.CoordenadasResidencia = document.getElementById("coord_resid_paciente").value;

        }

        // si está editando
        if (document.getElementById("protocolo_paciente").value != ""){

            // asigna en la variable de clase
            this.Protocolo = document.getElementById("protocolo_paciente").value;

        }

        // si está editando
        if (document.getElementById("historia_paciente").value != ""){

            // asigna en la variable de clase
            this.HistoriaClinica = document.getElementById("historia_paciente").value;

        }

        // si no seleccionó el documento
        if (document.getElementById("tipo_doc_paciente").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el tipo de documento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tipo_doc_paciente").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.TipoDocumento = document.getElementById("tipo_doc_paciente").value;

        }

        // si no ingresó el número de documento
        if (document.getElementById("documento_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el número de documento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("documento_paciente").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Documento = document.getElementById("documento_paciente").value;

        }

        // si no ingresó el mail
        if (document.getElementById("mail_paciente").value == ""){

            // presena el alerta
            mensaje = "Falta el mail del paciente, imposible contactar";
            new jBox('Notice', {content: mensaje, color: 'red'});

        // si ingresó
        } else {

            // verifica el formato
            if (!echeck(document.getElementById("mail_paciente").value)){

                // presenta el mensaje
                mensaje = "El mail parece ser incorrecto";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("mail_paciente").focus();
                return false;

            // si está correcto
            } else {

                // asigna en la variable de clase
                this.Mail = document.getElementById("mail_paciente").value;

            }

        }

        // si no ingresó el apellido
        if (document.getElementById("apellido_paciente").value == ""){

            // presenta el mensaje
            mensaje = "Indique el apellido del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("apellido_paciente").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Apellido = document.getElementById("apellido_paciente").value;

        }

        // si no ingresó el nombre
        if (document.getElementById("nombre_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el nombre del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_paciente").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Nombre = document.getElementById("nombre_paciente").value;

        }

        // si no ingresó ni la fecha de nacimiento ni la edad
        if (document.getElementById("fecha_nac_paciente").value == "" && document.getElementById("edad_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la fecha de nacimiento o la edad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_nac_paciente").focus();
            return false;

        }

        // si ingresó la fecha de nacimiento
        if (document.getElementById("fecha_nac_paciente").value != ""){

            // asigna en la variable de clase
            this.FechaNacimiento = document.getElementById("fecha_nac_paciente").value;

            // si no ingresó la edad
            if (document.getElementById("edad_paciente").value == ""){

                // calcula la edad y la asigna
                var dias = FechaDiff(this.FechaNacimiento, fechaActual());
                var anios = Math.trunc(dias / 365);
                document.getElementById("edad_paciente").value = anios;

            }

        }

        // si ingresó la edad
        if (document.getElementById("edad_paciente").value != ""){

            // lo asigna en la variable de clase
            this.Edad = document.getElementById("edad_paciente").value;

        }

        // si no ingresó el sexo
        if (document.getElementById("sexo_paciente").value == 0){

            // presenta el mensaje
            mensaje = "Seleccione el sexo del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("sexo_paciente").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Sexo = document.getElementById("sexo_paciente").value;

        }

        // si no ingresó el estado civil
        if (document.getElementById("estado_civil_paciente").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el estado civil del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("estado_civil_paciente").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.EstadoCivil = document.getElementById("estado_civil_paciente").value;

        }

        // el número de hijos lo permite nulo
        if (document.getElementById("hijos_paciente").value != ""){

            // asigna en la variable de clase
            this.Hijos = document.getElementById("hijos_paciente").value;

        }

        // si no ingresó la dirección
        if (document.getElementById("direccion_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la dirección del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("direccion_paciente").focus();
            return false;

        // si ingresó
        } else {

            // lo asigna en la variable de clase
            this.Direccion = document.getElementById("direccion_paciente").value;

        }

        // si ingresó el teléfono del paciente
        if (document.getElementById("telefono_paciente").value != ""){

            // lo asigna en la variable de clase
            this.Telefono = document.getElementById("telefono_paciente").value;

        }

        // si ingresó el celular
        if (document.getElementById("celular_paciente").value != ""){

            // verifica que halla indicado la compañía
            if (document.getElementById("compania_paciente").value == 0){

                // presenta el mensaje y retorna
                mensaje = "Seleccione la compañía de celulares";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("direccion_paciente").focus();
                return false;

            }

            // asigna el teléfono y la compañía
            this.Celular = document.getElementById("celular_paciente").value;
            this.Compania = document.getElementById("compania_paciente").value;

        }

        // verificamos que exista una forma de contacto
        if (this.Mail == "" && this.Telefono == "" && this.Celular == ""){

            // presenta el mensaje y retorna
            mensaje = "No hay información para contactar al paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no indicó la localidad de nacimiento
        if (document.getElementById("id_loc_nac_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la localidad de nacimiento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("loc_nac_paciente").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.LocalidadNacimiento = document.getElementById("id_loc_nac_paciente").value;

        }

        // si no seleccionó la localidad de residencia
        if (document.getElementById("id_loc_res_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la localidad de residencia";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("loc_res_paciente").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.LocalidadResidencia = document.getElementById("id_loc_res_paciente").value;


        }

        // si no indicó la localidad de nacimiento de la madre
        if (document.getElementById("id_loc_nac_madre").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la localidad de nacimiento de la madre";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("loc_nac_madre").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.LocalidadMadre = document.getElementById("id_loc_nac_madre").value;

        }

        // si no indicó si la madre era positiva
        if (document.getElementById("madre_positiva").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si la madre es positiva";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("madre_positiva").focus();
            return false;

        // si marcó
        } else {

            // asigna en la variable de clase
            this.MadrePositiva = document.getElementById("madre_positiva").value;

        }

        // la ocupación la permite en blanco pero lanza un alerta
        if (document.getElementById("ocupacion_paciente").value == ""){

            // presenta el mensaje
            mensaje = "No hay información de la ocupación<br>";
            mensaje += "Se grabará el registro igualmente";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // asigna en la variable de clase
        this.Ocupacion = document.getElementById("ocupacion_paciente").value;

        // la obra social la permite en blanco pero lanza un alerta
        if (document.getElementById("obra_soc_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "No hay información de la Obra Social<br>";
            mensaje += "Se grabará el registro igualmente";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // asigna en la variable de clase
        this.ObraSocial = document.getElementById("obra_soc_paciente").value;

        // si no seleccionó el motivo de consulta
        if (document.getElementById("motivo_paciente").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el motivo de consulta";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("motivo_paciente").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Motivo = document.getElementById("motivo_paciente").value;

        }

        // si no indicó la derivación lanza un alerta
        if (document.getElementById("derivo_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "No hay información del profesional que<br>";
            mensaje += "solicitó el estudio.";
            mensaje += "Se grabará el registro igualmente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asigna en la variable de clase
        this.Derivacion = document.getElementById("derivo_paciente").value;

        // si no indicó si hubo tratamiento
        if (document.getElementById("tratamiento_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si ha recibido tratamiento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tratamiento_paciente").focus();
            return false;

        // si selecciono
        } else {

            // asigna en la variable de clase
            this.Tratamiento = document.getElementById("tratamiento_paciente").value;

        }

        // asigna las observaciones
        this.Comentarios = CKEDITOR.instances['observaciones_paciente'].getData();

        // grabamos el registro
        this.grabaPaciente();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario, envía los datos
     * al servidor por ajax
     */
    grabaPaciente(){

        // declaración de variables
        var datosPaciente = new FormData();

        // si está editando
        if (this.Protocolo != 0){
            datosPaciente.append("Protocolo", this.Protocolo);
        }

        // si tiene historia clínica
        if (this.HistoriaClinica != ""){
            datosPaciente.append("HistoriaClinica", this.HistoriaClinica);
        }

        // agregamos el resto de las propiedades
        datosPaciente.append("Apellido", this.Apellido);
        datosPaciente.append("Nombre", this.Nombre);
        datosPaciente.append("Documento", this.Documento);
        datosPaciente.append("TipoDocumento", this.TipoDocumento);
        datosPaciente.append("FechaNacimiento", this.FechaNacimiento);
        datosPaciente.append("Edad", this.Edad);
        datosPaciente.append("Sexo", this.Sexo);
        datosPaciente.append("EstadoCivil", this.EstadoCivil);
        datosPaciente.append("Hijos", this.Hijos);
        datosPaciente.append("Direccion", this.Direccion);
        datosPaciente.append("Telefono", this.Telefono);
        datosPaciente.append("Celular", this.Celular);
        datosPaciente.append("Compania", this.Compania);
        datosPaciente.append("Mail", this.Mail);
        datosPaciente.append("LocNacimiento", this.LocalidadNacimiento);
        datosPaciente.append("CoordNacimiento", this.CoordenadasNacimiento);
        datosPaciente.append("LocResidencia", this.LocalidadResidencia);
        datosPaciente.append("CoordResidencia", this.CoordenadasResidencia);
        datosPaciente.append("LocMadre", this.LocalidadMadre);
        datosPaciente.append("MadrePositiva", this.MadrePositiva);
        datosPaciente.append("Ocupacion", this.Ocupacion);
        datosPaciente.append("ObraSocial", this.ObraSocial);
        datosPaciente.append("Motivo", this.Motivo);
        datosPaciente.append("Derivacion", this.Derivacion);
        datosPaciente.append("Tratamiento", this.Tratamiento);
        datosPaciente.append("Comentarios", this.Comentarios);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "pacientes/graba_paciente.php",
            type: "POST",
            data: datosPaciente,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Protocolo != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // almacena el protocolo y la historia
                    document.getElementById("protocolo_paciente").value = data.Protocolo;
                    document.getElementById("historia_paciente").value = data.Historia;

                    // si existe algún layer de tablas auxiliares lo destruye
                    // recordar que en el constructor están todos
                    // inicializados a una cadena vacía
                    if (enfermedades.layerEnfermedades != ""){
                        enfermedades.layerEnfermedades.destroy();
                    }
                    if(transfusiones.layerTransfusiones != ""){
                        transfusiones.layerTransfusiones.destroy();
                    }
                    if (transplantes.layerTransplantes != ""){
                        transplantes.layerTransplantes.destroy();
                    }
                    if (Entrevistas.layerEntrevistas != ""){
                        entrevistas.layerEntrevistas.destroy();
                    }

                    // si la edad es igual o menor a un año
                    if (pacientes.Edad <= 1){

                        // muestra el formulario de chagas congénito
                        congenito.verCongenito();

                        // muestra los datos de chagas congénito
                        congenito.getDatosCongenito(data.Protocolo);

                    }

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al grabar el registro o al pulsar el botón
     * protocolo, si recibe la clave del protocolo, asume que
     * es un alta y genera la nueva toma de muestras, si no
     * recibe nada pide confirmación si debe agregar una toma
     * de muestras del registro activo
     */
    protocoloPaciente(){

        // obtenemos el protocolo del formulario
        var protocolo = document.getElementById("protocolo_paciente").value;

        // si no grabó el registro
        if (protocolo == ""){

            // retorna avisando
            new jBox('Notice', {content: "Debe grabar el registro primero", color: 'red'});
            return false;

        }

        // pide confirmación
        var Confirmacion = new jBox('Confirm', {
                    animation: 'flip',
                    title: 'Toma de Muestras',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    overlay: false,
                    content: 'Desea generar una nueva toma de muestra?',
                    theme: 'TooltipBorder',
                    confirm: function() {

                        // generamos la nueva muestra
                        muestras.nuevaMuestra(protocolo);

                    },
                    cancel: function(){
                        Confirmacion.destroy();
                    },
                    confirmButton: 'Aceptar',
                    cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

        // ahora imprimimos el protocolo
        var layerProtocolo = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Protocolo de Paciente',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'pacientes/protocolo.php?protocolo='+protocolo,
                        reload: 'strict'
                    }
            });

        // mostramos el pdf
        layerProtocolo.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que reinicia el formulario de pacientes y las variables
     * de clase
     */
    cancelaPaciente(){

        // reiniciamos el formulario
        document.getElementById("datos_pacientes").reset();

        // reiniciamos las variables de clase
        this.initPacientes();

        // fijamos el foco
        document.getElementById("documento_paciente").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} campo - objeto html a buscar
     * @param {string} localidad - cadena a buscar
     * Método llamado en la selección y búsqueda de localidades (tanto de residencia, como
     * de nacimiento y de la madre), recibe como parámetro el campo del formulario en el
     * que está buscando, abre el cuadro de diálogo pasándole estos datos como argumento
     */
    buscaLocalidad(campo, localidad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no recibió la localidad
        if (typeof(localidad) == "undefined" || localidad == ""){

            // presenta la alerta y retorna
            mensaje = "Debe ingresar parte del nombre de la Localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // abrimos el cuadro de diálogo
        // cargamos el formulario en una ventana emergente
        this.layerPacientes = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        repositionOnContent: true,
                        overlay: false,
                        title: 'Búsqueda de Localidades',
                        draggable: 'title',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                        this.destroy();
                        },
                        ajax: {
                            url: 'pacientes/sel_localidad.php?Localidad='+localidad+'&Campo='+campo,
                            reload: 'strict'
                        }
        });
        this.layerPacientes.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} campo - nombre del campo
     * @param {string} pais - nombre del país
     * @param {string} provincia - nombre de la jurisdicción
     * @param {string} localidad - nombre de la localidad
     * @param {string} codloc - código de la localidad
     * Método llamado desde la grilla de búsqueda de localidades, recibe como parámetros
     * los datos del registro y el campo en el que estamos buscando, cierra el layer
     * y asigna los valores en el formulario
     */
    selLocalidad(campo, pais, provincia, localidad, codloc){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si el campo es nacimiento
        if (campo == "nacimiento"){

            // fija los valores en el formulario
            document.getElementById("pais_nac_paciente").value = pais;
            document.getElementById("jur_nac_paciente").value = provincia;
            document.getElementById("loc_nac_paciente").value = localidad;
            document.getElementById("id_loc_nac_paciente").value = codloc;

            // setea el foco en el siguiente campo
            document.getElementById("loc_res_paciente").focus();

        // si el campo es residencia
        } else if (campo == "residencia"){

            // fija los valores en el formulario
            document.getElementById("pais_res_paciente").value = pais;
            document.getElementById("jur_res_paciente").value = provincia;
            document.getElementById("loc_res_paciente").value = localidad;
            document.getElementById("id_loc_res_paciente").value = codloc;

            // setea el foco en el siguiente campo
            document.getElementById("loc_nac_madre").focus();

        // si el campo es la madre
        } else {

            // fija los valores en el formulario
            document.getElementById("pais_nac_madre").value = pais;
            document.getElementById("jur_nac_madre").value = provincia;
            document.getElementById("loc_nac_madre").value = localidad;
            document.getElementById("id_loc_nac_madre").value = codloc;

            // setea el foco en el siguiente campo
            document.getElementById("madre_positiva").focus();

        }

        // destruímos el layer
        this.layerPacientes.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} protocolo - clave del registro
     * Método que recibe como parámetro la clave de un paciente y
     * obtiene los datos del mismo asigándolos a las variables de
     * clase
     */
    getDatosPaciente(protocolo){

        // obtenemos el registro
        $.ajax({
            url: "pacientes/get_paciente.php?Protocolo=" + protocolo,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // asignamos en las variables de clase
                pacientes.setProtocolo(data.Protocolo);
                pacientes.setHistoriaClinica(data.Historia);
                pacientes.setIdLaboratorio(data.IdLaboratorio);
                pacientes.setLaboratorio(data.Laboratorio);
                pacientes.setApellido(data.Apellido);
                pacientes.setNombre(data.Nombre);
                pacientes.setDocumento(data.Documento);
                pacientes.setTipoDocumento(data.IdDocumento);
                pacientes.setFechaNacimiento(data.FechaNacimiento);
                pacientes.setEdad(data.Edad);
                pacientes.setSexo(data.IdSexo);
                pacientes.setEstadoCivil(data.IdEstadoCivil);
                pacientes.setHijos(data.Hijos);
                pacientes.setDireccion(data.Direccion);
                pacientes.setTelefono(data.Telefono);
                pacientes.setCelular(data.Celular);
                pacientes.setCompania(data.IdCompania);
                pacientes.setMail(data.Mail);
                pacientes.setProvinciaNacimiento(data.JurisdiccionNacimiento);
                pacientes.setIdLocalidadNacimiento(data.IdLocNacimiento);
                pacientes.setLocalidadNacimiento(data.LocalidadNacimiento);
                pacientes.setPaisNacimiento(data.PaisNacimiento);
                pacientes.setCoordenadasNacimiento(data.CoordenadasNacimiento);
                pacientes.setProvinciaResidencia(data.JurisdiccionResidencia);
                pacientes.setIdLocalidadResidencia(data.IdLocResidencia);
                pacientes.setLocalidadResidencia(data.LocalidadResidencia);
                pacientes.setPaisResidencia(data.PaisResidencia);
                pacientes.setCoordenadasResidencia(data.CoordenadasResidencia);
                pacientes.setProvinciaMadre(data.JurisdiccionMadre);
                pacientes.setIdLocalidadMadre(data.IdLocalidadMadre);
                pacientes.setLocalidadMadre(data.LocalidadMadre);
                pacientes.setPaisMadre(data.PaisMadre);
                pacientes.setMadrePositiva(data.MadrePositiva);
                pacientes.setOcupacion(data.Ocupacion);
                pacientes.setObraSocial(data.ObraSocial);
                pacientes.setMotivo(data.IdMotivo);
                pacientes.setDerivacion(data.Derivacion);
                pacientes.setTratamiento(data.Tratamiento);
                pacientes.setUsuario(data.Usuario);
                pacientes.setFechaAlta(data.FechaAlta);
                pacientes.setComentarios(data.Comentarios);

                // cargamos los datos de antecedentes
                antecedentes.getDatosAntecedentes(protocolo);
                
                // mostramos el registro
                pacientes.muestraPaciente();

            }

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase muestra los
     * datos del paciente
     */
    muestraPaciente(){

        // si hay abierto un layer de búsqueda
        if (typeof(this.layerPacientes != "undefined")){

            // lo destruimos
            this.layerPacientes.destroy();

        }

        // fijamos los valores en el formulario
        document.getElementById("laboratorio_paciente").value = this.IdLaboratorio;
        document.getElementById("coord_nac_paciente").value = this.CoordenadasNacimiento;
        document.getElementById("coord_resid_paciente").value = this.CoordenadasResidencia;
        document.getElementById("protocolo_paciente").value = this.Protocolo;
        document.getElementById("historia_paciente").value = this.HistoriaClinica;
        document.getElementById("tipo_doc_paciente").value = this.TipoDocumento;
        document.getElementById("documento_paciente").value = this.Documento;
        document.getElementById("mail_paciente").value = this.Mail;
        document.getElementById("apellido_paciente").value = this.Apellido;
        document.getElementById("nombre_paciente").value = this.Nombre;
        document.getElementById("fecha_nac_paciente").value = this.FechaNacimiento;
        document.getElementById("edad_paciente").value = this.Edad;
        document.getElementById("sexo_paciente").value = this.Sexo;
        document.getElementById("estado_civil_paciente").value = this.EstadoCivil;
        document.getElementById("hijos_paciente").value = this.Hijos;
        document.getElementById("direccion_paciente").value = this.Direccion;
        document.getElementById("telefono_paciente").value = this.Telefono;
        document.getElementById("compania_paciente").value = this.Compania;
        document.getElementById("celular_paciente").value = this.Celular;
        document.getElementById("pais_nac_paciente").value = this.PaisNacimiento;
        document.getElementById("jur_nac_paciente").value = this.ProvinciaNacimiento;
        document.getElementById("loc_nac_paciente").value = this.LocalidadNacimiento;
        document.getElementById("id_loc_nac_paciente").value = this.IdLocalidadNacimiento;
        document.getElementById("pais_res_paciente").value = this.PaisResidencia;
        document.getElementById("jur_res_paciente").value = this.ProvinciaResidencia;
        document.getElementById("loc_res_paciente").value = this.LocalidadResidencia;
        document.getElementById("id_loc_res_paciente").value = this.IdLocalidadResidencia;
        document.getElementById("pais_nac_madre").value = this.PaisMadre;
        document.getElementById("jur_nac_madre").value = this.ProvinciaMadre;
        document.getElementById("loc_nac_madre").value = this.LocalidadMadre;
        document.getElementById("id_loc_nac_madre").value = this.IdLocalidadMadre;
        document.getElementById("madre_positiva").value = this.MadrePositiva;
        document.getElementById("ocupacion_paciente").value = this.Ocupacion;
        document.getElementById("obra_soc_paciente").value = this.ObraSocial;
        document.getElementById("motivo_paciente").value = this.Motivo;
        document.getElementById("derivo_paciente").value = this.Derivacion;
        document.getElementById("tratamiento_paciente").value = this.Tratamiento;
        document.getElementById("observaciones_paciente").value = this.Comentarios;
        document.getElementById("usuario_paciente").value = this.Usuario;
        document.getElementById("alta_paciente").value = this.FechaAlta;

        // asigna las observaciones en el editor
        CKEDITOR.instances['observaciones_paciente'].setData(this.Comentarios);

        // verificamos la seguridad
        this.seguridadPacientes();

        // fijamos el foco
        document.getElementById("apellido_paciente").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que fija el acceso de seguridad el usuario
     */
    seguridadPacientes(){

        // si puede editar pacientes
        if (sessionStorage.getItem("Personas") == "Si"){

            // mostramos los botones
            $('#BtnGrabaPaciente').show();
            $('#BtnProtocoloPaciente').show();
            $('#BtnCancelaPaciente').show();

        // si no está habilitado
        } else {

            // ocultamos los botones
            $('#BtnGrabaPaciente').hide();
            $('#BtnProtocoloPaciente').hide();
            $('#BtnCancelaPaciente').hide();

        }

    }

}
