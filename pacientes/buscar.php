<?php

/**
 *
 * pacientes/buscar.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una cadena a buscar y presenta la
 * grilla de resultados con los registros encontrados
 *
*/

// incluimos e instanciamos las clases
require_once("pacientes.class.php");
$paciente = new Pacientes();

// buscamos el texto
$nomina = $paciente->buscaPaciente($_GET["texto"]);

// si no hay resultados
if (count($nomina) == 0){

    // presenta el mensaje
    echo "<h2>No se han encontrado registros coincidentes</h2>";

// si encontró
} else {

    // define la tabla
    echo "<table id='grilla_pacientes' width='700px' align='center' border='0'>";

    // define los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th align='left'>Protocolo</th>";
    echo "<th align='left'>Historia</th>";
    echo "<th align='left'>Laboratorio</th>";
    echo "<th align='left'>Nombre</th>";
    echo "<th align='left'>Documento</th>";
    echo "<th align='left'>Jurisdicción</th>";
    echo "<th align='left'>Localidad</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo de la tabla
    echo "<body>";

    // recorremos el vector
    foreach($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // presentamos el registro
        echo "<td>$protocolo</td>";
        echo "<td>$historia_clinica</td>";
        echo "<td>$laboratorio</td>";
        echo "<td>$nombre</td>";
        echo "<td>" . number_format($documento, 0, ",", ".") . "</td>";
        echo "<td>$jurisdiccion_residencia</td>";
        echo "<td>$localidad_residencia</td>";

        // arma el enlace para visualizar
        echo "<td>";
        echo "<input type='button'
               name='btnVerPaciente'
               id='btnVerPaciente'
               class='botoneditar'
               title='Pulse para ver el registro'
               onClick='pacientes.getDatosPaciente($protocolo)'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cierra la tabla
    echo "</body>";
    echo "</table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

    ?>
    <script>

        // definimos las propiedades de la tabla
        $('#grilla_pacientes').datatable({
            pageSize: 15,
            sort:    [true, true, true, true, true, true,     true, false],
            filters: [true, true, true, true, true, 'select', true, false],
            filterText: 'Buscar ... '
        });

    </script>
    <?php

}
?>