<?php

/**
 *
 * Class Pacientes | pacientes/pacientes.class.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de pacientes
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Pacientes {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // variables de la tabla de pacientes
    protected $Protocolo;              // número de protocolo
    protected $IdLaboratorio;          // clave del laboratorio al que pertenece
    protected $LaboratorioUsuario;     // clave del laboratorio del usuario activo
    protected $Laboratorio;            // nombre del laboratorio
    protected $Historia;               // número de historia clínica
    protected $Apellido;               // apellido del paciente
    protected $Nombre;                 // nombre del paciente
    protected $Documento;              // número de documento
    protected $IdDocumento;            // clave del tipo de documento
    protected $TipoDocumento;          // descripción del documento
    protected $FechaNacimiento;        // fecha de nacimiento del paciente
    protected $Edad;                   // edad del paciente (calculada)
    protected $Sexo;                   // sexo del paciente
    protected $IdSexo;                 // clave del sexo
    protected $EstadoCivil;            // estado civil del paciente
    protected $IdEstadoCivil;          // clave del estado civil
    protected $Hijos;                  // número de hijos
    protected $Direccion;              // dirección del paciente
    protected $Telefono;               // número de teléfono
    protected $Celular;                // número de celular
    protected $CompaniaCelular;        // nombre de la compañia de celular
    protected $IdCompania;             // clave de la compañia de celular
    protected $Mail;                   // dirección de correo
    protected $JurisdiccionNacimiento; // nombre de la jurisdicción
    protected $IdJurNacimiento;        // clave indec de la jurisdicción
    protected $LocNacimiento;          // localidad de nacimiento
    protected $IdLocNacimiento;        // clave indec de la localidad de nacimiento
    protected $PaisNacimiento;         // nombre del país
    protected $IdPaisNacimiento;       // id del país de nacimiento
    protected $CoordenadasNac;         // coordenadas gps
    protected $JurisdiccionResid;      // nombre de la jurisdicción
    protected $IdJurisdiccionResid;    // clave de la jurisdicción
    protected $LocResidencia;          // nombre de la localidad
    protected $IdLocResidencia;        // clave de la localidad
    protected $PaisResidencia;         // nombre del país
    protected $IdPaisResidencia;       // clave del país
    protected $CoordenadasResid;       // coordenadas gps
    protected $JurisdiccionMadre;      // jurisdicción de la madre
    protected $IdJurisdiccionMadre;    // clave de la jurisdicción
    protected $LocalidadMadre;         // localidad de la madre
    protected $IdLocalidadMadre;       // clave de la localidad
    protected $PaisMadre;              // nombre del país
    protected $IdPaisMadre;            // clave del país
    protected $MadrePositiva;          // si la madre fue detectada
    protected $Ocupacion;              // descripción
    protected $ObraSocial;             // nombre de la obra social
    protected $Motivo;                 // motivo de consulta
    protected $IdMotivo;               // clave del motivo
    protected $Derivacion;             // tipo de derivacion
    protected $IdDerivacion;           // clave de la derivacion
    protected $Tratamiento;            // si recibió tratamiento anteriormente
    protected $Usuario;                // nombre del usuario
    protected $IdUsuario;              // clave del usuario
    protected $FechaAlta;              // fecha de alta del registro
    protected $Comentarios;            // observaciones del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->Protocolo = 0;
        $this->IdLaboratorio = 0;
        $this->Laboratorio = "";
        $this->Historia = "";
        $this->Apellido = "";
        $this->Nombre = "";
        $this->Documento = "";
        $this->IdDocumento = 0;
        $this->TipoDocumento = "";
        $this->FechaNacimiento = "";
        $this->Edad = "";
        $this->Sexo = "";
        $this->IdSexo = "";
        $this->EstadoCivil = "";
        $this->IdEstadoCivil = 0;
        $this->Hijos = 0;
        $this->Direccion = "";
        $this->Telefono = "";
        $this->Celular = "";
        $this->CompaniaCelular = "";
        $this->IdCompania = 0;
        $this->Mail = "";
        $this->JurisdiccionNacimiento = "";
        $this->IdJurNacimiento = "";
        $this->LocNacimiento = "";
        $this->IdLocNacimiento = "";
        $this->PaisNacimiento = "";
        $this->IdPaisNacimiento = 0;
        $this->CoordenadasNac = "";
        $this->JurisdiccionResid = "";
        $this->IdJurisdiccionResid = "";
        $this->LocResidencia = "";
        $this->IdLocResidencia = "";
        $this->PaisResidencia = "";
        $this->IdPaisResidencia = 0;
        $this->CoordenadasResid = "";
        $this->JurisdiccionMadre = "";
        $this->IdJurisdiccionMadre = "";
        $this->LocalidadMadre = "";
        $this->IdLocalidadMadre = "";
        $this->PaisMadre = "";
        $this->IdPaisMadre = 0;
        $this->MadrePositiva = "";
        $this->Ocupacion = "";
        $this->ObraSocial = "";
        $this->Motivo = "";
        $this->IdMotivo = 0;
        $this->Derivacion = "";
        $this->IdDerivacion = 0;
        $this->Tratamiento = "";
        $this->Usuario = "";
        $this->FechaAlta = "";
        $this->Comentarios = "";

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y del laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->LaboratorioUsuario = $_SESSION["IdLaboratorio"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setIdLaboratorio($idlaboratorio){
        $this->IdLaboratorio = $idlaboratorio;
    }
    public function setHistoria($historia){
        $this->Historia = $historia;
    }
    public function setApellido($apellido){
        $this->Apellido = $apellido;
    }
    public function setNombre($nombre){
        $this->Nombre = $nombre;
    }
    public function setDocumento($documento){
        $this->Documento = $documento;
    }
    public function setIdDocumento($iddocumento){
        $this->IdDocumento = $iddocumento;
    }
    public function setFechaNacimiento($fecha_nacimiento){
        $this->FechaNacimiento = $fecha_nacimiento;
    }
    public function setEdad($edad){
        $this->Edad = $edad;
    }
    public function setIdSexo($idsexo){
        $this->IdSexo = $idsexo;
    }
    public function setIdEstadoCivil($id_estado){
        $this->IdEstadoCivil = $id_estado;
    }
    public function setHijos($hijos){
        $this->Hijos = $hijos;
    }
    public function setDireccion($direccion){
        $this->Direccion = $direccion;
    }
    public function setTelefono($telefono){
        $this->Telefono = $telefono;
    }
    public function setCelular($celular){
        $this->Celular = $celular;
    }
    public function setMail($mail){
        $this->Mail = $mail;
    }
    public function setIdCompania($idcompania){
        $this->IdCompania = $idcompania;
    }
    public function setIdLocNacimiento($idlocalidad){
        $this->IdLocNacimiento = $idlocalidad;
    }
    public function setCoordenadasNac($coordenadas){
        $this->CoordenadasNac = $coordenadas;
    }
    public function setIdLocResidencia($idlocalidad){
        $this->IdLocResidencia = $idlocalidad;
    }
    public function setCoordenadasResid($coordenadas){
        $this->CoordenadasResid = $coordenadas;
    }
    public function setIdLocalidadMadre($idlocalidad){
        $this->IdLocalidadMadre = $idlocalidad;
    }
    public function setMadrePositiva($positiva){
        $this->MadrePositiva = $positiva;
    }
    public function setOcupacion($ocupacion){
        $this->Ocupacion = $ocupacion;
    }
    public function setObraSocial($obrasocial){
        $this->ObraSocial = $obrasocial;
    }
    public function setIdMotivo($idmotivo){
        $this->IdMotivo = $idmotivo;
    }
    public function setIdDerivacion($derivacion){
        $this->IdDerivacion = $derivacion;
    }
    public function setTratamiento($tratamiento){
        $this->Tratamiento = $tratamiento;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }

    // métodos de recuperación de valores
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getLaboratorio(){
        return $this->Laboratorio;
    }
    public function getHistoria(){
        return $this->Historia;
    }
    public function getApellido(){
        return $this->Apellido;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getDocumento(){
        return $this->Documento;
    }
    public function getIdDocumento(){
        return $this->IdDocumento;
    }
    public function getTipoDocumento(){
        return $this->TipoDocumento;
    }
    public function getFechaNacimiento(){
        return $this->FechaNacimiento;
    }
    public function getEdad(){
        return $this->Edad;
    }
    public function getIdSexo(){
        return $this->IdSexo;
    }
    public function getSexo(){
        return $this->Sexo;
    }
    public function getIdEstadoCivil(){
        return $this->IdEstadoCivil;
    }
    public function getEstadoCivil(){
        return $this->EstadoCivil;
    }
    public function getHijos(){
        return $this->Hijos;
    }
    public function getDireccion(){
        return $this->Direccion;
    }
    public function getTelefono(){
        return $this->Telefono;
    }
    public function getCelular(){
        return $this->Celular;
    }
    public function getIdCompania(){
        return $this->IdCompania;
    }
    public function getMail(){
        return $this->Mail;
    }
    public function getJurisdiccionNacimiento(){
        return $this->JurisdiccionNacimiento;
    }
    public function getIdJurNacimiento(){
        return $this->IdJurNacimiento;
    }
    public function getIdLocNacimiento(){
        return $this->IdLocNacimiento;
    }
    public function getLocNacimiento(){
        return $this->LocNacimiento;
    }
    public function getIdPaisNacimiento(){
        return $this->IdPaisNacimiento;
    }
    public function getPaisNacimiento(){
        return $this->PaisNacimiento;
    }
    public function getCoordenadasNac(){
        return $this->CoordenadasNac;
    }
    public function getIdJurisdiccionResid(){
        return $this->IdJurisdiccionResid;
    }
    public function getJurisdiccionResidencia(){
        return $this->JurisdiccionResid;
    }
    public function getIdLocResidencia(){
        return $this->IdLocResidencia;
    }
    public function getLocResidencia(){
        return $this->LocResidencia;
    }
    public function getPaisResidencia(){
        return $this->PaisResidencia;
    }
    public function getIdPaisResidencia(){
        return $this->IdPaisResidencia;
    }
    public function getCoordenadasResid(){
        return $this->CoordenadasResid;
    }
    public function getJurisdiccionMadre(){
        return $this->JurisdiccionMadre;
    }
    public function getIdJurisdiccionMadre(){
        return $this->IdJurisdiccionMadre;
    }
    public function getIdLocalidadMadre(){
        return $this->IdLocalidadMadre;
    }
    public function getLocalidadMadre(){
        return $this->LocalidadMadre;
    }
    public function getPaisMadre(){
        return $this->PaisMadre;
    }
    public function getIdPaisMadre(){
        return $this->IdPaisMadre;
    }
    public function getMadrePositiva(){
        return $this->MadrePositiva;
    }
    public function getOcupacion(){
        return $this->Ocupacion;
    }
    public function getObraSocial(){
        return $this->ObraSocial;
    }
    public function getIdMotivo(){
        return $this->IdMotivo;
    }
    public function getMotivo(){
        return $this->Motivo;
    }
    public function getIdDerivacion(){
        return $this->IdDerivacion;
    }
    public function getDerivacion(){
        return $this->Derivacion;
    }
    public function getTratamiento(){
        return $this->Tratamiento;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }

    /**
     * Método que recibe como parámetro un texto, busca en la base de datos la
     * ocurrencia del mismo en el nombre y apellido, el número de documento,
     * la localidad y la provincia, retorna un array asociativo con los
     * registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto - cadena a buscar en la base
     * @return array
     */
    public function buscaPaciente($texto){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_personas.protocolo AS protocolo,
                            diagnostico.v_personas.laboratorio AS laboratorio,
                            diagnostico.v_personas.historia_clinica AS historia_clinica,
                            CONCAT(diagnostico.v_personas.apellido, ', ', diagnostico.v_personas.nombre) AS nombre,
                            diagnostico.v_personas.documento AS documento,
                            diagnostico.v_personas.localidad_residencia AS localidad_residencia,
                            diagnostico.v_personas.jurisdiccion_residencia AS jurisdiccion_residencia
                      FROM diagnostico.v_personas
                      WHERE diagnostico.v_personas.nombre LIKE '%$texto%' OR
                            diagnostico.v_personas.apellido LIKE '%$texto%' OR
                            diagnostico.v_personas.protocolo LIKE '%$texto%' OR
                            diagnostico.v_personas.historia_clinica LIKE '%$texto%' OR
                            diagnostico.v_personas.documento LIKE '%$texto%' OR
                            diagnostico.v_personas.localidad_residencia LIKE '%$texto%' OR
                            diagnostico.v_personas.jurisdiccion_residencia LIKE '%$texto%'
                      ORDER BY diagnostico.v_personas.protocolo;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el array
        return $fila;

    }

    /**
     * Método que recibe como parámetro la clave de un registro y asigna en
     * las variables de clase los valores de la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $clave - clave del protocolo del paciente
     */
    public function getDatosPaciente($clave){

        // inicializamos las variables
        $protocolo = "";
        $id_laboratorio = 0;
        $laboratorio = "";
        $historia_clinica = "";
        $apellido = "";
        $nombre = "";
        $documento = "";
        $tipo_documento = "";
        $id_documento = 0;
        $fecha_nacimiento = "";
        $edad = 0;
        $id_sexo = 0;
        $sexo = "";
        $id_estado_civil = 0;
        $estado_civil = "";
        $hijos = 0;
        $direccion = "";
        $telefono = "";
        $celular = "";
        $id_compania = 0;
        $mail = "";
        $id_localidad_nacimiento = "";
        $localidad_nacimiento = "";
        $jurisdiccion_nacimiento = "";
        $pais_nacimiento = "";
        $coordenadas_nacimiento = "";
        $id_localidad_residencia = "";
        $localidad_residencia = "";
        $jurisdiccion_residencia = "";
        $pais_residencia = "";
        $coordenadas_residencia = "";
        $localidad_madre = "";
        $id_localidad_madre = "";
        $jurisdiccion_madre = "";
        $pais_madre = "";
        $madre_positiva = "";
        $ocupacion = "";
        $obra_social = "";
        $id_motivo = 0;
        $motivo = "";
        $idderivacion = 0;
        $derivacion = 0;
        $tratamiento = "";
        $usuario = "";
        $fecha_alta = "";
        $comentarios = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_personas.protocolo AS protocolo,
                            diagnostico.v_personas.id_laboratorio AS id_laboratorio,
                            diagnostico.v_personas.laboratorio AS laboratorio,
                            diagnostico.v_personas.historia_clinica AS historia_clinica,
                            diagnostico.v_personas.apellido AS apellido,
                            diagnostico.v_personas.nombre AS nombre,
                            diagnostico.v_personas.documento AS documento,
                            diagnostico.v_personas.id_documento AS id_documento,
                            diagnostico.v_personas.tipo_documento AS tipo_documento,
                            diagnostico.v_personas.fecha_nacimiento AS fecha_nacimiento,
                            diagnostico.v_personas.edad AS edad,
                            diagnostico.v_personas.id_sexo AS id_sexo,
                            diagnostico.v_personas.sexo AS sexo,
                            diagnostico.v_personas.id_estado_civil AS id_estado_civil,
                            diagnostico.v_personas.estado_civil AS estado_civil,
                            diagnostico.v_personas.hijos AS hijos,
                            diagnostico.v_personas.direccion AS direccion,
                            diagnostico.v_personas.telefono AS telefono,
                            diagnostico.v_personas.celular AS celular,
                            diagnostico.v_personas.id_compania AS id_compania,
                            diagnostico.v_personas.mail AS mail,
                            diagnostico.v_personas.id_localidad_nacimiento AS id_localidad_nacimiento,
                            diagnostico.v_personas.localidad_nacimiento AS localidad_nacimiento,
                            diagnostico.v_personas.jurisdiccion_nacimiento AS jurisdiccion_nacimiento,
                            diagnostico.v_personas.pais_nacimiento AS pais_nacimiento,
                            diagnostico.v_personas.coordenadas_nacimiento AS coordenadas_nacimiento,
                            diagnostico.v_personas.id_localidad_residencia AS id_localidad_residencia,
                            diagnostico.v_personas.localidad_residencia AS localidad_residencia,
                            diagnostico.v_personas.jurisdiccion_residencia AS jurisdiccion_residencia,
                            diagnostico.v_personas.pais_residencia AS pais_residencia,
                            diagnostico.v_personas.coordenadas_residencia AS coordenadas_residencia,
                            diagnostico.v_personas.id_localidad_madre AS id_localidad_madre,
                            diagnostico.v_personas.localidad_madre AS localidad_madre,
                            diagnostico.v_personas.jurisdiccion_madre AS jurisdiccion_madre,
                            diagnostico.v_personas.pais_madre AS pais_madre,
                            diagnostico.v_personas.madre_positiva AS madre_positiva,
                            diagnostico.v_personas.ocupacion AS ocupacion,
                            diagnostico.v_personas.obra_social AS obra_social,
                            diagnostico.v_personas.id_motivo AS id_motivo,
                            diagnostico.v_personas.motivo AS motivo,
                            diagnostico.v_personas.id_derivacion AS idderivacion,
                            diagnostico.v_personas.derivacion AS derivacion,
                            diagnostico.v_personas.tratamiento AS tratamiento,
                            diagnostico.v_personas.usuario AS usuario,
                            diagnostico.v_personas.fecha_alta AS fecha_alta,
                            diagnostico.v_personas.comentarios AS comentarios
                     FROM diagnostico.v_personas
                     WHERE diagnostico.v_personas.protocolo = '$clave';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // asignamos en las variables de clase
        $this->Protocolo = $protocolo;
        $this->IdLaboratorio = $id_laboratorio;
        $this->Laboratorio = $laboratorio;
        $this->Historia = $historia_clinica;
        $this->Apellido = $apellido;
        $this->Nombre = $nombre;
        $this->Documento = $documento;
        $this->TipoDocumento = $tipo_documento;
        $this->IdDocumento = $id_documento;
        $this->FechaNacimiento = $fecha_nacimiento;
        $this->Edad = $edad;
        $this->IdSexo = $id_sexo;
        $this->Sexo = $sexo;
        $this->IdEstadoCivil = $id_estado_civil;
        $this->EstadoCivil = $estado_civil;
        $this->Hijos = $hijos;
        $this->Direccion = $direccion;
        $this->Telefono = $telefono;
        $this->Celular = $celular;
        $this->IdCompania = $id_compania;
        $this->Mail = $mail;
        $this->JurisdiccionNacimiento = $jurisdiccion_nacimiento;
        $this->LocNacimiento = $localidad_nacimiento;
        $this->IdLocNacimiento = $id_localidad_nacimiento;
        $this->PaisNacimiento = $pais_nacimiento;
        $this->CoordenadasNac = $coordenadas_nacimiento;
        $this->JurisdiccionResid = $jurisdiccion_residencia;
        $this->LocResidencia = $localidad_residencia;
        $this->IdLocResidencia = $id_localidad_residencia;
        $this->PaisResidencia = $pais_residencia;
        $this->CoordenadasResid = $coordenadas_residencia;
        $this->JurisdiccionMadre = $jurisdiccion_madre;
        $this->LocalidadMadre = $localidad_madre;
        $this->IdLocalidadMadre = $id_localidad_madre;
        $this->PaisMadre = $pais_madre;
        $this->MadrePositiva = $madre_positiva;
        $this->Ocupacion = $ocupacion;
        $this->ObraSocial = $obra_social;
        $this->IdMotivo = $id_motivo;
        $this->Motivo = $motivo;
        $this->Derivacion = $derivacion;
        $this->IdDerivacion = $idderivacion;
        $this->Tratamiento = $tratamiento;
        $this->Usuario = $usuario;
        $this->FechaAlta = $fecha_alta;
        $this->Comentarios = $comentarios;

    }

    /**
     * Método público que genera la consulta de inserción o edición
     * según corresponda, retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaPaciente(){

        // si no existe el protocolo
        if($this->Protocolo == 0){
            $this->nuevoPaciente();
        } else {
            $this->editaPaciente();
        }

        // retorna la clave
        return $this->Protocolo;

    }

    /**
     * Método que genera el nuevo número de historia clínica de
     * acuerdo al laboratorio del usuario
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaHistoria(){

        // inicializamos las variables
        $historia = "";

        // obtenemos la longitud de la clave mas el guión
        $longitud = strlen($this->LaboratorioUsuario) + 2;

        // ahora buscamos el valor máximo
        $consulta = "SELECT MAX(SUBSTRING(diagnostico.v_personas.historia_clinica, $longitud)) + 1 AS historia
                     FROM diagnostico.v_personas
                     WHERE diagnostico.v_personas.id_laboratorio = '$this->LaboratorioUsuario';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si todavía no hay registros
        if ($historia == ""){
            $historia = 1;
        }

        // asignamos en la variable de clase
        $this->Historia = $this->LaboratorioUsuario . "-" . $historia;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido llamado al insertar un paciente
     */
    protected function nuevoPaciente(){

        // obtenemos la nueva historia clínica
        $this->nuevaHistoria();

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.personas
                            (id_laboratorio,
                             historia_clinica,
                             apellido,
                             nombre,
                             documento,
                             tipo_documento,
                             fecha_nacimiento,
                             edad,
                             sexo,
                             estado_civil,
                             hijos,
                             direccion,
                             telefono,
                             celular,
                             compania,
                             mail,
                             localidad_nacimiento,
                             coordenadas_nacimiento,
                             localidad_residencia,
                             coordenadas_residencia,
                             localidad_madre,
                             madre_positiva,
                             ocupacion,
                             obra_social,
                             motivo,
                             derivacion,
                             tratamiento,
                             usuario,
                             comentarios)
                            VALUES
                            (:id_laboratorio,
                             :historia_clinica,
                             :apellido,
                             :nombre,
                             :documento,
                             :tipo_documento,
                             STR_TO_DATE(:fecha_nacimiento, '%d/%m/%Y'),
                             :edad,
                             :sexo,
                             :estado_civil,
                             :hijos,
                             :direccion,
                             :telefono,
                             :celular,
                             :compania,
                             :mail,
                             :localidad_nacimiento,
                             :coordenadas_nacimiento,
                             :localidad_residencia,
                             :coordenadas_residencia,
                             :localidad_madre,
                             :madre_positiva,
                             :ocupacion,
                             :obra_social,
                             :motivo,
                             :derivacion,
                             :tratamiento,
                             :usuario,
                             :comentarios);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":id_laboratorio", $this->LaboratorioUsuario);
        $psInsertar->bindParam(":historia_clinica", $this->Historia);
        $psInsertar->bindParam(":apellido", $this->Apellido);
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":documento", $this->Documento);
        $psInsertar->bindParam(":tipo_documento", $this->IdDocumento);
        $psInsertar->bindParam(":fecha_nacimiento", $this->FechaNacimiento);
        $psInsertar->bindParam(":edad", $this->Edad);
        $psInsertar->bindParam(":sexo", $this->IdSexo);
        $psInsertar->bindParam(":estado_civil", $this->IdEstadoCivil);
        $psInsertar->bindParam(":hijos", $this->Hijos);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":celular", $this->Celular);
        $psInsertar->bindParam(":compania", $this->IdCompania);
        $psInsertar->bindParam(":mail", $this->Mail);
        $psInsertar->bindParam(":localidad_nacimiento", $this->IdLocNacimiento);
        $psInsertar->bindParam(":coordenadas_nacimiento", $this->CoordenadasNac);
        $psInsertar->bindParam(":localidad_residencia", $this->IdLocResidencia);
        $psInsertar->bindParam(":coordenadas_residencia", $this->CoordenadasResid);
        $psInsertar->bindParam(":localidad_madre", $this->IdLocalidadMadre);
        $psInsertar->bindParam(":madre_positiva", $this->MadrePositiva);
        $psInsertar->bindParam(":ocupacion", $this->Ocupacion);
        $psInsertar->bindParam(":obra_social", $this->ObraSocial);
        $psInsertar->bindParam(":motivo", $this->IdMotivo);
        $psInsertar->bindParam(":derivacion", $this->IdDerivacion);
        $psInsertar->bindParam(":tratamiento", $this->Tratamiento);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la id del protocolo
        $this->Protocolo = $this->Link->lastInsertId();

    }

    /**
     * Método protegido llamado al editar un paciente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaPaciente(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.personas SET
                            id_laboratorio = :id_laboratorio,
                            apellido = :apellido,
                            nombre = :nombre,
                            documento = :documento,
                            tipo_documento = :tipo_documento,
                            fecha_nacimiento = STR_TO_DATE(:fecha_nacimiento, '%d/%m/%Y),
                            edad = :edad,
                            sexo = :sexo,
                            estado_civil = :estado_civil,
                            hijos = :hijos,
                            direccion = :direccion,
                            telefono = :telefono,
                            celular = :celular,
                            compania = :compania,
                            mail = :mail,
                            localidad_nacimiento = :localidad_nacimiento,
                            coordenadas_nacimiento = :coordenadas_nacimiento,
                            localidad_residencia = :localidad_residencia,
                            coordenadas_residencia = :coordenadas_residencia,
                            localidad_madre = :localidad_madre,
                            ocupacion = :ocupacion,
                            obra_social = :obra_social,
                            motivo = :motivo,
                            derivacion = :derivacion,
                            tratamiento = :tratamiento,
                            usuario = :usuario,
                            comentarios = :comentarios
                     WHERE diagnostico.personas.protocolo = :protocolo;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":id_laboratorio", $this->LaboratorioUsuario);
        $psInsertar->bindParam(":historia_clinica", $this->Historia);
        $psInsertar->bindParam(":apellido", $this->Apellido);
        $psInsertar->bindParam(":nombre", $this->Nombre);
        $psInsertar->bindParam(":documento", $this->Documento);
        $psInsertar->bindParam(":tipo_documento", $this->IdDocumento);
        $psInsertar->bindParam(":fecha_nacimiento", $this->FechaNacimiento);
        $psInsertar->bindParam(":edad", $this->Edad);
        $psInsertar->bindParam(":sexo", $this->IdSexo);
        $psInsertar->bindParam(":estado_civil", $this->IdEstadoCivil);
        $psInsertar->bindParam(":hijos", $this->Hijos);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":celular", $this->Celular);
        $psInsertar->bindParam(":compania", $this->IdCompania);
        $psInsertar->bindParam(":mail", $this->Mail);
        $psInsertar->bindParam(":localidad_nacimiento", $this->IdLocNacimiento);
        $psInsertar->bindParam(":coordenadas_nacimiento", $this->CoordenadasNac);
        $psInsertar->bindParam(":localidad_residencia", $this->IdLocResidencia);
        $psInsertar->bindParam(":coordenadas_residencia", $this->CoordenadasResid);
        $psInsertar->bindParam(":localidad_madre", $this->IdLocalidadMadre);
        $psInsertar->bindParam(":madre_positiva", $this->MadrePositiva);
        $psInsertar->bindParam(":ocupacion", $this->Ocupacion);
        $psInsertar->bindParam(":obra_social", $this->ObraSocial);
        $psInsertar->bindParam(":motivo", $this->IdMotivo);
        $psInsertar->bindParam(":derivacion", $this->IdDerivacion);
        $psInsertar->bindParam(":tratamiento", $this->Tratamiento);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":protocolo", $this->Protocolo);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro un número de documento, busca el
     * mismo en la base de datos y retorna el número de registros encontrados
     * Utilizado para evitar el ingreso de registros duplicados de
     * pacientes
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $documento - número de documento a verificar
     * @return int
     */
    public function verificaPaciente($documento){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_personas.protocolo AS protocolo
                     FROM diagnostico.v_personas
                     WHERE diagnostico.v_personas.documento = '$documento';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // si no hubo resultados
        if ($resultado->rowCount() == 0){

            // asignamos a la variable
            $protocolo = 0;

        // si encontró
        } else {

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($fila);

        }

        // retornamos los registros
        return $protocolo;

    }

}