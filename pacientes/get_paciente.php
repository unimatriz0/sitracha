<?php

/**
 *
 * pacientes/get_paciente.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (27/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un protocolo y retorna
 * un array json con los datos del registro
 *
*/

// incluimos e instanciamos las clases
require_once ("pacientes.class.php");
$paciente = new Pacientes();

// obtenemos el registro
$paciente->getDatosPaciente($_GET["Protocolo"]);

// retornamos el array json
echo json_encode(array("Protocolo" =>              $paciente->getProtocolo(),
                       "Historia" =>               $paciente->getHistoria(),
                       "IdLaboratorio" =>          $paciente->getIdLaboratorio(),
                       "Laboratorio" =>            $paciente->getHistoria(),
                       "Apellido" =>               $paciente->getApellido(),
                       "Nombre" =>                 $paciente->getNombre(),
                       "Documento" =>              $paciente->getDocumento(),
                       "IdDocumento" =>            $paciente->getIdDocumento(),
                       "FechaNacimiento" =>        $paciente->getFechaNacimiento(),
                       "Edad" =>                   $paciente->getEdad(),
                       "IdSexo" =>                 $paciente->getIdSexo(),
                       "IdEstadoCivil" =>          $paciente->getIdEstadoCivil(),
                       "Hijos" =>                  $paciente->getHijos(),
                       "Direccion" =>              $paciente->getDireccion(),
                       "Telefono" =>               $paciente->getTelefono(),
                       "Celular" =>                $paciente->getCelular(),
                       "IdCompania" =>             $paciente->getIdCompania(),
                       "Mail" =>                   $paciente->getMail(),
                       "JurisdiccionNacimiento" => $paciente->getJurisdiccionNacimiento(),
                       "IdLocNacimiento" =>        $paciente->getIdLocNacimiento(),
                       "LocalidadNacimiento" =>    $paciente->getLocNacimiento(),
                       "PaisNacimiento" =>         $paciente->getPaisNacimiento(),
                       "CoordenadasNacimiento" =>  $paciente->getCoordenadasNac(),
                       "JurisdiccionResidencia" => $paciente->getJurisdiccionResidencia(),
                       "IdLocResidencia" =>        $paciente->getIdLocResidencia(),
                       "LocalidadResidencia" =>    $paciente->getLocResidencia(),
                       "PaisResidencia" =>         $paciente->getPaisResidencia(),
                       "CoordenadasResidencia" =>  $paciente->getCoordenadasResid(),
                       "JurisdiccionMadre" =>      $paciente->getJurisdiccionMadre(),
                       "IdLocalidadMadre" =>       $paciente->getIdLocalidadMadre(),
                       "LocalidadMadre" =>         $paciente->getLocalidadMadre(),
                       "PaisMadre" =>              $paciente->getPaisMadre(),
                       "MadrePositiva" =>          $paciente->getMadrePositiva(),
                       "Ocupacion" =>              $paciente->getOcupacion(),
                       "ObraSocial" =>             $paciente->getObraSocial(),
                       "IdMotivo" =>               $paciente->getIdMotivo(),
                       "Derivacion" =>             $paciente->getIdDerivacion(),
                       "Tratamiento" =>            $paciente->getTratamiento(),
                       "Usuario" =>                $paciente->getUsuario(),
                       "FechaAlta" =>              $paciente->getFechaAlta(),
                       "Comentarios" =>            $paciente->getComentarios()));

?>