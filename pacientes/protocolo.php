<?php

/**
 *
 * pacientes/protocolo.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (20/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que genera el documento pdf con el protocolo y lo presenta en pantalla
 * recibe como parámetro el protocolo del paciente
 *
*/

// incluimos e instanciamos las clases
require_once("protocolos.class.php");
$protocolo = new Protocolo($_GET["protocolo"]);

// enviamos los encabezados
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0, false");
header("Pragma: no-cache");
?>

<!-- cargamos el documento -->
<object data="temp/protocolo.pdf"
        type="application/pdf"
        width="850" height="500">
</object>
?>
