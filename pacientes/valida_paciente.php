<?php

/**
 *
 * pacientes/valida_paciente.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que recibe por get el número de documento de un
 * paciente y retorna el número de protocolo si existe o cero
 * si no está declarado
 *
*/

// incluimos e instanciamos las clases
require_once("pacientes.class.php");
$paciente = new Pacientes();

// verificamos si existe
$protocolo = $paciente->verificaPaciente($_GET["Documento"]);

// retornamos también el número de documento
echo json_encode(array("Protocolo" => $protocolo,
                       "Documento" => $_GET["Documento"]));

?>