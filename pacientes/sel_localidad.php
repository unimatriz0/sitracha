<?php

/**
 *
 * pacientes/sel_localidad.php
 *
 * @package     Diagnostico
 * @subpackage  Pacientes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get parte del nombre de una localidad y
 * el campo en que estamos buscando, presenta la grilla con la
 * nómina de logalidades y envía a la rutina javascript la
 * localidad seleccionada y el campo al que pertenece
 *
*/

// incluimos e instanciamos la clase de localidades
require_once("../localidades/localidades.class.php");
$localidades = new Localidades();

// obtenemos los valores recibidos por get
$localidad = $_GET["Localidad"];
$campo = $_GET["Campo"];

// buscamos en la tabla de localidades
$nomina = $localidades->nominaLocalidades($localidad);

// si no hubo registros
if (count($nomina) == 0){

    // presenta el aviso
    echo "<h2>No se han encontrado Localidades coincidentes</h2>";

// si hubo registros
} else {

    // define la tabla
    echo "<table width='600px' align='center' border='0'>";

    // define los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>País</th>";
    echo "<th>Jurisdicción</th>";
    echo "<th>Localidad</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach ($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // presentamos el registro
        echo "<td>$pais</td>";
        echo "<td>$provincia</td>";
        echo "<td>$localidad</td>";

        // ahora armamos el enlace
        echo "<td>";
        echo "<input type='button'
                     name='btnSelLocalidad'
                     id='btnSelLocalidad'
                     class='botonconfirmar'
                     title='Pulse para seleccionar'
                     onClick='pacientes.selLocalidad(" . chr(34) . $campo . chr(34) . ", " . chr(34) . $pais . chr(34) . "," . chr(34) . $provincia . chr(34) . ", " . chr(34) . $localidad . chr(34) . ", " . chr(34) . $codloc . chr(34) . ")'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cierra la tabla
    echo "</tbody>";
    echo "</table>";

}
