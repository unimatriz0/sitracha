<?php

/**
 *
 * Class Sexos | sexos/sexos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Sexos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de sexos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Sexos{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Link;                  // puntero a la base de datos
    protected $FechaAlta;             // fecha de alta del registro
    protected $IdSexo;                // clave del sexo
    protected $Sexo;                  // nombre del sexo

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdSexo = 0;
        $this->Sexo = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método público que retorna un array asociativo con la tabla de sexos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaSexos(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.sexos.id AS id_sexo,
                            diccionarios.sexos.sexo AS sexo,
                            DATE_FORMAT(diccionarios.sexos.fecha_alta, '%d/%m/%Y') AS fecha_alta,
                            diccionarios.sexos.usuario AS id_usuario,
                            cce.responsables.usuario AS usuario
                     FROM diccionarios.sexos INNER JOIN cce.responsables ON diccionarios.sexos.usuario = cce.responsables.id;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

}