<?php

/**
 *
 * lista_sexos.php
 *
 * @package     Diagnostico
 * @subpackage  Sexos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de sexos
 *
*/

// incluimos e instanciamos la clase
require_once("sexos.class.php");
$sexos = new Sexos();

// obtenemos la nómina
$nomina = $sexos->nominaSexos();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_sexo,
                        "Sexo" => $sexo);

}

// retornamos el vector
echo json_encode($jsondata);

?>