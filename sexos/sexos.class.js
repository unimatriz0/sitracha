/*
 * Nombre: sexos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 06/03/2018
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de sexos
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del formulario de sexos
 */
class Sexos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // por ahora no tenemos variables para inicializar

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un objeto html
     * @param {int} idsexo clave del elemento preseleccionado
     * Método que recibe como parámetro la id de un elemento del formulario
     * y carga en el la nómina de sexos, si además recibe la clave de
     * un sexo la predetermina
     */
    nominaSexos(idelemento, idsexo){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "sexos/lista_sexos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idsexo) != "undefined"){

                        // si coincide
                        if (data[i].Id == idsexo){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Sexo + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Sexo + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Sexo + "</option>");
                    }

                }

        }});

    }

}