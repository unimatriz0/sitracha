<?php

/**
 *
 * Class Agudos | agudo/agudo.class.php
 *
 * @package     Diagnostico
 * @subpackage  Agudo
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * síntomas de chagas agudo
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */

// declaración de la clase
class Agudo {

    // declaración de variables
    protected $Link;              // puntero a la base de datos
    protected $Id;                // clave del registro
    protected $Paciente;          // clave del paciente
    protected $Sintomas;          // indica si tuvo síntomas
    protected $Observaciones;     // observaciones del usuario
    protected $IdUsuario;         // clave del usuario
    protected $Usuario;           // nombre del usuario
    protected $FechaAlta;         // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initAgudo();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // método que inicializa las variables de clase
    protected function initAgudo(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Sintomas = 0;
        $this->Observaciones = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setSintomas($sintomas){
        $this->Sintomas = $sintomas;
    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getSintomas(){
        return $this->Sintomas;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $protocolo clave del paciente
     * Método que recibe como parámetro la clave de un
     * registro y asigna en las variables de clase los
     * valores del mismo
     */
    public function getDatosAgudo($protocolo){

        // componemos la consulta y la ejecutamos
        $consulta = "SELECT diagnostico.v_agudo.id AS id,
                            diagnostico.v_agudo.paciente AS paciente,
                            diagnostico.v_agudo.sintomas AS sintomas,
                            diagnostico.v_agudo.observaciones AS observaciones,
                            diagnostico.v_agudo.usuario AS usuario,
                            diagnostico.v_agudo.fecha AS fecha
                     FROM diagnostico.v_agudo
                     WHERE diagnostico.v_agudo.paciente = '$protocolo'; ";
        $resultado = $this->Link->query($consulta);

        // si hubo registros
        if ($resultado->rowCount() != 0){

            // obtenemos el vector
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // extraemos el registro y asignamos los valores
            // en las variables de clase
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Sintomas = $sintomas;
            $this->Observaciones = $observaciones;
            $this->Usuario = $usuario;
            $this->FechaAlta = $fecha;

        // si no hubo registros
        } else {

            // inicializamos las variables
            $this->initAgudo();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public function grabaAgudo(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoAgudo();
        } else {
            $this->editaAgudo();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un
     * nuevo registro
     */
    protected function nuevoAgudo(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.agudo
                            (paciente,
                             sintomas,
                             observaciones,
                             usuario)
                            VALUES
                            (:paciente,
                             :sintomas,
                             :observaciones,
                             :idusuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":sintomas",      $this->Sintomas);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":idusuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaAgudo(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.agudo SET
                            sintomas = :sintomas,
                            observaciones = :observaciones,
                            usuario = :idusuario
                     WHERE diagnostico.agudo.id = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":sintomas",      $this->Sintomas);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":idusuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",            $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}
?>