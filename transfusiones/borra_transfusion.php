<?php

/**
 *
 * transfusiones/borra_transfusion.php
 *
 * @package     Diagnostico
 * @subpackage  Transfusiones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/07/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por get la clave de un registro y elimina
 * el mismo
 *
*/

// incluimos e instanciamos las clases
require_once ("transfusiones.class.php");
$transfusion = new Transfusiones();

// ejecutamos la consulta
$transfusion->borraTransfusion($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>