<?php

/**
 *
 * transfusiones/nomina_transfusion.php
 *
 * @package     Diagnostico
 * @subpackage  Transfusiones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/06/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por el el protocolo de un paciente y retorna
 * el array json con la nómina de transfusiones
 *
*/

// incluimos e instanciamos las clases
require_once ("transfusiones.class.php");
$transfusion = new Transfusiones();

// obtenemos la nómina
$nomina = $transfusion->nominaTransfusiones($_GET["id"]);

// retornamos
echo json_encode($nomina);

?>