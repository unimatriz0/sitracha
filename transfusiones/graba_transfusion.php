<?php

/**
 *
 * transfusiones/graba_transfusion.php
 *
 * @package     Diagnostico
 * @subpackage  Transfusiones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/07/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por post los datos del registro y ejecuta la
 * consulta en el servidor, retorna en un array json la clave
 * del nuevo registro
 *
*/

// incluimos e instanciamos las clases
require_once ("transfusiones.class.php");
$transfusion = new Transfusiones();

// fijamos los valores
$transfusion->setFechaTransfusion($_POST["Fecha"]);
$transfusion->setMotivo($_POST["Motivo"]);
$transfusion->setIdLocalidad($_POST["IdLocalidad"]);
$transfusion->setIdProtocolo($_POST["Protocolo"]);

// grabamos el registro
$resultado = $transfusion->grabaTransfusion();

// retornamos
echo json_encode(array("Id" => $resultado));

?>