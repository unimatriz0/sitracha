<?php

/**
 *
 * Class Transfusiones | transfusiones/transfusiones.class.php
 *
 * @package     Diagnostico
 * @subpackage  Transfusiones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/03/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla la tabla de transfusiones que ha recibido
 * el paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Transfusiones{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Link;                  // puntero a la base de datos
    protected $IdTransfusion;         // clave del registro
    protected $IdProtocolo;           // clave del protocolo
    protected $FechaTransfusion;      // fecha en que fue realizada
    protected $Motivo;                // descripción de la causa
    protected $Localidad;             // localidad de la transfusión
    protected $IdLocalidad;           // clave de la localidad
    protected $Provincia;             // provincia de la transfusión
    protected $Pais;                  // país de la transfusión
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdTransfusion = 0;
        $this->IdProtocolo = 0;
        $this->FechaTransfusion = "";
        $this->Localidad = "";
        $this->IdLocalidad = "";
        $this->Provincia = "";
        $this->Pais = "";
        $this->Motivo = "";
        $this->FechaAlta = "";
        $this->Usuario = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdTransfusion($idtransfusion){
        $this->IdTransfusion = $idtransfusion;
    }
    public function setIdProtocolo($idprotocolo){
        $this->IdProtocolo = $idprotocolo;
    }
    public function setFechaTransfusion($fecha){
        $this->FechaTransfusion = $fecha;
    }
    public function setIdLocalidad($idlocalidad){
        $this->IdLocalidad = $idlocalidad;
    }
    public function setMotivo($motivo){
        $this->Motivo = $motivo;
    }

    // métodos de retorno de valores
    public function getIdTransfusion(){
        return $this->IdTransfusion;
    }
    public function getIdProtoclo(){
        return $this->IdProtocolo;
    }
    public function getFechaTransfusion(){
        return $this->FechaTransfusion;
    }
    public function getMotivo(){
        return $this->Motivo;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getIdLocalidad(){
        return $this->IdLocalidad;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getFechaAlta(){
        $this->FechaAlta;
    }
    public function getUsuario(){
        $this->Usuario;
    }

    /**
     * Método que recibe como parámetro el protocolo de un paciente y retorna
     * un array con la nómina de transfusiones de ese paciente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $protocolo - clave del protocolo del paciente
     * @return array
     */
    public function nominaTransfusiones($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_transfusiones.id_transfusion AS id_transfusion,
                            diagnostico.v_transfusiones.id_protocolo AS id_protocolo,
                            diagnostico.v_transfusiones.fecha_transfusion AS fecha_transfusion,
                            diagnostico.v_transfusiones.idlocalidad AS idlocalidad,
                            diagnostico.v_transfusiones.localidad AS localidad,
                            diagnostico.v_transfusiones.motivo AS motivo,
                            diagnostico.v_transfusiones.laboratorio AS laboratorio,
                            diagnostico.v_transfusiones.id_laboratorio AS id_laboratorio,
                            diagnostico.v_transfusiones.usuario AS usuario,
                            diagnostico.v_transfusiones.fecha_alta AS fecha_alta
                     FROM diagnostico.v_transfusiones
                     WHERE diagnostico.v_transfusiones.id_protocolo = '$protocolo'
                     ORDER BY STR_TO_DATE(diagnostico.v_transfusiones.fecha_transfusion, '%d/%m/%Y');";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que ejecuta la consulta de edición o inserción
     * según el caso, retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaTransfusion(){

        // si está insertanto
        if ($this->IdTransfusion == 0){
            $this->nuevaTransfusion();
        } else {
            $this->editaTransfusion();
        }

        // retornamos la clave
        return $this->IdTransfusion;

    }

    /**
     * Método que ejecuta la consulta de inserción de un
     * nuevo registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaTransfusion(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.transfusiones
                            (id_protocolo,
                             fecha_transfusion,
                             localidad,
                             motivo,
                             id_usuario)
                            VALUES
                            (:id_protocolo,
                             STR_TO_DATE(:fecha_transfusion, '%d/%m/%Y'),
                             :localidad,
                             :motivo,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->IdProtocolo);
        $psInsertar->bindParam(":fecha_transfusion", $this->FechaTransfusion);
        $psInsertar->bindParam(":localidad", $this->IdLocalidad);
        $psInsertar->bindParam(":motivo", $this->Motivo);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdTransfusion = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición de un
     * registro
     * @author  Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaTransfusion(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.transfusiones SET
                            id_protocolo = :id_protocolo,
                            fecha_transfusion = STR_TO_DATE(:fecha_transfusion, '%d/%m/%Y'),
                            localidad = :localidad,
                            motivo = :motivo,
                            id_usuario = :id_usuario
                     WHERE diagnostico.transfusiones.id = :id_transfusion;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->IdProtocolo);
        $psInsertar->bindParam(":fecha_transfusion", $this->FechaTransfusion);
        $psInsertar->bindParam(":localidad", $this->IdLocalidad);
        $psInsertar->bindParam(":motivo", $this->Motivo);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_transfusion", $this->IdTransfusion);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idtransfusion - clave del registro
     */
    public function borraTransfusion($idtransfusion){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.transfusiones
                     WHERE diagnostico.transfusiones.id = '$idtransfusion';";
        $this->Link->exec($consulta);

    }

}