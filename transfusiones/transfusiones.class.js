/*
 * Nombre: transfusiones.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 21/03/2018
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de las transfusiones del
 *              paciente
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las transfusiones de un paciente
 */
class Transfusiones {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // el layer de propósito general
        this.layerTransfusiones = "";
        this.layerLocalidades = "";

        // inicializamos las variables
        this.initTransfusiones();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de la clase
     */
    initTransfusiones(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializa las variables
        this.IdTransfusion = 0;         // clave del registro
        this.IdProtocolo = 0;           // clave del protocolo
        this.FechaTransfusion = "";     // fecha en que se realizó
        this.IdLocalidad = "";          // localidad de la transfusión
        this.Motivo = "";               // motivo de la transfusión
        this.Usuario = "";              // nombre del usuario
        this.FechaAlta = "";            // fecha de alta del registro

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la grilla de las transfusiones del paciente activo
     */
    grillaTransfusiones(){

        // reiniciamos la sesion
        sesion.reiniciar();

        // declaración de variables
        var mensaje;
        var protocolo = document.getElementById("protocolo_paciente").value;

        // verifica que exista un paciente activo
        if (protocolo == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe tener en pantalla un paciente antes<br>";
            mensaje += "de declarar las transfusiones";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // fijamos la variable control
        pacientes.setTransfusiones();

        // cargamos el formulario en una ventana emergente
        this.layerTransfusiones = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Transfusiones del Paciente',
                    draggable: 'title',
                    width: 850,
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'transfusiones/grilla_transfusiones.html',
                        reload: 'strict'
                    }
        });
        this.layerTransfusiones.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario antes de
     * enviarlo a grabar
     */
    verificaTransfusion(){

        // reiniciamos la sesion
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no ingresó la fecha
        if (document.getElementById("fecha_transfusion").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de la transfusión";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_transfusion").focus();
            return false;

        // si declaró
        } else {

            // asignamos en la variable de clase
            this.FechaTransfusion = document.getElementById("fecha_transfusion").value;

        }

        // si no declaró la localidad
        if (this.IdLocalidad == 0){

            // presenta el mensaje y retorna
            mensaje = "Indique la localidad de la transfusión";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("localidad_transfusion").focus();
            return false;

        }

        // si no declaró el motivo
        if (document.getElementById("motivo_transfusion").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el motivo de la transfusión";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("motivo_transfusion").focus();
            return false;

        // si declaró
        } else {

            // asignamos en la variable de clase
            this.Motivo = document.getElementById("motivo_transfusion").value;

        }

        // agregamos el protocolo
        this.IdProtocolo = document.getElementById("protocolo_paciente").value;

        // grabamos el registro
        this.grabaTransfusion();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía al servidor los datos del formulario
     * para grabarlos
     */
    grabaTransfusion(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosTransfusion = new FormData();

        // agregamos los datos del registro
        datosTransfusion.append("Fecha", this.FechaTransfusion);
        datosTransfusion.append("Motivo", this.Motivo);
        datosTransfusion.append("IdLocalidad", this.IdLocalidad);
        datosTransfusion.append("Protocolo", this.IdProtocolo);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "transfusiones/graba_transfusion.php",
            data: datosTransfusion,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // inicializamos las variables
                    transfusiones.initTransfusiones();

                    // limpiamos el formulario
                    transfusiones.limpiaFormulario();

                    // cargamos la grilla
                    transfusiones.cargaTransfusiones();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro la clave de un registro
     * y luego de pedir confirmación, envía la consulta al
     * servidor para su eliminación
     */
    borraTransfusion(idtransfusion){

        // reiniciamos la sesion
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Transfusión',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                 // llama la rutina php
                 $.get('transfusiones/borra_transfusion.php?id='+idtransfusion,
                     function(data){

                         // si retornó correcto
                         if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // inicializamos las variables
                            transfusiones.initTransfusiones();

                            // inicializamos el formulario
                            transfusiones.limpiaFormulario();

                            // cargamos la grilla
                            transfusiones.cargaTransfusiones();

                         // si no pudo eliminar
                         } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                         }

                     }, "json");

             },
             cancel: function(){
                 Confirmacion.destroy();
             },
             confirmButton: 'Aceptar',
             cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene el protocolo del paciente y carga
     * el div con la grilla de transfusiones recibidas
     */
    cargaTransfusiones(){

        // reiniciamos la sesión
        sesion.reiniciar();

        $.get('transfusiones/nomina_transfusion.php?id=' + document.getElementById("protocolo_paciente").value,
        function(data){

            // cargamos la grilla
            transfusiones.cargaGrilla(data);

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} vector con los datos de las transfusiones
     * Método que recibe como parámetro el vector con las
     * transfusiones recibidas por el paciente y carga la grilla
     */
    cargaGrilla(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var texto = "";

        // limpia el cuerpo de la tabla
        $("#cuerpo_transfusion").html('');

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // abrimos la fila
            texto = "<tr>";

            // agregamos la fecha
            texto += "<td>";
            texto += datos[i].fecha_transfusion;
            texto += "</td>";

            // agregamos la localidad
            texto += "<td>";
            texto += datos[i].localidad;
            texto += "</td>";

            // agregamos el motivo
            texto += "<td>";
            texto += datos[i].motivo;
            texto += "</td>";

            // agregamos la fecha de alta
            texto += "<td>";
            texto += datos[i].fecha_alta;
            texto += "</td>";

            // agregamos el usuario
            texto += "<td>";
            texto += datos[i].usuario;
            texto += "</td>";

            // agrega el botón eliminar
            texto += "<td>";
            texto += "<input type='button' ";
            texto += "       name='btnEliminaTransfusion' ";
            texto += "       id='btnEliminaTransfusion' ";
            texto += "       title='Pulse para eliminar el registro' ";
            texto += "       class='botonborrar' ";
            texto += "       onClick='transfusiones.borraTransfusion(" + datos[i].id_transfusion + ")'>";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos la fila a la tabla
            $("#cuerpo_transfusion").append(texto);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de datos
     */
    limpiaFormulario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializamos el formulario
        document.getElementById("fecha_transfusion").value = "";
        document.getElementById("localidad_transfusion").value = "";
        document.getElementById("motivo_transfusion").value = "";
        document.getElementById("alta_transfusion").value = fechaActual();
        document.getElementById("usuario_transfusion").value = sessionStorage.getItem("Usuario");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente con la nómina de
     * localidades de la transfusión
     */
    buscaLocalidad(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no recibió la localidad
        if (document.getElementById("localidad_transfusion").value == ""){

            // presenta la alerta y retorna
            mensaje = "Debe ingresar parte del nombre de la Localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // abrimos el cuadro de diálogo
        // cargamos el formulario en una ventana emergente
        this.layerLocalidades = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        repositionOnContent: true,
                        overlay: false,
                        title: 'Búsqueda de Localidades',
                        draggable: 'title',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                        this.destroy();
                        },
                        ajax: {
                            url: 'transfusiones/sel_localidad.php?Localidad='+document.getElementById("localidad_transfusion").value,
                            reload: 'strict'
                        }
        });
        this.layerLocalidades.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} clave de la localidad
     * @param {string} nombre nombre de la localidad
     * Método que recibe como parámetro la clave indec de
     * la localidad, la asigna a la variable de clase y
     * cierra el formulario de búsqueda
     */
    selLocalidad(clave, nombre){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en la variable de clase y en el formulario
        this.IdLocalidad = clave;
        document.getElementById("localidad_transfusion").value = nombre;

        // destruimos el formulario
        this.layerLocalidades.destroy();

        // seteamos el foco en el motivo
        document.getElementById("motivo_transfusion").focus();

    }

}