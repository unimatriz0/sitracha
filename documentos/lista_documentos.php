<?php

/**
 *
 * documentos/lista_documentos.php
 *
 * @package     Diagnostico
 * @subpackage  Documentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de documentos
 * 
*/

// incluimos e instanciamos la clase
require_once("documentos.class.php");
$documentos = new Documentos();

// obtenemos la nómina
$nomina = $documentos->listaDocumentos();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_documento,
                        "Documento" => $descripcion);

}

// retornamos el vector
echo json_encode($jsondata);

?>