<?php

/**
 *
 * Class Documentos | documentos/documentos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Documentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre el diccionario de 
 * tipos de documento
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Documentos{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Link;                  // puntero a la base de datos
    protected $IdDocumento;           // clave del tipo de documento
    protected $TipoDocumento;         // nombre completo del tipo
    protected $DescAbreviada;         // sigla del documento
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdDocumento = 0;
        $this->TipoDocumento = "";
        $this->DescAbreviada = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdDocumento($iddocumento){
        $this->IdDocumento = $iddocumento;
    }
    public function setTipoDocumento($tipodocumento){
        $this->TipoDocumento = $tipodocumento;
    }
    public function setDescripcion($descripcion){
        $this->DescAbreviada = $descripcion;
    }

    // métodos de retorno de valores
    public function getIdDocumento(){
        return $this->IdDocumento;
    }
    public function getTipoDocumento(){
        return $this->TipoDocumento;
    }
    public function getDescripcion(){
        return $this->DescAbreviada;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método público que retorna la nómina de documentos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaDocumentos(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.tipo_documento.id_documento AS id_documento,
                            diccionarios.tipo_documento.tipo_documento AS tipo_documento,
                            diccionarios.tipo_documento.des_abreviada AS descripcion,
                            cce.responsables.usuario AS usuario,
                            DATE_FORMAT(diccionarios.tipo_documento.fecha, '%d/%m/%Y') AS fecha_alta
                     FROM diccionarios.tipo_documento INNER JOIN cce.responsables ON diccionarios.tipo_documento.id_usuario = cce.responsables.id
                     ORDER BY diccionarios.tipo_documento.tipo_documento;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que según el valor de la clave genera la consulta de
     * edición o actualización, retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / editado
     */
    public function grabaDocumento(){

        // si está insertando
        if ($this->IdDocumento == 0){

            // inserta un nuevo registro
            $this->nuevoDocumento();

        // si está editando
        } else {

            // actualiza el registro
            $this->editaDocumento();

        }

        // retornamos la clave
        return $this->IdDocumento;

    }

    /**
     * Método que genera la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoDocumento(){

        // componemos la consulta
        $consulta = "INSERT INTO diccionarios.tipo_documento
                            (tipo_documento,
                             des_abreviada,
                             id_usuario)
                            VALUES
                            (:tipo_documento,
                             :descripcion,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":tipo_documento", $this->TipoDocumento);
        $psInsertar->bindParam(":descripcion", $this->DescAbreviada);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asignamos la clave
        $this->IdDocumento = $this->Link->lastInsertId();

    }

    /**
     * Método que genera la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaDocumento(){

        // componemos la consulta
        $consulta = "UPDATE diccionarios.tipo_documento SET
                            tipo_documento = :tipo_documento,
                            des_abreviada = :descripcion,
                            id_usuario = :id_usuario
                     WHERE diccionarios.tipo_documento.id_documento = :id_documento;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":tipo_documento", $this->TipoDocumento);
        $psInsertar->bindParam(":descripcion", $this->DescAbreviada);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_documento", $this->IdDocumento);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}