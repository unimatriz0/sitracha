<?php

/**
 *
 * documentos/form_documentos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Documentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario para la edición del 
 * tipo de documento
 * 
*/

// incluimos e instanciamos la clase
require_once("documentos.class.php");
$documentos = new Documentos();

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["ID"])){
    $usuario_actual = $_SESSION["Usuario"];
}
session_write_close();

// presentamos el título
echo "<h2>Nómina de Documentos</h2>";

// definimos la tabla
echo "<table width='70%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Documento</th>";
echo "<th align='left'>Desc.</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la matriz de documentos
$nomina = $documentos->listaDocumentos();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el tipo de documento
    echo "<td>";
    echo "<span class='tooltip'
                title='Tipo de Documento'>";
    echo "<input type='text'
                 name='tipo_documento_$id_documento'
                 id='tipo_documento_$id_documento'
                 value='$tipo_documento'
                 size='40'>";
    echo "</span>";
    echo "</td>";

    // presenta la abreviatura
    echo "<td>";
    echo "<span class='tooltip'
                title='Abreviatura'>";
    echo "<input type='text'
                 name='abreviatura_$id_documento'
                 id='abreviatura_$id_documento'
                 value='$descripcion'
                 size='10'>";
    echo "</td>";

    // presenta el usuario y la fecha de alta
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // arma el enlace
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaDocumento'
           id='btnGrabaDocumento'
           title='Pulse para grabar el registro'
           onClick='documentos.verificaDocumento($id_documento)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// ahora agregamos la fila de altas
echo "<tr>";

// presentamos el tipo de documento
echo "<td>";
echo "<span class='tooltip'
            title='Tipo de Documento'>";
echo "<input type='text'
                name='tipo_documento_nuevo'
                id='tipo_documento_nuevo'
                size='40'>";
echo "</span>";
echo "</td>";

// presenta la abreviatura
echo "<td>";
echo "<span class='tooltip'
            title='Abreviatura'>";
echo "<input type='text'
                name='abreviatura_nuevo'
                id='abreviatura_nuevo'
                size='10'>";
echo "</td>";

// obtenemos el usuario actual y la fecha de alta
$fecha_alta = date('d/m/Y');

// presenta el usuario y la fecha de alta
echo "<td align='center'>$usuario_actual</td>";
echo "<td align='center'>$fecha_alta</td>";

// arma el enlace
echo "<td align='center'>";
echo "<input type='button'
        name='btnGrabaDocumento'
        id='btnGrabaDocumento'
        title='Ingresa un nuevo registro'
        onClick='documentos.verificaDocumento()'
        class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>