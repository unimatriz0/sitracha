<?php

/**
 *
 * documentos/graba_documento.php
 *
 * @package     Diagnostico
 * @subpackage  Documentos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y 
 * ejecuta la consulta en el servidor, retorna el resultado
 * de la operación (cero si es error)
 * 
*/

// incluimos e instanciamos la clase
require_once("documentos.class.php");
$documentos = new Documentos();

// si recibió la id
if (!empty($_POST["Id"])){
    $documentos->setIdDocumento($_POST["Id"]);
}

// setea el resto de las propiedades
$documentos->setTipoDocumento($_POST["TipoDocumento"]);
$documentos->setDescripcion($_POST["Descripcion"]);

// ejecutamos la consulta
$error = $documentos->grabaDocumento();

// retornamos el resultado
echo json_encode(array("Error" => $error));
?>