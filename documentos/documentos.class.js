/*
 * Nombre: documentos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 28/02/2018
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de tipos de
 *              documentos
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre los tipos de documento
 */
class Documentos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initDocumentos();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDocumentos(){

        // inicializamos las variables
        this.IdDocumento = 0;
        this.TipoDocumento = "";
        this.Descripcion = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de tipos de documento
     */
    muestraDocumentos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("documentos/form_documentos.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} iddocumento - clave del tipo de documento
     * Método llamado antes de grabar el registro de documentos, recibe como
     * parámetro la clave del registro que es también la id del campo, si
     * no la recibe, asume que es un alta
     */
    verificaDocumento(iddocumento){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió la id
        if (typeof(iddocumento) == "undefined"){

            // asigna en la variable local
            iddocumento = "nuevo";

        // si la recibió
        } else {

            // asigna en la variable de clase
            this.IdDocumento = iddocumento;

        }

        // si no ingresò el nombre completo
        if (document.getElementById("tipo_documento_" + iddocumento).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre completo";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tipo_documento_" + iddocumento).focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.TipoDocumento = document.getElementById("tipo_documento_" + iddocumento).value;

        }

        // si no ingresó la abreviatura
        if (document.getElementById("abreviatura_" + iddocumento).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la abreviatura";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("abreviatura_" + iddocumento).focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Descripcion = document.getElementById("abreviatura_" + iddocumento).value;

        }

        // graba el registro
        this.grabaDocumento();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar los datos del formulario,
     * ejecuta la consulta en la base de datos
     */
    grabaDocumento(){

        // inicializamos el formulario
        var datosDocumento = new FormData();

        // si existe la clave
        if (this.IdDocumento !== 0){
            datosDocumento.append("Id", this.IdDocumento);
        }

        // agregamos los otros valores
        datosDocumento.append("TipoDocumento", this.TipoDocumento);
        datosDocumento.append("Descripcion", this.Descripcion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "documentos/graba_documento.php",
            type: "POST",
            data: datosDocumento,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    documentos.initDocumentos();

                    // recarga el formulario para reflejar los cambios
                    documentos.muestraDocumentos();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} iddocumento - clave preseleccionada
     * Método que recibe como parámetro la id de un campo de
     * formulario y carga en ese elemento la lista de tipos de
     * documento, si recibe la clave de un documento lo
     * preselecciona
     */
    nominaDocumentos(idelemento, iddocumento){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "documentos/lista_documentos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(iddocumento) != "undefined"){

                        // si coincide
                        if (data[i].Id == iddocumento){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Documento + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Documento + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Documento + "</option>");
                    }

                }

        }});

    }

}