<?php

/**
 *
 * nominaadversos | tratamiento/nominaadversos.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna el array json con los datos del tratamiento
 * del paciente cuya clave recibe por get
*/

// incluimos e instanciamos las clases
require_once("adversos.class.php");
$adversos = new Adversos();

// obtenemos la nómina
$nomina = $adversos->nominaAdversos($_GET["id"]);

// retornamos el array
echo json_encode($nomina);

?>