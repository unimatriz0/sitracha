/*
 * Nombre: adversospac.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 17/06/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de la tabla de
 *              efectos adversos del tratamiento
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de los efectos
 * adversos de los tratamientos recibidos por el paciente
 */
class AdversosPac {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initAdversos();

        // declaramos el layer
        this.layerAdversos = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initAdversos(){

        // inicializamos las variables
        this.IdAdverso = 0;               // clave del registro
        this.Tratamiento = 0;             // clave del registro de tratamiento
        this.Adverso = 0;                 // clave del efecto adverso
        this.Fecha = "";                  // fecha de ocurrencia del efecto

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que muestra el layer emergente y carga el
     * formulario en el
     */
    muestraAdversos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // de otra forma cargamos el layer
        this.layerAdversos = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: false,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Efectos ADversos',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    width: 450,
                    responsiveHeight: true,
                    position: {x:850, y:200},
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    ajax: {
                        url: 'tratamiento/form_adversos.html',
                        reload: 'strict'
                    }
            });

        // mostramos el pdf
        this.layerAdversos.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idtratamiento - clave del registro
     * Método que recibe como parámetro la clave de la
     * tabla de tratamiento y obtiene la nómina de efectos
     * adversos para ese registro
     */
    nominaAdversos(idtratamiento){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si recició porque fue llamado desde
        // la tabla de tratamiento
        if (typeof(idtratamiento) != "undefined"){

            // asignamos en la variable de clase
            this.Tratamiento = idtratamiento;

        }

        // llama la rutina php
        $.get('tratamiento/nominaadversos.php?id='+this.Tratamiento,
            function(data){

                // llamamos la rutina pasándole el vector
                adversospac.cargaAdversos(data);

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos, vector con los resultados
     * Método que recibe como parámetro el vector con la
     * nómina de efectos adversos y carga la grilla
     */
    cargaAdversos(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos las variables
        var texto = "";

        // limpia el cuerpo de la tabla
        $("#cuerpo_adversos").html('');

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // componemos la fila
            texto = "<tr>";

            // abrimos la primer columna
            texto += "<td>";
            texto += datos[i].adverso;
            texto += "</td>";

            // agregamos la fecha
            texto += "<td>";
            texto += datos[i].fecha;
            texto += "</td>";

            // agrega el usuario
            texto += "<td>";
            texto += datos[i].usuario;
            texto += "</td>";

            // agrega la fecha de alta
            texto += "<td>";
            texto += datos[i].alta;
            texto += "</td>";

            // agrega el botón eliminar
            texto += "<td>";
            texto += "<input type='button' ";
            texto += "       name='btnEliminaAdverso' ";
            texto += "       id='btnEliminaAdverso' ";
            texto += "       title='Pulse para eliminar el registro' ";
            texto += "       class='botonborrar' ";
            texto += "       onClick='adversospac.borraAdverso(" + datos[i].id + ")'>";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos la fila a la tabla
            $("#cuerpo_adversos").append(texto);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que
     * verifica el formulario de efectos adversos antes
     * de enviarlo al servidor
     */
    validaAdverso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no seleccionó el efecto
        if (document.getElementById("efecto_tratamiento").value == 0){

            // presenta el mensaje
            mensaje = "Seleccione el efecto de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no seleccionó la fecha
        if (document.getElementById("fecha_efecto").value == ""){

            // presenta el mensaje
            mensaje = "Indique la fecha de ocurrencia del efecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // la id del tratamiento la asigna al cargar la grilla

        // asignamos los valores
        this.Adverso = document.getElementById("efecto_tratamiento").value;
        this.Fecha = document.getElementById("fecha_efecto").value;

        // grabamos el registro
        this.grabaAdverso();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * envía los datos al servidor
     */
    grabaAdverso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosAdverso = new FormData();

        // asignamos los valores
        datosAdverso.append("IdTratamiento", this.Tratamiento);
        datosAdverso.append("IdEfecto", this.Adverso);
        datosAdverso.append("Fecha", this.Fecha);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "tratamiento/grabaadverso.php",
            data: datosAdverso,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // limpiamos el formulario
                    adversospac.limpiaAdverso();

                    // recargamos la grilla
                    adversospac.nominaAdversos();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de grabar o borrar que limpia
     * el formulario de datos
     */
    limpiaAdverso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos los campos
        document.getElementById("efecto_tratamiento").value = 0;
        document.getElementById("fecha_efecto").value = "";
        document.getElementById("usuario_efecto").value = sessionStorage.getItem("Usuario");
        document.getElementById("alta_efecto").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idadverso - clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y luego de pedir confirmación ejecuta la
     * consulta de eliminación
     */
    borraAdverso(idadverso){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Efecto',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('tratamiento/borraefecto.php?id='+idefecto,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // limpiamos el formulario
                            adversospac.limpiaAdverso();

                            // recargamos la grilla
                            adversospac.nominaAdversos();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}
