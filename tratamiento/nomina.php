<?php

/**
 *
 * nomina | tratamiento/nomina.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna el array json con los datos del tratamiento
 * del paciente cuya clave recibe por get
*/

// incluimos e instanciamos las clases
require_once("tratamiento.class.php");
$tratamiento = new Tratamiento();

// obtenemos la nómina
$nomina = $tratamiento->nominaTratamiento($_GET["protocolo"]);

// retornamos el array
echo json_encode($nomina);

?>