<?php

/**
 *
 * borrar | tratamiento/borrar.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta 
 * la consulta de eliminación
*/

// incluimos e instanciamos las clases
require_once("tratamiento.class.php");
$tratamiento = new Tratamiento();

// eliminamos el registro
$tratamiento->borraTratamiento($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>