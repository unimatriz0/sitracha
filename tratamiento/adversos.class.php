<?php

/**
 *
 * Class Adversos | tratamiento/adversos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla pivot de
 * efectos adversos del tratamiento recibido por el paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Adversos {

    // declaración de variables
    protected $Id;              // clave del registro
    protected $Tratamiento;     // clave del tratamiento
    protected $Adverso;         // clave del efecto adverso
    protected $Fecha;           // fecha de ocurrencia del efecto
    protected $IdUsuario;       // clave del usuario
    protected $Link;            // puntero a la base de datos

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // instanciamos la conexión
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Id = 0;
        $this->Tratamiento = 0;
        $this->Adverso = 0;
        $this->Fecha = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setTratamiento($tratamiento){
        $this->Tratamiento = $tratamiento;
    }
    public function setAdverso($adverso){
        $this->Adverso = $adverso;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int clave del registro afectado
     * Método que ejecuta la consulta de insersión de un
     * nuevo efecto adverso, retorna la clave del registro
     */
    public function grabaAdverso(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.adversos_paciente
                            (tratamiento,
                             adverso,
                             fecha,
                             usuario)
                            VALUES
                            (:tratamiento,
                             :adverso,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :idusuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":tratamiento",  $this->Tratamiento);
        $psInsertar->bindParam(":adverso",      $this->Adverso);
        $psInsertar->bindParam(":fecha",        $this->Fecha);
        $psInsertar->bindParam(":idusuario",    $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

        // retornamos la clave
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int clave del registro
     * Método que ejecuta la consulta de eliminación de
     * un efecto adverso
     */
    public function borraAdverso($clave){

        // componemos la consulta
        $consulta = "DELETE FROM diagnostico.adversos_paciente
                     WHERE diagnostico.adversos_paciente.id = '$clave'; ";
        $this->Link->exec($consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idtratamiento clave del registro
     * Método que recibe como parámetro la clave de un
     * tratamiento y retorna el vector con los efectos
     * adversos declarados
     */
    public function nominaAdversos($idtratamiento){

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_adversostratamiento.id AS id,
                            diagnostico.v_adversostratamiento.adverso AS adverso,
                            diagnostico.v_adversostratamiento.idadverso AS idadverso,
                            diagnostico.v_adversostratamiento.fecha AS fecha,
                            diagnostico.v_adversostratamiento.usuario AS usuario,
                            diagnostico.v_adversostratamiento.alta AS alta
                     FROM diagnostico.v_adversostratamiento
                     WHERE diagnostico.v_adversostratamiento.idtratamiento = '$idtratamiento'
                     ORDER BY STR_TO_DATE(diagnostico.v_adversostratamiento.fecha, '%d/%m/%Y') DESC; ";

        // ejecutamos la consulta y retornamos el vector
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $protocolo clave del paciente
     * Método que recibe como parámetro el protocolo de un
     * paciente y retorna el vector los tratamientos recibidos
     * y los efectos adversos declarados
     */
    public function adversosTratamiento($protocolo){

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_adversostratamiento.idtratamiento AS id,
                            diagnostico.v_adversostratamiento.droga AS droga,
                            diagnostico.v_adversostratamiento.dosis AS dosis,
                            diagnostico.v_adversostratamiento.inicio_tratamiento AS inicio,
                            diagnostico.v_adversostratamiento.fin_tratamiento AS fin,
                            diagnostico.v_adversostratamiento.fecha AS fecha_adverso,
                            diagnostico.v_adversostratamiento.adverso AS adverso
                     FROM diagnostico.v_adversostratamiento
                     WHERE diagnostico.v_adversostratamiento.protocolo = '$protocolo'
                     ORDER BY STR_TO_DATE(diagnostico.v_adversostratamiento.inicio_tratamiento, '%d/%m/%Y'); ";

        // ejecutamos la consulta y retornamos el vector
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

}

?>
