<?php

/**
 *
 * Class Tratamiento | tratamiento/tratamiento.class.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * tratamientos recibidos por el paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Tratamiento {

    // definición de variables
    protected $Link;        // puntero a la base de datos
    protected $Id;                 // clave del registro
    protected $Paciente;           // clave del paciente
    protected $IdDroga;            // clave de la droga
    protected $Droga;           // nombre de la droga
    protected $Comercial;       // nombre comercial de la droga
    protected $Dosis;              // dosis en miligramos
    protected $Comprimidos;        // número de comprimidos al día
    protected $Inicio;          // fecha de inicio del tratamiento
    protected $Fin;             // fecha de finalización del tratamiento
    protected $IdUsuario;          // clave del usuario
    protected $Usuario;         // nombre del usuario
    protected $Fecha;           // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();
        $this->Id = 0;
        $this->Paciente = 0;
        $this->IdDroga = 0;
        $this->Droga = "";
        $this->Comercial = "";
        $this->Dosis = 0;
        $this->Comprimidos = 0;
        $this->Inicio = "";
        $this->Fin = "";
        $this->Usuario = "";
        $this->Fecha = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setIdDroga($iddroga){
        $this->IdDroga = $iddroga;
    }
    public function setDosis($dosis){
        $this->Dosis = $dosis;
    }
    public function setComprimidos($comprimidos){
        $this->Comprimidos = $comprimidos;
    }
    public function setInicio($inicio){
        $this->Inicio = $inicio;
    }
    public function setFin($fin){
        $this->Fin = $fin;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getIdDroga(){
        return $this->IdDroga;
    }
    public function getDroga(){
        return $this->Droga;
    }
    public function getComprimidos(){
        return $this->Comprimidos;
    }
    public function getComercial(){
        return $this->Comercial;
    }
    public function getDosis(){
        return $this->Dosis;
    }
    public function getInicio(){
        return $this->Inicio;
    }
    public function getFin(){
        return $this->Fin;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente - clave del paciente
     * @return resultset con los registros
     * Método que retorna la nómina con los tratamientos que
     * ha recibido un paciente
     */
    public function nominaTratamiento($idpaciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_tratamiento.id AS id,
                            CONCAT(diagnostico.v_tratamiento.droga, ' - ', diagnostico.v_tratamiento.dosis) AS droga,
                            diagnostico.v_tratamiento.comprimidos AS comprimidos,
                            diagnostico.v_tratamiento.inicio AS inicio,
                            diagnostico.v_tratamiento.fin AS fin,
                            diagnostico.v_tratamiento.usuario AS usuario,
                            diagnostico.v_tratamiento.fecha AS alta
                     FROM diagnostico.v_tratamiento
                     WHERE diagnostico.v_tratamiento.paciente = '$idpaciente'
                     ORDER BY STR_TO_DATE(diagnostico.v_tratamiento.inicio, '%d/%m/%Y') DESC; ";

        // ejecutamos la consulta y retornamos el vector
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtratamiento clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y asigna los valores del mismo en las
     * variables de clase
     */
    public function getDatosTratamiento($idtratamiento){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_tratamiento.id AS id,
                            diagnostico.v_tratamiento.paciente AS paciente,
                            diagnostico.v_tratamiento.iddroga AS iddroga,
                            diagnostico.v_tratamiento.droga AS droga,
                            diagnostico.v_tratamiento.dosis AS dosis,
                            diagnostico.v_tratamiento.comercial AS comercial,
                            diagnostico.v_tratamiento.comprimidos AS comprimidos,
                            diagnostico.v_tratamiento.inicio AS inicio,
                            diagnostico.v_tratamiento.fin AS fin,
                            diagnostico.v_tratamiento.usuario AS usuario,
                            diagnostico.v_tratamiento.fecha AS fecha
                     FROM diagnostico.v_tratamiento
                     WHERE diagnostico.v_tratamiento.id = '$idtratamiento'; ";
        $resultado = $this->Link->query($consulta);

        // asignamos en las variables de clase
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Paciente = $paciente;
        $this->IdDroga = $iddroga;
        $this->Droga = $droga;
        $this->Dosis = $dosis;
        $this->Comercial = $comercial;
        $this->Comprimidos = $comprimidos;
        $this->Inicio = $inicio;
        $this->Fin = $fin;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return idtratamiento clave del registro
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda, retorna la clave del registro afectado
     */
    public function grabaTratamiento(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoTratamiento();
        } else {
            $this->editaTratamiento();
        }

        // retorna la id
        return $this->Id;

    }

     /**
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * Método que ejecuta la consulta de inserción
      */
    protected function nuevoTratamiento(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.tratamiento
                            (paciente,
                             droga,
                             comprimidos,
                             inicio,
                             fin,
                             usuario)
                            VALUES
                            (:paciente,
                             :droga,
                             :comprimidos,
                             STR_TO_DATE(:inicio, '%d/%m/%Y'),
                             STR_TO_DATE(:fin, '%d/%m/%Y'),
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",    $this->Paciente);
        $psInsertar->bindParam(":droga",       $this->IdDroga);
        $psInsertar->bindParam(":comprimidos", $this->Comprimidos);
        $psInsertar->bindParam(":inicio",      $this->Inicio);
        $psInsertar->bindParam(":fin",         $this->Fin);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaTratamiento(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.tratamiento SET
                            droga = :droga,
                            comprimidos = :comprimidos,
                            inicio = STR_TO_DATE(:inicio, '%d/%m/%Y'),
                            fin = STR_TO_DATE(:fin, '%d/%m/%Y'),
                            usuario = :usuario
                     WHERE diagnostico.tratamiento.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":droga",       $this->IdDroga);
        $psInsertar->bindParam(":comprimidos", $this->Comprimidos);
        $psInsertar->bindParam(":inicio",      $this->Inicio);
        $psInsertar->bindParam(":fin",         $this->Fin);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtratamiento clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public function borraTratamiento($idtratamiento){

        // compone y ejecuta la consulta
        $consulta = "DELETE FROM diagnostico.tratamiento
                     WHERE diagnostico.tratamiento.id = '$idtratamiento'; ";
        $this->Link->exec($consulta);

    }

}