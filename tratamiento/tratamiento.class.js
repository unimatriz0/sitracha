/*
 * Nombre: tratamiento.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 13/06/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de la tabla de
 *              tratamiento recibido por el paciente
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de los tratamientos
 * recibidos por el paciente
 */
class Tratamiento {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initTratamiento();

        // declaramos el layer
        this.layerTratamiento = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initTratamiento(){

        // inicializamos las variables
        this.Id = 0;                     // clave del registro
        this.Paciente = 0;               // clave del paciente (protocolo)
        this.IdDroga = 0;                // clave de la droga
        this.Comprimidos = 0;            // número de comprimidos diarios
        this.Inicio = "";                // fecha de inicio del tratamiento
        this.Fin = "";                   // fecha de finalización del tratamiento
        this.Usuario = "";               // usuario que ingresó el registro
        this.Fecha = "";                 // fecha de alta del registro

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta en el layer emergente la nómina
     * de tratamientos recibidos por el paciente
     */
    verTratamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // verifica si hay un registro activo
        if (document.getElementById("protocolo_paciente").value == ""){

            // presenta el mensaje
            var mensaje = "Debe grabar el registro primero\n";
            mensaje += "para generar el protocolo.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // de otra forma cargamos el layer
        this.layerTratamiento = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Tratamientos Recibidos',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    width: 730,
                    responsiveHeight: true,
                    position: {x:100, y:200},
                    onCloseComplete: function(){
                        tratamiento.Cerrar();
                    },
                    ajax: {
                        url: 'tratamiento/form_tratamiento.html',
                        reload: 'strict'
                    }
            });

        // mostramos el formulario
        this.layerTratamiento.open();

        // mostramos el formulario de efectos adversos
        adversospac.muestraAdversos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el div la grilla con los
     * tratamientos del paciente (lo hacemos así para
     * no recargar el layer)
     */
    cargaTratamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos el protocolo
        var protocolo = document.getElementById("protocolo_paciente").value;

        // llama la rutina php
        $.get('tratamiento/nomina.php?protocolo='+protocolo,
            function(data){

                // llamamos la rutina pasándole el vector
                tratamiento.cargaGrilla(data);

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} vector con los datos del tratamiento
     * Método que recibe el vector con los tratamientos del
     * paciente y carga la grilla con los datos
     */
    cargaGrilla(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos las variables
        var texto = "";

        // limpia el cuerpo de la tabla
        $("#cuerpo_tratamiento").html('');

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // componemos la fila
            texto = "<tr>";

            // abrimos la primer columna
            texto += "<td>";
            texto += datos[i].droga;
            texto += "</td>";

            // agregamos los comprimidos
            texto += "<td>";
            texto += datos[i].comprimidos;
            texto += "</td>";

            // agrega la fecha de inicio
            texto += "<td>";
            texto += datos[i].inicio;
            texto += "</td>";

            // agrega la fecha de finalización
            texto += "<td>";
            texto += datos[i].fin;
            texto += "</td>";

            // agrega el usuario
            texto += "<td>";
            texto += datos[i].usuario;
            texto += "</td>";

            // agrega la fecha de alta
            texto += "<td>";
            texto += datos[i].alta;
            texto += "</td>";

            // agrega el botón eliminar
            texto += "<td>";
            texto += "<input type='button' ";
            texto += "       name='btnEliminaTratamiento' ";
            texto += "       id='btnEliminaTratamiento' ";
            texto += "       title='Pulse para eliminar el registro' ";
            texto += "       class='botonborrar' ";
            texto += "       onClick='tratamiento.borraTratamiento(" + datos[i].id + ")'>";
            texto += "</td>";

            // agrega el botón efectos secundarios
            texto += "<td>";
            texto += "<input type='button' ";
            texto += "       name='btnEfectosAdversos' ";
            texto += "       id='btnEfectosAdversos' ";
            texto += "       title='Efectos adversos' ";
            texto += "       class='botonadverso' ";
            texto += "       onClick='adversospac.nominaAdversos(" + datos[i].id + ")'>";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos la fila a la tabla
            $("#cuerpo_tratamiento").append(texto);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que valida los datos del tratamiento antes
     * de enviarlos al servidor
     */
    validaTratamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no seleccionó la droga
        if(document.getElementById("droga_tratamiento").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la droga utilizada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("droga_tratamiento").focus();
            return false;

        }

        // si no indicó el número de comprimidos
        if (document.getElementById("comprimidos_tratamiento").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Indique el número de comprimidos\n";
            mensaje += "diarios administrados.";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("comprimidos_tratamiento").focus();
            return false;

        }

        // si no indicó la fecha de inicio
        if (document.getElementById("inicio_tratamiento").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de inicio del tratamiento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("inicio_tratamiento").focus();
            return false;

        }

        // el fin del tratamiento lo permite en blanco

        // asignamos en las variables de clase
        this.Paciente = document.getElementById("protocolo_paciente").value;
        this.IdDroga = document.getElementById("droga_tratamiento").value;
        this.Comprimidos = document.getElementById("comprimidos_tratamiento").value;
        this.Inicio = document.getElementById("inicio_tratamiento").value;
        this.Fin = document.getElementById("fin_tratamiento").value;

        // grabamos el registro
        this.grabaTratamiento();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * ejecuta la consulta en el servidor
     */
    grabaTratamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosTratamiento = new FormData();

        // asignamos las variables
        datosTratamiento.append("Paciente", this.Paciente);
        datosTratamiento.append("Droga", this.IdDroga);
        datosTratamiento.append("Comprimidos", this.Comprimidos);
        datosTratamiento.append("Inicio", this.Inicio);
        datosTratamiento.append("Fin", this.Fin);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "tratamiento/grabar.php",
            data: datosTratamiento,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // limpiamos el formulario
                    tratamiento.limpiaTratamiento();

                    // recargamos la grilla
                    tratamiento.cargaTratamiento();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // inicializamos las variables por las dudas
                    tratamiento.initTratamiento();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de datos luego de
     * grabar el registro
     */
    limpiaTratamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializamos el formulario
        document.getElementById("droga_tratamiento").value = 0;
        document.getElementById("comprimidos_tratamiento").value = 0;
        document.getElementById("inicio_tratamiento").value = "";
        document.getElementById("fin_tratamiento").value = "";
        document.getElementById("usuario_tratamiento").value = sessionStorage.getItem("Usuario");
        document.getElementById("alta_tratamiento").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idtratamiento clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y luego de pedir confirmación ejecuta
     * la consulta de eliminación
     */
    borraTratamiento(idtratamiento){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Tratamiento',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('tratamiento/borrar.php?id='+idtratamiento,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // inicializamos las variables
                            tratamiento.initTratamiento();

                            // inicializamos el formulario
                            tratamiento.limpiaTratamiento();

                            // recargamos la grilla
                            tratamiento.cargaTratamiento();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cerrar el layer de tratamiento que
     * se encarga de destruirlo y destruir también el layer
     * de efectos adversos
     */
    Cerrar(){

        // destruimos los layers
        this.layerTratamiento.destroy();
        adversospac.layerAdversos.destroy();

    }

}