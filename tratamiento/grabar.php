<?php

/**
 *
 * grabar | tratamiento/grabar.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta 
 * la consulta de grabación
*/

// incluimos e instanciamos las clases
require_once("tratamiento.class.php");
$tratamiento = new Tratamiento();

// asignamos los valores en la clase
$tratamiento->setPaciente($_POST["Paciente"]);
$tratamiento->setIdDroga($_POST["Droga"]);
$tratamiento->setComprimidos($_POST["Comprimidos"]);
$tratamiento->setInicio($_POST["Inicio"]);
$tratamiento->setFin($_POST["Fin"]);

// grabamos el registro 
$id = $tratamiento->grabaTratamiento();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>