<?php

/**
 *
 * grabaadverso | tratamiento/grabaadverso.php
 *
 * @package     Diagnostico
 * @subpackage  Tratamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta de grabación
*/

// incluimos e instanciamos las clases
require_once("adversos.class.php");
$adverso = new Adversos();

// asignamos los valores en la clase
$adverso->setTratamiento($_POST["IdTratamiento"]);
$adverso->setAdverso($_POST["IdEfecto"]);
$adverso->setFecha($_POST["Fecha"]);

// grabamos el registro
$id = $adverso->grabaAdverso();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>