<?php

/**
 *
 * stock/inventario/verificacritico.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que verifica si existen artículos por debajo del valor crítico
 * definido por el usuario (para el laboratorio activo), retorna la
 * cantida de artículos que se encuentran por debajo
 *
*/

// inclusión de clases
require_once("inventario.class.php");
$critico = new Inventario();

// lo validamos
$resultado = $critico->verificaCritico();

// retornamos el estado de la operación
echo json_encode(array("Registros" => $resultado));

?>