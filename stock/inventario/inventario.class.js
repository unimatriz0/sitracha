/*
    Nombre: inventario.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 15/12/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de inventario

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las entradas y
 * salidas del depósito
 */
class Inventario {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // el layer de los cuadros emergentes
        this.layerEmergente = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente carga en el contenido de la página la grilla con el
     * inventario existente
     */
    Inventario() {

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el documento
        $("#form_stock").load("stock/inventario/inventario.php");

    }

    /**
     * Método que verifica si existen artículos en cantidad crítica
     * y en todo caso presenta la alerta
     */
    verificaCritico(){

        // llama la rutina php
        $.ajax({
            url: "stock/inventario/verificacritico.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {

                // si hay elementos en estado crítico
                if (data.Registros != 0){

                    // presenta el alerta
                    var texto = "<p align='justify'>";
                    texto += "Existen " + data.Registros + " artículos ";
                    texto += "que se encuentran por debajo de la cantidad ";
                    texto += "definida  como crítica.</p>";
                    texto += "<p align='justify'>";
                    texto += "Por favor verifique</p>";

                    // abrimos el layer
                    var formAlerta = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: true,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    repositionOnContent: true,
                                    overlay: false,
                                    title: 'Inventario',
                                    width: 300,
                                    draggable: 'title',
                                    theme: 'TooltipBorder',
                                    content: texto
                       });
                    formAlerta.open();

                }

            }
        });

    }

}