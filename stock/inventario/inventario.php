<?php

/**
 *
 * stock/inventario/inventario.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que obtiene la nómina de artículos existentes y los
 * presenta en una tabla
 *
*/

// incluimos e instanciamos la clase
require_once("inventario.class.php");
$inventario = new Inventario();

// obtenemos la matriz existente
$inventario = $inventario->getInventario();

// definimos el título
echo "<h2 align='center'>";
echo "Elementos existentes en depósito";
echo "</h2>";

// define la tabla
echo "<table width='80%' align='center' border='0' id='tinventario'>";

// define los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Descripción</th>";
echo "<th align='left'>Crítico</th>";
echo "<th align='left'>Existentes</th>";
echo "<th align='left'>Valor</th>";
echo "</tr>";
echo "</thead>";

// define el cuerpo de la tabla
echo "<tbody>";

// recorremos la matriz
foreach($inventario AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo presentamos
    echo "<tr>";
    echo "<td>$descripcion</td>";
    echo "<td>$critico</td>";
    echo "<td>$existencia</td>";
    echo "<td>" . number_format($valor, 2, ",", ".") . "</td>";

}

// cerramos la tabla
echo "</tbody></table>";

// definimos el paginador
echo "<div class='paging'></div>";

?>
<SCRIPT>

    // propiedades de la tabla
    $('#tinventario').datatable({
        pageSize: 15,
        sort:    [true, true, true, true],
        filters: [true, false, false, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>
