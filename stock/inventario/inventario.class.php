<?php

/**
 *
 * Class Inventario | stock/inventario.class.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones sobre las distintas tablas
 * que hacen al sistema de control de stock
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Inventario {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdUsuario;             // clave del usuario activo

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesión
        session_start();

        // si existen los valores de sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y el laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];

        // si no inició
        } else {

            // abandona por error
            echo "No hay sesión iniciada";
            exit;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que consulta la vista de inventario y retorna un array
     * asociativo con todos los elementos en depósito para el laboratorio
     * del usuario activo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $inventario
     */
    public function getInventario(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_inventario.iditem AS iditem,
                            diagnostico.v_inventario.descripcion AS descripcion,
                            diagnostico.v_inventario.codigosop AS sop,
                            diagnostico.v_inventario.critico AS critico,
                            diagnostico.v_inventario.existencia AS existencia,
                            diagnostico.v_inventario.importe AS valor
                     FROM diagnostico.v_inventario
                     WHERE diagnostico.v_inventario.idlaboratorio = '$this->IdLaboratorio'
                     ORDER BY diagnostico.v_inventario.descripcion; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $inventario = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $inventario;

    }

    /**
     * Método que retorna el número de artículos que se encuentran
     * por debajo del valor crítico para el laboratorio activo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function verificaCritico(){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.v_inventario.iditem) AS registros
                     FROM diagnostico.v_inventario
                     WHERE diagnostico.v_inventario.critico < diagnostico.v_inventario.existencia AND
                           diagnostico.v_inventario.critico != 0 AND 
                           idlaboratorio = '$this->IdLaboratorio'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos
        extract($fila);

        // retornamos los artículos críticos
        return $registros;

    }

    /**
     * Método que retorna un vector con la nómina de artículos con volumen 
     * crítico
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function getCritico(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_inventario.iditem AS iditem,
                            diagnostico.v_inventario.descripcion AS descripcion,
                            diagnostico.v_inventario.codigosop AS codigosop,
                            diagnostico.v_inventario.critico AS critico,
                            diagnostico.v_inventario.existencia AS existencia
                     FROM diagnostico.v_inventario
                     WHERE diagnostico.v_inventario.critico < diagnostico.v_inventario.existencia AND
                           diagnostico.v_inventario.critico != 0 AND 
                           diagnostico.v_inventario.idlaboratorio = '$this->IdLaboratorio'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el vector y retornamos
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

}