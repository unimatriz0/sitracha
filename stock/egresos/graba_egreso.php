<?php

/**
 *
 * stock/egresos/graba_egreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro de egreso
 * y ejecuta la consulta de actualización, retorna la clave del
 * registro afectado y el número de remito generado
 *
*/

// incluimos e instanciamos las clases
require_once("egresos.class.php");
$egresos = new Egresos();

// si está editando
if (!empty($_POST["Id"])){
    $egreso->setIdEgreso($_POST["Id"]);
}

// asignamos el resto de los valores
$egresos->setIdItemEgreso($_POST["IdItem"]);
$egresos->setCantidadEgreso($_POST["Cantidad"]);
$egresos->setRecibio($_POST["IdRecibio"]);
$egresos->setComentariosEgreso($_POST["Comentarios"]);

// ejecutamos la consulta
$idegreso = $egresos->grabaEgreso();

// obtenemos el remito
$remito = $egresos->getRemitoEgreso();

// retornamos el resultado
echo json_encode(array("Id" => $idegreso,
                       "Remito" => $remito));

?>