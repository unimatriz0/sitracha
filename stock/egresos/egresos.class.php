<?php

/**
 *
 * Class Egresos | stock/egresos/egresos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones sobre las tabla de
 * egresos de artículos del depósito
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Egresos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdDepartamento;        // clave del departamento del usuario
    protected $IdUsuario;             // clave del usuario activo

    // las variables de la tabla de egresos
    protected $IdItemEgreso;          // clave del artículo
    protected $DescripcionEgreso;     // descripción del artículo
    protected $ImagenEgreso;          // imagen del artículo
    protected $IdEgreso;              // clave del egreso
    protected $RemitoEgreso;          // número de remito
    protected $CantidadEgreso;        // cantidad entregada
    protected $FechaEgreso;           // fecha de salida
    protected $EntregoEgreso;         // usuario que entregó el artículo
    protected $IdEntregoEgreso;       // clave del usuario que entregó
    protected $RecibioEgreso;         // usuario que recibió
    protected $IdRecibioEgreso;       // clave del usuario que recibió
    protected $ComentariosEgreso;     // comentarios y observaciones

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // las variables de la tabla de egresos
        $this->IdItemEgreso = 0;
        $this->DescripcionEgreso = "";
        $this->ImagenEgreso = "";
        $this->IdEgreso = "";
        $this->RemitoEgreso = 0;
        $this->CantidadEgreso = 0;
        $this->FechaEgreso = "";
        $this->EntregoEgreso = "";
        $this->IdEntregoEgreso = 0;
        $this->RecibioEgreso = "";
        $this->IdRecibioEgreso = 0;
        $this->ComentariosEgreso = "";

        // iniciamos sesión
        session_start();

        // si existen los valores de sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y el laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];
            $this->IdDepartamento = $_SESSION["IdDepartamento"];

        // si no inició
        } else {

            // abandona por error
            echo "No hay sesión iniciada";
            exit;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdItemEgreso($iditem){

        // verificamos que sea un número
        if (!is_numeric($iditem)){

            // abandona por error
            echo "La clave del item egresado debe ser un número";
            exit;

        // si es correcto
        } else {

            // asigna
            $this->IdItemEgreso = $iditem;

        }

    }
    public function setDescripcionEgreso($descripcion){
        $this->DescripcionEgreso = $descripcion;
    }
    public function setIdEgreso($idegreso){

        // verifica que sea un número
        if (!is_numeric($idegreso)){

            // abandona por error
            echo "La clave del egreso debe ser un número";
            exit;

        // si es correcto
        } else {

            // asigna
            $this->IdEgreso = $idegreso;

        }

    }
    public function setRemitoEgreso($remito){

        // verifica que sea un número
        if (!is_numeric($remito)){

            // abandona por error
            echo "El Remito del egreso debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->RemitoEgreso = $remito;

        }

    }
    public function setCantidadEgreso($cantidad){

        // verifica que sea un número
        if (!is_numeric($cantidad)){

            // abandona por error
            echo "La cantidad egresada debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->CantidadEgreso = $cantidad;

        }

    }
    public function setRecibio($idrecibio){

        // verifica que sea un número
        if (!is_numeric($idrecibio)){

            // abandona por error
            echo "La clave del usuario que recibe debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->IdRecibioEgreso = $idrecibio;

        }

    }
    public function setComentariosEgreso($comentarios){
        $this->ComentariosEgreso = $comentarios;
    }

    // métodos de retorno de valores
    public function getIdItemEgreso(){
        return $this->IdItemEgreso;
    }
    public function getDescripcionEgreso(){
        return $this->DescripcionEgreso;
    }
    public function getImagenEgreso(){
        return $this->ImagenEgreso;
    }
    public function getIdEgreso(){
        return $this->IdEgreso;
    }
    public function getRemitoEgreso(){
        return $this->RemitoEgreso;
    }
    public function getCantidadEgreso(){
        return $this->CantidadEgreso;
    }
    public function getFechaEgreso(){
        return $this->FechaEgreso;
    }
    public function getEntrego(){
        return $this->EntregoEgreso;
    }
    public function getRecibio(){
        return $this->RecibioEgreso;
    }
    public function getIdRecibio(){
        return $this->IdRecibioEgreso;
    }
    public function getComentariosEgreso(){
        return $this->ComentariosEgreso;
    }

    /**
     * Método que retorna la nómina de egresos del depósito en
     * forma de un array asociativo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function getEgresos(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_egresos.idegreso AS idegreso,
                            diagnostico.v_egresos.descripcion AS descripcion,
                            diagnostico.v_egresos.remito AS remito,
                            diagnostico.v_egresos.cantidad AS cantidad,
                            diagnostico.v_egresos.fecha_egreso AS fecha_egreso,
                            diagnostico.v_egresos.entrego AS entrego,
                            diagnostico.v_egresos.recibio AS recibio
                     FROM diagnostico.v_egresos
                     WHERE diagnostico.v_egresos.idlaboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.v_egresos.id_departamento = '$this->IdDepartamento'
                     ORDER BY diagnostico.v_egresos.descripcion; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $egresos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $egresos;

    }

    /**
     * Método que retorna la nómina de egresos del depósito en
     * las fechas que recibe como parámetro 
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $fechainicio - fecha inicial 
     * @param $fechafin - fecha final
     * @return array
     */
    public function getEgresosFecha($fechainicio, $fechafin){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_egresos.idegreso AS idegreso,
                            diagnostico.v_egresos.descripcion AS descripcion,
                            diagnostico.v_egresos.codigosop AS sop,
                            diagnostico.v_egresos.remito AS remito,
                            diagnostico.v_egresos.cantidad AS cantidad,
                            diagnostico.v_egresos.fecha_egreso AS fecha_egreso,
                            diagnostico.v_egresos.entrego AS entrego,
                            diagnostico.v_egresos.recibio AS recibio
                     FROM diagnostico.v_egresos
                     WHERE diagnostico.v_egresos.idlaboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.v_egresos.id_departamento = '$this->IdDepartamento' AND 
                           STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') >= STR_TO_DATE('$fechainicio', '%d/%m/%Y') AND 
                           STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') <= STR_TO_DATE('$fechafin', '%d/%m/%Y')
                     ORDER BY diagnostico.v_egresos.descripcion; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $egresos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $egresos;

    }

    /**
     * Método que recibe como parámetros la fecha inicial y la fecha 
     * final y arroja el total entregado agrupado por artículo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $fechainicio - fecha inicial a reportar
     * @param string $fechafinal - fecha final a reportar
     * @return array
     */
    public function conciliadoEgresos($fechainicio, $fechafin){

        $consulta = "SELECT diagnostico.v_egresos.descripcion AS descripcion,
                            diagnostico.v_egresos.codigosop AS sop,
                            SUM(diagnostico.v_egresos.cantidad) AS cantidad
                     FROM diagnostico.v_egresos
                     WHERE diagnostico.v_egresos.idlaboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.v_egresos.id_departamento = '$this->IdDepartamento' AND 
                           STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') >= STR_TO_DATE('$fechainicio', '%d/%m/%Y') AND 
                           STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') <= STR_TO_DATE('$fechafin', '%d/%m/%Y')
                     GROUP BY diagnostico.v_egresos.iditem
                     ORDER BY diagnostico.v_egresos.descripcion; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $egresos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $egresos;        

    }

    /**
     * Método que recibe como parámetro la clave de un egreso y asigna
     * en las variables de clase los datos del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idegreso - clave del registro de egreso
     */
    public function getDatosEgreso($idegreso){

        // inicializamos la variable
        $iditem = 0;
        $descripcion = "";
        $codigosop = "";
        $imagen = "";
        $remito = "";
        $cantidad = 0;
        $fecha_egreso = "";
        $entrego = "";
        $recibio = "";
        $idrecibio = 0;
        $comentarios = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_egresos.idegreso AS idegreso,
                            diagnostico.v_egresos.iditem AS iditem,
                            diagnostico.v_egresos.descripcion AS descripcion,
                            diagnostico.v_egresos.codigosop AS codigosop,
                            diagnostico.v_egresos.imagen AS imagen,
                            diagnostico.v_egresos.remito AS remito,
                            diagnostico.v_egresos.cantidad AS cantidad,
                            diagnostico.v_egresos.fecha_egreso AS fecha_egreso,
                            diagnostico.v_egresos.entrego AS entrego,
                            diagnostico.v_egresos.recibio AS recibio,
                            diagnostico.v_egresos.idrecibio AS idrecibio,
                            diagnostico.v_egresos.comentarios AS comentarios
                     FROM diagnostico.v_egresos
                     WHERE diagnostico.v_egresos.idegreso = '$idegreso';";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $egreso = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y lo asignamos
        extract($egreso);
        $this->IdItemEgreso = $iditem;
        $this->DescripcionEgreso = $descripcion;
        $this->Sop = $codigosop;
        $this->ImagenEgreso = $imagen;
        $this->IdEgreso = $idegreso;
        $this->RemitoEgreso = $remito;
        $this->CantidadEgreso = $cantidad;
        $this->FechaEgreso = $fecha_egreso;
        $this->EntregoEgreso = $entrego;
        $this->RecibioEgreso = $recibio;
        $this->IdRecibioEgreso = $idrecibio;
        $this->ComentariosEgreso = $comentarios;

    }

    /**
     * Método que recibe como parámetro la clave de un egreso, ejecuta la
     * consulta de eliminación en la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idegreso - clave del registro a eliminar
     */
    public function borraEgreso($idegreso){

        // componemos la consulta y la ejecutamos
        $consulta = "DELETE FROM diagnostico.egresos
                     WHERE diagnostico.egresos.id = '$idegreso';";
        $this->Link->exec($consulta);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / eliminado
     * Método público que llama la consulta de inserción o edición
     * según corresponda, retorna la clave del registro afectado
     */
    public function grabaEgreso(){

        // si está insertando
        if ($this->IdEgreso == 0){

            // insertamos
            $this->insertaEgreso();

        // si está editando
        } else {

            // llamamos la edición
            $this->editaEgreso();

        }

        // retornamos la clave del registro
        return $this->IdEgreso;

    }

    /**
     * Método protegido que genera la consulta de inserción y asigna en la
     * variable de clase el valor del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function insertaEgreso(){

        // obtenemos el nuevo remito
        $this->nuevoRemito();

        // componemos la consulta de inserción
        $consulta = "INSERT INTO diagnostico.egresos
                            (item,
                             id_laboratorio,
                             departamento,
                             remito,
                             cantidad,
                             id_entrego,
                             id_recibio,
                             comentarios)
                            VALUES
                            (:item,
                             :id_laboratorio,
                             :departamento,
                             :remito,
                             :cantidad,
                             :id_entrego,
                             :id_recibio,
                             :comentarios);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":item", $this->IdItemEgreso);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":departamento", $this->IdDepartamento);
        $psInsertar->bindParam(":remito", $this->RemitoEgreso);
        $psInsertar->bindParam(":cantidad", $this->CantidadEgreso);
        $psInsertar->bindParam(":id_entrego", $this->IdUsuario);
        $psInsertar->bindParam(":id_recibio", $this->IdRecibioEgreso);
        $psInsertar->bindParam(":comentarios", $this->IdComentariosEgreso);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtenemos la id del registro
        $this->IdEgreso = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que genera la consulta de edición del registro de
     * egresos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaEgreso(){

        // componemos la consulta de edición
        $consulta = "UPDATE diagnostico.egresos SET
                            item = :item,
                            remito = :remito,
                            cantidad = :cantidad,
                            id_entrego = :id_entrego,
                            id_recibio = :id_recibio,
                            comentarios = :comentarios
                     WHERE diagnostico.egresos.id = :idegreso;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":item", $this->IdItemEgreso);
        $psInsertar->bindParam(":remito", $this->RemitoEgreso);
        $psInsertar->bindParam(":cantidad", $this->CantidadEgreso);
        $psInsertar->bindParam(":id_entrego", $this->IdUsuario);
        $psInsertar->bindParam(":id_recibio", $this->IdRecibioEgreso);
        $psInsertar->bindParam(":comentarios", $this->IdComentariosEgreso);
        $psInsertar->bindParam(":idegreso", $this->IdEgreso);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método protegido que obtiene el valor del nuevo remito de
     * salida para el mismo laboratorio
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoRemito(){

        // inicializamos las variables
        $remito = "";

        // obtenemos el valor del nuevo remito
        $consulta = "SELECT MAX(diagnostico.egresos.remito) + 1 AS remito
                     FROM diagnostico.egresos
                     WHERE diagnostico.egresos.id_laboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.egresos.departamento = '$this->IdDepartamento';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos
        extract($fila);

        // si es el primer remito
        if ($remito == ""){
            $this->RemitoEgreso = 1;
        } else {
            $this->RemitoEgreso = $remito;
        }

    }

}