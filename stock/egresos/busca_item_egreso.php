<?php

/**
 *
 * stock/egresos/busca_item_egreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un artículo y retorna
 * por json la clave sop y la imagen del mismo
 *
*/

// incluimos e instanciamos las clases
require_once ("../articulos/items.class.php");
$items = new Items();

// obtenemos la imagen
$items->getDatosItem($_GET["iditem"]);
$imagen = $items->getImagen();
$sop = $items->getSop();

// retornamos la imagen
echo json_encode(array("Sop" => $sop,
                       "Imagen" => $imagen));

?>