<?php

/**
 *
 * stock/egresos/getegreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro de egreso
 * y retorna un array son con los datos del mismo
 *
*/

// incluimos e instanciamos las clases
require_once("egresos.class.php");
$egreso = new Egresos();

// obtenemos los datos del registro
$egreso->getDatosEgreso($_GET["idegreso"]);

// retornamos los valores
echo json_encode(array("IdEgreso" =>      $egreso->getIdEgreso(),
                       "IdItem" =>        $egreso->getIdItemEgreso(),
                       "Descripcion" =>   $egreso->getDescripcionEgreso(),
                       "Sop" =>           $egreso->getSop(),
                       "Imagen" =>        $egreso->getImagenEgreso(),
                       "Remito" =>        $egreso->getRemitoEgreso(),
                       "Cantidad" =>      $egreso->getCantidadEgreso(),
                       "Fecha" =>         $egreso->getFechaEgreso(),
                       "Usuario" =>       $egreso->getEntrego(),
                       "IdRecibio" =>     $egreso->getIdRecibio(),
                       "Observaciones" => $egreso->getComentariosEgreso()));

?>