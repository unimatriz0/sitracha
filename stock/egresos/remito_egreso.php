<?php

/**
 *
 * stock/egresos/remito_egreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un remito, instancia
 * la clase y obtiene el contenido del remito, el que presenta
 * en un layer emergente
 *
*/

// incluimos e instanciamos las clases
require_once("remitos_egreso.class.php");
$remito = new Remitos();

// generamos el remito
$remito->generaRemito($_GET["idegreso"]);

// enviamos los encabezados
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0, false");
header("Pragma: no-cache");
?>

<!-- cargamos el documento -->
<object data="temp/remito_egreso.pdf"
        type="application/pdf"
        width="850" height="500">
</object>