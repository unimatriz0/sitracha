<?php

/**
 *
 * Class Remitos | stock/remitos_egresos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// define la ruta a las fuentes pdf (lo llamamos desde
// el script de remitos y el path queda en stock)
define('FPDF_FONTPATH', $_SERVER['DOCUMENT_ROOT'] . '/clases/fpdf/font');

// la clase pdf que ya extiende la tFPDF
require_once ($_SERVER['DOCUMENT_ROOT'] . "/clases/fpdf/code39/code39.php");

// la clase qrcode
require_once($_SERVER['DOCUMENT_ROOT'] . '/clases/fpdf/qrcode/qrcode.class.php');

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula
// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que genera el PDF con el remito del egreso del
 * Depósito
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Remitos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Descripcion;           // descripcion del item
    protected $CodigoSop;             // código sop del item
    protected $Imagen;                // imagen del item
    protected $IdEgreso;              // clave del egreso
    protected $Laboratorio;           // nombre del laboratorio
    protected $IdLaboratorio;         // clave del laboratorio
    protected $Logo;                  // logo del laboratorio
    protected $Remito;                // nùmero del remito
    protected $Cantidad;              // cantidad ingresada
    protected $FechaEgreso;           // fecha en que se entregó
    protected $Usuario;               // usuario que entregó
    protected $Recibio;               // usuario que recibió
    protected $Comentarios;           // comentarios del ingreso
    protected $Documento;             // objeto de la clase pdf

    /**
     * Constructor de la clase, se conecta a la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct() {

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Descripcion = "";
        $this->CodigoSop = "";
        $this->Imagen = "";
        $this->IdEgreso = 0;
        $this->Laboratorio = "";
        $this->IdLaboratorio = 0;
        $this->Logo = "";
        $this->Remito = "";
        $this->Cantidad = 0;
        $this->FechaEgreso = "";
        $this->Usuario = "";
        $this->Recibio = "";
        $this->Comentarios = "";
        $this->Documento = "";

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct() {

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método público que recibe como parámetro la clave del ingreso
     * genera y guarda el documento pdf en el directorio temporal
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idegreso
     */
    public function generaRemito($idegreso){

        // asignamos en la variable de clase
        $this->IdEgreso = $idegreso;

        // obtenemos los datos del registro
        $this->getDatosRemito();

        // obtenemos los datos de la institución
        $this->getDatosInstitucion();

        // instanciamos la clase pdf
        $this->Documento = new PDF_Code39();

        // establecemos las propiedades del documento
        $this->Documento->SetAuthor("Claudio Invernizzi");
        $this->Documento->SetCreator("INP - Mario Fatala Chaben");
        $this->Documento->SetSubject("Entrega de Materiales", true);
        $this->Documento->SetTitle("Entrega de Materiales", true);
        $this->Documento->SetAutoPageBreak(true, 10);

        // agrega una fuente unicode
        $this->Documento->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->Documento->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // fijamos el margen izquierdo y derecho
        $this->Documento->SetLeftMargin(15);
        $this->Documento->setRightMargin(15);

        // agregamos la página
        $this->Documento->AddPage("P", "A4");

        // generamos el documento
        $this->imprimirRemito();

        // lo cerramos y asignamos en una variable
        $this->Documento->Output("../../temp/remito_egreso.pdf", 'F');

    }

    /**
     * Método protegido que obtiene los datos del remito y los
     * asigna a las variables de clase
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getDatosRemito(){

        // inicializamos las variables
        $descripcion = "";
        $codigosop = 0;
        $imagen = "";
        $idlaboratorio = 0;
        $remito = "";
        $cantidad = 0;
        $fecha_egreso = "";
        $usuario = "";
        $recibio = "";
        $comentarios = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_egresos.descripcion AS descripcion,
                            diagnostico.v_egresos.codigosop AS codigosop,
                            diagnostico.v_egresos.imagen AS imagen,
                            diagnostico.v_egresos.idlaboratorio AS idlaboratorio,
                            diagnostico.v_egresos.remito AS remito,
                            diagnostico.v_egresos.cantidad AS cantidad,
                            diagnostico.v_egresos.fecha_egreso AS fecha_egreso,
                            diagnostico.v_egresos.nombre_entrego AS usuario,
                            diagnostico.v_egresos.nombre_recibio AS recibio,
                            diagnostico.v_egresos.comentarios AS comentarios
                     FROM diagnostico.v_egresos
                     WHERE diagnostico.v_egresos.idegreso = '$this->IdEgreso';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $item = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro
        extract($item);

        // asignamos en las variables de clase, decodificamos la imagen
        // porque la guardamos como una cadena base 64
        $this->Descripcion = $descripcion;
        $this->CodigoSop = $codigosop;
        $this->Imagen = $imagen;
        $this->IdLaboratorio = $idlaboratorio;
        $this->Remito = $remito;
        $this->Cantidad = $cantidad;
        $this->FechaEgreso = $fecha_egreso;
        $this->Usuario = $usuario;
        $this->Recibio = $recibio;
        $this->Comentarios = $comentarios;

    }

    /**
     * Método público que obtiene los datos de la institución
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getDatosInstitucion(){

        // inicializamos las variables
        $laboratorio = "";
        $logo = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_laboratorios.laboratorio AS laboratorio,
                            diagnostico.v_laboratorios.logo AS logo
                     FROM diagnostico.v_laboratorios
                     WHERE diagnostico.v_laboratorios.id = '$this->IdLaboratorio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $item = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro
        extract($item);

        // asignamos en las variables de clase (la imagen la tenemos
        // que decodificar porque la tenemos como una cadena base64
        $this->Laboratorio = $laboratorio;
        $this->Logo = $logo;

    }

    /**
     * Método que a partir de las variables de clase
     * genera el remito
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function imprimirRemito(){

        // iniciamos un bucle para imprimir original y copia
        for ($i = 0; $i < 2; $i++){

            // imprimimos el encabezado
            $this->imprimirEncabezado();

            // imprimimos el detalle
            $this->imprimirDetalle();

            // fijamos la posición de impresión
            $this->Documento->setX(100);

            // si está en la primera parte
            if ($i == 0){

                // imprimimos como original y punteamos
                $this->Documento->Cell(45, 10, "Original", 0, 1, "L");
                $this->Documento->Cell(0, 10, str_repeat("- ", 80), 0, 0, "L");

                // agregamos el ícono de tijeras
                $this->Documento->Image("../../imagenes/tijeras.jpg", $this->Documento->GetX() - 20, $this->Documento->GetY() + 3, 5);

            // imprime la leyenda
            } else {

                // duplicado solamente
                $this->Documento->Cell(45, 10, "Duplicado", 0, 1, "L");

            }

        }

    }

    /**
     * Método protegido que imprime los datos del encabezado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function imprimirEncabezado(){

       // si estamos imprimiendo el original
        if ($this->Documento->GetY() < 100){

            // definimos las coordenadas
            $this->Documento->SetXY(20, 15);

        // si estamos imprimiendo el duplicado
        } else {

            // definimos las coordenadas
            $this->Documento->SetXY(20, 155);

        }

        // definimos la fuente
        $this->Documento->setFont("DejaVu", "B", 12);

        // presentamos el nombre de la institución
        $this->Documento->MultiCell(100, 8, $this->Laboratorio, 0, "C");

        // según si estamos imprimiendo el original o el duplicado
        if ($this->Documento->GetY() < 100){

            // si existe la imagen
            if (!is_null($this->Logo)){

                // determinamos el tipo de imagen
                $tipo = $this->tipoImagen($this->Logo);

                // agregamos la imagen indicándole el tamaño y la extensión
                $this->Documento->Image($this->Logo, 150, 5, 40, 40, $tipo);

            }

            // presenta el código de barras
            $this->Documento->Code39(140, 50, "E-$this->IdEgreso-$this->IdLaboratorio", 1, 10);

            // obtenemos el código qr
            $qrcode = new QRcode("E-$this->IdEgreso-$this->IdLaboratorio", 'H');

            // lo adjuntamos al documento
            $qrcode->displayFPDF($this->Documento, 140, 70, 40);

            // fijamos las coordenadas de impresión
            $this->Documento->setXY(10, 50);

        } else {

            // si existe la imagen
            if (!is_null($this->Logo)){

                // determinamos el tipo de imagen
                $tipo = $this->tipoImagen($this->Logo);

                // agregamos la imagen indicando el tamaño y la extensión
                $this->Documento->Image($this->Logo, 150, 145, 40, 40, $tipo);

            }

            // presenta el código de barras
            $this->Documento->Code39(140, 190, "E-$this->IdEgreso-L-$this->IdLaboratorio", 1, 10);

            // obtenemos el código qr
            $qrcode = new QRcode("E-$this->IdEgreso-$this->IdLaboratorio", 'H');

            // lo adjuntamos al documento
            $qrcode->displayFPDF($this->Documento, 140, 210, 40);

            // fijamos las coordenadas de impresión
            $this->Documento->setXY(10,190);

        }

        // ahora presenta el título
        $this->Documento->Cell(50,10, "Entrega de Materiales", 0, 0, "C");

    }

    /**
     * Método que presenta el detalle del remito
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function imprimirDetalle(){

       // si estamos imprimiendo el original
       if ($this->Documento->GetY() < 100){

            // la imagen si tiene
            if (!is_null($this->Imagen)){

                // determinamos el tipo
                $tipo = $this->tipoImagen($this->Imagen);
                $this->Documento->Image($this->Imagen, 10, 70, 20, 20, $tipo);

            // si no tiene imagen
            } else {

                // cargamos la imagen por defecto
                $this->Documento->Image("../../imagenes/imagen_no_disponible.gif", 10, 70, 20, 20);

            }

            // definimos las coordenadas
            $this->Documento->SetXY(45, 70);

        // si estamos imprimiendo el duplicado
        } else {

            // la imagen si tiene
            if (!is_null($this->Imagen)){

                // determinamos el tipo
                $tipo = $this->tipoImagen($this->Imagen);
                $this->Documento->Image($this->Imagen, 10, 200, 20, 20, $tipo);

            // si no tiene imagen
            } else {

                // cargamos la imagen por defecto
                $this->Documento->Image("../../imagenes/imagen_no_disponible.gif", 10, 200, 20, 20);

            }

            // definimos las coordenadas
            $this->Documento->SetXY(45, 200);

        }

        // fijamos la fuente
        $this->Documento->setFont("DejaVu", "", 12);

        // la descripción
        $texto = "Item: " . $this->Descripcion;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // si tiene código sop
        if ($this->CodigoSop != ""){

            // establecemos la posición del cabezal
            $this->Documento->SetX(45);

            // imprimimos el código sop
            $texto = "Código Sop: " . $this->CodigoSop;
            $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        }

        // establecemos la posición del cabezal
        $this->Documento->SetX(45);

        // el número de remito
        $texto = "Remito Nro. " . $this->Remito;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // establecemos la posición del cabezal
        $this->Documento->SetX(45);

        // la fecha de entrega
        $texto = "Fecha Entrega: " . $this->FechaEgreso;

        // establecemos la posición del cabezal
        $this->Documento->SetX(45);

        // si existen los comentarios
        if (!empty($this->Comentarios)){
            $this->Documento->MultiCell(100, 8, strip_tags($this->Comentarios), 0, "J");
        }

        // establecemos la posición del cabezal
        $this->Documento->SetX(45);

        // ahora imprimimos el espacio para la firma
        $this->Documento->MultiCell(100, 8, "Entregó: " . $this->Usuario, 0, 'L');

        // establecemos la posición del cabezal
        $this->Documento->SetX(45);

        // el usuario que recibió
        $this->Documento->MultiCell(100, 8, "Recibió: " . $this->Recibio, 0, 'L');

    }

    /**
     * Método que a partir de la cadena que recibe, retorna el tipo de
     * archivo de imagen (lo guardamos logos e imágenes como base 64
     * en la base)
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string
     */
    protected function tipoImagen($archivo){

        // inicializamos la variable
        $tipo = null;

        // si es jpeg
        if (stripos($archivo, "data:image/jpeg;") !== false){
            $tipo = "jpeg";
        // si es gif
        } elseif (stripos($archivo, "data:image/gif;") !== false){
            $tipo = "gif";
        // si es png
        } if (stripos($archivo, "data:image/png;") !== false){
            $tipo = "png";
        // si es jpg
        } elseif (stripos($archivo, "data:image/jpg;") !== false){
            $tipo = "jpg";
        }

        // retornamos el tipo
        return $tipo;

    }

}
