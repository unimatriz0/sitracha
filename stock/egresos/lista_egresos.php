<?php

/**
 *
 * stock/lista_egresos.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/02/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de egresos
 *
*/

// incluímos los archivos e instanciamos
require_once("egresos.class.php");
$egreso = new Egresos();

// obtenemos la nómina
$nomina = $egreso->getEgresos();

// definimos el array
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al vector
    $jsondata[] = array("Id" =>          $idegreso,
                        "Descripcion" => $descripcion,
                        "Remito" =>      $remito,
                        "Cantidad" =>    $cantidad,
                        "Fecha" =>       $fecha_egreso,
                        "Entrego" =>     $entrego,
                        "Recibio" =>     $recibio);

}

// retornamos el array
echo json_encode(($jsondata));
