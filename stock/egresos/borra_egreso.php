<?php

/**
 *
 * stock/borra_egreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un egreso y ejecuta la
 * consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once("stock.class.php");
$egreso = new Stock();

// ejecutamos la consulta
$egreso->borraEgreso($_GET["id"]);

// retornamos verdadero
echo json_encode(array("Error" => 1));
?>