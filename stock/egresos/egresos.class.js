/*
    Nombre: egresos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 15/12/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de stock del sistema

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las salidas del depósito
 */
class Egresos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // del layer de ingresos
        this.layerEgresos = "";

        // el layer de los cuadros emergentes
        this.layerEmergente = "";

        // de los egresos del depósito
        this.initEgresos();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase de los egresos
     */
    initEgresos(){

        // inicializamos las variables
        this.IdEgreso = 0;                      // clave del registro
        this.ItemEgreso = 0;                    // clave del artículo
        this.DescripcionEgreso = "";            // descripción del artìculo
        this.IdLabEgreso = 0;                   // clave del laboratorio
        this.RemitoEgreso = 0;                  // número de remito
        this.ImagenEgreso = 0;                  // imagen del artículo
        this.CantidadEgreso = 0;                // cantidad entregada
        this.FechaEgreso = "";                  // fecha de entrega
        this.IdEntrego = 0;                     // clave del usurio que entregó
        this.UsuarioEntrego = "";               // nombre del usuario que entregó
        this.IdRecibio = 0;                     // clave del usuario que recibió
        this.UsuarioRecibio = "";               // nombre del usuario que recibió
        this.ComentariosEgreso = "";            // comentarios y observaciones

    }

    // asignación de valores a las variables de egresos
    setIdEgreso(idegreso){
        this.IdEgreso = idegreso;
    }
    setItemEgreso(item){
        this.ItemEgreso = item;
    }
    setDescripcionEgreso(descripcion){
        this.DescripcionEgreso = descripcion;
    }
    setSop(sop){
        this.Sop = sop;
    }
    setImagenEgreso(imagen){

        // si recibió algo
        if (imagen != null && imagen != "null") {
            this.ImagenEgreso = imagen;
        } else {
            this.ImagenEgreso = 'imagenes/imagen_no_disponible.gif';
        }

    }
    setRemitoEgreso(remito){
        this.RemitoEgreso = remito;
    }
    setCantidadEgreso(cantidad){
        this.CantidadEgreso = cantidad;
    }
    setFechaEgreso(fecha){
        this.FechaEgreso = fecha;
    }
    setEntrego(entrego){
        this.UsuarioEntrego = entrego;
    }
    setRecibio(recibio){
        this.UsuarioRecibio = recibio;
    }
    setIdRecibio(idrecibio){
        this.IdRecibio = idrecibio;
    }
    setComentariosEgreso(comentarios){
        this.ComentariosEgreso = comentarios;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenedor la grilla con los egresos del depósito
     */
    Egresos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el documento
        $("#form_stock").load("stock/egresos/grilla_egresos.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idegreso - clave del registro a mostrar
     * Método que recibe como parámetro la clave de un agreso, abre el layer
     * emergente con el formulario de egresos y si la clave es distinta de
     * indeterminada, carga en el formulario los datos del egreso
     */
    verEgreso(idegreso){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el formulario en una ventana emergente
        this.layerIngresos = new jBox('Modal', {
                                 animation: 'flip',
                                 closeOnEsc: true,
                                 closeOnClick: false,
                                 closeOnMouseleave: false,
                                 closeButton: true,
                                 repositionOnContent: true,
                                 overlay: false,
                                 title: 'Egresos del Depósito',
                                 draggable: 'title',
                                 onCloseComplete: function(){
                                    this.destroy();
                                 },
                                 theme: 'TooltipBorder',
                                 ajax: {
                                     url: 'stock/egresos/egresos.html',
                                     reload: 'strict'
                                 }
                            });
        this.layerIngresos.open();

        // si recibió la id
        if (typeof(idegreso) != "undefined"){

            // obtenemos los datos del registro
            // lo llamamos asincrónico
            $.ajax({
                url: "stock/egresos/getegreso.php?idegreso=" + idegreso,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                async: false,
                success: function (data) {

                    // asignamos los resultados a las variables de clase
                    egresos.setIdEgreso(data.IdEgreso);
                    egresos.setItemEgreso(data.IdItem);
                    egresos.setDescripcionEgreso(data.Descripcion);
                    egresos.setSop(data.Sop);
                    egresos.setImagenEgreso(data.Imagen);
                    egresos.setRemitoEgreso(data.Remito);
                    egresos.setCantidadEgreso(data.Cantidad);
                    egresos.setFechaEgreso(data.Fecha);
                    egresos.setEntrego(data.Usuario);
                    egresos.setIdRecibio(data.IdRecibio);
                    egresos.setComentariosEgreso(data.Observaciones);

                    // mostramos el registro
                    egresos.muestraEgreso();

                }

            });

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase carga los datos en el
     * formulario de egresos
     */
    muestraEgreso(){

        // cargamos en el formulario los valores de las variables de clase
        // de los egresos del depósito
        document.getElementById("id_egreso").value = this.IdEgreso;
        document.getElementById("id_item_egreso").value = this.ItemEgreso;
        document.getElementById("desc_item_egreso").value = this.DescripcionEgreso;
        document.getElementById("sop_egreso").value = this.Sop;
        document.getElementById("remito_egreso").value = this.RemitoEgreso;
        document.getElementById("imagen_egreso").src = this.ImagenEgreso;
        document.getElementById("cantidad_egreso").value = this.CantidadEgreso;
        document.getElementById("fecha_egreso").value = this.FechaEgreso;
        document.getElementById("usuario_egreso").value = this.UsuarioEntrego;
        document.getElementById("observaciones_egreso").value = this.ComentariosEgreso;

        // recargamos el select de usuarios y le pasamos como argumento el
        // usuario que recibió
        egresos.cargaUsuarios(this.IdRecibio);

        // fijamos el valor de el plugin de comentarios
        CKEDITOR.instances['observaciones_egreso'].setData(this.ComentariosEgreso);

        // fijamos el foco
        document.getElementById("desc_item_egreso").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente destruye el layer de los egresos
     */
    cancelaEgreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // reiniciamos las variables
        this.initEgresos();

        // destruimos el layer
        this.layerEgresos.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de pedir confirmación llama la rutina de eliminación del
     * egreso activo en el formulario
     */
    borraEgreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la id del egreso
        var idegreso = document.getElementById("id_egreso").value;

        // por las dudas si está en blanco
        if (idegreso == ""){

            // simplemente retornamos
            return false;

        }

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                            animation: 'flip',
                            title: 'Borrar Egreso',
                            closeOnEsc: false,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: true,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            overlay: false,
                            content: 'Está seguro que desea eliminar el egreso?',
                            theme: 'TooltipBorder',
                            confirm: function() {

                                // aquí llamamos la eliminación
                                egresos.eliminaEgreso(idegreso);

                                // destruimos el cuadro de confirmación
                                Confirmacion.destroy();

                                // reiniciamos las variables
                                egresos.initEgresos();

                                // recarga el formulario
                                egresos.Egresos();

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'

                });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idegreso - clave del registro a eliminar
     * Método que recibe la clave de un egreso y llama la rutina de eliminación
     */
    eliminaEgreso(idegreso){

        // llamamos sincrónico
        $.ajax({
            url: "stock/egresos/borra_egreso.php?id=" + idegreso,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // siempre retorna verdadero
                if (data.Error == 1){

                    // presenta el mensaje
                    new jBox('Notice', { content: "Egreso eliminado ...", color: 'red' });

                    // destruimos el layer
                    stock.layerIngresos.destroy();

                }

            }

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar de un egreso, verifica los
     * datos del formulario y asigna los valores en las variables de clase
     */
    verificaEgreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("id_egreso").value != ""){

            // asigna en la variable de clase
            this.IdEgreso = document.getElementById("id_egreso").value;

        }

        // si no seleccionó correctamente el item
        if (document.getElementById("id_item_egreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el artículo a entregar";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("desc_item_egreso").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.ItemEgreso = document.getElementById("id_item_egreso").value;

        }

        // si no ingresó la cantidad
        if (document.getElementById("cantidad_egreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la cantidad a entregar";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("cantidad_egreso").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.CantidadEgreso = document.getElementById("cantidad_egreso").value;

        }

        // si no indicó quien recibió
        if (document.getElementById("recibio_egreso").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione quien recibe el artículo";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("recibio_egreso").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.IdRecibio = document.getElementById("recibio_egreso").value;

        }

        // si hay observaciones
        this.ComentariosEgreso = CKEDITOR.instances['observaciones_egreso'].getData();

        // llamamos la rutina de grabado
        this.grabaEgreso();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario, ejecuta la consulta
     * en el servidor y luego asigna en el formulario la id del egreso
     * y el número de remito
     */
    grabaEgreso(){

        // definimos el formulario
        var datosEgreso = new FormData();

        // si está editando
        if (this.IdEgreso != 0){
            datosEgreso.append("Id", this.IdEgreso);
        }

        // asignamos el resto
        datosEgreso.append("IdItem", this.ItemEgreso);
        datosEgreso.append("Cantidad", this.CantidadEgreso);
        datosEgreso.append("IdRecibio", this.IdRecibio);
        datosEgreso.append("Comentarios", this.ComentariosEgreso);

        // enviamos el formulario por ajax lo llamamos sincrónico para
        // que al recargar la grilla refleje los cambios
        $.ajax({
            type: "POST",
            url: "stock/egresos/graba_egreso.php",
            data: datosEgreso,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si ocurrió un error
                if (data.Id == 0) {

                    // presenta el mensaje
                    new jBox('Notice', { content: "Ha ocurrido un error", color: 'red' });

                } else {

                    // presenta el mensaje
                    new jBox('Notice', { content: "Registro grabado ... ", color: 'green' });

                    // fijamos la id del registro y el remito
                    document.getElementById("id_egreso").value = data.Id;
                    document.getElementById("remito_egreso").value = data.Remito;

                    // recargamos la grilla de egresos
                    egresos.limpiaGrilla();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idegreso - clave del registro a imprimir
     * Método llamado al pulsar el botón remito del formulario de egresos
     * verifica que el registro esté grabado y luego genera el remito, si
     * no recibe la id del egreso asume que fue llamada directamente
     * desde el formulario, si recibe la id asumimos que fue llamado desde
     * la grilla de entrega de materiales
     */
    remitoEgreso(idegreso){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no lo recibió
        if (typeof(idegreso) == "undefined"){

            // leemos del formulario
            idegreso = document.getElementById("id_egreso").value;

        }

        // ahora llamamos el layer emergente que va a
        // presentar el remito
        this.layerIngresos = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Remito Egreso',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'stock/egresos/remito_egreso.php?idegreso='+idegreso,
                        reload: 'strict'
                    }
            });
        this.layerIngresos.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} iditem - clave del item
     * Método llamado al seleccionar un item en el autocomplete de los
     * egresos, fija el valor de la clave del artículo y obtiene la
     * imagen y el código sop del mismo
     */
    selItemEgreso(iditem){

        // reiniciamos la sesión
        sesion.reiniciar();

        // actualizamos en el formulario
        document.getElementById("id_item_egreso").value = iditem;

        // lo llamamos sincrónico
        $.ajax({
            url: "stock/egresos/busca_item_egreso.php?iditem=" + iditem,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // asignamos el resultado a la variable de clase
                egresos.setImagenEgreso(data.Imagen);
                egresos.setSop(data.Sop);
                document.getElementById("imagen_egreso").src = egresos.ImagenEgreso;
                document.getElementById("sop_egreso").value = data.Sop;

            }

        });

        // seteamos el foco
        document.getElementById("cantidad_egreso").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idusuario - clave del usuario
     * Método que llama la clase de usuarios para cargar en el select
     * la nómina de usuarios del laboratorio, si recibe la id del
     * usuario es que fue llamada en la presentación de datos
     */
    cargaUsuarios(idusuario){

        // llamamos la clase de usuarios
        usuarios.nominaUsuarios("recibio_egreso", idusuario);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cargar la grilla de egresos que obtiene
     * la nómina de egresos
     */
    cargaEgresos(){

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/egresos/lista_egresos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                egresos.cargaGrilla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - nómina de ingresos
     * Método que recibe el array con los egresos del depósito
     * y los carga en la grilla
     */
    cargaGrilla(datos){

        // declaramos las variables
        var tabla = $('#tegresos').DataTable();
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // armamos el enlace
            enlace =  "<input type='button' ";
            enlace += "        name='btnEditar' ";
            enlace += "        class='botoneditar' ";
            enlace += "        title='Pulse para editar el registro' ";
            enlace += "        onClick='egresos.verEgreso(" + datos[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                    datos[i].Descripcion,
                    datos[i].Remito,
                    datos[i].Cantidad,
                    datos[i].Fecha,
                    datos[i].Entrego,
                    datos[i].Recibio,
                    enlace
                ]).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de grabar un registro que
     * recarga la grilla
     */
    limpiaGrilla(){

        // limpiamos la tabla y luego recargamos
        var tabla = $('#tegresos').DataTable();
        tabla.clear();
        this.cargaEgresos();

    }

}