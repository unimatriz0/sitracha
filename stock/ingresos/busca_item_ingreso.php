<?php

/**
 *
 * stock/ingresos/busca_item_ingreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método utilizado al seleccionar un item en el formulario de
 * ingresos, recibe como parámetro la clave del item y retorna
 * la imagen del artículo
 *
*/

// incluimos e instanciamos las clases
require_once ("../articulos/items.class.php");
$items = new Items();

// obtenemos la imagen
$items->getDatosItem($_GET["iditem"]);
$imagen = $items->getImagen();

// retornamos la imagen
echo json_encode(array("Imagen" => $imagen));

?>