<?php

/**
 *
 * stock/ingresos/lista_ingresos.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/02/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con los ingresos al
 * depósito
 *
*/

// incluimos e instanciamos las clases
require_once("ingresos.class.php");
$ingreso = new Ingresos();

// declaración de variables
$jsondata = array();

// obtenemos el vector
$nomina = $ingreso->getIngresos();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al array
    $jsondata[] = array("Id" =>          $idingreso,
                        "Descripcion" => $descripcion,
                        "Importe" =>     $importe,
                        "Remito" =>      $remito,
                        "Cantidad" =>    $cantidad,
                        "Vencimiento" => $vencimiento,
                        "Usuario" =>     $usuario);

}

// retornamos el vector
echo json_encode($jsondata);

?>