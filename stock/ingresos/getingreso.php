<?php

/**
 *
 * stock/ingresos/getingreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro de ingreso y
 * retorna un array json con los datos del mismo
 *
*/

// incluimos e instanciamos las clases
require_once("ingresos.class.php");
$ingreso = new Ingresos();

// obtenemos el registro
$ingreso->getDatosIngreso($_GET["idingreso"]);

// retornamos el registro
echo json_encode(array("IdItem" =>         $ingreso->getIdItemIngreso(),
                       "Descripcion" =>    $ingreso->getDescripcionIngreso(),
                       "Imagen" =>         $ingreso->getImagenIngreso(),
                       "IdIngreso" =>      $ingreso->getIdIngreso(),
                       "Importe" =>        $ingreso->getImporteIngreso(),
                       "Remito" =>         $ingreso->getRemitoIngreso(),
                       "Cantidad" =>       $ingreso->getCantidadIngreso(),
                       "Financiamiento" => $ingreso->getIdFinanciamiento(),
                       "Vencimiento" =>    $ingreso->getVencimiento(),
                       "Ubicacion" =>      $ingreso->getUbicacion(),
                       "Lote" =>           $ingreso->getLote(),
                       "Fecha" =>          $ingreso->getFechaIngreso(),
                       "Usuario" =>        $ingreso->getUsuarioIngreso(),
                       "Comentarios" =>    $ingreso->getComentariosIngreso()));
?>