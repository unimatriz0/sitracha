<?php

/**
 *
 * stock/ingresos/graba_ingreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro de ingreso
 * y ejecuta la consulta de actualización, retorna la clave
 * del registro afectado
 *
*/

// incluimos e instanciamos las clases
require_once("ingresos.class.php");
$ingreso = new Ingresos();

// si está editando
if (!empty($_POST["Id"])){
    $ingreso->setIdIngreso($_POST["Id"]);
}

// agregamos el resto de las propiedades
$ingreso->setIdItemIngreso($_POST["Item"]);
$ingreso->setImporteIngreso($_POST["Importe"]);
$ingreso->setRemitoIngreso($_POST["Remito"]);
$ingreso->setCantidadIngreso($_POST["Cantidad"]);
$ingreso->setIdFinanciamiento($_POST["Financiamiento"]);
$ingreso->setVencimiento($_POST["Vencimiento"]);
$ingreso->setLote($_POST["Lote"]);
$ingreso->setUbicacion($_POST["Ubicacion"]);
$ingreso->setComentariosIngreso($_POST["Comentarios"]);

// ejecutamos la consulta
$resultado = $ingreso->grabaIngreso();

// retornamos el resultado
echo json_encode(array("Id" => $resultado));

?>