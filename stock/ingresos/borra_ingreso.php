<?php

/**
 *
 * stock/ingresos/borra_ingreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un ingreso y ejecuta la
 * consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once("ingresos.class.php");
$egreso = new Ingresos();

// ejecutamos la consulta
$egreso->borraIngreso($_GET["id"]);

// retornamos verdadero
echo json_encode(array("Error" => 1));

?>