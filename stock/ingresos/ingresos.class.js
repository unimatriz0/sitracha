/*
    Nombre: ingresos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 15/12/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de stock del sistema

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las entradas y
 * salidas del depósito
 */
class Ingresos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // del layer de ingresos
        this.layerIngresos = "";

        // el layer de los cuadros emergentes
        this.layerEmergente = "";

        // de los ingresos al inventario
        this.initIngresos();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase de los ingresos
     */
    initIngresos(){

        // inicializamos las variables
        this.IdIngreso = 0;
        this.ItemIngreso = 0;
        this.DescripcionIngreso = "";
        this.ImagenIngreso = "";
        this.ImporteIngreso = 0;
        this.IdLabIngreso = 0;
        this.RemitoIngreso = "";
        this.CantidadIngreso = 0;
        this.Financiamiento = "";
        this.IdFinanciamiento = 0;
        this.Vencimiento = "";
        this.FechaIngreso = "";
        this.Lote = "";
        this.Ubicacion = "";
        this.IdUsuarioIngreso = 0;
        this.UsuarioIngreso = "";
        this.ComentariosIngreso = "";

    }

    // asignación de valores en las variables de ingresos
    setIdIngreso(idingreso){
        this.IdIngreso = idingreso;
    }
    setItemIngreso(itemingreso){
        this.ItemIngreso = itemingreso;
    }
    setDescripcionIngreso(descripcion){
        this.DescripcionIngreso = descripcion;
    }
    setImagenIngreso(imagen) {

        // si recibió algo
        if (imagen != null && imagen != "null") {
            this.ImagenIngreso = imagen;
        } else {
            this.ImagenIngreso = 'imagenes/imagen_no_disponible.gif';
        }

    }
    setImporteIngreso(importe){
        this.ImporteIngreso = importe;
    }
    setRemitoIngreso(remito){
        this.RemitoIngreso = remito;
    }
    setCantidadIngreso(cantidad){
        this.CantidadIngreso = cantidad;
    }
    setIdFinanciamiento(idfinanciamiento){
        this.IdFinanciamiento = idfinanciamiento;
    }
    setFinanciamiento(financiamiento){
        this.Financiamiento = financiamiento;
    }
    setVencimiento(vencimiento){
        this.Vencimiento = vencimiento;
    }
    setFechaIngreso(fecha){
        this.FechaIngreso = fecha;
    }
    setLote(lote){
        this.Lote = lote;
    }
    setUbicacion(ubicacion){
        this.Ubicacion = ubicacion;
    }
    setUsuarioIngreso(usuario){
        this.UsuarioIngreso = usuario;
    }
    setComentariosIngreso(comentarios){
        this.ComentariosIngreso = comentarios;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenedor la grilla con los ingresos al depósito
     */
    Ingresos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el documento
        $("#form_stock").load("stock/ingresos/grilla_ingresos.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el formulario de ingresos los elementos
     * del select de financiamiento
     */
    cargaFinanciamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // le pasamos a la clase la id del elemento
        financiamiento.nominaFinanciamiento("financiamiento_ingreso");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} iditem - clave del registro
     * Método llamado al seleccionar un item de len el autocomplete
     * del formulario de ingresos, recibe como parámetro la id del
     * itemy obtiene la imagen del mismo que presenta en el
     * formulario
     */
    selItemIngreso(iditem){

        // reiniciamos la sesión
        sesion.reiniciar();

        // los cargamos en el formulario
        document.getElementById("iditem").value = iditem;

        // lo llamamos sincrónico
        $.ajax({
            url: "stock/ingresos/busca_item_ingreso.php?iditem=" + iditem,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // asignamos el resultado a la variable de clase
                ingresos.setImagenIngreso(data.Imagen);
                document.getElementById("imagen_ingreso").src = ingresos.ImagenIngreso;

            }

        });

        // seteamos el foco
        document.getElementById("remito_ingreso").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idingreso
     * Método que recibe como parámetro la clave de un ingreso y presenta el formulario
     * para la edición del mismo, en caso de no recibir la clave, asume que se trata de
     * un alta y presenta el formulario en blanco
     */
    verIngreso(idingreso){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el formulario en una ventana emergente
        this.layerIngresos = new jBox('Modal', {
                                 animation: 'flip',
                                 closeOnEsc: true,
                                 closeOnClick: false,
                                 closeOnMouseleave: false,
                                 closeButton: true,
                                 repositionOnContent: true,
                                 overlay: false,
                                 title: 'Ingresos al Depósito',
                                 draggable: 'title',
                                 onCloseComplete: function(){
                                    this.destroy();
                                 },
                                 theme: 'TooltipBorder',
                                 ajax: {
                                     url: 'stock/ingresos/ingresos.html',
                                     reload: 'strict'
                                 }
                            });
        this.layerIngresos.open();

        // si recibió la id
        if (typeof(idingreso) == "number"){

            // lo llamamos asincrónico
            $.ajax({
                url: "stock/ingresos/getingreso.php?idingreso=" + idingreso,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                async: false,
                success: function (data) {

                    // asignamos los resultados a las variables de clase
                    ingresos.setItemIngreso(data.IdItem);
                    ingresos.setDescripcionIngreso(data.Descripcion);
                    ingresos.setImagenIngreso(data.Imagen);
                    ingresos.setIdIngreso(data.IdIngreso);
                    ingresos.setImporteIngreso(data.Importe);
                    ingresos.setRemitoIngreso(data.Remito);
                    ingresos.setCantidadIngreso(data.Cantidad);
                    ingresos.setIdFinanciamiento(data.Financiamiento);
                    ingresos.setVencimiento(data.Vencimiento);
                    ingresos.setLote(data.Lote);
                    ingresos.setUbicacion(data.Ubicacion);
                    ingresos.setFechaIngreso(data.Fecha);
                    ingresos.setUsuarioIngreso(data.Usuario);
                    ingresos.setComentariosIngreso(data.Comentarios);

                    // mostramos el registro
                    ingresos.muestraIngreso();

                }

            });

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase completa los datos del formulario
     * de ingresos
     */
    muestraIngreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos los valores de las variables de clase en el formulario
        document.getElementById("idingreso").value = this.IdIngreso;
        document.getElementById("iditem").value = this.ItemIngreso;
        document.getElementById("descripcion_ingreso").value = this.DescripcionIngreso;
        document.getElementById("remito_ingreso").value = this.RemitoIngreso;
        document.getElementById("imagen_ingreso").src = this.ImagenIngreso;
        document.getElementById("cantidad_ingreso").value = this.CantidadIngreso;
        document.getElementById("importe_ingreso").value = this.ImporteIngreso;
        document.getElementById("financiamiento_ingreso").value = this.IdFinanciamiento;
        document.getElementById("lote_ingreso").value = this.Lote;
        document.getElementById("ubicacion_ingreso").value = this.Ubicacion;
        document.getElementById("vencimiento_ingreso").value = this.Vencimiento;
        document.getElementById("alta_ingreso").value = this.FechaIngreso;
        document.getElementById("usuario_ingreso").value = this.UsuarioIngreso;
        document.getElementById("observaciones_ingreso").value = this.ComentariosIngreso;

        // si el usuario no puede editar stock
        if (sessionStorage.getItem("Stock") == "No"){

            // desactivamos el botón grabar
            document.getElementById("btnGrabaIngreso").enabled = false;

        // si está autorizado
        } else {

            // lo activa
            document.getElementById("btnGrabaIngreso").enabled = true;

        }

        // seteamos el foco
        document.getElementById("descripcion_ingreso").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que cierra el formulario de ingresos
     */
    cancelaIngreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializamos las variables
        this.initIngresos();

        // destruimos el formulario
        this.layerIngresos.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que genera y presenta el remito de un ingreso
     */
    remitoIngreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la id del ingreso
        var idingreso = document.getElementById("idingreso").value;

        // ahora llamamos el layer emergente que va a
        // presentar el remito
        this.layerIngresos = new jBox('Modal', {
                                  animation: 'flip',
                                  closeOnEsc: true,
                                  closeOnClick: false,
                                  closeOnMouseleave: false,
                                  closeButton: true,
                                  repositionOnContent: true,
                                  overlay: false,
                                  title: 'Remito Ingresos',
                                  draggable: 'title',
                                  theme: 'TooltipBorder',
                                  onCloseComplete: function(){
                                    this.destroy();
                                  },
                                  ajax: {
                                      url: 'stock/ingresos/remito_ingreso.php?idingreso='+idingreso,
                                      reload: 'strict'
                                  }
                            });
        this.layerIngresos.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de ingresos al
     * depósito y asigna los valores en las variables de clase
     */
    verificaIngreso(){

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("idingreso").value != ""){

            // asigna en la clase
            this.IdIngreso = document.getElementById("idingreso").value;

        // si está editando
        } else {

            // inicializamos la variable de clase
            this.IdIngreso = 0;

        }

        // si no ingresó la descripción
        if (document.getElementById("descripcion_ingreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la descripción del Item";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("descripcion_ingreso").focus();
            return false;

        }

        // si no seleccionó adecuadamente
        if (document.getElementById("iditem").value == ""){

            // presenta el mensaje y retorna
            mensaje = "El item no está correctamente seleccionado";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("descripcion_ingreso").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.ItemIngreso = document.getElementById("iditem").value;

        }

        // si no indicó el remito
        if (document.getElementById("remito_ingreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el remito de ingreso";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("remito_ingreso").focus();
            return false;

        // si asignó
        } else {

            // asigna en la variable de clase
            this.RemitoIngreso = document.getElementById("remito_ingreso").value;

        }

        // si no indicó el importe total
        if (document.getElementById("importe_ingreso").value == "" || document.getElementById("importe_ingreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el importe total del remito";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("importe_ingreso").focus();
            return false;

        // si indicó
        } else {

            // asigna en la variable de clase
            this.ImporteIngreso = document.getElementById("importe_ingreso").value;

        }

        // si no indicó la cantidad
        if (document.getElementById("cantidad_ingreso").value == "" || document.getElementById("cantidad_ingreso").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Indique el número de unidades ingresadas";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("cantidad_ingreso").focus();
            return false;

        // si asignó
        } else {

            // asigna en la variable de clase
            this.CantidadIngreso = document.getElementById("cantidad_ingreso").value;

        }

        // si no indicó la fuente de financiamiento
        if (document.getElementById("financiamiento_ingreso").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista la fuente de financiamiento";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("financiamiento_ingreso").focus();
            return false;

        // si indicó
        } else {

            // asigna en la variable de clase
            this.IdFinanciamiento = document.getElementById("financiamiento_ingreso").value;

        }

        // si no indicó la fecha de vencimiento
        if (document.getElementById("vencimiento_ingreso").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de vencimiento";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("vencimiento_ingreso").focus();
            return false;

        // si asignó
        } else {

            // asigna en la variable de clase
            this.Vencimiento = document.getElementById("vencimiento_ingreso").value;

        }

        // si no ingresó el lote
        if (document.getElementById("lote_ingreso").value == ""){

            // simplemente lanza un alerta
            mensaje = "No hay información del lote";
            new jBox('Notice', { content: mensaje, color: 'red' });

        }

        // si no ingresó la ubicación
        if (document.getElementById("ubicacion_ingreso").value == ""){

            // lanza un alerta
            mensaje = "No hay información de la ubicación";
            new jBox('Notice', { content: mensaje, color: 'red' });

        }

        // asignamos en la variable de clase
        this.Lote = document.getElementById("lote_ingreso").value;
        this.Ubicacion = document.getElementById("ubicacion_ingreso").value;

        // asigna los comentarios
        this.ComentariosIngreso = CKEDITOR.instances['observaciones_ingreso'].getData();

        // llama la rutina de grabación
        this.grabaIngreso();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de la verificación de los datos del
     * formulario, llama por ajax la actualización de la base
     */
    grabaIngreso(){

        // declaración de variables
        var mensaje;

        // creamos el objeto
        var datosIngreso = new FormData();

        // adjuntamos las variables de clase
        if (this.IdIngreso != 0 && this.IdIngreso != ""){
            datosIngreso.append("Id", this.IdIngreso);
        }
        datosIngreso.append("Item", this.ItemIngreso);
        datosIngreso.append("Importe", this.ImporteIngreso);
        datosIngreso.append("Remito", this.RemitoIngreso);
        datosIngreso.append("Cantidad", this.CantidadIngreso);
        datosIngreso.append("Financiamiento", this.IdFinanciamiento);
        datosIngreso.append("Ubicacion", this.Ubicacion);
        datosIngreso.append("Lote", this.Lote);
        datosIngreso.append("Vencimiento", this.Vencimiento);
        datosIngreso.append("Comentarios", this.ComentariosIngreso);

        // enviamos el formulario por ajax lo llamamos sincrónico para
        // que al recargar la grilla refleje los cambios
        $.ajax({
            type: "POST",
            url: "stock/ingresos/graba_ingreso.php",
            data: datosIngreso,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si ocurrió un error
                if (data.Id == 0) {

                    // presenta el mensaje
                    mensaje = "Ha ocurrido un error";
                    new jBox('Notice', { content: mensaje, color: 'red' });

                } else {

                    // presenta el mensaje
                    mensaje = "Registro grabado ... ";
                    new jBox('Notice', { content: mensaje, color: 'green' });

                    // asignamos la id del registro
                    document.getElementById("idingreso").value = data.Id;

                    // cerramos el layer
                    ingresos.layerIngresos.destroy();

                    // recargamos la grilla de ingresos
                    ingresos.limpiaGrilla();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de pedir confirmación, llama la consulta de
     * eliminación del ingreso activo
     */
    borraIngreso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la clave del ingreso
        var idingreso = document.getElementById("idingreso").value;

        // por las dudas si está vacío
        if (idingreso == ""){

            // simplemente retornamos
            return false;

        }

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                                animation: 'flip',
                                title: 'Borrar Ingreso',
                                closeOnEsc: false,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                overlay: false,
                                content: 'Está seguro que desea eliminar el ingreso?',
                                theme: 'TooltipBorder',
                                confirm: function() {

                                    // aquí llamamos la eliminación
                                    ingresos.eliminaIngreso(idingreso);

                                    // destruimos el cuadro de diálogo
                                    Confirmacion.destroy();

                                },
                                cancel: function(){
                                    Confirmacion.destroy();
                                },
                                confirmButton: 'Aceptar',
                                cancelButton: 'Cancelar'
                    });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idingreso - clave del registro a eliminar
     * Método que ejecuta la eliminación del ingreso, recibe como
     * parámetro la clave del registro
     */
    eliminaIngreso(idingreso){

        // llamamos sincrónico
        $.ajax({
            url: "stock/ingresos/borra_ingreso.php?id=" + idingreso,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // siempre retorna verdadero
                if (data.Error == 1){

                    // presenta el mensaje
                    new jBox('Notice', { content: "Ingreso eliminado ...", color: 'red' });

                    // inicializamos las variables
                    ingresos.initIngresos();

                    // recargamos la grilla
                    ingresos.limpiaGrilla();

                    // destruimos el layer
                    ingresos.layerIngresos.destroy();

                }

            }

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cargar la grilla de ingresos que obtiene
     * la nómina de ingresos
     */
    cargaIngresos(){

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/ingresos/lista_ingresos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                ingresos.cargaGrilla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - nómina de ingresos
     * Método que recibe el array con los ingresos al depósito
     * y los carga en la grilla
     */
    cargaGrilla(datos){

        // declaramos las variables
        var tabla = $('#tingresos').DataTable();
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // armamos el enlace
            enlace =  "<input type='button' ";
            enlace += "        name='btnEditar' ";
            enlace += "        class='botoneditar' ";
            enlace += "        title='Pulse para editar el registro' ";
            enlace += "        onClick='ingresos.verIngreso(" + datos[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                    datos[i].Descripcion,
                    datos[i].Importe,
                    datos[i].Remito,
                    datos[i].Cantidad,
                    datos[i].Vencimiento,
                    datos[i].Usuario,
                    enlace
                ]).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de grabar un registro que
     * recarga la grilla
     */
    limpiaGrilla(){

        // limpiamos la tabla y luego recargamos
        var tabla = $('#tingresos').DataTable();
        tabla.clear();
        this.cargaIngresos();

    }

}