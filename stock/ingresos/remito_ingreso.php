<?php

/**
 *
 * stock/remito_ingreso.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un remito, instancia
 * la clase y obtiene el contenido del remito el cual presenta
 * en un layer
 *
*/

// incluimos e instanciamos las clases
require_once("remitos.class.php");
$remito = new Remitos();

// generamos el remito
$remito->generaRemito($_GET["idingreso"]);

// enviamos los encabezados
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0, false");
header("Pragma: no-cache");
?>

<!-- cargamos el documento -->
<object data="temp/remito_ingreso.pdf"
        type="application/pdf"
        width="850" height="500">
</object>