<?php

/**
 *
 * Class Stock | stock/ingresos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones sobre las distintas tablas
 * que hacen al sistema de control de stock
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Ingresos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdDepartamento;        // clave del departamento del usuario
    protected $IdUsuario;             // clave del usuario activo

    // las variables de la tabla de ingresos
    protected $IdItemIngreso;         // clave del item
    protected $DescripcionIngreso;    // descripción del item
    protected $ImagenIngreso;         // imagen del item
    protected $IdIngreso;             // clave del ingreso
    protected $ImporteIngreso;        // importe del ingreso
    protected $IdFinanciamiento;      // clave del financiamiento
    protected $Financiamiento;        // financiamiento de la compra
    protected $RemitoIngreso;         // número de remito
    protected $CantidadIngreso;       // cantidad ingresada
    protected $Vencimiento;           // fecha de vencimiento
    protected $Lote;                  // lote del ingreso
    protected $Ubicacion;             // ubicación física del artículo
    protected $FechaIngreso;          // fecha del ingreso
    protected $UsuarioIngreso;        // nombre del usuario
    protected $ComentariosIngreso;    // comentarios

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // las variables de la tabla de ingresos
        $this->IdItemIngreso = 0;
        $this->DescripcionIngreso = "";
        $this->ImagenIngreso = "";
        $this->IdIngreso = 0;
        $this->ImporteIngreso = 0;
        $this->RemitoIngreso = "";
        $this->CantidadIngreso = 0;
        $this->IdFinanciamiento = 0;
        $this->Financiamiento = "";
        $this->Vencimiento = "";
        $this->FechaIngreso = "";
        $this->UsuarioIngreso = "";
        $this->ComentariosIngreso = "";

        // iniciamos sesión
        session_start();

        // si existen los valores de sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y el laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];
            $this->IdDepartamento = $_SESSION["IdDepartamento"];

        // si no inició
        } else {

            // abandona por error
            echo "No hay sesión iniciada";
            exit;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de la tabla ingresos
    public function setIdItemIngreso($iditem){

        // verifica que sea un número
        if (!is_numeric($iditem)){

            // abandona por error
            echo "La clave del item de ingreso debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->IdItemIngreso = $iditem;

        }

    }
    public function setDescripcionIngreso($descripcion){
        $this->DescripcionIngreso = $descripcion;
    }
    public function setIdIngreso($idingreso){

        // verifica que sea un número
        if (!is_numeric($idingreso)){

            // abandona por error
            echo "La clave del ingreso debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->IdIngreso = $idingreso;

        }

    }
    public function setImporteIngreso($importe){

        // verifica que sea un número
        if (!is_numeric($importe)){

            // abandona por error
            echo "El importe del ingreso debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->ImporteIngreso = $importe;

        }

    }
    public function setRemitoIngreso($remito){
        $this->RemitoIngreso = $remito;
    }
    public function setCantidadIngreso($cantidad){

        // verifica que sea un número
        if (!is_numeric($cantidad)){

            // abandona por error
            echo "La cantidad ingresada debe ser un número";
            exit;

        // si está correcto
        } else {

            // asigna
            $this->CantidadIngreso = $cantidad;

        }

    }
    public function setIdFinanciamiento($idfinanciamiento){
        $this->IdFinanciamiento = $idfinanciamiento;
    }
    public function setFinanciamiento($financiamiento){
        $this->Financiamiento = $financiamiento;
    }
    public function setVencimiento($vencimiento){
        $this->Vencimiento = $vencimiento;
    }
    public function setLote($lote){
        $this->Lote = $lote;
    }
    public function setUbicacion($ubicacion){
        $this->Ubicacion = $ubicacion;
    }
    public function setComentariosIngreso($comentarios){
        $this->ComentariosIngreso = $comentarios;
    }

    // métodos de retorno de valores de la tabla de ingresos
    public function getIdItemIngreso(){
        return $this->IdItemIngreso;
    }
    public function getDescripcionIngreso(){
        return $this->DescripcionIngreso;
    }
    public function getImagenIngreso(){
        return $this->ImagenIngreso;
    }
    public function getIdIngreso(){
        return $this->IdIngreso;
    }
    public function getImporteIngreso(){
        return $this->ImporteIngreso;
    }
    public function getRemitoIngreso(){
        return $this->RemitoIngreso;
    }
    public function getCantidadIngreso(){
        return $this->CantidadIngreso;
    }
    public function getIdFinanciamiento(){
        return $this->IdFinanciamiento;
    }
    public function getFinanciamiento(){
        return $this->Financiamiento;
    }
    public function getVencimiento(){
        return $this->Vencimiento;
    }
    public function getFechaIngreso(){
        return $this->FechaIngreso;
    }
    public function getLote(){
        return $this->Lote;
    }
    public function getUbicacion(){
        return $this->Ubicacion;
    }
    public function getUsuarioIngreso(){
        return $this->UsuarioIngreso;
    }
    public function getComentariosIngreso(){
        return $this->ComentariosIngreso;
    }

    /**
     * Método que retorna el array con los ingresos al depósito para
     * el laboratorio del usuario activo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function getIngresos(){

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_ingresos.idingreso AS idingreso,
                            diagnostico.v_ingresos.descripcion AS descripcion,
                            diagnostico.v_ingresos.importe AS importe,
                            diagnostico.v_ingresos.remito AS remito,
                            diagnostico.v_ingresos.cantidad AS cantidad,
                            diagnostico.v_ingresos.vencimiento AS vencimiento,
                            diagnostico.v_ingresos.usuario AS usuario
                     FROM diagnostico.v_ingresos
                     WHERE diagnostico.v_ingresos.idlaboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.v_ingresos.id_departamento = '$this->IdDepartamento' 
                     ORDER BY diagnostico.v_ingresos.descripcion;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $ingresos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $ingresos;

    }

    /**
     * Método que retorna el array con los ingresos al depósito para
     * el laboratorio actual en las fechas que recibe como parámetro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $fechainicial - fecha de inicio del reporte
     * @param string $fechafinal - fecha de finalización del reporte
     * @return array
     */
    public function getIngresosFecha($fechainicial, $fechafinal){

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_ingresos.idingreso AS idingreso,
                            diagnostico.v_ingresos.descripcion AS descripcion,
                            diagnostico.v_ingresos.codigosop AS sop,
                            diagnostico.v_ingresos.importe AS importe,
                            diagnostico.v_ingresos.remito AS remito,
                            diagnostico.v_ingresos.cantidad AS cantidad,
                            diagnostico.v_ingresos.vencimiento AS vencimiento,
                            diagnostico.v_ingresos.usuario AS usuario
                     FROM diagnostico.v_ingresos
                     WHERE diagnostico.v_ingresos.idlaboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.v_ingresos.id_departamento = '$this->IdDepartamento' AND
                           STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') >= STR_TO_DATE('$fechainicial', '%d/%m/%Y') AND 
                           STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') <= STR_TO_DATE('$fechafinal', '%d/%m/%Y')
                     ORDER BY diagnostico.v_ingresos.descripcion;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $ingresos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $ingresos;

    }

    /**
     * Método que recibe como parámetros la fecha inicial y la fecha final 
     * y retorna el vector con los ingresos agrupados por artículo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $fechainicio - fecha inicial
     * @param string $fechafinal - fecha final del reporte
     * @return array
     */
    public function conciliadoIngresos($fechainicio, $fechafinal){

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_ingresos.descripcion AS descripcion,
                            diagnostico.v_ingresos.codigosop AS sop,
                            SUM(diagnostico.v_ingresos.cantidad) AS cantidad
                     FROM diagnostico.v_ingresos
                     WHERE diagnostico.v_ingresos.idlaboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.v_ingresos.id_departamento = '$this->IdDepartamento' AND
                           STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') >= STR_TO_DATE('$fechainicio', '%d/%m/%Y') AND 
                           STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') <= STR_TO_DATE('$fechafinal', '%d/%m/%Y')
                     GROUP BY diagnostico.v_ingresos.iditem
                     ORDER BY diagnostico.v_ingresos.descripcion;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $ingresos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $ingresos;

    }

    /**
     * Método que recibe como parámetro la clave de un ingreso y
     * asigna en las variables de clase los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idingreso - clave del ingreso
     */
    public function getDatosIngreso($idingreso){

        // inicializamos las variables
        $iditem = 0;
        $descripcion = "";
        $imagen = "";
        $importe = "";
        $remito = "";
        $cantidad = 0;
        $id_financiamiento = 0;
        $financiamiento = "";
        $vencimiento = "";
        $lote = "";
        $ubicacion = "";
        $fecha_ingreso = "";
        $usuario = "";
        $comentarios = "";

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_ingresos.iditem AS iditem,
                            diagnostico.v_ingresos.descripcion AS descripcion,
                            diagnostico.v_ingresos.imagen AS imagen,
                            diagnostico.v_ingresos.idingreso AS idingreso,
                            diagnostico.v_ingresos.importe AS importe,
                            diagnostico.v_ingresos.remito AS remito,
                            diagnostico.v_ingresos.cantidad AS cantidad,
                            diagnostico.v_ingresos.id_financiamiento AS id_financiamiento,
                            diagnostico.v_ingresos.financiamiento AS financiamiento,
                            diagnostico.v_ingresos.vencimiento AS vencimiento,
                            diagnostico.v_ingresos.lote AS lote,
                            diagnostico.v_ingresos.ubicacion AS ubicacion,
                            diagnostico.v_ingresos.fecha_ingreso AS fecha_ingreso,
                            diagnostico.v_ingresos.usuario AS usuario,
                            diagnostico.v_ingresos.comentarios AS comentarios
                     FROM diagnostico.v_ingresos
                     WHERE diagnostico.v_ingresos.idingreso = '$idingreso';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $ingreso = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro y lo asignamos a las variables de clase
        extract($ingreso);
        $this->IdItemIngreso = $iditem;
        $this->DescripcionIngreso = $descripcion;
        $this->ImagenIngreso = $imagen;
        $this->IdIngreso = $idingreso;
        $this->ImporteIngreso = $importe;
        $this->RemitoIngreso = $remito;
        $this->CantidadIngreso = $cantidad;
        $this->IdFinanciamiento = $id_financiamiento;
        $this->Financiamiento = $financiamiento;
        $this->Vencimiento = $vencimiento;
        $this->Lote = $lote;
        $this->Ubicacion = $ubicacion;
        $this->FechaIngreso = $fecha_ingreso;
        $this->UsuarioIngreso = $usuario;
        $this->ComentariosIngreso = $comentarios;

    }

    /**
     * Método que recibe como parámetro la clave de un ingreso, ejecuta  la
     * consulta de eliminación del mismo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idingreso - clave del registro a eliminar
     */
    public function borraIngreso($idingreso){

        // componemos la consulta
        $consulta = "DELETE FROM diagnostico.ingresos
                     WHERE diagnostico.ingresos.id = '$idingreso';";
        $this->Link->exec($consulta);

    }

    /**
     * Método que ejecuta la consulta de grabación de un ingreso,
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idingreso - clave del registro insertado / editado
     */
    public function grabaIngreso(){

        // si está insertando
        if ($this->IdIngreso == 0){

            // llama la consulta de inserción
            $this->nuevoIngreso();

        // si está declarada la clave
        } else {

            // llama la consulta de edición
            $this->editaIngreso();

        }

        // retornamos la clave
        return $this->IdIngreso;

    }

    /**
     * Método llamado en un nuevo ingreso, ejecuta la consulta de inserción
     * y obtiene la clave del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoIngreso(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.ingresos
                            (item,
                             importe,
                             id_laboratorio,
                             departamento,
                             id_financiamiento,
                             remito,
                             cantidad,
                             vencimiento,
                             lote,
                             ubicacion,
                             id_usuario,
                             comentarios)
                            VALUES
                            (:item,
                             :importe,
                             :id_laboratorio,
                             :departamento,
                             :id_financiamiento,
                             :remito,
                             :cantidad,
                             STR_TO_DATE(:vencimiento, '%d/%m/%Y'),
                             :lote,
                             :ubicacion,
                             :id_usuario,
                             :comentarios)";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":item", $this->IdItemIngreso);
        $psInsertar->bindParam(":importe", $this->ImporteIngreso);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":departamento", $this->IdDepartamento);
        $psInsertar->bindParam(":id_financiamiento", $this->IdFinanciamiento);
        $psInsertar->bindParam(":remito", $this->RemitoIngreso);
        $psInsertar->bindParam(":cantidad", $this->CantidadIngreso);
        $psInsertar->bindParam(":vencimiento", $this->Vencimiento);
        $psInsertar->bindParam(":lote", $this->Lote);
        $psInsertar->bindParam(":ubicacion", $this->Ubicacion);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":comentarios", $this->ComentariosIngreso);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtenemos la id del registro
        $this->IdIngreso = $this->Link->lastInsertId();

    }

    /**
     * Método llamado en la edición de un ingreso
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaIngreso(){

        // componemos la consulta de edición
        $consulta = "UPDATE diagnostico.ingresos SET
                            item = :item,
                            importe = :importe,
                            id_financiamiento = :id_financiamiento,
                            remito = :remito,
                            cantidad = :cantidad,
                            vencimiento = STR_TO_DATE(:vencimiento, '%d/%m/%Y'),
                            lote = :lote,
                            ubicacion = :ubicacion,
                            id_usuario = :id_usuario,
                            comentarios = :comentarios
                     WHERE diagnostico.ingresos.id = :idingreso; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":item", $this->IdItemIngreso);
        $psInsertar->bindParam(":importe", $this->ImporteIngreso);
        $psInsertar->bindParam(":id_financiamiento", $this->IdFinanciamiento);
        $psInsertar->bindParam(":remito", $this->RemitoIngreso);
        $psInsertar->bindParam(":cantidad", $this->CantidadIngreso);
        $psInsertar->bindParam(":vencimiento", $this->Vencimiento);
        $psInsertar->bindParam(":lote", $this->Lote);
        $psInsertar->bindParam(":ubicacion", $this->Ubicacion);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":comentarios", $this->ComentariosIngreso);
        $psInsertar->bindParam(":idingreso", $this->IdIngreso);

        // ejecutamos la edición
        $psInsertar->execute();

    }

}