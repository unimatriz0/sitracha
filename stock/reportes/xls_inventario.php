<?php

/**
 *
 * stock/reportes/xls_inventario.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (11/02/2019)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que genera el excel con el inventario en el
 * depósito
 *
*/

// incluimos e instanciamos las clases
require_once ("../inventario/inventario.class.php");
require_once ("../../clases/phpexcel/PHPExcel.php");
$hoja = new PHPExcel();
$inventario = new Inventario();

// obtenemos la nómina artículos declarados
$nomina = $inventario->getInventario();

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Control de Stock")
					  ->setSubject("Inventario en Depósito")
					  ->setDescription("Artículos en Depósito")
					  ->setKeywords("SiTraCha")
					  ->setCategory("Reportes");

// contador de filas
$fila = 8;

// fijamos el estilo del título
$estilo = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 12,
        'name'  => 'Verdana'
    ));

// fijamos el estilo de los encabezados
$encabezado = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 10,
        'name'  => 'Verdana'
    ));

// leemos la plantilla
$hoja = PHPExcel_IOFactory::load("../../clases/phpexcel/plantilla.xls");

// establecemos el ancho de las columnas
$hoja->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('B')->setWidth(75);
$hoja->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$hoja->getActiveSheet()->getColumnDimension('G')->setWidth(20);

// presenta el título y el laboratorio
$hoja->setActiveSheetIndex(0)
    ->setCellValue('B3', 'Sistema de Trazabilidad y Diagnóstico');
$hoja->setActiveSheetIndex(0)
    ->setCellValue('B5', 'Inventario en Existencia');

// centramos las celdas de los títulos
$hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// fijamos el estilo
$hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('B7')->applyFromArray($estilo);

// unimos las celdas
$hoja->getActiveSheet()->mergeCells('B3:F3');
$hoja->getActiveSheet()->mergeCells('B5:F5');
$hoja->getActiveSheet()->mergeCells('B7:F7');

// establecemos la fuente
$hoja->getDefaultStyle()->getFont()->setName('Arial')
        ->setSize(10);

// renombramos la hoja
$hoja->getActiveSheet()->setTitle('Inventario');

// incrementamos el contador
$fila++;

// presenta los encabezados
titulosColumna();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila, $iditem);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('B' . $fila, $descripcion);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('C' . $fila, $sop);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('D' . $fila, $critico);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('E' . $fila, $existencia);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('F' . $fila, $valor);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('G' . $fila, $existencia * $valor);

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa la fila
    $fila++;

}

// fijamos la primer hoja como activa para abrirla predeterminada
$hoja->setActiveSheetIndex(0);

// creamos el writer y lo dirigimos al navegador en formato 2007
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="inventario.xlsx"');
header('Cache-Control: max-age=0');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
$Writer->save('php://output');

/**
 * Función que presenta los títulos de columna
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
function titulosColumna(){

    // declaración de variables globales
    global $hoja, $fila, $encabezado;

    // incrementamos la fila
    $fila++;

    // presentamos los encabezados
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'Clave');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('B' . $fila, 'Descripción');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('C' . $fila, 'Sop');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('D' . $fila, 'Crítico');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('E' . $fila, 'Existencia');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('F' . $fila, 'Importe');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('G' . $fila, 'Total');

    // fijamos el formato
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($encabezado);

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('F' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa la fila
    $fila++;

}

?>