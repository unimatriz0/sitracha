/*
    Nombre: reportes.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 10/02/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de reportes de stock

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de reportes de
 * control de stock
 */
class Reportes {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // inicializamos las variables
        this.initReportes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initReportes(){

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de selección de
     * tipo de reporte
     */
    nominaReportes(){

        // cargamos el formulario
        $("#form_stock").load("stock/reportes/form_reportes.html");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón generar que verifica
     * los datos del formulario antes de llamar la generación
     */
    validarReporte(){

        // declaración de variables
        var mensaje;

        // si no seleccionó el tipo de reporte
        if (document.reportes_stock.tiporeporte.value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el tipo de reporte a generar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si el reporte es de ingresos o egresos
        var tipo = document.reportes_stock.tiporeporte.value;
        if (tipo == "ingresos" || tipo == "egresos" || tipo == "conciliadoingresos" || tipo == "conciliadoegresos"){

            // si no indicó la fecha de inicio
            if (document.getElementById("inicio_reporte").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Seleccione la fecha inicial a reportar";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

            // si no indicó la fecha de finalización
            if (document.getElementById("fin_reporte").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Seleccione la fecha final a reportar";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // si llegó hasta aquí genera el reporte
        this.generarReporte();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que según el valor seleccionado genera el tipo
     * de reporte
     */
    generarReporte(){

        // asignamos las variables
        var tipo = document.reportes_stock.tiporeporte.value;
        var inicio = document.getElementById("inicio_reporte").value;
        var fin = document.getElementById("fin_reporte").value;
        var url = "";

        // según el tipo de reporte a generar compone
        // la url de destino
        if (tipo == "ingresos"){
            url = "stock/reportes/xls_ingresos.php?inicio="+inicio+"&fin="+fin;
        } else if (tipo == "conciliadoingresos"){
            url = "stock/reportes/xls_conc_ingresos.php?inicio="+inicio+"&fin="+fin;
        } else if (tipo == "egresos"){
            url = "stock/reportes/xls_egresos.php?inicio="+inicio+"&fin="+fin;
        } else if (tipo == "conciliadoegresos"){
            url = "stock/reportes/xls_conc_egresos.php?inicio="+inicio+"&fin="+fin;
        } else if (tipo == "critico"){
            url = "stock/reportes/xls_critico.php";
        } else if (tipo == "articulos"){
            url = "stock/reportes/xls_articulos.php";
        } else if (tipo == "inventario"){
            url = "stock/reportes/xls_inventario.php";
        }

        // redireccionamos el navegador
        window.location = url;

    }

}