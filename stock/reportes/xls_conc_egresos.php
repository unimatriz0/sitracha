<?php

/**
 *
 * stock/reportes/xls_conc_egresos.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.4.0 (15/02/2019)
 * @copyright   Copyright (c) 2018, INP
 *
 * Procedimiento que genera el excel con la nómina de egresos al
 * depósito en las fechas que recibe por get y agrupados por
 * artículo
 *
*/

// incluimos e instanciamos las clases
require_once ("../egresos/egresos.class.php");
require_once ("../../clases/phpexcel/PHPExcel.php");
$hoja = new PHPExcel();
$egresos = new Egresos();

// obtenemos la nómina artículos críticos
$nomina = $egresos->conciliadoEgresos()($_GET["inicio"], $_GET["fin"]);

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Control de Stock")
					  ->setSubject("Egresos del Depósito")
					  ->setDescription("Nómina de Artículos Entregados")
					  ->setKeywords("SiTraCha")
					  ->setCategory("Reportes");

// contador de filas
$fila = 8;

// fijamos el estilo del título
$estilo = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 12,
        'name'  => 'Verdana'
    ));

// fijamos el estilo de los encabezados
$encabezado = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 10,
        'name'  => 'Verdana'
    ));

// leemos la plantilla
$hoja = PHPExcel_IOFactory::load("../../clases/phpexcel/plantilla.xls");

// establecemos el ancho de las columnas
$hoja->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$hoja->getActiveSheet()->getColumnDimension('B')->setWidth(75);
$hoja->getActiveSheet()->getColumnDimension('C')->setWidth(15);

// presenta el título y el laboratorio
$hoja->setActiveSheetIndex(0)
    ->setCellValue('B3', 'Sistema de Trazabilidad y Diagnóstico');
$hoja->setActiveSheetIndex(0)
    ->setCellValue('B5', 'Conciliado de Artículos Entregados entre el ' . $_GET["inicio"] . " y el " . $_GET["fin"]);

// centramos las celdas de los títulos
$hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// fijamos el estilo
$hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('B7')->applyFromArray($estilo);

// unimos las celdas
$hoja->getActiveSheet()->mergeCells('B3:F3');
$hoja->getActiveSheet()->mergeCells('B5:F5');
$hoja->getActiveSheet()->mergeCells('B7:F7');

// establecemos la fuente
$hoja->getDefaultStyle()->getFont()->setName('Arial')
        ->setSize(10);

// renombramos la hoja
$hoja->getActiveSheet()->setTitle('Egresos');

// incrementamos el contador
$fila++;

// presenta los encabezados
titulosColumna();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // presentamos el registro
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila, $sop);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('B' . $fila, $descripcion);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('C' . $fila, $cantidad);

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa la fila
    $fila++;

}

// fijamos la primer hoja como activa para abrirla predeterminada
$hoja->setActiveSheetIndex(0);

// creamos el writer y lo dirigimos al navegador en formato 2007
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="egresos.xlsx"');
header('Cache-Control: max-age=0');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
$Writer->save('php://output');

/**
 * Función que presenta los títulos de columna
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
function titulosColumna(){

    // declaración de variables globales
    global $hoja, $fila, $encabezado;

    // incrementamos la fila
    $fila++;

    // presentamos los encabezados
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('A' . $fila, 'SOP');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('B' . $fila, 'Descripción');
    $hoja->setActiveSheetIndex(0)
          ->setCellValue('C' . $fila, 'Cantidad');

    // fijamos el formato
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa la fila
    $fila++;

}

?>