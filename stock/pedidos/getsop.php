<?php

/**
 *
 * stock/pedidos/getsop.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (08/02/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un item y el código Sop
 *
*/

 // incluimos e instanciamos las clases
 require_once ("../articulos/items.class.php");
 $items = new Items();

 // obtenemos el registro
 $items->getDatosItem($_GET["iditem"]);

 // retornamos el registro
 echo json_encode(array("Sop" => $items->getSop()));

?>