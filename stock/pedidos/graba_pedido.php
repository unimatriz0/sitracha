<?php

/**
 *
 * stock/pedidos/grabapedido.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del pedido y ejecuta
 * la consulta en el servidor, retorna el resultado de la
 * operación
 *
*/

// incluimos e instanciamos las clases
require_once("pedidos.class.php");
$pedidos = new Pedidos();

// si recibió la id del pedido
if (!empty($_POST["IdPedido"])){
    $pedidos->setIdPedido($_POST["IdPedido"]);
}

// si recibió la id del item
if (!empty($_POST["IdItem"])){
    $pedidos->setIdItem($_POST["IdItem"]);

}

// asigna la cantidad
$pedidos->setCantidad($_POST["Cantidad"]);

// grabamos el registro
$resultado = $pedidos->grabaPedido();

// retornamos el resultado de la operación
echo json_encode(array("Error" => $resultado));

?>