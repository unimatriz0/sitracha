<?php

/**
 *
 * stock/pedidos/borra_pedido.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un pedido y ejecuta la
 * consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once("pedidos.class.php");
$pedidos = new Pedidos();

// eliminamos el registro
$pedidos->anulaPedido($_GET["pedido"]);

echo json_encode(array("Error" => "1"));

?>