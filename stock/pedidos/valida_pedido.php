<?php

/**
 *
 * stock/pedidos/valida_pedido.php
 *
 * @package     Diagnostico
 * @subpackage  Sexos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un item, verifica que el usuario
 * actual no halla solicitado el mismo item, y retorna el número de
 * registros encontrados, utilizado para evitar el requerimiento
 * repetido de artículos
 *
*/

// incluimos e instanciamos las clases
require_once("pedidos.class.php");
$pedidos = new Pedidos();

// obtenemos el número de registros
$registros = $pedidos->verificaPedido($_GET["item"]);

// retornamos los registros hallados
echo json_encode(array("Registros" => $registros));

?>