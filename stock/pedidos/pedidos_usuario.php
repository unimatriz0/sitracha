<?php

/**
 *
 * stock/pedidos/pedidos_usuario.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (08/02/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con los pedidos pendientes
 * del usuario actual
 *
*/

// incluimos e instanciamos las clases
require_once("pedidos.class.php");
$pedidos = new Pedidos();

// inicializamos el array
$jsondata = array();

// obtenemos el vector
$pendientes = $pedidos->nominaPedidos();

// recorremos el vector
foreach($pendientes AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al vector
    $jsondata[] = array("Id" =>          $id_pedido,
                        "Descripcion" => $descripcion,
                        "CodigoSop"   => $codigosop,
                        "Cantidad"    => $cantidad,
                        "FechaAlta"   => $fecha_alta);

}

// retornamos el vector
echo json_encode(($jsondata));
