<?php

/**
 *
 * stock/pedidos/form_aprobar.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta la nómina de pedidos pendientes de aprobación
 *
*/

// incluimos e instanciamos las clases
require_once("pedidos.class.php");
$pedidos = new Pedidos();

// presentamos el título
echo "<h2>Pedidos pendientes de aprobación</h2>";

// definimos la tabla
echo "<table width='80%' align='center' border='0'>";

// define los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Descripción</th>";
echo "<th>Sop</th>";
echo "<th align='left'>Usuario</th>";
echo "<th>Cantidad</th>";
echo "<th>Existencia</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// define el cuerpo de la tabla
echo "<tbody>";

// obtenemos la nómina de pedidos pendientes
$pendientes = $pedidos->pedidosPendientes();

// recorremos el vector
foreach($pendientes AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>$descripcion</td>";
    echo "<td align='center'>$codigosop</td>";
    echo "<td>$usuario</td>";

    // permite editar la cantidad
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Cantidad Solicitada'>";
    echo "<input type='number'
                 id='cantidad_$id_pedido'
                 name='cantidad_$id_pedido'
                 value=$cantidad
                 class='numero'>";
    echo "</td>";
    echo "</td>";

    // presentamos el resto
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Existencia en Depósito'>";
    echo "<input type='text'
                 name='existencia_$id_pedido'
                 id='existencia_$id_pedido'
                 value='$existencia'
                 size='8'
                 readonly>";
    echo "</span>";
    echo "</td>";

    // presentamos la fecha
    echo "<td align='center'>$fecha_alta</td>";

    // presentamos el botón entregar
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaEntrega'
           id='btnGrabaEntrega'
           onClick='pedidos.entregarPedido($id_pedido)'
           title='Pulse para aprobar la entrega'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}
// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>
