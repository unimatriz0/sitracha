/*
    Nombre: pedidos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 08/03/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de pedidos de materias

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre los pedidos
 * de materiales del depósito
 */
class Pedidos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // inicializamos las variables
        this.initPedidos();

        // el layer que usamos para los mensajes
        this.layerEmergente = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initPedidos(){

        // inicializamos las variables
        this.IdPedido = 0;
        this.IdItem = 0;
        this.Descripcion = "";
        this.CodigoSop = "";
        this.Imagen = "";
        this.IdUsuario = 0;
        this.Usuario = "";
        this.Cantidad = 0;
        this.FechaAlta = "";

        // el switch de verificación
        this.pedidoCorrecto = true;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que muestra los pedidos de material del usuario activo y
     * permite realizar nuevos o editarlos
     */
    hacerPedidos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el documento
        $("#form_stock").load("stock/pedidos/form_pedidos.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} iditem - clave del item
     * Método llamado seleccionar un elemento del autocomplete
     * que obtiene el código sop del artículo y lo carga en
     * el formulario de nuevo pedido
     */
    selItem(iditem){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos la id en el formulario
        document.getElementById("id_nuevo").value = iditem;

        // lo llamamos sincrónico
        $.ajax({
            url: "stock/pedidos/getsop.php?iditem=" + iditem,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // asignamos en el formulario
                document.getElementById("sop_nuevo").value = data.Sop;

            }

        });

        // seteamos el foco
        document.getElementById("cantidad_nuevo").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpedido - clave del registro
     * Método llamado desde el formulario de pedidos del usuario, verifica
     * los datos del registro y luego llama la consulta de actualización
     * recibe como parámetro la id del registro (a partir del cual
     * construye la id de los elementos), si no recibe ninguno asume que
     * es un alta
     */
    verificaPedido(idpedido){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no recibió la id
        if (typeof(idpedido) == "undefined"){

            // se trata de un alta

            // verifica halla seleccionado un item
            if (document.getElementById("id_nuevo").value == ""){

                // presenta el mensaje y retorna
                mensaje = "El item no está seleccionado correctamente";
                new jBox('Notice', { content: mensaje, color: 'red' });
                document.getElementById("id_nuevo").focus();
                return false;

            // si seleccionó
            } else {

                // asigna en las variables de clase
                this.IdItem = document.getElementById("id_nuevo").value;

            }

            // verifica halla indicado la cantidad
            if (document.getElementById("cantidad_nuevo").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Debe ingresar la cantidad pedida";
                new jBox('Notice', { content: mensaje, color: 'red' });
                document.getElementById("cantidad_nuevo").focus();
                return false;

            // si seleccionó
            } else {

                // asigna en la variable de clase
                this.Cantidad = document.getElementById("cantidad_nuevo").value;

            }

            // verificamos por callback que no esté repitiendo
            // el pedido
            this.validarPedido(function(pedidoCorrecto){

                // si está repetido
                if (!pedidoCorrecto){

                    // presenta el mensaje
                    mensaje = "Ya se encuentra declarado un\n";
                    mensaje += "pedido por el mismo artículo";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

            // graba el registro
            this.grabarPedido();

        // si está editando
        } else {

            // asigna la id del pedido
            this.IdPedido = idpedido;

            // verifica la cantidad
            if (document.getElementById("cantidad_" + this.IdPedido).value == ""){

                // presenta el mensaje y retorna
                mensaje = "Debe ingresar la cantidad pedida";
                new jBox('Notice', { content: mensaje, color: 'red' });
                document.getElementById("idpedido").focus();
                return false;

            }

            // asigna la cantidad
            this.Cantidad = document.getElementById("cantidad_" + this.IdPedido).value;

            // graba el registro
            this.grabarPedido();

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado antes de grabar el pedido y verifica que el usuario
     * no esté solicitando nuevamente el mismo item
     */
    validarPedido(pedidoCorrecto){

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/pedidos/valida_pedido.php?item=" + this.IdItem,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si no está repetido
                if (data.Registros == 0) {

                    // asignamos en la variable
                    pedidos.pedidoCorrecto = true;

                // si hay registros
                } else {

                    // asignamos en la variable
                    pedidos.pedidoCorrecto = false;

                }

            }
        });

        // retornamos el estado
        pedidoCorrecto(this.pedidoCorrecto);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el pedido, actualiza la base de
     * datos y recarga el formulario
     */
    grabarPedido(){

        // si hubo un error en el pedido
        if (!this.pedidoCorrecto){
            return;
        }

        // creamos el formulario
        var datosPedido = new FormData();

        // si está editando
        if (this.IdPedido != ""){
            datosPedido.append("IdPedido", this.IdPedido);
        }

        // si está realizando un pedido nuevo
        if (this.IdItem != ""){
            datosPedido.append("IdItem", this.IdItem);
        }

        // agregamos la cantidad
        datosPedido.append("Cantidad", this.Cantidad);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "stock/pedidos/graba_pedido.php",
            data: datosPedido,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {

                // si ocurrió un error
                if (data.Error == 0) {

                    // presenta el mensaje
                    new jBox('Notice', { content: "Ha ocurrido un error", color: 'red' });

                } else {

                    // presenta el mensaje
                    new jBox('Notice', { content: "Registro grabado ... ", color: 'green' });

                    // inicializamos las variables de clase
                    pedidos.initPedidos();

                    // recargamos la grilla
                    pedidos.limpiaGrillaPedidos();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpedido - clave del pedido
     * Método llamado al usuario eliminar su propio pedido, ejecuta la
     * consulta en el servidor y recarga el formulario, recibe como
     * parámetro la clave del pedido a eliminar
     */
    borraPedido(idpedido){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                           animation: 'flip',
                           title: 'Eliminar Solicitud',
                           closeOnEsc: false,
                           closeOnClick: false,
                           closeOnMouseleave: false,
                           closeButton: true,
                           onCloseComplete: function(){
                               this.destroy();
                           },
                           overlay: false,
                           content: 'Está seguro que desea eliminar la solicitud?',
                           theme: 'TooltipBorder',
                           confirm: function() {

                                // lo llamamos asincrónico
                                $.ajax({
                                    url: "stock/pedidos/borra_pedido.php?pedido=" + idpedido,
                                    type: "GET",
                                    cahe: false,
                                    contentType: false,
                                    processData: false,
                                    dataType: 'json',
                                    success: function (data) {

                                        if (data.Error != 0){

                                            // presenta el mensaje
                                            new jBox('Notice', { content: "Solicitud eliminada correctamente .... ", color: 'red' });

                                            // reiniciamos las variables de clase
                                            pedidos.initPedidos();

                                            // recargamos la grilla
                                            pedidos.limpiaGrillaPedidos();

                                        }

                                    }

                                });

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'
        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que muestra la nómina de pedidos y permite aprobarlos o
     * rechazarlos
     */
    aprobarPedidos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el documento
        $("#form_stock").load("stock/pedidos/form_aprobar.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpedido - clave del registro
     * Método llamado en el formulario de entrega de pedidos, recibe la
     * id del pedido y verifica el formulario, luego entrega el material
     */
    entregarPedido(idpedido){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;
        var cantidad = document.getElementById("cantidad_" + idpedido).value;
        var existentes = document.getElementById("existencia_" + idpedido).value;

        // verificamos que no esté entregando demás
        if (parseInt(cantidad) > parseInt(existentes)){

            // presenta el mensaje y retorna
            mensaje = "No hay unidades suficientes para <br>";
            mensaje += "satisfacer la solicitud.";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("cantidad_" + idpedido).focus();
            return false;

        }

        // asignamos en la variable de clase y grabamos
        this.IdPedido = idpedido;
        this.Cantidad = cantidad;
        this.grabarSalida();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al aprobar un pedido, actualiza la base de datos
     * y luego permite imprimir el remito de salida
     */
    grabarSalida(){

        // grabamos el registro
         $.ajax({
            url: "stock/pedidos/entrega_pedidos.php?pedido=" + this.IdPedido + "&cantidad="+this.Cantidad,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si no está repetido
                if (data.Id == 0) {

                    // presenta el mensaje de error
                    new jBox('Notice', { content: "Ha ocurrido un error ... ", color: 'red' });

                // si hay registros
                } else {

                    // presenta el mensaje
                    new jBox('Notice', { content: "Entrega generada", color: 'green' });

                    // pide confirmación para imprimir el remito
                    pedidos.confirmaRemito(data.Id);

                    // reinicia las variables de clase
                    pedidos.initPedidos();

                    // eliminamos la fila
                    $("#fila" + pedidos.IdPedido).remove();

                }

            }

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idegreso - clave del registro
     * Método llamado luego de generar la entrega que pide confirmación para imprimir
     * el remito
     */
    confirmaRemito(idegreso){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
                           animation: 'flip',
                           title: 'Imprimir Remito',
                           closeOnEsc: false,
                           closeOnClick: false,
                           closeOnMouseleave: false,
                           closeButton: true,
                           onCloseComplete: function(){
                               this.destroy();
                           },
                           overlay: false,
                           content: 'Desea imprimir el remito de entrega?',
                           theme: 'TooltipBorder',
                           confirm: function() {

                                // aquí llamamos la impresión
                                egresos.remitoEgreso(idegreso);

                                // elimina el cuadro de confirmación
                                Confirmacion.destroy();

                            },
                            cancel: function(){
                                Confirmacion.destroy();
                            },
                            confirmButton: 'Aceptar',
                            cancelButton: 'Cancelar'
        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene los datos de los pedidos realizados
     * por el usuario actual
     */
    pedidosUsuario(){

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/pedidos/pedidos_usuario.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si hay pedidos
                if (data.length != 0) {

                    // le pasamos el array
                    pedidos.grillaPedidosUsuario(data);

                }

            }

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} - vector con los pedidos del usuario
     * Método que recibe como parámetro el vector con los
     * pedidos del usuario actual, agrega las filas a la
     * tabla de pedidos
     */
    grillaPedidosUsuario(datos){

        // declaramos las variables
        var texto;

        for(var i=0; i < datos.length; i++){

            // abre la fila
            texto = "<tr>";

            // presenta la descripción
            texto += "<td>" + datos[i].Descripcion + "</td>";

            // presenta el código sop
            texto += "<td>" + datos[i].CodigoSop + "</td>";

            // pide la cantidad
            texto += "<td align='center'>";
            texto += "<span class='tooltip' title='Cantidad Solicitada'>";
            texto += "<input type='number' name='cantidad_" + datos[i].Id + "' ";
            texto += "id='cantidad_" + datos[i].Id + "' class='numero' ";
            texto += "value='" + datos[i].Cantidad + "'>";
            texto += "</span>";
            texto += "</td>";

            // presenta la fecha
            texto += "<td align='center'>" + datos[i].FechaAlta + "</td>";

            // presenta el botón editar con el enlace
            texto += "<td align='center'>";
            texto += "<input type='button' name='btnGrabaPedido' id='btnGrabaPedido' ";
            texto += "onClick='pedidos.verificaPedido(" + datos[i].Id + ")' ";
            texto += "title='Pulse para grabar el registro' class='botongrabar'>";
            texto += "</td>";

            // presenta el botón borrar
            texto += "<td align='center'>";
            texto += "<input type='button' name='btnBorraPedido' id='btnBorrarPedido' ";
            texto += "onClick='pedidos.borraPedido(" + datos[i].Id + ")' ";
            texto += "title='Pulse para eliminar el pedido' class='botonborrar'>";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos la fila al dom de la tabla
            $("#cuerpopedidos").append( $(texto));

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de grabar el pedido que limpia
     * la grilla de pedidos, luego la recarga y finalmente
     * limpia el formulario de nuevo pedido
     */
    limpiaGrillaPedidos(){

        // limpiamos el formulario de nuevo pedido
        document.getElementById("item_nuevo").value = "";
        document.getElementById("id_nuevo").value = "";
        document.getElementById("sop_nuevo").value = "";
        document.getElementById("cantidad_nuevo").value = "";
        document.getElementById("alta_pedido").value = fechaActual();

        // limpiamos la grilla (usamos empty que remueve los hijos)
        $("#cuerpopedidos").empty();

        // la recargamos
        this.pedidosUsuario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga la grilla de pedidos pendientes, ejecuta
     * la consulta por ajax y luego le pasa el array al
     * método javascript
     */
    cargaPedidos(){

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/pedidos/pedidos_pendientes.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si hay pedidos
                if (data.length != 0) {

                    // le pasamos el array
                    pedidos.grillaPedidos(data);

                }

            }

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} - vector con los datos
     * Método que recibe como parámetro el array con pedidos
     * pendientes y arma la grilla
     */
    grillaPedidos(datos){

        // definimos las variables
        var texto;

        // recorremos el vector
        for(var i=0; i < datos.length; i++){

            // abrimos la fila
            texto = "<tr id='fila" + datos[i].Id + "'>";

            // presentamos el registro
            texto += "<td>" + datos[i].Descripcion + "</td>";
            texto += "<td align='center'>" + datos[i].CodigoSop + "</td>";
            texto += "<td>" + datos[i].Usuario + "</td>";

            // permite editar la cantidad
            texto += "<td align='center'>";
            texto += "<span class='tooltip' title='Cantidad Solicitada'>";
            texto += "<input type='number' id='cantidad_" + datos[i].Id + "' ";
            texto += "name='cantidad_-" + datos[i].Id + "' ";
            texto += "value= " + datos[i].Cantidad + " class='numero'>";
            texto += "</td>";

            // presentamos el resto
            texto += "<td align='center'>";
            texto += "<span class='tooltip' title='Existencia en Depósito'>";
            texto += "<input type='text' name='existencia_" + datos[i].Id + "' ";
            texto += "id='existencia_" + datos[i].Id + "' ";
            texto += "value='" + datos[i].Existencia + "' size='8' readonly>";
            texto += "</span>";
            texto += "</td>";

            // presentamos la fecha
            texto += "<td align='center'>" + datos[i].FechaAlta + "</td>";

            // presentamos el botón entregar
            texto += "<td align='center'>";
            texto += "<input type='button' name='btnGrabaEntrega' ";
            texto += "id='btnGrabaEntrega' ";
            texto += "onClick='pedidos.entregarPedido(" + datos[i].Id + ")' ";
            texto += "title='Pulse para aprobar la entrega' ";
            texto += "class='botongrabar'>";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos la fila
            $("#cuerpopendientes").append( $(texto));

        }

    }

}