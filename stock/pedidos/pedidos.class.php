<?php

/**
 *
 * Class Pedidos | stock/pedidos/pedidos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de
 * pedidos de materiales realizados por los usuarios
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Pedidos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdUsuario;             // clave del usuario activo
    protected $IdPedido;              // clave del pedido
    protected $IdItem;                // clave del item
    protected $Descripcion;           // descripcion del item
    protected $CodigoSop;             // código sop del item
    protected $Imagen;                // imagen del item
    protected $Usuario;               // nombre del usuario del pedido
    protected $Remito;                // número de remito
    protected $IdUsuarioPedido;       // clave del usuario del pedido
    protected $Cantidad;              // cantidad solicitada
    protected $FechaAlta;             // fecha de alta del pedido

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicialización de variables
        $this->IdPedido = 0;
        $this->IdItem = 0;
        $this->Descripcion = "";
        $this->CodigoSop = "";
        $this->Imagen = "";
        $this->Usuario = "";
        $this->Remito = 0;
        $this->IdUsuarioPedido = 0;
        $this->Cantidad = "";
        $this->FechaAlta = "";

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesión
        session_start();

        // si existen los valores de sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y el laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];

        // si no inició
        } else {

            // abandona por error
            echo "No hay sesión iniciada";
            exit;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos públicos de asignación de variables
    public function setIdPedido($idpedido){
        $this->IdPedido = $idpedido;
    }
    public function setIdItem($iditem){
        $this->IdItem = $iditem;
    }
    public function setDescripcion($descripcion){
        $this->Descripcion = $descripcion;
    }
    public function setIdUsuarioPedido($idusuario){
        $this->IdUsuarioPedido = $idusuario;
    }
    public function setCantidad($cantidad){
        $this->Cantidad = $cantidad;
    }

    // métodos públicos de obtención de valores
    public function getIdPedido(){
        return $this->IdPedido;
    }
    public function getIdItem(){
        return $this->IdItem;
    }
    public function getDescripcion(){
        return $this->Descripcion;
    }
    public function getCodigoSop(){
        return $this->CodigoSop;
    }
    public function getImagen(){
        return $this->Imagen;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getIdUsuarioPedido(){
        return $this->IdUsuarioPedido;
    }
    public function getRemito(){
        return $this->Remito();
    }
    public function getCantidad(){
        return $this->Cantidad;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método público que retorna los pedidos de materiales pendientes para
     * el usuario actual
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaPedidos(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_pedidos.id_pedido AS id_pedido,
                            diagnostico.v_pedidos.id_item AS id_item,
                            diagnostico.v_pedidos.descripcion AS descripcion,
                            diagnostico.v_pedidos.codigosop AS codigosop,
                            diagnostico.v_pedidos.cantidad AS cantidad,
                            diagnostico.v_pedidos.fecha_alta AS fecha_alta
                     FROM diagnostico.v_pedidos
                     WHERE diagnostico.v_pedidos.id_usuario = '$this->IdUsuario'
                     ORDER BY diagnostico.v_pedidos.descripcion;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa pasándola a minúsculas
        $pedidos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $pedidos;

    }

    /**
     * Método llamado por el gestor de stock que retorna la nómina de pedidos
     * para el laboratorio actual
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function pedidosPendientes(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_pedidos.id_pedido AS id_pedido,
                            diagnostico.v_pedidos.id_item AS id_item,
                            diagnostico.v_pedidos.descripcion AS descripcion,
                            diagnostico.v_pedidos.codigosop AS codigosop,
                            diagnostico.v_pedidos.usuario AS usuario,
                            diagnostico.v_pedidos.cantidad AS cantidad,
                            diagnostico.v_pedidos.existencia AS existencia,
                            diagnostico.v_pedidos.fecha_alta AS fecha_alta
                     FROM diagnostico.v_pedidos
                     WHERE diagnostico.v_pedidos.id_laboratorio = '$this->IdLaboratorio'
                     ORDER BY diagnostico.v_pedidos.descripcion,
                              diagnostico.v_pedidos.usuario;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa pasándola a minúsculas
        $pedidos = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $pedidos;

    }

    /**
     * Método público que ejecuta la consulta de edición o inserción
     * según corresponda, retorna la id del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaPedido(){

        // si está insertando
        if ($this->IdPedido == 0){
            $this->nuevoPedido();
        } else {
            $this->editaPedido();
        }

        // retornamos la id
        return $this->IdPedido;

    }

    /**
     * Método protegido que inserta un nuevo pedido en la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoPedido(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.pedidos
                            (id_item,
                             id_usuario,
                             cantidad)
                            VALUES
                            (:id_item,
                             :id_usuario,
                             :cantidad);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_item", $this->IdItem);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":cantidad", $this->Cantidad);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtenemos la id del registro
        $this->IdPedido = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que edita el registro de un pedido
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaPedido(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.pedidos SET
                            cantidad = :cantidad
                     WHERE diagnostico.pedidos.id = :id_pedido;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":cantidad", $this->Cantidad);
        $psInsertar->bindParam(":id_pedido", $this->IdPedido);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método llamado antes de grabar el pedido, verifica que
     * el usuario no esté pidiendo el mismo artículo y retorna
     * la cantidad de registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $iditem - clave del item solicitado
     * @return int
     */
    public function verificaPedido($iditem){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.pedidos.id) AS registros
                     FROM diagnostico.pedidos
                     WHERE diagnostico.pedidos.id_item = '$iditem' AND
                           diagnostico.pedidos.id_usuario = '$this->IdUsuario';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $pedidos = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos
        extract($pedidos);
        return $registros;

    }

    /**
     * Método público que elimina un pedido realizado por el
     * usuario actual, recibe como parámetro la clave de un
     * pedido
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int - clave del registro a eliminar
     */
    public function anulaPedido($idpedido){

        // componemos la consulta
        $consulta = "DELETE FROM diagnostico.pedidos
                     WHERE diagnostico.pedidos.id = '$idpedido';";
        $this->Link->exec($consulta);

    }

    /**
     * Método público que recibe como parámetro la clave de un pedido y
     * asigna en las variables de clase los valores del mismo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idpedido - clave del registro de pedidos
     */
    public function getDatosPedido($idpedido){

        // inicializamos las variables
        $id_pedido = 0;
        $id_item = "";
        $id_usuario = 0;
        $cantidad = 0;

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_pedidos.id_pedido AS id_pedido,
                            diagnostico.v_pedidos.id_item AS id_item,
                            diagnostico.v_pedidos.id_usuario AS id_usuario,
                            diagnostico.v_pedidos.cantidad AS cantidad
                     FROM diagnostico.v_pedidos
                     WHERE diagnostico.v_pedidos.id_pedido = '$idpedido';";
         $resultado = $this->Link->query($consulta);

         // obtenemos el registro
        $pedidos = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

         // obtenemos el registro y retornamos
         extract($pedidos);

         // ahora asignamos en las variables de clase
         $this->IdPedido = $id_pedido;
         $this->IdItem = $id_item;
         $this->IdUsuarioPedido = $id_usuario;
         $this->Cantidad = $cantidad;

    }

    /**
     * Método público que aprueba el pedido de un usuario, inserta
     * los datos en el registro de salidas del depósito y elimina
     * el registro de la tabla de pedidos pendientes, retorna la
     * clave del egreso generado (aquí siempre es un nuevo egreso)
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int - clave del pedido
     * @param int - cantidad aprobada
     * @return int
     */
    public function apruebaPedido($idpedido, $cantidad){

        // primero obtenemos los datos del pedido
        $this->getDatosPedido($idpedido);

        // si la cantidad es distinta de la solicitdad
        if ($this->Cantidad != $cantidad){

            // actualizamos la cantidad, no actualizamos la base
            // porque dentro de poco vamos a eliminar el registro
            $this->Cantidad = $cantidad;

        }

        // ahora grabamos la tabla de egresos
        $idegreso = $this->grabaEgreso();

        // ahora eliminamos el pedido
        $this->anulaPedido($idpedido);

        // retorna la clave del egreso
        return $idegreso;

    }

    /**
     * Método protegido que graba en la tabla de egresos el pedido
     * retorna la clave del egreso
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / editado
     */
    protected function grabaEgreso(){

        // generamos el nuevo remito
        $this->nuevoRemito();

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.egresos
                            (item,
                             id_laboratorio,
                             remito,
                             cantidad,
                             id_entrego,
                             id_recibio)
                            VALUES
                            (:iditem,
                             :id_laboratorio,
                             :remito,
                             :cantidad,
                             :id_entrego,
                             :id_recibio);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":iditem", $this->IdItem);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":remito", $this->Remito);
        $psInsertar->bindParam(":cantidad", $this->Cantidad);
        $psInsertar->bindParam(":id_entrego", $this->IdUsuario);
        $psInsertar->bindParam(":id_recibio", $this->IdUsuarioPedido);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtenemos la id del registro
        $idegreso = $this->Link->lastInsertId();

        // retornamos la id
        return $idegreso;

    }

    /**
     * Método protegido que obtiene el valor del nuevo remito para el mismo
     * laboratorio
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoRemito(){

        // inicializamos las variables
        $remito = "";

        // obtenemos el valor del nuevo remito
        $consulta = "SELECT MAX(diagnostico.egresos.remito) + 1 AS remito
                     FROM diagnostico.egresos
                     WHERE diagnostico.egresos.id_laboratorio = '$this->IdLaboratorio';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos
        extract($fila);

        // si es el primer remito
        if ($remito == ""){
            $this->Remito = 1;
        } else {
            $this->Remito = $remito;
        }

    }

}