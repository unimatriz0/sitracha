<?php

/**
 *
 * stock/pedidos/entrega_pedidos.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la id de un pedido y la cantidad entregad
 * y genera la entrega
 *
*/

// incluimos e instanciamos las clases
require_once("pedidos.class.php");
$pedidos = new Pedidos();

// grabamos el registro
$resultado = $pedidos->apruebaPedido($_GET["pedido"], $_GET["cantidad"]);

// retornamos el resultado de la operación
echo json_encode(array("Id" => $resultado));

?>