<?php

/**
 *
 * stock/articulos/valida_item.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (19/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la descripción de un item y
 * verifica que no se encuentre repetido para el mismo
 * laboratorio
 *
*/

// inclusión de clases
require_once("items.class.php");
$item = new Items();

// lo validamos
$resultado = $item->verificaItem($_GET["descripcion"]);

// retornamos el estado de la operación
echo json_encode(array("Error" => $resultado));

?>