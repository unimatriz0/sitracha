<?php

/**
 *
 * Class Items | stock/articulos/items.class.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones sobre las distintas tablas
 * que hacen al sistema de control de stock
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Items {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdDepartamento;        // clave del departamento
    protected $IdUsuario;             // clave del usuario activo

    // las variables de la tabla de items
    protected $IdItem;                // clave del item
    protected $Descripcion;           // descripción del item
    protected $Valor;                 // valor unitario del item
    protected $Imagen;                // cadena con la imagen
    protected $Critico;               // valor crítico
    protected $Sop;                   // código sop del item
    protected $FechaItem;             // fecha de alta del registro
    protected $IdUsuarioItem;         // clave del usuario
    protected $UsuarioItem;           // nombre del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de la tabla de items
        $this->IdItem = 0;
        $this->Descripcion = "";
        $this->Valor = 0;
        $this->Imagen = "";
        $this->Critico = 0;
        $this->Sop = "";
        $this->FechaItem = "";
        $this->IdUsuarioItem = "";
        $this->UsuarioItem = "";

        // iniciamos sesión
        session_start();

        // si existen los valores de sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y el laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];
            $this->IdDepartamento = $_SESSION["IdDepartamento"];

        // si no inició
        } else {

            // abandona por error
            echo "No hay sesión iniciada";
            exit;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setIdItem($iditem){

        // verificamos que sea un número
        if (!is_numeric($iditem)){

            // abandona por error
            echo "La clave del item debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->IdItem = $iditem;

        }

    }
    public function setDescripcion($descripcion){
        $this->Descripcion = $descripcion;
    }
    public function setValor($valor){

        // verifica que sea un número
        if (!is_numeric($valor)){

            // abandona por error
            echo "El valor unitario del item debe ser un número";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->Valor = $valor;

        }

    }
    public function setImagen($imagen){
        $this->Imagen = $imagen;
    }
    public function setCritico($critico){

        // verifica que sea un número
        if (!is_numeric($critico)){

            // abandona por error
            echo "El número crítico es incorrecto";
            exit;

        // si está correcto
        } else {

            // lo asigna
            $this->Critico = $critico;

        }

    }
    public function setSop($sop){
        $this->Sop = $sop;
    }

    // métodos de retorno de valores
    public function getIdItem(){
        return $this->IdItem;
    }
    public function getDescripcion(){
        return $this->Descripcion;
    }
    public function getValor(){
        return $this->Valor;
    }
    public function getImagen(){
        return $this->Imagen;
    }
    public function getCritico(){
        return $this->Critico;
    }
    public function getSop(){
        return $this->Sop;
    }
    public function getFechaItem(){
        return $this->FechaItem;
    }
    public function getUsuarioItem(){
        return $this->UsuarioItem;
    }

    /**
     * Método que retorna un array con los elementos del diccionario de items
     * para el laboratorio del usuario activo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $items
     */
    public function getItems(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.items.id AS iditem,
                            diagnostico.items.descripcion AS descripcion,
                            diagnostico.items.codigosop AS sop,
                            diagnostico.items.valor AS valor,
                            diagnostico.items.critico AS critico,
                            cce.responsables.usuario AS usuario
                     FROM diagnostico.items INNER JOIN cce.responsables ON diagnostico.items.id_usuario = cce.responsables.id
                     WHERE diagnostico.items.id_laboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.items.departamento = '$this->IdDepartamento'
                     ORDER BY diagnostico.items.descripcion;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la matriz completa
        $items = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la matriz
        return $items;

    }

    /**
     * Método que recibe como parámetro la clave del item y asigna en las
     * variables de clase los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int - clave del item
     */
    public function getDatosItem($iditem){

        // inicializamos las variables
        $id_item = 0;
        $descripcion = "";
        $valor = 0;
        $critico = 0;
        $codigosop = "";
        $imagen = "";
        $fecha = "";
        $usuario = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_items.id_item AS id_item,
                            diagnostico.v_items.descripcion AS descripcion,
                            diagnostico.v_items.valor AS valor,
                            diagnostico.v_items.critico AS critico,
                            diagnostico.v_items.codigosop AS codigosop,
                            diagnostico.v_items.imagen AS imagen,
                            diagnostico.v_items.fecha AS fecha,
                            diagnostico.v_items.usuario AS usuario
                     FROM diagnostico.v_items
                     WHERE diagnostico.v_items.id_item = '$iditem';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $item = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro
        extract($item);

        // lo asignamos a las variables de clase
        $this->IdItem = $id_item;
        $this->Descripcion = $descripcion;
        $this->Valor = $valor;
        $this->Critico = $critico;
        $this->Sop = $codigosop;
        $this->Imagen = $imagen;
        $this->FechaItem = $fecha;
        $this->UsuarioItem = $usuario;

    }

    /**
     * Método público que llama la consulta de inserción o edición de items
     * según corresponda, retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $iditem - clave del registro insertado / editado
     */
    public function grabaItem(){

        // si está insertando
        if ($this->IdItem == 0){

            // llama la consulta de inserción
            $this->nuevoItem();

        // si está editando
        } else {

            // llama la consulta de edición
            $this->editaItem();

        }

        // verifica si tiene que actualizar la imagen
        $this->imagenItem();

        // retornamos la clave del registro
        return $this->IdItem;

    }

    /**
     * Método protegido que ejecuta la clave de inserción de items
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoItem(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.items
                            (id_laboratorio,
                             departamento,
                             descripcion,
                             valor,
                             critico,
                             codigosop,
                             id_usuario)
                            VALUES
                            (:id_laboratorio,
                             :iddepartamento,
                             :descripcion,
                             :valor,
                             :critico,
                             :sop,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":departamento", $this->IdDepartamento);
        $psInsertar->bindParam(":descripcion", $this->Descripcion);
        $psInsertar->bindParam(":valor", $this->Valor);
        $psInsertar->bindParam(":critico", $this->Critico);
        $psInsertar->bindParam(":sop", $this->Sop);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtenemos la id del registro
        $this->IdItem = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que ejecuta la consulta de edición de items
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaItem(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.items SET
                            descripcion = :descripcion,
                            valor = :valor,
                            critico = :critico,
                            codigosop = :sop,
                            id_usuario = :id_usuario
                     WHERE diagnostico.items.id = :id_item;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":descripcion", $this->Descripcion);
        $psInsertar->bindParam(":valor", $this->Valor);
        $psInsertar->bindParam(":critico", $this->Critico);
        $psInsertar->bindParam(":sop", $this->Sop);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_item", $this->IdItem);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método protegido que actualiza la imagen del item si corresponde
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function imagenItem(){

        // si existe una imagen
        if ($this->Imagen != ""){

            // componemos la consulta
            $consulta = "UPDATE diagnostico.items SET
                                imagen = :imagen
                         WHERE diagnostico.items.id = :iditem; ";

            // asignamos la consulta
            $psInsertar = $this->Link->prepare($consulta);

            // asignamos los parámetros de la consulta
            $psInsertar->bindParam(":imagen", $this->Imagen);
            $psInsertar->bindParam(":iditem", $this->IdItem);

            // ejecutamos la edición
            $psInsertar->execute();

        }

    }

    /**
     * Método que recibe como parámetro la descripción de un item y
     * retorna true si el item está declarado para ese laboratorio
     * utilizado para evitar la repetición de descripciones
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $descripcion - descripción del artículo
     * @return boolean
     */
    public function verificaItem($descripcion){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.v_items.descripcion) AS registros
                     FROM diagnostico.v_items
                     WHERE diagnostico.v_items.descripcion = '$descripcion' AND
                           diagnostico.v_items.id_laboratorio = '$this->IdLaboratorio' AND
                           diagnostico.v_items.id_departamento = '$this->IdDepartamento'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $item = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro y retornamos
        extract($item);
        return $registros;

    }

    /**
     * Método utilizado tanto en los ingresos como en los egresos para buscar
     * el item y su clave, recibe como parámetro una cadena y retorna un
     * array con los registros coincidentes
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $item - nombre del item
     * @return array
     */
    public function buscaItem($item){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_items.id_item AS iditem,
                            diagnostico.v_items.descripcion AS descripcion,
                            diagnostico.v_items.codigosop AS codigosop,
                            diagnostico.v_items.imagen AS imagen
                     FROM diagnostico.v_items
                     WHERE diagnostico.v_items.id_laboratorio = '$this->IdLaboratorio' AND
                           diagnostico.v_items.id_departamento = '$this->IdDepartamento'
                           diagnostico.v_items.descripcion LIKE '%$item%'
                     ORDER BY diagnostico.v_items.descripcion; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $items = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $items;

    }

}