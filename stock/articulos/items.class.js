/*
    Nombre: items.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 15/12/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de stock del sistema

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones del diccionario
 * de artículos del depósito
 */
class Items {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // inicializamos las variables
        this.initItems();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase de los items
     */
    initItems(){

        // inicializamos las variables
        this.IdItem = 0;                     // clave del registro
        this.IdLaboratorio = 0;              // clave del laboratorio
        this.Descripcion = "";               // descripción del artículo
        this.Valor = 0;                      // valor unitario
        this.Critico = 0;                    // cantidad crítica
        this.Sop = "";                       // código sop de función publica
        this.FechaItem = "";                 // fecha de alta
        this.IdUsuarioItem = 0;              // clave del usuario
        this.UsuarioItem = "";               // nombre del usuario
        this.Imagen = "";                    // imágen del artículo
        this.itemCorrecto = true;            // switch de artículo repetido

    }

    // métodos de asignación de variables
    setIdItem(iditem) {
        this.IdItem = iditem;
    }
    setDescripcion(descripcion) {
        this.Descripcion = descripcion;
    }
    setValor(valor) {
        this.Valor = valor;
    }
    setCritico(critico) {
        this.Critico = critico;
    }
    setSop(sop){
        this.Sop = sop;
    }
    setFechaItem(fecha) {
        this.FechaItem = fecha;
    }
    setUsuarioItem(usuario) {
        this.UsuarioItem = usuario;
    }
    setImagen(imagen) {

        // si recibió algo
        if (imagen != "null" && imagen != null) {
            this.Imagen = imagen;
        } else {
            this.Imagen = 'imagenes/imagen_no_disponible.gif';
        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega al dom de la página los div del formulario
     * de items y la grila con el diccionario de items
     */
    Items() {

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el contenido del div
        $("#form_stock").html('');

        // agregamos al dom el formulario de items
        $("#form_stock").append("<div id='datos_items'></div>");
        $("#datos_items").load("stock/articulos/form_items.html");

        // agregamos al dom la grilla de items
        $("#form_stock").append("<div id='grilla_items'></div>");
        $("#grilla_items").load("stock/articulos/grilla_items.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente activa el onclick del input file para
     * abrir el cuadro de diálogo
     */
    cargaImagen() {

        // activamos el evento
        document.getElementById("archivo_item").click();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange del input file de items
     * si se seleccionó un archivo lo lee y adjunta a la variable
     * de clase
     */
    subirImagen() {

        // obtenemos el archivo seleccionado
        var file = $("#archivo_item")[0].files[0];

        // si canceló
        if (file === undefined) {

            // simplemente retornamos
            return false;

        }

        // usamos la nueva característica filereader que nos permite
        // cargar un archivo en el navegador y mostrarlo sin
        // subirlo al servidor, si es una imagen ya lo convierte
        // en base 64
        var reader = new FileReader();

        // evento llamado cuando termina la carga del archivo
        reader.onload = function (e) {

            // lo asignamos a la variable de clase
            var imagen = e.target.result;

            // e.target.result contents the base64 data from the image uploaded
            document.getElementById('imagen_item').src = imagen;

            // asignamos en la variable de clase porque aquí estamos
            // en un espacio privado (dentro de function)
            items.setImagen(imagen);

        };

        // definimos el elemento del formulario a leer
        var archivo = document.getElementById("archivo_item");
        reader.readAsDataURL(archivo.files[0]);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que reinicia el formulario de items
     */
    cancelaItem() {

        // reiniciamos la sesión
        sesion.reiniciar();

        // reiniciamos el formulario en blanco
        document.getElementById("id_item").value = "";
        document.getElementById("descripcion_item").value = "";
        document.getElementById("valor_item").value = "";
        document.getElementById("critico_item").value = "";
        document.getElementById("sopitem").value = "";
        document.getElementById("alta_item").value = fechaActual();
        document.getElementById("usuario_item").value = sessionStorage.getItem("Usuario");
        document.getElementById("imagen_item").src = '../../imagenes/imagen_no_disponible.gif';

        // reiniciamos las variables de clase
        this.initItems();

        // fijamos el foco
        document.getElementById("descripcion_item").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {boolean} itemCorrecto
     * @return {boolean} - si encontró el item en la base
     * Método que consulta la base por ajax para verificar que la descripción
     * no se encuentre repetida, recibe como parámetro la cadena con la
     * descripción del item
     */
    validarItem(itemCorrecto) {

        // declaración de variables
        this.itemCorrecto = false;

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/articulos/valida_item.php?descripcion=" + items.Descripcion,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function (data) {

                // si está repetido
                if (data.Error != 0) {

                    // asignamos en la variable
                    items.itemCorrecto = false;

                // si no hay registros
                } else {

                    // asignamos en la variable
                    items.itemCorrecto = true;

                }

            }
        });

        // retornamos el estado
        itemCorrecto(this.itemCorrecto);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los valores del formulario antes de enviarlo a
     * la rutina de grabación
     */
    verificaItem() {

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("id_item").value != "") {
            this.IdItem = document.getElementById("id_item").value;
        }

        // si no ingresó la descripción
        if (document.getElementById("descripcion_item").value == "") {

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la descripción del Item";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("descripcion_item").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Descripcion = document.getElementById("descripcion_item").value;

        }

        // si no ingresó el valor
        if (document.getElementById("valor_item").value == "" || document.getElementById("valor_item").value == 0) {

            // presenta el mensaje y retorna
            mensaje = "Ingrese el valor unitario del item";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("valor_item").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Valor = document.getElementById("valor_item").value;

        }

        // si no ingresó el valor crítico
        if (document.getElementById("critico_item").value == "" || document.getElementById("critico_item").value == 0) {

            // presenta el mensaje y retorna
            mensaje = "Ingrese el número crítico";
            new jBox('Notice', { content: mensaje, color: 'red' });
            document.getElementById("critico_item").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Critico = document.getElementById("critico_item").value;

        }

        // si no ingresó el código sop avisa
        if (document.getElementById("sopitem").value == ""){

            // presenta el mensaje y retorna
            mensaje = "El registro no tendrá el Código SOP del Item";
            new jBox('Notice', { content: mensaje, color: 'red' });

        // si lo ingresó
        } else {

            // lo agrega
            this.Sop = document.getElementById("sopitem").value;

        }

        // si está dando altas
        if (document.getElementById("id_item").value == "") {

            // llamamos por callbak para verificar el item
            this.validarItem(function(itemCorrecto){

                // si está repetido
                if (!itemCorrecto){

                    // presentamos el mensaje
                    mensaje = "Ese artículo ya está declarado. Verifique";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // si llegó hasta aquí
        this.grabaItem();

    }


    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos del formulario por ajax al servidor
     */
    grabaItem() {

        // si hubo algún error
        if (!this.itemCorrecto){
            return false;
        }

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos las variables
        var mensaje;

        // definimos el formulario
        var datosItem = new FormData();

        /// agregamos los elementos
        if (this.IdItem != 0) {
            datosItem.append("Id", this.IdItem);
        }
        datosItem.append("Descripcion", this.Descripcion);
        datosItem.append("Valor", this.Valor);
        datosItem.append("Critico", this.Critico);

        // si cargó una imagen
        if (this.Imagen != "") {
            datosItem.append("Imagen", this.Imagen);
        }

        // si cargó el código sop
        if (this.Sop != ""){
            datosItem.append("Sop", this.Sop);
        }

        // enviamos el formulario por ajax asincrónico
        $.ajax({
            type: "POST",
            url: "stock/articulos/graba_item.php",
            data: datosItem,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {

                // si ocurrió un error
                if (data.Id === false) {

                    // presenta el mensaje
                    mensaje = "Ha ocurrido un error";
                    new jBox('Notice', { content: mensaje, color: 'red' });

                } else {

                    // presenta el mensaje
                    mensaje = "Registro grabado ... ";
                    new jBox('Notice', { content: mensaje, color: 'green' });

                    // recargamos la grilla de items
                    items.limpiaGrilla();

                    // limpiamos el formulario
                    items.nuevoItem();


                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de items e inicializa las variables de clase
     */
    nuevoItem() {

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializamos el formulario
        document.getElementById("id_item").value = "";
        document.getElementById("descripcion_item").value = "";
        document.getElementById("valor_item").value = 0;
        document.getElementById("critico_item").value = 0;
        document.getElementById("sopitem").value = "";
        document.getElementById("alta_item").value = fechaActual();
        document.getElementById("usuario_item").value = sessionStorage.getItem("Usuario");
        document.getElementById("imagen_item").src = '../../imagenes/imagen_no_disponible.gif';

        // inicializamos las variables de clase
        this.initItems();

        // fijamos el foco
        document.getElementById("descripcion_item").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} iditem - clave del registro a obtener
     * Método que recibe como parámetro la clave de un item, carga el formulario
     * de items con los datos del registro
     */
    verItem(iditem) {

        // obtenemos el valor del registro y lo asignamos a las
        // variables de clase

        // reiniciamos la sesión
        sesion.reiniciar();

        // lo llamamos sincrónico
        $.ajax({
            url: "stock/articulos/getitem.php?iditem=" + iditem,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {

                // cargamos las variables de clase
                items.setIdItem(data.Id);
                items.setDescripcion(data.Descripcion);
                items.setValor(data.Valor);
                items.setCritico(data.Critico);
                items.setSop(data.Sop);
                items.setImagen(data.Imagen);
                items.setFechaItem(data.Fecha);
                items.setUsuarioItem(data.Usuario);

                // ahora mostramos el registro
                items.muestraItem();

            }
        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase carga los valores en
     * el formulario
     */
    muestraItem() {

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos en el formulario los valores de las variables de clase
        document.getElementById("id_item").value = this.IdItem;
        document.getElementById("descripcion_item").value = this.Descripcion;
        document.getElementById("valor_item").value = this.Valor;
        document.getElementById("critico_item").value = this.Critico;
        document.getElementById("sopitem").value = this.Sop;
        document.getElementById("alta_item").value = this.FechaItem;
        document.getElementById("usuario_item").value = this.UsuarioItem;
        document.getElementById("imagen_item").src = this.Imagen;

        // setea el foco
        document.getElementById("descripcion_item").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene la nómina de artículos y lo agrega
     * a la grilla
     */
    nominaItems(){

        // lo llamamos asincrónico
        $.ajax({
            url: "stock/articulos/lista_items.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // pasamos el array y lo agregamos
                items.cargaArticulos(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - array con el diccionario de artículos
     * Metodo que recibe como parámetro un array con el registro
     * y lo agrega como una fila al dom de la tabla
     */
    cargaArticulos(datos){

        // declaramos las variables
        var tabla = $('#nominaitems').DataTable();
        var enlace = "";

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // armamos el enlace
            enlace =  "<input type='button' ";
            enlace += "        name='btnEditar' ";
            enlace += "        class='botoneditar' ";
            enlace += "        title='Pulse para editar el registro' ";
            enlace += "        onClick='items.verItem(" + datos[i].Id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                datos[i].Descripcion,
                datos[i].Valor,
                datos[i].Critico,
                datos[i].Usuario,
                enlace
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recarga la grilla de items luego de grabar
     * un registro
     */
    limpiaGrilla(){

        // limpiamos la tabla y luego recargamos
        var tabla = $('#nominaitems').DataTable();
        tabla.clear();
        this.nominaItems();

    }

}