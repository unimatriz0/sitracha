<?php

/**
 *
 * stock/articulos/graba_item.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (19/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de un item y ejecuta
 * la consulta en el servidor, retorna el resultado de la
 * operación
 *
*/

// incluimos e instanciamos las clases
require_once("items.class.php");
$item = new Items();

// si recibió la id
if (!empty($_POST["Id"])){
    $item->setIdItem($_POST["Id"]);
}

// asignamos el resto de los valores
$item->setDescripcion($_POST["Descripcion"]);
$item->setValor($_POST["Valor"]);
$item->setCritico($_POST["Critico"]);

// si recibió el código sop
if (!empty($_POST["Sop"])){
    $item->setSop($_POST["Sop"]);
}

// si recibió la imagen
if (!empty($_POST["Imagen"])){
    $item->setImagen($_POST["Imagen"]);
}

// grabamos el registro y obtenemos el resultado
$resultado = $item->grabaItem();

// retornamos el resultado de la operación
echo json_encode(array("Id" => $resultado));
?>