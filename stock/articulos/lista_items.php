<?php

/**
 *
 * stock/articulos/lista_items.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de artículos
 *
*/

// incluimos e instanciamos las clases
require_once ("items.class.php");
$stock = new Items();

// obtenemos la nómina de items
$items = $stock->getItems();

// declaramos las variables
$jsondata = array();

// recorremos el vector
foreach ($items AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos al array
    $jsondata[] = array("Id" => $iditem,
                        "Descripcion" => $descripcion,
                        "Valor" =>       $valor,
                        "Critico" =>     $critico,
                        "Usuario" =>     $usuario);

}

// retornamos el array
echo json_encode($jsondata);
