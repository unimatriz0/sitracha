<?php

/**
 *
 * stock/articulos/getitem.php
 *
 * @package     Diagnostico
 * @subpackage  Stock
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (18/12/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un item y retorna en
 * un array json los valores del registro
 *
*/

 // incluimos e instanciamos las clases
 require_once ("items.class.php");
 $items = new Items();

 // obtenemos el registro
 $items->getDatosItem($_GET["iditem"]);

 // retornamos el registro
 echo json_encode(array("Id" =>          $items->getIdItem(),
                        "Descripcion" => $items->getDescripcion(),
                        "Valor" =>       $items->getValor(),
                        "Critico" =>     $items->getCritico(),
                        "Sop" =>         $items->getSop(),
                        "Imagen" =>      $items->getImagen(),
                        "Fecha" =>       $items->getFechaItem(),
                        "Usuario" =>     $items->getUsuarioItem()));
?>