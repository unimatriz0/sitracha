<?php

/**
 *
 * grabaholter | holter/grabaaholter.php
 *
 * @package     Diagnostico
 * @subpackage  Holter
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta en el servidor
 *
*/

// incluimos e instanciamos la clase
require_once("holter.class.php");
$holter = new Holter();

// asignamos los valores en la clase
$holter->setId($_POST["Id"]);
$holter->setPaciente($_POST["Paciente"]);
$holter->setVisita($_POST["Visita"]);
$holter->setFecha($_POST["Fecha"]);
$holter->setMedia($_POST["Media"]);
$holter->setMinima($_POST["Minima"]);
$holter->setMaxima($_POST["Maxima"]);
$holter->setLatidos($_POST["Latidos"]);
$holter->setEv($_POST["Ev"]);
$holter->setNev($_POST["Nev"]);
$holter->setTasaEv($_POST["Tasaev"]);
$holter->setEsv($_POST["Esv"]);
$holter->setNesv($_POST["Nesv"]);
$holter->setTasaEsv($_POST["Tasaesv"]);
$holter->setNormal($_POST["Normal"]);
$holter->setBradicardia($_POST["Bradicardia"]);
$holter->setRtaCronotopica($_POST["RtaCronotropica"]);
$holter->setArritmiaSevera($_POST["ArritmiaSevera"]);
$holter->setArritmiaSimple($_POST["ArritmiaSimple"]);
$holter->setArritmiaSupra($_POST["ArritmiaSupra"]);
$holter->setBdeRama($_POST["BdeRama"]);
$holter->setBav2g($_POST["Bav2g"]);
$holter->setDisociacion($_POST["Disociacion"]);

// grabamos el registro
$id = $holter->grabaHolter();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>
