<?php

/**
 *
 * Class Holter | holter/holter.class.php
 *
 * @package     Diagnostico
 * @subpackage  Holter
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * holter administrados al paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Holter {

    // definimos las variables
    protected $Link;                  // puntero a la base de datos
    protected $Id;                    // clave del registro
    protected $Paciente;              // clave del paciente
    protected $Visita;                // clave de la visita
    protected $Fecha;                 // fecha de la toma
    protected $Media;                 // frecuencia cardíaca media
    protected $Minima;                // frecuencia cardíaca mínima
    protected $Maxima;                // frecuencia cardíaca máxima
    protected $Latidos;               // número de latidos totales
    protected $Ev;                    // si hubo eventos 0 no - 1 si
    protected $Nev;                   // número de eventos
    protected $Tasaev;                // tasa de eventos
    protected $Esv;                   // eventos extra entriculares 0 no - 1 si
    protected $Nesv;                  // número de eventos
    protected $Tasaesv;               // tasa de eventos
    protected $Normal;                // si es normal 0 no - 1 si
    protected $Bradicardia;           // si hubo bradicardia 0 no - 1 si
    protected $RtaCronotopica;        // si hubo respuesta cronotópica
    protected $ArritmiaSevera;        // si hubo arritmia severa
    protected $ArritmiaSimple;        // si hubo arritmia simple
    protected $ArritmiaSupra;         // si hubo arritmia supraventricular
    protected $BdeRama;               // si presenta bloqueo de rama
    protected $Bav2g;                 // 0 no - 1 si
    protected $Disociacion;           // si hubo disociación
    protected $Comentarios;           // comentarios del usuario
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initHolter();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase, llamado
     * desde el constructor o desde la consulta cuando no
     * hay registros
     */
    protected function initHolter(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Visita = 0;
        $this->Fecha = "";
        $this->Media = 0;
        $this->Minima = 0;
        $this->Maxima = 0;
        $this->Latidos = 0;
        $this->Ev = 0;
        $this->Nev = 0;
        $this->Tasaev = 0;
        $this->Esv = 0;
        $this->Nesv = 0;
        $this->Tasaesv = 0;
        $this->Normal = 0;
        $this->Bradicardia = 0;
        $this->RtaCronotopica = 0;
        $this->ArritmiaSevera = 0;
        $this->ArritmiaSimple = 0;
        $this->ArritmiaSupra = 0;
        $this->BdeRama = 0;
        $this->Bav2g = 0;
        $this->Disociacion = 0;
        $this->Comentarios = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

    }

    // métodos de asignación de variables
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }
    public function setMedia($media){
        $this->Media = $media;
    }
    public function setMinima($minima){
        $this->Minima = $minima;
    }
    public function setMaxima($maxima){
        $this->Maxima = $maxima;
    }
    public function setLatidos($latidos){
        $this->Latidos = $latidos;
    }
    public function setEv($ev){
        $this->Ev = $ev;
    }
    public function setNev($nev){
        $this->Nev = $nev;
    }
    public function setTasaEv($tasa){
        $this->Tasaev = $tasa;
    }
    public function setEsv($esv){
        $this->Esv = $esv;
    }
    public function setNesv($nesv){
        $this->Nesv = $nesv;
    }
    public function setTasaEsv($tasa){
        $this->Tasaesv = $tasa;
    }
    public function setNormal($normal){
        $this->Normal = $normal;
    }
    public function setBradicardia($bradicardia){
        $this->Bradicardia = $bradicardia;
    }
    public function setRtaCronotopica($cronotopica){
        $this->RtaCronotopica = $cronotopica;
    }
    public function setArritmiaSevera($arritmia){
        $this->ArritmiaSevera = $arritmia;
    }
    public function setArritmiaSimple($arritmia){
        $this->ArritmiaSimple = $arritmia;
    }
    public function setArritmiaSupra($arritmia){
        $this->ArritmiaSupra = $arritmia;
    }
    public function setBdeRama($bloqueo){
        $this->BdeRama = $bloqueo;
    }
    public function setBav2g($bav){
        $this->Bav2g = $bav;
    }
    public function setDisociacion($disociacion){
        $this->Disociacion = $disociacion;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getMedia(){
        return $this->Media;
    }
    public function getMinima(){
        return $this->Minima;
    }
    public function getMaxima(){
        return $this->Maxima;
    }
    public function getLatidos(){
        return $this->Latidos;
    }
    public function getEv(){
        return $this->Ev;
    }
    public function getNev(){
        return $this->Nev;
    }
    public function getTasaEv(){
        return $this->Tasaev;
    }
    public function getEsv(){
        return $this->Esv;
    }
    public function getNesv(){
        return $this->Nesv;
    }
    public function getTasaesv(){
        return $this->Tasaesv;
    }
    public function getNormal(){
        return $this->Normal;
    }
    public function getBradicardia(){
        return $this->Bradicardia;
    }
    public function getRtaCronotopica(){
        return $this->RtaCronotopica;
    }
    public function getArritmiaSevera(){
        return $this->ArritmiaSevera;
    }
    public function getArritmiaSimple(){
        return $this->ArritmiaSimple;
    }
    public function getArritmiaSupra(){
        return $this->ArritmiaSupra;
    }
    public function getBdeRama(){
        return $this->BdeRama;
    }
    public function getBav2g(){
        return $this->Bav2g;
    }
    public function getDisociacion(){
        return $this->Disociacion;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param paciente - clave del paciente
     * @return resultset con los registros encontrados
     * Método utilizado en la impresión de historias agrupadas
     * por estudio que recibe como parámetro el protocolo de
     * un paciente y retorna el vector con todos los registros
     */
    public function nominaVisitas($paciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_holter.id AS id,
                            diagnostico.v_holter.paciente AS paciente,
                            diagnostico.v_holter.visita AS visita,
                            diagnostico.v_holter.fecha AS fecha,
                            diagnostico.v_holter.media AS media,
                            diagnostico.v_holter.minima AS minima,
                            diagnostico.v_holter.maxima AS maxima,
                            diagnostico.v_holter.laticos AS latidos,
                            diagnostico.v_holter.ev AS ev,
                            diagnostico.v_holter.nev AS nev,
                            diagnostico.v_holter.tasaev AS tasaev,
                            diagnostico.v_holter.esv AS esv,
                            diagnostico.v_holter.nesv AS nesv,
                            diagnostico.v_holter.tasaesv AS tasaesv,
                            diagnostico.v_holter.normal AS normal,
                            diagnostico.v_holter.bradicardia AS bradicardia,
                            diagnostico.v_holter.rtacronotropica AS rtacronotropica,
                            diagnostico.v_holter.arritmiasevera AS arritmiasevera,
                            diagnostico.v_holter.arritmiasimple AS arritmiasimple,
                            diagnostico.v_holter.arritmiasupra AS arritmiasupra,
                            diagnostico.v_holter.bderama AS bderama,
                            diagnostico.v_holter.bav2g AS bav2g,
                            diagnostico.v_holter.disociacion AS disociacion,
                            diagnostico.v_holter.comentarios AS comentarios,
                            diagnostico.v_holter.usuario AS usuario
                     FROM diagnostico.v_holter
                     WHERE diagnostico.v_holter.paciente = '$paciente'
                     ORDER BY STR_TO_DATE(diagnostico.v_holter, '%d/%m/%Y') ASC; ";

        // obtenemos el vector y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param visita clave de la visita
     * Método que recibe como parámetro la clave del registro
     * y asigna en las variables de clase los valores de la
     * base
     */
    public function getDatosHolter($visita){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_holter.id AS id,
                            diagnostico.v_holter.paciente AS paciente,
                            diagnostico.v_holter.visita AS visita,
                            diagnostico.v_holter.fecha AS fecha,
                            diagnostico.v_holter.media AS media,
                            diagnostico.v_holter.minima AS minima,
                            diagnostico.v_holter.maxima AS maxima,
                            diagnostico.v_holter.latidos AS latidos,
                            diagnostico.v_holter.ev AS ev,
                            diagnostico.v_holter.nev AS nev,
                            diagnostico.v_holter.tasaev AS tasaev,
                            diagnostico.v_holter.esv AS esv,
                            diagnostico.v_holter.nesv AS nesv,
                            diagnostico.v_holter.tasaesv AS tasaesv,
                            diagnostico.v_holter.normal AS normal,
                            diagnostico.v_holter.bradicardia AS bradicardia,
                            diagnostico.v_holter.rtacronotropica AS rtacronotropica,
                            diagnostico.v_holter.arritmiasevera AS arritmiasevera,
                            diagnostico.v_holter.arritmiasimple AS arritmiasimple,
                            diagnostico.v_holter.arritmiasupra AS arritmiasupra,
                            diagnostico.v_holter.bderama AS bderama,
                            diagnostico.v_holter.bav2g AS bav2g,
                            diagnostico.v_holter.disociacion AS disociacion,
                            diagnostico.v_holter.comentarios AS comentarios,
                            diagnostico.v_holter.usuario AS usuario,
                            diagnostico.v_holter.fecha_alta AS fecha_alta
                     FROM diagnostico.v_holter
                     WHERE diagnostico.v_holter.visita = '$visita'; ";
        $resultado = $this->Link->query($consulta);

        // si hubo resultados
        if ($resultado->rowCount() != 0) {

            // obtenemos el registro y asignamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Visita = $visita;
            $this->Fecha = $fecha;
            $this->Media = $media;
            $this->Minima = $minima;
            $this->Maxima = $maxima;
            $this->Latidos = $latidos;
            $this->Ev = $ev;
            $this->Tasaev = $tasaev;
            $this->Esv = $esv;
            $this->Nesv = $nesv;
            $this->Tasaesv = $tasaesv;
            $this->Normal = $normal;
            $this->Bradicardia = $bradicardia;
            $this->RtaCronotopica = $rtacronotropica;
            $this->ArritmiaSevera = $arritmiasevera;
            $this->ArritmiaSimple = $arritmiasimple;
            $this->ArritmiaSupra = $arritmiasupra;
            $this->BdeRama = $bderama;
            $this->Bav2g = $bav2g;
            $this->Disociacion = $disociacion;
            $this->Comentarios = $comentarios;
            $this->Usuario = $usuario;
            $this->FechaAlta = $fecha_alta;

        // si no encontró registros
		} else {

            // inicializamos las variables
            $this->initHolter();

		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id del registro afectado
     * Método que ejecuta la consulta de edicion o eliminación
     * según corresponda, retorna la clave del registro afectado
     */
    public function grabaHolter(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoHolter();
        } else {
            $this->editaHolter();
        }

        // retorna la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevoHolter(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.holter
                           (paciente,
                            visita,
                            fecha,
                            media,
                            minima,
                            maxima,
                            latidos,
                            ev,
                            nev,
                            tasaev,
                            esv,
                            nesv,
                            tasaesv,
                            normal,
                            bradicardia,
                            rtacronotropica,
                            arritmiasevera,
                            arritmiasimple,
                            arritmiasupra,
                            bderama,
                            bav2g,
                            disociacion,
                            comentarios,
                            usuario)
                            VALUES
                            (:paciente,
                             :visita,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :media,
                             :minima,
                             :maxima,
                             :latidos,
                             :ev,
                             :nev,
                             :tasaev,
                             :esv,
                             :nesv,
                             :tasaesv,
                             :normal,
                             :bradicardia,
                             :rtacronotropica,
                             :arritmiasevera,
                             :arritmiasimple,
                             :arritmiasupra,
                             :bderama,
                             :bav2g,
                             :disociacion,
                             :comentarios,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",    $this->Paciente);
        $psInsertar->bindParam(":visita",      $this->Visita);
        $psInsertar->bindParam(":fecha",       $this->Fecha);
        $psInsertar->bindParam(":media",       $this->Media);
        $psInsertar->bindParam(":minima",      $this->Minima);
        $psInsertar->bindParam(":maxima",      $this->Maxima);
        $psInsertar->bindParam(":latidos",     $this->Latidos);
        $psInsertar->bindParam(":ev",          $this->Ev);
        $psInsertar->bindParam(":nev",         $this->Nev);
        $psInsertar->bindParam(":tasaev",      $this->Tasaev);
        $psInsertar->bindParam(":esv",         $this->Esv);
        $psInsertar->bindParam(":nesv",        $this->Nesv);
        $psInsertar->bindParam(":tasaesv",     $this->Tasaesv);
        $psInsertar->bindParam(":normal",      $this->Normal);
        $psInsertar->bindParam(":bradicardia", $this->Bradicardia);
        $psInsertar->bindParam(":rtacronotropica", $this->RtaCronotopica);
        $psInsertar->bindParam(":arritmiasevera",  $this->ArritmiaSevera);
        $psInsertar->bindParam(":arritmiasimple",  $this->ArritmiaSimple);
        $psInsertar->bindParam(":arritmiasupra",   $this->ArritmiaSupra);
        $psInsertar->bindParam(":bderama",     $this->BdeRama);
        $psInsertar->bindParam(":bav2g",       $this->Bav2g);
        $psInsertar->bindParam(":disociacion", $this->Disociacion);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que ejecuta la consulta de edición
     */
    protected function editaHolter(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.holter SET
                            paciente = :paciente,
                            visita = :visita,
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            media = :media,
                            minima = :minima,
                            maxima = :maxima,
                            latidos = :latidos,
                            eventos = :eventos,
                            nev = :nev,
                            tasaev = :tasaev,
                            esv = :esv,
                            nesv = :nesv,
                            tasanesv = :tasanesv,
                            normal = :normal,
                            bradicardia = :bradicardia,
                            rtacronotropica = :rtacronotropica,
                            arritmiasevera = :arritmiasevera,
                            arritmiasimple = :arritmiasimple,
                            arritmiasupra = :arritmiasupra,
                            bderama = :bderama,
                            bav2g = :bav2g,
                            disociacion = :disociacion,
                            comentarios = :comentarios,
                            usuario = :usuario
                     WHERE diagnostico.holter.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",    $this->Paciente);
        $psInsertar->bindParam(":visita",      $this->Visita);
        $psInsertar->bindParam(":fecha",       $this->Fecha);
        $psInsertar->bindParam(":media",       $this->Media);
        $psInsertar->bindParam(":minima",      $this->Minima);
        $psInsertar->bindParam(":maxima",      $this->Maxima);
        $psInsertar->bindParam(":latidos",     $this->Latidos);
        $psInsertar->bindParam(":ev",          $this->Ev);
        $psInsertar->bindParam(":Nev",         $this->Nev);
        $psInsertar->bindParam(":tasaev",      $this->Tasaev);
        $psInsertar->bindParam(":esv",         $this->Esv);
        $psInsertar->bindParam(":nesv",        $this->Nesv);
        $psInsertar->bindParam(":tasaesv",     $this->Tasaesv);
        $psInsertar->bindParam(":normal",      $this->Normal);
        $psInsertar->bindParam(":bradicardia", $this->Bradicardia);
        $psInsertar->bindParam(":rtacronotropica", $this->RtaCronotopica);
        $psInsertar->bindParam(":arritmiasevera",  $this->ArritmiaSevera);
        $psInsertar->bindParam(":arritmiasimple",  $this->ArritmiaSimple);
        $psInsertar->bindParam(":arritmiasupra",   $this->ArritmiaSupra);
        $psInsertar->bindParam(":bderama",     $this->BdeRama);
        $psInsertar->bindParam(":disociacion", $this->Disociacion);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de un registro y ejecuta
     * la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idholter - clave del registro
     */
    public function borraHolter($idholter){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.holter
                     WHERE diagnostico.holter.id = '$idholter'; ";
        $this->Link->exec($consulta);

    }

}
