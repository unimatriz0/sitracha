<?php

/**
 *
 * getdatosholter | holter/getdatosholter.php
 *
 * @package     Diagnostico
 * @subpackage  Holter
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y retorna el array
 * json con los valores del registro
 *
*/

// incluimos e instanciamos la clase
require_once("holter.class.php");
$holter = new Holter();

// obtenemos el registro
$holter->getDatosHolter($_GET["idvisita"]);

// retornamos el registro
echo json_encode(array("Id" =>              $holter->getId(),
                       "Fecha" =>           $holter->getFecha(),
                       "Media" =>           $holter->getMedia(),
                       "Minima" =>          $holter->getMinima(),
                       "Maxima" =>          $holter->getMaxima(),
                       "Latidos" =>         $holter->getLatidos(),
                       "Ev" =>              $holter->getEv(),
                       "Nev" =>             $holter->getNev(),
                       "Tasaev" =>          $holter->getTasaEv(),
                       "Esv" =>             $holter->getEsv(),
                       "Nesv" =>            $holter->getNesv(),
                       "Tasaesv" =>         $holter->getTasaesv(),
                       "Normal" =>          $holter->getNormal(),
                       "Bradicardia" =>     $holter->getBradicardia(),
                       "RtaCronoTropica" => $holter->getRtaCronotopica(),
                       "ArritmiaSevera" =>  $holter->getArritmiaSevera(),
                       "ArritmiaSimple"  => $holter->getArritmiaSimple(),
                       "ArritmiaSupra" =>   $holter->getArritmiaSupra(),
                       "Bderama" =>         $holter->getBdeRama(),
                       "Disociacion" =>     $holter->getDisociacion(),
                       "Usuario" =>         $holter->getUsuario(),
                       "FechaAlta" =>       $holter->getFechaAlta()));

?>
