/*
 * Nombre: holter.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 12/06/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de los holter
 *
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de los holter
 */
class Holter {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initHolter();

        // inicializamos el layer
        this.layerHolter = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initHolter(){

        // inicializamos las variables
        this.Id = 0;               // clave del registro
        this.Paciente = 0;         // clave del paciente (protocolo)
        this.Visita = 0;           // clave de la visita
        this.Fecha = "";           // fecha de administración
        this.Media = 0;            // media de la frecuencia cardíaca
        this.Minima = 0;           // frecuencia mínima
        this.Maxima = 0;           // frecuencia máxima
        this.Latidos = 0;          // cantidad total de latidos
        this.Ev = 0;               // Eventos ventriculares (0 falso 1 true)
        this.Nev = 0;              // número de eventos ventriculares
        this.Tasaev = 0;           // tasa de eventos ventriculares
        this.Esv = 0;              // tasa de eventos supraventriculares
        this.Nesv = 0;             // número de eventos supraventriculares
        this.Tasaesv = 0;          // tasa de eventos supraventriculares
        this.Normal = 0;           // si el resultado es normal (0 falso 1 true)
        this.Bradicardia = 0;      // si presentó bradicardia (0 falso 1 true)
        this.RtaCronotropica = 0;  // respuesta cronotrópica (0 falso 1 true)
        this.ArritmiaSevera = 0;   // (0 falso 1 true)
        this.ArritmiaSimple = 0;   // (0 falso 1 true)
        this.ArritmiaSupra = 0;    // (0 falso 1 true)
        this.BdeRama = 0;          // bloqueo de rama (0 falso 1 true)
        this.Bav2g = 0;            // (0 falso 1 true)
        this.Disociacion = 0;      // (0 falso 1 true)
        this.Usuario = "";         // nombre del usuario
        this.FechaAlta = "";       // fecha de alta del registro

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setPaciente(paciente){
        this.Paciente = paciente;
    }
    setVisita(visita){
        this.Visita = visita;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setMedia(media){
        this.Media = media;
    }
    setMinima(minima){
        this.Minima = minima;
    }
    setMaxima(maxima){
        this.Maxima = maxima;
    }
    setLatidos(latidos){
        this.Latidos = latidos;
    }
    setEv(ev){
        this.Ev = ev;
    }
    setNev(nev){
        this.Nev = nev;
    }
    setTasaev(tasa){
        this.Tasaev = tasa;
    }
    setEsv(esv){
        this.Esv = esv;
    }
    setNesv(nesv){
        this.Nesv = nesv;
    }
    setTasaesv(tasa){
        this.Tasaesv = tasa;
    }
    setNormal(normal){
        this.Normal = normal;
    }
    setBradicardia(bradicardia){
        this.Bradicardia = bradicardia;
    }
    setRtaCronoTropica(respuesta){
        this.RtaCronotropica = respuesta;
    }
    setArritmiaSevera(arritmia){
        this.ArritmiaSevera = arritmia;
    }
    setArritmiaSupra(arritmia){
        this.ArritmiaSupra = arritmia;
    }
    setArritmiaSimple(arritmia){
        this.ArritmiaSimple = arritmia;
    }
    setBderama(bderama){
        this.BdeRama = bderama;
    }
    setBav2g(bav){
        this.Bav2g = bav;
    }
    setDisociacion(disociacion){
        this.Disociacion = disociacion;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta en el layer emergente el
     * formulario de las radiografías
     */
    verHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no grabó la visita
        if (visitas.IdVisita == 0){

            // presenta el mensaje
            mensaje = "Debe grabar los datos de la visita primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos los valores del formulario padre
        this.Paciente = document.getElementById("protocolo_paciente").value;
        this.Visita = visitas.IdVisita;

        // abrimos y mostramos el layer
        this.layerHolter = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Holter',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:700,
                    draggable: 'title',
                    zIndex: 20000,
                    ajax: {
                        url: 'holter/form_holter.html',
                        reload: 'strict'
                    }
            });
        this.layerHolter.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase obtiene
     * los datos de la visita
     */
    getDatosHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "holter/getdatosholter.php?idvisita=" + this.Visita,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                holter.cargaDatosHolter(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} - vector con los datos del registro
     * Método que recibe un array json con los datos del
     * registro y asigna en las variables de clase
     */
    cargaDatosHolter(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en las variables de clase, visita y
        // paciente los tomamos del formulario padre
        this.setId(datos.Id);
        this.setFecha(datos.Fecha);
        this.setMedia(datos.Media);
        this.setMinima(datos.Minima);
        this.setMaxima(datos.Maxima);
        this.setLatidos(datos.Latidos);
        this.setEv(datos.Ev);
        this.setNev(datos.Nev);
        this.setTasaev(datos.Tasaev);
        this.setEsv(datos.Esv);
        this.setNesv(datos.Nesv);
        this.setTasaesv(datos.Tasaesv);
        this.setNormal(datos.Normal);
        this.setBradicardia(datos.Bradicardia);
        this.setRtaCronoTropica(datos.RtaCronotropica);
        this.setArritmiaSevera(datos.ArritmiaSevera);
        this.setArritmiaSimple(datos.ArritmiaSimple);
        this.setArritmiaSupra(datos.ArritmiaSupra);
        this.setBderama(datos.BdeRama);
        this.setBav2g(datos.Bav2g);
        this.setDisociacion(datos.Disociacion);
        this.setUsuario(datos.Usuario);
        this.setFechaAlta(datos.FechaAlta);

        // mostramos el registro
        this.muestraDatosHolter();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de los valores de las variables
     * de clase, carga las mismas en el formulario de datos
     */
    muestraDatosHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en el formulario
        document.getElementById("fecha_holter").value = this.Fecha;
        document.getElementById("media_holter").value = this.Media;
        document.getElementById("minima_holter").value = this.Minima;
        document.getElementById("maxima_holter").value = this.Maxima;
        document.getElementById("totales_holter").value = this.Latidos;

        // si hubo eventos
        if (this.Ev == 0){
            document.getElementById("evholter").checked = false;
        } else {
            document.getElementById("evholter").checked = true;
        }

        // seguimos asignando
        document.getElementById("nevholter").value = this.Nev;
        document.getElementById("tasaevholter").value = this.Tasaev;

        // si hubo eventos supraventriculares
        if (this.Esv == 0){
            document.getElementById("nevsholter").checked = false;
        } else {
            document.getElementById("nevsholter").checked = true;
        }

        // seguimos asignando
        document.getElementById("tasaesvholter").value = this.Tasaesv;

        // si el resultado es normal
        if (this.Normal == 0){
            document.getElementById("normalholter").checked = false;
        } else {
            document.getElementById("normalholter").checked = true;
        }

        // si presenta bradicardia sinusal
        if (this.Bradicardia == 0){
            document.getElementById("sinusalholter").checked = false;
        } else {
            document.getElementById("sinusalholter").checked = true;
        }

        // si hay respuesta cronotrópica
        if (this.RtaCronotropica == 0){
            document.getElementById("cronoholter").checked = false;
        } else {
            document.getElementById("cronoholter").checked = true;
        }

        // si hay arritmia ventricular
        if (this.ArritmiaSevera == 0){
            document.getElementById("avholter").checked = false;
        } else {
            document.getElementById("avholter").checked = true;
        }

        // si presenta arritmia simple
        if (this.ArritmiaSimple == 0){
            document.getElementById("avsimpleholter").checked = false;
        } else {
            document.getElementById("avsimpleholter").checked = true;
        }

        // si presenta arritmia supraventricular
        if (this.ArritmiaSupra == 0){
            document.getElementById("arritmiasvholter").checked = false;
        } else {
            document.getElementById("arritmiasvholter").checked = true;
        }

        // si presenta bloqueo de rama
        if (this.BdeRama == 0){
            document.getElementById("bramaholter").checked = false;
        } else {
            document.getElementById("bramaholter").checked = true;
        }

        // si presenta bav
        if (this.Bav2g == 0){
            document.getElementById("bavholter").checked = false;
        } else {
            document.getElementById("bavholter").checked = true;
        }

        // si hay disociación
        if (this.Disociacion == 0){
            document.getElementById("disociacionholter").checked = false;
        } else {
            document.getElementById("disociacionholter").checked = true;
        }

        // fijamos la fecha de alta y el usuario si está editando
        if (this.Id != 0){
            document.getElementById("altaholter").value = this.FechaAlta;
            document.getElementById("usuarioholter").value = this.Usuario;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que valida
     * el formulario antes de enviarlo al servidor
     */
    validaHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no ingresó la fecha
        if (document.getElementById("fecha_holter").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha del estudio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_holter").focus();
            return false;

        // si inresó
        } else {

            // asigna en la clase
            this.Fecha = document.getElementById("fecha_holter").value;

        }

        // si no ingresó la frecuencia media
        if (document.getElementById("media_holter").value == 0 || document.getElementById("media_holter").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la media defrecuencia cardíaca";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("media_holter").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Media = document.getElementById("media_holter").value;

        }

        // si no ingresó la frecuencia mínima
        if (document.getElementById("minima_holter").value == 0 || document.getElementById("minima_holter").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique frecuencia mínima durante el estudio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("minima_holter").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Minima = document.getElementById("minima_holter").value;

        }

        // si no ingresó la frecuencia máxima
        if (document.getElementById("maxima_holter").value == 0 || document.getElementById("maxima_holter").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la frecuencia máxima durante el estudio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("maxima_holter").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Maxima = document.getElementById("maxima_holter").value;

        }

        // si no ingresó el número de latidos totales
        if (document.getElementById("totales_holter").value == 0 || document.getElementById("totales_holter").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el número total de latidos";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("totales_holter").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Latidos = document.getElementById("totales_holter").value;

        }

        // si hubo eventos verifica que halla ingresado
        // el número
        if (document.getElementById("evholter").checked){

            // asigna en la clase
            this.Ev = 1;

            // verifica que halla ingresado el número
            // de eventos
            if (document.getElementById("nevholter").value == 0 || document.getElementById("nevholter") == ""){

                // presenta el mensaje y retorna
                mensaje = "Si hubo eventos debe indicar el número";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("fecha_holter").focus();
                return false;

            // si ingresó los eventos
            } else {

                // asigna en la clase
                this.Nev = document.getElementById("nevholter").value;

                // verifica que se halla calculado la tasa
                this.calculaEv();

            }

        // si no hubo eventos
        } else {

            // asigna en la clase
            this.Ev = 0;

            // asigna también los eventos y la tasa
            this.Nev = 0;
            this.Tasaev = 0;

        }

        // si hubo eventos supraventriculares verifica
        // que halla ingresado el número
        if (document.getElementById("evsholter").checked){

            // asigna en la clase
            this.Esv = 1;

            // verifica que se halla ingresado el número
            if (document.getElementById("nevsholter").value == 0 || document.getElementById("nevsholter").value == ""){

                // presenta el mensaje y retorna
                mensaje = "Si hubo eventos supraventriculares indique el número";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("nevsholter").focus();
                return false;

            // si ingresó
            } else {

                // asigna en la clase
                this.Nesv = document.getElementById("nevsholter").value;

                // verifica la tasa
                this.calculaEsv();

            }

        // si no hubo eventos
        } else {

            // asigna en la clase
            this.Esv = 0;

            // asigna también la tasa y el número
            this.Nesv = 0;
            this.Tasaesv = 0;

        }

        // los check puede marcarlos o no, asignamos
        // el valor en las variables de clase
        if (document.getElementById("normalholter").checked){
            this.Normal = 1;
        } else {
            this.Normal = 0;
        }
        if (document.getElementById("sinusalholter").checked){
            this.Bradicardia = 1;
        } else {
            this.Bradicardia = 0;
        }
        if (document.getElementById("cronoholter").checked){
            this.RtaCronotropica = 1;
        } else {
            this.RtaCronotropica = 0;
        }
        if (document.getElementById("avholter").checked){
            this.ArritmiaSevera = 1;
        } else {
            this.ArritmiaSevera = 0;
        }
        if (document.getElementById("avsimpleholter").checked){
            this.ArritmiaSimple = 1;
        } else {
            this.ArritmiaSimple = 0;
        }
        if (document.getElementById("arritmiasvholter").checked){
            this.ArritmiaSupra = 1;
        } else {
            this.ArritmiaSupra = 0;
        }
        if (document.getElementById("bramaholter").checked){
            this.BdeRama = 1;
        } else {
            this.BdeRama = 0;
        }
        if (document.getElementById("bavholter").checked){
            this.Bav2g = 1;
        } else {
            this.Bav2g = 0;
        }
        if (document.getElementById("disociacionholter").checked){
            this.Disociacion = 1;
        } else {
            this.Disociacion = 0;
        }

        // grabamos el registro
        this.grabaHolter();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * envía los datos al servidor
     */
    grabaHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var formHolter = new FormData();

        // agregamos los datos al formulario
        formHolter.append("Id", this.Id);
        formHolter.append("Paciente", this.Paciente);
        formHolter.append("Visita", this.Visita);
        formHolter.append("Fecha", this.Fecha);
        formHolter.append("Media", this.Media);
        formHolter.append("Minima", this.Minima);
        formHolter.append("Maxima", this.Maxima);
        formHolter.append("Latidos", this.Latidos);
        formHolter.append("Ev", this.Ev);
        formHolter.append("Nev", this.Nev);
        formHolter.append("Tasaev", this.Tasaev);
        formHolter.append("Esv", this.Esv);
        formHolter.append("Nesv", this.Nesv);
        formHolter.append("Tasaesv", this.Tasaesv);
        formHolter.append("Normal", this.Normal);
        formHolter.append("Bradicardia",this.Bradicardia);
        formHolter.append("RtaCronotropica", this.RtaCronotropica);
        formHolter.append("ArritmiaSevera", this.ArritmiaSevera);
        formHolter.append("ArritmiaSimple", this.ArritmiaSimple);
        formHolter.append("ArritmiaSupra", this.ArritmiaSupra);
        formHolter.append("BdeRama", this.BdeRama);
        formHolter.append("Bav2g", this.Bav2g);
        formHolter.append("Disociacion", this.Disociacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "holter/grabaholter.php",
            type: "POST",
            data: formHolter,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    holter.setId(data.Id);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }


    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar, recarga
     * el formulario sin grabar los cambios
     */
    cancelaHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está editando
        if (this.Id != 0){

            // recargamos los datos
            this.cargaDatosHolter();

        // si está insertando
        } else {

            // lo limpiamos
            this.limpiaHolter();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de datos
     */
    limpiaHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos los elementos
        document.getElementById("fecha_holter").value = "";
        document.getElementById("mediaholter").value = "";
        document.getElementById("minimaholter").value = "";
        document.getElementById("maximaholter").value = "";
        document.getElementById("totalesholter").value = "";
        document.getElementById("evholter").checked = false;
        document.getElementById("nevholter").value = "";
        document.getElementById("tasaevholter").value = "";
        document.getElementById("evsholter").checked = false;
        document.getElementById("nevsholter").value = "";
        document.getElementById("tasaesvholter").value = "";
        document.getElementById("normalholter").checked = false;
        document.getElementById("sinusalholter").checked = false;
        document.getElementById("cronoholter").checked = false;
        document.getElementById("avholter").checked = false;
        document.getElementById("avsimpleholter").checked = false;
        document.getElementById("arritmiasvholter").checked = false;
        document.getElementById("bramaholter").checked = false;
        document.getElementById("bavholter").checked = false;
        document.getElementById("disociacionholter").checked = false;

        // fijamos la fecha de alta y el usuario
        document.getElementById("altaholter").value = fechaActual();
        document.getElementById("usuarioholter").value = sessionStorage.getItem("Usuario");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al perder el foco el número de eventos
     * ventriculares que calcula la tasa y la presenta en
     * el formulario
     */
    calculaEv(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no está marcado
        if (!document.getElementById("evholter").checked){
            return false;
        }

        // declaramos las variables
        var latidos = document.getElementById("totales_holter").value;
        var eventos = document.getElementById("nevholter").value;

        // si falta el número de latidos o el número de eventos
        if (latidos == 0 || latidos == "" || eventos == 0 || eventos == ""){
            return false;
        }

        // calculamos la tasa
        var tasa = eventos / latidos;

        // fijamos el valor y asignamos en la clase
        document.getElementById("tasaevholter").value = tasa.toFixed(2);
        this.Tasaev = tasa.toFixed(2);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al perder el foco el número de
     * eventos supraventriculares que calcula la tasa y
     * la presenta en el formulario
     */
    calculaEsv(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no está marcado
        if (!document.getElementById("evsholter").checked){
            return false;
        }

        // declaramos las variables
        var latidos = document.getElementById("totales_holter").value;
        var eventos = document.getElementById("nevsholter").value;

        // si falta el número de latidos o el número de eventos
        if (latidos == 0 || latidos == "" || eventos == 0 || eventos == ""){
            return false;
        }

        // calculamos la tasa
        var tasa = eventos / latidos;

        // fijamos el valor y asignamos en la clase
        document.getElementById("tasaesvholter").value = tasa.toFixed(2);
        this.Tasaesv = tasa.toFixed(2);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón borrar que luego de
     * pedir confirmación ejecuta la eliminación del registro
     * actual y cierra el layer
     */
    borraHolter(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Holter',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('holter/borraholter.php?id='+holter.Id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // cerramos el layer
                            holter.layerHolter.destroy();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}