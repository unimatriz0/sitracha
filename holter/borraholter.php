<?php

/**
 *
 * borraholter | holter/borraholter.php
 *
 * @package     Diagnostico
 * @subpackage  Holter
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta 
 * la consulta de eliminación
 * 
*/

// incluimos e instanciamos la clase
require_once("holter.class.php");
$holter = new Holter();

// eliminamos el registro
$holter->borraHolter($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Id" => 1));

?>
