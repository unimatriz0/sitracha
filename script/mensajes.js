// Nombre: mensajes.js
// Autor: Claudio Invernizzi
// Fecha: 28/09/2017
// Proyecto: Diagnóstico
// Producido en: INP - Dr. Mario Fatala Chaben
// Licencia: GPL
// Comentarios: Clases utilizadas para los cuadros emergentes y mensajes

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * Clase que define un layer nuevo en la pàgina y luego retorna el objeto creado,
 * provee los mètodos para mostrarlo, ocultarlo, y destruirlo, como asì tambièn
 * para cargar o mostrar un texto o pàgina cargada dinàmicamente
 */
class Dialogo {

    /**
    * @param: nada o el nombre del layer
    * @param: boolean si el layer será modal
    * @param: boolean si el layer tendra botón cerrar
    * @return: objeto
    * Constructor de la clase
    */
    constructor(ventana, modal, cerrar){

        // si no recibió el nombre
        if (ventana === undefined  || ventana === ""){
            this.nombre = "emergente";
        } else {
            this.nombre = ventana;
        }

        // si no recibió si es modal
        if (modal === undefined){
            this.modal = true;
        } else {
            this.modal = modal;
        }

        // si es modal
        if (this.modal){

            // agrega el layer opaco al dom de la página
            $("body").append("<div id='" + this.nombre + "overlay' class='overlay'></div>");

        }

        // si no recibió el botón cerrar
        if (cerrar === undefined){

            // lo inicializa
            cerrar = true;

        }

        // agregamos el layer al dom de la página
        $("body").append("<div id='" + this.nombre + "' class='emergente'></div>");

        // si presenta el botón cerrar
        if (cerrar){

            // agrega el botón cerrar
            $("#" + this.nombre).append("<div id='" + this.nombre + "close' class='emergenteclose'></div>");

            // creamos el enlace
            this.crearEnlace();

            // agregamos el evento del botón cerrar
            $("#" + this.nombre + "close").append(this.enlace);

        }

        // agregamos un div de datos
        $("#" + this.nombre).append("<div id='" + this.nombre + "datos'></div>");

        // definimos las propiedades del diálogo
        this.enlace = "";                 // código del enlace cerrar
        this.x = 0;                       // oordenada x
        this.ancho = 300;                 // ancho del layer
        this.alto = 300;                  // alto del layer
        this.y = 0;                       // coordenada y

        // en el constructor lo centramos
        this.centrarDialogo();

    }

    // método que fija la altura del layer
    setAlto(alto){
        this.alto = alto;
        $("#" + this.nombre).css("height", this.alto + "px");
    }

    // método que fija el ancho del layer
    setAncho(ancho){
        this.ancho = ancho;
        $("#" + this.nombre).css("width", this.ancho + "px");
    }

    // fija la ordenada x
    setX(x){
        this.x = x;
        $("#" + this.nombre).css("left", this.x + "px");
    }

    // fija la coordenada y
    setY(y){
        this.y = y;
        $("#" + this.nombre).css("top", this.y + "px");
    }

    // método que carga la url en el layer
    setUrl(url){
        $("#" + this.nombre + "datos").load(url);
    }

    // método que muestra u oculta las barras
    setBarras(barras){
        if (barras){
            $("#" + this.nombre).css("overflow-y", "auto");
        } else {
            $("#" + this.nombre).css("overflow-y", "none");
        }
    }

    // método que fija el orden del layer
    setOrden(orden){
        $("#" + this.nombre).css("z-index", orden);
    }

    // este método centra el layer
    centrarDialogo(){

        // obtenemos las dimensiones de la ventana
        var altura = $(window).height();
        var anchura = $(window).width();

        // calculamos las nuevas coordenadas
        var x = (anchura - this.ancho) / 2;
        var y = (altura - this.alto) / 2;

        // posicionamos el dialogo
        this.setX(x);
        this.setY(y);

    }

    // método que muestra el layer
    mostrar(){

        // si es modal
        if (this.modal){
            $("#" + this.nombre + 'overlay').show();
        }

        // muestra el layer
        $("#" + this.nombre ).show();

    }

    // método que crea el enlace para cerrar el layer
    crearEnlace(){

        // creamos el enlace y le pasamos los argumentos
        this.enlace = "<a href='#' ";
        this.enlace += "title='Pulse para cerrar la ventana' ";
        this.enlace += "onClick='$(" + '"#' + this.nombre + '"' + ").remove(); ";
        this.enlace += "$(" + '"#' + this.nombre + 'overlay"' + ").remove(); '> ";
        this.enlace += "<img src='imagenes/error.png' ";
        this.enlace += "     whidth=18px ";
        this.enlace += "     height=18px >";
        this.enlace += "</a>";

    }

    // método que destruye el layer
    destruir(){

        // destruimos el layer por su id
        $("#" + this.nombre ).remove();

        // si está mostrando modal
        if (this.modal){

            // destruimos el layer de base
            $("#" + this.nombre + "overlay").remove();

        }

    }

}

/**
 * @param texto a mostrar
 * @param color de fondo
 * @return nada o el objeto
 * Método que crea un nuevo layer en la página y presenta el mensaje
 * en la esquina superior derecha de la pantalla, durante una
 * cantidad de tiempo, los valores de color de fondo aceptados
 * son verde (green), azul(blue), rojo(red)
 */
class Mensaje{

    // definimos el constructor
    constructor(mensaje, color){

        // declaramos la variable local
        var fondo = "";

        // verificamos el color
        if (color == "green"){
            fondo = "#37ca20";
        } else if (color == "blue"){
            fondo = "#6a91f9";
        } else if (color == "red"){
            fondo = '#FF4000';
        } else {
            fondo = "";
        }

        // lo agregamos al cuerpo del documento
        $("body").append("<div id='alerta' class='alerta'>" + mensaje + "</div>");
        $('#alerta').css('background-color', fondo);

        // definimmos el temporizador con tres segundos
        window.setTimeout(this.ocultarMensaje, 3000);

    }

    // función llamada por el temporizador, se encarga de
    // destruir el div del mensaje
    ocultarMensaje(){

        // directamente eliminamos el div
        $("#alerta").remove();

    }

}
