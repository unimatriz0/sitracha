/*

    Nombre: mapas.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 14/12/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que interactúa con la api de google maps

*/

/*jshint esversion: 6 */

// declaración de la clase
class Mapas {

    // constructor de la clase
    constructor(){

        // declaración de variables

    }

    /**
     * Nombre: agregaMarcador
     * @param {map} map objeto mapa
     * @param {infowindow} infowindow objeto infowindow
     * @param {marker} marcador objeto marcador
     * @param {string} coordenadas
     * @param {string} texto el texto del marcador
     * Método que recibe como parámetros el objeto infowindow, el objeto
     * marcador, las coordenadas a presentar y el texto, añade el
     * marcador al mapa y el evento para mostrar el texto, el objeto
     * mapa lo suponemos público
     */
    agregaMarcador(map, infowindow, marcador, coordenadas, texto){

        // agrega el evento
        google.maps.event.addListener(marcador, 'click', function() {

            // posiciona las coordenadas del la ventana de información
            infowindow.setPosition(coordenadas);

            // setea el texto
            infowindow.setContent(texto);

            // abre la ventana
            infowindow.open(map, marcador);

        });

    }

    /**
     * Nombre: initMap
     * @param {string} leyenda
     * @param {double} latitud
     * @param {double} longitud
     * Método que recibe como parámetros las coordenadas a presentar
     * muestra los layers y llama la rutina google, recibe la cadena
     * con la leyenda a presentar en el marcador
     */
    initMap(leyenda, latitud, longitud) {

        // mostramos los layers
        $("#map_cerrar").show();
        $("#map").show();

        // llamamos la rutina de google
        var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: latitud,
                            lng: longitud},
                    zoom: 16
                });

        // obtenemos el punto de las coordenadas
        var coordenadas = new google.maps.LatLng(latitud, longitud);

        // declara la ventana de información
        var infowindow = new google.maps.InfoWindow(
                        { content: "",
                        size: new google.maps.Size(50,50)
                        });

        // agrega el punto al mapa
        var marca = new google.maps.Marker({
                        position: coordenadas,
                        map: map
                    });

        // agregamos el marcador
        agregaMarcador(map, infowindow, marca, coordenadas, leyenda);

    }

    /**
     * Nombre: cerrarMapa
     * @return void
     * Método que simplemente oculta el layer de los mapas
     */
    cerrarMapa(){

        // ocultamos el mapa y el botón cerrar
        $("#map").hide();
        $("#map_cerrar").hide();

    }

    /**
     * Nombre: detectaLaboratorios
     * @param: [int] id del registro (y también de objeto html)
     * @param [string] dirección
     * Método que recibe como parámetros la id del registro (que también es la id
     * del texto con las coordenadas) obtiene las coordenadas gps a través del
     * servicio de google maps y luego en caso de éxito, ejecuta la consulta
     * de actualización en el servidor y muestra las coordenadas en la página
     */
    detectaLaboratorios(id, domicilio){

        // reiniciamos el contador
        contador();

        // declaración de variables
        var coordenadas;

        // instancia el geocoder
        geocoder = new google.maps.Geocoder();

        // busca la dirección
        geocoder.geocode( { 'address': domicilio}, function(results, status) {

            // si hubo resultados
            if (status == google.maps.GeocoderStatus.OK) {

                // presenta las coordenadas en el formulario
                coordenadas = results[0].geometry.location;
                document.getElementById(id).value = coordenadas;

                // las graba en la base
                $.post('laboratorios/graba_coordenadas.php','id='+id+'&coordenadas='+coordenadas);

            // si no encontró
            } else {

                // presenta el mensaje
                mensaje = "No se ha podido detectar una\n";
                mensaje += "serie de coordenadas válidas, el\n";
                mensaje += "error fue: " + status;
                new jBox('Notice', {content: mensaje, color: 'red'});

            }

        });

        // retorna
        return false;

    }

}