/*

    Nombre: tabs.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 11/10/2017
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Comentarios: Script que controla los tabuladores de la pantalla principal
                 de la aplicación

*/

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @param evt
 * @param tabulador
 * Método que recibe como parámetro el evento y el tabulador sobre
 * el que se pulsó, activa el div de ese tabulador, lo marca como
 * activo y desactiva todos los demás
 */
function mostrarTab(evt, tabulador) {

    // declaramos las variables locales
    var i, tabcontent, tablinks;

    // obtenemos todos los elementos con la id 'tabcontent'
    tabcontent = document.getElementsByClassName("tabcontent");

    // recorremos el array ocultándolos
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // obtenemos todos los elementos con la clase
    tablinks = document.getElementsByClassName("tablinks");

    // recorremos el vector removiendo la clase activo
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // muestra el tab actual y la añade la clase 'active' al boton
    document.getElementById(tabulador).style.display = "block";
    evt.currentTarget.className += " active";

}
