/*
    Nombre: progressbar.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 16/10/2017
    Mail: cinvernizzi@gmail.com
    Comentarios: Clase que crea y controla la barra de progreso

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

 /**
 * @param: nada o un texto a mostrar
 * @return: objeto
 * Clase que define un layer nuevo en la página y luego retorna el objeto creado,
 * provee los métodos para mostrarlo, ocultarlo, y destruirlo
 */
class progressBar {

    // constructor de la clase
    constructor(texto){

        // agrega el layer de la barra de progreso
        $("body").append("<div id='myProgress' class='myProgress'></div>");

        // agregamos la barra
        $("#myProgress").append("<div id='myBar' class='myBar'></div>");

        // si recibió un texto
        if (texto !== undefined && texto !== ""){

            // agregamos el texto de fondo
            $("#myBar").append(texto);

        }

        // centramos la barra en pantalla
        this.centrarBarra();

    }

    // este método centra el layer
    centrarBarra(){

        // obtenemos las dimensiones de la ventana
        var altura = $(window).height();
        var anchura = $(window).width();

        // calculamos las nuevas coordenadas
        var x = (anchura - 800) / 2;
        var y = (altura - 30) / 2;

        // posicionamos el dialogo
        $("#myProgress").css("left", x + "px");
        $("#myProgress").css("top", y + "px");

    }

    // función que recibe como parámetro el valor de la barra
    fijarValor(valor){
        $("#myBar").css("width", valor + "%");
    }

    // función que simplemente elimina la barra del dom
    destruir(){
        $("#myProgress" ).remove();
    }

}
