<?php

/**
 *
 * organos/form_organos.php
 *
 * @package     Diagnostico
 * @subpackage  Organos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario de abm de órganos de transplante
 *
*/

// incluimos e instanciamos las clases
require_once("organos.class.php");
$organos = new Organos();

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["ID"])){
    $usuario_actual = $_SESSION["Usuario"];
}
session_write_close();

// presentamos el título
echo "<h2>Diccionario de Organos Transplantados</h2>";

// definimos la tabla
echo "<table width='60%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Organo</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la nómina de órganos
$nomina = $organos->nominaOrganos();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre del Organo'>";
    echo "<input type='text'
                 name='organo_$id_organo'
                 id='organo_$id_organo'
                 value='$organo'>";
    echo "</span>";
    echo "</td>";

    // presentamos el usuario y la fecha
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // arma el enlace
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaOrgano'
           id='btnGrabaOrgano'
           title='Pulse para grabar el registro'
           onClick='organos.verificaOrgano($id_organo)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// presentamos el nuevo registro
echo "<tr>";

// presentamos el registro
echo "<td>";
echo "<span class='tooltip'
            title='Nombre del Organo'>";
echo "<input type='text'
             name='organo_nuevo'
             id='organo_nuevo'>";
echo "</span>";
echo "</td>";

// obtenemos la fecha de alta
$fecha_alta = date('d/m/Y');

// presentamos el usuario y la fecha
echo "<td align='center'>$usuario_actual</td>";
echo "<td align='center'>$fecha_alta</td>";

// arma el enlace
echo "<td align='center'>";
echo "<input type='button'
       name='btnGrabaOrgano'
       id='btnGrabaOrgano'
       title='Pulse para grabar el registro'
       onClick='organos.verificaOrgano()'
       class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>