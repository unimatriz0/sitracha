<?php

/**
 *
 * organos/graba_organo.php
 *
 * @package     Diagnostico
 * @subpackage  Organos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario,
 * ejecuta la consulta en el servidor y retorna el
 * resultado de la operación
 *
*/

// incluimos e instanciamos las clases
require_once("organos.class.php");
$organo = new Organos();

// si está editando
if (!empty($_POST["Id"])){
    $organo->setIdOrgano($_POST["Id"]);
}

// fija el nombre
$organo->setOrgano($_POST["Organo"]);

// grabamos y obtenemos el resultado
$error = $organo->grabaOrgano();

// retornamos el resultado
echo json_encode(array("Error" => $error));

?>