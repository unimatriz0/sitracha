/*
 * Nombre: organos.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: /
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de organos
 *              transplantados
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el abm de órganos de transplante
 */
class Organos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializa las variables de clase
        this.initOrganos();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initOrganos(){

        // declaración de variables
        this.IdOrgano = 0;
        this.Organo = "";
        this.IdUsuario = 0;
        this.Usuario = "";
        this.FechaAlta = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de órganos
     */
    muestraOrganos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("organos/form_organos.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idorgano clave del órgano a verificar
     * Método que recibe como parámetro la id del órgano y verifica
     * los valores del formulario, si no recibe id asume que
     * es un alta
     */
    verificaOrgano(idorgano){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no recibió la id
        if (typeof(idorgano) == "undefined"){
            idorgano = "nuevo";
        } else {
            this.IdOrgano = idorgano;
        }

        // verifica que halla ingresado el nombre
        if (document.getElementById("organo_" + idorgano).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre del órgano";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("organo_" + idorgano).focus();
            return false;

        } else {

            // asignamos en la variable de clase
            this.Organo = document.getElementById("organo_" + idorgano).value;

        }

        // verificamos que no esté repetido
        this.validaOrgano();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de actualización en el servidor
     */
    grabaOrgano(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosOrgano = new FormData();

        // si está editando
        if (this.IdOrgano != 0){
            datosOrgano.append("Id", this.IdOrgano);
        }

        // agregamos el nombre
        datosOrgano.append("Organo", this.Organo);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "organos/graba_organo.php",
            type: "POST",
            data: datosOrgano,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    organos.initOrganos();

                    // recarga el formulario para reflejar los cambios
                    organos.muestraOrganos();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase verifica que el
     * órgano no se encuentre repetido
     */
    validaOrgano(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está dando un alta
        if (this.IdOrgano == 0){

            // validamos que no esté declarada
            // lo llamamos asincrónico
            $.ajax({
                url: "organos/valida_organo.php?organo="+this.Organo,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // si no hay registros
                    if (data.Registros == 0){

                        // grabamos
                        organos.grabaOrgano();

                    // si hay registros
                    } else {

                        // presenta la alerta
                        new jBox('Notice', {content: "Ya está declarado ese órgano", color: 'red'});

                    }

            }});

        // si está editando
        } else {

            // directamente grabamos
            this.grabaOrgano();

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un objeto html
     * @param {int} idorgano clave del elemento preseleccionado
     * Método que recibe como parámetro la id de un elemento del formulario
     * y carga en el la nómina de órganos, si además recibe la clave de
     * un órgano la predetermina
     */
    nominaOrganos(idelemento, idorgano){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "organos/lista_organos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idorgano) != "undefined"){

                        // si coincide
                        if (data[i].Id == idorgano){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Organo + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Organo + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Organo + "</option>");
                    }

                }

        }});

    }

}
