<?php

/**
 *
 * organos/valida_organo.php
 *
 * @package     Diagnostico
 * @subpackage  Organos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el nombre de un órgano y
 * verifica que no se encuentre declarado en la base
 * retorna un array json con el número de registros
 * encontrados
 *
*/

// incluimos e instanciamos la clase
require_once ("organos.class.php");
$organo = new Organos();

// obtenemos el número de registros
$registros = $organo->verificaOrgano($_GET["organo"]);

// retornamos el número de registros
echo json_encode(array("Registros" => $registros));

?>