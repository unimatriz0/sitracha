<?php

/**
 *
 * organos/lista_organos.php
 *
 * @package     Diagnostico
 * @subpackage  Organos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de órganos
 *
*/

// incluimos e instanciamos la clase
require_once("organos.class.php");
$organos = new Organos();

// obtenemos la nómina
$nomina = $organos->nominaOrganos();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_organo,
                        "Organo" => $organo);

}

// retornamos el vector
echo json_encode($jsondata);

?>