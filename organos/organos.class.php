<?php

/**
 *
 * Class Organos | organos/organos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Organos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla el diccionario de órganos de transplante
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Organos{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdOrgano;              // clave del órgano
    protected $Organo;                // nombre del órgano
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdOrgano = 0;
        $this->Organo = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdOrgano($idorgano){
        $this->IdOrgano = $idorgano;
    }
    public function setOrgano($organo){
        $this->Organo = $organo;
    }

    // métodos de retorno de valores
    public function getIdOrgano(){
        return $this->IdOrgano;
    }
    public function getOrgano(){
        return $this->Organo;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que retorna en un array asociativo la nómina completa de
     * órganos y sus claves
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaOrganos(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.organos.id AS id_organo,
                            diagnostico.organos.organo AS organo,
                            diagnostico.organos.id_usuario AS id_usuario,
                            cce.responsables.usuario AS usuario,
                            DATE_FORMAT(diagnostico.organos.fecha, '%d/%m/%Y') AS fecha_alta
                     FROM diagnostico.organos INNER JOIN cce.responsables ON diagnostico.organos.id_usuario = cce.responsables.id
                     ORDER BY diagnostico.organos.organo;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método llamado en las altas, recibe como parámetro el nombre de un
     * órgano, lo busca en la base y retorna el número de registros
     * encontrados.
     * Utilizado para evitar el ingreso de registros duplicados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $organo - nombre del órgano a verificar
     * @return int $registros - número de registros encontrados
     */
    public function verificaOrgano($organo){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.organos.id) AS registros
                     FROM diagnostico.organos
                     WHERE diagnostico.organos.organo = '$organo';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($fila);

        // retornamos el número de registros
        return $registros;

    }

    /**
     * Método que según el caso llama la consulta de edición o inserción
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idorgano - clave del registro insertado / editado
     */
    public function grabaOrgano(){

        // si está insertando
        if ($this->IdOrgano == 0){
            $this->nuevoOrgano();
        } else {
            $this->editaOrgano();
        }

        // retornamos la clave
        return $this->IdOrgano;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoOrgano(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.organos
                            (organo,
                             id_usuario)
                            VALUES
                            (:organo,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":organo", $this->Organo);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asignamos la clave
        $this->IdOrgano = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaOrgano(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.organos SET
                            organo = :organo,
                            id_usuario = :id_usuario
                     WHERE diagnostico.organos.id = :id_organo;";

         // asignamos la consulta
         $psInsertar = $this->Link->prepare($consulta);

         // asignamos los valores
         $psInsertar->bindParam(":organo", $this->Organo);
         $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
         $psInsertar->bindParam(":id_organo", $this->IdOrgano);

         // ejecutamos la consulta
         $psInsertar->execute();

         // asignamos la clave
         $this->IdOrgano = $this->Link->lastInsertId();

    }

}