/*

    Nombre: historias.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 22/07/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de impresión
                 de las historias clínicas

*/

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de impresión de las
 * historias clínicas
 */
class Historias {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // declaración de variables
        this.Protocolo = 0;
        this.layerFormulario = "";
        this.layerPresentacion = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al imprimir una historia, presenta el
     * formulario con la selección del tipo de historia a
     * imprimir
     */
    pideHistoria(){

        // reinicia la sesión
        sesion.reiniciar();

        // verifica que esté editando un registro
        if (document.getElementById("protocolo_paciente").value == ""){

            // presenta el mensaje y retorna
            var mensaje = "Debe tener un registro en pantalla para<br>";
            mensaje += "poder imprimir la Historia Clínica";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si está editando
        } else {

            // asigna en la clase
            this.Protocolo = document.getElementById("protocolo_paciente").value;

        }

        // abre el layer emergente
        this.layerFormulario = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    title: 'Impresión de Historias',
                    draggable: 'title',
                    repositionOnContent: true,
                    theme: 'TooltipBorder',
                    width: 400,
                    ajax: {
                        url: 'historias/form_historias.html',
                        reload: 'strict'
                    }
                });
        this.layerFormulario.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar
     */
    Cancelar(){

        // reinicia la sesión
        sesion.reiniciar();

        // simplemente destruimos el layer
        this.layerFormulario.destroy();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón imprimir, verifica
     * se halla seleccionado un criterio y llama la rutina
     * correspondiente
     */
    Seleccionar(){

        // reinicia la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no declaró ningún criterio
        if (document.getElementById("tipo_historia").value == 0){

            mensaje = "Debe indicar un criterio de agrupameiento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si seleccionó por visita
        } else if (document.getElementById("tipo_historia").value == 1){

            // redirige
            this.historiaVisita();

        // si seleccionó por estudio
        } else if (document.getElementById("tipo_historia").value == 2){

            // redirige
            this.historiaEstudio();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente con la historia clínica agrupada
     * por visita
     */
    historiaVisita(){

        // destruimos el layer del formulario
        this.layerFormulario.destroy();

        // reiniciamos la sesión
        sesion.reiniciar();

        // ahora llamamos el layer emergente que va a
        // presentar la historia clínica
        this.layerPresentacion = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Historia Clínica',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    ajax: {
                        url: 'historias/historia_visita.php?protocolo='+this.Protocolo,
                        reload: 'strict'
                    }
            });
        this.layerPresentacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente con la historia clínica agrupada
     * por estudio
     */
    historiaEstudio(){

        // destruimos el layer del formulario
        this.layerFormulario.destroy();

        // reiniciamos la sesión
        sesion.reiniciar();

        // ahora llamamos el layer emergente que va a
        // presentar la historia clínica
        this.layerPresentacion = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Historia Clínica',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    ajax: {
                        url: 'historias/historia_estudio.php?protocolo='+this.Protocolo,
                        reload: 'strict'
                    }
            });
        this.layerPresentacion.open();

    }

}
