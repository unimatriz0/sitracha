<?php

/**
 *
 * Class Historia | historia/historia_visita.class.php
 *
 * @package     Diagnostico
 * @subpackage  Historias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Clase que genera el documento con la historia clínica agrupada
 * por visita
 *
*/

// incluimos la clase pdf
require_once ("pdf.class.php");

// inclusión de archivos
require_once ("../clases/conexion.class.php");
require_once ("../clases/herramientas.class.php");
require_once ("../visitas/visitas.class.php");
require_once ("../agudo/agudo.class.php");
require_once ("../pacientes/pacientes.class.php");
require_once ("../tratamiento/adversos.class.php");
require_once ("../enfermedades/enfermedades.class.php");
require_once ("../transfusiones/transfusiones.class.php");
require_once ("../transplantes/transplantes.class.php");
require_once ("../antecedentes/antecedentes.class.php");
require_once ("../familiares/familiares.class.php");
require_once ("../disautonomia/disautonomia.class.php");
require_once ("../digestivo/digestivo.class.php");
require_once ("../clasificacion/clasificacion.class.php");
require_once ("../fisico/fisico.class.php");
require_once ("../sintomas/sintomas.class.php");
require_once ("../cardiovascular/cardiovascular.class.php");
require_once ("../ecocardio/ecocardio.class.php");
require_once ("../electro/electro.class.php");
require_once ("../ergometria/ergometria.class.php");
require_once ("../holter/holter.class.php");
require_once ("../rx/rx.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/*

    Atención, esta clase extiende la clase tfpdf que a su vez extiende
    la clase fpdf, la diferencia es que permite definir y generar documentos
    pdf con página de códigos UTF8, el funcionamiento es el mismo que en
    la clase original.

    Sin embargo, la clase para ahorrar espacio regenera las fuentes que
    son utilizadas en el documento, si migramos el sistema y cambia el
    path de la aplicación arroja un error señalando que no encuentra las
    fuentes.

    Para obligar a que regenere las fuentes basta con eliminar todos los
    archivos dat y aquellos php que tengan nombres de fuentes del directorio
    /font/unifont/ dejando solamente los archivos ttf y el archivo ttfonts.php
    que es el que se encarga de generar las fuentes

    Si se desean incluir otras fuentes en el documento, bastaría con
    copiar los archivos ttf en este directorio y luego al incluirlos en
    el documento el sistema se encarga automáticamente de generar los dat

*/

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
class Historia {

    // declaración de variables
    protected $Docummento;            // objeto pdf
    protected $Protocolo;             // protocolo del paciente
    protected $Link;                  // puntero a la base de datos
    protected $Encabezado;            // texto de los encabezados
    protected $Utilidades;            // clase de herramientas
    protected $Visitas;               // clase de las visitas
    protected $DatosAgudo;            // clase de antecedentes
    protected $Pacientes;             // clase de pacientes
    protected $Adversos;              // clase de efectos adversos
    protected $Enfermedades;          // clase de enfermedades
    protected $Transfusiones;         // clase de transfusiones
    protected $Transplantes;          // clase de transplantes
    protected $AntToxicos;            // antecedentes tóxicos
    protected $AntFamiliares;         // antecedentes familiares
    protected $AntDisautonomia;       // clase de disautonomía
    protected $AntDigestivo;          // clase de compromiso digestivo
    protected $DatosClasificacion;    // clase de la clasificación
    protected $DatosFisico;           // datos del examan físico
    protected $DatosSintomas;         // síntomas de cada visita
    protected $DatosCardio;           // aparato cardiovascular
    protected $DatosEco;              // datos del ecocardiograma
    protected $DatosElectro;          // datos del electrocardiograma
    protected $DatosErgo;             // datos de la ergometría
    protected $DatosHolter;           // datos del holter
    protected $DatosRx;               // datos de las radiografías
    protected $Alto;                  // alto de línea

    /**
     * Constructor de la clase, recibe como parámetro
     * el número de protocolo
     */
    public function __construct($protocolo){

        // inicializamos las variables
        $this->Link = new Conexion();
        $this->Utilidades = new Herramientas();
        $this->Visitas = new Visitas();
        $this->DatosAgudo = new Agudo();
        $this->Pacientes = new Pacientes();
        $this->Adversos = new Adversos();
        $this->Enfermedades = new Enfermedades();
        $this->Transfusiones = new Transfusiones();
        $this->Transplantes = new Transplantes();
        $this->AntToxicos = new Antecedentes();
        $this->AntFamiliares = new Familiares();
        $this->AntDisautonomia = new Disautonomia();
        $this->AntDigestivo = new Digestivo();
        $this->DatosClasificacion = new Clasificacion();
        $this->DatosFisico = new Fisico();
        $this->DatosSintomas = new Sintomas();
        $this->DatosCardio = new Cardiovascular();
        $this->DatosEco = new Ecocardio();
        $this->DatosElectro = new Electro();
        $this->DatosErgo = new Ergometria();
        $this->DatosHolter = new Holter();
        $this->DatosRx = new Rx();
        $this->Protocolo = $protocolo;
        $this->Encabezado = "";
        $this->Alto = 8;

        // generamos el documento
        $this->initHistoria();

    }

    /**
     * Destructor de la clase
     */
    function __destruct() {

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que inicializa el documento
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    public function initHistoria(){

        // llamamos al constructor de la clase padre
        $this->Documento = new PDF();

        // obtenemos los datos de la institución
        $this->Documento->Inicializar();

        // establecemos las propiedades
        $this->Documento->SetMargins(4,6,1.5);
        $this->Documento->SetAuthor("Claudio Invernizzi");
        $this->Documento->SetCreator("INP - Fatala Chaben");
        $this->Documento->SetSubject("Historias Clínicas", true);
        $this->Documento->SetTitle("Historia por Visita", true);
        $this->Documento->SetAutoPageBreak(true, 10);

        // agrega una fuente unicode
        $this->Documento->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->Documento->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // establecemos el alias para el número total de páginas
        $this->Documento->AliasNbPages();

        // presenta el título del documento
        $this->Caratula();

        // presenta los datos de filiación
        $this->DatosFiliacion();

        // presenta los antecedentes patológicos
        $this->Antecedentes();

        // presenta los antecedentes tóxicos
        $this->Toxicos();

        // presenta los antecedentes familiares
        $this->Familiares();

        // presenta la disautonomía
        $this->Disautonomia();

        // presenta el compromiso digestivo
        $this->Digestivo();

        // imprime las visitas
        $this->ImprimeVisitas();

        // cierra y graba el documento
        $this->Documento->Output("../temp/historia.pdf","F");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que arma la carátula del documento
     */
    protected function Caratula(){

        // agregamos la página
        $this->Documento->AddPage('P');

        // presenta el logo del instituto con 300 pixeles
        $this->Documento->Image('../imagenes/logo_fatala.jpg', 60, 40, 80,60);

        // fija la fuente
        $this->Documento->SetFont("DejaVu", "B", 18);

        // posicionamos el cursor
        $this->Documento->SetY(150);

        // presenta el título
        $texto = "Instituto Nacional de Parasitología";
        $this->Documento->MultiCell(0, $this->Alto, $texto, 0, 'C');
        $texto = "Dr. Mario Fatala Chaben";
        $this->Documento->MultiCell(0, $this->Alto, $texto, 0, 'C');
        $texto = "Departamento de Clínica Médica";
        $this->Documento->MultiCell(0, $this->Alto, $texto, 0, 'C');

        // reducimos la fuente
        $this->Documento->SetFont("DejaVu", "B", 16);

        // presenta la descripción
        $texto = "Historia Clínica N°: $this->Protocolo";
        $this->Documento->MultiCell(0, $this->Alto + 4, $texto, 0, 'C');

        // reducimos la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta la fecha de impresión
        $this->Documento->MultiCell(0, $this->Alto + 4, "Buenos Aires " . $this->Utilidades->fechaLetras(date('d/m/Y')), 0, 'C');

        // fija la fuente normal
        $this->Documento->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Documento->AddPage();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta los datos de filiación del paciente
     */
    protected function DatosFiliacion(){

        // obtenemos los datos del paciente
        $this->Pacientes->getDatosPaciente($this->Protocolo);

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->Documento->Cell(100, $this->Alto, "Datos de Filiación", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // insertamos un separador
        $this->Documento->Ln(10);

        // presenta el nombre de la institución
        $texto = "Institución: " . $this->Pacientes->getLaboratorio();
        $this->Documento->Cell(200, $this->Alto, $texto, 0, 1, 'L');

        // presenta la historia clínica
        $texto = "Historia Clínica: " . $this->Pacientes->getHistoria();
        $this->Documento->Cell(80, $this->Alto, $texto, 0, 0, 'L');

        // presenta el protocolo
        $texto = "Protocolo: " . $this->Pacientes->getProtocolo();
        $this->Documento->Cell(80, $this->Alto, $texto, 0, 1, 'L');

        // presenta los datos del paciente
        $texto = "Nombre y Apellido: " . $this->Pacientes->getNombre() . " " . $this->Pacientes->getApellido();
        $this->Documento->Cell(130, $this->Alto, $texto, 0, 0, "L");

        // presenta el documento
        $texto = $this->Pacientes->getTipoDocumento() . " - " . $this->Pacientes->getDocumento();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, "L");

        // presenta la fecha de nacimiento
        $texto = "Fecha Nacimiento: " . $this->Pacientes->getFechaNacimiento();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta la edad
        $texto = "Edad: " . $this->Pacientes->getEdad() . " años";
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta el sexo
        $texto = "Sexo: " . $this->Pacientes->getSexo();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta el estado civil
        $texto = "Estado Civil: " . $this->Pacientes->getEstadoCivil();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta el número de hijos
        $texto = "Hijos: " . $this->Pacientes->getHijos();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta la dirección postal
        $texto = "Dirección: " . $this->Pacientes->getDireccion();
        $this->Documento->Cell(200, $this->Alto, $texto, 0, 1, 'L');

        // presenta los datos de residencia
        $texto = "Residencia - País: " . $this->Pacientes->getPaisResidencia();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');
        $texto = "Jurisdicción: " . $this->Pacientes->getLocResidencia();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');
        $texto = "Localidad: " . $this->Pacientes->getLocResidencia();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta el número de teléfono
        $texto = "Teléfono: " . $this->Pacientes->getTelefono();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta el celular
        $texto = "Móvil: " . $this->Pacientes->getCelular();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta el mail
        $texto = "Mail: " . $this->Pacientes->getMail();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta los datos de nacimiento
        $texto = "Nacimiento - País: " . $this->Pacientes->getPaisNacimiento();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');
        $texto = "Jurisdicción: " . $this->Pacientes->getJurisdiccionNacimiento();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');
        $texto = "Localidad: " . $this->Pacientes->getLocNacimiento();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta los datos de la madre
        $texto = "Madre - País: " . $this->Pacientes->getPaisMadre();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');
        $texto = "Jurisdicción: " . $this->Pacientes->getJurisdiccionMadre();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');
        $texto = "Localidad: " . $this->Pacientes->getLocalidadMadre();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta si la madre es positiva
        $texto = "Madre Positiva: " . $this->Pacientes->getMadrePositiva();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta la ocupación
        $texto = "Ocupación: " . $this->Pacientes->getOcupacion();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta la obra social
        $texto = "Obra Social: " . $this->Pacientes->getObraSocial();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta el motivo de consulta
        $texto = "Motivo de Consulta: " . $this->Pacientes->getMotivo();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0, 'L');

        // presenta la derivación
        $texto = "Derivado por: " . $this->Pacientes->getDerivacion();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta si recibió tratamiento
        $texto = "Recibió Tratamiento: " . $this->Pacientes->getTratamiento();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1, 'L');

        // presenta los comentarios
        if ($this->Pacientes->getComentarios() != ""){

            // presentamos el texto html
            $texto = "Comentarios: " . $this->Pacientes->getComentarios();
            $this->Documento->WriteHTML($texto);

            // insertamos un salto
            $this->Documento->Ln($this->Alto);

        }

        // presenta el usuario y la fecha de alta del registro
        $texto = "Ingresó: " . $this->Pacientes->getUsuario();
        $texto .= " Fecha Alta: " . $this->Pacientes->getFechaAlta();
        $this->Documento->Cell(200, $this->Alto, $texto, 0, 1, 'L');

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta los antecedentes patológicos
     */
    protected function Antecedentes(){

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->Documento->Cell(100, $this->Alto, "Antecedentes", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // inserta un salto
        $this->Documento->Ln(10);

        // si recibió tratamiento obtiene los datos y
        // los efectos adversos
        if ($this->Pacientes->getTratamiento() == "Si"){

            // obtenemos la nómina de tratamiento y de efectos adversos
            $nomina = $this->Adversos->adversosTratamiento($this->Protocolo);

            // si hay registros
            if (count($nomina) != 0){

                // presenta el título
                $this->Documento->Cell(100, $this->Alto, "Tratamientos Recibidos", 0, 1);

                // insertamos un separador
                $this->Documento->Ln(10);

                // presentamos los títulos
                $this->Documento->Cell(70, $this->Alto, "Droga", 1, 0, 'L');
                $this->Documento->Cell(25, $this->Alto, "Dosis", 1, 0, 'L');
                $this->Documento->Cell(30, $this->Alto, "Inicio", 1, 0, 'L');
                $this->Documento->Cell(30, $this->Alto, "Fin", 1, 0, 'L');
                $this->Documento->Cell(50, $this->Alto, "Adverso", 1, 0, 'L');
                $this->Documento->Cell(25, $this->Alto, "Fecha", 1, 1, 'L');

                // presentamos la nómina de tratamientos y efectos
                foreach($nomina AS $registro){

                    // obtenemos el registro
                    extract($registro);

                    // lo presentamos
                    $this->Documento->Cell(70, $this->Alto, $droga, 1, 0, 'L');
                    $this->Documento->Cell(25, $this->Alto, $dosis, 1, 0, 'L');
                    $this->Documento->Cell(30, $this->Alto, $inicio, 1, 0, 'L');
                    $this->Documento->Cell(30, $this->Alto, $fin, 1, 0, 'L');
                    $this->Documento->Cell(50, $this->Alto, $adverso, 1, 0, 'L');
                    $this->Documento->Cell(25, $this->Alto, $fecha_adverso, 1, 1, 'L');

                }

            }

            // inserta un separador
            $this->Documento->Ln(10);

        }

        // si declaró enfermedades las presenta
        $nomina = $this->Enfermedades->enfermedadesPaciente($this->Protocolo);

        // si hay enfermedades
        if (count($nomina) != 0){

            // presenta el título
            $this->Documento->Cell(100, $this->Alto, "Enfermedades Declaradas", 0, 1);

            // inserta un separador
            $this->Documento->Ln(10);

            // define los encabezados
            $this->Documento->Cell(25, $this->Alto, "", 0, 0);
            $this->Documento->Cell(100, $this->Alto, "Enfermedad", 1, 0);
            $this->Documento->Cell(40, $this->Alto, "Fecha", 1, 1, 'C');

            // recorre el vector
            foreach($nomina AS $registro){

                // obtenemos el registro
                extract($registro);

                // lo presentamos
                $this->Documento->Cell(25, $this->Alto, "", 0, 0);
                $this->Documento->Cell(100, $this->Alto, $enfermedad, 1, 0);
                $this->Documento->Cell(40, $this->Alto, $fecha, 1, 1, 'C');

            }

            // inserta un salto
            $this->Documento->Ln(10);

        }

        // obtenemos las transfusiones
        $nomina = $this->Transfusiones->nominaTransfusiones($this->Protocolo);

        // si declaró transfusiones las presenta
        if (count($nomina) != 0){

            // presenta el título
            $this->Documento->Cell(100, $this->Alto, "Tansfusiones Recibidas");

            // inserta un separador
            $this->Documento->Ln(10);

            // presentamos los encabezados
            $this->Documento->Cell(25, $this->Alto, "", 0, 0);
            $this->Documento->Cell(30, $this->Alto, "Fecha", 1, 0, 'C');
            $this->Documento->Cell(80, $this->Alto, "Localidad", 1, 0);
            $this->Documento->Cell(80, $this->Alto, "Motivo", 1, 1);

            // recorremos el vector
            foreach($nomina AS $registro){

                // obtenemos el registro
                extract($registro);

                // lo presentamos
                $this->Documento->Cell(25, $this->Alto, "", 0, 0);
                $this->Documento->Cell(30, $this->Alto, $fecha_transfusion, 1, 0, 'C');
                $this->Documento->Cell(80, $this->Alto, $localidad, 1, 0);
                $this->Documento->Cell(80, $this->Alto, $motivo, 1, 1);

            }

            // insertamos un separador
            $this->Documento->Ln(10);

        }

        // obtenemos la nómina de transplantes
        $nomina = $this->Transplantes->nominaTransplantes($this->Protocolo);

        // si declaró transplantes las presenta
        if (count($nomina) != 0){

            // presenta el título
            $this->Documento->Cell(100, $this->Alto, "Transplantes Recibidos", 0, 1);

            // inserta un separador
            $this->Documento->Ln(10);

            // presenta los encabezados
            $this->Documento->Cell(25, $this->Alto, "", 0, 0);
            $this->Documento->Cell(30, $this->Alto, "Fecha", 1, 0, 'C');
            $this->Documento->Cell(30, $this->Alto, "Positivo", 1, 0, 'C');
            $this->Documento->Cell(80, $this->Alto, "Organo", 1,1);


            // recorremos el vector
            foreach($nomina AS $registro){

                // obtenemos el registro
                extract($registro);

                // lo presentamos
                $this->Documento->Cell(25, $this->Alto, "", 0, 0);
                $this->Documento->Cell(30, $this->Alto, $fecha_transplante, 1, 0, 'C');
                $this->Documento->Cell(30, $this->Alto, $positivo, 1, 0, 'C');
                $this->Documento->Cell(80, $this->Alto, $organo, 1,1);

            }

            // insertamos un separador
            $this->Documento->Ln(10);

        }

        // presenta si tuvo antecedentes de chagas
        $this->DatosAgudo->getDatosAgudo($this->Protocolo);
        if ($this->DatosAgudo->getSintomas() == 0){
            $texto = "Antecedentes de Chagas Agudo: No";
        } else {
            $texto = "Antecedentes de Chagas Agudo: Si";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0,1);

        // presenta los comentarios
        if (!empty($this->DatosAgudo->getObservaciones())){
            $texto = "Comentarios: " . $this->DatosAgudo->getObservaciones();
            $this->Documento->WriteHTML($texto);
            $this->Documento->Ln($this->Alto);
        }

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta los antecedentes tóxicos del paciente
     */
    protected function Toxicos(){

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->Documento->Cell(100, $this->Alto, "Antecedentes Tóxicos", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // obtiene el registro
        $this->AntToxicos->getDatosAntecedente($this->Protocolo);

        // inserta un separador
        $this->Documento->Ln(10);

        // presenta si fuma
        if ($this->AntToxicos->getFuma() == 0){
            $texto = "Fuma: No";
        } else {
            $texto = "Fuma: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si fuma el número de cigarrillos
        if ($this->AntToxicos->getFuma() == 1){
            $texto = "Cigarrillos Diarios: " . $this->AntToxicos->getCigarrillos();
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);
        }

        // si es ex fumador
        if ($this->AntToxicos->getExFumador() == 1){

            // presenta el texto
            $this->Documento->Cell(50, $this->Alto, "Ex-Fumador", 0, 0);

            // si es ex fumador, cuando dejó
            $texto = "Abandonó el: " . $this->AntToxicos->getDejo();
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

            // si es ex fumador, cuantos años fumó
            $texto = "Fumó durante " . $this->AntToxicos->getAnios() . " años";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        }

        // si bebe alcohol
        if ($this->AntToxicos->getAlcohol() == 1){
            $texto = "Bebe Alcohol";
        } else {
            $texto = "No Bebe";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si toma alcohol cuantos libros a la semana
        if ($this->AntToxicos->getAlcohol() == 1){

            // cuantos libros a la semana
            $texto = $this->AntToxicos->getLitros() . " litros a la semana";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

            // si toma alcohol el tipo de bebida
            $texto = "Tipo de Bebida: " . $this->AntToxicos->getBebida();
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        }

        // si tiene adicciones las presenta
        if (!empty($this->AntToxicos->getAdicciones())){

            // las presenta
            $texto = "Adicciones: " . $this->AntToxicos->getAdicciones();
            $this->Documento->WriteHTML($texto);

            // inserta un salto
            $this->Documento->Ln($this->Alto);

        }

        // inserta un salto de línea
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta los antecedentes familiares
     */
    protected function Familiares(){

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->Documento->Cell(100, $this->Alto, "Antecedentes Familiares", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // obtenemos el registro
        $this->AntFamiliares->getDatosFamiliares($this->Protocolo);

        // inserta un separador
        $this->Documento->Ln(10);

        // si hay antecedentes de muerte súbita
        if ($this->AntFamiliares->getSubita() == 0){
            $texto = "No presenta antecedentes de muerte súbita";
        } else {
            $texto = "Existen antecedentes de muerte súbita";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0);

        // si hay antecedentes de cardiopatía
        if ($this->AntFamiliares->getCardiopatia() == 0){
            $texto = "No presenta antecedentes de cardiopatía";
        } else {
            $texto = "Existen antecedentes de cardiopatía";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1);

        // si hay antecedentes de disfagia
        if ($this->AntFamiliares->getDisfagia() == 0){
            $texto = "No presenta antecedentes de Disfagia";
        } else {
            $texto = "Existen antecedentes de Disfagia";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 0);

        // si alguien usó marcapaso
        if ($this->AntFamiliares->getMarcapaso() == 0){
            $texto = "No hay antecedentes de uso de marcapaso";
        } else {
            $texto = "Existen familiares con uso de marcapaso";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1);

        // si no sabe
        if ($this->AntFamiliares->getNoSabe() == 1){
            $texto = "El paciente desconoce antecedentes familiares";
            $this->Documento->Cell(100, $this->Alto, $texto, 0, 0);
        }

        // si existen otras enfermedades crónicas
        if (!empty($this->AntFamiliares->getOtra())){

            // lo presenta
            $texto = "Otra Enfermedad Crónica: " . $this->AntFamiliares->getOtra();
            $this->Documento->Cell(200, $this->Alto, $texto, 0, 1);

        }

        // si no tiene
        if ($this->AntFamiliares->getNoTiene() == 1){
            $texto = "El paciente refiere que no existen antecedentes familiares";
            $this->Documento->Cell(100, $this->Alto, $texto, 0, 0);
        }

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta los antecedentes de disautonomía
     */
    protected function Disautonomia(){

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->Documento->Cell(100, $this->Alto, "Disautonomía", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // obtenemos el registro
        $this->AntDisautonomia->getDatosDisautonomia($this->Protocolo);

        // inserta un separador
        $this->Documento->Ln(10);

        // si presenta hipotensión
        if ($this->AntDisautonomia->getHipotension() == 0){
            $texto = "Hipotensión: No";
        } else {
            $texto = "Hipotensión: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta bradicardia
        if ($this->AntDisautonomia->getBradicardia() == 0){
            $texto = "Bradicardia: No";
        } else {
            $texto = "Bradicardia: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta astenia
        if ($this->AntDisautonomia->getAstenia() == 0){
            $texto = "Astenia: No";
        } else {
            $texto = "Astenia: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si no tiene
        if ($this->AntDisautonomia->getNoTiene() == 1){
            $texto = "No presenta síntomas";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);
        }

        // inserta un salto
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el compromiso digestivo
     */
    protected function Digestivo(){

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título
        $this->Documento->Cell(100, $this->Alto, "Compromiso Digestivo", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // obtenemos el registro
        $this->AntDigestivo->getDatosDigestivo($this->Protocolo);

        // inserta un separador
        $this->Documento->Ln(10);

        // si presenta disfagia
        if ($this->AntDigestivo->getDisfagia() == 0){
            $texto = "Disfagia: No";
        } else {
            $texto = "Disfagia: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta pirosis
        if ($this->AntDigestivo->getPirosis() == 0){
            $texto = "Pirosis: No";
        } else {
            $texto = "Pirosis: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta regurgitación
        if ($this->AntDigestivo->getRegurgitacion() == 0){
            $texto = "Regurgitación: No";
        } else {
            $texto = "Regurgitación: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta constipación
        if ($this->AntDigestivo->getConstipacion() == 0){
            $texto = "Constipación: No";
        } else {
            $texto = "Constipación: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta bolo
        if ($this->AntDigestivo->getBolo() == 0){
            $texto = "Bolo Fecal / Enemas: No";
        } else {
            $texto = "Bolo Fecal / Enemas: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si no tiene
        if ($this->AntDigestivo->getNoTiene() == 1){
            $texto = "No tiene";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);
        }

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la evolución del paciente agrupada
     * por visita
     */
    protected function ImprimeVisitas(){

        // obtenemos la nómina de visitas
        $entrevistas = $this->Visitas->nominaVisitas($this->Protocolo);

        // recorremos el vector
        foreach($entrevistas AS $registro){

            // obtenemos el registro
            extract($registro);

            // presenta la clasificación y el examen físico
            $this->Clasificacion($id);

            // presenta el examen físico
            $this->Fisico($id);

            // presenta los síntomas
            $this->Sintomas($id);

            // presenta el aparato cardiovascular
            $this->Cardiovascular($id);

            // presenta el ecocardiograma
            $this->Ecocardiograma($id);

            // presenta el electrocardiograma
            $this->Electro($id);

            // presenta la ergometría
            $this->Ergometria($id);

            // presenta el holter
            $this->Holter($id);

            // presenta los rayos x
            $this->Rx($id);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de una visita
     * y presenta los datos de clasificacion
     */
    protected function Clasificacion($id){

        // agregamos una página
        $this->Documento->AddPage();

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Entrevista en Consultorio", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // obtenemos los datos de la visita
        $this->DatosClasificacion->getDatosClasificacion($id);

        // insertamos un separador
        $this->Documento->Ln(10);

        // presenta la fecha de la visita
        $texto = "Fecha Visita: " . $this->DatosClasificacion->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el estadío
        $texto = "Clasificación Enfermedad de Chagas según Kuscknir: " . $this->DatosClasificacion->getEstadio();
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1);

        // presenta las observaciones
        if (!empty($this->DatosClasificacion->getObservaciones())){

            // lo presenta
            $texto = "Observaciones: " . $this->DatosClasificacion->getObservaciones();
            $this->Documento->WriteHTML($texto);

            // inserta un salto
            $this->Documento->Ln($this->Alto);

        }

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de una visita
     * y presenta los datos del examen físico
     */
    protected function Fisico($id){

        // obtenemos el registro
        $this->DatosFisico->getDatosVisita($id);

        // si no hay registro físico
        if ($this->DatosFisico->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Examen Físico", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // insertamos un separador
        $this->Documento->Ln(10);

        // presenta la fecha de la visita
        $texto = "Fecha Visita: " . $this->DatosFisico->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta la tensión arterial
        $texto = "TA: " . $this->DatosFisico->getTa();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el peso
        $texto = "Peso: " . $this->DatosFisico->getPeso() . " kg.";
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta la frecuencia cardíaca
        $texto = "FC: " . $this->DatosFisico->getFc();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // la saturación de oxígeno
        $texto = "Spo: " . $this->DatosFisico->getSpo() . " %";
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // la talla
        $texto = "Talla: " . $this->DatosFisico->getTalla() . " mts.";
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // el índice de masa corporal
        $texto = "BMI: " . $this->DatosFisico->getBmi();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta edema de miembros inferiores
        if ($this->DatosFisico->getEdema() == 0){
            $texto = "No presenta edema de miembros inferiores";
        } else {
            $texto = "Presenta edema de miembros inferiores";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1);

        // murmullo vensicular conservado
        if ($this->DatosFisico->getSp() == 0){
            $texto = "Murmullo Vesicular Conservado: No";
        } else {
            $texto = "Murmullo Vesicular Conservado: Si";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1);

        // murmullo vesicular disminuido
        if ($this->DatosFisico->getMvdisminuido() == 0){
            $texto = "Murmullo Vesicular Disminuido: No";
        } else {
            $texto = "Murmullo Vesicular Disminuido: Si";
        }
        $this->Documento->Cell(100, $this->Alto, $texto, 0, 1);

        // crepitantes
        if ($this->DatosFisico->getCrepitantes() == 0){
            $texto = "Crepitantes: No";
        } else {
            $texto = "Crepitantes: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // sibilancias
        if ($this->DatosFisico->getSibilancias() == 0){
            $texto = "Sibilancias: No";
        } else {
            $texto = "Sibilancias: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos de los síntomas de esa visita
     */
    protected function Sintomas($id){

        // obtenemos el registro
        $this->DatosSintomas->getDatosSintoma($id);

        // si no hay datos de síntomas
        if ($this->DatosSintomas->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Síntomas", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Documento->Ln(10);

        // presenta la fecha de la visita
        $texto = "Fecha Visita: " . $this->DatosSintomas->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el tipo de disnea
        $texto = "Disnea: " . $this->DatosSintomas->getDisnea();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta el tipo de palpitaciones
        $texto = "Palpitaciones: " . $this->DatosSintomas->getPalpitaciones();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta el tipo de dolor precordial
        $texto = "Dolor Precordial: " . $this->DatosSintomas->getPrecordial();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si tuvo pérdida de consciencia
        if ($this->DatosSintomas->getConciencia() == 0){
            $texto = "Pérdida de Consciencia: No";
        } else {
            $texto = "Pérdida de Consciencia: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si tuvo presíncope
        if ($this->DatosSintomas->getPresincope() == 0){
            $texto = "Presíncope: No";
        } else {
            $texto = "Presíncope: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si tuvo edema de miembros inferiores
        if ($this->DatosSintomas->getEdema() == 0){
            $texto = "Edema de miembros inferiores: No";
        } else {
            $texto = "Edema de miembros inferiores: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos del aparato cardiovascular
     */
    protected function Cardiovascular($id){

        // obtenemos el registro
        $this->DatosCardio->getDatosCardiovascular($id);

        // si no hay datos
        if ($this->DatosCardio->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Antecedentes Cardiovasculares", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Documento->Ln(10);

        // presenta la fecha de la visita
        $texto = "Fecha Visita: " . $this->DatosCardio->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta la auscultación
        $this->Documento->Cell(50, $this->Alto, "Auscultación: ", 0, 1);

        // si es normal
        if ($this->DatosCardio->getAuscultacion() == 0){
            $texto = "Auscultación Normal: No";
        } else {
            $texto = "Auscultación Normal: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si el ritmo es irregular
        if ($this->DatosCardio->getIrregular() == 0){
            $texto = "Ritmo Irregular: No";
        } else {
            $texto = "Ritmo Irregular: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // el tercer
        if ($this->DatosCardio->getTercer() == 0){
            $texto = "3er. R: No";
        } else {
            $texto = "3er. R: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // el cuarto
        if ($this->DatosCardio->getCuarto() == 0){
            $texto = "4to. R: No";
        } else {
            $texto = "4to. R: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // soplos sistólicos
        $this->Documento->Cell(50, $this->Alto, "Soplos Sistólicos: ", 0, 1);

        // si es eyectivo aórtico
        if ($this->DatosCardio->getEyectivo() == 0){
            $texto = "Eyectivo Aórtico: No";
        } else {
            $texto = "Eyectivo Aórtico: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si es regurgitativo mitral
        if ($this->DatosCardio->getRegurgitativo() == 0){
            $texto = "Regurgitativo Mitral: No";
        } else {
            $texto = "Regurgitativo Mitral: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si no tiene
        if ($this->DatosCardio->getSinSistolico() == 0){
            $texto = "No Tiene Splos Sistólicos";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);
        }

        // presenta los soplos diastólicos
        $this->Documento->Cell(50, $this->Alto, "Soplos Diastólicos: ", 0, 1);

        // si es aórtico
        if ($this->DatosCardio->getAortico() == 0){
            $texto = "Eyectivo Aórtico: No";
        } else {
            $texto = "Eyectivo Aórtico: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si es mitral
        if ($this->DatosCardio->getDiastolicoMitral() == 0){
            $texto = "Regurgitativo Mitral: No";
        } else {
            $texto = "Regurgitativo Mitral: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si no tiene
        if ($this->DatosCardio->getSinDiastolico() == 0){
            $texto = "No tiene soplos diastólicos";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);
        }

        // si hay hepatomegalia
        if ($this->DatosCardio->getHepatomegalia() == 0){
            $texto = "Hepatomegalia: No";
        } else {
            $texto = "Hepatomegalia: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si hay esplenomegalia
        if ($this->DatosCardio->getEsplenomegalia() == 0){
            $texto = "Esplenomegalia: No";
        } else {
            $texto = "Esplenomegalia: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si hay ingurgitación yugular
        if ($this->DatosCardio->getIngurgitacion() == 0){
            $texto = "Ingurgitación Yugular: No";
        } else {
            $texto = "Ingurgitación Yugular: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos del ecocardiograma si existen
     */
    protected function Ecocardiograma ($id){

        // obtenemos el registro
        $this->DatosEco->getDatosEcocardio($id);

        // si no hay datos de eco
        if ($this->DatosEco->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Ecocardiograma", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // insertamos un separador
        $this->Documento->Ln(10);

        // presenta la fecha
        $texto = "Fecha Visita: " . $this->DatosEco->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta si es normal
        if ($this->DatosEco->getNormal() == 0){
            $texto = "Normal: No";
        } else {
            $texto = "Normal: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el ddvi
        $texto = "DDVI: " . $this->DatosEco->getDdvi();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el dsvi
        $texto = "DSVI: " . $this->DatosEco->getDsvi();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el fac
        $texto = "FAC: " . $this->DatosEco->getFac();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el siv
        $texto = "SIV: " . $this->DatosEco->getSiv();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el pp
        $texto = "PP: " . $this->DatosEco->getPp();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el ai
        $texto = "AI: " . $this->DatosEco->getAi();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el ao
        $texto = "AO: " . $this->DatosEco->getAo();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el fey
        $texto = "FEY: " . $this->DatosEco->getFey();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta el ddvd
        $texto = "DDVD: " . $this->DatosEco->getDdvd();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el ad
        $texto = "AD: " . $this->DatosEco->getAd();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta la movilidad
        $texto = "Movilidad: " . $this->DatosEco->getMovilidad();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta el fsvi
        $texto = "FSVI: " . $this->DatosEco->getFsvi();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // insertamos un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos del electrocardiograma si existe
     */
    protected function Electro($id){

        // obtenemos el registro
        $this->DatosElectro->getDatosElectro($id);

        // si no hay datos de electro
        if ($this->DatosElectro->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Electrocardiograma", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // insertamos un separador
        $this->Documento->Ln(10);

        // presenta la fecha
        $texto = "Fecha Visita: " . $this->DatosElectro->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // presenta si es normal
        if ($this->DatosElectro->getNormal() == 0){
            $texto = "Normal: No";
        } else {
            $texto = "Normal: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta bradicardia sinusal
        if ($this->DatosElectro->getBradicardia() == 0){
            $texto = "Bradicardia Sinusal: No";
        } else {
            $texto = "Bradicardia Sinusal: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // la fc
        $texto = "FC: " . $this->DatosElectro->getFc();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // el tipo de arritmia
        $texto = "Arritmia: " . $this->DatosElectro->getArritmia();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // el valor de qrs
        $texto = "QRS: " . $this->DatosElectro->getQrs();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // el eje qrs
        $texto = "Eje QRS: " . $this->DatosElectro->getEjeqrs();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // el valor de pr
        $texto = "PR: " . $this->DatosElectro->getPr();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta brd
        if ($this->DatosElectro->getBrd() == 0){
            $texto = "BRD de BG: No";
        } else {
            $texto = "BRD de BG: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta brd moderado
        if ($this->DatosElectro->getBrdModerado() == 0){
            $texto = "BRD Moderado: No";
        } else {
            $texto = "BRD Moderado: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta bcrd
        if ($this->DatosElectro->getBcrd() == 0){
            $texto = "BCRD: No";
        } else {
            $texto = "BCRD: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si tiene tendencia al hbai
        if ($this->DatosElectro->getTendenciaHbai() == 0){
            $texto = "Tendencia al HBAI: No";
        } else {
            $texto = "Tendencia al HBAI: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta hbai
        if ($this->DatosElectro->getHbai() == 0){
            $texto = "HBAI: No";
        } else {
            $texto = "HBAI: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta tciv
        if ($this->DatosElectro->getTciv() == 0){
            $texto = "TCIV: No";
        } else {
            $texto = "TCIV: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta bcri
        if ($this->DatosElectro->getBcri() == 0){
            $texto = "BCRI: No";
        } else {
            $texto = "BCRI: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta bav
        if ($this->DatosElectro->getBav1g() == 0){
            $texto = "Bav1g: No";
        } else {
            $texto = "Bav1g: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta bav
        if ($this->DatosElectro->getBav2g() == 0){
            $texto = "Bav2g: No";
        } else {
            $texto = "Bav2g: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta bav
        if ($this->DatosElectro->getBav2g() == 0){
            $texto = "Bav3g: No";
        } else {
            $texto = "Bav3g: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta fibrosis
        if ($this->DatosElectro->getFibrosis() == 0){
            $texto = "Fibrosis: No";
        } else {
            $texto = "Fibrosis: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta aumento ai
        if ($this->DatosElectro->getAumentoAi() == 0){
            $texto = "Aumento AI: No";
        } else {
            $texto = "Aumento AI: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si presenta wpv
        if ($this->DatosElectro->getWpv() == 0){
            $texto = "Wpv: No";
        } else {
            $texto = "Wpv: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 0);

        // si presenta hvi
        if ($this->DatosElectro->getHvi() == 0){
            $texto = "HVI: No";
        } else {
            $texto = "HVI: Si";
        }
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si no tiene
        if ($this->DatosElectro->getNotiene() == 1){
            $this->Documento->Cell(50, $this->Alto, "No Tiene", 0, 0);
        }

        // presenta el tipo de transforno
        $texto = "Transtornos de la Repolarización: " . $this->DatosElectro->getRepolarizacion();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // si no tiene transtornos
        if ($this->DatosElectro->getSinTranstornos() == 1){
            $texto = "Sin Transtornos de la repolarización";
            $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);
        }

        // inserta un separador
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos de la ergometría si existe
     */
    protected function Ergometria($id){

        // obtenemos el registro
        $this->DatosErgo->getDatosErgometria($id);

        // si no hay datos de ergometría
        if ($this->DatosErgo->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Ergometría", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // insertamos un separador
        $this->Documento->Ln(10);

        // presenta la fecha
        $texto = "Fecha Visita: " . $this->DatosErgo->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta la frecuencia basal
        $texto = "Frecuencia Basal: " . $this->DatosErgo->getFcbasal();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta la frecuencia máxima
        $texto = "Frecuencia Máxima: " . $this->DatosErgo->getFcmax();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // presenta la tensión basal
        $texto = "Tensión Máxima: " . $this->DatosErgo->getTabasal();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta la tensión máxima
        $texto = "Tensión Máxima: " . $this->DatosErgo->getTamax();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // presenta los kilográmetros
        $texto = "Kgm: " . $this->DatosErgo->getKpm();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta el itt
        $texto = "ITT: " . $this->DatosErgo->getItt();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si existen presenta las observaciones
        if (!empty($this->DatosErgo->getObservaciones())){

            // lo presenta
            $texto = "Observaciones: " . $this->DatosErgo->getObservaciones();
            $this->Documento->WriteHTML($texto);

            // inserta un salto
            $this->Documento->Ln($this->Alto);

        }

        // inserta un salto
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos del holter si existe
     */
    protected function Holter($id){

        // obtenemos el registro
        $this->DatosHolter->getDatosHolter($id);

        // si no hay datos de holter
        if ($this->DatosHolter->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Holter", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Documento->Ln(10);

        // presenta la fecha de la visita
        $texto = "Fecha Visita: " . $this->DatosHolter->getFecha();
        $this->Documento->Cell(50, $this->Alto, $texto, 0, 1);

        // presenta la frecuencia media
        $texto = "Frecuencia Media: " . $this->DatosHolter->getMedia();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta la frecuencia mínima
        $texto = "Frecuencia Mínima: " . $this->DatosHolter->getMinima();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta la frecuencia máxima
        $texto = "Frecuencia Máxima: " . $this->DatosHolter->getMaxima();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // presenta el número de latidos
        $texto = "Latidos Totales: " . $this->DatosHolter->getLatidos();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // presenta si hubo eventos
        if ($this->DatosHolter->getEv() == 0){

            // presenta que no hubo
            $texto = "No presenta eventos ventriculares";
            $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si hubo eventos
        } else {

            // presenta el número de eventos
            $texto = "N° Eventos Ventriculares: " . $this->DatosHolter->getNev();
            $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

            // presenta la tasa de eventos
            $texto = "Tasa de Eventos: " . $this->DatosHolter->getTasaEv();
            $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        }

        // presenta si hubo eventos supraventriculares
        if ($this->DatosHolter->getEsv() == 0){

            // presenta el texto
            $texto = "No presenta eventos supraventriculares";
            $this->Documento->Cell(120, $this->Alto, $texto, 0, 1);

        // si hubo
        } else {

            // presenta el número de eventos
            $texto = "N° Eventos Supraventriculares: " . $this->DatosHolter->getNesv();
            $this->Documento->Cell(120, $this->Alto, $texto, 0, 0);

            // presenta la tasa de eventos supraventriculares
            $texto = "Tasa de Eventos: " . $this->DatosHolter->getTasaesv();
            $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        }

        // presenta si es normal
        if ($this->DatosHolter->getNormal() == 0){
            $texto = "Normal: No";
        } else {
            $texto = "Normal: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hubo bradicardia
        if ($this->DatosHolter->getBradicardia() == 0){
            $texto = "Bradicardia: No";
        } else {
            $texto = "Bradicardia: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hubo respuesta cronotrópica
        if ($this->DatosHolter->getRtaCronotopica() == 0){
            $texto = "Respuesta Cronotrópica: No";
        } else {
            $texto = "Respuesta Cronotrópica: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si hubo arritmia severa
        if ($this->DatosHolter->getArritmiaSevera() == 0){
            $texto = "Arritmia Severa: No";
        } else {
            $texto = "Arritmia Severa: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hubo arritmia simple
        if ($this->DatosHolter->getArritmiaSimple() == 0){
            $texto = "Arritmia Simple: No";
        } else {
            $texto = "Arritmia Simple: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hubo arritmia supraventricular
        if ($this->DatosHolter->getArritmiaSupra() == 0){
            $texto = "Arritmia Supraventricular: No";
        } else {
            $texto = "Arritmia Supraventricular: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si hubo bloqueo de rama
        if ($this->DatosHolter->getBdeRama() == 0){
            $texto = "Bloqueo de Rama: No";
        } else {
            $texto = "Bloqueo de Rama: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hubo bav2g
        if ($this->DatosHolter->getBav2g() == 0){
            $texto = "Bav2g: No";
        } else {
            $texto = "Bav2g: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hubo disociación
        if ($this->DatosHolter->getDisociacion() == 0){
            $texto = "Disociación: No";
        } else {
            $texto = "Disociación: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si hubo comentarios
        if (!empty($this->DatosHolter->getComentarios())){

            // lo presenta
            $texto = "Comentarios: " . $this->DatosHolter->getComentarios();
            $this->Documento->WriteHTML($texto);

            // inserta un salto
            $this->Documento->Ln($this->Alto);

        }

        // inserta un salto
        $this->Documento->Ln(10);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave de la visita
     * Método que recibe como parámetro la clave de la visita
     * y presenta los datos de la radiografía si existe
     */
    protected function Rx($id){

        // obtenemos el registro
        $this->DatosRx->getDatosRx($id);

        // si no hay datos de rx
        if ($this->DatosRx->getId() == 0){
            return;
        }

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "B", 14);

        // presenta el título de la visita
        $this->Documento->Cell(100, $this->Alto, "Rx", 1, 1, "C");

        // setea la fuente
        $this->Documento->SetFont("DejaVu", "", 12);

        // inserta un separador
        $this->Documento->Ln(10);

        // presenta la fecha
        $texto = "Fecha Administración: " . $this->DatosRx->getFecha();
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si hay ict
        if ($this->DatosRx->getIct() == 0){
            $texto = "ICT: No";
        } else {
            $texto = "ICT: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hay cardiomegalia
        if ($this->DatosRx->getCardiomegalia() == 0){
            $texto = "Cardiomegalia: No";
        } else {
            $texto = "Cardiomegalia: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hay secreciones
        if ($this->DatosRx->getPleuro() == 0){
            $texto = "Sec. Pleuro / Pulmonar: No";
        } else {
            $texto = "Sec. Pleuro / Pulmonar: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si hay epoc
        if ($this->DatosRx->getEpoc() == 0){
            $texto = "EPOC: No";
        } else {
            $texto = "EPOC: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 0);

        // si hay calcificaciones
        if ($this->DatosRx->getCalcificaciones() == 0){
            $texto = "Calcificaciones: No";
        } else {
            $texto = "Calcificaciones: Si";
        }
        $this->Documento->Cell(60, $this->Alto, $texto, 0, 1);

        // si no tiene
        if ($this->DatosRx->getNotiene() == 1){
            $this->Documento->Cell(50, $this->Alto, "No Tiene", 0, 1);
        }

        // inserta un separador
        $this->Documento->Ln(10);

    }

}
?>