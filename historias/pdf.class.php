<?php

/**
 *
 * Class Historia | historia/historia_visita.class.php
 *
 * @package     Diagnostico
 * @subpackage  Historias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Clase que extiende la clase pdf para incluir los encabezados y
 * pié de página
 *
*/

// define la ruta a las fuentes pdf, usamos el server root
// porque lo llamamos desde distintos lugares
define('FPDF_FONTPATH', $_SERVER['DOCUMENT_ROOT'] . '/clases/fpdf/font');

// la clase pdf y las clases del sistema
require_once ($_SERVER['DOCUMENT_ROOT'] . "/clases/fpdf/writehtml/WriteHTML.php");
require_once ("../laboratorios/laboratorios.class.php");

// declaración de la clase
class PDF extends PDF_HTML {

    // declaración de variables
    protected $Logo;                   // logo del laboratorio
    protected $Institucion;            // nombre de la institución
    protected $Extension;              // extensión del logo

    /**
     * Método llamado al instanciar la clase que obtiene los
     * datos del laboratorio activo, lo llamamos manualmente
     * porque si creamos un constructor no carga correctamente
     * el path de las fuentes (probablemente porque estamos
     * heredando varias clases)
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */

     public function Inicializar(){

        // instanciamos las clases e inicializamos las variables
        $this->Laboratorio = new Laboratorios();
        $this->Logo = "";
        $this->Institucion = "";
        $this->Extension = "";

        // obtenemos los datos de la institución
        $this->getDatos();

    }

    /**
     * Método que a partir de las variables de sesión obtiene los
     * datos de la institución
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getDatos(){

        // obtenemos la clave del laboratorio
        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["IdLaboratorio"])){

            // obtenemos los valores de la sesión
            $this->Laboratorio->getDatosLaboratorio($_SESSION["IdLaboratorio"]);

            // obtenemos el logo y el nombre
            $this->Logo = $this->Laboratorio->getLogo();
            $this->Institucion = $this->Laboratorio->getNombre();

            // obtenemos la extensión de la imagen
            $this->getExtension();

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Método que obtiene la extensión del archivo de imagen para pooder
     * incorporarlo al encabezado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function getExtension(){

        // si es jpeg
        if (stripos($this->Logo, "data:image/jpeg;") !== false){
            $this->Extension = "jpeg";
        // si es gif
        } elseif (stripos($this->Logo, "data:image/gif;") !== false){
            $this->Extension = "gif";
        // si es png
        } if (stripos($this->Logo, "data:image/png;") !== false){
            $this->Extension = "png";
        // si es jpg
        } elseif (stripos($this->Logo, "data:image/jpg;") !== false){
            $this->Extension = "jpg";
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Sobrecargamos el método header del padre para presentar
     * el encabezado de las páginas
     */
    public function Header(){

        // incluimos el logo
        $this->Image($this->Logo, 5, 5, 40, 30, $this->Extension);

        // fijamos la posición de impresión
        $this->SetXY(60,10);

        // establecemos la fuente
        $this->SetFont("DejaVu", "", 10);

        // imprime el encabezado
        $this->MultiCell(80, 8, $this->Institucion, 0, 'C');

        // mostramos el logo del ministerio
        $this->Image("../imagenes/logo_msal.jpg", 150, 5, 60, 30);

        // fija las dos líneas separadoras
        $this->Image('../imagenes/separador.png', 5, 35, 210, 3);

        // fijamos la posición del cabezal
        $this->setY(40);

        // establecemos la fuente normal
        $this->SetFont("DejaVu", "", 12);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Sobrecargamos el mètodo footer para presentar el
     * piè de página
     */
    public function Footer(){

        // vamos a 1 centímetro del fin de página
        $this->SetY(-10);

        // fijamos la fuente
        $this->SetFont("DejaVu", "", 10);

        // componemos el texto
        $texto = "Departamento de Clínica ";
        $texto .= "- Página Nº: " . $this->PageNo() . " de " . '{nb}';

        // imprimimos el pié y el número de página
        $this->Cell(0, 7, $texto, "T", 0, 'C');

        // establecemos la fuente
        $this->SetFont("DejaVu", "", 12);

    }

}

?>