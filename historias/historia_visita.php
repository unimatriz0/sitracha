<?php

/**
 *
 * historias/historia_visita.php
 *
 * @package     Diagnostico
 * @subpackage  Historias
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el número de historia clínica y presenta
 * el layer emergente con la historia clínica agrupada por visita
 *
*/

// incluimos e instanciamos las clases
require_once("historia_visita.class.php");
$historia = new Historia($_GET["protocolo"]);

// enviamos los encabezados
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0, false");
header("Pragma: no-cache");
?>

<!-- cargamos el documento -->
<object data="temp/historia.pdf"
        type="application/pdf"
        width="850" height="500">
</object>