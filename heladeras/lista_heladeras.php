<?php

/**
 *
 * heladeras/lista_heladeras.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de heladeras
 * para el laboratorio activo
 * 
*/

// incluimos e instanciamos las clases
require_once ("heladeras.class.php");
$heladera = new Heladeras();

// obtenemos la nómina
$nomina = $heladera->nominaHeladeras();

// definimos el array
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){
    
    // obtenemos el registro
    extract($registro);
    
    // lo agregamos al array
    $jsondata[] = array("Id" => $id_heladera,
                        "Heladera" => $marca);
    
}

// retornamos el vector
echo json_encode($jsondata);
?>