<?php

/**
 *
 * Class Heladeras | heladeras/heladeras.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de
 * heladeras / freezers
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Heladeras{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del usuario
    protected $IdHeladera;            // clave del registro
    protected $Marca;                 // marca de la heladera
    protected $Ubicacion;             // descripción de la ubicación
    protected $Patrimonio;            // código de patrimonio
    protected $Temperatura;           // temperatura ideal de la heladera
    protected $Temperatura2;          // segunda temperatura de la heladera
    protected $Tolerancia;            // margen de tolerancia en grados
    protected $Usuario;               // nombre del usuario
    protected $FechaAlta;             // fecha de alta del registro
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdDepartamento;        // clave del departamento del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdHeladera = 0;
        $this->Marca = "";
        $this->Ubicacion = "";
        $this->Patrimonio = "";
        $this->Temperatura = 0;
        $this->Temperatura2 = 0;
        $this->Tolerancia = 0;
        $this->Usuario = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];
            $this->IdDepartamento = $_SESSION["IdDepartamento"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // Métodos de asignación de valores
    public function setIdHeladera($idheladera){
        $this->IdHeladera = $idheladera;
    }
    public function setMarca($marca){
        $this->Marca = $marca;
    }
    public function setUbicacion($ubicacion){
        $this->Ubicacion = $ubicacion;
    }
    public function setPatrimonio($patrimonio){
        $this->Patrimonio = $patrimonio;
    }
    public function setTemperatura($temperatura){
        $this->Temperatura = $temperatura;
    }
    public function setTemperatura2($temperatura){
        $this->Temperatura2 = $temperatura;
    }
    public function setTolerancia($tolerancia){
        $this->Tolerancia = $tolerancia;
    }

    // Métodos de retorno de valores
    public function getIdHeladera(){
        return $this->IdHeladera;
    }
    public function getMarca(){
        return $this->Marca;
    }
    public function getUbicacion(){
        return $this->Ubicacion;
    }
    public function getPatrimonio(){
        return $this->Patrimonio;
    }
    public function getTemperatura(){
        return $this->Temperatura;
    }
    public function getTemperatura2(){
        return $this->Temperatura2;
    }
    public function getTolerancia(){
        return $this->Tolerancia;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que retorna la nómina de heladeras para el Laboratorio
     * del usuario
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array $nomina
     */
    public function nominaHeladeras(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.heladeras.id AS id_heladera,
                            diagnostico.heladeras.marca AS marca,
                            diagnostico.heladeras.ubicacion AS ubicacion,
                            diagnostico.heladeras.patrimonio AS patrimonio,
                            diagnostico.heladeras.temperatura AS temperatura,
                            diagnostico.heladeras.temperatura2 AS temperatura2,
                            diagnostico.heladeras.tolerancia AS tolerancia,
                            cce.responsables.usuario AS usuario,
                            DATE_FORMAT(diagnostico.heladeras.fecha_alta, '%d/%m/%Y') AS fecha_alta
                     FROM diagnostico.heladeras INNER JOIN cce.responsables ON diagnostico.heladeras.usuario = cce.responsables.id
                     WHERE diagnostico.heladeras.laboratorio = '$this->IdLaboratorio' AND 
                           diagnostico.heladeras.departamento = '$this->IdDepartamento'
                     ORDER BY diagnostico.heladeras.marca,
                              diagnostico.heladeras.ubicacion;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe como parámetro la clave de un registro y asigna en
     * las variables de clase los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idheladera - clave del registro a obtener
     */
    public function getDatosHeladera($idheladera){

        // inicializamos las variables
        $id_heladera = 0;
        $marca = "";
        $ubicacion = "";
        $patrimonio = "";
        $temperatura = 0;
        $temperatura2 = 0;
        $tolerancia = 0;
        $usuario = "";
        $fecha_alta = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.heladeras.id AS id_heladera,
                            diagnostico.heladeras.marca AS marca,
                            diagnostico.heladeras.ubicacion AS ubicacion,
                            diagnostico.heladeras.patrimonio AS patrimonio,
                            diagnostico.heladeras.temperatura AS temperatura,
                            diagnostico.heladeras.temperatura2 AS temperatura2,
                            diagnostico.heladeras.tolerancia AS tolerancia,
                            cce.responsables.usuario AS usuario,
                            DATE_FORMAT(diagnostico.heladeras.fecha_alta, '%d/%m/%Y') AS fecha_alta
                     FROM diagnostico.heladeras INNER JOIN cce.responsables ON diagnostico.heladeras.usuario = cce.responsables.id
                     WHERE diagnostico.heladeras.id = '$idheladera';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y lo asignamos
        extract($fila);
        $this->IdHeladera = $id_heladera;
        $this->Marca = $marca;
        $this->Ubicacion = $ubicacion;
        $this->Patrimonio = $patrimonio;
        $this->Temperatura = $temperatura;
        $this->Temperatura2 = $temperatura2;
        $this->Tolerancia = $tolerancia;
        $this->Usuario = $usuario;
        $this->FechaAlta = $fecha_alta;

    }

    /**
     * Método que ejecuta la consulta de edición o edición según corresponda
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idheladera - clave del registro insertado / editado
     */
    public function grabaHeladera(){

        // si está insertando
        if ($this->IdHeladera == 0){
            $this->nuevaHeladera();
        } else {
            $this->editaHeladera();
        }

        // retornamos la id
        return $this->IdHeladera;

    }

    /**
     * Método protegido que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaHeladera(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.heladeras
                            (marca,
                             laboratorio,
                             departamento,
                             ubicacion,
                             patrimonio,
                             temperatura,
                             temperatura2,
                             tolerancia,
                             usuario)
                            VALUES
                            (:marca,
                             :laboratorio,
                             :departamento,
                             :ubicacion,
                             :patrimonio,
                             :temperatura,
                             :temperatura2,
                             :tolerancia,
                             :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":marca", $this->Marca);
        $psInsertar->bindParam(":laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":departamento", $this->IdDepartamento);
        $psInsertar->bindParam(":ubicacion", $this->Ubicacion);
        $psInsertar->bindParam(":patrimonio", $this->Patrimonio);
        $psInsertar->bindParam(":temperatura", $this->Temperatura);
        $psInsertar->bindParam(":temperatura2", $this->Temperatura2);
        $psInsertar->bindParam(":tolerancia", $this->Tolerancia);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->IdHeladera = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaHeladera(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.heladeras SET
                            marca = :marca,
                            laboratorio = :laboratorio,
                            ubicacion = :ubicacion,
                            patrimonio = :patrimonio,
                            temperatura = :temperatura,
                            temperatura2 = :temperatura2,
                            tolerancia = :tolerancia,
                            usuario = :usuario
                     WHERE diagnostico.heladeras.id = :idheladera;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":marca", $this->Marca);
        $psInsertar->bindParam(":laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":ubicacion", $this->Ubicacion);
        $psInsertar->bindParam(":patrimonio", $this->Patrimonio);
        $psInsertar->bindParam(":temperatura", $this->Temperatura);
        $psInsertar->bindParam(":temperatura2", $this->Temperatura2);
        $psInsertar->bindParam(":tolerancia", $this->Tolerancia);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":idheladera", $this->IdHeladera);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método llamado antes de eliminar que verifica que la heladera
     * no tenga registros de temperatura, retorna la cantidad de
     * registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idheladera - clave del registro a eliminar
     * @return int $registros
     */
    public function verificaHeladera($idheladera){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.v_heladeras.id_registro) AS registros
                     FROM diagnostico.v_heladeras
                     WHERE diagnostico.v_heladeras.id_heladera = '$idheladera';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y retornamos
        extract($fila);
        return $registros;

    }

    /**
     * Método que recibe como parámetro la id de un registro y ejecuta la
     * consulta de eliminación
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idheladera - clave de la heladera a eliminar
     */
    public function borraHeladera($idheladera){

        // componemos la consulta y la ejecutamos
        $consulta = "DELETE FROM diagnostico.heladeras WHERE id = '$idheladera';";
        $this->Link->exec($consulta);

    }

}