<?php

/**
 *
 * heladeras/getheladera.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (03/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get como parámetro la clave de una 
 * heladera y retorna un array json con los datos del 
 * registro
 * 
*/

// incluimos e instanciamos la clase
require_once("heladeras.class.php");
$heladera = new Heladeras();

// obtenemos el registro
$heladera->getDatosHeladera($_GET["id"]);

// retornamos el registro
echo json_encode(array("Id" =>           $heladera->getIdHeladera(),
                       "Marca" =>        $heladera->getMarca(),
                       "Ubicacion" =>    $heladera->getUbicacion(),
                       "Patrimonio" =>   $heladera->getPatrimonio(),
                       "Temperatura" =>  $heladera->getTemperatura(),
                       "Temperatura2" => $heladera->getTemperatura2(),
                       "Tolerancia" =>   $heladera->getTolerancia(),
                       "Usuario" =>      $heladera->getUsuario(),
                       "FechaAlta" =>    $heladera->getFechaAlta()));

?>