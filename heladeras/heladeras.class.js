/*
 * Nombre: heladeras.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 02/05/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones que controlan el abm de heladeras
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de heladeras
 */
class Heladeras {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initHeladeras();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initHeladeras(){

        // inicializamos las variables de clase
        this.IdHeladera = 0;
        this.Marca = "";
        this.Ubicacion = "";
        this.Patrimonio = "";
        this.Temperatura = 0;
        this.Temperatura2 = 0;
        this.Tolerancia = 0;
        this.Usuario = "";
        this.FechaAlta = "";

        // definimos el layer para el formulario
        this.layerHeladeras = "";

    }

    //Métodos de asignación de valores
    setIdHeladera(idheladera){
        this.IdHeladera = idheladera;
    }
    setMarca(marca){
        this.Marca = marca;
    }
    setUbicacion(ubicacion){
        this.Ubicacion = ubicacion;
    }
    setPatrimonio(patrimonio){
        this.Patrimonio = patrimonio;
    }
    setTemperatura(temperatura){
        this.Temperatura = temperatura;
    }
    setTemperatura2(temperatura){
        this.Temperatura2 = temperatura;
    }
    setTolerancia(tolerancia){
        this.Tolerancia = tolerancia;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idheladera - clave del registro
     * Método que carga en el layer emergente el formulario de heladeras
     * si no recibe la id asume que es un alta
     */
    formHeladeras(idheladera){

        this.layerHeladeras = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        title: 'Heladeras y Freezer',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        draggable: 'title',
                        ajax: {
                            url: 'heladeras/heladeras.html',
                        reload: 'strict'
                    }
            });
        this.layerHeladeras.open();

        // si recibió la id
        if (typeof(idheladera) != "undefined"){

            // carga los datos
            this.getDatosHeladera(idheladera);

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idheladera - clave del registro
     * Método que recibe como parámetro la clave de una heladera
     * y obtiene los datos del registro
     */
    getDatosHeladera(idheladera){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la rutina php para obtener los datos del laboratorio
        $.get('heladeras/getheladera.php', 'id='+idheladera,
            function(data){

                // asignamos los valores en las variables de clase
                heladeras.setIdHeladera(data.Id);
                heladeras.setMarca(data.Marca);
                heladeras.setUbicacion(data.Ubicacion);
                heladeras.setPatrimonio(data.Patrimonio);
                heladeras.setTemperatura(data.Temperatura);
                heladeras.setTemperatura2(data.Temperatura2);
                heladeras.setTolerancia(data.Tolerancia);
                heladeras.setUsuario(data.Usuario);
                heladeras.setFechaAlta(data.FechaAlta);

                // mostramos el registro
                heladeras.muestraHeladera();

            }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase presenta
     * el registro de la heladera en el formulario
     */
    muestraHeladera(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // fijamos los valores del formulario
        document.getElementById("id_heladera").value = this.IdHeladera;
        document.getElementById("marca_heladera").value = this.Marca;
        document.getElementById("ubicacion_heladera").value = this.Ubicacion;
        document.getElementById("patrimonio_heladera").value = this.Patrimonio;
        document.getElementById("temperatura_heladera").value = this.Temperatura;
        document.getElementById("temperatura2_heladera").value = this.Temperatura2;
        document.getElementById("tolerancia_heladera").value = this.Tolerancia;
        document.getElementById("alta_heladera").value = this.FechaAlta;
        document.getElementById("usuario_heladera").value = this.Usuario;

        // fijamos el foco
        document.getElementById("marca_heladera").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar en el formulario
     * que verifica los datos del mismo
     */
    validaHeladera(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("id_heladera").value != ""){

            // asignamos en la variable de clase
            this.IdHeladera = document.getElementById("id_heladera").value;

        }

        // si no ingresó la marca
        if(document.getElementById("marca_heladera").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la marca de la heladera / freezer";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("marca_heladera").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Marca = document.getElementById("marca_heladera").value;

        }

        // si no ingresó la ubicación
        if(document.getElementById("ubicacion_heladera").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la ubicación de la heladera / freezer";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ubicacion_heladera").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Ubicacion = document.getElementById("ubicacion_heladera").value;

        }

        // si no ingresó el código de patrimonio
        if (document.getElementById("patrimonio_heladera").value == ""){

            // presenta el mensaje solamente
            mensaje = "No hay información de patrimonio de la heladera / freezer";
            new jBox('Notice', {content: mensaje, color: 'red'});

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Patrimonio = document.getElementById("patrimonio_heladera").value;

        }

        // si no ingresó la temperatura
        if (document.getElementById("temperatura_heladera").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la temperatura de funcionamiento<br>";
            mensaje += "de la heladera / freezer";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("temperatura_heladera").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Temperatura = document.getElementById("temperatura_heladera").value;

        }

        // la temperatura 2 la permite en blanco
        if (document.getElementById("temperatura2_heladera").value != ""){

            // asignamos en la variable de clase
            this.Temperatura2 = document.getElementById("temperatura2_heladera").value;

        }

        // si no ingresó la tolerancia
        if (document.getElementById("tolerancia_heladera").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la tolerancia de funcionamiento<br>";
            mensaje += "en grados de la heladera / freezer";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("tolerancia_heladera").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Tolerancia = document.getElementById("tolerancia_heladera").value;

        }

        // grabamos el registro
        this.grabaHeladera();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar del formulario
     * que simplemente cierra el layer
     */
    cancelaHeladera(){

        // destruimos el layer
        this.layerHeladeras.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar los datos del registro
     * y que ejecuta la consulta de actualización
     */
    grabaHeladera(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos la variable
        var datosHeladera = new FormData();

        // si está editando
        if (this.IdHeladera != 0){
            datosHeladera.append("Id", this.IdHeladera);
        }

        // asignamos el resto de valores
        datosHeladera.append("Marca", this.Marca);
        datosHeladera.append("Ubicacion", this.Ubicacion);
        datosHeladera.append("Patrimonio", this.Patrimonio);
        datosHeladera.append("Temperatura", this.Temperatura);
        datosHeladera.append("Temperatura2", this.Temperatura2);
        datosHeladera.append("Tolerancia", this.Tolerancia);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "heladeras/graba_heladera.php",
            data: datosHeladera,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // recargamos la grilla
                    heladeras.grillaHeladeras();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // cerramos el layer
                    heladeras.cancelaHeladera();

                    // inicializamos las variables por las dudas
                    heladeras.initHeladeras();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la rutina de impresión de la ficha de
     * la heladera con su código de barras
     */
    imprimirHeladera(){

        // reiniciamos la sesion
        sesion.reiniciar();

        // obtenemos la id de la heladera
        var idheladera = document.getElementById("id_heladera").value;

        // si no grabó el registro
        if (idheladera == ""){

            // presenta el mensaje y retorna
            new jBox('Notice', {content: "Debe grabar el registro primero", color: 'red'});
            return false;

        }

        // ahora llamamos el layer emergente que presenta la etiqueta
        var layerEtiqueta = new jBox('Modal', {
                                animation: 'flip',
                                closeOnEsc: true,
                                closeOnClick: false,
                                closeOnMouseleave: false,
                                closeButton: true,
                                repositionOnContent: true,
                                overlay: false,
                                title: 'Etiqueta Heladera',
                                draggable: 'title',
                                theme: 'TooltipBorder',
                                zIndex: 22000,
                                onCloseComplete: function(){
                                    this.destroy();
                                },
                                ajax: {
                                    url: 'heladeras/etiqueta.php?id='+idheladera,
                                    reload: 'strict'
                                }
                            });
        layerEtiqueta.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el div la nómina de heladeras registradas
     */
    grillaHeladeras(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_controles").load("heladeras/grilla_heladeras.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idheladera - clave de la heladera a eliminar
     * Método que recibe como parámetro la id de una heladera y
     * verifica si puede eliminarla antes de ejecutar la consulta
     */
    borraHeladera(idheladera){

        // verificamos si puede eliminar
        $.get('heladeras/verifica_heladera.php', 'id='+idheladera,
            function(data){

                // si no hay registros de temperaturas
                if (data.Registros == 0){

                    // eliminamos
                    heladeras.eliminaHeladera(idheladera);

                // si encontró registros
                } else {

                    // presenta el mensaje
                    var mensaje = "Esa heladera / freezer cuenta con registros<br>";
                    mensaje += "de temperaturas y no puede ser eliminada";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idheladera - clave de la heladera a eliminar
     * Método llamado luego de verificar la heladera si puede ser
     * eliminada, pide confirmación y luego elimina el registro
     */
    eliminaHeladera(idheladera){

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Heladera / Freezer',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar la Heladera?',
            theme: 'TooltipBorder',
            confirm: function() {

                 // llama la rutina php
                 $.get('heladeras/borra_heladera.php?id='+idheladera,
                     function(data){

                         // si retornó error
                         if (data.Error != 0){

                             // presenta el mensaje
                             new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                             // recargamos la grilla
                             heladeras.grillaHeladeras();

                         // si no pudo eliminar
                         } else {

                             // presenta el mensaje
                             new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                         }

                     }, "json");

             },
             cancel: function(){
                 Confirmacion.destroy();
             },
             confirmButton: 'Aceptar',
             cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idheladera - clave de la heladera preseleccionada
     * Método que recibe como parámetro la id de un elemento del
     * formulario y carga en ese elemento la nómina de heladeras del
     * laboratorio activo
     */
    nominaHeladeras(idelemento, idheladera){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "heladeras/lista_heladeras.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la heladera
                    if (typeof(idheladera) != "undefined"){

                        // si coincide
                        if (data[i].Id == idheladera){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Heladera + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Heladera + "</option>");

                        }

                    // si no recibió heladera
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Heladera + "</option>");
                    }

                }

        }});

    }

}