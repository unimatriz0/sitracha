<?php

/**
 *
 * heladeras/graba_heladera.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro de 
 * una heladera y ejecuta la consulta, retorna el resultado
 * de la operación
 * 
*/

// incluimos e instanciamos la clase
require_once("heladeras.class.php");
$heladera = new Heladeras();

// si recibió la id
if (!empty($_POST["Id"])){
    $heladera->setIdHeladera($_POST["Id"]);
}

// seteamos el resto de las propiedades
$heladera->setMarca($_POST["Marca"]);
$heladera->setUbicacion($_POST["Ubicacion"]);
$heladera->setPatrimonio($_POST["Patrimonio"]);
$heladera->setTemperatura($_POST["Temperatura"]);
$heladera->setTemperatura2($_POST["Temperatura2"]);
$heladera->setTolerancia($_POST["Tolerancia"]);

// grabamos el registro
$resultado = $heladera->grabaHeladera();

// retornamos
echo json_encode(array("Id" => $resultado));

?>