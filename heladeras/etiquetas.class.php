<?php

/**
 *
 * Class Etiquetas | heladeras/etiquetas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (09/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// define la ruta a las fuentes pdf (lo llamamos desde
// el script de remitos y el path queda en stock)
define('FPDF_FONTPATH', '../clases/fpdf/font');

// la clase pdf
require_once ("../clases/fpdf/code39/code39.php");

// la clase qrcode
require_once('../clases/fpdf/qrcode/qrcode.class.php');

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que genera la etiqueta en formato PDF de la heladera
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Etiquetas{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                         // puntero a la base de datos
    protected $IdHeladera;                   // clave del registro
    protected $Laboratorio;                  // nombre del laboratorio
    protected $Marca;                        // marca de la heladera
    protected $Ubicacion;                    // ubicación física
    protected $Patrimonio;                   // número de patrimonio
    protected $Temperatura;                  // temperatura de funcionamiento
    protected $Temperatura2;                 // segunda temperatura
    protected $Tolerancia;                   // tolerancia en grados de funcionamiento
    protected $Usuario;                      // nombre del usuario
    protected $FechaAlta;                    // fecha de alta del registro
    protected $Logo;                         // logo del laboratorio

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdHeladera = 0;
        $this->Laboratorio = "";
        $this->Marca = "";
        $this->Ubicacion = "";
        $this->Patrimonio = "";
        $this->Temperatura = "";
        $this->Temperatura2 = "";
        $this->Tolerancia = "";
        $this->Usuario = "";
        $this->FechaAlta = "";
        $this->Logo = "";

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método llamado luego de instanciar la clase que recibe como
     * parámetro la clave de la heladera y genera la etiqueta
     * @param int $idheladera
     */
    public function generaEtiqueta($idheladera){

        // asignamos en la variable de clase
        $this->IdHeladera = $idheladera;

        // obtenemos los datos de la heladera
        $this->getDatosHeladera();

        // instanciamos la clase pdf
        $this->Documento = new PDF_Code39();

        // establecemos las propiedades del documento
        $this->Documento->SetAuthor("Claudio Invernizzi");
        $this->Documento->SetCreator("INP - Mario Fatala Chaben");
        $this->Documento->SetSubject("Ingreso al Depósito", true);
        $this->Documento->SetTitle("Ingreso al Depósito", true);
        $this->Documento->SetAutoPageBreak(true, 10);

        // agrega una fuente unicode
        $this->Documento->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->Documento->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // fijamos el margen izquierdo y derecho
        $this->Documento->SetLeftMargin(15);
        $this->Documento->setRightMargin(15);

        // agregamos la página
        $this->Documento->AddPage("P", "A4");

        // imprimimos el encabezado del documento
        $this->imprimirEncabezado();

        // imprimimos el cuerpo del documento
        $this->cuerpoEtiqueta();

        // lo cerramos y creamos en el directorio temporal
        $this->Documento->Output("../temp/etiqueta_heladera.pdf", 'F');

    }

    /**
     * Método que asigna en las variables de clase los valores
     * del registro
     */
    protected function getDatosHeladera(){

        // inicializamos las variables
        $laboratorio = "";
        $idlaboratorio = 0;
        $marca = "";
        $ubicacion = "";
        $patrimonio = "";
        $temperatura = 0;
        $temperatura2 = 0;
        $tolerancia = 0;
        $usuario = "";
        $fecha_alta = "";

        // componemos y ejecutamos la consulta
        $consulta = "SELECT diagnostico.heladeras.id AS id_heladera,
                            cce.laboratorios.nombre AS laboratorio,
                            diagnostico.heladeras.laboratorio AS idlaboratorio,
                            diagnostico.heladeras.marca AS marca,
                            diagnostico.heladeras.ubicacion AS ubicacion,
                            diagnostico.heladeras.patrimonio AS patrimonio,
                            diagnostico.heladeras.temperatura AS temperatura,
                            diagnostico.heladeras.temperatura2 AS temperatura2,
                            diagnostico.heladeras.tolerancia AS tolerancia,
                            cce.responsables.usuario AS usuario,
                            DATE_FORMAT(diagnostico.heladeras.fecha_alta, '%d/%m/%Y') AS fecha_alta
                     FROM diagnostico.heladeras INNER JOIN cce.responsables ON diagnostico.heladeras.usuario = cce.responsables.id
                                                INNER JOIN cce.laboratorios ON diagnostico.heladeras.laboratorio = cce.laboratorios.id
                     WHERE diagnostico.heladeras.id = '$this->IdHeladera';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro
        extract($fila);

        // asignamos en las variables de clase
        $this->Laboratorio = $laboratorio;
        $this->Marca = $marca;
        $this->Ubicacion = $ubicacion;
        $this->Patrimonio = $patrimonio;
        $this->Temperatura = $temperatura;
        $this->Temperatura2 = $temperatura2;
        $this->Tolerancia = $tolerancia;
        $this->Usuario = $usuario;
        $this->FechaAlta = $fecha_alta;

        // obtenemos el logo
        $this->obtenerLogo($idlaboratorio);

    }

    /**
     * Método que asigna en la variable de clase el logo
     * del laboratorio
     * @param int $idlaboratorio
     */
    protected function obtenerLogo($idlaboratorio){

        // inicializamos las variables
        $logo = "";

        // obtenemos el logo del laboratorio
        $consulta = "SELECT cce.laboratorios.logo AS logo
                     FROM cce.laboratorios
                     WHERE cce.laboratorios.id = '$idlaboratorio';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // extraemos el registro
        extract($fila);

        // asignamos en la variable de clase
        $this->Logo = $logo;

    }

    /**
     * Método protegido que imprime los datos del encabezado
     */
    protected function imprimirEncabezado(){

        // definimos las coordenadas
        $this->Documento->SetXY(20, 15);

        // definimos la fuente
        $this->Documento->setFont("DejaVu", "B", 12);

        // presentamos el nombre de la institución
        $this->Documento->MultiCell(100, 8, $this->Laboratorio, 0, "C");

        // si existe la imagen
        if (!empty($this->Logo)){

            // determinamos el tipo de imagen
            $tipo = $this->tipoImagen($this->Logo);

            // agregamos la imagen indicándole el tamaño y la extensión
            $this->Documento->Image($this->Logo, 150, 5, 40, 40, $tipo);

        }

        // presenta el código de barras
        $this->Documento->Code39(140, 50, "$this->IdHeladera", 1, 10);

        // obtenemos el código qr
        $qrcode = new QRcode($this->IdHeladera, 'H');

        // lo adjuntamos al documento
        $qrcode->displayFPDF($this->Documento, 140, 70, 40);

        // fijamos las coordenadas de impresión
        $this->Documento->setXY(10, 50);

        // definimos la fuente
        $this->Documento->setFont("DejaVu", "B", 12);

        // ahora presenta el título
        $this->Documento->Cell(50,10, "Heladera / Freezer", 0, 1, "C");

    }

    /**
     * Método que imprime el cuerpo de la etiqueta
     */
    protected function cuerpoEtiqueta(){

        // fijamos la fuente
        $this->Documento->setFont("DejaVu", "", 12);

        // la marca
        $texto = "Marca: " . $this->Marca;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // la ubicación
        $texto = "Ubicación: " . $this->Ubicacion;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // el registro de patrimonio
        $texto = "Registro Patrimonial: " . $this->Patrimonio;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // la temperatura
        $texto = "Temperatura de Funcionamiento: " . $this->Temperatura;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // la segunda temperatura
        $texto = "2º Temperatura de Funcionamiento: " . $this->Temperatura2;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // la tolerancia
        $texto = "Tolerancia en Grados: " . $this->Tolerancia;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // el usuario
        $texto = "Usuario: " . $this->Usuario;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // la fecha de alta
        $texto = "Fecha de Alta: " . $this->FechaAlta;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

    }

    /**
     * Método que a partir de la cadena que recibe, retorna el tipo de
     * archivo de imagen (lo guardamos logos e imágenes como base 64
     * en la base)
     * @param string $archivo
     * @return string
     */
    protected function tipoImagen($archivo){

        // si es jpeg
        if (stripos($archivo, "data:image/jpeg;") !== false){
            $tipo = "jpeg";
        // si es gif
        } elseif (stripos($archivo, "data:image/gif;") !== false){
            $tipo = "gif";
        // si es png
        } if (stripos($archivo, "data:image/png;") !== false){
            $tipo = "png";
        // si es jpg
        } elseif (stripos($archivo, "data:image/jpg;") !== false){
            $tipo = "jpg";
        }

        // retornamos el tipo
        return $tipo;

    }

}