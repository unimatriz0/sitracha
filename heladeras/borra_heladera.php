<?php

/**
 *
 * heladeras/borra_heladera.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una heladera y ejecuta la
 * consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once("heladeras.class.php");
$heladera = new Heladeras();
$heladera->borraHeladera($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>