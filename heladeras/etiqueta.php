<?php

/**
 *
 * heladeras/etiqueta.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (09/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una heladera y 
 * genera la etiqueta en formato PDF
 * 
*/

// incluimos e instanciamos la clase
require_once("etiquetas.class.php");
$etiqueta = new Etiquetas();

// generamos el documento
$etiqueta->generaEtiqueta($_GET["id"]);

// enviamos los encabezados
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0, false");
header("Pragma: no-cache");
?>

<!-- cargamos el documento -->
<object data="temp/etiqueta_heladera.pdf"
        type="application/pdf"
        width="850" height="500">
</object>
?>