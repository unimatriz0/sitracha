<?php

/**
 *
 * heladeras/verifica_heladera.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una heladera y verifica
 * si puede ser eliminada (solo puede ser eliminada cuando no 
 * tiene ninguna lectura realizada)
 * 
*/

/*

    Archivo: verifica_heladeras.php
    Autor: Claudio Invernizzi
    Fecha: 04/05/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Licencia: GPL
    Comentarios: Método que recibe por get la clave de una heladera y verifica
                 si puede ser eliminada

*/

// incluimos e instanciamos la clase
require_once("heladeras.class.php");
$heladera = new Heladeras();

// verificamos si tiene registros
$registros = $heladera->verificaHeladera($_GET["id"]);

// retornamos el número de registros
echo json_encode(array("Registros" => $registros));

?>