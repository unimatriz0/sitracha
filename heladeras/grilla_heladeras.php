<?php

/**
 *
 * heladeras/grilla_heladeras.class.php
 *
 * @package     Diagnostico
 * @subpackage  Heladeras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma la grilla con la nómina de las heladeras
 * registradas del laboratorio activo
 * 
*/

// incluimos e instanciamos la clase
require_once("heladeras.class.php");
$heladera = new Heladeras();

// presenta el título
echo "<h2>Heladeras y Freezers</h2>";

// definimos la tabla
echo "<table width='90%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Marca</th>";
echo "<th align='left'>Ubicación</th>";
echo "<th>Temperatura</th>";
echo "<th>Temperatura</th>";
echo "<th>Tolerancia</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th>";
echo "<input type='button'
             name='btnNuevaHeladera'
             id='btnNuevaHeladera'
             class='botonagregar'
             title='Agrega una nueva heladera'
             onClick='heladeras.formHeladeras()'>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la nómina
$nomina = $heladera->nominaHeladeras();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>$marca</td>";
    echo "<td>$ubicacion</td>";
    echo "<td align='center'>$temperatura</td>";
    echo "<td align='center'>$temperatura2</td>";
    echo "<td align='center'>$tolerancia</td>";
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // presenta el botón editar
    echo "<td align='center'>";
    echo "<input type='button'
               name='btnEditaHeladera'
               id='btnEtidaHeladera'
               class='botoneditar'
               title='Edita el registro'
               onClick='heladeras.formHeladeras($id_heladera)'>";
    echo "</td>";

    // presenta el botón eliminar
    echo "<td align='center'>";
    echo "<input type='button'
               name='btnBorraHeladera'
               id='btnBorraHeladera'
               class='botonborrar'
               title='Elimina el registro'
               onClick='heladeras.borraHeladera($id_heladera)'>";
    echo "</td>";

    // cierra la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody>";
echo "</table>";

?>