<?php

/**
 *
 * borrar | drogas/borrar.php
 *
 * @package     Diagnostico
 * @subpackage  Drogas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta
 * la consulta de eliminación
*/

// incluimos e instanciamos las clases
require_once("drogas.class.php");
$droga = new Drogas();

// ejecutamos la consulta
$droga->borraDroga($_GET["id"]);

?>