<?php

/**
 *
 * grabar | drogas/grabar.php
 *
 * @package     Diagnostico
 * @subpackage  Drogas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta de actualización en la base
*/

// incluimos e instanciamos las clases
require_once("drogas.class.php");
$droga = new Drogas();

// fijamos las variables
$droga->setId($_POST["Id"]);
$droga->setDroga($_POST["Droga"]);
$droga->setDosis($_POST["Dosis"]);
$droga->setComercial($_POST["Comercial"]);

// ejecutamos la consulta
$droga->grabaDroga();

?>