/*

    Nombre: drogas.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 10/06/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones del diccionario de drogas

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de drogas utilizadas
 */
class Drogas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initDrogas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initDrogas(){

        // inicializamos las variables
        this.Id = 0;                 // clave del registro
        this.Droga = "";             // nombre de la droga
        this.Dosis = 0;              // dosis en miligramos
        this.Comercial = "";         // nombre comercial de la droga

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de datos en el contenedor
     */
    muestraDrogas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("drogas/listadrogas.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que verifica los datos del formulario antes
     * de enviarlo al servidor
     */
    verificaDroga(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si está dando un alta
        if (typeof(id) == "undefined"){
            id = "nuevo";
        } else {
            this.Id = id;
        }

        // verifica que halla ingresado el nombre
        if (document.getElementById("droga_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la droga";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("droga_" + id).focus();
            return false;

        }

        // si no ingresó la dosis
        if (document.getElementById("dosis_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la dosis de la droga";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("dosis_" + id).focus();
            return false;

        // verifica que sea un número
        } else if (isNaN(document.getElementById("dosis_" + id).value)){

            // presenta el mensaje y retorna
            mensaje = "La dosis debe ser un número";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("dosis_" + id).focus();
            return false;

        }

        // verifica que halla ingresado el nombre comercial
        if (document.getElementById("comercial_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el nombre comercial";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("comercial_" + id).focus();
            return false;

        }

        // asigna en la variable de clase y graba
        this.Droga = document.getElementById("droga_" + id).value;
        this.Dosis = document.getElementById("dosis_" + id).value;
        this.Comercial = document.getElementById("comercial_" + id).value;
        this.grabaDroga();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que graba el registro en la base y luego
     * recarga la grilla
     */
    grabaDroga(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosDroga = new FormData();

        // agregamos al formulario
        datosDroga.append("Id", this.Id);
        datosDroga.append("Droga", this.Droga);
        datosDroga.append("Dosis", this.Dosis);
        datosDroga.append("Comercial", this.Comercial);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "drogas/grabar.php",
            type: "POST",
            data: datosDroga,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    drogas.initDrogas();

                    // recarga el formulario para reflejar los cambios
                    drogas.muestraDrogas();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que llamado al pulsar el botón borrar que
     * verifica si puede borrar el registro
     */
    borraDroga(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la rutina php
        $.get('drogas/puedeborrar.php?id='+id,
            function(data){

            // si retornó correcto
            if (data.Registros != 0){

                // llamamos la rutina de eliminación
                drogas.eliminaDroga(id);

            // si no puede borrar
            } else {

                // presenta el mensaje
                new jBox('Notice', {content: "El registro tiene pacientes", color: 'red'});

            }

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método llamado luego de verificar que puede
     * borrar el registro y luego de pedir confirmación
     * elimina el registro y recarga la grilla
     */
    eliminaDroga(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Droga',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('drogas/borrar?id='+id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // inicializamos las variables
                            drogas.initDrogas();

                            // recargamos la grilla
                            drogas.muestraDrogas();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un elemento html
     * @param {int} iddroga - clave del registro preseleccionado
     * Método que recibe como parámetro la id de un elemento del
     * formulario y carga en ese elemento la nómina de tipos de
     * derivacion
     */
    nominaDrogas(idelemento, iddroga){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "drogas/nominadrogas.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // componemos el nombre
                    var Nombre = data[i].droga + " - " + data[i].dosis + " - " + data[i].comercial;

                    // si recibió la clave de la derivacion
                    if (typeof(idderivacion) != "undefined"){

                        // si coincide
                        if (data[i].Id == idderivacion){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].id + ">" + Nombre + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].id + ">" + Nombre + "</option>");

                        }

                    // si no recibió la clave
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].id + ">" + Nombre + "</option>");
                    }

                }

        }});

    }

}