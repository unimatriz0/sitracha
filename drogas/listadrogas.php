<?php

/**
 *
 * listadrogas | drogas/listadrogas.php
 *
 * @package     Diagnostico
 * @subpackage  Drogas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta la grilla con los las drogas utilizadas
 * y permite el abm
*/

// incluimos e instanciamos las clases
require_once("drogas.class.php");
$drogas = new Drogas();

// obtenemos la matriz
$nomina = $drogas->nominaDrogas();

// presentamos el título
echo "<h2>Drogas Utilizadas</h2>";

// definimos la tabla
echo "<table id='drogas'
             whidth='60%'
             align='center'
             border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Droga</th>";
echo "<th>Dosis</th>";
echo "<th>Comercial</th>";
echo "<th>Usuario</th>";
echo "<th></th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abre la fila
    echo "<tr>";

    // presentamos el nombre de la droga
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre de la droga'>";
    echo "<input type='text'
                 size='20'
                 name='droga_$id'
                 id='droga_$id'
                 value = '$droga'>";
    echo "</span>";
    echo "</td>";

    // presenta la dosis
    echo "<td>";
    echo "<span class='tooltip'
                title='Dosis en miligramos'>";
    echo "<input type='text'
                 size='10'
                 name='dosis_$id'
                 id='dosis_$id'
                 value = '$dosis'>";
    echo "</span>";
    echo "</td>";

    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre comercial'>";
    echo "<input type='text'
                 size='30'
                 name='comercial_$id'
                 id='comercial_$id'
                 value = '$comercial'>";
    echo "</span>";
    echo "</td>";

    // presenta el usuario
    echo "<td align='center'>$usuario</td>";

    // presenta el enlace de edición
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaDroga'
           id='btnGrabaDroga'
           title='Pulse para grabar el registro'
           onClick='drogas.verificaDroga($id)'
           class='botongrabar'>";
    echo "</td>";

    // presenta el enlace de borrar
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnBorraDroga'
           id='btnBorraDroga'
           title='Pulse para borrar el registro'
           onClick='drogas.borraDroga($id)'
           class='botonborrar'>";
    echo "</td>";

    // cierra la fila
    echo "</tr>";

}

// agregamos la última fila
echo "<tr>";

// presentamos el nombre de la droga
echo "<td>";
echo "<span class='tooltip'
            title='Nombre de la droga'>";
echo "<input type='text'
                size='20'
                name='droga_nuevo'
                id='droga_nuevo'>";
echo "</span>";
echo "</td>";

// presenta la dosis
echo "<td>";
echo "<span class='tooltip'
            title='Dosis en miligramos'>";
echo "<input type='text'
                size='10'
                name='dosis_nuevo'
                id='dosis_nuevo'>";
echo "</span>";
echo "</td>";

echo "<td>";
echo "<span class='tooltip'
            title='Nombre comercial'>";
echo "<input type='text'
                size='30'
                name='comercial_nuevo'
                id='comercial_nuevo'>";
echo "</span>";
echo "</td>";

// presenta el usuario
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Usuario que ingresó el registro'>";
echo "<input type='text'
             size='10'
             name='usuario_droga'
             id='usuario_droga'
             readonly>";
echo "</span>";
echo "</td>";

// presenta el enlace de edición
echo "<td align='center'>";
echo "<input type='button'
       name='btnGrabaDroga'
       id='btnGrabaDroga'
       title='Pulse para grabar el registro'
       onClick='drogas.verificaDroga()'
       class='botongrabar'>";
echo "</td>";

// presenta la última celda y cierra
echo "<td align='center'></td>";
echo "<tr>";

// cierra la fila
echo "</tr>";

// cerramos la tabla
echo "</table>";

?>

<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

    // carga el usuario y la fecha de alta
    document.getElementById("usuario_droga").value = sessionStorage.getItem("Usuario");

</script>