<?php

/**
 *
 * drogas | drogas/nominadrogas.php
 *
 * @package     Diagnostico
 * @subpackage  Drogas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna el array json con la nómina de drogas
 * utilizado para cargar los combos
*/

// incluimos e instanciamos las clases
require_once("drogas.class.php");
$droga = new Drogas();

// ejecutamos la consulta
$nomina = $droga->nominaDrogas();

// encodeamos el array y retornamos
echo json_encode($nomina);

?>