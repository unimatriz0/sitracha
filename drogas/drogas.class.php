<?php

/**
 *
 * Class Drogas | drogas/drogas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Drogas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * drogas utilizadas
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Drogas {

    // declaración de variables
    protected $Link;                   // puntero a la base de datos
    protected $Id;                     // clave del registro
    protected $Droga;                  // nombre de la droga
    protected $Dosis;                  // dosis en miligramos
    protected $Comercial;              // nombre comercial de la droga
    protected $IdUsuario;              // clave del usuario
    protected $Usuario;                // nombre del usuario
    protected $Fecha;                  // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();
        $this->Id = 0;
        $this->Droga = "";
        $this->Dosis = 0;
        $this->Comercial = "";
        $this->Usuario = "";
        $this->Fecha = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setDroga($droga){
        $this->Droga = $droga;
    }
    public function setDosis($dosis){
        $this->Dosis = $dosis;
    }
    public function setComercial($comercial){
        $this->Comercial = $comercial;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getDroga(){
        return $this->Droga;
    }
    public function getDosis(){
        return $this->Dosis;
    }
    public function getComercial(){
        return $this->Comercial;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iddroga entero con la clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * asigna en las variables de clase los valores del mismo
     */
    public function getDatosDroga($iddroga){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_drogas.id AS id,
                            diagnostico.v_drogas.droga AS droga,
                            diagnostico.v_drogas.dosis AS dosis,
                            diagnostico.v_drogas.comercial AS comercial,
                            diagnostico.v_drogas.usuario AS usuario,
                            diagnostico.v_drogas.fecha AS fecha
                     FROM diagnostico.v_drogas
                     WHERE diagnostico.v_drogas.id = '$iddroga'; ";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y asignamos en las
        // variables de clase
        extract($fila);
        $this->Id = $id;
        $this->Droga = $droga;
        $this->Dosis = $dosis;
        $this->Comercial = $comercial;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de registros
     * Método que retorna un resultset con el listado de
     * todas las drogas utilizadas, este es utilizado para
     * completar los combos ya que concatena la dosis
     */
    public function listaDrogas(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT diagnostico.v_drogas.id AS id,
                            CONCAT(diagnostico.v_drogas.droga, ' - ', diagnostico.v_drogas.dosis) AS droga
                     FROM diagnostico.v_drogas
                     ORDER BY diagnostico.v_drogas.droga; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de registros
     * Método que retorna un resultset con el listado de
     * todas las drogas utilizadas
     */
    public function nominaDrogas(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT diagnostico.v_drogas.id AS id,
                            diagnostico.v_drogas.droga AS droga,
                            diagnostico.v_drogas.dosis AS dosis,
                            diagnostico.v_drogas.comercial AS comercial,
                            diagnostico.v_drogas.usuario AS usuario
                     FROM diagnostico.v_drogas
                     ORDER BY diagnostico.v_drogas.droga; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro afectado
     */
    public function grabaDroga(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaDroga();
        } else {
            $this->editaDroga();
        }

        // retorna la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevaDroga(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.drogas
                                (droga,
                                 dosis,
                                 comercial,
                                 usuario)
                                VALUES
                                (:droga,
                                 :dosis,
                                 :comercial,
                                 :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":droga",     $this->Droga);
        $psInsertar->bindParam(":dosis",     $this->Dosis);
        $psInsertar->bindParam(":comercial", $this->Comercial);
        $psInsertar->bindParam(":usuario",   $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaDroga(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.drogas SET
                            droga = :droga,
                            dosis = :dosis,
                            comercial = :comercial,
                            usuario = :usuario
                     WHERE diagnostico.drogas.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":droga",     $this->Droga);
        $psInsertar->bindParam(":dosis",     $this->Dosis);
        $psInsertar->bindParam(":comercial", $this->Comercial);
        $psInsertar->bindParam(":usuario",   $this->IdUsuario);
        $psInsertar->bindParam(":id",        $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iddroga clave de la droga
     * @return boolean si puede borrar
     * Método llamado antes de borrar que verifica si en la
     * tabla de tratamiento existe algún registro para la
     * droga que recibe como parámetro
     */
    public function puedeBorrar($iddroga){

        // componemos la consulta sobre la tabla de tratamiento
        $consulta = "SELECT COUNT(diagnostico.tratamiento.id) AS registros
                     FROM diagnostico.tratamiento
                     WHERE diagnostico.tratamiento.droga = '$iddroga';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // según si halló
        if ($registros != 0){
            return false;
        } else {
            return true;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iddroga entero con la clave del registro
     * M{etodo que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public function borraDroga($iddroga){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.drogas
                     WHERE diagnostico.drogas.id = '$iddroga'; ";
        $this->Link->exec($consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param droga cadena con el nombre de la droga
     * @param dosis entero con la dosis
     * @param comercial nombre comercial de la droga
     * @return boolean si existe la droga en la base
     * Método utilizado para evitar el ingreso de registros
     * duplicados recibe como parámetros el nombre de la
     * droga y la dosis y retorna si ya está declarada
     * en la base
     */
    public function validaDroga($droga, $dosis, $comercial){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.drogas.id) AS registros
                     FROM diagnostico.drogas
                    WHERE diagnostico.drogas.droga = '$droga' AND
                          diagnostico.drogas.dosis = '$dosis' AND
                          diagnostico.drogas.comercial = '$comercial'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // según si encontró
        if ($registros != 0){
            return false;
        } else {
            return true;
        }

    }

}
?>