<?php

/**
 *
 * instituciones/buscar.php
 *
 * @package     Diagnostico
 * @subpackage  Instituciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una cadena de texto y realiza la 
 * búsqueda en la tabla de instituciones, luego arma la grilla
 * con los registros obtenidos (permitiendo editarlos)
 * 
*/

// incluimos e instanciamos la clase
require_once ("instituciones.class.php");
$institucion = new Instituciones();

// buscamos el texto
$resultado = $institucion->buscaInstitucion($_GET["texto"]);

// si no hubo resultados
if (count($resultado) == 0){

    // presenta el mensaje
    echo "<h2>No se han encontrado registros coincidentes</h2>";

// si encontró
} else {

    // define la tabla
    echo "<table width='90%' align='center' border='0' id='busca_institucion'>";

    // define los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Nombre</th>";
    echo "<th>Responsable</th>";
    echo "<th>Jurisdicción</th>";
    echo "<th>Localidad</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach ($resultado AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // lo agregamos
        echo "<td><small>$institucion</small></td>";
        echo "<td><small>$responsable</small></td>";
        echo "<td><small>$provincia</small></td>";
        echo "<td><small>$localidad</small></td>";

        // armamos el enlace
        echo "<td>";
        echo "<input type='button'
               id='btnVerInstitucion
               name='btnVerInstitucion'
               class='botoneditar'
               title='Pulse para ver el registro'
               onClick='instituciones.getDatosInstitucion($id_institucion)'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cierra la tabla
    echo "</tbody>";
    echo "</table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

}

?>
<SCRIPT>

    // definimos las propiedades de la tabla
     $('#busca_institucion').datatable({
        pageSize: 15,
        sort: [true, true, true, true, false],
        filters: [true, true, 'select', true, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>