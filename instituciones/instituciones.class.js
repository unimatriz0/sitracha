/*
    Nombre: instituciones.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 12/01/2018
    E-Mail:/cinvernizzi@gmail.com/
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de instituciones asistenciales
    del sistema

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre instituciones asistenciales
 */
class Instituciones {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor() {

        // inicializamos las variables de clase
        this.initInstituciones();

        // inicializamos el layer
        this.layerInstitucion = "";

        // cargamos el formulario
        $("#form_instituciones").load("instituciones/form_instituciones.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initInstituciones(){

        // inicializamos las variables
        this.Id = 0;                  // clave de la institución
        this.Institucion = "";        // nombre de la institución
        this.Siisa = "";              // código siisa de la institución
        this.CodLoc = "";             // clave indec de la localidad
        this.Localidad = "";          // nombre de la localidad
        this.CodProv = "";            // clave de la jurisdicción
        this.Provincia = "";          // nombre de la provincia
        this.IdPais = 0;              // clave del país
        this.Pais = "";               // nombre del país
        this.Direccion = "";          // dirección postal
        this.CodigoPostal = "";       // código postal
        this.Telefono = "";           // teléfono de la institución
        this.Mail = "";               // correo electrónico de la institución
        this.Responsable = "";        // nombre del responsable o director
        this.IdUsuario = 0;           // clave del usuario
        this.Usuario = "";            // nombre del usuario
        this.IdDependencia = 0;       // clave de la fuente de financiamiento
        this.Dependencia = "";        // nombre de la fuente de financiamiento
        this.FechaAlta = "";          // fecha de alta del registro
        this.Comentarios = "";        // comentarios del usuario
        this.Coordenadas = "";        // coordenadas gps

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setInstitucion(institucion){
        this.Institucion = institucion;
    }
    setSiisa(siisa){
        this.Siisa = siisa;
    }
    setCodLoc(codloc){
        this.CodLoc = codloc;
    }
    setLocalidad(localidad){
        this.Localidad = localidad;
    }
    setCodProv(codprov){
        this.CodProv = codprov;
    }
    setProvincia(provincia){
        this.Provincia = provincia;
    }
    setIdPais(idpais){
        this.IdPais = idpais;
    }
    setPais(pais){
        this.Pais = pais;
    }
    setDireccion(direccion){
        this.Direccion = direccion;
    }
    setCodigoPostal(codigopostal){
        this.CodigoPostal = codigopostal;
    }
    setTelefono(telefono){
        this.Telefono = telefono;
    }
    setMail(mail){
        this.Mail = mail;
    }
    setResponsable(responsable){
        this.Responsable = responsable;
    }
    setIdUsuario(idusuario){
        this.IdUsuario = idusuario;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setIdDependencia(iddependencia){
        this.IdDependencia = iddependencia;
    }
    setDependencia(dependencia){
        this.Dependencia = dependencia;
    }
    setFechaAlta(fechaalta){
        this.FechaAlta = fechaalta;
    }
    setComentarios(comentarios){
        this.Comentarios = comentarios;
    }
    setCoordenadas(coordenadas){
        this.Coordenadas = coordenadas;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario y las variables de clase para
     * ingresar una nueva institución
     */
    nuevaInstitucion(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // reiniciamos el formulario y limpiamos los select de
        // provincia y localidad
        document.getElementById('id_institucion').value = "";
        document.getElementById('nombre_institucion').value = "";
        document.getElementById('id_siisa').value = "";
        document.getElementById('pais_institucion').value = "";
        $("#provincia_institucion").html('');
        $("#provincia_institucion").append("<option></option>");
        $("#provincia_institucion").append("<option>Seleccione un País</option>");
        $("#localidad_institucion").html('');
        $("#localidad_institucion").append("<option></option>");
        $("#localidad_institucion").append("<option>Seleccione una Jurisdicción</option>");
        document.getElementById('direccion_institucion').value = "";
        document.getElementById('cod_postal_institucion').value = "";
        document.getElementById('telefono_institucion').value = "";
        document.getElementById('mail_institucion').value = "";
        document.getElementById('responsable_institucion').value = "";
        document.getElementById('dependencia_institucion').value = "";
        document.getElementById('alta_institucion').value = fechaActual();
        document.getElementById('usuario_institucion').value = sessionStorage.getItem("Usuario");

        // reiniciamos las variables de clase
        this.initInstituciones();

        // seteamos el foco
        document.getElementById('nombre_institucion').focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el cuadro de diálogo para la búsqueda de instituciones
     */
    buscaInstitucion(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var contenido;

        // definimos el contenido
        contenido = "Ingrese parte del nombre de la Institución a buscar.<br>";
        contenido += "<form name='busca_institucion'>";
        contenido += "<p><b>Texto:</b> ";
        contenido += "<input type='text' name='texto' size='25'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Buscar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='instituciones.encuentraInstitucion()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerInstitucion = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    content: contenido,
                                    draggable: 'title',
                                    title: 'Buscar Institucion',
                                    theme: 'TooltipBorder',
                                    width: 300,
                            });
        this.layerInstitucion.open();

        // fijamos el foco
        document.busca_institucion.texto.focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de búsqueda y luego presenta la grilla
     * de resultados
     */
    encuentraInstitucion(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definición de variables
        var mensaje;
        var texto = document.busca_institucion.texto.value;

        // si no ingresó texto
        if (texto == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar un texto a buscar !!!";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.busca_institucion.texto.focus();
            return false;

        }

        // cierra el cuadro de diálogo
        this.layerInstitucion.destroy();

        // abre el cuadro de resultados y le pasa por ajax
        // el texto a buscar
        this.layerInstitucion = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    title: 'Resultados de la Búsqueda',
                                    theme: 'TooltipBorder',
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    ajax: {
                                    url: 'instituciones/buscar.php?texto='+texto,
                                    reload: 'strict'
                                }
                            });
        this.layerInstitucion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el combo de dependencias
     */
    cargaDependencias(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llamamos la clase javascript
        dependencias.listaDependencias("dependencia_institucion");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpais - clave del país seleccionado
     * Método que carga el combo de países, si recibe la id de un país
     * lo preselecciona
     */
    cargaPaises(idpais){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la clase de javascript
        paises.listaPaises("pais_institucion", idpais);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpais - clave del país seleccionado
     * @param {string} idprovincia - clave de la provincia seleccionada
     * Método que recibe como parámetro la id de un país y la clave de una
     * jurisdicción, si no recibe ninguno de ellos asume que fue llamado
     * por el evento onchange
     */
    cargaProvincias(idpais, idprovincia){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos las variables
        var claveprovincia = "";

        // si no recibió la id
        if (typeof(idpais) == "undefined"){

            // lee del formulario
            idpais = document.getElementById("pais_institucion").value;

        }

        // llamamos la clase de jurisdicciones
        provincias.listaJurisdicciones("provincia_institucion", idpais, idprovincia);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idprovincia - clave de la provincia seleccionada
     * @param {string} idlocalidad - clave de la localidad seleccionada
     * Método que recibe como parámetro la id de una provincia y la id
     * de una localidad, si no recibe ninguno de ellos asume que fue
     * llamado por el evento onchange de provincias
     */
    cargaLocalidades(idprovincia, idlocalidad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var clavelocalidad = "";

        // si no recibió la clave de la provincia
        if (typeof(idprovincia) == "undefined"){

            // lee del formulario
            idprovincia = document.getElementById("provincia_institucion").value;

            // si fue llamado por el onchange y está en blanco
            if (idprovincia == ""){

                // simplemente retorna
                document.getElementById("provincia_institucion").focus();
                return false;

            }

        }

        // llama la clase de localidades
        ciudades.nominaLocalidades("localidad_institucion", idprovincia, idlocalidad);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idinstitucion - clave de la institución a mostrar
     * Método que recibe como parámetro la id de una institución y obtiene por ajax
     * los valores del registro y los asigna en la variable de clase
     */
    getDatosInstitucion(idinstitucion){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos los datos de la institución
        $.get('instituciones/getinstitucion.php', 'id='+idinstitucion,
            function(data){

                // asignamos los valores en las variables de clase
                // métodos de asignación de valores
                instituciones.setId(data.Id);
                instituciones.setInstitucion(data.Institucion);
                instituciones.setSiisa(data.Siisa);
                instituciones.setCodLoc(data.CodLoc);
                instituciones.setCodProv(data.CodProv);
                instituciones.setIdPais(data.IdPais);
                instituciones.setDireccion(data.Direccion);
                instituciones.setCodigoPostal(data.CodigoPostal);
                instituciones.setTelefono(data.Telefono);
                instituciones.setMail(data.Mail);
                instituciones.setResponsable(data.Responsable);
                instituciones.setUsuario(data.Usuario);
                instituciones.setIdDependencia(data.IdDependencia);
                instituciones.setFechaAlta(data.FechaAlta);
                instituciones.setComentarios(data.Comentarios);
                instituciones.setCoordenadas(data.Coordenadas);

                // mostramos el registro
                instituciones.muestraInstitucion();

            }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase muestra los datos de la
     * institución
     */
    muestraInstitucion(){

        // cargamos en el formulario los valores de las variables de clase
        document.getElementById("id_institucion").value = this.Id;
        document.getElementById("nombre_institucion").value = this.Institucion;
        document.getElementById("id_siisa").value = this.Siisa;
        document.getElementById("pais_institucion").value = this.IdPais;
        this.cargaProvincias(this.IdPais, this.CodProv);
        this.cargaLocalidades(this.CodProv, this.CodLoc);
        document.getElementById("direccion_institucion").value = this.Direccion;
        document.getElementById("cod_postal_institucion").value = this.CodigoPostal;
        document.getElementById("telefono_institucion").value = this.Telefono;
        document.getElementById("mail_institucion").value = this.Mail;
        document.getElementById("responsable_institucion").value = this.Responsable;
        document.getElementById("dependencia_institucion").value = this.IdDependencia;
        document.getElementById("alta_institucion").value = this.FechaAlta;
        document.getElementById("usuario_institucion").value = this.Usuario;
        document.getElementById("coordenadas_institucion").value = this.Coordenadas;
        document.getElementById("observacionesinstitucion").value = this.Comentarios;
        CKEDITOR.instances['observacionesinstitucion'].setData(this.Comentarios);

        // activamos la seguridad del registro
        this.seguridadInstituciones();

        // si las coordenadas son válidas
        if (this.Coordenadas != ""){

            // muestra el mapa
            $("#btnMapaInstitucion").show();

        // si no existen
        } else {

            // lo oculta
            $("#btnMapaInstitucion").hide();

        }

        // si llamó desde el buscador
        this.layerInstitucion.destroy();

        // fijamos el foco
        document.getElementById("nombre_institucion").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de instituciones y asigna los
     * valores a las variables de clase
     */
    verificaInstitucion(){

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById('id_institucion').value != ""){
            this.Id = document.getElementById('id_institucion').value;
        }

        // si no ingresó el nombre de la institución
        if (document.getElementById('nombre_institucion').value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la Institución";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_institucion").focus();
            return false;

        // si ingresó
        } else {

            // asigna
            this.Institucion = document.getElementById('nombre_institucion').value;

        }

        // si no ingresó el código siisa
        if (document.getElementById('id_siisa').value == ""){

            // presenta la alerta
            mensaje = "No existe código Sissa";
            new jBox('Notice', {content: mensaje, color: 'red'});

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Siisa = document.getElementById('id_siisa').value;

        }

        // si no seleccionó localidad
        if (document.getElementById('localidad_institucion').value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el País, la Jurisdicción y la\n";
            mensaje += "Localidad de la Institución";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("localidad_institucion").focus();
            return false;

        // si seleccionó correctamente
        } else {

            // asigna en la variable de clase
            this.CodLoc = document.getElementById('localidad_institucion').value;

        }

        // si no ingresó la dirección
        if (document.getElementById('direccion_institucion').value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la dirección postal para georreferenciarla";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("direccion_institucion").focus();
            return false;

        // si ingresó
        } else {

            // asigna en las variables de clase
            this.Direccion = document.getElementById('direccion_institucion').value;

        }

        // si no ingresó a la localidad
        if (document.getElementById('cod_postal_institucion').value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el Código Postal de la Localidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cod_postal_institucion").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.CodigoPostal = document.getElementById('cod_postal_institucion').value;

        }

        // el teléfono lo permite en blanco
        this.Telefono = document.getElementById("telefono_institucion").value;

        // si no ingresó el mail
        if (document.getElementById('mail_institucion').value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique un mail de la Institución";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("mail_institucion").focus();
            return false;

        // verifica el formato
        } else if (!echeck(document.getElementById('mail_institucion').value)){

            // presenta el mensaje y retorna
            mensaje = "El formato del mail parece incorrecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("mail_institucion").focus();
            return false;

        // si está correcto
        } else {

            // asigna en la variable de clase
            this.Mail = document.getElementById('mail_institucion').value;

        }

        // si no ingresó el responsable
        if (document.getElementById('responsable_institucion').value == ""){

            // presenta el mensaje y continúa
            mensaje = "No se ha ingresado información del responsable";
            new jBox('Notice', {content: mensaje, color: 'red'});

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Responsable = document.getElementById('responsable_institucion').value;

        }

        // si no ingresó dependencia
        if (document.getElementById('dependencia_institucion').value == ""){

            // presenta el alerta y retorna
            mensaje = "Seleccione la dependencia administrativa";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("dependencia_institucion").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.IdDependencia = document.getElementById('dependencia_institucion').value;

        }

        // asigna los comentarios
        this.Comentarios = CKEDITOR.instances['observacionesinstitucion'].getData();

        // asigna las coordenadas
        this.Coordenadas = document.getElementById('coordenadas_institucion').value;

        // grabamos el registro
        this.grabaInstitucion();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de los datos de las variables de clase envía la consulta
     * al servidor
     */
    grabaInstitucion(){

        // declaración de variables
        var datosInstitucion = new FormData();

        // si está editando
        if (this.Id != 0){
            datosInstitucion.append("Id", this.Id);
        }

        // completa el resto de los datos
        datosInstitucion.append("Institucion", this.Institucion);
        datosInstitucion.append("Siisa", this.Siisa);
        datosInstitucion.append("CodLoc", this.CodLoc);
        datosInstitucion.append("Direccion", this.Direccion);
        datosInstitucion.append("CodigoPostal", this.CodigoPostal);
        datosInstitucion.append("Telefono", this.Telefono);
        datosInstitucion.append("Mail", this.Mail);
        datosInstitucion.append("Responsable", this.Responsable);
        datosInstitucion.append("Dependencia", this.IdDependencia);
        datosInstitucion.append("Comentarios", this.Comentarios);
        datosInstitucion.append("Coordenadas", this.Coordenadas);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "instituciones/graba_institucion.php",
            data: datosInstitucion,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // almacenamos la id en el formulario
                    document.getElementById("id_institucion").value = data.Id;
                    this.Id = data.Id;

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // setea el foco
                    document.getElementById("nombre_institucion").focus();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que cancela los cambios del formulario
     */
    cancelaInstitucion(){

        // reiniciamos las variables de clase
        this.initInstituciones();

        // reinicia el formulario y setea el foco
        document.getElementById('form_instituciones').reset();
        document.getElementById('nombre_institucion').focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de sesión fija el nivel de acceso
     * del usuario
     */
    seguridadInstituciones(){

        // aquí solo podrán editar instituciones los responsables
        // de nivel central
        if (sessionStorage.getItem("Administrador") != "Si"){

            // oculta los botones
            $("#btnGrabaInstitucion").hide();

        // si es administrador
        } else {

            // los muestra
            $("#btnGrabaInstitucion").show();

        }

    }

}