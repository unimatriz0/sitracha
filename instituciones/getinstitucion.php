<?php

/**
 *
 * instituciones/getinstitucion.php
 *
 * @package     Diagnostico
 * @subpackage  Instituciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una institución y 
 * obtiene los datos del registro, los que retorna en un 
 * array json 
 * 
*/

 // incluimos e instanciamos la clase
 require_once("instituciones.class.php");
 $institucion = new Instituciones();

 // obtenemos los datos del registro
 $institucion->getDatosInstitucion($_GET["id"]);

 // retornamos los valores
 echo json_encode(array("Id" =>            $institucion->getIdInstitucion(),
                        "Institucion" =>   $institucion->getInstitucion(),
                        "Siisa" =>         $institucion->getSiisa(),
                        "CodLoc" =>        $institucion->getCodLoc(),
                        "CodProv" =>       $institucion->getCodProv(),
                        "IdPais" =>        $institucion->getIdPais(),
                        "Direccion" =>     $institucion->getDireccion(),
                        "CodigoPostal" =>  $institucion->getCodigoPostal(),
                        "Telefono" =>      $institucion->getTelefono(),
                        "Mail" =>          $institucion->getMail(),
                        "Responsable" =>   $institucion->getResponsable(),
                        "Usuario" =>       $institucion->getUsuario(),
                        "IdDependencia" => $institucion->getIdDependencia(),
                        "FechaAlta" =>     $institucion->getFechaAlta(),
                        "Coordenadas" =>   $institucion->getCoordenadas(),
                        "Comentarios" =>   $institucion->getComentarios()));

?>