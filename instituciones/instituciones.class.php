<?php

/**
 *
 * Class Instituciones | instituciones/instituciones.class.php
 *
 * @package     Diagnostico
 * @subpackage  Instituciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/01/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de 
 * instituciones asistenciales
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Instituciones {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdLaboratorio;         // clave del laboratorio del usuario
    protected $IdUsuario;             // clave del usuario activo

    // variables de la base de datos
    protected $IdInstitucion;         // clave de la institución
    protected $Institucion;           // nombre de la institución
    protected $Siisa;                 // clave siisa de la institución
    protected $CodLoc;                // clave indec de la localidad
    protected $Localidad;             // nombre de la localidad
    protected $CodProv;               // clave indec de la provincia
    protected $Provincia;             // nombre de la provincia
    protected $IdPais;                // clave del país
    protected $Pais;                  // nombre del país
    protected $Direccion;             // dirección postal
    protected $CodigoPostal;          // código postal de la dirección
    protected $Telefono;              // teléfono de la institución
    protected $Mail;                  // correo electrónico
    protected $Responsable;           // responsable o director
    protected $Usuario;               // nombre del usuario
    protected $IdDependencia;         // clave de la dependencia
    protected $Dependencia;           // nombre de la dependencia
    protected $FechaAlta;             // fecha de alta del registro
    protected $Comentarios;           // comentarios y observaciones
    protected $Coordenadas;           // coordenadas gps

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos las variables de la base de datos
        $this->IdInstitucion = 0;
        $this->Institucion = "";
        $this->Siisa = "";
        $this->CodLoc = "";
        $this->Localidad = "";
        $this->CodProv = "";
        $this->Provincia = "";
        $this->IdPais = 0;
        $this->Pais = "";
        $this->Direccion = "";
        $this->CodigoPostal = "";
        $this->Telefono = "";
        $this->Mail = "";
        $this->Responsable = "";
        $this->Usuario = "";
        $this->IdDependencia = 0;
        $this->Dependencia = "";
        $this->FechaAlta = "";
        $this->Comentarios = "";
        $this->Coordenadas = "";

        // iniciamos sesión
        session_start();

        // si existen los valores de sesión
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y el laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];

        // si no inició
        } else {

            // abandona por error
            echo "No hay sesión iniciada";
            exit;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdInstitucion($idinstitucion){
        $this->IdInstitucion = $idinstitucion;
    }
    public function setInstitucion($institucion){
        $this->Institucion = $institucion;
    }
    public function setSiisa($siisa){
        $this->Siisa = $siisa;
    }
    public function setCodLoc($codloc){
        $this->CodLoc = $codloc;
    }
    public function setLocalidad($localidad){
        $this->Localidad = $localidad;
    }
    public function setCodProv($codprov){
        $this->CodProv = $codprov;
    }
    public function setProvincia($provincia){
        $this->Provincia = $provincia;
    }
    public function setIdPais($idpais){
        $this->IdPais = $idpais;
    }
    public function setPais($pais){
        $this->Pais = $pais;
    }
    public function setDireccion($direccion){
        $this->Direccion = $direccion;
    }
    public function setCodigoPostal($codigopostal){
        $this->CodigoPostal = $codigopostal;
    }
    public function setTelefono($telefono){
        $this->Telefono = $telefono;
    }
    public function setMail($mail){
        $this->Mail = $mail;
    }
    public function setResponsable($responsable){
        $this->Responsable = $responsable;
    }
    public function setIdDependencia($iddependencia){
        $this->IdDependencia = $iddependencia;
    }
    public function setDependencia($dependencia){
        $this->Dependencia = $dependencia;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }
    public function setCoordenadas($coordenadas){
        $this->Coordenadas = $coordenadas;
    }

    // métodos de retorno de valores
    public function getIdInstitucion(){
        return $this->IdInstitucion;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getSiisa(){
        return $this->Siisa;
    }
    public function getCodLoc(){
        return $this->CodLoc;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getCodProv(){
        return $this->CodProv;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getIdPais(){
        return $this->IdPais;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getDireccion(){
        return $this->Direccion;
    }
    public function getCodigoPostal(){
        return $this->CodigoPostal;
    }
    public function getTelefono(){
        return $this->Telefono;
    }
    public function getMail(){
        return $this->Mail;
    }
    public function getResponsable(){
        return $this->Responsable;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getIdDependencia(){
        return $this->IdDependencia;
    }
    public function getDependencia(){
        return $this->Dependencia;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }
    public function getCoordenadas(){
        return $this->Coordenadas;
    }

    /**
     * Método que recibe una cadena y busca la ocurrencia de esa cadena en la
     * tabla de instituciones, retorna un array con los registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto - cadena a buscar en la base
     * @return array $nomina
     */
    public function buscaInstitucion($texto){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_instituciones.id_centro AS id_institucion,
                            diagnostico.v_instituciones.nombre_centro AS institucion,
                            diagnostico.v_instituciones.localidad AS localidad,
                            diagnostico.v_instituciones.provincia AS provincia,
                            diagnostico.v_instituciones.responsable AS responsable
                     FROM diagnostico.v_instituciones
                     WHERE diagnostico.v_instituciones.nombre_centro LIKE '%$texto%' OR
                           diagnostico.v_instituciones.localidad LIKE '%$texto%' OR
                           diagnostico.v_instituciones.provincia LIKE '%$texto%' OR
                           diagnostico.v_instituciones.responsable LIKE '%$texto%'
                     ORDER BY diagnostico.v_instituciones.provincia,
                              diagnostico.v_instituciones.nombre_centro;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

    /**
     * Método que recibe como parámetro la clave de una institución y asigna
     * en las variables de clase los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $clave - clave del registro
     */
    public function getDatosInstitucion($clave){

        // inicializamos las variables
        $id_institucion = 0;
        $institucion = "";
        $siisa = "";
        $codloc = "";
        $localidad = "";
        $codprov = "";
        $provincia = "";
        $pais = "";
        $idpais = 0;
        $direccion = "";
        $codigo_postal = "";
        $telefono = "";
        $mail = "";
        $responsable = "";
        $usuario = "";
        $dependencia = "";
        $id_dependencia = "";
        $fecha_alta = "";
        $comentarios = "";
        $coordenadas = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_instituciones.id_centro AS id_institucion,
                            diagnostico.v_instituciones.nombre_centro AS institucion,
                            diagnostico.v_instituciones.siisa AS siisa,
                            diagnostico.v_instituciones.codloc AS codloc,
                            diagnostico.v_instituciones.localidad AS localidad,
                            diagnostico.v_instituciones.codprov As codprov,
                            diagnostico.v_instituciones.provincia AS provincia,
                            diagnostico.v_instituciones.pais AS pais,
                            diagnostico.v_instituciones.idpais AS idpais,
                            diagnostico.v_instituciones.direccion AS direccion,
                            diagnostico.v_instituciones.codigo_postal AS codigo_postal,
                            diagnostico.v_instituciones.telefono AS telefono,
                            diagnostico.v_instituciones.mail AS mail,
                            diagnostico.v_instituciones.responsable AS responsable,
                            diagnostico.v_instituciones.id_usuario AS idusuario,
                            diagnostico.v_instituciones.usuario AS usuario,
                            diagnostico.v_instituciones.id_dependencia AS id_dependencia,
                            diagnostico.v_instituciones.dependencia AS dependencia,
                            diagnostico.v_instituciones.fecha_alta AS fecha_alta,
                            diagnostico.v_instituciones.comentarios AS comentarios,
                            diagnostico.v_instituciones.coordenadas AS coordenadas
                     FROM diagnostico.v_instituciones
                     WHERE diagnostico.v_instituciones.id = '$clave';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($registro);

        // asignamos en las variables de clase
        $this->IdInstitucion = $id_institucion;
        $this->Institucion = $institucion;
        $this->Siisa = $siisa;
        $this->CodLoc = $codloc;
        $this->Localidad = $localidad;
        $this->CodProv = $codprov;
        $this->Provincia = $provincia;
        $this->IdPais = $idpais;
        $this->Pais = $pais;
        $this->Direccion = $direccion;
        $this->CodigoPostal = $codigo_postal;
        $this->Telefono = $telefono;
        $this->Mail = $mail;
        $this->Responsable = $responsable;
        $this->Usuario = $usuario;
        $this->IdDependencia = $id_dependencia;
        $this->Dependencia = $dependencia;
        $this->FechaAlta = $fecha_alta;
        $this->Comentarios = $comentarios;
        $this->Coordenadas = $coordenadas;

    }

    /**
     * Método que según el caso llama la consulta de edición o inserción,
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idinstitucion - clave del registro insertado / editado
     */
    public function grabaInstitucion(){

        // si está insertando
        if ($this->IdInstitucion == 0){

            // inserta el registro
            $this->nuevaInstitucion();

        // si está editando
        } else {

            // actualiza
            $this->editaInstitucion();

        }

        // retornamos la id
        return $this->IdInstitucion;

    }

    /**
     * Método llamado al insertar una institución
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaInstitucion(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.centros_asistenciales
                            (nombre,
                             siisa,
                             localidad,
                             direccion,
                             codigo_postal,
                             telefono,
                             mail,
                             responsable,
                             id_usuario,
                             dependencia,
                             comentarios,
                             coordenadas)
                            VALUES
                            (:nombre,
                             :siisa,
                             :idlocalidad,
                             :direccion,
                             :codigo_postal,
                             :telefono,
                             :mail,
                             :responsable,
                             :idusuario,
                             :dependencia,
                             :comentarios,
                             :coordenadas);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Institucion);
        $psInsertar->bindParam(":siisa", $this->Siisa);
        $psInsertar->bindParam(":idlocalidad", $this->CodLoc);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":mail", $this->Mail);
        $psInsertar->bindParam(":responsable", $this->Responsable);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":dependencia", $this->IdDependencia);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":coordenadas", $this->Coordenadas);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdInstitucion = $this->Link->lastInsertId();

    }

    /**
     * Método llamado al editar una institución
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaInstitucion(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.centros_asistenciales SET
                            nombre = :nombre,
                            siisa = :siisa,
                            localidad = :idlocalidad,
                            direccion = :direccion,
                            codigo_postal = :codigo_postal,
                            telefono = :telefono,
                            mail = :mail,
                            responsable = :responsable,
                            id_usuario = :idusuario,
                            dependencia = :dependencia,
                            comentarios = :comentarios,
                            coordenadas = :coordenadas
                     WHERE diagnostico.centros_asistenciales.id = :idinstitucion;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->Institucion);
        $psInsertar->bindParam(":siisa", $this->Siisa);
        $psInsertar->bindParam(":idlocalidad", $this->CodLoc);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":mail", $this->Mail);
        $psInsertar->bindParam(":responsable", $this->Responsable);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":dependencia", $this->IdDependencia);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":coordenadas", $this->Coordenadas);
        $psInsertar->bindParam(":idinstitucion", $this->IdInstitucion);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro el nombre de la institución y la clave de
     * la localidad, retorna verdadero si ya existe, utilizado para evitar
     * la inserción de duplicados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $institucion - nombre de la institución a verificar
     * @param string $codloc - código indec de la localidad
     * @return boolean
     */
    public function verificaInstitucion($institucion, $codloc){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.v_instituciones.id) AS registros
                     FROM diagnostico.v_instituciones
                     WHERE diagnostico.v_instituciones.institucion = '$institucion' AND
                           diagnostico.v_instituciones.codloc = '$codloc'; ";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // si hay registros
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

}
?>