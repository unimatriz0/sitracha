<?php

/**
 *
 * instituciones/graba_institucione.php
 *
 * @package     Diagnostico
 * @subpackage  Instituciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/01/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y 
 * graba el registro en la base, retorna el resultado de 
 * la operación
 * 
*/

// incluimos e instanciamos las clases
require_once ("instituciones.class.php");
$institucion = new Instituciones();

// si recibió la id
if (!empty($_POST["Id"])){
    $institucion->setIdInstitucion($_POST["Id"]);
}

// asigna en la clase el resto de los valores
$institucion->setInstitucion($_POST["Institucion"]);
$institucion->setSiisa($_POST["Siisa"]);
$institucion->setCodLoc($_POST["CodLoc"]);
$institucion->setDireccion($_POST["Direccion"]);
$institucion->setCodigoPostal($_POST["CodigoPostal"]);
$institucion->setTelefono($_POST["Telefono"]);
$institucion->setMail($_POST["Mail"]);
$institucion->setIdDependencia($_POST["Dependencia"]);
$institucion->setResponsable($_POST["Responsable"]);
$institucion->setComentarios($_POST["Comentarios"]);
$institucion->setCoordenadas($_POST["Coordenadas"]);

// grabamos el registro y retornamos la id
$id = $institucion->grabaInstitucion();
echo json_encode(array("Id" => $id));

?>