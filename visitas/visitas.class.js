/*
 * Nombre: visitas.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 11/06/2019
 * Licencia: GPL
 * Comentarios: Clase que provee los métodos para el formulario de
 *              visitas
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controlan el abm de visitas
 */
class Visitas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initVisitas();

        // inicializamos el layer
        this.layerVisitas = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initVisitas(){

        // inicializamos las variables de la tabla de visitas
        this.IdVisita = 0;           // clave de la visita
        this.FechaVisita = "";       // fecha de la visita
        this.Usuario = "";           // nombre del usuario
        this.Alta = "";              // fecha de alta del registro

        // inicializamos las variables de la tabla de clasificación
        this.IdClasificacion = 0;    // clave del registro
        this.Paciente = 0;           // clave del paciente (protocolo)
        this.Estadio = "";           // estadío de la enfermedad
        this.Observaciones = "";     // observaciones del usuario

        // datos de la tabla de examen físico
        this.IdFisico = 0;           // clave del registro
        this.Ta = "";                // tensión arterial
        this.Peso = "";              // peso del paciente
        this.Fc = 0;                 // frecuencia cardíaca
        this.Spo = 0;                // saturación de oxígeno
        this.Talla = 0;              // altura del paciente
        this.Bmi = 0;                // índice de masa corporal
        this.Edema = 0;              // edema de miembros inferiores (0 falso 1 true)
        this.Sp = 0;                 // (0 falso 1 true)
        this.MvDisminuido = 0;       // (0 falso 1 true)
        this.Crepitantes = 0;        // (0 falso 1 true)
        this.Sibilancias = 0;        // (0 falso 1 true)

        // datos de la tabla de síntomas
        this.IdSintoma = 0;          // clave del registro
        this.Disnea = "";            // tipo de disnea
        this.Palpitaciones = "";     // tipo de palpitaciones
        this.Precordial = "";        // tipo de dolor precordial
        this.Conciencia = 0;         // (0 falso 1 true)
        this.Presincope = 0;         // (0 falso 1 true)
        this.EdemaSintomas = 0;      // (0 falso 1 true)

        // datos de la tabla de cardiovascular
        this.IdCardiovascular = 0;   // clave del registro
        this.Ausnormal = 0;          // auscultación normal (0 falso 1 true)
        this.Irregular = 0;          // (0 falso 1 true)
        this.Tercer = 0;             // (0 falso 1 true)
        this.Cuarto = 0;             // (0 falso 1 true)
        this.Eyectivo = 0;           // (0 falso 1 true)
        this.Regurgitativo = 0;      // (0 falso 1 true)
        this.SinSistolico = 0;       // (0 falso 1 true)
        this.Aortico = 0;            // (0 falso 1 true)
        this.Diastolicomitral = 0;   // (0 falso 1 true)
        this.SinDiastolico = 0;      // (0 falso 1 true)
        this.Hepatomegalia = 0;      // (0 falso 1 true)
        this.Esplenomegalia = 0;     // (0 falso 1 true)
        this.Ingurgitacion = 0;      // (0 falso 1 true)

    }

    // métodos de asignación de valores

    // de la tabla de visitas
    setIdVisita(idvisita){
        this.IdVisita = idvisita;
    }
    setFechaVisita(fecha){
        this.FechaVisita = fecha;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setAlta(alta){
        this.Alta = alta;
    }

    // de la tabla de clasificación
    setIdClasificacion(idclasificacion){
        this.IdClasificacion = idclasificacion;
    }
    setPaciente(paciente){
        this.Paciente = paciente;
    }
    setEstadio(estadio){
        this.Estadio = estadio;
    }
    setObservaciones(observaciones){
        this.Observaciones = observaciones;
    }

    // de la tabla de examen físico
    setIdFisico(idfisico){
        this.IdFisico = idfisico;
    }
    setTa(ta){
        this.Ta = ta;
    }
    setPeso(peso){
        this.Peso = peso;
    }
    setFc(fc){
        this.Fc = fc;
    }
    setSpo(spo){
        this.Spo = spo;
    }
    setTalla(talla){
        this.Talla = talla;
    }
    setBmi(bmi){
        this.Bmi = bmi;
    }
    setEdema(edema){
        this.Edema = edema;
    }
    setSp(sp){
        this.Sp = sp;
    }
    setMvDisminuido(mvdisminuido){
        this.MvDisminuido = mvdisminuido;
    }
    setCrepitantes(crepitantes){
        this.Crepitantes = crepitantes;
    }
    setSibilancias(sibilancias){
        this.Sibilancias = sibilancias;
    }

    // de la tabla de síntomas
    setIdSintoma(idsintoma){
        this.IdSintoma = idsintoma;
    }
    setDisnea(disnea){
        this.Disnea = disnea;
    }
    setPalpitaciones(palpitaciones){
        this.Palpitaciones = palpitaciones;
    }
    setPrecordial(precordial){
        this.Precordial = precordial;
    }
    setConciencia(conciencia){
        this.Conciencia = conciencia;
    }
    setPresincope(presincope){
        this.Presincope = presincope;
    }
    setEdemaSintomas(edema){
        this.EdemaSintomas = edema;
    }

    // tabla de cardiovascular
    setIdCardiovascular(id){
        this.IdCardiovascular = id;
    }
    setAusnormal(ausnormal){
        this.Ausnormal = ausnormal;
    }
    setIrregular(irregular){
        this.Irregular = irregular;
    }
    setTercer(tercer){
        this.Tercer = tercer;
    }
    setCuarto(cuarto){
        this.Cuarto = cuarto;
    }
    setEyectivo(eyectivo){
        this.Eyectivo = eyectivo;
    }
    setRegurgitativo(regurgitativo){
        this.Regurgitativo = regurgitativo;
    }
    setSinSistolico(sinsistolico){
        this.SinSistolico = sinsistolico;
    }
    setAortico(aortico){
        this.Aortico = aortico;
    }
    setDiastolicomitral(diastolico){
        this.Diastolicomitral = diastolico;
    }
    setSinDiastolico(sindiastolico){
        this.SinDiastolico = sindiastolico;
    }
    setHepatomegalia(hepatomegalia){
        this.Hepatomegalia = hepatomegalia;
    }
    setEsplenomegalia(esplenomegalia){
        this.Esplenomegalia = esplenomegalia;
    }
    setIngurgitacion(ingurgitacion){
        this.Ingurgitacion = ingurgitacion;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente con el formulario
     * de visitas
     */
    verVisitas(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en la variable de clase el protocolo
        this.Paciente = document.getElementById("protocolo_paciente").value;

        // abrimos el layer
        this.layerVisitas = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Visitas del Paciente',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:1000,
                    height:630,
                    draggable: 'title',
                    ajax: {
                    url: 'visitas/form_visitas.html',
                    reload: 'strict'
                }
            });
        this.layerVisitas.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {idvisita} - clave del registro
     * Método llamado desde el formulario de antecedentes
     * que recibe como parámetro la clave del registro y
     * asigna el valor en la variable de clase y muestra
     * el formulario
     */
    editaVisita(idvisita){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos el valor en la variable de clase y
        // mostramos el formulario (luego al estar asignada
        // la variable automáticamente carga el registro)
        this.setIdVisita(idvisita);
        this.verVisitas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase obtiene
     * los datos del registro
     */
    getDatosVisita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "visitas/getvisita.php?idvisita=" + this.IdVisita,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                visitas.cargaDatosVisita(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} vector con los datos del registro
     * Método que recibe como parámetro el vector json con
     * los datos del registro y asigna los valores a las
     * variables de clase
     */
    cargaDatosVisita(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // la tabla de visitas
        this.setFechaVisita(datos.Fecha);
        this.setUsuario(datos.Usuario);
        this.setAlta(datos.Alta);

        // de la tabla de clasificación
        this.setIdClasificacion(datos.IdClasificacion);
        this.setPaciente(datos.Paciente);
        this.setEstadio(datos.Estadio);
        this.setObservaciones(datos.Observaciones);

        // de la tabla de examen físico
        this.setIdFisico(datos.IdFisico);
        this.setTa(datos.Ta);
        this.setPeso(datos.Peso);
        this.setFc(datos.Fc);
        this.setSpo(datos.Spo);
        this.setTalla(datos.Talla);
        this.setBmi(datos.Bmi);
        this.setEdema(datos.Edema);
        this.setSp(datos.Sp);
        this.setMvDisminuido(datos.MvDisminuido);
        this.setCrepitantes(datos.Crepitantes);
        this.setSibilancias(datos.Sibilancias);

        // de la tabla de síntomas
        this.setIdSintoma(datos.IdSintoma);
        this.setDisnea(datos.Disnea);
        this.setPalpitaciones(datos.Palpitaciones);
        this.setPrecordial(datos.Precordial);
        this.setConciencia(datos.Conciencia);
        this.setPresincope(datos.Presincope);
        this.setEdemaSintomas(datos.EdemaSintomas);

        // de la tabla de cardiovascular
        this.setIdCardiovascular(datos.IdCardiovascular);
        this.setAusnormal(datos.AusNormal);
        this.setIrregular(datos.Irregular);
        this.setTercer(datos.Tercer);
        this.setCuarto(datos.Cuarto);
        this.setEyectivo(datos.Eyectivo);
        this.setRegurgitativo(datos.Regurgitativo);
        this.setSinSistolico(datos.SinSistolico);
        this.setAortico(datos.Aortico);
        this.setDiastolicomitral(datos.Diastolicomitral);
        this.setSinDiastolico(datos.SinDiatolico);
        this.setHepatomegalia(datos.Hepatomegalia);
        this.setEsplenomegalia(datos.Esplenomegalia);
        this.setIngurgitacion(datos.Ingurgitacion);

        // mostramos el registro
        this.verDatosVisita();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase
     * carga el formulario
     */
    verDatosVisita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el estadio
        document.getElementById("estadio").value = this.Estadio;

        // la fecha de la visita
        document.getElementById("fecha_visita").value = this.FechaVisita;

        // los datos del examen físico
        document.getElementById("ta_visita").value = this.Ta;
        document.getElementById("peso_visita").value = this.Peso;
        document.getElementById("fc_visita").value = this.Fc;
        document.getElementById("saturacion_visita").value = this.Spo;
        document.getElementById("talla_visita").value = this.Talla;
        document.getElementById("indice_visita").value = this.Bmi;

        // según el valor de edema
        if (this.Edema == 0){
            document.getElementById("edemavisita").checked = false;
        } else {
            document.getElementById("edemavisita").checked = true;
        }

        // según el valor de sp
        if (this.Sp == 0){
            document.getElementById("spvisita").checked = false;
        } else {
            document.getElementById("spvisita").checked = true;
        }

        // según el valor de mvdisminuido
        if (this.MvDisminuido == 0){
            document.getElementById("mvdisminuido").checked = false;
        } else {
            document.getElementById("mvdisminuido").checked = true;
        }

        // según el valor de crepitantes
        if (this.Crepitantes == 0){
            document.getElementById("crepitantes").checked = false;
        } else {
            document.getElementById("crepitantes").checked = true;
        }

        // según el valor de sibilancias
        if (this.Sibilancias == 0){
            document.getElementById("sibilancias").checked = false;
        } else {
            document.getElementById("sibilancias").checked = true;
        }

        // de la tabla de síntomas

        // carga el tipo de disnea
        document.getElementById("disnea_visitas").value = this.Disnea;

        // el tipo de palpitaciones
        document.getElementById("palpitaciones").value = this.Palpitaciones;

        // el tipo de dolor precordial
        document.getElementById("dolorprecordial").value = this.Precordial;

        // carga los checkbox
        if (this.Conciencia == 0){
            document.getElementById("pdeconciencia").checked = false;
        } else {
            document.getElementById("pdeconciencia").checked = true;
        }

        // si tuvo presíncope
        if (this.Presincope == 0){
            document.getElementById("presincope").checked = false;
        } else {
            document.getElementById("presincope").checked = true;
        }

        // edema de miembros inferiores
        if (this.EdemaSintomas == 0){
            document.getElementById("edemainferiores").checked = false;
        } else {
            document.getElementById("edemainferiores").checked = true;
        }

        // de la tabla de aparato cardiovascular

        // la auscultación
        if (this.Ausnormal == 0){
            document.getElementById("auscultacion").checked = false;
        } else {
            document.getElementById("auscultacion").checked = true;
        }

        // si es irregular
        if (this.Irregular == 0){
            document.getElementById("irregular").checked = false;
        } else {
            document.getElementById("irregular").checked = true;
        }

        // si tiene tercer r
        if (this.Tercer == 0){
            document.getElementById("tercer").checked = false;
        } else {
            document.getElementById("tercer").checked = true;
        }

        // si tiene cuarto
        if (this.Cuarto == 0){
            document.getElementById("cuarto").checked = false;
        } else {
            document.getElementById("cuarto").checked = true;
        }

        // si tiene soplos sistólicos
        if (this.Eyectivo == 0){
            document.getElementById("eyectivo").checked = false;
        } else {
            document.getElementById("eyectivo").checked = true;
        }

        // si tiene soplo regurgitativo
        if (this.Regurgitativo == 0){
            document.getElementById("regurgitativo").checked = false;
        } else {
            document.getElementById("regurgitativo").checked = true;
        }

        // si no tiene
        if (this.SinSistolico == 0){
            document.getElementById("notiene").checked = false;
        } else {
            document.getElementById("notiene").checked = true;
        }

        // si tiene soplo aórtico
        if (this.Aortico == 0){
            document.getElementById("aortico").checked = false;
        } else {
            document.getElementById("aortico").checked = true;
        }

        // si tiene soplo mitral
        if (this.Diastolicomitral == 0){
            document.getElementById("mitral").checked = false;
        } else {
            document.getElementById("mitral").checked = true;
        }

        // si no tiene soplos diastólicos
        if (this.SinDiastolico == 0){
            document.getElementById("sindiastolico").checked = false;
        } else {
            document.getElementById("sindiastolico").checked = true;
        }

        // si tiene hepatomegalia
        if (this.Hepatomegalia == 0){
            document.getElementById("hepatomegalia").checked = false;
        } else {
            document.getElementById("hepatomegalia").checked = true;
        }

        // si tiene esplenomegalia
        if (this.Esplenomegalia == 0){
            document.getElementById("esplenomegalia").checked = false;
        } else {
            document.getElementById("esplenomegalia").checked = true;
        }

        // si tiene ingurgitación
        if (this.Ingurgitacion == 0){
            document.getElementById("ingurgitacion").checked = false;
        } else {
            document.getElementById("ingurgitacion").checked = true;
        }

        // fijamos las observaciones
        CKEDITOR.instances['observaciones_visita'].setData(this.Observaciones);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idvisita del registro
     * Método que recibe como parámetro la clave de una visita
     * y luego de pedir confirmación elimina el registro y
     * todos los hijos
     */
    borraVisita(idvisita){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Visita',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('visitas/borravisita.php?id='+idvisita,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // recargamos la grilla
                            antecedentes.cargaVisitas();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que verifica
     * los datos del formulario
     */
    validaVisita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje = "";
        var correcto = false;

        // si no seleccionó el estadio
        if (document.getElementById("estadio").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el estadio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("estadio").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Estadio = document.getElementById("estadio").value;

        }

        // si no indicó la fecha de la visita
        if (document.getElementById("fecha_visita").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de la visita";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_visita").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.FechaVisita = document.getElementById("fecha_visita").value;

        }

        // si no indicó la tensión arterial
        if (document.getElementById("ta_visita").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la tensión arterial en mm de hg";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ta_visita").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Ta = document.getElementById("ta_visita").value;

        }

        // si no indicó el peso
        if (document.getElementById("peso_visita").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el peso en kilogramos";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("peso_visita").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Peso = document.getElementById("peso_visita").value;

        }

        // si no indicó la frecuencia cardíaca
        if (document.getElementById("fc_visita").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la frecuencia cardíaca";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fc_visita").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Fc = document.getElementById("fc_visita").value;

        }

        // la saturación tiene valor por defecto
        this.Spo = document.getElementById("saturacion_visita").value;

        // si no indicó la talla
        if (document.getElementById("talla_visita").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la altura del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("talla_visita").focus();
            return false;

        // si indicó
        } else {

            // asigna en la clase
            this.Talla = document.getElementById("talla_visita").value;

        }

        // verifica que halla calculado el índice de masa corporal
        if (document.getElementById("indice_visita").value == ""){

            // calcula el índice
            this.calculaIMC();

        // si asignó
        } else {

            // asigna en la clase
            this.Bmi = document.getElementById("indice_visita").value;

        }

        // los check de visita los permite todos en blanco así
        // que asigna en las variables de clase
        if (document.getElementById("edemavisita").checked){
            this.Edema = 1;
        } else {
            this.Edema = 0;
        }
        if (document.getElementById("spvisita").checked){
            this.Sp = 1;
        } else {
            this.Sp = 0;
        }
        if (document.getElementById("mvdisminuido").checked){
            this.MvDisminuido = 1;
        } else {
            this.MvDisminuido = 0;
        }
        if (document.getElementById("crepitantes").checked){
            this.Crepitantes = 1;
        } else {
            this.Crepitantes = 0;
        }
        if (document.getElementById("sibilancias").checked){
            this.Sibilancias = 1;
        } else {
            this.Sibilancias = 0;
        }

        // si no seleccionó el tipo de disnea
        if (document.getElementById("disnea_visitas").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el tipo de disnea";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("disnea_visitas").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Disnea = document.getElementById("disnea_visitas").value;

        }

        // si no seleccionó el tipo de palpitaciones
        if (document.getElementById("palpitaciones").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el tipo de palpitaciones";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("palpitaciones").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Palpitaciones = document.getElementById("palpitaciones").value;

        }

        // si no seleccionó el tipo de dolor precordial
        if (document.getElementById("dolorprecordial").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el tipo de dolor precordial";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("dolorprecordial").focus();
            return false;

        // si asignó
        } else {

            // asigna en la clase
            this.Precordial = document.getElementById("dolorprecordial").value;

        }

        // los check de síntomas los permite en blanco así
        // que asignamos directamente a las variables
        if (document.getElementById("pdeconciencia").checked){
            this.Conciencia = 1;
        } else {
            this.Conciencia = 0;
        }
        if (document.getElementById("presincope").checked){
            this.Presincope = 1;
        } else {
            this.Presincope = 0;
        }
        if (document.getElementById("edemainferiores").checked){
            this.EdemaSintomas = 1;
        } else {
            this.EdemaSintomas = 0;
        }

        // los check de aparato cardiovascular los permite en
        // blanco así que asigna en las variables de clase
        if (document.getElementById("auscultacion").checked){
            this.Ausnormal = 1;
        } else {
            this.Ausnormal = 0;
        }
        if (document.getElementById("irregular").checked){
            this.Irregular = 1;
        } else {
            this.Irregular = 0;
        }
        if (document.getElementById("tercer").checked){
            this.Tercer = 1;
        } else {
            this.Tercer = 0;
        }
        if (document.getElementById("cuarto").checked){
            this.Cuarto = 1;
        } else {
            this.Cuarto = 0;
        }

        // verifica que halla marcado al menos un elemento
        // de los soplos sistólicos
        correcto = false;

        // si tiene soplos eyectivos
        if (document.getElementById("eyectivo").checked){
            correcto = true;
            this.Eyectivo = 1;
        } else {
            this.Eyectivo = 0;
        }

        // si tiene soplos regurgitativos
        if (document.getElementById("regurgitativo").checked){
            correcto = true;
            this.Regurgitativo = 1;
        } else {
            this.Regurgitativo = 0;
        }

        // si no tiene
        if (document.getElementById("notiene").checked){
            correcto = true;
            this.SinSistolico = 1;
        } else {
            this.SinSistolico = 0;
        }

        // si no marcó ninguno
        if (!correcto){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el tipo de Soplo Sistólico<br>";
            mensaje += "(puede marcar No Tiene).";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica que halla marcado al menos un elemento
        // de los soplos diastólicos
        correcto = false;

        // si tiene soplo aórtico
        if (document.getElementById("aortico").checked){
            correcto = true;
            this.Aortico = 1;
        } else {
            this.Aortico = 0;
        }

        // si tiene diastólico mitral
        if (document.getElementById("mitral").checked){
            correcto = true;
            this.Diastolicomitral = 1;
        } else {
            this.Diastolicomitral = 0;
        }

        // si no tiene
        if (document.getElementById("sindiastolico").checked){
            correcto = true;
            this.SinDiastolico = 1;
        } else {
            this.SinDiastolico = 0;
        }

        // si no marcó ninguno
        if (!correcto){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el tipo de Soplo Diastólico<br>";
            mensaje += "(puede marcar No Tiene).";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // los check restantes los permite en blanco así
        // que asignamos directamente en las variables
        if (document.getElementById("hepatomegalia").checked){
            this.Hepatomegalia = 1;
        } else {
            this.Hepatomegalia = 0;
        }
        if (document.getElementById("esplenomegalia").checked){
            this.Esplenomegalia = 1;
        } else {
            this.Esplenomegalia = 0;
        }
        if (document.getElementById("ingurgitacion").checked){
            this.Ingurgitacion = 1;
        } else {
            this.Ingurgitacion = 0;
        }

        // asigna las observaciones
        this.Observaciones = CKEDITOR.instances['observaciones_visita'].getData();

        // graba el registro
        this.grabaVisita();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al perder el foco la talla que calcula
     * el índice de masa corporal
     */
    calculaIMC(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaramos las variables
        var peso = document.getElementById("peso_visita").value;
        var altura = document.getElementById("talla_visita").value;

        // si el peso o la altura están en cero
        if (peso == "" || altura == ""){

            // presenta el mensaje y retorna
            var mensaje = "No puedo calcular el IMC";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // lo calculamos
        altura = Math.pow(altura, 2);
        var imc = peso / altura;

        // lo presenta con dos decimales
        document.getElementById("indice_visita").value = imc.toFixed(2);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * ejecuta la consulta en el servidor
     */
    grabaVisita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosVisita = new FormData();

        // asignamos los valores
        datosVisita.append("IdVisita", this.IdVisita);
        datosVisita.append("Fecha", this.FechaVisita);

        // las variables de la tabla de clasificación
        datosVisita.append("IdClasificacion", this.IdClasificacion);
        datosVisita.append("Paciente", this.Paciente);
        datosVisita.append("Estadio", this.Estadio);
        datosVisita.append("Observaciones", this.Observaciones);

        // la tabla de examen físico
        datosVisita.append("IdFisico", this.IdFisico);
        datosVisita.append("Ta", this.Ta);
        datosVisita.append("Peso", this.Peso);
        datosVisita.append("Fc", this.Fc);
        datosVisita.append("Spo", this.Spo);
        datosVisita.append("Talla", this.Talla);
        datosVisita.append("Bmi", this.Bmi);
        datosVisita.append("Edema", this.Edema);
        datosVisita.append("Sp", this.Sp);
        datosVisita.append("MvDisminuido", this.MvDisminuido);
        datosVisita.append("Crepitantes", this.Crepitantes);
        datosVisita.append("Sibilancias", this.Sibilancias);

        // datos de la tabla de síntomas
        datosVisita.append("IdSintoma", this.IdSintoma);
        datosVisita.append("Disnea", this.Disnea);
        datosVisita.append("Palpitaciones", this.Palpitaciones);
        datosVisita.append("Precordial", this.Precordial);
        datosVisita.append("Conciencia", this.Conciencia);
        datosVisita.append("Presincope", this.Presincope);
        datosVisita.append("EdemaSintomas", this.EdemaSintomas);

        // datos de la tabla de cardiovascular
        datosVisita.append("IdCardiovascular", this.IdCardiovascular);
        datosVisita.append("Auscultacion", this.Ausnormal);
        datosVisita.append("Irregular", this.Irregular);
        datosVisita.append("Tercer", this.Tercer);
        datosVisita.append("Cuarto", this.Cuarto);
        datosVisita.append("Eyectivo", this.Eyectivo);
        datosVisita.append("Regurgitativo", this.Regurgitativo);
        datosVisita.append("SinSistolico", this.SinSistolico);
        datosVisita.append("Aortico", this.Aortico);
        datosVisita.append("Diastolicomitral", this.Diastolicomitral);
        datosVisita.append("SinDiastolico", this.SinDiastolico);
        datosVisita.append("Hepatomegalia", this.Hepatomegalia);
        datosVisita.append("Esplenomegalia", this.Esplenomegalia);
        datosVisita.append("Ingurgitacion", this.Ingurgitacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "visitas/graba_visita.php",
            type: "POST",
            data: datosVisita,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    // valores de las claves
                    visitas.setIdVisita(data.IdVisita);
                    visitas.setIdClasificacion(data.IdClasificacion);
                    visitas.setIdFisico(data.IdFisico);
                    visitas.setIdSintoma(data.IdSintoma);
                    visitas.setIdCardiovascular(data.IdCardiovascular);

                    // recargamos la grilla
                    antecedentes.listaVisitas();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * limpia el formulario o recarga el registro según
     * corresponda
     */
    cancelaVisita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está insertando
        if (this.IdVisita == 0){

            // limpia el formulario
            this.limpiaVisita();

        // si está editando
        } else {

            // recarga el registro
            this.getDatosVisita();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de visitas
     */
    limpiaVisita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("estadio").value = "";
        document.getElementById("fecha_visita").value = "";
        document.getElementById("ta_visita").value = "";
        document.getElementById("peso_visita").value = "";
        document.getElementById("fc_visita").value = "";
        document.getElementById("saturacion_visita").value = "";
        document.getElementById("talla_visita").value = "";
        document.getElementById("indice_visita").value = "";
        document.getElementById("edemavisita").checked = false;
        document.getElementById("spvisita").checked = false;
        document.getElementById("mvdisminuido").checked = false;
        document.getElementById("crepitantes").checked = false;
        document.getElementById("sibilancias").checked = false;
        document.getElementById("disneavisitas").value = "";
        document.getElementById("palpitaciones").value = "";
        document.getElementById("dolorprecordial").value = "";
        document.getElementById("pdeconciencia").checked = false;
        document.getElementById("presincope").checked = false;
        document.getElementById("edemainferiores").checked = false;
        document.getElementById("auscultacion").checked = false;
        document.getElementById("irregular").checked = false;
        document.getElementById("tercer").checked = false;
        document.getElementById("cuarto").checked = false;
        document.getElementById("eyectivo").checked = false;
        document.getElementById("regurgitativo").checked = false;
        document.getElementById("notiene").checked = false;
        document.getElementById("aortico").checked = false;
        document.getElementById("mitral").checked = false;
        document.getElementById("sindiastolico").checked = false;
        document.getElementById("hepatomegalia").checked = false;
        document.getElementById("esplenomegalia").checked = false;
        document.getElementById("ingurgitacion").checked = false;
        CKEDITOR.instances['observaciones_visita'].setData();

    }

}