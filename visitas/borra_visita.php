<?php

/**
 *
 * borravisita | visitas/borra_visita.php
 *
 * @package     Diagnostico
 * @subpackage  Visitas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe la clave de una visita por get y ejecuta la
 * consulta de eliminación de la visita y de todas las tablas
 * hijas
*/

// incluimos e instanciamos las librerìas
require_once("visitas.class.php");
$visitas = new Visitas();

// eliminamos los registros
$visitas->borraVisita($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>