<?php

/**
 *
 * graba_visita | visitas/graba_visita.php
 *
 * @package     Diagnostico
 * @subpackage  Visitas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta en el servidor, retorna las claves de las tablas
 * asociadas
*/

// incluimos las librerìas
require_once("visitas.class.php");
require_once("../clasificacion/clasificacion.class.php");
require_once("../fisico/fisico.class.php");
require_once("../sintomas/sintomas.class.php");
require_once("../cardiovascular/cardiovascular.class.php");

// instanciamos las clases
$visitas = new Visitas();
$clasificacion = new Clasificacion();
$fisico = new Fisico();
$sintomas = new Sintomas();
$cardiovascular = new Cardiovascular();

// asignamos los valores en la tabla de visitas
$visitas->setId($_POST["IdVisita"]);
$visitas->setFecha($_POST["Fecha"]);
$visitas->setPaciente($_POST["Paciente"]);

// primero grabamos el registro de visitas y
// nos aseguramos de tener la clave
$idvisita = $visitas->grabaVisita();

// asignamos en el resto de las tablas con la
// clave de la visita

// la clase de clasificacion
$clasificacion->setId($_POST["IdClasificacion"]);
$clasificacion->setPaciente($_POST["Paciente"]);
$clasificacion->setVisita($idvisita);
$clasificacion->setEstadio($_POST["Estadio"]);
$clasificacion->setObservaciones(($_POST["Observaciones"]));

// la clase de examen físico
$fisico->setId($_POST["IdFisico"]);
$fisico->setProtocolo($_POST["Paciente"]);
$fisico->setVisita($idvisita);
$fisico->setTa($_POST["Ta"]);
$fisico->setPeso($_POST["Peso"]);
$fisico->setFc($_POST["Fc"]);
$fisico->setSpo($_POST["Spo"]);
$fisico->setTalla($_POST["Talla"]);
$fisico->setBmi($_POST["Bmi"]);
$fisico->setEdema($_POST["Edema"]);
$fisico->setSp($_POST["Sp"]);
$fisico->setMvdisminuido($_POST["MvDisminuido"]);
$fisico->setCrepitantes($_POST["Crepitantes"]);
$fisico->setSibilancias($_POST["Sibilancias"]);

// la clase de síntomas
$sintomas->setId($_POST["IdSintoma"]);
$sintomas->setVisita($idvisita);
$sintomas->setPaciente($_POST["Paciente"]);
$sintomas->setDisnea(($_POST["Disnea"]));
$sintomas->setPalpitaciones($_POST["Palpitaciones"]);
$sintomas->setPrecordial($_POST["Precordial"]);
$sintomas->setConciencia($_POST["Conciencia"]);
$sintomas->setPresincope($_POST["Presincope"]);
$sintomas->setEdema($_POST["EdemaSintomas"]);

// de la clase de cardiovascular
$cardiovascular->setId($_POST["IdCardiovascular"]);
$cardiovascular->setVisita($idvisita);
$cardiovascular->setPaciente($_POST["Paciente"]);
$cardiovascular->setAuscultacion($_POST["Auscultacion"]);
$cardiovascular->setIrregular($_POST["Irregular"]);
$cardiovascular->setTercer($_POST["Tercer"]);
$cardiovascular->setCuarto($_POST["Cuarto"]);
$cardiovascular->setEyectivo($_POST["Eyectivo"]);
$cardiovascular->setRegurgitativo($_POST["Regurgitativo"]);
$cardiovascular->setSinSistolico($_POST["SinSistolico"]);
$cardiovascular->setAortico($_POST["Aortico"]);
$cardiovascular->setDiastolicoMitral($_POST["Diastolicomitral"]);
$cardiovascular->setSinDiastolico($_POST["SinDiastolico"]);
$cardiovascular->setHepatomegalia($_POST["Hepatomegalia"]);
$cardiovascular->setEsplenomegalia($_POST["Esplenomegalia"]);
$cardiovascular->setIngurtitacion($_POST["Ingurgitacion"]);

// grabamos los registros
$idclasificacion = $clasificacion ->grabaClasificacion();
$idfisico = $fisico->grabaVisita();
$idsintoma = $sintomas->grabaSintoma();
$idcardiovascular = $cardiovascular->grabaCardiovascular();


// retornamos las claves
echo json_encode(array("Error" =>            1,
                       "IdVisita" =>         $idvisita,
                       "IdClasificacion" =>  $idclasificacion,
                       "IdFisico" =>         $idfisico,
                       "IdSintoma" =>        $idsintoma,
                       "IdCardiovascular" => $idcardiovascular))

?>
