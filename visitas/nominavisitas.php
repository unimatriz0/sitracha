<?php

/**
 *
 * nominavisitas | visitas/nominavisitas.php
 *
 * @package     Diagnostico
 * @subpackage  Visitas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un protocolo y retorna 
 * el vector json con la nómina de visitas
 * 
*/

// incluimos e instanciamos la clase
require_once("visitas.class.php");
$visitas = new Visitas();

// retornamos el array json
echo json_encode($visitas->nominaVisitas($_GET["protocolo"]));

?>