<?php

/**
 *
 * getvisita | visitas/getvisita.php
 *
 * @package     Diagnostico
 * @subpackage  Visitas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe la clave de una visita por get y obtiene los
 * datos del registro y de sus tablas hijas
*/

// incluimos las librerìas
require_once("visitas.class.php");
require_once("../clasificacion/clasificacion.class.php");
require_once("../fisico/fisico.class.php");
require_once("../sintomas/sintomas.class.php");
require_once("../cardiovascular/cardiovascular.class.php");

// instanciamos las clases
$visitas = new Visitas();
$clasificacion = new Clasificacion();
$fisico = new Fisico();
$sintomas = new Sintomas();
$cardiovascular = new Cardiovascular();

// obtenemos la clave
$id = $_GET["idvisita"];

// obtenemos los registros
$visitas->getDatosVisita($id);
$clasificacion->getDatosClasificacion($id);
$fisico->getDatosVisita($id);
$sintomas->getDatosSintoma($id);
$cardiovascular->getDatosCardiovascular($id);

// retornamos el array
echo json_encode(array("Fecha" =>            $visitas->getFecha(),
                       "Usuario" =>          $visitas->getUsuario(),
                       "Alta" =>             $visitas->getFechaAlta(),
                       "IdClasificacion" =>  $clasificacion->getId(),
                       "Paciente" =>         $clasificacion->getPaciente(),
                       "Estadio" =>          $clasificacion->getEstadio(),
                       "Observaciones" =>    $clasificacion->getObservaciones(),
                       "IdFisico" =>         $fisico->getId(),
                       "Ta" =>               $fisico->getTa(),
                       "Peso" =>             $fisico->getPeso(),
                       "Fc" =>               $fisico->getFc(),
                       "Spo" =>              $fisico->getSpo(),
                       "Talla" =>            $fisico->getTalla(),
                       "Bmi" =>              $fisico->getBmi(),
                       "Edema" =>            $fisico->getEdema(),
                       "Sp" =>               $fisico->getSp(),
                       "MvDisminuido" =>     $fisico->getMvdisminuido(),
                       "Crepitantes" =>      $fisico->getCrepitantes(),
                       "Sibilancias" =>      $fisico->getSibilancias(),
                       "IdSintoma" =>        $sintomas->getId(),
                       "Disnea" =>           $sintomas->getDisnea(),
                       "Palpitaciones" =>    $sintomas->getPalpitaciones(),
                       "Precordial" =>       $sintomas->getPrecordial(),
                       "Conciencia" =>       $sintomas->getConciencia(),
                       "Presincope" =>       $sintomas->getyPresincope(),
                       "EdemaSintomas" =>    $sintomas->getEdema(),
                       "IdCardiovascular" => $cardiovascular->getId(),
                       "AusNormal" =>        $cardiovascular->getAuscultacion(),
                       "Irregular" =>        $cardiovascular->getIrregular(),
                       "Tercer" =>           $cardiovascular->getTercer(),
                       "Cuarto" =>           $cardiovascular->getCuarto(),
                       "Eyectivo" =>         $cardiovascular->getEyectivo(),
                       "Regurgitativo" =>    $cardiovascular->getRegurgitativo(),
                       "SinSistolico" =>     $cardiovascular->getSinSistolico(),
                       "Aortico" =>          $cardiovascular->getAortico(),
                       "Diastolicomitral" => $cardiovascular->getDiastolicoMitral(),
                       "SinDiastolico" =>    $cardiovascular->getSinDiastolico(),
                       "Hepatomegalia" =>    $cardiovascular->getHepatomegalia(),
                       "Esplenomegalia" =>   $cardiovascular->getEsplenomegalia(),
                       "Ingurgitacion" =>    $cardiovascular->getIngurgitacion()))

?>