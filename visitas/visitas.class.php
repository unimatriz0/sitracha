<?php

/**
 *
 * Class Visitas | visitas/visitas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Visitas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * visitas del paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Visitas{

    // definimos las variables
    private $Link;               // puntero a la base de datos
    private $IdUsuario;          // clave del usuario
    private $Id;                 // clave del registro
    private $Usuario;            // nombre del usuario
    private $Fecha;              // fecha de la visita
    private $Paciente;           // protocolo del paciente
    private $FechaAlta;          // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // instanciamos la conexión
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Id = 0;
        $this->Usuario = "";
        $this->Fecha = "";
        $this->Paciente = 0;

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - clave del paciente
     * @return vector con los registros
     * Método que recibe como parámetro el protocolo de un
     * paciente y retorna el vector con las visitas del mismo
     */
    public function nominaVisitas($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_visitas.id AS id,
                            diagnostico.v_visitas.fecha AS fecha,
                            diagnostico.v_visitas.usuario AS usuario,
                            diagnostico.v_visitas.estadio AS estadio,
                            diagnostico.v_visitas.paciente AS paciente,
                            diagnostico.v_visitas.peso AS peso,
                            diagnostico.v_visitas.auscultacion AS auscultacion,
                            diagnostico.v_visitas.soplos AS soplos
                     FROM diagnostico.v_visitas
                     WHERE diagnostico.v_visitas.paciente = '$protocolo'; ";

        // ejecutamos la consulta y retornmos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita - clave del registro
     * Método que recibe como parámetro la clave de la visita
     * y ejecuta la consulta de eliminación en todas las
     * tablas asociadas
     */
    public function borraVisita($idvisita){

        // componemos la consulta para eliminar en cascada
        $consulta = "DELETE diagnostico.visitas,
                            diagnostico.ecocardiograma,
                            diagnostico.electrocardiograma,
                            diagnostico.ergometria,
                            diagnostico.fisico,
                            diagnostico.holter,
                            diagnostico.rx,
                            diagnostico.cardiovascular,
                            diagnostico.clasificacion,
                            diagnostico.sintomas
                     FROM diagnostico.visitas JOIN diagnostico.ecocardiograma ON diagnostico.visitas.id = diagnostico.ecocardiograma.visita
                                              JOIN diagnostico.electrocardiograma ON diagnostico.visitas.id = diagnostico.electrocardiograma.visita
                                              JOIN diagnostico.ergometria ON diagnostico.visitas.id = diagnostico.ergometria.visita
                                              JOIN diagnostico.fisico ON diagnostico.visitas.id = diagnostico.fisico.visita
                                              JOIN diagnostico.holter ON diagnostico.visitas.id = diagnostico.holter.visita
                                              JOIN diagnostico.rx ON diagnostico.visitas.id = diagnostico.rx.visita
                                              JOIN diagnostico.cardiovascular ON diagnostico.visitas.id = diagnostico.cardiovascular.visita
                                              JOIN diagnostico.clasificacion ON diagnostico.visitas.id = diagnostico.clasificacion.visita
                                              JOIN diagnostico.sintomas ON diagnostico.visitas.id = diagnostico.sintomas.visita
                     WHERE diagnostico.visitas.id = '$idvisita'; ";

        // ejecutamos la consulta
        $this->Link->exec($consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave entero con la clave del registro
     * Método que recibe como parámetro la clave del
     * registro y asigna en las variables de clase los
     * valores del mismo
     */
    public function getDatosVisita($clave){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_visitas.id AS id,
                            diagnostico.v_visitas.fecha AS fecha,
                            diagnostico.v_visitas.usuario AS usuario,
                            diagnostico.v_visitas.paciente AS paciente,
                            diagnostico.v_visitas.fecha_alta AS fecha_alta
                     FROM diagnostico.v_visitas
                     WHERE diagnostico.v_visitas.id = '$clave';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y asignamos
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Fecha = $fecha;
        $this->Usuario = $usuario;
        $this->Paciente = $paciente;
        $this->FechaAlta = $fecha_alta;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id - clave del registro afectado
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda
     */
    public function grabaVisita(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaVisita();
        } else {
            $this->editaVisita();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    private function nuevaVisita(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.visitas
                            (fecha,
                             usuario)
                            VALUES
                            (STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":fecha",    $this->Fecha);
        $psInsertar->bindParam(":usuario",  $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    private function editaVisita(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.visitas SET
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            usuario = :usuario
                     WHERE diagnostico.visitas.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":fecha",    $this->Fecha);
        $psInsertar->bindParam(":usuario",  $this->IdUsuario);
        $psInsertar->bindParam(":id",       $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}