<?php

/**
 *
 * Class Celulares | celulares/celulares.class.php
 *
 * @package     Diagnostico
 * @subpackage  Celulares
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de 
 * compañìas de celular
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Celulares{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Link;                  // puntero a la base de datos
    protected $IdCompania;            // clave del registro
    protected $Compania;              // nombre de la companía
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Usuario = "";
        $this->IdCompania = 0;
        $this->Compania = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdCompania($idcompania){
        $this->IdCompania = $idcompania;
    }
    public function setCompania($compania){
        $this->Compania = $compania;
    }

    // métodos de retorno de valores
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getIdCompania(){
        return $this->IdCompania;
    }
    public function getCompania(){
        return $this->Compania;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que retorna el array con la nómina de compañias de celular
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaCelulares(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.celulares.id AS id_compania,
                            diccionarios.celulares.compania AS compania,
                            DATE_FORMAT(diccionarios.celulares.fecha_alta, '%d/%m/%Y') AS fecha_alta,
                            diccionarios.celulares.id_usuario AS id_usuario,
                            cce.responsables.usuario AS usuario
                     FROM diccionarios.celulares INNER JOIN cce.responsables ON diccionarios.celulares.id_usuario = cce.responsables.id
                     ORDER BY diccionarios.celulares.compania;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que ejecuta la consulta de edición o inserción segun
     * el caso, retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idcompania - clave del registro insertado / editado
     */
    public function grabaCelular(){

        // si está insertando
        if ($this->IdCompania == 0){
            $this->nuevaCompania();
        } else {
            $this->editaCompania();
        }

        // retornamos la clave
        return $this->IdCompania;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaCompania(){

        // componemos la consulta
        $consulta = "INSERT INTO diccionarios.celulares
                            (compania,
                             id_usuario)
                            VALUES
                            (:compania,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":compania", $this->Compania);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->IdCompania = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de actualización
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaCompania(){

        // componemos la consulta
        $consulta = "UPDATE diccionarios.celulares SET
                            compania = :compania,
                            id_usuario = :id_usuario
                     WHERE diccionarios.celulares.id = :id_compania;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":compania", $this->Compania);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_compania", $this->IdCompania);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}