/*
 * Nombre: celulares.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 01/03/2018
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de compañias
 *              de celular
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/** 
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de prestadores de celular
 */
class Celulares {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initCelulares();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initCelulares(){

        // inicializa las variables
        this.IdCompania = 0;
        this.Compania = "";
        this.IdUsuario = 0;
        this.Usuario = "";
        this.FechaAlta = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de celulares
     */
    muestraCelulares(){

        // carga el formulario
        $("#form_administracion").load("celulares/form_celulares.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idcompania - clave de la compañía
     * Método que verifica los datos del formulario antes de grabar
     * si no recibe la id de la compañia asume que es un alta
     */
    verificaCelular(idcompania){

        // declaración de variables
        var mensaje;

        // si está dando un alta
        if (typeof(idcompania) == "undefined"){
            idcompania = "nuevo";
        } else {
            this.IdCompania = idcompania;
        }

        // verifica que halla ingresado el nombre
        if (document.getElementById("compania_" + idcompania).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la compania";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("compania_" + idcompania).focus();
            return false;

        }

        // asigna en la variable de clase y graba
        this.Compania = document.getElementById("compania_" + idcompania).value;
        this.grabaCelular();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos al servidor para actualizar la base
     */
    grabaCelular(){

        // declaración de variables
        var datosCelular = new FormData();

        // si está editando
        if (this.IdCompania != 0){
            datosCelular.append("Id", this.IdCompania);

        }

        // agregamos el nombre
        datosCelular.append("Compania", this.Compania);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "celulares/graba_celular.php",
            type: "POST",
            data: datosCelular,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    celulares.initCelulares();

                    // recarga el formulario para reflejar los cambios
                    celulares.muestraCelulares();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un elemento html
     * @param {int} idcompania - clave del registro preseleccionado
     * Método que recibe como parámetro la id de un elemento del
     * formulario y carga en ese elemento la nómina de compañias de
     * celular
     */
    nominaCelulares(idelemento, idcompania){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "celulares/lista_celulares.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la companía
                    if (typeof(idcompania) != "undefined"){

                        // si coincide
                        if (data[i].Id == idcompania){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Compania + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Compania + "</option>");

                        }

                    // si no recibió companía
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Compania + "</option>");
                    }

                }

        }});

    }

}