<?php

/**
 *
 * celulares/graba_celular.php
 *
 * @package     Diagnostico
 * @subpackage  Celulares
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (03/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de un registro y 
 * ejecuta la consulta en la base de datos, retorna 
 * la id del registro afectado o cero en caso de error
*/

// incluimos e instanciamos la clase
require_once("celulares.class.php");
$celulares = new Celulares();

// si recibió la id
if (!empty($_POST["Id"])){
    $celulares->setIdCompania($_POST["Id"]);
}

// asigna el nombre
$celulares->setCompania($_POST["Compania"]);

// grabamos el registro
$resultado = $celulares->grabaCelular();

// retornamos el resultado
echo json_encode(array("Error" => $resultado));
?>