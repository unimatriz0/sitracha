<?php

/**
 *
 * celulares/form_celulares.php
 *
 * @package     Diagnostico
 * @subpackage  Celulares
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario para la edición de compañías 
 * de celular
 * 
*/

// incluimos e instanciamos la clase
require_once("celulares.class.php");
$celulares = new Celulares();

// presentamos el título
echo "<h2>Nómina de Compañias de Celular</h2>";

// definimos la tabla
echo "<table width='60%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Compañía</th>";
echo "<th align='left'>Usuario</th>";
echo "<th align='left'>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la matriz de documentos
$nomina = $celulares->nominaCelulares();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el nombre de la compañia
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre de la companía'>";
    echo "<input type='text'
                 name='compania_$id_compania'
                 id='compania_$id_compania'
                 value='$compania'
                 size='30'>";
    echo "</span>";
    echo "</td>";

    // presenta el usuario y la fecha de alta
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // arma el enlace
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaCompania'
           id='btnGrabaCompania'
           title='Pulse para grabar el registro'
           onClick='celulares.verificaCelular($id_compania)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// ahora agregamos la fila de altas
echo "<tr>";

// presentamos el tipo de documento
echo "<td>";
echo "<span class='tooltip'
            title='Nombre de la compañia'>";
echo "<input type='text'
                name='compania_nuevo'
                id='compania_nuevo'
                size='30'>";
echo "</span>";
echo "</td>";

// obtenemos el usuario actual y la fecha de alta
$fecha_alta = date('d/m/Y');

// arma el enlace
echo "<td align='center'>";
echo "<input type='button'
        name='btnGrabaCompania'
        id='btnGrabaCompania'
        title='Ingresa un nuevo registro'
        onClick='celulares.verificaCelular()'
        class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>