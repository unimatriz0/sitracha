<?php

/**
 *
 * celulares/lista_celulares.class.php
 *
 * @package     Diagnostico
 * @subpackage  Celulares
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de 
 * compañías de celular
*/

// incluimos e instanciamos la clase
require_once("celulares.class.php");
$celulares = new Celulares();

// obtenemos la nómina
$nomina = $celulares->nominaCelulares();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_compania,
                        "Compania" => $compania);

}

// retornamos el vector
echo json_encode($jsondata);

?>