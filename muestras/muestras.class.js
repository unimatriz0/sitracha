/*

    Nombre: muestras.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 12/06/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de muestras

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de toma de muestras
 */
class Muestras {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initMuestras();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initMuestras(){

        // inicializamos
        this.IdMuestra = 0;
        this.Protocolo = 0;
        this.IdLaboratorio = 0;
        this.Comentarios = "";
        this.IdGenero = "";
        this.FechaAlta = "";
        this.IdTomo = "";
        this.FechaToma = "";

    }

    // métodos de asignación de variables
    setIdMuestra(idmuestra){
        this.IdMuestra = idmuestra;
    }
    setProtocolo(protocolo){
        this.Protocolo = protocolo;
    }
    setIdLaboratorio(idlaboratorio){
        this.IdLaboratorio = 0;
    }
    setComentarios(comentarios){
        this.Comentarios = comentarios;
    }
    setIdGenero(idgenero){
        this.IdGenero = idgenero;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }
    setIdTomo(idtomo){
        this.IdTomo = idtomo;
    }
    setFechaToma(fecha){
        this.FechaToma = fecha;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde la inserción de muestras o también
     * períodicamente que carga la grilla con muestras pendientes
     */
    cargaMuestras(){

        // cargamos el formulario
        $("#form_muestras").load("muestras/grilla_muestras.php");

    }

    /**
     * @param {int} protocolo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro un número de protocolo y
     * genera la consulta de inserción de una nueva muestra en
     * la base (llamado desde el formulario de pacientes en caso
     * de un alta o generación de nueva muestra)
     */
    nuevaMuestra(protocolo){

        // ejecutamos la consulta en el servidor
        $.ajax({
            url: "muestras/nueva_muestra.php?protocolo="+protocolo,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si no hubo error
                if (data.IdMuestra != 0){

                    // presentamos el mensaje
                    new jBox('Notice', {content: "Toma de muestra generada", color: 'green'});

                    // recargamos la grilla
                    muestras.cargaMuestras();

                // si hubo un error
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                }

            }});

    }

    /**
     * @param {int} idmuestra
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre la toma de muestras, llama la rutina
     * php que asigna la muestra como tomada por el usuario activo
     */
    tomaMuestra(idmuestra){

        // ejecutamos la consulta en el servidor
        $.ajax({
            url: "muestras/toma_muestra.php?muestra="+idmuestra,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // presentamos el mensaje
                new jBox('Notice', {content: "Muestra tomada", color: 'green'});

                // recargamos la grilla
                muestras.cargaMuestras();


            }});

    }

}