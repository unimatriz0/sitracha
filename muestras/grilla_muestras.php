<?php

/**
 *
 * muestras/grilla_muestras.php
 *
 * @package     Diagnostico
 * @subpackage  Muestras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta la grilla de muestras no tomadas para el laboratorio
 * del usuario activo
*/

// incluimos e instanciamos las clases
require_once("muestras.class.php");
$muestras = new Muestras();

// obtenemos la nómina de muestras
$nomina = $muestras->nominaMuestras();

// si no hay registros
if (count($nomina) == 0){

    // presenta el mensaje
    echo "<h2>No existen muestras pendientes</h2>";

// si hay muestras sin tomar
} else {

    // definimos la tabla
    echo "<table width='95%' align='center' border='0' id='grilla_muestras'>";

    // definimos el encabezado
    echo "<thead>";
    echo "<tr>";
    echo "<th>Protocolo</th>";
    echo "<th>Nombre</th>";
    echo "<th>Documento</th>";
    echo "<th>Sexo</th>";
    echo "<th>Ingreso</th>";
    echo "<th>Alta</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // presenta el registro
        echo "<td>$protocolo</td>";
        echo "<td>$nombre</td>";
        echo "<td>$documento</td>";
        echo "<td>$sexo</td>";
        echo "<td>$genero</td>";
        echo "<td>$fecha_alta</td>";

        // arma el enlace de toma
        echo "<td>";
        echo "<input type='button'
                     name='btnTomar'
                     id='btnTomar'
                     title='Pulse para tomar la muestra'
                     class='botonagregar'
                     onClick='muestras.tomaMuestra($idmuestra)'>";

        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody>";
    echo "</table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

    ?>
    <SCRIPT>

        // definimos las propiedades de la tabla
         $('#grilla_muestras').datatable({
            pageSize: 15,
            sort:    [true, true, true, true,     true,     true, false],
            filters: [true, true, true, 'select', 'select', true, false],
            filterText: 'Buscar ... '
        });

    </SCRIPT>
    <?php

}

?>
