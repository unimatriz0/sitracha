<?php

/**
 *
 * muestras/toma_muestra.php
 *
 * @package     Diagnostico
 * @subpackage  Muestras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una muestra y la asigna como
 * tomada por el usuario activo, retorna siempre verdadero
 */

// incluimos e instanciamos las clases
require_once("muestras.class.php");
$muestras = new Muestras();

// ejecutamos la consulta
$muestras->tomaMuestra($_GET["muestra"]);

// retornamos
echo json_encode(array("Muestra" => $_GET["muestra"]));

?>