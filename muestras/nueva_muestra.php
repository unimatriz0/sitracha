<?php

/**
 *
 * muestras/nueva_muestra.php
 *
 * @package     Diagnostico
 * @subpackage  Muestras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un protocolo y genera la toma de
 * una nueva muestra, retorna el resultado de la operación
*/

// inclusión de archivos
require_once ("muestras.class.php");
$muestra = new Muestras();

// asignamos el protocolo
$muestra->setIdProtocolo($_GET["Protocolo"]);

// generamos la muestra
$resultado = $muestra->grabaMuestra();

// retornamos el resultado
echo json_encode(array("IdMuestra" => $resultado));

?>