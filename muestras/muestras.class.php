<?php

/**
 *
 * Class Muestras | muestras/muestras.class.php
 *
 * @package     Diagnostico
 * @subpackage  Muestras
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de muestras
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Muestras {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos

    // variables de la tabla de muestras
    protected $IdMuestra;             // clave del registro
    protected $IdProtocolo;           // clave del protocolo
    protected $IdLaboratorio;         // laboratorio del usuario
    protected $Comentarios;           // comentarios y observaciones
    protected $IdGenero;              // usuario que generó la muestra
    protected $IdTomo;                // usuario que tomo la muestra
    protected $FechaToma;             // fecha de la toma
    protected $FechaAlta;             // fecha alta / generación de la toma

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables de clase
        $this->IdMuestra = 0;
        $this->IdProtocolo = 0;
        $this->Comentarios = "";
        $this->FechaToma = date('d/m/Y');
        $this->FechaAlta = "";

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario activo y del laboratorio
            $this->IdGenero = $_SESSION["ID"];
            $this->IdTomo = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdMuestra($idmuestra){
        $this->IdMuestra = $idmuestra;
    }
    public function setIdProtocolo($idprotocolo){
        $this->IdProtocolo = $idprotocolo;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }

    // métodos de retorno de valores
    public function getIdMuestra(){
        return $this->IdMuestra;
    }
    public function getIdProtocolo(){
        return $this->IdProtocolo;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }
    public function getFechaToma(){
        return $this->FechaToma;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método público que retorna las muestras generadas perso sin
     * tomar para el laboratorio del usuario activo
     * @return array
     */
    public function nominaMuestras(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_muestras.id_muestra AS idmuestra,
                            diagnostico.v_muestras.id_protocolo AS protocolo,
                            diagnostico.v_muestras.nombre AS nombre,
                            diagnostico.v_muestras.documento AS documento,
                            diagnostico.v_muestras.sexo AS sexo,
                            diagnostico.v_muestras.genero AS genero,
                            diagnostico.v_muestras.fecha_alta AS fecha_alta
                     FROM diagnostico.v_muestras
                     WHERE ISNULL(diagnostico.v_muestras.idtomo) AND
                           diagnostivo.v_muestras.idlaboratorio = '$this->IdLaboratorio'
                     ORDER BY diagnostico.v_muestras.id_muestra;";
        $resultado = $this->Link->query($consulta);

        // obtenemos la grilla
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos la grilla
        return $nomina;

    }

    /**
     * Método que genera la consulta de grabación en la base
     * retorna la clave del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaMuestra(){

        // si está insertando
        if ($this->IdMuestra == 0){
            $this->nuevaMuestra();
        } else {
            $this->editaMuestra();
        }

        // retornamos la id
        return $this->IdMuestra;

    }

    /**
     * Método protegido que genera la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaMuestra(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.muestras
                            (id_protocolo,
                             id_laboratorio,
                             id_genero,
                             comentarios)
                            VALUES
                            (:id_protocolo,
                             :id_laboratorio,
                             :id_genero,
                             :comentarios)";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":id_protocolo", $this->IdProtocolo);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":id_genero", $this->IdGenero);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);

        // la ejecutamos
        $psInsertar->execute();

        // obtenemos la id del protocolo
        $this->IdMuestra = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que genera la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaMuestra(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.muestras SET
                            id_protocolo = :id_protocolo,
                            id_laboratorio = :id_laboratorio,
                            comentarios = :comentarios,
                            id_genero = :idgenero
                     WHERE diagnostico.muestras.id_muestra = :idmuestra;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":id_protocolo", $this->IdProtocolo);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":id_genero", $this->IdGenero);
        $psInsertar->bindParam(":comentarios", $this->Comentarios);
        $psInsertar->bindParam(":id_muestra", $this->IdMuestra);

        // la ejecutamos
        $psInsertar->execute();

    }

    /**
     * Método público que recibe la id de la muestra y la asigna como
     * tomada por el usuario activo
     * @param int $idmuestra
     */
    public function tomaMuestra($idmuestra){

        // compone la consulta de actualización
        $consulta = "UPDATE diagnostico.muestras SET
                            id_tomo = :id_tomo,
                            fecha_toma = :fecha_toma
                     WHERE diagnostico.muestras.id_muestra = :idmuestra;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":id_tomo", $this->IdTomo);
        $psInsertar->bindParam(":fecha_toma", $this->FechaToma);
        $psInsertar->bindParam(":id_muestra", $idmuestra);

        // la ejecutamos
        $psInsertar->execute();

    }

    /**
     * Método púlico que recibe la id de la muestra y la asigna como
     * no tomada
     * @param int $idmuestra
     */
    public function borraToma($idmuestra){

        // compone la consulta
        $consulta = "UPDATE diagnostico.muestras SET
                            id_tomo = NULL
                            fecha_toma = NULL
                     WHERE diagnostico.muestras.id_muestra = '$idmuestra';";
        $this->Link->exec($consulta);

    }

}