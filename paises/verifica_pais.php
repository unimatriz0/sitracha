<?php

/**
 *
 * paises/verifica_pais.php
 *
 * @package     Diagnostico
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/01/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una cadena de texto con el nombre 
 * de un país y retorna verdadero si ese país ya está declarado
 * utilizado en el alta para evitar registros repetidos
 * 
 * @param $_GET["pais"]
 * 
*/

// incluimos e instanciamos la clase
require_once ("paises.class.php");
$paises = new Paises();

// verificamos si existe
$resultado = $paises->existePais($_GET["pais"]);

// retornamos el estado de la operación
echo json_encode(array("error" => $resultado));

?>