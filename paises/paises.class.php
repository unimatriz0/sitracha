<?php

/**
 *
 * Class Paises | paises/paises.class.php
 *
 * @package     Diagnostico
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 *
 * Esta clase controla las operaciones de la tabla auxiliar de países
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 *
 */
class Paises{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $FechaAlta;             // fecha de alta del registro
    protected $IdPais;                // clave del país
    protected $NombrePais;            // nombre del país
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdPais = 0;
        $this->NombrePais = "";

        // inicializamos la fecha de alta en formato mysql
        $this->FechaAlta = date('Y/m/d');

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setIdPais($idpais){

        // verifica que sea un número
        if (!is_numeric($idpais)){

            // abandona por error
            echo "La clave del país debe ser un número";
            exit;

            // si está correcto
        } else {

            // lo asigna
            $this->IdPais = $idpais;

        }

    }
    public function setNombrePais($nombrepais){
        $this->NombrePais = $nombrepais;

    }

    /**
     * Método que retorna un vector con la nómina de países de la base
     * de datos de diccionarios
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function listaPaises(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.paises.NOMBRE AS pais,
                            diccionarios.paises.ID AS idpais,
                            cce.responsables.USUARIO AS usuario,
                            DATE_FORMAT(diccionarios.paises.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta
                     FROM diccionarios.paises INNER JOIN cce.responsables ON diccionarios.paises.USUARIO = responsables.ID
                     WHERE diccionarios.paises.NOMBRE <> 'NO DECLARADA'
                     ORDER BY diccionarios.paises.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // obtenemos el array con todos los registros
        $registros = $resultado->fetchAll(PDO::FETCH_ASSOC);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaPaises = array_change_key_case($registros, CASE_LOWER);

        // retornamos el vector
        return $nominaPaises;

    }

    /**
     * Método que recibe como parámetro el nombre de un país y retorna
     * verdadero si ya está declarado en la base, utilizado para evitar
     * el ingreso de registros duplicados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $pais - nombre del país
     * @return boolean - si lo encontró en la base
     */
    public function existePais($pais){

        // inicializamos las variables
        $registros = 0;

        // componemos y ejecutamos la consulta
        $consulta = "SELECT COUNT(diccionarios.paises.ID) AS registros
                     FROM diccionarios.paises
                     WHERE diccionarios.paises.NOMBRE = '$pais';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = $resultado->fetch(PDO::FETCH_ASSOC);
        $registro = array_change_key_case($fila, CASE_LOWER);
        extract($registro);

        // si lo encontró
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método que según el caso realiza la consulta de inserción o edición
     * en la tabla de paises, retorna la id del registro o el resultado
     * de la operación
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int . clave del registro insertado / editado
     */
    public function grabaPais(){

        // si está insertando
        if ($this->IdPais == 0){

            // insertamos el registro
            $this->nuevoPais();

            // si está editando
        } else {

            // editamos el registro
            $this->editaPais();

        }

        // retornamos la id del registro
        return $this->IdPais;

    }

    /**
     * Método protegido que inserta un nuevo registro en la tabla de países
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoPais(){

        // compone la consulta de inserción
        $consulta = "INSERT INTO diccionarios.paises
                            (NOMBRE,
                            USUARIO,
                            FECHA_ALTA)
                            VALUES
                            (:pais,
                            :usuario,
                            :fecha_alta);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":pais", $this->NombrePais);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->IdPais = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que edita el registro de la tabla de países
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaPais(){

        // compone la consulta de actualización
        $consulta = "UPDATE diccionarios.paises SET
                            NOMBRE = :pais,
                            USUARIO = :usuario,
                            FECHA_ALTA = :fecha_alta
                     WHERE diccionarios.paises.ID = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":pais", $this->NombrePais);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);
        $psInsertar->bindParam(":id", $this->IdPais);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}

?>