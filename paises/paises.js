/*
 * Nombre: paises.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 13/01/2017
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en los formularios de
 *              abm de paises
 */

 /**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

 /**
  * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
  * @class Clase que controla el abm de países
  */
 class Paises {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor (){

        // inicializa las variables de clase
        this.initPaises();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initPaises(){

        // declaración de variables
        this.Id = 0;
        this.Pais = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de países en el contenido
     */
    mostrarPaises(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("paises/form_paises.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id clave del registro
     * @return [boolean] el resultado de la operación
     * Método que verifica los datos del formulario de países
     */
    verificaPais(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está dando un alta
        if (id === undefined){
            id = "nuevo";
        }

        // asignamos en la variable de clase
        this.Id = id;

        // obtiene el nombre
        this.Pais = document.getElementById("nombrepais_" + id).value;

        // si no lo ingresó
        if (this.Pais === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre del País";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombrepais_" + id).focus();
            return false;

        }

        // si está dando un alta
        if (this.Id == "nuevo"){

            // verifica que no esté repetido
            if (this.validarPais()){

                // presenta el mensaje y retorna
                mensaje = "Ese País ya se encuentra declarado";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // graba el registro
        this.grabaPais();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} el resultado de la operación
     * Método llamado luego de verificar el formulario de países
     * crea el formdata y lo envía por ajax al servidor
     */
    grabaPais(){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id !== "nuevo"){
            formulario.append("Id", this.Id);
        }

        // agrega los valores
        formulario.append("Pais", this.Pais);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "paises/graba_pais.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // inicializa las variables de clase
                    paises.initPaises();

                    // recarga el formulario para reflejar los cambios
                    paises.mostrarPaises();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean}
     * Método llamado en el alta de países, verifica que no exista
     * en la base para evitar entradas repetidas
     */
    validarPais(){

        // declaración de variables
        var resultado;

        // llama sincrónicamente
        $.ajax({
            url: 'paises/verifica_pais.php?pais='+this.Pais,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error === true){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id del select de un formulario
     * @param {int} idpais clave del país preseleccionado (optativo)
     * Método que recibe como parámetro la id del elemento de un
     * formulario y (optativo) la clave de un país, carga en el
     * select la nómina de países y si recibió la clave de uno
     * de ellos lo preselecciona
     */
    listaPaises(idelemento, idpais){

        // limpia el combo
        $("#" + idelemento).html('');

        // lo llamamos asincrónico
        $.ajax({
            url: "paises/lista_paises.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // agrega el primer elemento en blanco
                $("#" + idelemento).append("<option value=0>Seleccione</option>");

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si existe la clave y es igual
                    if (typeof(idpais) != "undefined"){

                        // si es la misma
                        if (idpais == data[i].Id){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Pais + "</option>");

                        // si son distintos
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Pais + "</option>");

                        }

                    // si no recibió la clave del país recorre el
                    // bucle agregando
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Pais + "</option>");
                    }

                }

        }});

    }

}
