<?php

/**
 *
 * paises/graba_pais.php
 *
 * @package     Diagnostico
 * @subpackage  Paises
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/01/2017)
 * @copyright   Copyright (c) 2017, INP
 * 
 * Método que recibo por post los datos del formulario y ejecuta la 
 * consulta en la base de datos, retorna en formato json la id del 
 * registro (cero en caso de error)
 *
*/

// incluimos e instanciamos la clase
require_once ("paises.class.php");
$pais = new Paises();

// si recibió la id
if (!empty($_POST["Id"])){
    $pais->setIdPais($_POST["Id"]);
}

// asigna el nombre
$pais->setNombrePais($_POST["Pais"]);

// grabamos
$resultado = $pais->grabaPais();

// retorna el resultado
echo json_encode(array("error" => $resultado));

?>