<?php

/**
 * 
 * paises/form_paises.php
 *
 * Procedimiento que arma el formulario para el abm de países
 * @package     Diagnostico
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (13/01/2017)
 * @copyright   Copyright (c) 2017, INP
 * 
 * 
*/

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["Usuario"])){
    $usuario_sesion = $_SESSION["Usuario"];
}
session_write_close();

// incluimos e instanciamos la clase
require_once ("paises.class.php");
$paises = new Paises();

// obtenemos la nómina de países
$nomina_paises = $paises->listaPaises();

// presentamos el título
echo "<h2>Nómina de Paises registrados</h2>";

// definimos la tabla con un formato a dos columnas
echo "<table width='95%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";

// la primer columna
echo "<th>País</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th></th>";

// la segunda columna
echo "<th>País</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th></th>";

// cerramos el encabezado
echo "</tr>";
echo "</thead>";

// abrimos el cuerpo
echo "<tbody>";

// iniciamos el contador de columnas
$columna = 1;

// recorremos el vector
foreach ($nomina_paises AS $registro){

    // obtenemos el registro
    extract($registro);

    // si estamos en la primer columna
    if ($columna == 1){

        // abrimos la fila
        echo "<tr>";

    }

    // presentamos el país
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre del País'>";
    echo "<input type='text' name='nombrepais'
                 id='nombrepais_$idpais'
                 value='$pais'
                 size='20'>";
    echo "</span>";
    echo "</td>";

    // presentamos el resto del registro
    echo "<td>$usuario</td>";
    echo "<td>$fecha_alta</td>";

    // presenta el botón grabar
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaPais'
           id='btnGrabaPais'
           title='Graba el país en la base'
           onClick='paises.verificaPais($idpais)'
           class='botongrabar'>";
    echo "</td>";

    // incrementamos el contador de columnas
    $columna++;

    // si ya presentamos la segunda columna
    if ($columna > 2){

        // cerramos la fila y reiniciamos el contador
        echo "</tr>";
        $columna = 1;

    }

}

// agregamos el nuevo registro

// si estamos en la primer columna
if ($columna == 1){

    // abrimos la fila
    echo "<tr>";

}

// presentamos el país
echo "<td>";
echo "<span class='tooltip'
            title='Nombre del País'>";
echo "<input type='text'
             name='nombrepais'
             id='nombrepais_nuevo'
             size='20'>";
echo "</span>";
echo "</td>";

// obtenemos la fecha actual
$fecha_alta = date('d/m/Y');

// presentamos el resto del registro
echo "<td>$usuario_sesion</td>";
echo "<td>$fecha_alta</td>";

// presenta el botón grabar
echo "<td align='center'>";
echo "<input type='button' name='btnGrabaPais'
       id='btnGrabaPais'
       title='Graba el País en la base'
       onClick='paises.verificaPais()'
       class='botonagregar'>";
echo "</td>";

// siempre cerramos
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>