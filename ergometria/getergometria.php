<?php

/**
 *
 * getergometria | ergometria/getergometria.class.php
 *
 * @package     Diagnostico
 * @subpackage  Ergometria
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (26/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una visita y retorna
 * el array json con los datos del registro
 *
*/

// inclusión de archivos
require_once("ergometria.class.php");
$ergometria = new Ergometria();

// obtenemos el registro
$ergometria->getDatosErgometria($_GET["idvisita"]);

// retornamos en formato json
echo json_encode(array("Id" =>        $ergometria->getId(),
                       "Fecha" =>     $ergometria->getFecha(),
                       "FcBasal" =>   $ergometria->getFcbasal(),
                       "FcMax" =>     $ergometria->getFcmax(),
                       "TaBasal" =>   $ergometria->getTabasal(),
                       "TaMax" =>     $ergometria->getTamax(),
                       "Kpm" =>       $ergometria->getKpm(),
                       "Itt" =>       $ergometria->getItt(),
                       "Usuario" =>   $ergometria->getUsuario(),
                       "FechaAlta" => $ergometria->getFechaAlta()));

?>