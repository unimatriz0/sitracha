/*
 * Nombre: ergometria.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 11/06/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de las ergometrías
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las ergometrías
 */
class Ergometria {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initErgometria();

        // inicializamos el layer
        this.layerErgometria = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initErgometria(){

        // inicializamos las variables
        this.Id = 0;                 // clave del registro
        this.Visita = 0;             // clave de la visita
        this.Paciente = 0;           // protocolo del paciente
        this.Fecha = "";             // fecha de la visita
        this.FcBasal = 0;            // frecuencia cardíaca basal
        this.FcMax = 0;              // frecuencia cardíaca máxima
        this.TaBasal = 0;            // tensión arterial basal
        this.TaMax = 0;              // tensión arterial máxima
        this.Kpm = 0;                // kilómetros recorridos
        this.Itt = 0;                // texto libre
        this.Usuario = "";           // nombre del usuario
        this.FechaAlta = "";         // fecha de alta del registro

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setFcBasal(fc){
        this.FcBasal = fc;
    }
    setFcMax(fc){
        this.FcMax = fc;
    }
    setTaBasal(ta){
        this.TaBasal = ta;
    }
    setTaMax(ta){
        this.TaMax = ta;
    }
    setKpm(kpm){
        this.Kpm = kpm;
    }
    setItt(itt){
        this.Itt = itt;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta en el layer emergente el
     * formulario de ergometría
     */
    verErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no grabó la visita
        if (visitas.IdVisita == 0){

            // presenta el mensaje
            mensaje = "Debe grabar los datos de la visita primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // asignamos los valores del registro
        this.Visita = visitas.IdVisita;
        this.Paciente = document.getElementById("protocolo_paciente").value;

        // abrimos y mostramos el layer
        this.layerErgometria = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Ergometrías',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:700,
                    draggable: 'title',
                    zIndex: 20000,
                    ajax: {
                        url: 'ergometria/form_ergometria.html',
                        reload: 'strict'
                }
            });
        this.layerErgometria.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de la clave obtiene los datos de
     * la ergometría
     */
    getDatosErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "ergometria/getergometria.php?idvisita=" + this.Visita,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                ergometria.cargaDatosErgometria(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector con el registro
     * Método que recibe como parámetro el vector con
     * los datos del registro y lo asigna a las variables
     * de clase
     */
    cargaDatosErgometria(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // la clave de la visita y el protocolo no lo
        // asignamos porque la leemos del formulario padre

        // asignamos el resto de las variables
        this.setId(datos.Id);
        this.setFecha(datos.Fecha);
        this.setFcBasal(datos.FcBasal);
        this.setFcMax(datos.FcMax);
        this.setTaBasal(datos.TaBasal);
        this.setTaMax(datos.TaMax);
        this.setKpm(datos.Kpm);
        this.setItt(datos.Itt);
        this.setUsuario(datos.Usuario);
        this.setFechaAlta(datos.FechaAlta);

        // mostramos el registro
        this.muestraErgometria();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase
     * carga los datos en el formulario
     */
    muestraErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en el formulario

        // la fecha de la administraciòn
        document.getElementById("fecha_ergometria").value = this.Fecha;

        // la frecuencia basal
        document.getElementById("fc_basal").value = this.FcBasal;

        // la tensión basal
        document.getElementById("ta_basal").value = this.TaBasal;

        // la distancia
        document.getElementById("kpm").value = this.Kpm;

        // la frecuencia máxima
        document.getElementById("fc_max").value = this.FcMax;

        // la tensión máxima
        document.getElementById("ta_max").value = this.TaMax;

        // el itt
        document.getElementById("itt_max").value = this.Itt;

        // fijamos el usuario y la fecha de alta
        if (this.Id != 0){
            document.getElementById("usuarioergo").value = this.Usuario;
            document.getElementById("altaergo").value = this.FechaAlta;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que valida
     * los datos del formulario
     */
    validaErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no indicó la fecha
        if (document.getElementById("fecha_ergometria").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de administración";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_ergometria").focus();
            return false;

        // si indicò
        } else {

            // asigna en la variable de clase
            this.Fecha = document.getElementById("fecha_ergometria").value;

        }

        // si no indicò la frecuencia basal
        if (document.getElementById("fc_basal").value == 0 || document.getElementById("fc_basal").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la frecuencia cardíaca basal";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fc_basal").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.FcBasal = document.getElementById("fc_basal").value;

        }

        // si no indicò la tensión arterial basal
        if (document.getElementById("ta_basal").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la tensión arterial basal";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ta_basal").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.TaBasal = document.getElementById("ta_basal").value;

        }

        // si no indicò la distancia
        if (document.getElementById("kpm").value == 0 || document.getElementById("kpm").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la distancia";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("kpm").focus();
            return false;

        // si asignó
        } else {

            // asigna en la clase
            this.Kpm = document.getElementById("kpm").value;

        }

        // si no indicó la frecuencia máxima
        if (document.getElementById("fc_max").value == 0 || document.getElementById("fc_max").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la frecuencia cardíaca máxima";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fc_max").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.FcMax = document.getElementById("fc_max").value;

        }

        // si no ingresó la tensión máxima
        if (document.getElementById("ta_max").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la tensión arterial máxima";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ta_max").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.TaMax = document.getElementById("ta_max").value;

        }

        // si no ingresó el itt
        if (document.getElementById("itt_max").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la ITT máxima";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("itt_max").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Itt = document.getElementById("itt_max").value;

        }

        // grabamos el registro
        this.grabaErgometria();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario
     * que ejecuta la consulta en el servidor
     */
    grabaErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosErgometria = new FormData();

        // agregamos los datos al formulario
        datosErgometria.append("Id", this.Id);
        datosErgometria.append("Visita", this.Visita);
        datosErgometria.append("Paciente", this.Paciente);
        datosErgometria.append("Fecha", this.Fecha);
        datosErgometria.append("FcBasal", this.FcBasal);
        datosErgometria.append("FcMax", this.FcMax);
        datosErgometria.append("TaBasal", this.TaBasal);
        datosErgometria.append("TaMax", this.TaMax);
        datosErgometria.append("Kpm", this.Kpm);
        datosErgometria.append("Itt", this.Itt);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "ergometria/grabaergometria.php",
            type: "POST",
            data: datosErgometria,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    ergometria.setId(data.Id);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * recarga el formulario o limpia según si está
     * editando o insertando
     */
    cancelaErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está editando
        if (this.Id != 0){
            this.getDatosErgometria();
        } else {
            this.limpiaErgometria();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario
     */
    limpiaErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("fecha_ergometria").value = "";
        document.getElementById("fc_basal").value = "";
        document.getElementById("ta_basal").value = "";
        document.getElementById("kpm").value = "";
        document.getElementById("fc_max").value = "";
        document.getElementById("ta_max").value = "";
        document.getElementById("itt_max").value = "";

        // fijamos el usuario y la fecha de alta
        document.getElementById("usuarioergo").value = sessionStorage.getItem("Usuario");
        document.getElementById("altaergo").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de pedir confirmación elimina el registro
     * actual y cierra el formulario
     */
    borraErgometria(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Ergometría',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('ergometria/borraergo.php?id='+ergometria.Id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // cerramos el layer
                            ergometria.layerErgometria.destroy();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}