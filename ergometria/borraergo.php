<?php

/**
 *
 * borraergo | ergometria/borraergo.class.php
 *
 * @package     Diagnostico
 * @subpackage  Ergometria
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (26/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta
 * la consulta de eliminación en el servidor
 *
*/

// inclusión de archivos
require_once("ergometria.class.php");
$ergometria = new Ergometria();

// eliminamos
$ergometria->borraErgometria($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>