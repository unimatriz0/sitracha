<?php

/**
 *
 * Class Ergometria | ergometria/ergometria.class.php
 *
 * @package     Diagnostico
 * @subpackage  Ergometria
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (0/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * ergometrías realizadas
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Ergometria {

    // definición de variables de clase
    protected $Link;                // puntero a la base de datos
    protected $IdUsuario;           // clave del usuario activo
    protected $Id;                  // clave del registro
    protected $Paciente;            // clave del protocolo
    protected $Visita;              // clave de la visita
    protected $Fecha;               // fecha de administración
    protected $Fcbasal;             // frecuencia basal
    protected $Fcmax;               // frecuencia máxima
    protected $Tabasal;             // tensiòn arterial basal
    protected $Tamax;               // tensión arterial máxima
    protected $Kpm;                 // kilòmetros?
    protected $Itt;                 // verificar unidad
    protected $Observaciones;       // observaciones del usuario
    protected $Usuario;             // nombre del usuario
    protected $FechaAlta;           // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initErgometria();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el constructor o desde la consulta
     * que inicializa las variables de clase
     */
    protected function initErgometria() {

    	// inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Visita = 0;
        $this->Fecha = "";
        $this->Fcbasal = 0;
        $this->Fcmax = 0;
        $this->Tabasal = "";
        $this->Tamax = "";
        $this->Kpm = 0;
        $this->Itt = "";
        $this->Observaciones = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }
    public function setFcbasal($frecuencia){
        $this->Fcbasal = $frecuencia;
    }
    public function setFcmax($frecuencia){
        $this->Fcmax = $frecuencia;
    }
    public function setTabasal($tension){
        $this->Tabasal = $tension;
    }
    public function setTamax($tension){
        $this->Tamax = $tension;
    }
    public function setKpm($kpm){
        $this->Kpm = $kpm;
    }
    public function setItt($itt){
        $this->Itt = $itt;
    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getFcbasal(){
        return $this->Fcbasal;
    }
    public function getFcmax(){
        return $this->Fcmax;
    }
    public function getTabasal(){
        return $this->Tabasal;
    }
    public function getTamax(){
        return $this->Tamax;
    }
    public function getKpm(){
        return $this->Kpm;
    }
    public function getItt(){
        return $this->Itt;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - entero con el protocolo del paciente
     * @return resultset con los registros encontrados
     * Método que recibe como parámetro la clave de un protocolo
     * y retorna todos las ergometrías de ese paciente, utilizado
     * en las historias clínicas agrupadas por estudio
     */
    public function nominaErgometria($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_ergometria.id AS id,
                            diagnostico.v_ergometria.paciente AS paciente,
                            diagnostico.v_ergometria.visita AS visita,
                            diagnostico.v_ergometria.fecha AS fecha,
                            diagnostico.v_ergometria.fcbasal AS fcbasal,
                            diagnostico.v_ergometria.fcmax AS fcmax,
                            diagnostico.v_ergometria.tabasal AS tabasal,
                            diagnostico.v_ergometria.tamax AS tamax,
                            diagnostico.v_ergometria.kpm AS kpm,
                            diagnostico.v_ergometria.itt AS itt,
                            diagnostico.v_ergometria.observaciones AS observaciones,
                            diagnostico.v_ergometria.usuario AS usuario,
                            diagnostico.v_ergometria.fecha_alta AS fecha_alta
                     FROM diagnostico.ergometria
                     WHERE diagnostico.ergometria.paciente = '$protocolo'
                     ORDER BY STR_TO_DATE(diagnostico.ergometria.fecha, '%d/%m/%Y') ASC; ";

        // la ejecutamos y retornamos el vector
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave de la visita
     * Método que recibe como parámetro la clave de una
     * ergometría y asigna los valores del registro en
     * las variables de clase
     */
    public function getDatosErgometria($clave){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_ergometria.id AS id,
                            diagnostico.v_ergometria.paciente AS paciente,
                            diagnostico.v_ergometria.visita AS visita,
                            diagnostico.v_ergometria.fecha AS fecha,
                            diagnostico.v_ergometria.fcbasal AS fcbasal,
                            diagnostico.v_ergometria.fcmax AS fcmax,
                            diagnostico.v_ergometria.tabasal AS tabasal,
                            diagnostico.v_ergometria.tamax AS tamax,
                            diagnostico.v_ergometria.kpm AS kpm,
                            diagnostico.v_ergometria.itt AS itt,
                            diagnostico.v_ergometria.observaciones AS observaciones,
                            diagnostico.v_ergometria.usuario AS usuario,
                            diagnostico.v_ergometria.fecha_alta AS fecha_alta
                     FROM diagnostico.v_ergometria
                     WHERE diagnostico.v_ergometria.visita = '$clave'; ";

        // la ejecutamos
        $resultado = $this->Link->query($consulta);

        // si tuvo registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro y lo asignamos en
            // las variables de clase
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Visita = $visita;
            $this->Fecha = $fecha;
            $this->Fcbasal = $fcbasal;
            $this->Fcmax = $fcmax;
            $this->Tabasal = $tabasal;
            $this->Tamax = $tamax;
            $this->Kpm = $kpm;
            $this->Itt = $itt;
            $this->Observaciones = $observaciones;
            $this->Usuario = $usuario;
            $this->FechaAlta = $fecha_alta;

        // si no hu resultados
        } else {

            // inicializamos las variables
            $this->initErgometria();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public function grabaErgometria(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaErgometria();
        } else {
            $this->editaErgometria();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevaErgometria(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.ergometria
                            (paciente,
                             visita,
                             fecha,
                             fcbasal,
                             fcmax,
                             tabasal,
                             tamax,
                             kpm,
                             itt,
                             observaciones,
                             usuario)
                            VALUES
                            (:paciente,
                             :visita,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :fcbasal,
                             :fcmax,
                             :tabasal,
                             :tamax,
                             :kpm,
                             :itt,
                             :observaciones,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":visita",        $this->Visita);
        $psInsertar->bindParam(":fecha",         $this->Fecha);
        $psInsertar->bindParam(":fcbasal",       $this->Fcbasal);
        $psInsertar->bindParam(":fcmax",         $this->Fcmax);
        $psInsertar->bindParam(":tabasal",       $this->Tabasal);
        $psInsertar->bindParam(":tamax",         $this->Tamax);
        $psInsertar->bindParam(":kpm",           $this->Kpm);
        $psInsertar->bindParam(":itt",           $this->Itt);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaErgometria(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.ergometria SET
                            paciente = :paciente,
                            visita = :visita,
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            fcbasal = :fcbasal,
                            fcmax = :fcmax,
                            tabasal = :tabasal,
                            tamax = :tamax,
                            kpm = :kpm,
                            itt = :itt,
                            observaciones = :observaciones,
                            usuario = :usuario
                     WHERE diagnostico.ergometria.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":visita",        $this->Visita);
        $psInsertar->bindParam(":fecha",         $this->Fecha);
        $psInsertar->bindParam(":fcbasal",       $this->Fcbasal);
        $psInsertar->bindParam(":fcmax",         $this->Fcmax);
        $psInsertar->bindParam(":tabasal",       $this->Tabasal);
        $psInsertar->bindParam(":tamax",         $this->Tamax);
        $psInsertar->bindParam(":kpm",           $this->Kpm);
        $psInsertar->bindParam(":itt",           $this->Itt);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);
        $psInsertar->bindParam(":id",            $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe la clave de un registro y ejecuta la
     * consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idergometria - clave del registro
     */
    public function borraErgometria($idergometria){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.ergometria
                     WHERE diagnostico.ergometria.id = '$idergometria';";
        $this->Link->exec($consulta);

    }

}
?>