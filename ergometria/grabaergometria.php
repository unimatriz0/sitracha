<?php

/**
 *
 * grabaergometria | ergometria/grabaergometria.class.php
 *
 * @package     Diagnostico
 * @subpackage  Ergometria
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (26/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta la
 * consulta en el servidor
 *
*/

// inclusión de archivos
require_once("ergometria.class.php");
$ergometria = new Ergometria();

// asignamos los valores
$ergometria->setId($_POST["Id"]);
$ergometria->setVisita(($_POST["Visita"]));
$ergometria->setPaciente($_POST["Paciente"]);
$ergometria->setFecha($_POST["Fecha"]);
$ergometria->setFcbasal($_POST["FcBasal"]);
$ergometria->setFcmax($_POST["FcMax"]);
$ergometria->setTabasal($_POST["TaBasal"]);
$ergometria->setTamax($_POST["TaMax"]);
$ergometria->setKpm($_POST["Kpm"]);
$ergometria->setItt($_POST["Itt"]);

// ejecutamos la consulta
$id = $ergometria->grabaErgometria();

// retornamos la id
echo json_encode(array("Error" => $id));

?>