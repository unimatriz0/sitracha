<?php

/**
 *
 * noconformidad/graba_noconformidad.php
 *
 * @package     Diagnostico
 * @subpackage  NoConformidad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta en el servidor, retorna por json la id del
 * registro afectado
 *
*/

// incluimos e instanciamos las clases
require_once("noconformidad.class.php");
$noconformidad = new NoConformidad();

// si recibió la id
if (!empty($_POST["Id"])){
    $noconformidad->setIdConformidad($_POST["Id"]);
}

// asignamos el resto de valores
$noconformidad->setTitulo($_POST["Titulo"]);
$noconformidad->setFecha($_POST["Fecha"]);
$noconformidad->setDetalle($_POST["Detalle"]);
$noconformidad->setCausa($_POST["Causa"]);
$noconformidad->setRespuesta($_POST["Respuesta"]);
$noconformidad->setAccion($_POST["Accion"]);
$noconformidad->setEjecucion($_POST["Ejecucion"]);

// grabamos el registro
$resultado = $noconformidad->grabaConformidad();

// retornamos la id
echo json_encode(array("Id" => $resultado));

?>