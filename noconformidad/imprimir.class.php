<?php

/**
 *
 * Class Imprimir | noconformidad/imprimir.class.php
 *
 * @package     Diagnostico
 * @subpackage  NoConformidad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// define la ruta a las fuentes pdf (lo llamamos desde
// el script de impresión y el path queda en noconformidades)
define('FPDF_FONTPATH', '../clases/fpdf/font');

// la clase pdf con la extensión html
require_once ("../clases/fpdf/tfpdf.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que genera el documento PDF de la no conformidad
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Imprimir{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Documento;                     // objeto de la clase pdf
    protected $Nombre;                        // nombre completo del usuario
    protected $Laboratorio;                   // laboratorio de la no conformidad
    protected $Jefe;                          // jefe del laboratorio
    protected $Fecha;                         // fecha de la no conformidad
    protected $Titulo;                        // titulo de la no conformidad
    protected $Detalle;                       // detalle de la incidencia
    protected $Causa;                         // causa de la incidencia
    protected $Respuesta;                     // fecha de respuesta
    protected $Accion;                        // descripción de la acción correctiva
    protected $Ejecucion;                     // fecha de ejecución de la acción
    protected $Logo;                          // logo del laboratorio
    protected $Link;                          // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Documento = "";
        $this->Nombre = "";
        $this->Laboratorio = "";
        $this->Jefe = "";
        $this->Fecha = "";
        $this->Titulo = "";
        $this->Detalle = "";
        $this->Causa = "";
        $this->Respuesta = "";
        $this->Accion = "";
        $this->Ejecucion = "";
        $this->Logo = "";

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // desactivamos los errores porque la clase pdf chifla
        // por una variable no inicializada
        error_reporting(0);

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método principal del sistema que recibe como parámetro la id de la
     * no conformidad y luego genera el documento
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param [int] idnoconformidad - clave del registro a imprimir
     */
    public function ImprimirNoConformidad($idnoconformidad){

        // obtenemos los datos de la no conformidad
        $this->getDatosConformidad($idnoconformidad);

        // instanciamos la clase pdf
        $this->Documento = new tFPDF();

        // establecemos las propiedades del documento
        $this->Documento->SetAuthor("Claudio Invernizzi");
        $this->Documento->SetCreator("INP - Mario Fatala Chaben");
        $this->Documento->SetSubject("No Conformidad", true);
        $this->Documento->SetTitle("No Conformidad", true);
        $this->Documento->SetAutoPageBreak(true, 10);

        // agrega una fuente unicode
        $this->Documento->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->Documento->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // fijamos el margen izquierdo y derecho
        $this->Documento->SetLeftMargin(15);
        $this->Documento->setRightMargin(15);

        // agregamos la página
        $this->Documento->AddPage("P", "A4");

        // llamamos al logo
        $this->Encabezado();

        // generamos el documento
        $this->generarDocumento();

        // lo guardamos
        $this->Documento->Output("../temp/noconformidad.pdf", 'F');

    }

    /**
     * Método que sobrecarga el encabezado del documento (el cual es una
     * clase abstracta) con los datos del logo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function Encabezado(){

        // si existe la imagen
        if (!empty($this->Logo)){

            // determinamos el tipo de imagen
            $tipo = $this->tipoImagen($this->Logo);

            // agregamos la imagen indicándole el tamaño y la extensión
            $this->Documento->Image($this->Logo, 150, 5, 40, 40, $tipo);

        }

        // definimos la fuente
        $this->Documento->setFont("DejaVu", "B", 12);

        // presenta el nombre del laboratorio
        $this->Documento->MultiCell(100, 8, $this->Laboratorio, 0, "C");

        // insertamos un separador
        $this->Documento->Ln(20);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param [int] id - clave del registro
     * Método que asigna en las variables de clase los datos de la
     * no conformidad
     */
    protected function getDatosConformidad($id){

        // inicializamos las variables
        $nombre = "";
        $laboratorio = "";
        $jefe = "";
        $fecha = "";
        $titulo = "";
        $detalle = "";
        $causa = "";
        $respuesta = "";
        $accion = "";
        $ejecucion = "";
        $logo = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_conformidades.nombre AS nombre,
                            diagnostico.v_conformidades.laboratorio AS laboratorio,
                            diagnostico.v_conformidades.jefelaboratorio AS jefe,
                            diagnostico.v_conformidades.fecha AS fecha,
                            diagnostico.v_conformidades.titulo AS titulo,
                            diagnostico.v_conformidades.detalle AS detalle,
                            diagnostico.v_conformidades.causa AS causa,
                            diagnostico.v_conformidades.respuesta AS respuesta,
                            diagnostico.v_conformidades.accion AS accion,
                            diagnostico.v_conformidades.ejecucion AS ejecucion,
                            diagnostico.v_conformidades.logo AS logo
                     FROM diagnostico.v_conformidades
                     WHERE diagnostico.v_conformidades.id = '$id';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // lo pasamos a variables
        extract($fila);

        // asignamos en las variables de clase
        $this->Nombre = $nombre;
        $this->Laboratorio = $laboratorio;
        $this->Jefe = $jefe;
        $this->Fecha = $fecha;
        $this->Titulo = $titulo;
        $this->Detalle = $detalle;
        $this->Causa = $causa;
        $this->Respuesta = $respuesta;
        $this->Accion = $accion;
        $this->Ejecucion = $ejecucion;
        $this->Logo = $logo;

    }

    /**
     * Método que a partir de las variables de clase genera el
     * documento
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function generarDocumento(){

        // fijamos la fuente
        $this->Documento->setFont("DejaVu", "B", 12);

        // presentamos el título del reporte
        $this->Documento->MultiCell(100,8,"Registro de no Conformidad", 0, "C");

        // fijamos la fuente
        $this->Documento->setFont("DejaVu", "", 12);

        // presentamos el nombre del usuario
        $texto = "Generado por: " . $this->Nombre;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // presentamos el jefe del laboratorio
        $texto = "Jefe Laboratorio: " . $this->Jefe;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // presentamos la fecha de la no conformidad
        $texto = "Fecha de Ocurrencia: " . $this->Fecha;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // presentamos el título
        $texto = "Descripción Breve: " . $this->Titulo;
        $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        // presenta el detalle
        $this->Documento->Ln(8);
        $this->Documento->setFont("DejaVu", "B", 12);
        $this->Documento->MultiCell(100, 8, "Descripción de la No Conformidad", 0, "L");
        $this->Documento->setFont("DejaVu", "", 12);
        $this->Documento->MultiCell(100, 8, strip_tags($this->Detalle), 0, "J");

        // si se ingresó la causa
        if (!empty($this->Causa)){

            // presenta la causa
            $this->Documento->Ln(8);
            $this->Documento->setFont("DejaVu", "B", 12);
            $this->Documento->MultiCell(100, 8, "Causa de la No Conformidad", 0, "L");
            $this->Documento->setFont("DejaVu", "", 12);
            $this->Documento->MultiCell(100, 8, strip_tags($this->Causa), 0, "J");

        }

        // si existe una fecha de respuesta
        if (!empty($this->Respuesta)){

            // presenta la fecha de respuesta
            $texto = "Fecha Respuesta: " . $this->Respuesta;
            $this->Documento->MultiCell(100, 8, $texto, 0, "L");
        }

        // si se declaró acción correctiva
        if (!empty($this->Accion)){

            // presenta la acción correctiva
            $this->Documento->Ln(8);
            $this->Documento->setFont("DejaVu", "B", 12);
            $this->Documento->MultiCell(100, 8, "Acción Correctiva", 0, "L");
            $this->Documento->setFont("DejaVu", "", 12);
            $this->Documento->MultiCell(100, 8, strip_tags($this->Accion), 0, "J");

        }

        // si hay una fecha de ejecución
        if (!empty($this->Ejecucion)){

            // presenta la fecha de ejecución
            $texto = "Fecha Ejecución de la Acción Correctiva: " . $this->Ejecucion;
            $this->Documento->MultiCell(100, 8, $texto, 0, "L");

        }

    }

    /**
     * Método que a partir de la cadena que recibe, retorna el tipo de
     * archivo de imagen (lo guardamos logos e imágenes como base 64
     * en la base)
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return [string] - extensión de la imagen
     */
    protected function tipoImagen($archivo){

        // si es jpeg
        if (stripos($archivo, "data:image/jpeg;") !== false){
            $tipo = "jpeg";
        // si es gif
        } elseif (stripos($archivo, "data:image/gif;") !== false){
            $tipo = "gif";
        // si es png
        } if (stripos($archivo, "data:image/png;") !== false){
            $tipo = "png";
        // si es jpg
        } elseif (stripos($archivo, "data:image/jpg;") !== false){
            $tipo = "jpg";
        }

        // retornamos el tipo
        return $tipo;

    }

}