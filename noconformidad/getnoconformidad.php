<?php

/**
 *
 * noconformidad/getnoconformidad.php
 *
 * @package     Diagnostico
 * @subpackage  NoConformidad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una no conformidad y
 * retorna un array json con los datos del registro
 *
*/

// incluimos e instanciamos las clases
require_once("noconformidad.class.php");
$noconformidad = new NoConformidad();

// obtenemos el registro
$noconformidad->getDatosConformidad($_GET["Id"]);

// retornamos el array
echo json_encode(array("Id" =>        $noconformidad->getId(),
                       "Usuario" =>   $noconformidad->getUsuario(),
                       "Titulo" =>    $noconformidad->getTitulo(),
                       "Fecha" =>     $noconformidad->getFecha(),
                       "Detalle" =>   $noconformidad->getDetalle(),
                       "Causa" =>     $noconformidad->getCausa(),
                       "Respuesta" => $noconformidad->getRespuesta(),
                       "Accion" =>    $noconformidad->getAccion(),
                       "Ejecucion" => $noconformidad->getEjecucion(),
                       "Jefe" =>      $noconformidad->getJefe()));

?>