/*
 * Nombre: noconformidad.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 04/04/2018
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema para completar las no conformidades
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las no conformidades
 */
class NoConformidad {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializa las variables de clase
        this.initConformidad();

        // el layer con la grilla y el del formulario
        this.layerGrillaConformidades = "";
        this.layerFormConformidades = "";

        // el layer de los documentos pdf
        this.layerImpresion = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initConformidad(){

        // inicializamos las variables
        this.IdConformidad = 0;
        this.IdUsuario = 0;
        this.Usuario = "";
        this.Nombre = "";
        this.IdLaboratorio = sessionStorage.getItem("IdLaboratorio");
        this.Laboratorio = sessionStorage.getItem("Laboratorio");
        this.JefeLaboratorio = "";
        this.Fecha = "";
        this.Titulo = "";
        this.Detalle = "";
        this.Causa = "";
        this.Respuesta = "";
        this.Accion = "";
        this.Ejecucion = "";

    }

    // Métodos de asignación de valores
    setIdConformidad(idconformidad){
        this.IdConformidad = idconformidad;
    }
    setIdUsuario(idusuario){
        this.IdUsuario = idusuario;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setNombre(nombre){
        this.Nombre = nombre;
    }
    setJefe(jefe){
        this.JefeLaboratorio = jefe;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setTitulo(titulo){
        this.Titulo = titulo;
    }
    setDetalle(detalle){
        this.Detalle = detalle;
    }
    setCausa(causa){
        this.Causa = causa;
    }
    setRespuesta(respuesta){
        this.Respuesta = respuesta;
    }
    setAccion(accion){
        this.Accion = accion;
    }
    setEjecuion(ejecucion){
        this.Ejecucion = ejecucion;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que muestra el layer con la grilla de no conformidades
     */
    verConformidad(){

        // reiniciamos la sesion
        sesion.reiniciar();

        // cargamos la grilla en un layer emergente
        this.layerGrillaConformidades = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'No conformidades declaradas',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'noconformidad/grilla.php',
                        reload: 'strict'
                    }
        });
        this.layerGrillaConformidades.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idconformidad clave del registro
     * Método llamado al pulsar sobre la grilla de no conformidades
     * si no recibe ningún parámetro asume que se trata de un alta
     */
    formConformidad(idconformidad){

        // reiniciamos la sesion
        sesion.reiniciar();

        // cargamos el formulario en el layer
        this.layerFormConformidades = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Declaración de No Conformidad',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    ajax: {
                        url: 'noconformidad/noconformidad.html',
                        reload: 'strict'
                    }
        });
        this.layerFormConformidades.open();

        // si recibió la id
        if (typeof(idconformidad) != "undefined"){

            // carga el registro
            this.getDatosConformidad(idconformidad);

        // si está dando un alta
        } else {

            // nos aseguramos que las variables no tengan basura
            this.initConformidad();

        }

        // destruimos la grilla de no conformidades
        this.layerGrillaConformidades.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idconformidad
     * Método que recibe como parámetro la id de un registro y obtiene
     * los datos del mismo y los asigna a las variables de clase
     */
    getDatosConformidad(idconformidad){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la rutina php para obtener los datos del laboratorio
        $.get('noconformidad/getnoconformidad.php', 'Id='+idconformidad,
            function(data){

                // asignamos los valores en las variables de clase
                noconformidad.setIdConformidad(data.Id);
                noconformidad.setUsuario(data.Usuario);
                noconformidad.setTitulo(data.Titulo);
                noconformidad.setFecha(data.Fecha);
                noconformidad.setDetalle(data.Detalle);
                noconformidad.setCausa(data.Causa);
                noconformidad.setRespuesta(data.Respuesta);
                noconformidad.setAccion(data.Accion);
                noconformidad.setEjecuion(data.Ejecucion);
                noconformidad.setJefe(data.Jefe);

                // mostramos el registro
                noconformidad.muestraConformidad();

            }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase, carga los valores en
     * el formulario de no conformidad
     */
    muestraConformidad(){

        // cargamos los valores en el formulario
        document.getElementById("id_noconformidad").value = this.IdConformidad;
        document.getElementById("usuario_noconformidad").value = this.Usuario;
        document.getElementById("titulo_noconformidad").value = this.Titulo;
        document.getElementById("responsable_noconformidad").value = this.JefeLaboratorio;
        document.getElementById("fecha_noconformidad").value = this.Fecha;
        document.getElementById("detalle_noconformidad").value = this.Detalle;
        CKEDITOR.instances['detalle_noconformidad'].setData(this.Detalle);
        document.getElementById("respuesta_noconformidad").value = this.Respuesta;
        document.getElementById("causa_noconformidad").value = this.Causa;
        CKEDITOR.instances['causa_noconformidad'].setData(this.Causa);
        document.getElementById("ejecucion_noconformidad").value = this.Ejecucion;
        document.getElementById("correccion_noconformidad").value = this.Accion;
        CKEDITOR.instances['correccion_noconformidad'].setData(this.Accion);

        // fijamos el foco
        document.getElementById("titulo_noconformidad").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que cierra el layer del formulario de conformidades
     */
    cierraEditor(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // destruimos el layer
        this.layerFormConformidades.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos antes de enviarlo por ajax
     * al servidor
     */
    verificaConformidad(){

        // reiniciamos la sesion
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si está editando
        if (document.getElementById("id_noconformidad") != ""){
            this.IdConformidad = document.getElementById("id_noconformidad").value;
        }

        // si no ingresó el título
        if (document.getElementById("titulo_noconformidad").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe indicar el título de la no conformidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("titulo_noconformidad").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Titulo = document.getElementById("titulo_noconformidad").value;

        }

        // si no ingresó la fecha de la incidencia
        if (document.getElementById("fecha_noconformidad").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la fecha del evento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_noconformidad").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Fecha = document.getElementById("fecha_noconformidad").value;

        }

        // si no ingresó el detalle
        if (CKEDITOR.instances['detalle_noconformidad'].getData() == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la descripción del evento";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.Detalle = CKEDITOR.instances['detalle_noconformidad'].getData();

        }

        // el resto de los elementos los permite en blanco
        this.Respuesta = document.getElementById("respuesta_noconformidad").value;
        this.Causa = CKEDITOR.instances['causa_noconformidad'].getData();
        this.Ejecucion = document.getElementById("ejecucion_noconformidad").value;
        this.Accion = CKEDITOR.instances['correccion_noconformidad'].getData();

        // grabamos el registro
        this.grabaNoConformidad();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos por ajax al servidor
     */
    grabaNoConformidad(){

        // declaración de variables
        var datosConformidad = new FormData();

        // si está editando
        if (this.IdConformidad != 0){
            datosConformidad.append("Id", this.IdConformidad);
        }

        // agregamos el resto de los elementos
        datosConformidad.append("Titulo", this.Titulo);
        datosConformidad.append("Fecha", this.Fecha);
        datosConformidad.append("Detalle", this.Detalle);
        datosConformidad.append("Causa", this.Causa);
        datosConformidad.append("Respuesta", this.Respuesta);
        datosConformidad.append("Accion", this.Accion);
        datosConformidad.append("Ejecucion", this.Ejecucion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "noconformidad/graba_conformidad.php",
            type: "POST",
            data: datosConformidad,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salió todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // almacena la id
                    document.getElementById("id_noconformidad").value = data.Id;

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime el registro activo
     */
    Imprimir(){

        // obtenemos la id de la no conformidad
        var id = document.getElementById("id_noconformidad").value;

        // verificamos que exista un registro
        if (id == ""){

            // simplemente retornamos
            return false;

        }

        // ahora llamamos el layer emergente que va a
        // presentar el documento pdf
        this.layerImpresion = new jBox('Modal', {
                                  animation: 'flip',
                                  closeOnEsc: true,
                                  closeOnClick: false,
                                  closeOnMouseleave: false,
                                  closeButton: true,
                                  repositionOnContent: true,
                                  overlay: false,
                                  title: 'No Conformidad',
                                  draggable: 'title',
                                  theme: 'TooltipBorder',
                                  onCloseComplete: function(){
                                    this.destroy();
                                  },
                                  ajax: {
                                      url: 'noconformidad/impresion.php?id='+id,
                                      reload: 'strict'
                                  }
                            });
        this.layerImpresion.open();

    }

}