<?php

/**
 *
 * noconformidad/impresion.php
 *
 * @package     Diagnostico
 * @subpackage  NoConformidad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que genera el documento PDF de no conformidad y lo
 * presenta en pantalla, recibe como parámetro la id del
 * registro de no conformidad
 *
*/

// incluimos e instanciamos la clase
require_once("imprimir.class.php");
$documento = new Imprimir();

// generamos el documento
$documento->ImprimirNoConformidad($_GET["id"]);

// enviamos los encabezados
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0, false");
header("Pragma: no-cache");
?>

<!-- cargamos el documento -->
<object data="temp/noconformidad.pdf"
        type="application/pdf"
        width="850" height="500">
</object>