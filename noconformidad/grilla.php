<?php

/**
 *
 * noconformidad/grilla.php
 *
 * @package     Diagnostico
 * @subpackage  NoConformidad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta la grilla con las no conformidades ingresadas
 *
*/

// incluimos e instanciamos las clases
require_once ("noconformidad.class.php");
$noconformidad = new NoConformidad();

// obtenemos la nómina
$nomina = $noconformidad->nominaConformidades();

// definimos la tabla
echo "<table id='noconformidades' width='800px' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th align='left'>Descripción</th>";
echo "<th>";
echo "<input type='button'
             name='btnNuevaConformidad'
             id='btnNuevaConformidad'
             class='botonagregar'
             title='Agrega una nueva no conformidad'
             onClick='noconformidad.formConformidad()'>";
echo "</th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>$usuario</td>";
    echo "<td>$fecha</td>";
    echo "<td>$titulo</td>";

    // presentamos el botón editar
    echo "<td>";
    echo "<input type='button'
                 name='btnEditaConformidad'
                 id='btnEditaConformidad'
                 class='botoneditar'
                 title='Edita el registro de no conformidad'
                 onClick='noconformidad.formConformidad($idconformidad)'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// cerramos la tabla
echo "</tbody>";
echo "</table>";

// define el div para el paginador
echo "<div class='paging'></div>";

?>
<SCRIPT>

    // definimos las propiedades de la tabla
    $('#noconformidades').datatable({
        pageSize: 15,
        sort: [true, true, true, false],
        filters: [true, true, true, false],
        filterText: 'Buscar ... '
    });

</SCRIPT>