<?php

/**
 *
 * Class NoConformidad | noconformidad/noconformidad.class.php
 *
 * @package     Diagnostico
 * @subpackage  NoConformidad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (04/04/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla
 * de no conformidades
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class NoConformidad{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $IdLaboratorio;         // laboratorio del usuario
    protected $IdDepartamento;        // departamento del usuario
    protected $Usuario;               // nombre del usuario
    protected $IdConformidad;         // clave del registro
    protected $Titulo;                // título de la no conformidad
    protected $Fecha;                 // fecha de denuncia de la no conformidad
    protected $Detalle;               // descripción de la no conformidad
    protected $Causa;                 // causa de la no conformidad
    protected $Respuesta;             // fecha de respuesta
    protected $Accion;                // descripción de la acción correctiva
    protected $Ejecucion;             // fecha de ejecución de la acción
    protected $Nombre;                // nombre completo del usuario
    protected $Jefe;                  // nombre del jefe del laboratorio
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Usuario = "";
        $this->IdConformidad = 0;
        $this->Titulo = "";
        $this->Fecha = "";
        $this->Detalle = "";
        $this->Causa = "";
        $this->Respuesta = "";
        $this->Accion = "";
        $this->Ejecucion = "";
        $this->Nombre = "";
        $this->Jefe = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];
            $this->IdDepartamento = $_SESSION["IdDepartamento"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Dstructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // Métodos de asignación de valores
    public function setIdConformidad($id){
        $this->IdConformidad = $id;
    }
    public function setTitulo($titulo){
        $this->Titulo = $titulo;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }
    public function setDetalle($detalle){
        $this->Detalle = $detalle;
    }
    public function setCausa($causa){
        $this->Causa = $causa;
    }
    public function setRespuesta($respuesta){
        $this->Respuesta = $respuesta;
    }
    public function setAccion($accion){
        $this->Accion = $accion;
    }
    public function setEjecucion($ejecucion){
        $this->Ejecucion = $ejecucion;
    }

    // Métodos públicos de retorno de valores
    public function getId(){
        return $this->IdConformidad;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getTitulo(){
        return $this->Titulo;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getDetalle(){
        return $this->Detalle;
    }
    public function getCausa(){
        return $this->Causa;
    }
    public function getRespuesta(){
        return $this->Respuesta;
    }
    public function getAccion(){
        return $this->Accion;
    }
    public function getEjecucion(){
        return $this->Ejecucion;
    }
    public function getNombre(){
        return $this->Nombre;
    }
    public function getJefe(){
        return $this->Jefe;
    }

    /**
     * Método que obtiene la nomina de no conformidades y las
     * retorna como un array asociativo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaConformidades(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT diagnostico.v_conformidades.id AS idconformidad,
                            diagnostico.v_conformidades.usuario AS usuario,
                            diagnostico.v_conformidades.fecha AS fecha,
                            diagnostico.v_conformidades.titulo AS titulo
                     FROM diagnostico.v_conformidades
                     WHERE diagnostico.v_conformidades.idlaboratorio = '$this->IdLaboratorio' AND
                           diagnostico.v_conformidades.iddepartamento = '$this->IdDepartamento'
                     ORDER BY STR_TO_DATE(diagnostico.v_conformidades.fecha, '%d/%m/%Y') DESC;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método público que ejecuta la consulta de edición o inserción según
     * corresponda, retorna la id del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idconformidad - clave del registro insertado / editado
     */
    public function grabaConformidad(){

        // si está insertando
        if ($this->IdConformidad == 0){
            $this->nuevaConformidad();
        } else {
            $this->editaConformidad();
        }

        // retornamos el registro
        return $this->IdConformidad;

    }

    /**
     * Método protegido que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaConformidad(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.noconformidad
                            (usuario,
                             titulo,
                             fecha,
                             detalle,
                             causa,
                             respuesta,
                             accion,
                             ejecucion)
                            VALUES
                            (:idusuario,
                             :titulo,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :detalle,
                             :causa,
                             STR_TO_DATE(:respuesta, '%d/%m/%Y'),
                             :accion,
                             STR_TO_DATE(:ejecucion, '%d/%m/%Y')); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":titulo", $this->Titulo);
        $psInsertar->bindParam(":fecha", $this->Fecha);
        $psInsertar->bindParam(":detalle", $this->Detalle);
        $psInsertar->bindParam(":causa", $this->Causa);
        $psInsertar->bindParam(":respuesta", $this->Respuesta);
        $psInsertar->bindParam(":accion", $this->Accion);
        $psInsertar->bindParam(":ejecucion", $this->Ejecucion);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la id del registro insertado
        $this->IdConformidad = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaConformidad(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.noconformidad SET
                            usuario = :idusuario,
                            titulo = :titulo,
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            detalle = :detalle,
                            causa = :causa,
                            respuesta = STR_TO_DATE(:respuesta, '%d/%m/%Y'),
                            accion = :accion,
                            ejecucion = STR_TO_DATE(:ejecucion, '%d/%m/%Y')
                     WHERE diagnostico.noconformidad.id = :idconformidad; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":titulo", $this->Titulo);
        $psInsertar->bindParam(":fecha", $this->Fecha);
        $psInsertar->bindParam(":detalle", $this->Detalle);
        $psInsertar->bindParam(":causa", $this->Causa);
        $psInsertar->bindParam(":respuesta", $this->Respuesta);
        $psInsertar->bindParam(":accion", $this->Accion);
        $psInsertar->bindParam(":ejecucion", $this->Ejecucion);
        $psInsertar->bindParam(":idconformidad", $this->IdConformidad);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método público que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $clave - clave del registro a obtener
     */
    public function getDatosConformidad($clave){

        // inicializamos las variables
        $idconformidad = 0;
        $usuario = "";
        $nombre = "";
        $jefe = "";
        $fecha = "";
        $titulo = "";
        $detalle = "";
        $causa = "";
        $respuesta = "";
        $accion = "";
        $ejecucion = "";

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_conformidades.id AS idconformidad,
                            diagnostico.v_conformidades.usuario AS usuario,
                            diagnostico.v_conformidades.nombre AS nombre,
                            diagnostico.v_conformidades.jefelaboratorio AS jefe,
                            diagnostico.v_conformidades.fecha AS fecha,
                            diagnostico.v_conformidades.titulo AS titulo,
                            diagnostico.v_conformidades.detalle AS detalle,
                            diagnostico.v_conformidades.causa AS causa,
                            diagnostico.v_conformidades.respuesta AS respuesta,
                            diagnostico.v_conformidades.accion AS accion,
                            diagnostico.v_conformidades.ejecucion AS ejecucion
                     FROM diagnostico.v_conformidades
                     WHERE diagnostico.v_conformidades.id = '$clave';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro
        extract($registro);

        // asignamos en las variables de clase
        $this->IdConformidad = $idconformidad;
        $this->Usuario = $usuario;
        $this->Nombre = $nombre;
        $this->Jefe = $jefe;
        $this->Fecha = $fecha;
        $this->Titulo = $titulo;
        $this->Detalle = $detalle;
        $this->Causa = $causa;
        $this->Respuesta = $respuesta;
        $this->Accion = $accion;
        $this->Ejecucion = $ejecucion;

    }

}