/*
 * Nombre: estadosciviles.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: /
 * Licencia/: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad en el abm de estados
 *              civiles
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre los estados civiles
 */
class EstadosCiviles {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables de clase
        this.initEstados();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initEstados(){

        // inicialización de variables
        this.IdEstado = 0;
        this.EstadoCivil = "";
        this.IdUsuario = 0;
        this.Usuario = "";
        this.FechaAlta = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de estados civiles
     */
    muestraEstados(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("estados/form_estados.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idestado - clave del registro a verificar
     * Método que verifica los datos del formulario de estados civiles
     * si no recibe la clave del estado asume que es un alta
     */
    verificaEstado(idestado){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió el estado
        if(typeof(idestado) == "undefined"){
            idestado = "nuevo";
        } else {
            this.IdEstado = idestado;
        }

        // verifica que halla ingresado la descripción
        if (document.getElementById("estado_" + idestado).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el estado civil";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("estado_" + idestado).focus();
            return false;

        // si ingresó
        } else {

            // asigna en la variable de clase
            this.EstadoCivil = document.getElementById("estado_" + idestado).value;

        }

        // grabamos el registro
        this.grabaEstados();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario, ejecuta la
     * consulta de actualización en el servidor y recarga el
     * formulario
     */
    grabaEstados(){

        // definimos el formulario
        var datosEstado = new FormData();

        // si está editando
        if (this.IdEstado != 0){
            datosEstado.append("Id", this.IdEstado);
        }

        // agregamos el estado civil
        datosEstado.append("Estado", this.EstadoCivil);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "estados/graba_estado.php",
            type: "POST",
            data: datosEstado,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    estados.initEstados();

                    // recarga el formulario para reflejar los cambios
                    estados.muestraEstados();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id del objeto html
     * @param {int} idestado - clave del registro preseleccionado
     * Método que recibe como parámetros la id de un elemento del formulario
     * y carga en ese elemento la nómina de estados civiles, si recibe la
     * clave del estado, lo predetermina
     */
    nominaEstados(idelemento, idestado){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "estados/lista_estados.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idestado) != "undefined"){

                        // si coincide
                        if (data[i].Id == idestado){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Estado + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Estado + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Estado + "</option>");
                    }

                }

        }});

    }

}
