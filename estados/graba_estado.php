<?php

/**
 *
 * estados/graba_estado.php
 *
 * @package     Diagnostico
 * @subpackage  EstadosCiviles
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de un estado civil 
 * ejecuta la consulta en el servidor y retorna el resultado
 * de la operación 
 * 
*/

// incluimos e instanciamos las clases
require_once("estados_civiles.class.php");
$estado = new EstadosCiviles();

// si está editando
if (!empty($_POST["Id"])){
    $estado->setIdEstado($_POST["Id"]);
}

// fija el nombre
$estado->setEstadoCivil($_POST["Estado"]);

// grabamos y obtenemos el resultado
$error = $estado->grabaEstado();

// retornamos el resultado
echo json_encode(array("Error" => $error));

?>