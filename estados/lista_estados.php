<?php

/**
 *
 * estados/lista_estados.class.php
 *
 * @package     Diagnostico
 * @subpackage  EstadosCiviles
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/02/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de los 
 * estados civiles
 * 
*/

// incluimos e instanciamos la clase
require_once("estados_civiles.class.php");
$estados = new EstadosCiviles();

// obtenemos la nómina
$nomina = $estados->nominaEstados();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_estado,
                        "Estado" => $estado_civil);

}

// retornamos el vector
echo json_encode($jsondata);

?>