<?php

/**
 *
 * Class EstadosCiviles | estados/estados_civiles.class.php
 *
 * @package     Diagnostico
 * @subpackage  EstadosCiviles
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre el diccionario de
 * estados civiles
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class EstadosCiviles{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $IdEstado;              // clave del estado civil
    protected $EstadoCivil;           // nombre del estado civil
    protected $FechaAlta;             // fecha de alta
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Usuario = "";
        $this->FechaAlta = "";
        $this->IdEstado = 0;
        $this->EstadoCivil = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdEstado($idestado){
        $this->IdEstado = $idestado;
    }
    public function setEstadoCivil($estado){
        $this->EstadoCivil = $estado;
    }

    // métodos de retorno de valores
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getIdEstado(){
        return $this->IdEstado;
    }
    public function getEstadoCivil(){
        return $this->EstadoCivil;
    }

    /**
     * Método que retorna un array con la nómina completa de
     * los estados civiles
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaEstados(){

        // componemos la consulta
        $consulta = "SELECT diccionarios.estado_civil.id_estado AS id_estado,
                            diccionarios.estado_civil.estado_civil AS estado_civil,
                            cce.responsables.usuario AS usuario,
                            diccionarios.estado_civil.id_usuario AS id_usuario,
                            DATE_FORMAT(diccionarios.estado_civil.fecha, '%d/%m/%Y') AS fecha_alta
                     FROM diccionarios.estado_civil INNER JOIN cce.responsables ON diccionarios.estado_civil.id_usuario = cce.responsables.id
                     ORDER BY diccionarios.estado_civil.estado_civil;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que ejecuta la consulta de actualización o edición
     * según corresponda, retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / editado
     */
    public function grabaEstado(){

        // si está insertando
        if ($this->IdEstado == 0){
            $this->nuevoEstado();
        } else {
            $this->editaEstado();
        }

        // retorna la clave
        return $this->IdEstado;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoEstado(){

        // compone la consulta
        $consulta = "INSERT INTO diccionarios.estado_civil
                            (estado_civil,
                             id_usuario)
                            VALUES
                            (:estado_civil,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":estado_civil", $this->EstadoCivil);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asignamos la clave
        $this->IdEstado = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaEstado(){

        // componemos la consulta
        $consulta = "UPDATE diccionarios.estado_civil SET
                            estado_civil = :estado_civil,
                            id_usuario = :id_usuario
                     WHERE diccionarios.estado_civil.id_estado = :id_estado; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":estado_civil", $this->EstadoCivil);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_estado", $this->IdEstado);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}