<?php

/**
 *
 * estados/form_estados.php
 *
 * @package     Diagnostico
 * @subpackage  EstadosCiviles
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario para el abm de estados civiles
 * 
*/

// incluimos e instanciamos las clases
require_once("estados_civiles.class.php");
$estado = new EstadosCiviles();

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["ID"])){
    $usuario_actual = $_SESSION["Usuario"];
}
session_write_close();

// presentamos el título
echo "<h2>Nómina de Estados Civiles</h2>";

// definimos la tabla
echo "<table width='60%' align='center' border'0'>";

// definimos el header
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Estado Civil</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// obtenemos la nómina de estados civiles
$nomina = $estado->nominaEstados();

// recorremos el vector
foreach ($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el estado civil
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre completo del estado'>";
    echo "<input type='text'
                 size='30'
                 name='estado_$id_estado'
                 id='estado_$id_estado'
                 value='$estado_civil'>";
    echo "</span>";
    echo "</td>";

    // presenta la fecha de alta y el usuario
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // arma el enlace
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaEstado'
           id='btnGrabaEstado'
           title='Pulse para grabar el registro'
           onClick='estados.verificaEstado($id_estado)'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// presentamos la fila para el alta
echo "<tr>";

// presentamos el estado civil
echo "<td>";
echo "<span class='tooltip'
            title='Nombre completo del estado'>";
echo "<input type='text'
                size='30'
                name='estado_nuevo'
                id='estado_nuevo'>";
echo "</span>";
echo "</td>";

// obtenemos la fecha actual
$fecha_alta = date('d/m/Y');

// presenta la fecha de alta y el usuario
echo "<td align='center'>$usuario_actual</td>";
echo "<td align='center'>$fecha_alta</td>";

// arma el enlace
echo "<td align='center'>";
echo "<input type='button'
        name='btnGrabaEstado'
        id='btnGrabaEstado'
        title='Pulse para grabar el registro'
        onClick='estados.verificaEstado()'
        class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos el cuerpo y la tabla
echo "</body>";
echo "</table>";

?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>