<?php

/**
 *
 * Class Rx | rx/rx.class.php
 *
 * @package     Diagnostico
 * @subpackage  Rx
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * radiografías del paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Rx {

    // declaración de variables de clase
    protected $Link;               // puntero a la base de datos
    protected $Id;                 // clave del registro
    protected $Paciente;           // clave del paciente
    protected $Visita;             // clave de la visita
    protected $Fecha;              // fecha de toma
    protected $Ict;                // 0 no 1 si
    protected $Cardiomegalia;      // 0 no 1 si
    protected $Pleuro;             // 0 no 1 si
    protected $Epoc;               // 0 no 1 si
    protected $Calcificaciones;    // 0 no 1 si
    protected $NoTiene;            // 0 no 1 si
    protected $IdUsuario;          // clave del usuario
    protected $Usuario;            // nombre del usuario
    protected $FechaAlta;          // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initRadiografias();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase desde
     * el constructor o llamado cuando no encontró registros
     */
    protected function initRadiografias(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Visita = 0;
        $this->Fecha = "";
        $this->Ict = 0;
        $this->Cardiomegalia = 0;
        $this->Pleuro = 0;
        $this->Epoc = 0;
        $this->Calcificaciones = 0;
        $this->NoTiene = 0;
        $this->Usuario = "";
        $this->FechaAlta = "";

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }
    public function setIct($ict){
        $this->Ict = $ict;
    }
    public function setCardiomegalia($cardiomegalia){
        $this->Cardiomegalia = $cardiomegalia;
    }
    public function setPleuro($pleuro){
        $this->Pleuro = $pleuro;
    }
    public function setEpoc($epoc){
        $this->Epoc = $epoc;
    }
    public function setCalcificaciones($calcificaciones){
        $this->Calcificaciones = $calcificaciones;
    }
    public function setNoTiene($notiene){
        $this->NoTiene = $notiene;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getIct(){
        return $this->Ict;
    }
    public function getCardiomegalia(){
        return $this->Cardiomegalia;
    }
    public function getPleuro(){
        return $this->Pleuro;
    }
    public function getEpoc(){
        return $this->Epoc;
    }
    public function getCalcificaciones(){
        return $this->Calcificaciones;
    }
    public function getNotiene(){
        return $this->NoTiene;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave del paciente
     * @return vector con los registros encontrados
     * Método que recibe como parámetro la clave de un
     * paciente y retorna el vector con todos los registros
     * de radiografías de ese paciente
     */
    public function nominaRx($idpaciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_rx.id AS id,
                            diagnostico.v_rx.fecha AS fecha,
                            diagnostico.v_rx.paciente AS paciente,
                            diagnostico.v_rx.idvisita AS visita,
                            diagnostico.v_rx.ict AS ict,
                            diagnostico.v_rx.cardiomegalia AS cardiomegalia,
                            diagnostico.v_rx.pleuro AS pleuro,
                            diagnostico.v_rx.epoc AS epoc,
                            diagnostico.v_rx.calcificaciones AS calcificaciones,
                            diagnostico.v_rx.notiene AS notiene,
                            diagnostico.v_rx.usuario AS usuario,
                            diagnostico.v_rx.fecha_alta AS fechaalta
                     FROM diagnostico.v_rx
                     WHERE diagnostico.v_rx.paciente = '$idpaciente'
                     ORDER BY STR_TO_DATE(diagnostico.v_rx.fecha_alta, '%d/%m/%Y') DESC; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita entero con la clave de la visita
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los datos del mismo
     */
    public function getDatosRx($idvisita){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_rx.id AS id,
                            diagnostico.v_rx.paciente AS paciente,
                            diagnostico.v_rx.idvisita AS visita,
                            diagnostico.v_rx.fecha AS fecha,
                            diagnostico.v_rx.ict AS ict,
                            diagnostico.v_rx.cardiomegalia AS cardiomegalia,
                            diagnostico.v_rx.pleuro AS pleuro,
                            diagnostico.v_rx.epoc AS epoc,
                            diagnostico.v_rx.calcificaciones AS calcificaciones,
                            diagnostico.v_rx.notiene AS notiene,
                            diagnostico.v_rx.usuario AS usuario,
                            diagnostico.v_rx.fecha_alta AS fechaalta
                     FROM diagnostico.v_rx
                     WHERE diagnostico.v_rx.idvisita = '$idvisita'; ";
        $resultado = $this->Link->query($consulta);

        // si hay registros
        if ($resultado->rowCount() != 0) {

            // obtenemos el registro y asignamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Visita = $visita;
            $this->Fecha = $fecha;
            $this->Ict = $ict;
            $this->Cardiomegalia = $cardiomegalia;
            $this->Pleuro = $pleuro;
            $this->Epoc = $epoc;
            $this->Calcificaciones = $calcificaciones;
            $this->NoTiene = $notiene;
            $this->Usuario = $usuario;
            $this->FechaAlta = $fechaalta;

        // si no hay registros
        } else {

            // las inicializamos
            $this->initRadiografias();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id entero con la clave del registro afectado
     * Método que ejectua la consulta de actualización o
     * edición según corresponda
     */
    public function grabaRx(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoRx();
        } else {
            $this->editaRx();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la inserción de un nuevo registro
     */
    public function nuevoRx(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.rx
                            (paciente,
                             visita,
                             fecha,
                             ict,
                             cardiomegalia,
                             pleuro,
                             epoc,
                             calcificaciones,
                             notiene,
                             usuario)
                            VALUES
                            (:paciente,
                             :visita,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :ict,
                             :cardiomegalia,
                             :pleuro,
                             :epoc,
                             :calcificaciones,
                             :notiene,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",    $this->Paciente);
        $psInsertar->bindParam(":visita",      $this->Visita);
        $psInsertar->bindParam(":fecha",       $this->Fecha);
        $psInsertar->bindParam(":ict",         $this->Ict);
        $psInsertar->bindParam(":cardiomegalia",  $this->Cardiomegalia);
        $psInsertar->bindParam(":pleuro",      $this->Pleuro);
        $psInsertar->bindParam(":epoc",        $this->Epoc);
        $psInsertar->bindParam(":calcificaciones", $this->Calcificaciones);
        $psInsertar->bindParam(":notiene",     $this->NoTiene);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la edición del registro
     */
    public function editaRx(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.rx SET
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            ict = :ict,
                            cardiomegalia = :cardiomegalia,
                            pleuro = :pleuro,
                            epoc = :epoc,
                            calcificaciones = :calcificaciones,
                            notiene = :notiene,
                            usuario = :usuario
                     WHERE diagnostico.rx.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":fecha",       $this->Fecha);
        $psInsertar->bindParam(":ict",         $this->Ict);
        $psInsertar->bindParam(":cardiomegalia",  $this->Cardiomegalia);
        $psInsertar->bindParam(":pleuro",      $this->Pleuro);
        $psInsertar->bindParam(":epoc",        $this->Epoc);
        $psInsertar->bindParam(":calcificaciones", $this->Calcificaciones);
        $psInsertar->bindParam(":notiene",     $this->NoTiene);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idrx entero con la clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public function borraRx($idrx){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.rx
                     WHERE diagnostico.rx.id = '$idrx'; ";
        $this->Enlace.exec(Consulta);

    }

}
