/*
 * Nombre: rx.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 11/06/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de las radiografías
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de las radiografías
 */
class Rx {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initRx();

        // inicializamos el layer
        this.layerRx = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initRx(){

        // inicializamos las variables
        this.Id = 0;                        // clave del registro
        this.Paciente = 0;                  // protocolo del paciente
        this.Visita = 0;                    // clave de la visita
        this.Fecha = "";                    // fecha de administración
        this.Ict = 0;                       // 0 no 1 si
        this.Cardiomegalia = 0;             // 0 no 1 si
        this.Pleuro = 0;                    // 0 no 1 si
        this.Epoc = 0;                      // 0 no 1 si
        this.Calcificaciones = 0;           // 0 no 1 si
        this.NoTiene = 0;                   // 0 no 1 si
        this.Usuario = "";                  // nombre del usuario
        this.FechaAlta = "";                // fecha de alta del registro

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setPaciente(paciente){
        this.Paciente = paciente;
    }
    setVisita(visita){
        this.Visita = visita;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setIct(ict){
        this.Ict = ict;
    }
    setCardiomegalia(cardiomegalia){
        this.Cardiomegalia = cardiomegalia;
    }
    setPleuro(pleuro){
        this.Pleuro = pleuro;
    }
    setEpoc(epoc){
        this.Epoc = epoc;
    }
    setCalcificaciones(calcificaciones){
        this.Calcificaciones = calcificaciones;
    }
    setNoTiene(notiene){
        this.NoTiene = notiene;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta en el layer emergente el
     * formulario de las radiografías
     */
    verRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no grabó la visita
        if (visitas.IdVisita == 0){

            // presenta el mensaje
            mensaje = "Debe grabar los datos de la visita primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos la clave
        this.Visita = visitas.IdVisita;
        this.Paciente = document.getElementById("protocolo_paciente").value;

        // abrimos y mostramos el layer
        this.layerRx = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Radiografías',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:700,
                    draggable: 'title',
                    zIndex: 20000,
                    ajax: {
                        url: 'rx/form_rx.html',
                        reload: 'strict'
                    }
            });
        this.layerRx.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de cargar el formulario que
     * obtiene la clave de la visita y obtiene los datos
     * del registro
     */
    getDatosRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "rx/getrx.php?idvisita=" + this.Visita,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                rx.cargaDatosRx(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} - vector con el registro
     * Método que recibe como parámetro el vector con los
     * datos del registro y asigna los valores en las
     * variables de clase
     */
    cargaDatosRx(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // ahora asignamos en las variables de clase
        this.setId(datos.Id);
        this.setPaciente(document.getElementById("protocolo_paciente").value);
        this.setFecha(datos.Fecha);
        this.setIct(datos.Ict);
        this.setCardiomegalia(datos.Cardiomegalia);
        this.setPleuro(datos.Pleuro);
        this.setEpoc(datos.Epoc);
        this.setCalcificaciones(datos.Calcificaciones);
        this.setNoTiene(datos.NoTiene);
        this.setUsuario(datos.Usuario);
        this.setAlta(datos.FechaAlta);

        // mostramos el registro
        this.verDatosRx();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de los valores de las variables
     * de clase, carga el formulario
     */
    verDatosRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en el formulario
        document.getElementById("fecha_rx").value = this.Fecha;

        // según el valor activamos los checkbox
        if (this.Ict == 0){
            document.getElementById("ictconservada").checked = false;
        } else {
            document.getElementById("ictconservada").checked = true;
        }
        if (this.Cardiomegalia == 0){
            document.getElementById("cardiomegalia").checked = false;
        } else {
            document.getElementById("cardiomegalia").checked = true;
        }
        if (this.Pleuro == 0){
            document.getElementById("pleuro").checked = false;
        } else {
            document.getElementById("pleuro").checked = true;
        }
        if (this.Epoc == 0){
            document.getElementById("epoc").checked = false;
        } else {
            document.getElementById("epoc").checked = true;
        }
        if (this.Calcificaciones == 0){
            document.getElementById("calcificaciones").checked = false;
        } else {
            document.getElementById("calcificaciones").checked = true;
        }
        if (this.NoTiene == 0){
            document.getElementById("notienerx").checked = false;
        } else {
            document.getElementById("notienerx").checked = true;
        }

        // carga el usuario y la fecha de alta
        if (this.Id != 0){
            document.getElementById("usuariorx").value = this.Usuario;
            document.getElementById("altarx").value = this.FechaAlta;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que verifica
     * el formulario de datos
     */
    verificaRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;
        var correcto = false;

        // si no ingresó la fecha
        if (document.getElementById("fecha_rx").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de administración";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Fecha = document.getElementById("fecha_rx").value;

        }

        // verifica que al menos halla marcado un checkbox

        // de ict conservada
        if (document.getElementById("ictconservada").checked){
            correcto = true;
            this.Ict = 1;
        } else {
            this.Ict = 0;
        }

        // de cardiomegalia
        if (document.getElementById("cardiomegalia").checked){
            correcto = true;
            this.Cardiomegalia = 1;
        } else {
            this.Cardiomegalia = 0;
        }

        // de pleuro
        if (document.getElementById("pleuro").checked){
            correcto = true;
            this.Pleuro = 1;
        } else {
            this.Pleuro = 0;
        }

        // de epoc
        if (document.getElementById("epoc").checked){
            correcto = true;
            this.Epoc = 1;
        } else {
            this.Epoc = 0;
        }

        // de calcificaciones
        if (document.getElementById("calcificaciones").checked){
            correcto = true;
            this.Calcificaciones = 1;
        } else {
            this.Calcificaciones = 0;

        }

        // si no tiene
        if (document.getElementById("notienerx").checked){
            correcto = true;
            this.NoTiene = 1;
        } else {
            this.NoTiene = 0;
        }

        // si no marcó ninguno
        if (!correcto){

            // presenta el mensaje y retorna
            mensaje = "Debe marcar al menos un elemento<br>";
            mensaje += "(puede marcar No Tiene)";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si llegó hasta aquí grabamos
        this.grabaRx();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta en el servidor
     */
    grabaRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos las variables
        var datosRx = new FormData();

        // asignamos en el formulario
        datosRx.append("Id", this.Id);
        datosRx.append("Paciente", this.Paciente);
        datosRx.append("Visita", this.Visita);
        datosRx.append("Fecha", this.Fecha);
        datosRx.append("Ict", this.Ict);
        datosRx.append("Cardiomegalia", this.Cardiomegalia);
        datosRx.append("Pleuro", this.Pleuro);
        datosRx.append("Epoc", this.Epoc);
        datosRx.append("Calcificaciones", this.Calcificaciones);
        datosRx.append("NoTiene", this.NoTiene);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "rx/graba_rx.php",
            type: "POST",
            data: datosRx,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    rx.setId(data.Id);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * reinicia el formulario o recarga el registro
     * según corresponda
     */
    cancelaRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está editando
        if (this.Rx != 0){
            this.getDatosRx();
        } else {
            this.limpiaRx();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de datos
     */
    limpiaRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializamos el formulario
        document.getElementById("fecha_rx").value = "";
        document.getElementById("ictconservada").checked = false;
        document.getElementById("cardiomegalia").checked = false;
        document.getElementById("pleuro").checked = false;
        document.getElementById("epoc").checked = false;
        document.getElementById("calcificaciones").checked = false;
        document.getElementById("notienerx").checked = false;

        // fijamos la fecha de alta y el usuario
        document.getElementById("altarx").value = fechaActual();
        document.getElementById("usuariorx").value = sessionStorage.getItem("Usuario");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que pide confirmación y luego elimina el
     * registro actual y cierra el layer
     */
    borraRx(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Rx',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('rx/borrarx.php?id='+rx.Id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // cerramos el layer
                            rx.layerRx.destroy();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}