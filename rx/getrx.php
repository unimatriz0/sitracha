<?php

/**
 *
 * getrx | rx/getrx.php
 *
 * @package     Diagnostico
 * @subpackage  Rx
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una visita y retorna 
 * el array json con los datos del registro
 * 
*/

// incluimos e instanciamos la clase
require_once("rx.class.php");
$rx = new Rx();

// obtenemos el registro
$rx->getDatosRx($_GET["idvisita"]);

// retornamos el array json
echo json_encode(array("Id" =>              $rx->getId(),
                       "Fecha" =>           $rx->getFecha(),
                       "Ict" =>             $rx->getIct(),
                       "Cardiomegalia" =>   $rx->getCardiomegalia(),
                       "Pleuro" =>          $rx->getPleuro(),
                       "Epoc" =>            $rx->getEpoc(),
                       "Calcificaciones" => $rx->getCalcificaciones(),
                       "NoTiene" =>         $rx->getNotiene(),
                       "Usuario" =>         $rx->getUsuario(),
                       "FechaAlta" =>       $rx->getFechaAlta()));

?>
