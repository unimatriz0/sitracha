<?php

/**
 *
 * borrarx | rx/borrarx.php
 *
 * @package     Diagnostico
 * @subpackage  Rx
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta 
 * la consulta de eliminación
 * 
*/

// incluimos e instanciamos la clase
require_once("rx.class.php");
$rx = new Rx();

// eliminamos el registro
$rx->borraRx($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Id" => 1));

?>
