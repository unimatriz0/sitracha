<?php

/**
 *
 * graba_rx | rx/graba_rx.php
 *
 * @package     Diagnostico
 * @subpackage  Rx
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (25/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta en el servidor
 * 
*/

// incluimos e instanciamos la clase
require_once("rx.class.php");
$rx = new Rx();

// asignamos los valores
$rx->setId($_POST["Id"]);
$rx->setPaciente($_POST["Paciente"]);
$rx->setVisita($_POST["Visita"]);
$rx->setFecha($_POST["Fecha"]);
$rx->setIct($_POST["Ict"]);
$rx->setCardiomegalia($_POST["Cardiomegalia"]);
$rx->setPleuro($_POST["Pleuro"]);
$rx->setEpoc($_POST["Epoc"]);
$rx->setCalcificaciones($_POST["Calcificaciones"]);
$rx->setNoTiene($_POST["NoTiene"]);

// ejecutamos la consulta
$id = $rx->grabaRx();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>
