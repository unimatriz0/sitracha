<?php

/**
 *
 * temperaturas/borra_lectura.class.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/05/2018)
 * @copyright   Copyright (c) 2017, INP/
 * 
 * Método que recibe por post la clave de una lectura y ejecuta la 
 * consulta de eliminación, retorna el resultado de la operación
 * 
*/

// incluimos e instanciamos la clase
require_once("temperaturas.class.php");
$lectura = new Temperaturas();

// eliminamos el registro
$lectura->borraLectura($_GET["id"]);

// retornamos verdadero
echo json_encode(array("Error" => 1));
?>