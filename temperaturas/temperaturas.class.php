<?php

/**
 *
 * Class Temperaturas | temperaturas/temperaturas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/05/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de
 * temperaturas de las heladeras
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Temperaturas{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Id;                    // clave del registro
    protected $Heladera;              // clave de la heladera
    protected $Fecha;                 // timestamp del registro
    protected $Temperatura;           // temperatura obtenida
    protected $Temperatura2;          // temperatura obtenida
    protected $Controlado;            // nombre del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Id = 0;
        $this->Heladera = 0;
        $this->Temperatura = 0;
        $this->Temperatura2 = 0;
        $this->Controlado = "";

        // la fecha la inicializa por defecto a la fecha actual
        $this->Fecha = date("Y-m-d H:i:s");

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores (nota, la fecha asume
    // que la recibe en formato timestamp)
    public function setId($id){
        $this->Id = $id;
    }
    public function setHeladera($heladera){
        $this->Heladera = $heladera;
    }
    public function setTemperatura($temperatura){
        $this->Temperatura = $temperatura;
    }
    public function setTemperatura2($temperatura){
        $this->Temperatura2 = $temperatura;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getHeladera(){
        return $this->Heladera;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getTemperatura(){
        return $this->Temperatura;
    }
    public function getTemperatura2(){
        return $this->Temperatura2;
    }
    public function getControlado(){
        return $this->Controlado;
    }

    /**
     * Método que recibe como parámetro la id de una heladera y retorna
     * las lecturas de esa heladera ordenadas por fecha en forma
     * descendente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idheladera - clave de la heladera
     * @return array
     */
    public function getLecturas($idheladera){

        // componemos la consulta
        $consulta = "SELECT diagnostico.temperaturas.id AS idlectura,
                            DATE_FORMAT(diagnostico.temperaturas.fecha, '%d/%m/%Y') AS fecha,
                            DATE_FORMAT(diagnostico.temperaturas.fecha, '%H:%i') AS hora,
                            diagnostico.temperaturas.temperatura AS temperatura,
                            diagnostico.temperaturas.temperatura2 AS temperatura2,
                            cce.responsables.usuario AS controlado
                     FROM diagnostico.temperaturas INNER JOIN cce.responsables ON diagnostico.temperaturas.controlado = cce.responsables.id
                     WHERE diagnostico.temperaturas.heladera = '$idheladera'
                     ORDER BY diagnostico.temperaturas.fecha DESC;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que recibe como parámetro la id de una heladera, la
     * fecha de inicio y finalización del reporte  y retorna
     * las lecturas de esa heladera ordenadas por fecha en forma
     * ascendente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idheladera - clave de la heladera
     * @param string $fechainicio - fecha inicial a reportar
     * @param string $fechafinal - fecha final a reportar
     * @return array
     */
    public function reporteLecturas($idheladera, $fechainicio, $fechafinal){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_heladeras.marca AS marca,
                            diagnostico.v_heladeras.ubicacion AS ubicacion,
                            diagnostico.v_heladeras.patrimonio AS patrimonio,
                            diagnostico.v_heladeras.temperatura AS temperatura,
                            diagnostico.v_heladeras.temperatura2 AS temperatura2,
                            diagnostico.v_heladeras.tolerancia AS tolerancia,
                            diagnostico.v_heladeras.fecha_lectura AS fecha,
                            diagnostico.v_heladeras.hora_lectura AS hora,
                            diagnostico.v_heladeras.valor_lectura AS lectura1,
                            diagnostico.v_heladeras.valor_lectura2 AS lectura2,
                            diagnostico.v_heladeras.controlado AS controlado
                     FROM diagnostico.v_heladeras
                     WHERE diagnostico.v_heladeras.id_heladera = '$idheladera' AND
                           STR_TO_DATE(diagnostico.v_heladeras.fecha_lectura, '%d/%m/%Y') >= STR_TO_DATE('$fechainicio', '%d/%m/%Y') AND
                           STR_TO_DATE(diagnostico.v_heladeras.fecha_lectura, '%d/%m/%Y') <= STR_TO_DATE('$fechafinal', '%d/%m/%Y')
                     ORDER BY diagnostico.v_heladeras.fecha_lectura ASC;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método llamado al grabar un registro, ejecuta la consulta de
     * inserción según retorna la clave del registro (no se prevee
     * ningún método de edición de lecturas)
     * afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idregistro - clave del registro insertado / editado
     */
    public function grabaLectura(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.temperaturas
                            (heladera,
                             fecha,
                             temperatura,
                             temperatura2,
                             controlado)
                            VALUES
                            (:heladera,
                             STR_TO_DATE(:fecha, '%d/%m/%Y %H:%i'),
                             :temperatura,
                             :temperatura2,
                             :controlado);";
        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":heladera", $this->Heladera);
        $psInsertar->bindParam(":fecha", $this->Fecha);
        $psInsertar->bindParam(":temperatura", $this->Temperatura);
        $psInsertar->bindParam(":temperatura2", $this->Temperatura2);
        $psInsertar->bindParam(":controlado", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->Id = $this->Link->lastInsertId();

        // retornamos la id del registro
        return $this->Id;

    }

    /**
     * Método público que recibe como parámetro la id de una lectura y
     * ejecuta la consulta de eliminación
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idlectura - clave de la lectura a eliminar
     */
    public function borraLectura($idlectura){

        // componemos la consulta
        $consulta = "DELETE FROM diagnostico.temperaturas WHERE id = '$idlectura';";
        $this->Link->exec($consulta);

    }

}