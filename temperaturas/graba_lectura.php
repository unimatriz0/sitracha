<?php

/**
 *
 * temperaturas/graba_lectura.class.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/05/2018)
 * @copyright   Copyright (c) 2017, INP/
 * 
 * Método que recibe por post los datos de una lectura y ejecuta la 
 * consulta en la base
 * 
*/

// incluimos e instanciamos la clase de temperaturas
require_once("temperaturas.class.php");
$lectura = new Temperaturas();

// fijamos los valores
$lectura->setHeladera($_POST["Heladera"]);
$lectura->setFecha($_POST["Fecha"]);
$lectura->setTemperatura($_POST["Temperatura"]);
$lectura->setTemperatura2($_POST["Temperatura2"]);

// grabamos el registro
$resultado = $lectura->grabaLectura();

// retornamos la id
echo json_encode(array("Id" => $resultado));

?>