<?php

/**
 *
 * temperaturas/xls_temperaturas.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/05/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por get la clave de la heladera y la fecha
 * inicial y final a reportar y genera el excel con el reporte
 *
*/

// incluimos e instanciamos la clase
require_once("temperaturas.class.php");
require_once ("../clases/phpexcel/PHPExcel.php");
$temperaturas = new Temperaturas();
$hoja = new PHPExcel();

// asignamos las variables
$inicio = $_GET["inicio"];
$fin = $_GET["fin"];

// obtenemos la nómina de lecturas
$lecturas = $temperaturas->reporteLecturas($_GET["heladera"], $inicio, $fin);

// fijamos las propiedades
$hoja->getProperties()->setCreator("Lic. Claudio Invernizzi")
					  ->setLastModifiedBy("Lic. Claudio Invernizzi")
					  ->setTitle("Control de Temperaturas")
					  ->setSubject("Reporte de Temperaturas")
					  ->setDescription("Informe de Lecturas")
					  ->setKeywords("SiTraCha")
					  ->setCategory("Reportes");

// inicializamos las variables
$fila = 9;             // puntero de la fila
$determinaciones = 0;  // número de lecturas realizadas
$incidencias = 0;      // cantidad de incidencias
$rango = true;        // switch de lectura fuera de rango

// fijamos el estilo del título
$estilo = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 12,
        'name'  => 'Verdana'
    ));

// fijamos el estilo de los encabezados
$encabezado = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 10,
        'name'  => 'Verdana'
    ));

// el estilo de las celdas con error
$error = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'FF0000')
    ));

// leemos la plantilla
$hoja = PHPExcel_IOFactory::load("../clases/phpexcel/plantilla.xls");

// establecemos el ancho de las columnas
$hoja->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$hoja->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$hoja->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$hoja->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$hoja->getActiveSheet()->getColumnDimension('E')->setWidth(20);

// compone el título
$titulo = "En el período: " . $inicio . " al " . $fin;

// presenta el título y el laboratorio
$hoja->setActiveSheetIndex(0)
    ->setCellValue('B3', 'Sistema de Trazabilidad y Diagnóstico');
$hoja->setActiveSheetIndex(0)
    ->setCellValue('B5', 'Reporte de Lecturas de Temperaturas');
$hoja->setActiveSheetIndex(0)
     ->setCellValue('B7', $titulo);

// centramos las celdas de los títulos
$hoja->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$hoja->getActiveSheet()->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// fijamos el estilo
$hoja->getActiveSheet()->getStyle('B3')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('B5')->applyFromArray($estilo);
$hoja->getActiveSheet()->getStyle('B7')->applyFromArray($estilo);

// unimos las celdas
$hoja->getActiveSheet()->mergeCells('B3:F3');
$hoja->getActiveSheet()->mergeCells('B5:F5');
$hoja->getActiveSheet()->mergeCells('B7:F7');

// establecemos la fuente
$hoja->getDefaultStyle()->getFont()->setName('Arial')
      ->setSize(10);

// renombramos la hoja
$hoja->getActiveSheet()->setTitle('Temperaturas');

// recorremos el vector
foreach($lecturas AS $registro){

    // obtenemos el registro
    extract($registro);

    // si estamos en el primer registro
    if ($fila == 9){

        // presenta el nombre de la heladera y la ubicación
        $marca = "Marca: " . $marca;
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('C9', $marca);
        $ubicacion = "Ubicación: " . $ubicacion;
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('C10', $ubicacion);

        // presenta los valores nominales y la tolerancia
        $titulotmp1 = "Temperatura 1: " . $temperatura . "°";
        $titulotmp2 = "Temperatura 2: " . $temperatura2 . "°";
        $titulotole = "Tolerancia: " . $tolerancia . "°";
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('C11', $titulotmp1);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('D11', $titulotmp2);
        $hoja->setActiveSheetIndex(0)
             ->setCellValue('C12', $titulotole);

        // fijamos el contador
        $fila = 13;

        // presenta los encabezados
        titulosColumna();

    }

    // si la temperatura 1 excede el rango de tolerancia
    if (abs(abs($temperatura) - abs($lectura1)) > abs($tolerancia)){

        // fijamos el switch
        $rango = false;

    // si se declaró la temperatura 2
    } elseif ($temperatura2 != 0){

        // si excede el rango
        if (abs(abs($temperatura2) - abs($lectura2)) > abs($tolerancia)){

            // fijamos el switch
            $rango = false;

        }

    // si no disparó error
    } else {

        // fijamos el sitch
        $rango = true;

    }

    // presentamos el registro
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('A' . $fila, $fecha);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('B' . $fila, $hora);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('C' . $fila, $lectura1);
    $hoja->setActiveSheetIndex(0)
            ->setCellValue('D' . $fila, $lectura2);
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('E' . $fila, $controlado);

    // si hubo una lectura fuera de rango
    if (!$rango){

        // aplicamos el formato
        $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($error);
        $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($error);
        $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($error);
        $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($error);
        $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($error);

        // incrementamos el contador
        $incidencias++;

    }

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa los contadores
    $fila++;
    $determinaciones++;

}

// incrementamos la fila
$fila++;

// ahora presentamos los totales
$texto = "Total Lecturas: " . $determinaciones;
$hoja->setActiveSheetIndex(0)
     ->setCellValue('A' . $fila, $texto);
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

// incrementamos la fila y presentamos las incidencias
$fila++;
$texto = "Incidencias del Período: " . $incidencias;
$hoja->setActiveSheetIndex(0)
     ->setCellValue('A' . $fila, $texto);
$hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);

// fijamos la primer hoja como activa para abrirla predeterminada
$hoja->setActiveSheetIndex(0);

// creamos el writer y lo dirigimos al navegador en formato 2007
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="temperaturas.xlsx"');
header('Cache-Control: max-age=0');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // fecha en el pasado
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // siempre modificado
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$Writer = PHPExcel_IOFactory::createWriter($hoja, 'Excel2007');
$Writer->save('php://output');

/**
 * Función que presenta los títulos de columna
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 */
function titulosColumna(){

    // declaración de variables globales
    global $hoja, $fila, $encabezado;

    // incrementamos la fila
    $fila++;

    // presentamos los encabezados
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('A' . $fila, 'Fecha');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('B' . $fila, 'Hora');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('C' . $fila, 'Lectura 1');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('D' . $fila, 'Lectura 2');
    $hoja->setActiveSheetIndex(0)
         ->setCellValue('E' . $fila, 'Controló');

    // fijamos el formato
    $hoja->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($encabezado);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($encabezado);

    // centramos las columnas correspondientes
    $hoja->getActiveSheet()->getStyle('A' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('B' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('C' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $hoja->getActiveSheet()->getStyle('E' . $fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // incrementa la fila
    $fila++;

}

?>