/*
 * Nombre: temperaturas.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 02/05/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones que controlan el abm de temperaturas
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controlan el abm de temperaturas
 */
class Temperaturas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initTemperaturas();
        this.layerTemperaturas = "";   // layer emergente

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * functión que inicializa las variables de clase
     */
    initTemperaturas(){

        // declaramos las variables de clase
        this.Id = 0;                 // clave del registro
        this.Heladera = 0;           // clave de la heladera
        this.Fecha = "";             // timestamp de la lectura
        this.Temperatura = 0;        // temperatura registrada
        this.Temperatura2 = 0;       // segunda temperatura
        this.Controlado = "";        // usuario que realizó la lectura

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que se encarga de cargar el html con la nómina de
     * heladeras
     */
    formTemperaturas(){

        // reiniciamos el contador
        sesion.reiniciar();

        // carga el formulario
        $("#form_controles").load("temperaturas/temperaturas.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el onchange del select de heladeras
     * verifica que se halla seleccionado alguna y luego
     * lista la nómina de temperaturas
     */
    grillaTemperaturas(){

        // reiniciamos el contador
        sesion.reiniciar();

        // si no hay una heladera activa
        if (document.getElementById("lista_heladera").value == 0){

            // retorna
            return false;

        }

        // cargamos en el div la grilla
        $("#grilla_lecturas").load("temperaturas/grilla_temperaturas.php?heladera="+document.getElementById("lista_heladera").value);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idlectura clave del registro a eliminar
     * Método llamado al pulsar el botón borrar, pide confirmación
     * antes de eliminar el registro de temperatura
     */
    borraLectura(idlectura){

        // reiniciamos el contador
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Lectura',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar la Lectura?',
            theme: 'TooltipBorder',
            confirm: function() {

                 // llama la rutina php
                 $.get('temperaturas/borra_lectura.php?id='+idlectura,
                     function(data){

                         // si retornó error
                         if (data.Error != 0){

                             // presenta el mensaje
                             new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                             // recargamos la grilla
                             temperaturas.grillaTemperaturas();

                         // si no pudo eliminar
                         } else {

                             // presenta el mensaje
                             new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                         }

                     }, "json");

             },
             cancel: function(){
                 Confirmacion.destroy();
             },
             confirmButton: 'Aceptar',
             cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar, verifica los datos
     * del formulario y luego lo envía al servidor
     */
    verificaLectura(){

        // reiniciamos el contador
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // obtenemos la id de la heladera y la asignamos
        this.Heladera = document.getElementById("lista_heladera").value;

        // si no cargó la fecha de lectura
        if (document.getElementById("fecha_lectura").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha del registro";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_lectura").focus();
            return false;

        }

        // si no cargó la hora de lectura
        if (document.getElementById("hora_lectura").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la hora del registro";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("hora_lectura").focus();
            return false;

        }

        // si no cargó los minutos
        if (document.getElementById("minutos_lectura").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique los minutos del registro";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("minutos_lectura").focus();
            return false;

        }

        // ahora que verificamos que cargó la hora y los minutos
        // componemos la cadena para grabar
        this.Fecha = document.getElementById("fecha_lectura").value +  " " + document.getElementById("hora_lectura").value + ":" + document.getElementById("minutos_lectura").value;

        // si no indicó la temperatura 1
        if (document.getElementById("temperatura_1").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la temperatura registrada";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("temperatura_1").focus();
            return false;

        // si cargó
        } else {

            // asignamos en la variable de clase
            this.Temperatura = document.getElementById("temperatura_1").value;

        }

        // la temperatura 2 la permitimos en blanco
        this.Temperatura2 = document.getElementById("temperatura_2").value;

        // grabamos el registro
        this.grabaLectura();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que envía los datos al servidor
     */
    grabaLectura(){

        // reiniciamos el contador
        sesion.reiniciar();

        // declaración de variables
        var datosLectura = new FormData();

        // agregamos los valores al formulario
        datosLectura.append("Heladera", this.Heladera);
        datosLectura.append("Fecha", this.Fecha);
        datosLectura.append("Temperatura", this.Temperatura);
        datosLectura.append("Temperatura2", this.Temperatura2);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "temperaturas/graba_lectura.php",
            data: datosLectura,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // recargamos la grilla
                    temperaturas.grillaTemperaturas();

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // inicializamos las variables por las dudas
                    temperaturas.initTemperaturas();

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón estadísticas, obtiene la id
     * de la heladera y abre el cuadro de diálogo emergente pidiendo
     * las fechas del reporte
     */
    generaReporte(){

        // reiniciamos el contador
        sesion.reiniciar();

        // obtenemos la clave del registro
        this.Heladera = document.getElementById("lista_heladera").value;

        // abrimos el layer emergente
        this.layerTemperaturas = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Reportes de Lecturas',
                    draggable: 'title',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    theme: 'TooltipBorder',
                    ajax: {
                        url: 'temperaturas/reporte.html',
                        reload: 'strict'
                    }
            });
        this.layerTemperaturas.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón aceptar del layer emergente
     * verifica los datos del formulario y luego llama la rutina
     * php de generación del excel
     */
    validaReporte(){

        // reiniciamos el contador
        sesion.reiniciar();

        // definición de variables
        var mensaje;
        var fecha_inicio = document.getElementById("fecha_inicio").value;
        var fecha_fin = document.getElementById("fecha_final").value;

        // si no seleccionó la fecha inicial
        if (fecha_inicio == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha inicial a reportar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no seleccionó la fecha final
        if (fecha_fin == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha final a reportar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // redirigimos el navegador
        window.location = "temperaturas/xls_temperaturas.php?heladera="+this.Heladera+"&inicio="+fecha_inicio+"&fin="+fecha_fin;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de generar el reporte o de pulsar
     * el botón cancelar que elimina el layer emergente
     */
    cancelaReporte(){

        // reiniciamos el contador
        sesion.reiniciar();

        // destruimos el layer
        this.layerTemperaturas.destroy();

    }

}