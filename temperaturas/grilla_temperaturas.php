<?php

/**
 *
 * temperaturas/grilla_temperaturas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/05/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe como parámetro la id de una heladera y presenta la grilla
 * con las lecturas obtenidas, permite agregar una nueva lectura o eliminar
 * las existentes
 *
 * @param $_GET["heladera"]
 *
*/

// incluimos e instanciamos la clase de temperaturas
require_once("temperaturas.class.php");
$lectura = new Temperaturas();

// incluimos e instanciamos la clase de heladeras
require_once("../heladeras/heladeras.class.php");
$heladera = new Heladeras();

// obtenemos la nómina de lecturas
$nomina = $lectura->getLecturas($_GET["heladera"]);

// obtenemos el registro de la heladera
$heladera->getDatosHeladera($_GET["heladera"]);

// obtenemos la temperatura de funcionamiento y la tolerancia
$nominal_1 = $heladera->getTemperatura();
$nominal_2 = $heladera->getTemperatura2();
$tolerancia = $heladera->getTolerancia();

// definimos la tabla
echo "<table width='60%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<td align='center'><b>Fecha</b></th>";
echo "<td align='center'><b>Hora</b></th>";
echo "<td><b>Temp. 1</b></td>";
echo "<td><b>Temp. 2</b></td>";
echo "<td align='left'><b>Controlado</b></td>";

// en el encabezado colocamos el botón de
// generar excel
echo "<td>";
echo "<input type='button'
       name='btnEstadisticas'
       id='btnEstadisticas'
       title='Genera estadísticas de la heladera'
       class='botonimprimir'
       onClick='temperaturas.generaReporte()'>";
echo "</td>";

// cerramos el encabezado
echo "</tr>";
echo "</thead>";

// definimos el cuerpo de la tabla
echo "<body>";

// presentamos la primer fila para agregar una lectura
echo "<tr>";

// la fecha de lectura
echo "<td align='center'>";
echo "<span class='tooltip'
      title='Seleccione la fecha lectura'>";
echo "<input type='text'
             name='fecha_lectura'
             id='fecha_lectura'
             size='10'
             class='datepicker'>";
echo "</span>";
echo "</td>";

// la hora de lectura (la definimos usando dos select)
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Seleccione la hora de lectura'>";
echo "<select name='hora_lectura'
              id='hora_lectura'
              size='1'>";

// agregamos el primer elemento
echo "<option></option>";

// agregamos los valores
for($i=0; $i<24; $i++){

    // agregamos la hora
    echo "<option>$i</option>";

}

// cerramos los tags
echo "</select>";
echo "</span>";

// presentamos un separador
echo ":";

// pedimos los minugos
echo "<span class='tooltip'
            title='Seleccione los minutos'>";
echo "<select name='minutos_lectura'
              id='minutos_lectura'
              size='1'>";

// agregamos el primer elemento en blanco
echo "<option></option>";

// agregamos los minutos
for($i=0;$i<60;$i=$i+10){

    // agregamos el valor
    echo "<option>$i</option>";

}

// cerramos los tags
echo "</select>";
echo "</span>";
echo "</td>";

// la temperatura 1
echo "<td>";
echo "<span class='tooltip'
            title='Indique el valor de lectura obtenido'>";
echo "<input type='number'
             name='temperatura_1'
             id='temperatura_1'
             class='numero'
             step='0.1'>";
echo "</span>";
echo "</td>";

// la temperatura 2
echo "<td>";
echo "<span class='tooltip'
            title='Indique el valor de lectura obtenido'>";
echo "<input type='number'
             name='temperatura_2'
             id='temperatura_2'
             class='numero'
             step='0.1'>";
echo "</span>";
echo "</td>";

// el usuario
echo "<td>";
echo "<span class='tooltip'
            title='Usuario que realizó la lectura'>";
echo "<input type='text'
             name='controlado_lectura'
             id='controlado_lectura'
             size=10'
             readonly>";
echo "</span>";
echo "</td>";

// el botón grabar
echo "<td>";
echo "<input type='button'
             name='btnNuevaLectura'
             id='btnNuevaLectura'
             class='botonagregar'
             title='Agrega una nueva lectura a la base'
             onClick='temperaturas.verificaLectura()'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// inicializamos la variable
$correcto = true;

// recorremos el vector de lecturas
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // si la temperatura 1 excede el rango de tolerancia
    if (abs(abs($nominal_1) - abs($temperatura)) > abs($tolerancia)){

        // fijamos el switch
        $correcto = false;

    // si se declaró la temperatura 2
    } elseif ($nominal_2 != 0){

        // si excede el rango
        if (abs(abs($nominal_2) - abs($temperatura2)) > abs($tolerancia)){

            // fijamos el switch
            $correcto = false;

        }

    // si no disparó error
    } else {

        // fijamos el sitch
        $correcto = true;

    }

    // según el valor del switch abrimos la fila
    if (!$correcto){
        echo "<tr style='background-color:#e5e90b;'>";
    } else {
        echo "<tr>";
    }

    // presentamos el registro
    echo "<td align='center'>$fecha</td>";
    echo "<td align='center'>$hora</td>";
    echo "<td align='center'>$temperatura</td>";

    // si se declaró la segunda temperatura
    if ($temperatura2 != 0){
        echo "<td align='center'>$temperatura2</td>";
    }
    echo "<td>$controlado</td>";

    // presenta el botón eliminar
    echo "<td>";
    echo "<input type='button'
                 name='btnBorraLectura'
                 id='btnBorraLectura'
                 class='botonborrar'
                 title='Elimina la lectura de la base'
                 onClick='temperaturas.borraLectura($idlectura)'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

    // reiniciamos la variable control
    $correcto = true;

}

// cerramos la tabla
echo "</body>";
echo "</table>";
?>
<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

    // fijamos los datepicker y configuramos el valor de retorno
    $('input.datepicker').Zebra_DatePicker({
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        format: "d/m/Y",
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    });

    // por defecto carga el usuario y la fecha actual
    document.getElementById("controlado_lectura").value = sessionStorage.getItem("Usuario");
    document.getElementById("fecha_lectura").value = fechaActual();

</script>