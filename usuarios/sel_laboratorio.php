<?php

/**
 *
 * usuarios/sel_laboratorio.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/10/2017)
 * @copyright   Copyright (c) 2017, INP
 * 
 * Método que recibe por get la clave del país, la clave indec de la 
 * jurisdicción y parte del nombre de un laboratorio, presenta la tabla 
 * con los registros concordantes y permite seleccionar el laboratorio
 * del usuario 
 * Utilizado en las altas y edición de responsables de laboratorios 
 * para seleccionar el laboratorio del usuario
 *
 * @param $_GET["pais"]
 * @param $_GET["jurisdiccion"]
 * @param $_GET["laboratorio"]
 * 
*/

// inclusión de archivos
require_once ("../laboratorios/laboratorios.class.php");
$laboratorio = new Laboratorios();

// buscamos los registros
$nominaLaboratorios = $laboratorio->buscaLaboratorioUsuario($_GET["pais"], $_GET["jurisdiccion"], $_GET["laboratorio"]);

// si no hay registros
if (count($nominaLaboratorios) == 0){

    // presenta el mensaje
    echo "<h3 align='center'>No hay registros coincidentes</h3>";

// si encontró
} else {

    // define la tabla
    echo "<table width='400' align='center' border='0'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Laboratorio</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($nominaLaboratorios AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila y presentamos el registro
        echo "<tr>";
        echo "<td>$laboratorio</td>";

        // presentamos el enlace
        echo "<td align='center'>";
        echo "<span class='tooltip'
                    title='Pulse para seleccionar el laboratorio'>";
        echo "<input type='button'
                     name='btnLabUsuario'
                     id='btnLabUsuario'
                     class='botoneditar'
                     onClick='usuarios.cargaLaboratorio($idlaboratorio, " . chr(34) . $laboratorio . chr(34) . ")'; ";
        echo "</span>";
        echo "<td>";


        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody></table>";

}
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
      attach: '.tooltip',
      theme: 'TooltipBorder'
    });

</SCRIPT>