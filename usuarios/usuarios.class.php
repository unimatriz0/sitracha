<?php

/**
 *
 * Class Usuarios | usuarios/usuarios.class.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Controla las operaciones de la base de datos autorizados del
 * sistema, recordar que la Base de Datos de Control de Calidad
 * contiene los datos de los usuarios, la Base de Diagnóstico
 * contiene los niveles de acceso y autorizaciones
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Usuarios {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del registro
    protected $NombreUsuario;         // nombre completo del usuario
    protected $CargoUsuario;          // cargo del usuario
    protected $Institucion;           // nombre de la institución
    protected $Telefono;              // número de teléfono con prefijos
    protected $EMail;                 // dirección de mail
    protected $Direccion;             // dirección postal
    protected $CodigoPostal;          // código postal
    protected $IdLaboratorio;         // clave del laboratorio al que pertenece
    protected $NombreLaboratorio;     // nombre del laboratorio al que pertenece
    protected $IdDepartamento;        // clave del departamento
    protected $Departamento;          // nombre del departamento
    protected $Matricula;             // matrícula profesional
    protected $Profesion;             // profesión o especialidad
    protected $Usuario;               // nombre de usuario para la base
    protected $Coordenadas;           // coordenadas gps de la dirección
    protected $Activo;                // indica si está activo
    protected $FechaAlta;             // fecha de alta del registro
    protected $Autorizo;              // clave del usuario que lo autorizó
    protected $NombreAutorizo;        // nombre del usuario que autorizó
    protected $Administrador;         // indica si es administrador
    protected $NivelCentral;          // indica si pertenece al nivel central
    protected $Personas;              // si puede editar personas
    protected $Congenito;             // si puede editar chagas congénito
    protected $Resultados;            // si puede cargar resultados
    protected $Muestras;              // si puede tomar muestras
    protected $Entrevistas;           // si puede realizar entrevistas
    protected $Auxiliares;            // si puede cargar tablas auxiliares
    protected $Protocolos;            // si puede firmar protocolos
    protected $Stock;                 // si puede editar el stock
    protected $Clinica;               // si puede acceder a la tabla de clìnica
    protected $Usuarios;              // si puede editar otros usuarios
    protected $Localidad;             // nombre de la localidad
    protected $CodLoc;                // código indec de la localidad
    protected $Provincia;             // nombre de la provincia
    protected $CodProv;               // código indec de la provincia
    protected $IdPais;                // clave del país
    protected $Pais;                  // nombre del país
    protected $Firma;                 // cadena con la firma
    protected $Comentarios;           // observaciones y comentarios

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->IdUsuario = 0;
        $this->NombreUsuario = "";
        $this->CargoUsuario = "";
        $this->Institucion = "";
        $this->Matricula = "";
        $this->Profesion = "";
        $this->Telefono = "";
        $this->EMail = "";
        $this->Direccion = "";
        $this->CodigoPostal = "";
        $this->IdLaboratorio = 0;
        $this->NombreLaboratorio = "";
        $this->IdDepartamento = 0;
        $this->Departamento = "";
        $this->Usuario = "";
        $this->Coordenadas = "";
        $this->Activo = "";
        $this->FechaAlta = "";
        $this->NombreAutorizo = "";
        $this->Administrador = "No";
        $this->NivelCentral = "No";
        $this->Personas = "No";
        $this->Congenito = "No";
        $this->Resultados = "No";
        $this->Muestras = "No";
        $this->Entrevistas = "No";
        $this->Auxiliares = "No";
        $this->Protocolos = "No";
        $this->Stock = "No";
        $this->Clinica = "No";
        $this->Usuarios = "No";
        $this->Localidad = "";
        $this->CodLoc = "";
        $this->Provincia = "";
        $this->CodProv = "";
        $this->IdPais = 0;
        $this->Pais = "";
        $this->Firma = "";
        $this->Comentarios = "";

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->Autorizo = $_SESSION["ID"];

        // si no inició
        } else {

            // inicializa la variable
            $this->Autorizo = 0;

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdUsuario($idusuario){

        // verifica que sea un entero
        if (!is_numeric($idusuario)){

            // abandona por error
            echo "La clave del usuario debe ser un número";
            exit;

        // si es correcto
        } else {

            // asigna
            $this->IdUsuario = $idusuario;

        }

    }
    public function setNombreUsuario($nombre){
        $this->NombreUsuario = $nombre;
    }
    public function setCargoUsuario($cargo){
        $this->CargoUsuario = $cargo;
    }
    public function setUsuario($usuario){
        $this->Usuario = $usuario;
    }
    public function setInstitucion($institucion){
        $this->Institucion = $institucion;
    }
    public function setMatricula($matricula){
        $this->Matricula = $matricula;
    }
    public function setProfesion($profesion){
        $this->Profesion = $profesion;
    }
    public function setTelefono($telefono){
        $this->Telefono = $telefono;
    }
    public function setMail($mail){
        $this->EMail = $mail;
    }
    public function setDireccion($direccion){
        $this->Direccion = $direccion;
    }
    public function setCodigoPostal($codigopostal){
        $this->CodigoPostal = $codigopostal;
    }
    public function setIdLaboratorio($idlaboratorio){
        $this->IdLaboratorio = $idlaboratorio;
    }
    public function setIdDepartamento($iddepartamento){
        $this->IdDepartaemnto = $iddepartamento;
    }
    public function setCoordenadas($coordenadas){
        $this->Coordenadas = $coordenadas;
    }
    public function setActivo($activo){

        // verifica el valor recibido
        if ($activo != "Si" && $activo != "No"){

            // abandona por error
            echo "Activo puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Activo = $activo;

        }

    }
    public function setAdministrador($administrador){

        // verifica el valor recibido
        if ($administrador != "Si" && $administrador != "No"){

            // abandona por error
            echo "Administrador puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Administrador = $administrador;

        }

    }
    public function setNivelCentral($nivelcentral){

        // verifica el valor recibido
        if ($nivelcentral != "Si" && $nivelcentral != "No"){

            // abandona por error
            echo "Nivel Central puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->NivelCentral = $nivelcentral;

        }

    }
    public function setPersonas($personas){

        // verifica el valor recibido
        if ($personas != "Si" && $personas != "No"){

            // abandona por error
            echo "Personas puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Personas = $personas;

        }

    }
    public function setCongenito($congenito){

        // verifica el valor recibido
        if ($congenito != "Si" && $congenito != "No"){

            // abandona por error
            echo "Congenito puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Congenito = $congenito;

        }

    }
    public function setResultados($resultados){

        // verifica el valor recibido
        if ($resultados != "Si" && $resultados != "No"){

            // abandona por error
            echo "Resultados puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Resultados = $resultados;

        }

    }
    public function setMuestras($muestras){

        // verifica el valor recibido
        if ($muestras != "Si" && $muestras != "No"){

            // abandona por error
            echo "Muestras puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Muestras = $muestras;

        }

    }
    public function setEntrevistas($entrevistas){

        // verifica el valor recibido
        if ($entrevistas != "Si" && $entrevistas != "No"){

            // abandona por error
            echo "Entrevistas puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Entrevistas = $entrevistas;

        }

    }
    public function setProtocolos($protocolos){

        // verifica el valor recibido
        if ($protocolos != "Si" && $protocolos != "No"){

            // abandona por error
            echo "Protocolos puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Protocolos = $protocolos;

        }

    }
    public function setAuxiliares($auxiliares){

        // verifica el valor recibido
        if ($auxiliares != "Si" && $auxiliares != "No"){

            // abandona por error
            echo "Auxiliares puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Auxiliares = $auxiliares;

        }

    }
    public function setClinica($clinica){

        // verifica el valor recibido
        if ($clinica != "Si" && $clinica != "No"){

            // abandona por error
            echo "Clínica Médica puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Clinica = $clinica;

        }

    }
    public function setStock($stock){

        // verifica el valor recibido
        if ($stock != "Si" && $stock != "No"){

            // abandona por error
            echo "Stock puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Stock = $stock;

        }

    }
    public function setUsuarios($usuarios){

        // verifica el valor recibido
        if ($usuarios != "Si" && $usuarios != "No"){

            // abandona por error
            echo "Usuarios puede ser solamente 'Si' o 'No'";
            exit;

        // de otra forma
        } else {

            // asigna
            $this->Usuarios = $usuarios;

        }

    }
    public function setCodLoc($codloc){
        $this->CodLoc = $codloc;
    }
    public function setCodProv($codprov){
        $this->CodProv = $codprov;
    }
    public function setIdPais($idpais){
        $this->IdPais = $idpais;
    }
    public function setFirma($firma){
        $this->Firma = $firma;
    }
    public function setComentarios($comentarios){
        $this->Comentarios = $comentarios;
    }

    // métodos de retorno de valores
    public function getIdUsuario(){
        return $this->IdUsuario;
    }
    public function getNombreUsuario(){
        return $this->NombreUsuario;
    }
    public function getCargoUsuario(){
        return $this->CargoUsuario;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getMatricula(){
        return $this->Matricula;
    }
    public function getProfesion(){
        return $this->Profesion;
    }
    public function getTelefono(){
        return $this->Telefono;
    }
    public function getMail(){
        return $this->EMail;
    }
    public function getDireccion(){
        return $this->Direccion;
    }
    public function getCodigoPostal(){
        return $this->CodigoPostal;
    }
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getNombreLaboratorio(){
        return $this->NombreLaboratorio;
    }
    public function getIdDepartamento(){
        return $this->IdDepartamento;
    }
    public function getDepartamento(){
        return $this->Departamento;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getCoordenadas(){
        return $this->Coordenadas;
    }
    public function getActivo(){
        return $this->Activo;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }
    public function getNombreAutorizo(){
        return $this->NombreAutorizo;
    }
    public function getAutorizo(){
        return $this->Autorizo;
    }
    public function getAdministrador(){
        return $this->Administrador;
    }
    public function getNivelCentral(){
        return $this->NivelCentral;
    }
    public function getPersonas(){
        return $this->Personas;
    }
    public function getCongenito(){
        return $this->Congenito;
    }
    public function getResultados(){
        return $this->Resultados;
    }
    public function getMuestras(){
        return $this->Muestras;
    }
    public function getEntrevistas(){
        return $this->Entrevistas;
    }
    public function getAuxiliares(){
        return $this->Auxiliares;
    }
    public function getProtocolos(){
        return $this->Protocolos;
    }
    public function getClinica(){
        return $this->Clinica;
    }
    public function getStock(){
        return $this->Stock;
    }
    public function getUsuarios(){
        return $this->Usuarios;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getCodLoc(){
        return $this->CodLoc;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getCodProv(){
        return $this->CodProv;
    }
    public function getIdPais(){
        return $this->IdPais;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getFirma(){
        return $this->Firma;
    }
    public function getComentarios(){
        return $this->Comentarios;
    }

    /**
     * Método que actualiza el registro en la base de datos, según el
     * valor de la clave primaria inserta o edita el registro, retorna
     * la id del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaUsuario(){

        // si está insertando
        if ($this->IdUsuario == 0){

            // primero grabamos el responsable
            // para obtener la id
            $this->nuevoResponsable();

            // grabamos los permisos del usuario
            $this->nuevoUsuario();

        // si tiene id
        } else {

            // primero editamos el responsable
            $this->editaResponsable();

            // editamos el registro del usuario
            $this->editaUsuario();

        }

        // si hay firma no importa si está editando o ya insertó
        // actualiza la base de datos
        if (strlen($this->Firma) != 0){
            $this->actualizaFirma();
        }

        // retorna la id del registro
        return $this->IdUsuario;

    }

    /**
     * Método que de existir una firma la actualiza en la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function actualizaFirma(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.usuarios SET
                            firma = :firma
                     WHERE id = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":firma", $this->Firma);
        $psInsertar->bindParam(":id", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que ejecuta la consulta de inserción de un nuevo registro
     * en la tabla de usuarios de diagnóstico
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoUsuario(){

        // componemos la consulta (ya tenemos la id de la
        // tabla de responsables)
        $consulta = "INSERT INTO diagnostico.usuarios
                            (id,
                             autorizo,
                             matricula,
                             profesion,
                             laboratorio,
                             departamento,
                             administrador,
                             personas,
                             congenito,
                             resultados,
                             muestras,
                             entrevistas,
                             auxiliares,
                             protocolos,
                             stock,
                             clinica,
                             usuarios)
                            VALUES
                            (:idusuario,
                             :autorizo,
                             :matricula,
                             :profesion,
                             :idlaboratorio,
                             :departamento,
                             :administrador,
                             :personas,
                             :congenito,
                             :resultados,
                             :muestras,
                             :entrevistas,
                             :auxiliares,
                             :protocolos,
                             :stock,
                             :clinica,
                             :usuarios);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);
        $psInsertar->bindParam(":autorizo", $this->Autorizo);
        $psInsertar->bindParam(":matricula", $this->Matricula);
        $psInsertar->bindParam(":profesion", $this->Profesion);
        $psInsertar->bindParam(":idlaboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":departamento", $this->IdDepartamento);
        $psInsertar->bindParam(":administrador", $this->Administrador);
        $psInsertar->bindParam(":personas", $this->Personas);
        $psInsertar->bindParam(":congenito", $this->Congenito);
        $psInsertar->bindParam(":resultados", $this->Resultados);
        $psInsertar->bindParam(":muestras", $this->Muestras);
        $psInsertar->bindParam(":entrevistas", $this->Entrevistas);
        $psInsertar->bindParam(":auxiliares", $this->Auxiliares);
        $psInsertar->bindParam(":protocolos", $this->Protocolos);
        $psInsertar->bindParam(":stock", $this->Stock);
        $psInsertar->bindParam(":clinica", $this->Clinica);
        $psInsertar->bindParam(":usuarios", $this->Usuarios);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método protegido llamado en la inserción de usuarios, inserta un registro
     * en la tabla de responsables
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoResponsable(){

        // componemos la consulta
        $consulta = "INSERT INTO cce.responsables
                            (nombre,
                             institucion,
                             cargo,
                             e_mail,
                             telefono,
                             pais,
                             localidad,
                             direccion,
                             codigo_postal,
                             coordenadas,
                             activo,
                             observaciones,
                             usuario)
                            VALUES
                            (:nombre,
                             :institucion,
                             :cargo,
                             :e_mail,
                             :telefono,
                             :idpais,
                             :codloc,
                             :direccion,
                             :codigo_postal,
                             :coordenadas,
                             :activo,
                             :observaciones,
                             :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->NombreUsuario);
        $psInsertar->bindParam(":institucion", $this->Institucion);
        $psInsertar->bindParam(":cargo", $this->CargoUsuario);
        $psInsertar->bindParam(":e_mail", $this->EMail);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":codloc", $this->CodLoc);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":coordenadas", $this->Coordenadas);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":observaciones", $this->Comentarios);
        $psInsertar->bindParam(":usuario", $this->Usuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la id del registro
        $this->IdUsuario = $this->Link->lastInsertId();

    }

    /**
     * Método que edita el registro actual de usuarios
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaUsuario(){

        // verificamos si ya tiene id en la tabla de responsables
        // (puede ser un alta en la tabla de usuarios autorizados
        // pero ya tener id en la tabla de responsables del cce)

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.usuarios.id) AS registros
                     FROM diagnostico.usuarios
                     WHERE diagnostico.usuarios.id = '$this->IdUsuario';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si no existe en la tabla de diagnóstico
        if ($registros == 0){

            // lo insertamos
            $this->nuevoUsuario();

        // si existe en la tabla de diagnóstico
        } else {

            // actualiza los permisos
            $this->actualizaUsuario();

        }

    }

    /**
     * Método que actualiza la tabla de usuarios de diagnóstico
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function actualizaUsuario(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.usuarios SET
                            autorizo = :autorizo,
                            matricula = :matricula,
                            profesion = :profesion,
                            laboratorio = :idlaboratorio,
                            departamento = :departamento,
                            administrador = :administrador,
                            personas = :personas,
                            congenito = :congenito,
                            resultados = :resultados,
                            muestras = :muestras,
                            entrevistas = :entrevistas,
                            auxiliares = :auxiliares,
                            protocolos = :protocolos,
                            stock = :stock,
                            clinica = :clinica,
                            usuarios = :usuarios
                      WHERE diagnostico.usuarios.id = :idusuario;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":autorizo", $this->Autorizo);
        $psInsertar->bindParam(":matricula", $this->Matricula);
        $psInsertar->bindParam(":profesion", $this->Profesion);
        $psInsertar->bindParam(":idlaboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":departamento", $this->IdDepartamento);
        $psInsertar->bindParam(":administrador", $this->Administrador);
        $psInsertar->bindParam(":personas", $this->Personas);
        $psInsertar->bindParam(":congenito", $this->Congenito);
        $psInsertar->bindParam(":resultados", $this->Resultados);
        $psInsertar->bindParam(":muestras", $this->Muestras);
        $psInsertar->bindParam(":entrevistas", $this->Entrevistas);
        $psInsertar->bindParam(":auxiliares", $this->Auxiliares);
        $psInsertar->bindParam(":protocolos", $this->Protocolos);
        $psInsertar->bindParam(":stock", $this->Stock);
        $psInsertar->bindParam(":clinica", $this->Clinica);
        $psInsertar->bindParam(":usuarios", $this->Usuarios);
        $psInsertar->bindParam(":idusuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método protegido que edita la tabla de responsables
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaResponsable(){

        // componemos la consulta
        $consulta = "UPDATE cce.responsables SET
                            nombre = :nombre,
                            institucion = :institucion,
                            cargo = :cargo,
                            e_mail = :email,
                            telefono = :telefono,
                            pais = :idpais,
                            localidad = :codloc,
                            direccion = :direccion,
                            codigo_postal = :codigo_postal,
                            coordenadas = :coordenadas,
                            activo = :activo,
                            observaciones = :observaciones
                     WHERE cce.responsables.id = :idresponsable; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":nombre", $this->NombreUsuario);
        $psInsertar->bindParam(":institucion", $this->Institucion);
        $psInsertar->bindParam(":cargo", $this->CargoUsuario);
        $psInsertar->bindParam(":email", $this->EMail);
        $psInsertar->bindParam(":telefono", $this->Telefono);
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":codloc", $this->CodLoc);
        $psInsertar->bindParam(":direccion", $this->Direccion);
        $psInsertar->bindParam(":codigo_postal", $this->CodigoPostal);
        $psInsertar->bindParam(":coordenadas", $this->Coordenadas);
        $psInsertar->bindParam(":activo", $this->Activo);
        $psInsertar->bindParam(":observaciones", $this->Comentarios);
        $psInsertar->bindParam(":idresponsable", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave del registro de usuario
     * y asigna en las variables de clase los valores del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idusuario - clave del usuario
     */
    public function getDatosUsuario($idusuario){

        // verificamos haber recibido una clave
        if (!is_numeric($idusuario)){

            // abandona por error
            echo "La clave del usuario debe ser un número";
            exit;

        }

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.nombreusuario AS nombreusuario,
                            diagnostico.v_usuarios.cargousuario AS cargousuario,
                            diagnostico.v_usuarios.institucion AS institucion,
                            diagnostico.v_usuarios.matricula AS matricula,
                            diagnostico.v_usuarios.profesion AS profesion,
                            diagnostico.v_usuarios.telefono AS telefono,
                            diagnostico.v_usuarios.e_mail AS e_mail,
                            diagnostico.v_usuarios.direccion AS direccion,
                            diagnostico.v_usuarios.codigo_postal AS codigo_postal,
                            diagnostico.v_usuarios.idlaboratorio AS idlaboratorio,
                            diagnostico.v_usuarios.nombrelaboratorio AS nombrelaboratorio,
                            diagnostico.v_usuarios.id_departamento AS iddepartamento,
                            diagnostico.v_usuarios.departamento AS departamento,
                            diagnostico.v_usuarios.usuario AS usuario,
                            diagnostico.v_usuarios.coordenadas AS coordenadas,
                            diagnostico.v_usuarios.activo AS activo,
                            diagnostico.v_usuarios.fecha_alta AS fecha_alta,
                            diagnostico.v_usuarios.autorizo AS autorizo,
                            diagnostico.v_usuarios.administrador AS administrador,
                            diagnostico.v_usuarios.nivelcentral AS nivel_central,
                            diagnostico.v_usuarios.personas AS personas,
                            diagnostico.v_usuarios.congenito AS congenito,
                            diagnostico.v_usuarios.resultados AS resultados,
                            diagnostico.v_usuarios.muestras AS muestras,
                            diagnostico.v_usuarios.entrevistas AS entrevistas,
                            diagnostico.v_usuarios.auxiliares AS auxiliares,
                            diagnostico.v_usuarios.protocolos AS protocolos,
                            diagnostico.v_usuarios.stock AS stock,
                            diagnostico.v_usuarios.clinica AS clinica,
                            diagnostico.v_usuarios.usuarios AS usuarios,
                            diagnostico.v_usuarios.localidad AS localidad,
                            diagnostico.v_usuarios.codloc AS codloc,
                            diagnostico.v_usuarios.provincia AS provincia,
                            diagnostico.v_usuarios.codprov AS codprov,
                            diagnostico.v_usuarios.pais AS pais,
                            diagnostico.v_usuarios.idpais AS idpais,
                            diagnostico.v_usuarios.firma AS firma,
                            diagnostico.v_usuarios.comentarios AS comentarios
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.idusuario = '$idusuario';";

        // ejecutamos la consulta y obtenemos el registro
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // asignamos en las variables de clase
        $this->IdUsuario = $idusuario;
        $this->NombreUsuario = $nombreusuario;
        $this->CargoUsuario = $cargousuario;
        $this->Institucion = $institucion;
        $this->Matricula = $matricula;
        $this->Profesion = $profesion;
        $this->Telefono = $telefono;
        $this->EMail = $e_mail;
        $this->Direccion = $direccion;
        $this->CodigoPostal = $codigo_postal;
        $this->IdLaboratorio = $idlaboratorio;
        $this->NombreLaboratorio = $nombrelaboratorio;
        $this->IdDepartamento = $iddepartamento;
        $this->Departamento = $departamento;
        $this->Usuario = $usuario;
        $this->Coordenadas = $coordenadas;
        $this->Activo = $activo;
        $this->FechaAlta = $fecha_alta;
        $this->NombreAutorizo = $autorizo;
        $this->Administrador = $administrador;
        $this->NivelCentral = $nivel_central;
        $this->Personas = $personas;
        $this->Congenito = $congenito;
        $this->Resultados = $resultados;
        $this->Muestras = $muestras;
        $this->Entrevistas = $entrevistas;
        $this->Auxiliares = $auxiliares;
        $this->Protocolos = $protocolos;
        $this->Stock = $stock;
        $this->Clinica = $clinica;
        $this->Usuarios = $usuarios;
        $this->Localidad = $localidad;
        $this->CodLoc = $codloc;
        $this->Provincia = $provincia;
        $this->CodProv = $codprov;
        $this->IdPais = $idpais;
        $this->Pais = $pais;
        $this->Firma = $firma;
        $this->Comentarios = $comentarios;

    }

    /**
     * Método que recibe como parámetro una cadena, busca la misma en las tablas
     * de usuarios y retorna el vector con los registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $texto - cadena de texto a buscar
     * @return array
     */
    public function buscaUsuario($texto){

        // componemos la consulta en la vista
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.nombreusuario AS nombre,
                            diagnostico.v_usuarios.institucion AS institucion,
                            diagnostico.v_usuarios.provincia AS provincia,
                            diagnostico.v_usuarios.localidad AS localidad
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.nombreusuario LIKE '%$texto%' OR
                           diagnostico.v_usuarios.institucion LIKE '%$texto%' OR
                           diagnostico.v_usuarios.provincia LIKE '%$texto%'
                     ORDER BY diagnostico.v_usuarios.provincia,
                              diagnostico.v_usuarios.nombreusuario;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método utilizado en el alta de usuarios, recibe como parámetro
     * el nombre de un usuario y verifica si ya se encuentra declarado
     * retorna el número de registros encontrados (utilizado para
     * evitar duplicados)
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $usuario - nombre de usuario a verificar
     * @return int $registros - número de registros encontrados
     */
    public function verificaUsuario($usuario){

        // inicializamos las variables
        $registros = 0;

        // compone la consulta
        $consulta = "SELECT COUNT(cce.responsables.usuario) AS registros
                     FROM cce.responsables
                     WHERE cce.responsables.usuario = '$usuario';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el número de registros
        extract($fila);
        return $registros;

    }

    /**
     * Método utilizado en el alta de nuevos usuarios, recibe como
     * parámetro una cadena con la dirección de correo y verifica
     * que no se encuentre declarada.
     * Retorna el número de registros encontrados
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - cadena con la dirección de correo
     * @return int $registros - número de registros encontrados
     */
    public function verificaMail($mail){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(cce.responsables.e_mail) AS registros
                     FROM cce.responsables
                     WHERE cce.responsables.e_mail = '$mail';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el número de registros
        extract($fila);
        return $registros;

    }

    /**
     * Método utilizado en el formulario de responsables que lista los
     * responsables activos de la jurisdicción que recibe como parámetro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $jurisdiccion - nombre de una jurisdicción
     * @return array
     */
    public function listaResponsables($jurisdiccion){

        // componemos la consulta
        $consulta = "SELECT cce.responsables.NOMBRE AS nombreresponsable,
                            cce.responsables.ID AS idresponsable
                     FROM cce.responsables INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                     WHERE diccionarios.provincias.COD_PROV = '$jurisdiccion' AND
                           cce.responsables.ACTIVO = 'Si' AND
                           cce.responsables.RESPONSABLE_CHAGAS = 'Si'
                     ORDER BY cce.responsables.NOMBRE;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaResponsables = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nominaResponsables;

    }

    /**
     * Método público que retorna la nómina de usuarios del mismo laboratorio
     * que el usuario actual
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaUsuarios(){

        // obtenemos el laboratorio del usuario actual
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $idlaboratorio = $_SESSION["IdLaboratorio"];

        }

        // cerramos la sesión
        session_write_close();

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.usuario AS usuario
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.idlaboratorio = '$idlaboratorio'
                     ORDER BY diagnostico.v_usuarios.usuario;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaResponsables = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nominaResponsables;

    }

    /**
     * Método que retorna la nómina de profesionales para el
     * laboratorio del usuario actual utilizado para las
     * historias clínicas y los certificados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaProfesionales(){

        // obtenemos el laboratorio del usuario actual
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $idlaboratorio = $_SESSION["IdLaboratorio"];

        }

        // cerramos la sesión
        session_write_close();

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.usuario AS usuario,
                            diagnostico.v_usuarios.nombreusuario AS nombre
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.idlaboratorio = '$idlaboratorio' AND
                           (NOT ISNULL(diagnostico.v_usuarios.matricula) AND
                            diagnostico.v_usuarios.matricula != '')
                     ORDER BY diagnostico.v_usuarios.usuario;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaProfesionales = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nominaProfesionales;

    }

    /**
     * Método que retorna la nómina de profesionales para el
     * laboratorio del usuario actual que tienen horarios de
     * atención asignados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaProfHorarios(){

        // obtenemos el laboratorio del usuario actual
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $idlaboratorio = $_SESSION["IdLaboratorio"];

        }

        // cerramos la sesión
        session_write_close();

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.usuario AS usuario,
                            diagnostico.v_usuarios.nombreusuario AS nombre
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.idlaboratorio = '$idlaboratorio' AND
                           (NOT ISNULL(diagnostico.v_usuarios.matricula) AND
                            diagnostico.v_usuarios.matricula != '')
                     ORDER BY diagnostico.v_usuarios.usuario;";

/*
        // componemos la consulta
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.usuario AS usuario,
                            diagnostico.v_usuarios.nombreusuario AS nombre
                     FROM diagnostico.v_usuarios INNER JOIN horarios ON diagnostico.v_usuarios.idusuario = diagnostico.horarios.profesional
                     WHERE diagnostico.v_usuarios.idlaboratorio = '$idlaboratorio'
                     ORDER BY diagnostico.v_usuarios.nombreusuario;";
*/
        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nomina;

    }

}
?>