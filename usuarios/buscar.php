<?php

/**
 *
 * usuarios/buscar.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/10/2017)
 * @copyright   Copyright (c) 2017, INP
 * 
 * Procedimiento que recibe por get una cadena de texto a buscar, ejecuta 
 * la consulta en la clase y presenta los registros encontrados en una 
 * grilla (permitiendo la selección de un usuario)
 * 
 * @param $_GET["texto"]
 *
*/

// inclusión de librerías
require_once("usuarios.class.php");
$usuario = new Usuarios();

// ejecutamos la consulta
$nomina = $usuario->buscaUsuario($_GET["texto"]);

// si no hubo registros
if (count($nomina) == 0){

    // presenta el mensaje
    echo "<h3>No se han encontrado registros coincidentes</h3>";

// si hubo resultados
} else {

    // definimos la tabla
    echo "<table width='650' border='0' align='center' id='usuarioshallados'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th left='center'>Jurisdicción</th>";
    echo "<th left='center'>Institución</th>";
    echo "<th left='center'>Usuario</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // definimos el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($nomina AS $registro){

        // extraemos el registro
        extract($registro);

        // abrimos la fila y lo presentamos
        echo "<tr>";
        echo "<td>$provincia</td>";
        echo "<td>$institucion</td>";
        echo "<td>$nombre</td>";

        // armamos el enlace
        echo "<td>";
        echo "<span class='tooltip'
                    title='Pulse para ver el usuario'>";
        echo "<input type='button'
                     name='btnVerUsuario'
                     id='btnVerUsuario'
                     class='botoneditar'
                     onClick='usuarios.obtenerUsuario($idusuario)'>";
        echo "</span>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</body></table>";

    // definimos el paginador
    echo "<div class='paging'></div>";

    // asignamos las propiedades de la tabla
    ?>
    <SCRIPT>

        // instanciamos los tooltips
        new jBox('Tooltip', {
            attach: '.tooltip',
            theme: 'TooltipBorder'
        });

        // propiedades de la tabla
        $('#usuarioshallados').datatable({
            pageSize: 15,
            sort: [true, true, true, false],
            filters: ['select', true, true, false],
            filterText: 'Buscar ... '
        });

    </SCRIPT>
    <?php

}
?>