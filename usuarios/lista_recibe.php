<?php

/**
 *
 * usuarios/lista_recibe.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/04/2018)
 * @copyright   Copyright (c) 2017, INP
 * 
 * Procedimiento que recibe por get la clave indec de una provincia y 
 * retorna un array json con los responsables activos de esa provincia
 * usado en el formulario de laboratorios para seleccionar el 
 * responsable de recibir las muestras
 * 
 * @param $_GET["provincia"]
 *
*/

// incluimos e instanciamos la clase
require_once ("usuarios.class.php");
$responsable = new Usuarios();

// obtenemos la nómina
$nomina = $responsable->listaResponsables($_GET["provincia"]);

// inicializa las variables
$jsondata = array();

// inicia un bucle recorriendo el vector
foreach ($nomina AS $registro){

    // obtiene el registro
    extract($registro);

    // lo agrega a la matriz
    $jsondata[] = array("Id" => $idresponsable,
                        "Nombre" => $nombreresponsable);

}

// devuelve la cadena
echo json_encode($jsondata);

?>