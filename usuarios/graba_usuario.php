<?php

/**
 *
 * usuarios/graba_usuario.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/11/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta la consulta
 * en la base de datos, retorna en un array json la clave del registro
 * afectado o cero en caso de error
 *
*/

// incluimos e instanciamos las clases
require_once ("usuarios.class.php");
$usuario = new Usuarios();

// si recibió la clave
if (!empty($_POST["Id"])){
    $usuario->setIdUsuario($_POST["Id"]);
}

// agrega los elementos
$usuario->setNombreUsuario($_POST["Nombre"]);
$usuario->setProfesion($_POST["Profesion"]);
$usuario->setMatricula($_POST["Matricula"]);
$usuario->setCargoUsuario($_POST["Cargo"]);
$usuario->setInstitucion($_POST["Institucion"]);
$usuario->setTelefono($_POST["Telefono"]);
$usuario->setMail($_POST["Mail"]);
$usuario->setDireccion($_POST["Direccion"]);
$usuario->setCodigoPostal($_POST["CodigoPostal"]);
$usuario->setIdLaboratorio($_POST["IdLaboratorio"]);
$usuario->setIdDepartamento($_POST["IdDepartamento"]);
$usuario->setUsuario($_POST["Usuario"]);
$usuario->setCoordenadas($_POST["Coordenadas"]);
$usuario->setActivo($_POST["Activo"]);
$usuario->setAdministrador($_POST["Administrador"]);
$usuario->setPersonas($_POST["Personas"]);
$usuario->setCongenito($_POST["Congenito"]);
$usuario->setResultados($_POST["Resultados"]);
$usuario->setMuestras($_POST["Muestras"]);
$usuario->setEntrevistas($_POST["Entrevistas"]);
$usuario->setAuxiliares($_POST["Auxiliares"]);
$usuario->setProtocolos($_POST["Protocolos"]);
$usuario->setStock($_POST["Stock"]);
$usuario->setClinica(($_POST["Clinica"]));
$usuario->setUsuarios($_POST["Usuarios"]);
$usuario->setCodLoc($_POST["CodLoc"]);
$usuario->setCodProv($_POST["CodProv"]);
$usuario->setIdPais($_POST["IdPais"]);
$usuario->setComentarios($_POST["Comentarios"]);

// si recibió la firma
if (!empty($_POST["Firma"])){
    $usuario->setFirma($_POST["Firma"]);
}

// grabamos el registro
$idusuario = $usuario->grabaUsuario();

// retornamos el resultado de la operación
echo json_encode(array("Id" => $idusuario));
?>