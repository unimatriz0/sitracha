<?php

/**
 *
 * usuarios/valida_mail.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/04/2018)
 * @copyright   Copyright (c) 2017, INP
 * 
 * Método que recibe por get el mail de un usuario y verifica que este no 
 * se encuentre declarado, utilizado en las altas para evitar nombres 
 * repetidos
 * 
 * @param $_GET["mail"]
 *
*/

// incluimos e instanciamos las clases
require_once("usuarios.class.php");
$usuario = new Usuarios();

// verificamos si existe
$registros = $usuario->verificaMail($_GET["mail"]);

// retornamos
echo json_encode(array("Registros" => $registros));

?>