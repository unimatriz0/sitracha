<?php

/**
 *
 * usuarios/lista_profhorarios.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (19/07/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que retorna un array json con la nómina de profesionales activos
 * del mismo laboratorio que el usuario de la sesión y que tienen horarios de
 * atención asignados
 *
*/

// incluimos e instanciamos la clase
require_once ("usuarios.class.php");
$responsable = new Usuarios();

// obtenemos la nómina
$nomina = $responsable->nominaProfHorarios();

// inicializa las variables
$jsondata = array();

// inicia un bucle recorriendo el vector
foreach ($nomina AS $registro){

    // obtiene el registro
    extract($registro);

    // lo agrega a la matriz
    $jsondata[] = array("Id" => $idusuario,
                        "Usuario" => $nombre);

}

// devuelve la cadena
echo json_encode($jsondata);

?>