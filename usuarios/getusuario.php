<?php

/**
 *
 * usuarios/getusuario.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/11/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Procedimiento que recibe por get la clave de un usuario, ejecuta la
 * consulta en la base y retorna en un array json los datos del registro
 *
 * @param $_GET["idusuario"]
 *
*/

// inclusión de librerías
require_once ("usuarios.class.php");
$usuario = new Usuarios();

// obtenemos el registro
$usuario->getDatosUsuario($_GET["idusuario"]);

// ahora componemos la matriz json y la enviamos
echo json_encode(array("IdUsuario" =>      $usuario->getIdUsuario(),
                       "Nombre" =>         $usuario->getNombreUsuario(),
                       "Matricula" =>      $usuario->getMatricula(),
                       "Profesion" =>      $usuario->getProfesion(),
                       "Cargo" =>          $usuario->getCargoUsuario(),
                       "Institucion" =>    $usuario->getInstitucion(),
                       "Telefono" =>       $usuario->getTelefono(),
                       "Email" =>          $usuario->getMail(),
                       "Direccion" =>      $usuario->getDireccion(),
                       "CodigoPostal" =>   $usuario->getCodigoPostal(),
                       "IdLaboratorio" =>  $usuario->getIdLaboratorio(),
                       "Laboratorio" =>    $usuario->getNombreLaboratorio(),
                       "IdDepartamento" => $usuario->getIdDepartamento(),
                       "Departamento" =>   $usuario->getDepartamento(),
                       "Usuario" =>        $usuario->getUsuario(),
                       "Coordenadas" =>    $usuario->getCoordenadas(),
                       "Activo" =>         $usuario->getActivo(),
                       "FechaAlta" =>      $usuario->getFechaAlta(),
                       "Autorizo" =>       $usuario->getNombreAutorizo(),
                       "Administrador" =>  $usuario->getAdministrador(),
                       "NivelCentral" =>   $usuario->getNivelCentral(),
                       "Personas" =>       $usuario->getPersonas(),
                       "Congenito" =>      $usuario->getCongenito(),
                       "Resultados" =>     $usuario->getResultados(),
                       "Muestras" =>       $usuario->getMuestras(),
                       "Entrevistas" =>    $usuario->getEntrevistas(),
                       "Auxiliares" =>     $usuario->getAuxiliares(),
                       "Protocolos" =>     $usuario->getProtocolos(),
                       "Stock" =>          $usuario->getStock(),
                       "Clinica" =>        $usuario->getClinica(),
                       "Usuarios" =>       $usuario->getUsuarios(),
                       "Localidad" =>      $usuario->getLocalidad(),
                       "CodLoc" =>         $usuario->getCodLoc(),
                       "Provincia" =>      $usuario->getProvincia(),
                       "CodProv" =>        $usuario->getCodProv(),
                       "IdPais" =>         $usuario->getIdPais(),
                       "Pais" =>           $usuario->getPais(),
                       "Firma" =>          $usuario->getFirma(),
                       "Comentarios" =>    $usuario->getComentarios()));

?>