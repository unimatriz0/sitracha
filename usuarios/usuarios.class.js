/*

    Nombre: usuarios.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 22/10/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones del formulario de usuarios

*/

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de usuarios del sistema
 */
class Usuarios {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initUsuarios();

        // declaramos las variables para los layers emergentes
        this.layerLaboratorio = "";

        // cargamos el formulario de usuarios
        $("#form_usuarios").load("usuarios/form_usuarios.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initUsuarios(){

        // inicializamos las variables
        this.IdUsuario = 0;
        this.NombreUsuario = "";
        this.Profesion = "";
        this.Matricula = "";
        this.CargoUsuario = "";
        this.Institucion = "";
        this.Telefono = "";
        this.EMail = "";
        this.Direccion = "";
        this.CodigoPostal = "";
        this.IdLaboratorio = "";
        this.NombreLaboratorio = "";
        this.IdDepartamento = 0;
        this.Departamento = "";
        this.Usuario = "";
        this.Coordenadas = "";
        this.Activo = "";
        this.FechaAlta = "";
        this.Autorizo = "";
        this.Administrador = "No";
        this.Personas = "No";
        this.Congenito = "No";
        this.Resultados = "No";
        this.Muestras = "No";
        this.Entrevistas = "No";
        this.Auxiliares = "No";
        this.Protocolos = "No";
        this.Stock = "No";
        this.Clinica = "No";
        this.Usuarios = "No";
        this.Localidad = "";
        this.CodLoc = "";
        this.Provincia = "";
        this.CodProv = "";
        this.IdPais = "";
        this.Pais = "";
        this.Firma = "";
        this.Comentarios = "";

        // switch utilizados (lo inicializamos a true porque solo
        // verificamos en las altas)
        this.MailCorrecto = true;
        this.UsuarioCorrecto = true;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de usuarios antes de grabar
     * el registro
     */
    verificaUsuario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje = "";

        // si está editando
        if (document.getElementById("id_usuario").value != ""){

            // asigna en la variable de clase
            this.IdUsuario = document.getElementById("id_usuario").value;

        }

        // si no ingresó el nombre de usuario
        if (document.getElementById("nombre_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el nombre completo del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.NombreUsuario = document.getElementById("nombre_usuario").value;
        }

        // si no ingresó el usuario de la base
        if (document.getElementById("usuario_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el usuario para la base de datos";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("usuario_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.Usuario = document.getElementById("usuario_usuario").value;
        }

        // la profesión puede estar en blanco, pero si la ingresó
        // verifica que ingrese la matrícula
        if (document.getElementById("profesion_usuario").value != "" && document.getElementById("matricula_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Si el usuario es profesional debe ingresar<br>";
            mensaje += "el número de matrícula habilitante";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("matricula_usuario").focus();
            return false;

        }

        // asigna los valores en la clase (en todo caso cadena vacía)
        this.Profesion = document.getElementById("profesion_usuario").value;
        this.Matricula = document.getElementById("matricula_usuario").value;

        // si no ingresó el cargo
        if (document.getElementById("cargo_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el cargo del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cargo_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.CargoUsuario = document.getElementById("cargo_usuario").value;
        }

        // si no ingresó la institución
        if (document.getElementById("institucion_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la institución del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("institucion_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.Institucion = document.getElementById("institucion_usuario").value;
        }

        // si no seleccionó el departamento
        if (document.getElementById("departamento_usuario").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el departamento de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("departamento_usuario").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.IdDepartamento = document.getElementById("departamento_usuario").value;

        }

        // si no ingresó el teléfono
        if (document.getElementById("telefono_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el teléfono del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("telefono_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else  {
            this.Telefono = document.getElementById("telefono_usuario").value;
        }

        // si no ingresó el mail
        if (document.getElementById("mail_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la dirección de mail del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("mail_usuario").focus();
            return false;

        // verifica que el formato sea correcto
        } else if (!echeck(document.getElementById("mail_usuario").value)){

            // presenta el mensaje y retorna
            mensaje = "La dirección de correo parece incorrecta";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("mail_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.EMail = document.getElementById("mail_usuario").value;
        }

        // si no ingresó la dirección
        if (document.getElementById("direccion_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la dirección postal del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("direccion_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.Direccion = document.getElementById("direccion_usuario").value;
        }

        // si no ingresó el código postal
        if (document.getElementById("cod_postal_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el código postal";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("cod_postal_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.CodigoPostal = document.getElementById("cod_postal_usuario").value;
        }

        // si no seleccionó un laboratorio
        if (document.getElementById("id_laborat_usuario").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique a que laboratorio pertenece el usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("laboratorio_usuario").focus();
            return false;

        // asigna en las variables de clase
        } else {
            this.IdLaboratorio = document.getElementById("id_laborat_usuario").value;
            this.NombreLaboratorio = document.getElementById("laboratorio_usuario").value;
        }

        // si existen coordenadas
        if (document.getElementById("coordenadas_usuario").value != ""){
            this.Coordenadas = document.getElementById("coordenadas_usuario").value;
        } else {
            this.Coordenadas = "";
        }

        // si marcó que está activo
        if (document.getElementById("activo_usuario").checked){
            this.Activo = "Si";
        } else {
            this.Activo = "No";
        }

        // si marcó que es administrador
        if (document.getElementById("chAdministrador").checked){
            this.Administrador = "Si";
        } else {
            this.Administrador = "No";
        }

        // inicializamos la variable control
        var correcto = false;

        // si habilitó personas
        if (document.getElementById("chPersonas").checked){
            this.Personas = "Si";
            correcto = true;
        } else {
            this.Personas = "No";
        }

        // si habilitó chagas congénito
        if (document.getElementById("chCongenito").checked){
            this.Congenito = "Si";
            correcto = true;
        } else {
            this.Congenito = "No";
        }

        // si habilitó carga de resultados
        if (document.getElementById("chResultados").checked){
            this.Resultados = "Si";
            correcto = true;
        } else {
            this.Resultados = "No";
        }

        // si puede tomar muestras
        if (document.getElementById("chMuestras").checked){
            this.Muestras = "Si";
            correcto = true;
        } else {
            this.Muestras = "No";
        }

        // si habilitó entrevistas
        if (document.getElementById("chEntrevistas").checked){
            this.Entrevistas = "Si";
            correcto = true;
        } else {
            this.Entrevistas = "No";
        }

        // si puede editar tablas auxiliares
        if (document.getElementById("chAuxiliares").checked){
            this.Auxiliares = "Si";
            correcto = true;
        } else {
            this.Auxiliares = "No";
        }

        // si puede cargar protocolos
        if (document.getElementById("chProtocolos").checked){
            this.Protocolos = "Si";
            correcto = true;
        } else {
            this.Protocolos = "No";
        }

        // si puede editar el stock
        if (document.getElementById("chStock").checked){
            this.Stock = "Si";
            correcto = true;
        } else {
            this.Stock = "No";
        }

        // si puede editar historias clìnicas
        if(document.getElementById("chClinica").checked){
            this.Clinica = "Si";
            correcto = true;
        } else {
            this.Stock = "No";
        }

        // si puede editar usuarios
        if (document.getElementById("chUsuarios").checked){
            this.Usuarios = "Si";
            correcto = true;
        } else {
            this.Usuarios = "No";

        }

        // si no habilitó ninguna tabla
        if (!correcto){

            // presenta el mensaje y retorna
            mensaje = "Debe autorizar al menos una tabla";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no seleccionó la localidad
        if (document.getElementById("localidad_usuario").value == 0){

            // presenta el mensaje
            mensaje = "Seleccione una localidad de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("localidad_usuario").focus();
            return false;

        // asigna en la variable de clase
        } else {
            this.CodLoc = document.getElementById("localidad_usuario").value;
        }

        // si no seleccionó una provincia
        if (document.getElementById("jurisdiccion_usuario").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione una jurisdicción de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("jurisdiccion_usuario").focus();
            return false;

        // asignamos en las variables de clase
        } else {
            this.CodProv = document.getElementById("jurisdiccion_usuario").value;
        }

        // si no seleccionó país
        if (document.getElementById("pais_usuario").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el país del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("pais_usuario").focus();
            return false;

        // asigna en la variable
        } else {
            this.IdPais = document.getElementById("pais_usuario").value;
        }

        // si puede firmar y no subió la firma
        if (document.getElementById("chProtocolos").checked && this.Firma == ""){

            // presenta el aviso y continúa
            mensaje = "Recuerde que para firmar protocolos debe\n";
            mensaje += "subir la firma digitalizada del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // si está dando altas
        if (document.getElementById("id_usuario").value == ""){

            // llamamos por callbak para verificar el usuario
            this.validaUsuario(function(usuarioCorrecto){

                // si está repetido
                if (!usuarioCorrecto){

                    // presentamos el mensaje
                    mensaje = "Ese nombre de usuario ya se \n";
                    mensaje += "encuentra en uso. Verifique";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

            // llamamos por callback para verificar el mail
            this.validaMail(function(mailCorrecto){

                // si está repetido
                if (!mailCorrecto){

                    // presenta el mensaje
                    mensaje = "Ese correo electrónico ya \n";
                    mensaje += "se encuentra en uso.";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                }

            });

        }

        // si hay comentarios los asigna
        this.Comentarios = CKEDITOR.instances['observacionesusuario'].getData();

        // grabamos el registro
        this.grabaUsuario();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {boolean} mailCorrecto
     * Método utilizado en las altas llamado por callback que verifica
     * que el correo no se encuentre repetido
     */
    validaMail(mailCorrecto){

        // inicializamos la variable de clase
        this.MailCorrecto = false;

        // verificamos si se encuentra repetido
        // lo llamamos sincrónico
        $.ajax({
            url: "usuarios/valida_mail.php?mail="+document.getElementById("mail_usuario").value,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si encontró
                if (data.Registros != 0){

                    // setea la variable control
                    usuarios.MailCorrecto = false;

                // si no encontró
                } else {

                    // setea la variable control
                    usuarios.MailCorrecto = true;

                }

        }});

        // retornamos
        mailCorrecto(this.MailCorrecto);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {boolean} usuarioCorrecto
     * Método utilizado en las altas, llamado por callback para verificar
     * que el nombre de usuario no se encuentre repetido
     */
    validaUsuario(usuarioCorrecto){

        // inicializamos la variable de clase
        this.UsuarioCorrecto = false;

        // verificamos si se encuentra repetido
        // lo llamamos sincrónico
        $.ajax({
            url: "usuarios/valida_usuario.php?usuario="+document.getElementById("usuario_usuario").value,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si encontró
                if (data.Registros != 0){

                    // setea la variable control
                    usuarios.UsuarioCorrecto = false;

                // si no encontró
                } else {

                    // setea la variable control
                    usuarios.UsuarioCorrecto = true;

                }

        }});

        // retornamos
        usuarioCorrecto(this.UsuarioCorrecto);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} el resultado de la operación
     * Método que a partir de las variables de clase, crea el objeto formdata
     * y lo envía por ajax al servidor
     */
    grabaUsuario(){

        // si el mail o el correo son incorrectos
        if (!this.MailCorrecto || !this.UsuarioCorrecto){

            // simplemente retornamos
            return false;

        }

        // declaramos el formulario
        var formUsuario = new FormData();
        var mensaje;

        // si está editando
        if (this.IdUsuario != ""){
            formUsuario.append("Id", this.IdUsuario);
        }

        // agrega los elementos
        formUsuario.append("Nombre", this.NombreUsuario);
        formUsuario.append("Cargo", this.CargoUsuario);
        formUsuario.append("Profesion", this.Profesion);
        formUsuario.append("Matricula", this.Matricula);
        formUsuario.append("Institucion", this.Institucion);
        formUsuario.append("Telefono", this.Telefono);
        formUsuario.append("Mail", this.EMail);
        formUsuario.append("Direccion", this.Direccion);
        formUsuario.append("CodigoPostal", this.CodigoPostal);
        formUsuario.append("IdLaboratorio", this.IdLaboratorio);
        formUsuario.append("IdDepartamento", this.IdDepartamento);
        formUsuario.append("Usuario", this.Usuario);
        formUsuario.append("Coordenadas", this.Coordenadas);
        formUsuario.append("Activo", this.Activo);
        formUsuario.append("Administrador", this.Administrador);
        formUsuario.append("Personas", this.Personas);
        formUsuario.append("Congenito", this.Congenito);
        formUsuario.append("Resultados", this.Resultados);
        formUsuario.append("Muestras", this.Muestras);
        formUsuario.append("Entrevistas", this.Entrevistas);
        formUsuario.append("Auxiliares", this.Auxiliares);
        formUsuario.append("Protocolos", this.Protocolos);
        formUsuario.append("Stock", this.Stock);
        formUsuario.append("Clinica", this.Clinica);
        formUsuario.append("Usuarios", this.Usuarios);
        formUsuario.append("CodLoc", this.CodLoc);
        formUsuario.append("CodProv", this.CodProv);
        formUsuario.append("IdPais", this.IdPais);
        formUsuario.append("Comentarios", this.Comentarios);

        // si subió una firma
        if (this.Firma != ""){
            formUsuario.append("Firma", this.Firma);
        }

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "usuarios/graba_usuario.php",
            data: formUsuario,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id === false){

                    // presenta el mensaje
                    mensaje = "Ha ocurrido un error";
                    new jBox('Notice', {content: mensaje, color: 'red'});

                } else {

                    // almacenamos la id en el formulario para poder
                    // agregar unidades inmediatamente
                    document.getElementById("id_usuario").value = data.Id;
                    this.IdUsuario = data.Id;

                    // presenta el mensaje
                    mensaje = "Registro grabado ... ";
                    new jBox('Notice', {content: mensaje, color: 'green'});

                    // setea el foco
                    document.getElementById("nombre_usuario").focus();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que simplemente reinicia el formulario de usuarios
     */
    cancelaUsuario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // reiniciamos las variables
        this.initUsuarios();

        // reiniciamos el formulario y fijamos el foco
        document.form_usuarios.reset();
        document.getElementById("nombre_usuario").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el formulario de búsqueda de usuarios
     */
    buscaUsuario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el contenido
        var contenido = "<p align='justify'>";
        contenido += "Ingrese el texto a buscar (recuerde que el sistema<br>";
        contenido += "Explorará también en el nombre de la Institución y<br>";
        contenido += "la Jurisdicción no solo en el Nombre)</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='text' ";
        contenido += "       name='buscarUsuario' ";
        contenido += "       id='buscarUsuario' ";
        contenido += "       placeholder='Texto a buscar' ";
        contenido += "       size='35'></p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' ";
        contenido += "       name='btnBuscaUsuario' ";
        contenido += "       id='btnBuscaUsuario' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       value='Buscar' ";
        contenido += "       title='Pulse para iniciar la búsqueda' ";
        contenido += "       onClick='usuarios.encuentraUsuario()'></p>";

        // abrimos el layer
        this.layerLaboratorio = new jBox('Modal', {
                                          animation: 'flip',
                                          closeOnEsc: true,
                                          closeOnClick: false,
                                          closeOnMouseleave: false,
                                          closeButton: true,
                                          repositionOnContent: true,
                                          overlay: false,
                                          title: 'Buscar Usuario',
                                          onCloseComplete: function(){
                                            this.destroy();
                                          },
                                          draggable: 'title',
                                          theme: 'TooltipBorder',
                                          content: contenido
                                    });
        this.layerLaboratorio.open();

        // fijamos el foco
        document.getElementById('buscarUsuario').focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento del formulario de búsqueda, verifica el
     * formulario y luego lo envía por ajax al servidor
     */
    encuentraUsuario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;
        var texto = document.getElementById("buscarUsuario").value;

        // verificamos que halla ingresado un texto
        if (texto == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar un texto a buscar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("buscarUsuario").focus();
            return false;

        }

        // destruimos el layer si existe
        if (this.layerLaboratorio != ""){
            this.layerLaboratorio.destroy();
        }

        // abrimos el layer y le pasamos los argumentos
        this.layerLaboratorio = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    title: 'Búsqueda de Usuarios',
                                    draggable: 'title',
                                    repositionOnContent: true,
                                    theme: 'TooltipBorder',
                                    ajax: {
                                        url: 'usuarios/buscar.php?texto='+texto,
                                        reload: 'strict'
                                    }
                                });
        this.layerLaboratorio.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de usuarios
     */
    nuevoUsuario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // reiniciamos el formulario
        document.getElementById("id_usuario").value = "";
        document.getElementById("nombre_usuario").value = "";
        document.getElementById("profesion_usuario").value = "";
        document.getElementById("matricula_usuario").value = "";
        document.getElementById("usuario_usuario").value = "";
        document.getElementById("cargo_usuario").value = "";
        document.getElementById("institucion_usuario").value = "";
        document.getElementById("telefono_usuario").value = "";
        document.getElementById("mail_usuario").value = "";
        document.getElementById("activo_usuario").checked = false;
        document.getElementById("chAdministrador").checked = false;
        document.getElementById("direccion_usuario").value = "";
        document.getElementById("cod_postal_usuario").value = "";

        // cargamos y preseleccionamos el combo de departamento
        departamentos.nominaDepartamentos("departamento_usuario");

        // el combo de país lo predeterminamos a argentina
        document.getElementById("pais_usuario").value = 1;

        // provincia y localidad debería limpiarlos con el onchange

        // seguimos limpiando
        document.getElementById("autorizo_usuario").value = sessionStorage.getItem("Usuario");
        document.getElementById("alta_usuario").value = fechaActual();
        document.getElementById("coordenadas_usuario").value = "";
        document.getElementById("laboratorio_usuario").value = "";
        document.getElementById("id_laborat_usuario").value = "";
        document.getElementById("chUsuarios").checked = false;
        document.getElementById("chPersonas").checked = false;
        document.getElementById("chCongenito").checked = false;
        document.getElementById("chResultados").checked = false;
        document.getElementById("chMuestras").checked = false;
        document.getElementById("chEntrevistas").checked = false;
        document.getElementById("chAuxiliares").checked = false;
        document.getElementById("chProtocolos").checked = false;
        document.getElementById("chStock").checked = false;
        document.getElementById("chClinica").checked = false;
        document.getElementById('observacionesusuario').value = "";
        CKEDITOR.instances['observacionesusuario'].setData("");

        // limpiamos la firma
        document.getElementById("firma_usuario").src = "imagenes/imagen_no_disponible.gif";

        // ahora inicializamos las variables de clase para
        // estar seguros que no arrastramos nada
        this.initUsuarios();

        // fijamos el foco
        document.getElementById("nombre_usuario").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idusuario clave del usuario a obtener
     * Método que recibe como parámetro la id de un registro, llama la rutina php
     * por ajax y asigna en las variables de clase los valores del registro
     */
    obtenerUsuario(idusuario){

        // reiniciamos la sesión
        sesion.reiniciar();

        // lo llamamos asincrónico
        $.ajax({
            url: "usuarios/getusuario.php?idusuario="+idusuario,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // cargamos las variables de clase
                usuarios.setIdUsuario(data.IdUsuario);
                usuarios.setNombre(data.Nombre);
                usuarios.setProfesion(data.Profesion);
                usuarios.setMatricula(data.Matricula);
                usuarios.setCargo(data.Cargo);
                usuarios.setInstitucion(data.Institucion);
                usuarios.setTelefono(data.Telefono);
                usuarios.setMail(data.Email);
                usuarios.setDireccion(data.Direccion);
                usuarios.setCodigoPostal(data.CodigoPostal);
                usuarios.setIdLaboratorio(data.IdLaboratorio);
                usuarios.setNombreLaboratorio(data.Laboratorio);
                usuarios.setIdDepartamento(data.IdDepartamento);
                usuarios.setDepartamento(data.Departamento);
                usuarios.setUsuario(data.Usuario);
                usuarios.setCoordenadas(data.Coordenadas);
                usuarios.setActivo(data.Activo);
                usuarios.setFechaAlta(data.FechaAlta);
                usuarios.setAutorizo(data.Autorizo);
                usuarios.setAdministrador(data.Administrador);
                usuarios.setPersonas(data.Personas);
                usuarios.setCongenito(data.Congenito);
                usuarios.setResultados(data.Resultados);
                usuarios.setMuestras(data.Muestras);
                usuarios.setEntrevistas(data.Entrevistas);
                usuarios.setAuxiliares(data.Auxiliares);
                usuarios.setProtocolos(data.Protocolos);
                usuarios.setStock(data.Stock);
                usuarios.setClinica(data.Clinica);
                usuarios.setUsuarios(data.Usuarios);
                usuarios.setLocalidad(data.Localidad);
                usuarios.setCodLoc(data.CodLoc);
                usuarios.setProvincia(data.Provincia);
                usuarios.setCodProv(data.CodProv);
                usuarios.setIdPais(data.IdPais);
                usuarios.setPais(data.Pais);
                usuarios.setFirma(data.Firma);
                usuarios.setComentarios(data.Comentarios);

                // ahora mostramos el registro
                usuarios.mostrarUsuario();

        }});

    }

    // Métodos de asignación de valores en las variables de clase, los
    // utilizamos para adjudicarlos desde la rutina ajax
    setIdUsuario(id){
        this.IdUsuario = id;
    }
    setNombre(nombre){
        this.NombreUsuario = nombre;
    }
    setProfesion(profesion){
        this.Profesion = profesion;
    }
    setMatricula(matricula){
        this.Matricula = matricula;
    }
    setCargo(cargo){
        this.CargoUsuario = cargo;
    }
    setInstitucion(institucion){
        this.Institucion = institucion;
    }
    setTelefono(telefono){
        this.Telefono = telefono;
    }
    setMail(mail){
        this.EMail = mail;
    }
    setDireccion(direccion){
        this.Direccion = direccion;
    }
    setCodigoPostal(codigopostal){
        this.CodigoPostal = codigopostal;
    }
    setIdLaboratorio(idlaboratorio){
        this.IdLaboratorio = idlaboratorio;
    }
    setNombreLaboratorio(nombrelaboratorio){
        this.NombreLaboratorio = nombrelaboratorio;
    }
    setIdDepartamento(iddepartamento){
        this.IdDepartamento = iddepartamento;
    }
    setDepartamento(departamento){
        this.Departamento = departamento;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setCoordenadas(coordenadas){
        this.Coordenadas = coordenadas;
    }
    setActivo(activo){
        this.Activo = activo;
    }
    setFechaAlta(fechaalta){
        this.FechaAlta = fechaalta;
    }
    setAutorizo(autorizo){
        this.Autorizo = autorizo;
    }
    setAdministrador(administrador){
        this.Administrador = administrador;
    }
    setPersonas(personas){
        this.Personas = personas;
    }
    setCongenito(congenito){
        this.Congenito = congenito;
    }
    setResultados(resultados){
        this.Resultados = resultados;
    }
    setMuestras(muestras){
        this.Muestras = muestras;
    }
    setEntrevistas(entrevistas){
        this.Entrevistas = entrevistas;
    }
    setAuxiliares(auxiliares){
        this.Auxiliares = auxiliares;
    }
    setProtocolos(protocolos){
        this.Protocolos = protocolos;
    }
    setStock(stock){
        this.Stock = stock;
    }
    setUsuarios(usuarios){
        this.Usuarios = usuarios;
    }
    setClinica(clinica){
        this.Clinica = clinica;
    }
    setLocalidad(localidad){
        this.Localidad = localidad;
    }
    setCodLoc(codloc){
        this.CodLoc = codloc;
    }
    setProvincia(provincia){
        this.Provincia = provincia;
    }
    setCodProv(codprov){
        this.CodProv = codprov;
    }
    setIdPais(idpais){
        this.IdPais = idpais;
    }
    setPais(pais){
        this.Pais = pais;
    }
    setFirma(firma){

        // si recibió algo
        if (firma != null){
            this.Firma = firma;
        } else {
            this.Firma ='../imagenes/imagen_no_disponible.gif';
        }

    }
    setComentarios(comentarios){
        this.Comentarios = comentarios;
    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase asigna los valores en
     * el formulario
     */
    mostrarUsuario(){

        // cargamos en el formulario los valores de las
        // variables de clase
        document.getElementById("id_usuario").value = this.IdUsuario;
        document.getElementById("nombre_usuario").value = this.NombreUsuario;
        document.getElementById("usuario_usuario").value = this.Usuario;
        document.getElementById("profesion_usuario").value = this.Profesion;
        document.getElementById("matricula_usuario").value = this.Matricula;
        document.getElementById("cargo_usuario").value = this.CargoUsuario;
        document.getElementById("institucion_usuario").value = this.Institucion;
        document.getElementById("telefono_usuario").value = this.Telefono;
        document.getElementById("mail_usuario").value = this.EMail;
        document.getElementById("direccion_usuario").value = this.Direccion;
        document.getElementById("cod_postal_usuario").value = this.CodigoPostal;

        // cargamos el departamento del usuario
        departamentos.nominaDepartamentos("departamento_usuario", this.IdLaboratorio, this.IdDepartamento);

        // cargamos la clave y el nombre del laboratorio que pertenece
        document.getElementById("id_laborat_usuario").value = this.IdLaboratorio;
        document.getElementById("laboratorio_usuario").value = this.NombreLaboratorio;

        // si existen las coordenadas
        if (this.Coordenadas != ""){

            // las cargamos y nos aseguramos de mostrar el botón
            document.getElementById("coordenadas_usuario").value = this.Coordenadas;
            $("#btnMapaUsuario").show();

        // si no están declaradas
        } else {

            // apagamos el botón y nos aseguramos de no arrastrar
            // ningún valor
            document.getElementById("coordenadas_usuario").value = "";
            $("#btnMapaUsuario").hide();

        }

        // el checbox de activo
        if (this.Activo == "Si"){
            document.getElementById("activo_usuario").checked = true;
        } else {
            document.getElementById("activo_usuario").checked = false;
        }

        // cargamos la fecha de alta y el usuario que autorizó
        document.getElementById("alta_usuario").value = this.FechaAlta;
        document.getElementById("autorizo_usuario").value = this.Autorizo;

        // si es administrador
        if (this.Administrador == "Si"){
            document.getElementById("chAdministrador").checked = true;
        } else {
            document.getElementById("chAdministrador").checked = false;
        }

        // si puede editar pacientes
        if (this.Personas == "Si"){
            document.getElementById("chPersonas").checked = true;
        } else {
            document.getElementById("chPersonas").checked = false;
        }

        // si puede cargar congénito
        if (this.Congenito == "Si"){
            document.getElementById("chCongenito").checked = true;
        } else {
            document.getElementById("chCongenito").checked = false;
        }

        // si puede cargar resultados
        if (this.Resultados == "Si"){
            document.getElementById("chResultados").checked = true;
        } else {
            document.getElementById("chResultados");
        }

        // si puede tomar muestras
        if (this.Muestras == "Si"){
            document.getElementById("chMuestras").checked = true;
        } else {
            document.getElementById("chMuestras").checked = false;
        }

        // si puede realizar entrevistas
        if (this.Entrevistas == "Si"){
            document.getElementById("chEntrevistas").checked = true;
        } else {
            document.getElementById("chEntrevistas").checked = false;
        }

        // si puede editar tablas auxiliares
        if (this.Auxiliares == "Si"){
            document.getElementById("chAuxiliares").checked = true;
        } else {
            document.getElementById("chAuxiliares").checked = false;

        }

        // si puede firmar protocolos
        if (this.Protocolos == "Si"){
            document.getElementById("chProtocolos").checked = true;
        } else {
            document.getElementById("chProtocolos").checked = false;
        }

        // si puede eidtar el stock
        if (this.Stock == "Si"){
            document.getElementById("chStock").checked = true;
        } else {
            document.getElementById("chStock").checked = false;
        }

        // si puede editar clìnica médica
        if (this.Clinica == "Si"){
            document.getElementById("chClinica").checked = true;
        } else {
            document.getElementById("chClinica").checked = false;
        }

        // si puede editar usuarios
        if (this.Usuarios == "Si"){
            document.getElementById("chUsuarios").checked = true;
        } else {
            document.getElementById("chUsuarios").checked = false;
        }

        // cargamos el combo de país
        this.cargaPaises(this.IdPais);

        // cargamos el combo de jurisdicción
        this.cargaJurisdicciones(this.IdPais, this.CodProv);

        // cargamos las localidades
        this.cargaLocalidades(this.CodProv, this.CodLoc);

        // aquí tenemos que cargar la firma
        document.getElementById('firma_usuario').src = this.Firma;

        // asignamos los comentarios
        document.getElementById("observacionesusuario").value = this.Comentarios;
        CKEDITOR.instances['observacionesusuario'].setData(this.Comentarios);

        // verificamos el nivel de acceso

        // si no es administrador y no puede editar usuarios
        if (sessionStorage.getItem("Administrador") == "No" && sessionStorage.getItem("Usuarios") == "No"){

            // fija como solo lectura los checbox
            document.getElementById("activo_usuario").disabled = true;
            document.getElementById("chAdministrador").disabled = true;
            document.getElementById("chUsuarios").disabled = true;
            document.getElementById("chPersonas").disabled = true;
            document.getElementById("chCongenito").disabled = true;
            document.getElementById("chResultados").disabled = true;
            document.getElementById("chMuestras").disabled = true;
            document.getElementById("chEntrevistas").disabled = true;
            document.getElementById("chAuxiliares").disabled = true;
            document.getElementById("chProtocolos").disabled = true;
            document.getElementById("chClinica").disabled = true;
            document.getElementById("chStock").disabled = true;

            // fija de solo lectura el campo de laboratorio
            document.getElementById("laboratorio_usuario").readOnly = true;

            // oculta el botón buscar laboratorio
            $("#btnBuscaLabUsuario").hide();

            // si es el registro del usuario
            if (sessionStorage.getItem("ID") == this.Id){

                // permite grabar el registro
                $("#btnGrabaUsuario").show();
                $("#btnCancelaUsuario").show();

            // si es el registro de otro usuario
            } else {

                // oculta los botones de grabación
                $("#btnGrabaUsuario").hide();
                $("#btnCancelaUsuario").hide();
            }

        // si es administrador y puede modificar usuarios
        } else {

            // nos aseguramos que los checbox sean de lectura / escritura
            document.getElementById("activo_usuario").disabled = false;
            document.getElementById("chAdministrador").disabled = false;
            document.getElementById("chUsuarios").disabled = false;
            document.getElementById("chPersonas").disabled = false;
            document.getElementById("chCongenito").disabled = false;
            document.getElementById("chResultados").disabled = false;
            document.getElementById("chMuestras").disabled = false;
            document.getElementById("chEntrevistas").disabled = false;
            document.getElementById("chAuxiliares").disabled = false;
            document.getElementById("chProtocolos").disabled = false;
            document.getElementById("chStock").disabled = false;
            document.getElementById("chClinica").disabled = false;

            // nos aseguramos que el nombre de laboratorio sea de lectura / escritura
            document.getElementById("laboratorio_usuario").readOnly = false;

            // muestra el botón buscar laboratorio
            $("#btnBuscaLabUsuario").show();

            // muestra los botones
            $("#btnGrabaUsuario").show();
            $("#btnCancelaUsuario").show();

        }

        // destruimos el layer si fue llamado desde la búsqueda
        if (typeof(this.layerLaboratorio) != "string"){
            this.layerLaboratorio.destroy();
        }

        // fija el foco en el primer elemento
        document.getElementById("nombre_usuario").focus();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpais clave del país
     * Método que llama al diccionario por ajax y carga en el formulario
     * la nómina de países, si recibe la clave del país selecciona este
     * por defecto
     */
    cargaPaises(idpais){

        // llamamos la clase de países
        paises.listaPaises("pais_usuario", idpais);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpais clave del país
     * @param {string} codprov clave indec de la provincia
     * Método que recibe como parámetro la clave de un país y carga en el
     * select de provincias la nómina, si no recibe nada asume que es
     * llamado en el evento onchange y obtiene la id del valor seleccionado
     * en el select de países
     * Igualmente, si recibe la clave de la jurisdicción la preselecciona
     * asume que fue llamado en la carga del formulario
     */
    cargaJurisdicciones(idpais, codprov){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió nada
        if (typeof(idpais) == "undefined"){

            // lo asigna
            idpais = document.getElementById("pais_usuario").value;

        }

        // llama la clase de provincias
        provincias.listaJurisdicciones("jurisdiccion_usuario", idpais, codprov);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idjurisdiccion clave indec de la provincia
     * @param {string} codloc localidad preseleccionada
     * Método que recibe como parámetro la clave indec de una jurisdicción
     * y carga en el select de localidades la nómina, si no recibe nada
     * asume que fue llamado en el evento onchange
     * Si recibe la clave de la localidad asume que está editando un registro
     * y selecciona esa localidad por defecto
     */
    cargaLocalidades(idjurisdiccion, codloc){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió nada
        if (typeof(idjurisdiccion) == "undefined"){

            // asigna la variable
            idjurisdiccion = document.getElementById("jurisdiccion_usuario").value;

        }

        // llamamos la clase de localidades
        ciudades.nominaLocalidades("localidad_usuario", idjurisdiccion, codloc);


    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento click del botón buscar laboratorio,
     * verifica que el usuario halla ingresado un string y abre el
     * cuadro de diálogo con los laboratorios coincidentes
     */
    buscaLaboratorio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var laboratorio = document.getElementById("laboratorio_usuario").value;
        var pais = document.getElementById("pais_usuario").value;
        var jurisdiccion = document.getElementById("jurisdiccion_usuario").value;
        var mensaje;

        // verificamos que halla ingresado texto
        if (laboratorio == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar parte del nombre del laboratorio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("laboratorio_usuario").focus();
            return false;

        }

        // verifica que exista un país seleccionado
        if (pais == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar el país del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("pais_usuario").focus();
            return false;

        }

        // verifica se halla seleccionado una jurisdicción
        if (jurisdiccion == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar la jurisdicción del usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("jurisdiccion_usuario").focus();
            return false;

        }

        // cargamos el formulario de búsqueda
        this.layerLaboratorio = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    repositionOnContent: true,
                                    overlay: false,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    title: 'Laboratorio Usuario',
                                    draggable: 'title',
                                    theme: 'TooltipBorder',
                                    ajax: {
                                        url: 'usuarios/sel_laboratorio.php?pais='+pais+'&jurisdiccion='+jurisdiccion+'&laboratorio='+laboratorio,
                                        reload: 'strict'
                                    }
                                });
        this.layerLaboratorio.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idlaboratorio clave del laboratorio
     * @param {string} laboratorio nombre completo del laboratorio
     * Método llamado al seleccionar un laboratorio autorizado, recibe como
     * parámetro la id del laboratorio y el nombre completo del mismo, carga en
     * el formulario principal los datos seleccionados
     */
    cargaLaboratorio(idlaboratorio, laboratorio){

        // cargamos en el formulario
        document.getElementById("id_laborat_usuario").value = idlaboratorio;
        document.getElementById("laboratorio_usuario").value = laboratorio;

        // si cambió el laboratorio 
        if (this.IdLaboratorio != idlaboratorio){

            // actualizamos el combo de departamento
            departamentos.nominaDepartamentos("departamento_usuario", idlaboratorio);
            
        }

        // destruimos el layer
        this.layerLaboratorio.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre la imagen de la firma, simplemente
     * dispara el evento click de explorador de archivos
     */
    cargaFirma(){

        // disparamos el evento
        document.getElementById("archivo_firma").click();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método: Procedimiento llamado en el evento onClick de la subida de
     * firma del usuario, sube el archivo con la imagen y la muestra en el
     * formulario sin cargarla en la base
    */
    subirFirma(){

        // obtenemos el archivo seleccionado
        var file = $("#archivo_firma")[0].files[0];

        // si canceló
        if (file === undefined){

            // simplemente retornamos
            return false;

        }

        // usamos la nueva característica filereader que nos permite
        // cargar un archivo en el navegador y mostrarlo sin
        // subirlo al servidor, si es una imagen ya lo convierte
        // en base 64
        var reader = new FileReader();

        // evento llamado cuando termina la carga del archivo
        reader.onload = function (e) {

            // lo asignamos a la variable de clase
            var firma = e.target.result;

            // e.target.result contents the base64 data from the image uploaded
            document.getElementById('firma_usuario').src = firma;

            // asignamos en la variable de clase porque aquí estamos
            // en un espacio privado (dentro de function)
            usuarios.setFirma(firma);

        };

        // definimos el elemento del formulario a leer
        var archivo = document.getElementById("archivo_firma");
        reader.readAsDataURL(archivo.files[0]);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un elemento html de la página
     * @param {int} idusuario clave del usuario preseleccionado
     * Método que recibe como parámetro la id de un elemento de un formulario
     * y carga en ese objeto la nómina de usuarios de ese mismo laboratorio
     * si además recibe como parámetro la clave de un usuario lo preselecciona
     */
    nominaUsuarios(idelemento, idusuario){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "usuarios/lista_usuarios.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idusuario) != "undefined"){

                        // si coincide
                        if (data[i].Id == idusuario){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Usuario + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Usuario + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Usuario + "</option>");
                    }

                }

        }});

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un elemento html de la página
     * @param {int} idusuario clave del usuario preseleccionado
     * Método que recibe como parámetro la id de un elemento de un formulario
     * y carga en ese objeto la nómina de profesionales de ese mismo laboratorio
     * si además recibe como parámetro la clave de un usuario lo preselecciona
     */
    nominaProfesionales(idelemento, idusuario){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "usuarios/lista_profesionales.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idusuario) != "undefined"){

                        // si coincide
                        if (data[i].Id == idusuario){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Usuario + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Usuario + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Usuario + "</option>");
                    }

                }

        }});

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un elemento html de la página
     * @param {int} idusuario clave del usuario preseleccionado
     * Método que recibe como parámetro la id de un elemento de un formulario
     * y carga en ese objeto la nómina de profesionales de ese mismo laboratorio
     * que además tienen horarios asignados, si además recibe como parámetro la
     * clave de un usuario lo preselecciona
     */
    nominaProfHorarios(idelemento, idusuario){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "usuarios/lista_profhorarios.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idusuario) != "undefined"){

                        // si coincide
                        if (data[i].Id == idusuario){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Usuario + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Usuario + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Usuario + "</option>");
                    }

                }

        }});

    }

}