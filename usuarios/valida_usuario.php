<?php

/**
 *
 * usuarios/valida_usuario.php
 *
 * @package     Diagnostico
 * @subpackage  Usuarios
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (11/04/2018)
 * @copyright   Copyright (c) 2017, INP
 * 
 * Método que recibe por get el nombre de un usuario y verifica que este no 
 * se encuentre declarado, utilizado en las altas para evitar nombres 
 * repetidos, retorna el número de registros encontrados
 * 
 * @param $_GET["usuario"]
 *
*/

// instanciamos la clase
require_once("usuarios.class.php");
$usuario = new Usuarios();

// verificamos si existe
$registros = $usuario->verificaUsuario($_GET["usuario"]);

// retornamos el número de registros
echo json_encode(array("Registros" => $registros));

?>