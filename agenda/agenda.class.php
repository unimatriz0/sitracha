<?php

/**
 *
 * Class Agenda | agenda/agenda.class.php
 *
 * @package     Diagnostico
 * @subpackage  Agenda
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (01/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones de las citas de los
 * pacientes
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Agenda {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Id;                     // clave del registro
    protected $IdProfesional;          // clave del profesional
    protected $Profesional;            // nombre del profesional
    protected $Protocolo;              // clave del paciente
    protected $Paciente;               // nombre del paciente
    protected $Documento;              // número de documento del paciente
    protected $Fecha;                  // fecha de la cita
    protected $Hora;                   // hora de la cita
    protected $Concurrio;              // 0 no 1 si
    protected $FechaAlta;              // fecha de alta del registro
    protected $Usuario;                // usuario que ingresó el registro
    protected $IdUsuario;              // clave del usuario actual
    protected $Link;                   // puntero a la base de datos

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->Id = 0;
        $this->IdProfesional = 0;
        $this->Profesional = "";
        $this->Protocolo = 0;
        $this->Fecha = "";
        $this->Hora = "";
        $this->Concurrio = 0;

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setIdProfesional($idprofesional){
        $this->IdProfesional = $idprofesional;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }
    public function setHora($hora){
        $this->Hora = $hora;
    }
    public function setConcurrio($concurrio){
        $this->Concurrio = $concurrio;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getIdProfesional(){
        return $this->IdProfesional;
    }
    public function getProfesional(){
        return $this->Profesional();
    }
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getDocumento(){
        return $this->Documento;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getHora(){
        return $this->Hora;
    }
    public function getConcurrio(){
        return $this->Concurrio;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que recibe como parámetro una fecha y retorna el vector
     * con todas las citas para esa fecha
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $fecha - fecha a consultar
     * @return array
     */
    public function nominaCitas($fecha){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_citas.id AS Id,
                            diagnostico.v_citas.profesional AS Profesional,
                            diagnostico.v_citas.protocolo AS Protocolo,
                            diagnostico.v_citas.paciente AS Paciente,
                            diagnostico.v_citas.documento AS Documento,
                            diagnostico.v_citas.fecha AS Fecha,
                            diagnostico.v_citas.hora AS Hora,
                            diagnostico.v_citas.concurrio AS Concurrio,
                            diagnostico.v_citas.usuario AS Usuario
                     FROM diagnostico.v_citas
                     WHERE STR_TO_DATE(diagnostico.v_citas.fecha, '%d/%m/%Y') = STR_TO_DATE($fecha, '%d/%m/%Y')
                     ORDER BY STR_TO_DATE(diagnostico.v_citas.hora, '%H:%i); ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método que retorna un array con la nómina de citas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaAgenda(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_citas.id AS Id,
                            diagnostico.v_citas.profesional AS Profesional,
                            diagnostico.v_citas.protocolo AS Protocolo,
                            diagnostico.v_citas.paciente AS Paciente,
                            diagnostico.v_citas.documento AS Documento,
                            diagnostico.v_citas.fecha AS Fecha,
                            diagnostico.v_citas.hora AS Hora,
                            diagnostico.v_citas.concurrio AS Concurrio,
                            diagnostico.v_citas.usuario AS Usuario
                     FROM diagnostico.v_citas
                     ORDER BY STR_TO_DATE(diagnostico.v_citas.fecha, '%d/%m/%Y'),
                              STR_TO_DATE(diagnostico.v_citas.hora, '%H:%i'); ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * Método que ejecuta la consulta de inserción o edición en
     * el servidor, retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaCita(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaCita();
        } else {
            $this->editaCita();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevaCita(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.citas
                            (profesional,
                             protocolo,
                             fecha,
                             hora,
                             concurrio,
                             usuario)
                            VALUES
                            (:profesional,
                             :protocolo,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             STR_TO_DATE(:hora, '%H:%i'),
                             :concurrio,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":profesional",  $this->IdProfesional);
        $psInsertar->bindParam(":protocolo",    $this->Protocolo);
        $psInsertar->bindParam(":fecha",        $this->Fecha);
        $psInsertar->bindParam(":hora",         $this->Hora);
        $psInsertar->bindParam(":concurrio",    $this->Concurrio);
        $psInsertar->bindParam(":usuario",      $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaCita(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.citas SET
                            profesional = :profesional,
                            protocolo = :protocolo,
                            fecha = :fecha,
                            hora = :hora,
                            concurrio = :concurrio,
                            usuario = :usuario
                     WHERE diagnostico.citas.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":profesional",  $this->IdProfesional);
        $psInsertar->bindParam(":protocolo",    $this->Protocolo);
        $psInsertar->bindParam(":fecha",        $this->Fecha);
        $psInsertar->bindParam(":hora",         $this->Hora);
        $psInsertar->bindParam(":concurrio",    $this->Concurrio);
        $psInsertar->bindParam(":usuario",      $this->IdUsuario);
        $psInsertar->bindParam(":id",           $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y asigna los datos del mismo en las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idcita - clave del registro
     */
    public function getDatosCita($idcita){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_citas.id AS id,
                            diagnostico.v_citas.idprofesional AS idprofesional,
                            diagnostico.v_citas.protocolo AS protocolo,
                            diagnostico.v_citas.paciente AS paciente,
                            diagnostico.v_citas.documento AS documento,
                            diagnostico.v_citas.fecha AS fecha,
                            diagnostico.v_citas.hora AS hora,
                            diagnostico.v_citas.concurrio AS concurrio,
                            diagnostico.v_citas.usuario AS usuario,
                            diagnostico.v_citas.alta AS alta
                     FROM diagnostico.v_citas
                     WHERE diagnostico.v_citas.id = '$idcita'; ";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // asignamos en la clase
        $this->Id = $id;
        $this->IdProfesional = $idprofesional;
        $this->Protocolo = $protocolo;
        $this->Paciente = $paciente;
        $this->Documento = $documento;
        $this->Fecha = $fecha;
        $this->Hora = $hora;
        $this->Concurrio = $concurrio;
        $this->Usuario = $usuario;
        $this->FechaAlta = $alta;

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idcita - clave del registro
     */
    public function borraCita($idcita){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.citas
                     WHERE diagnostico.citas.id = '$idcita';";
        $this->Link->exec($consulta);

    }

}
?>