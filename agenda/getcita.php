<?php

/**
 *
 * getcita | agenda/getcita.php
 *
 * @package     Diagnostico
 * @subpackage  Agenda
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe como parámetro la clave de un registro y
 * retorna un array json con los datos de una cita
 *
*/

// incluimos e instanciamos la clase
require_once("agenda.class.php");
$agenda = new Agenda();

// obtenemos la nómina
$agenda->getDatosCita($_GET["id"]);

// retornamos el vector
echo json_encode(array("Id" =>            $agenda->getId(),
                       "IdProfesional" => $agenda->getIdProfesional(),
                       "Paciente" =>      $agenda->getPaciente(),
                       "Documento" =>     $agenda->getDocumento(),
                       "Fecha" =>         $agenda->getFecha(),
                       "Hora" =>          $agenda->getHora(),
                       "Concurrio" =>     $agenda->getConcurrio(),
                       "Usuario" =>       $agenda->getUsuario(),
                       "FechaAlta" =>     $agenda->getFechaAlta()));

?>