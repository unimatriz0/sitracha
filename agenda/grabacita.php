<?php

/**
 *
 * grabacita | agenda/grabacita.php
 *
 * @package     Diagnostico
 * @subpackage  Agenda
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por post los datos de una cita y ejecuta
 * la consulta en el servidor
 *
*/

// incluimos e instanciamos la clase
require_once("agenda.class.php");
$agenda = new Agenda();

// asignamos los valores
$agenda->setId($_POST["Id"]);
$agenda->setIdProfesional(($_POST["IdProfesional"]));
$agenda->setProtocolo($_POST["Protocolo"]);
$agenda->setFecha($_POST["Fecha"]);
$agenda->setHora($_POST["Hora"]);
$agenda->setConcurrio($_POST["Concurrio"]);

// grabamos el registro
$id = $agenda->grabaCita();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>