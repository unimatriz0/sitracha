/*
 * Nombre: agenda.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 02/07/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de la agenda de citas
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el método ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de la agenda de citas
 */
class Agenda {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // declaramos el layer
        this.layerAgenda = "";
        this.layerBusqueda = "";

        // inicializamos las variables
        this.initAgenda();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initAgenda(){

        // inicializamos las variables
        this.Id = 0;                 // clave del registro
        this.Profesional = "";       // nombre del profesional
        this.IdProfesional = 0;      // clave del profesional
        this.Protocolo = 0;          // protocolo del paciente
        this.Paciente = "";          // nombre del paciente
        this.Documento = "";         // documento del paciente
        this.Fecha = "";             // fecha de la cita
        this.Hora = "";              // hota de la cita
        this.Concurrio = 0;          // 0 falso 1 verdadero
        this.Usuario = "";           // nombre del usuario
        this.FechaAlta = "";         // fecha de alta del registro

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setIdProfesional(idprofesiona){
        this.IdProfesional = idprofesional;
    }
    setProfesional(profesional){
        this.Profesional = profesional;
    }
    setProtocolo(protocolo){
        this.Protocolo = protocolo;
    }
    setPaciente(nombre){
        this.Paciente = nombre;
    }
    setDocumento(documento){
        this.Documento = documento;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setHora(hora){
        this.Hora = hora;
    }
    setConcurrio(concurrio){
        this.Concurrio = concurrio;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente y presenta el
     * formulario con los datos
     */
    verAgenda(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // abrimos y mostramos el layer
        this.layerAgenda = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Citas',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:1200,
                    draggable: 'title',
                    ajax: {
                        url: 'agenda/form_agenda.html',
                        reload: 'strict'
                    }
                });
        this.layerAgenda.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que consulta la base de datos y presenta la
     * agenda de citas contando a partir de la fecha actual
     */
    getDatosAgenda(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "agenda/getagenda.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                agenda.cargaAgenda(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector json con las citas
     * Método que recibe como parámetro un array json con
     * la nómina de citas y las agrega a la grilla
     */
    cargaAgenda(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var enlace = "";
        var borrar = "";
        var acudio = "";

        // limpiamos la tabla y luego recargamos
        var tabla = $('#grilla_agenda').DataTable();
        tabla.clear();

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // armamos el enlace de edición
            enlace =  "<input type='button' ";
            enlace += "       name='btnEditar' ";
            enlace += "       class='botoneditar' ";
            enlace += "       title='Pulse para editar la cita' ";
            enlace += "       onClick='agenda.editaCita(" + fila[i].Id + ")';>";

            // armamos el enlace de eliminación
            borrar =  "<input type='button' ";
            borrar += "       name='btnBorrar' ";
            borrar += "       class='botonborrar' ";
            borrar += "       title='Pulse para eliminar la cita' ";
            borrar += "       onClick='agenda.borraCita(" + fila[i].Id + ")';>";

            // establecemos si vino
            if (data[i].Concurrio == 0){
                acudio = "No";
            } else {
                acudio = "Si";
            }

            // agregamos el registro
            tabla.row.add( [
                fila[i].Profesional,
                fila[i].Protocolo,
                fila[i].Paciente,
                fila[i].Documento,
                fila[i].Fecha,
                fila[i].Hora,
                acudio,
                fila[1].Usuario,
                enlace,
                borrar
            ] ).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} clave - clave del registro
     * Método que recibe como parámetro la clave de un
     * registro, lo carga y lo presenta en el formulario
     * de datos
     */
    editaCita(clave){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "agenda/getcita.php?id=" + clave,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                agenda.cargaCita(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector json con el registro
     * Método que recibe como parámetro el array json con
     * los datos del registro y lo asigna a las variables
     * de clase
     */
    cargaCita(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en las variables de clase
        this.setId(datos.Id);
        this.setIdProfesional(datos.IdProfesional);
        this.setProtocolo(datos.Protocolo);
        this.setPaciente(datos.Paciente);
        this.setDocumento(datos.Documento);
        this.setFecha(datos.Fecha);
        this.setHora(datos.Hora);
        this.setConcurrio(datos.Concurrio);
        this.setUsuario(datos.Usuario);
        this.setFechaAlta(datos.FechaAlta);

        // cargamos el formulario
        this.verCita();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase
     * presenta los datos en el formulario
     */
    verCita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos los valores en el formulario
        document.getElementById("profesional_agenda").value = this.Profesional;
        document.getElementById("protocolo_agenda").value = this.Protocolo;
        document.getElementById("nombre_agenda").value = this.Nombre;
        document.getElementById("documento_agenda").value = this.Documento;
        document.getElementById("fecha_agenda").value = this.Fecha;
        document.getElementById("hora_agenda").value = this.Hora;

        // según el estado
        if (this.Concurrio == 0){
            document.getElementById("concurrio_agenda").checked = false;
        } else {
            document.getElementById("concurrio_agenda").checked = true;
        }

        // termina de asignar
        document.getElementById("usuario_agenda").value = this.Usuario;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que
     * verifica el formulario de datos antes de enviarlo
     * al servidor
     */
    validaCita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no seleccionó profesional
        if (document.getElementById("profesional").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione el profesional de la lista";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("profesional").focus();
            return false;

        // si seleccionó
        } else {

            // asignamos en la clase
            this.IdProfesional = document.getElementById("profesional").value;

        }

        // si no ingresó correctamente el nombre
        if (document.getElementById("protocolo_agenda").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el nombre del paciente<br>";
            mensaje += "y luego pulse <b>Buscar</b>";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("nombre_agenda").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Protocolo = document.getElementById("protocolo_agenda").value;

        }

        // si no ingresó la fecha
        if (document.getElementById("fecha_agenda").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione la fecha de la cita";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_agenda").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Fecha = document.getElementById("fecha_agenda").value;

        }

        // si no ingresó la hora
        if (document.getElementById("hora_agenda").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la hora de la cita";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("hora_agenda").focus();
            return false;

        // si ingresó
        } else {

            // asiognamos en la clase
            this.Hora = document.getElementById("hora_agenda").value;

        }

        // si concurrió o no
        if (document.getElementById("concurrio_agenda").checked){
            this.Concurrio = 1;
        } else {
            this.Concurrio = 0;
        }

        // grabamos el registro
        this.grabaCita();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario que
     * ejecuta la consulta en el servidor
     */
    grabaCita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaramos las variables
        var datosAgenda = new FormData();

        // asignamos en el formulario
        datosAgenda.append("Id", this.Id);
        datosAgenda.append("IdProfesional", this.IdProfesional);
        datosAgenda.append("Protocolo", this.Protocolo);
        datosAgenda.append("Fecha", this.Fecha);
        datosAgenda.append("Hora", this.Hora);
        datosAgenda.append("Concurrio", this.Concurrio);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "agenda/grabaagenda.php",
            type: "POST",
            data: datosAgenda,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // limpia el formulario
                    agenda.limpaFormulario();

                    // recarga la grilla
                    agenda.getDatosAgenda();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro a eliminar
     * Método que luego de pedir confirmación elimina el
     * registro cuya clave recibe como parámetro
     */
    borraCita(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Cita',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('agenda/borrarcita.php?id='+id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // limpiamos el formulario
                            agenda.limpaFormulario();

                            // recargamos la grilla
                            agenda.getDatosAgenda();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de borrar o grabar un registro
     * que limpia el formulario de datos
     */
    limpaFormulario(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("profesional").value = "";
        document.getElementById("protocolo_agenda").value = "";
        document.getElementById("nombre_agenda").value = "";
        document.getElementById("documento_agenda").value = "";
        document.getElementById("fecha_agenda").value = "";
        document.getElementById("hora_agenda").value = "";
        document.getElementById("concurrio_agenda").checked = false;
        document.getElementById("usuario_agenda").value = sessionStorage.getItem("Usuario");

        // inicializamos las variables de clase
        this.initAgenda();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón buscar paciente,
     * verifica que se halla ingresado parte del nombre
     * el protocolo o el documento y abre el cuadro de
     * diálogo con los resultados
     */
    buscaPaciente(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaraciòn de variables
        var protocolo = document.getElementById("protocolo_agenda").value;
        var nombre = document.getElementById("nombre_agenda").value;
        var documento = document.getElementById("documento_agenda").value;
        var mensaje;

        // si no ingresó ninguno
        if (protocolo == "" && nombre == "" && documento == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar parte del nombre, el documento<br>";
            mensaje += "o el protocolo del paciente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // abrimos y mostramos el layer
        this.layerBusqueda = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Citas',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:700,
                    draggable: 'title',
                    ajax: {
                    url: 'agenda/buscar.php?documento=' + documento + "&nombre=" + nombre + "&protocolo=" + protocolo,
                    reload: 'strict'
                }
            });
        this.layerBusqueda.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} protocolo - protocolo del paciente
     * @param {string} nombre - nombre del paciente
     * @param {int} documento - documento del paciente
     * Método llamado al seleccionar un registro del layer
     * de búsqueda de pacientes, asigna en el formulario
     * los valores recibidos y destruye el layer emergente
     */
    seleccionaPaciente(protocolo, nombre, documento){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en el formulario
        document.getElementById("protocolo_agenda").value = protocolo;
        document.getElementById("nombre_agenda").value = nombre;
        document.getElementById("documento_agenda").value = documento;

        // destruimos el layer
        this.layerBusqueda.destroy();

        // fijamos el foco
        document.getElementById("fecha_agenda").focus();

    }

}