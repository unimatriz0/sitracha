<?php

/**
 *
 * getagenda | agenda/getagenda.php
 *
 * @package     Diagnostico
 * @subpackage  Agenda
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que retorna un array json con la agenda de citas
*/

// incluimos e instanciamos la clase
require_once("agenda.class.php");
$agenda = new Agenda();

// obtenemos la nómina
$nomina = $agenda->nominaAgenda();

// retornamos el vector
echo json_encode($nomina);

?>