<?php

/**
 *
 * borraagenda | agenda/borracita.php
 *
 * @package     Diagnostico
 * @subpackage  Agenda
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (02/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por get la clave de un registro y ejecuta
 * la consulta de eliminación
 *
*/

// incluimos e instanciamos la clase
require_once("agenda.class.php");
$agenda = new Agenda();

// eliminamos el registro
$agenda->borraCita($_POST["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>