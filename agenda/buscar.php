<?php

/**
 *
 * buscar | agenda/buscar.php
 *
 * @package     Diagnostico
 * @subpackage  Agenda
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (03/07/2019)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por get el número de protocolo, el nombre
 * y el número de documento y presenta la grilla de registros
 * coincidentes para la selección del usuario
*/

// incluimos las librerías
require_once("../pacientes/pacientes.class.php");
$paciente = new Pacientes();

// según las variables que recibidos por get
if (!empty($_GET["documento"])){
    $texto = $_GET["documento"];
} elseif (!empty($_GET["protocolo"])){
    $texto = $_GET["protocolo"];
} else {
    $texto = $_GET["nombre"];
}

// buscamos el registro
$nomina = $paciente->buscaPaciente($texto);

// si encontró registros
if (count($nomina) != 0){

    // define la tabla
    echo "<table id='grilla_pac_agenda' width='95%' align='center' border='0'>";

    // definimos los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Protocolo</th>";
    echo "<th>Nombre</th>";
    echo "<th>Documento</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // presentamos el registro
        echo "<td>$protocolo</td>";
        echo "<td>$nombre</td>";
        echo "<td>$documento</td>";

        // arma el enlace
        echo "<td>";
        echo "<input type='button'
                     name='btnSelPaciente'
                     title='Pulse para seleccionar el paciente'
                     class='botoneditar'
                     onClick='agenda.seleccionaPaciente(" . chr(34) . $protocolo . chr(34) . "," .chr(34) . $nombre . chr(34) . "," . chr(34) . $documento . chr(34) . ")'>";
        echo "</td>";

        // cerramos la fila
        echo "</tr>";

    }

    // cerramos la tabla
    echo "</tbody>";
    echo "</table>";

    // define el div para el paginador
    echo "<div class='paging'></div>";

    ?>
    <script>

        // definimos las propiedades de la tabla
        $('#grilla_pac_agenda').datatable({
            pageSize: 15,
            sort:    [true, true, true, false],
            filters: [true, true, true, false],
            filterText: 'Buscar ... '
        });

    </script>
    <?php

// si no encontró
} else {

    // presenta el mensaje
    echo "<h2>No se han encontrado registros coincidentes</h2>";

}

?>