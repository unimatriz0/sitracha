/*

    Nombre: diagnostico.sql
    Fecha: 20/03/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: definicion de la estructura de base de datos del sistema
                 de diagnostico

    Se crean tablas de auditoria y los triggers porque el control de
    auditoría se hace demasiado complejo por código e introduce
    errores

    Para la distribución es necesario analizar
    1. Distribuir toda la plataforma en una sola base de datos
    2. Utilizar una base de datos para Calidad, otra para Diagnóstico
       y una tercera para las tablas auxiliares (localidades, documentos, etc.)

    Crear las bases para control de stock

    El acceso de los usuarios es independiente entre el sistema de diagnóstico
    y el de control de calidad (ya que un laboratorio puede participar del
    CCE y tener un sistema propio de diagnóstico)

    Así vamos a tener:

    En la base de datos CCE
    1. La tabla de Laboratorios
    2. La tabla de Usuarios Autorizados de CCE
    3. La tabla de determinaciones
    4. La tabla de otras determinaciones
    5. La tabla de etiquetas
    6. La tabla de notas recibidas
    7. La tabla de notas enviadas a los responsables
    8. La tabla de operativos chagas
    9. La tabla de palets (pivot con la de muestras / etiquetas)
   10. La tabla de técnicas
   11. La tabla de valores de las técnicas
   12. La tabla de no conformidades
   13. Las tablas de auditoría correspondientes

    En la base de datos de diagnóstico
    1. La tabla de pacientes / personas
    2. La tabla de chagas congénito
    3. La tabla de resultados de las pruebas
    4. La tabla de toma de muestras
    5. La tabla de entrevistas
    6. La tabla de motivos de consulta
    7. La tabla de transfusiones recibidas
    8. La tabla de transplantes recibidos
    9. El diccionario de órganos de transplante
   10. La tabla de usuarios y sus permisos
   11. La tabla de técnicas (la independizamos de CCE)
   12. La tabla de valores de corte (independiente de CCE)
   13. La tabla de centros asistenciales
   14. La tabla de protocolos o certificados
   15. La tabla de marcas de reactivos (porque usamos una tabla de tecnicas distinta)
   16. Tabla con las fechas de entrega de resultados según fecha de toma

   Usa como diccionario la tabla de laboratorios del CCE

   Se agrega la tabla de departamentos de las instituciones (laboratorios),
   originalmente el sistema estaba planificado para servicios de laboratorio
   únicamente, pero luego al agregarse las tablas de clínica, temperaturas
   y de stock se evaluó la conveniencia de independizar los registros dentro
   de una misma institución

   En la base de datos de stock tenemos
   1. Los items del stock
   2. La tabla de ingresos
   3. La tabla de egresos
   4. La tabla de inventario
   5. La tabla de fuentes de financiamiento
   6. La tabla de solicitudes de stock

   Usa la tabla de usuarios autorizados de diagnóstico

   En la base de Diccionarios tendremos (comunes a las dos plataformas)
   1. La tabla de países
   2. La tabla de jurisdicciones
   3. La tabla de localidades
   4. El diccionario de tipos de documento
   5. El diccionario de dependencias

   Las tablas de clínica son

    Derivacion diccionario de derivaciones de consulta
    ChagasAgudo antecedentes de chagas agudo
    Drogas diccionario de drogas utilizadas en el tratamiento
    Adversos diccionario de efectos adversos
    Tratamiento también, debe incluir droga
    Antecedentes antecedentes de la anamnesis
    Familiares antecedentes familiares
    Sintomas sintomas del paciente
    disautonomia otros síntomas del paciente
    Digestivo tipo de compromiso digestivo
    Examen examen físico del paciente
    Cardiovascular examen del aparato cardiovascular
    Rx evaluación de las radiografías
    Electro resultados del electrocardiograma
    Eco resultados del ecocardiograma
    Holter resultados del holter
    Ergometría resultados de la ergometría
    Clasificacion clasificación de la enfermedad según kuscknir
    Citas datos para la agenda de citas
    Horarios tabla pivot de horarios de los profesionales
    Proyectos tabla con los proyectos de investigación
    PacProyectos tabla pivot con los pacientes que participan
                 en proyectos

*/

-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- eliminamos la base si existe
DROP DATABASE IF EXISTS diagnostico;

-- la creamos
CREATE DATABASE IF NOT EXISTS diagnostico
DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;

-- seleccionamos
USE diagnostico;

/*************************************************************************/
/*                                                                       */
/*                        Tabla de Personas                              */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla de personas
 protocolo, entero 8 digitos autonumerico, numero de protocolo
 id_laboratorio, entero 6 digitos, laboratorio propietario del registro
 historia_clinica, varchar(20) numero identificador de la historia clinica
 apellido, varchar(50) apellido del paciente
 nombre, varchar(50) nombre del paciente
 documento, varchar(12) numero de documento del paciente (para poder adaptarlo
            a otros paises donde quiza usen letras)
 tipo_documento, entero pequeño (1) clave con el tipo de documento
 fecha_nacimiento, date fecha de nacimiento del paciente
 edad entero(2) edad del paciente puede ser calculado o ingresado manualmente
 sexo entero pequeño (1) clave con el sexo del paciente
 estado_civil entero pequeño (1) clave con el estado civil del paciente
 hijos entero pequeño(2) numero de hijos del paciente
 direccion varchar(100) direccion postal del paciente
 telefono varchar(20) numero de telefono
 celular varchar(30) número de celular
 compania tinyint(1) clave con la compañía de celular para enviar textos
 localidad_nacimiento cadena(9) clave con la localidad de nacimiento
 coordenadas_nacimiento varchar(50) coordenadas gps
 localidad_residencia cadena(9) clave con la localidad de residencia
 coordenadas_residencia varchar(50) coordenadas gps
 localidad_madre cadena(9) clave con la localidad de origen de la madre
 madre_positiva enum (si no no sabe) indica si la madre era positiva
 ocupacion varchar(250) descripcion de la ocupacion
 obra_social varchar(100) nombre de la obra social
 motivo entero pequeño(1) clave con el motivo de consulta
 derivacion int(1) clave del tipo de derivación
 tratamiento enum (si no no sabe) recibio tratamiento
 otras_enfermedades indica si tiene otras enfermedades
 usuario entero(4) clave de la tabla de usuarios autorizados
 fecha_alta date fecha en que ingreso el registro
 comentarios: texto, comentarios y observaciones

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS personas;

-- creamos la tabla
CREATE TABLE personas (
    protocolo int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    historia_clinica varchar(20) DEFAULT NULL,
    apellido varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    documento varchar(12) NOT NULL,
    tipo_documento tinyint(1) UNSIGNED NOT NULL,
    fecha_nacimiento date DEFAULT NULL,
    edad int(2) UNSIGNED DEFAULT NULL,
    sexo tinyint(1) UNSIGNED NOT NULL,
    estado_civil tinyint(1) UNSIGNED NOT NULL,
    hijos tinyint(2) UNSIGNED DEFAULT NULL,
    direccion varchar(100) DEFAULT NULL,
    telefono varchar(20) DEFAULT NULL,
    celular varchar(30) DEFAULT NULL,
    compania int(1) DEFAULT NULL,
    mail varchar(50) DEFAULT NULL,
    localidad_nacimiento varchar(9) NOT NULL,
    coordenadas_nacimiento varchar(50) DEFAULT NULL,
    localidad_residencia varchar(9) NOT NULL,
    coordenadas_residencia varchar(50) DEFAULT NULL,
    localidad_madre varchar(9) DEFAULT NULL,
    madre_positiva enum("Si", "No", "No Sabe") DEFAULT "No Sabe",
    ocupacion varchar(250) DEFAULT NULL,
    obra_social varchar(100) DEFAULT NULL,
    motivo tinyint(1) UNSIGNED NOT NULL,
    derivacion tinyint(1) UNSIGNED NOT NULL,
    tratamiento enum ("Si", "No", "No Sabe") NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY historia_clinica(historia_clinica),
    KEY apellido(apellido),
    KEY nombre(nombre),
    KEY nombre_completo(apellido, nombre),
    KEY documento(documento),
    KEY derivacion(derivacion),
    KEY tipo_documento(tipo_documento),
    KEY compania(compania),
    KEY sexo(sexo),
    KEY estado_civil(estado_civil),
    KEY localidad_nacimiento(localidad_nacimiento),
    KEY localidad_residencia(localidad_residencia),
    KEY localidad_madre(localidad_madre),
    KEY motivo(motivo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los pacientes';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_personas;

-- creamos la tabla
CREATE TABLE auditoria_personas (
    protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    historia_clinica varchar(20) DEFAULT NULL,
    apellido varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    documento varchar(12) NOT NULL,
    tipo_documento tinyint(1) UNSIGNED NOT NULL,
    fecha_nacimiento date DEFAULT NULL,
    edad int(2) UNSIGNED DEFAULT NULL,
    sexo tinyint(1) UNSIGNED NOT NULL,
    estado_civil tinyint(1) UNSIGNED NOT NULL,
    hijos tinyint(2) UNSIGNED DEFAULT NULL,
    direccion varchar(100) DEFAULT NULL,
    telefono varchar(20) DEFAULT NULL,
    celular varchar(30) DEFAULT NULL,
    compania int(1) DEFAULT NULL,
    mail varchar(50) DEFAULT NULL,
    localidad_nacimiento varchar(9) NOT NULL,
    coordenadas_nacimiento varchar(50) DEFAULT NULL,
    localidad_residencia varchar(9) NOT NULL,
    coordenadas_residencia varchar(50) DEFAULT NULL,
    localidad_madre varchar(9) DEFAULT NULL,
    madre_positiva enum("Si", "No", "No Sabe") DEFAULT "No Sabe",
    ocupacion varchar(250) DEFAULT NULL,
    obra_social varchar(100) DEFAULT NULL,
    motivo tinyint(1) UNSIGNED NOT NULL,
    derivacion tinyint(1) UNSIGNED NOT NULL,
    tratamiento enum ("Si", "No", "No Sabe") NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY protocolo(protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY historia_clinica(historia_clinica),
    KEY apellido(apellido),
    KEY nombre(nombre),
    KEY nombre_completo(apellido, nombre),
    KEY documento(documento),
    KEY compania(compania),
    KEY derivacion(derivacion),
    KEY tipo_documento(tipo_documento),
    KEY sexo(sexo),
    KEY estado_civil(estado_civil),
    KEY localidad_nacimiento(localidad_nacimiento),
    KEY localidad_residencia(localidad_residencia),
    KEY localidad_madre(localidad_madre),
    KEY motivo(motivo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de los pacientes';

-- eliminamos el trigger de edición de personas
DROP TRIGGER IF EXISTS edicion_personas;

-- creamos el trigger
CREATE TRIGGER edicion_personas
AFTER UPDATE ON personas
FOR EACH ROW
INSERT INTO auditoria_personas
     (protocolo,
      id_laboratorio,
      historia_clinica,
      apellido,
      nombre,
      documento,
      tipo_documento,
      fecha_nacimiento,
      edad,
      sexo,
      estado_civil,
      hijos,
      direccion,
      telefono,
      celular,
      compania,
      mail,
      localidad_nacimiento,
      coordenadas_nacimiento,
      localidad_residencia,
      coordenadas_residencia,
      localidad_madre,
      madre_positiva,
      ocupacion,
      obra_social,
      motivo,
      derivacion,
      tratamiento,
      usuario,
      fecha_alta,
      comentarios,
      evento)
     VALUES
     (OLD.protocolo,
      OLD.id_laboratorio,
      OLD.historia_clinica,
      OLD.apellido,
      OLD.nombre,
      OLD.documento,
      OLD.tipo_documento,
      OLD.fecha_nacimiento,
      OLD.edad,
      OLD.sexo,
      OLD.estado_civil,
      OLD.hijos,
      OLD.direccion,
      OLD.telefono,
      OLD.celular,
      OLD.compania,
      OLD.mail,
      OLD.localidad_nacimiento,
      OLD.coordenadas_nacimiento,
      OLD.localidad_residencia,
      OLD.coordenadas_residencia,
      OLD.localidad_madre,
      OLD.madre_positiva,
      OLD.ocupacion,
      OLD.obra_social,
      OLD.motivo,
      OLD.derivacion,
      OLD.tratamiento,
      OLD.usuario,
      OLD.fecha_alta,
      OLD.comentarios,
      "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_personas;

-- creamos el trigger
CREATE TRIGGER eliminacion_personas
AFTER DELETE ON personas
FOR EACH ROW
INSERT INTO auditoria_personas
     (protocolo,
      id_laboratorio,
      historia_clinica,
      apellido,
      nombre,
      documento,
      tipo_documento,
      fecha_nacimiento,
      edad,
      sexo,
      estado_civil,
      hijos,
      direccion,
      telefono,
      celular,
      compania,
      mail,
      localidad_nacimiento,
      coordenadas_nacimiento,
      localidad_residencia,
      coordenadas_residencia,
      localidad_madre,
      madre_positiva,
      ocupacion,
      obra_social,
      motivo,
      derivacion,
      tratamiento,
      usuario,
      fecha_alta,
      comentarios,
      evento)
     VALUES
     (OLD.protocolo,
      OLD.id_laboratorio,
      OLD.historia_clinica,
      OLD.apellido,
      OLD.nombre,
      OLD.documento,
      OLD.tipo_documento,
      OLD.fecha_nacimiento,
      OLD.edad,
      OLD.sexo,
      OLD.estado_civil,
      OLD.hijos,
      OLD.direccion,
      OLD.telefono,
      OLD.celular,
      OLD.compania,
      OLD.mail,
      OLD.localidad_nacimiento,
      OLD.coordenadas_nacimiento,
      OLD.localidad_residencia,
      OLD.coordenadas_residencia,
      OLD.localidad_madre,
      OLD.madre_positiva,
      OLD.ocupacion,
      OLD.obra_social,
      OLD.motivo,
      OLD.derivacion,
      OLD.tratamiento,
      OLD.usuario,
      OLD.fecha_alta,
      OLD.comentarios,
      "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*              Fechas de entrega de resultados                          */
/*                                                                       */
/*************************************************************************/

/*

    Estructura de la tabla
    id int(4) clave del registro (a una entrada por día tenemos tabla
              para 25 años)
    recepcion date fecha de recepción de la muestra
    entrega date fecha de entrega de resultados
    laboratorio int(6) clave del laboratorio (cada laboratorio puede
                tener una fecha distinta de entrega)
    usuario int(4) usuario que ingresó el registro

    En esta tabla no usamos registro de transacciones

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS entregas;

-- la recreamos
CREATE TABLE IF NOT EXISTS entregas (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    recepcion date NOT NULL,
    entrega date NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY laboratorio(laboratorio),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Fechas de entregas de resultados';


/*************************************************************************/
/*                                                                       */
/*                Diccionario de Enfermedades                            */
/*                                                                       */
/*************************************************************************/

/*

  Estructura de la tabla con el diccionario de enfermedades
  id entero(3) autonumérico, clave del registro
  enfermedad varchar(100) descripción de la enfermedad
  id_usuario entero(4) clave del usuario
  fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS enfermedades;

-- la recreamos
CREATE TABLE enfermedades (
    id int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    enfermedad varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY enfermedad(enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de enfermedades';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_enfermedades;

-- la recreamos
CREATE TABLE auditoria_enfermedades (
    id int(3) UNSIGNED NOT NULL,
    enfermedad varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta DATE NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY enfermedad(enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de enfermedades';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_enfermedades;

-- lo recreamos
CREATE TRIGGER edicion_enfermedades
AFTER UPDATE ON enfermedades
FOR EACH ROW
INSERT INTO auditoria_enfermedades
    (id,
     enfermedad,
     id_usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.enfermedad,
     OLD.id_usuario,
     OLD.fecha_alta,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_enfermedades;

-- lo recreamos
CREATE TRIGGER eliminacion_enfermedades
AFTER DELETE ON enfermedades
FOR EACH ROW
INSERT INTO auditoria_enfermedades
    (id,
     enfermedad,
     id_usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.enfermedad,
     OLD.id_usuario,
     OLD.fecha_alta,
     "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Otras Enfermedades                            */
/*                                                                       */
/*************************************************************************/

/*
   Estructura de la tabla de otras enfermedades, esta tabla es un pivot
   entre el diccionario de enfermedades y el de pacientes, agrega un
   registro por cada enfermedad sufrida por el paciente
   id int(9) clave del registro
   id_protocoloi int(8) clave del paciente
   id_enfermedad int(3) clave de la enfermedad
   id_usuario int(4) clave del usuario
   fecha date fecha de la enfermedad
   fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS otras_enfermedades;

-- la recreamos
CREATE TABLE otras_enfermedades(
    id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_enfermedad int(3) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_enfermedad(id_enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Otras Enfermedades de los pacientes';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_otras_enfermedades;

-- la recreamos
CREATE TABLE auditoria_otras_enfermedades(
    id int(9) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_enfermedad int(3) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_enfermedad(id_enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de otras enfermedades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_otras_enfermedades;

-- lo recreamos
CREATE TRIGGER edicion_otras_enfermedades
AFTER UPDATE ON otras_enfermedades
FOR EACH ROW
INSERT INTO auditoria_otras_enfermedades
       (id,
        id_protocolo,
        id_enfermedad,
        id_usuario,
        fecha,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_protocolo,
        OLD.id_enfermedad,
        OLD.id_usuario,
        OLD.fecha,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_otras_enfermedades;

-- lo recreamos
CREATE TRIGGER eliminacion_otras_enfermedades
AFTER DELETE ON otras_enfermedades
FOR EACH ROW
INSERT INTO auditoria_otras_enfermedades
       (id,
        id_protocolo,
        id_enfermedad,
        id_usuario,
        fecha,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_protocolo,
        OLD.id_enfermedad,
        OLD.id_usuario,
        OLD.fecha,
        OLD.fecha_alta,
        "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Departamentos                                 */
/*                                                                       */
/*************************************************************************/

/*

    Tabla con la información de los departmentos dentro de cada institución
    id int(4) clave del registro
    institucion int(4) clave de la institución
    departamento varchar(50) nombre del departamento
    usuario int(4) clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS departamentos;

-- la recreamos
CREATE TABLE departamentos (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    institucion int(4) UNSIGNED NOT NULL,
    departamento varchar(50) NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY institucion(institucion),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Departamentos de las instituciones';

-- insertamos los primeros registros
INSERT INTO departamentos(institucion, departamento, usuario) VALUES (397, "Diagnostico", 1);
INSERT INTO departamentos(institucion, departamento, usuario) VALUES (397, "Clinica Medica", 1);
INSERT INTO departamentos(institucion, departamento, usuario) VALUES (397, "Produccion", 1);

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_departamentos;

-- la recreamos
CREATE TABLE auditoria_departamentos(
    id int(4) UNSIGNED NOT NULL,
    institucion int(4) UNSIGNED NOT NULL,
    departamento varchar(50) NOT NULL,
    fecha_alta date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    PRIMARY KEY(id),
    KEY institucion(institucion),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los departamentos';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_departamentos;

-- lo recreamos
CREATE TRIGGER edicion_departamentos
AFTER UPDATE ON departamentos
FOR EACH ROW
INSERT INTO auditoria_departamentos
    (id,
     institucion,
     departamento,
     fecha_alta,
     usuario,
     evento)
    VALUES
    (OLD.id,
     OLD.institucion,
     OLD.departamento,
     OLD.fecha_alta,
     OLD.usuario,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminación_departamentos;

-- lo recreamos
CREATE TRIGGER eliminacion_departamentos
AFTER DELETE ON departamentos
FOR EACH ROW
INSERT INTO auditoria_departamentos
    (id,
     institucion,
     departamento,
     fecha_alta,
     usuario,
     evento)
    VALUES
    (OLD.id,
     OLD.institucion,
     OLD.departamento,
     OLD.fecha_alta,
     OLD.usuario,
     "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Chagas Congenito                              */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla congenito, esta tabla al ser menor, agrega como
 registro auxiliar los datos de chagas congenito
 id entero(8) autonumerico clave unica del registro
 id_protocolo entero(8) clave del paciente
 id_madre entero(8) clave del protocolo de la madre
 id_sivila varchar(20) clave del sivila
 reportado date fecha en que fue informado al sivila
 parto enum (normal / cesarea / otro) tipo de parto
 peso entero(4) peso al nacer en gramos
 prematuro enum (si / no) indica si el bebe fue prematuro
 institucion entero(8) clave de la institucion
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha_alta date fecha de alta del registro
 comentarios texto, comentarios especificos

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS congenito;

-- creamos la tabla
CREATE TABLE congenito (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_madre int(8) UNSIGNED NOT NULL,
    id_sivila varchar(20) DEFAULT NULL,
    reportado date DEFAULT NULL,
    parto enum("Normal", "Cesárea", "No Sabe") NOT NULL,
    peso int(4) UNSIGNED NOT NULL,
    prematuro enum("Si", "No", "No Sabe"),
    institucion int(8) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_madre(id_madre),
    KEY id_sivila(id_sivila),
    KEY institucion(institucion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de chagas congenito';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_congenito;

-- la recreamos
CREATE TABLE auditoria_congenito (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_madre int(8) UNSIGNED NOT NULL,
    id_sivila varchar(20) DEFAULT NULL,
    reportado date DEFAULT NULL,
    parto enum("Normal", "Cesárea", "No Sabe") NOT NULL,
    peso int(4) UNSIGNED NOT NULL,
    prematuro enum("Si", "No", "No Sabe"),
    institucion int(8) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_madre(id_madre),
    KEY id_sivila(id_sivila),
    KEY institucion(institucion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de chagas congenito';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_congenito;

-- lo recreamos
CREATE TRIGGER edicion_congenito
AFTER UPDATE ON congenito
FOR EACH ROW
INSERT INTO auditoria_congenito
    (id,
     id_protocolo,
     id_madre,
     id_sivila,
     reportado,
     parto,
     peso,
     prematuro,
     institucion,
     id_usuario,
     fecha_alta,
     comentarios,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.id_madre,
     OLD.id_sivila,
     OLD.reportado,
     OLD.parto,
     OLD.peso,
     OLD.prematuro,
     OLD.institucion,
     OLD.id_usuario,
     OLD.fecha_alta,
     OLD.comentarios,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_congenito;

-- lo recreamos
CREATE TRIGGER eliminacion_congenito
AFTER DELETE ON congenito
FOR EACH ROW
INSERT INTO auditoria_congenito
    (id,
     id_protocolo,
     id_madre,
     id_sivila,
     reportado,
     parto,
     peso,
     prematuro,
     institucion,
     id_usuario,
     fecha_alta,
     comentarios,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.id_madre,
     OLD.id_sivila,
     OLD.reportado,
     OLD.parto,
     OLD.peso,
     OLD.prematuro,
     OLD.institucion,
     OLD.id_usuario,
     OLD.fecha_alta,
     OLD.comentarios,
     "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                      Resultados de las Pruebas                           */
/*                                                                          */
/****************************************************************************/

/*

 Estructura de la tabla de resultados
 id_resultado entero(10) autonumerico, clave del registro
 id_muestra entero(8) clave de la muestra
 resultado enum (Reactivo / No Reactivo / Indeterminado) resultado obtenido
 valor_corte varchar(10) valor de corte utilizado
 valor_lectura varchar(10) valor de lectura obtenido
 id_tecnica entero pequeño (3) clave de la tecnica utilizada
 fecha_determinacion fecha en que se realizo la determinacion
 comentarios texto comentarios y observaciones
 id_usuario entero(4) usuario que realizo la determinacion
 id_verifico entero(4) usuario que verificó el resultado
 id_firmo entero(4) usuario que firmó o autorizó el protocolo
 fecha date fecha de alta del registro

*/

-- elimina la tabla
DROP TABLE IF EXISTS resultados;

-- creamos la tabla
CREATE TABLE resultados (
    id_resultado int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_muestra int(8) UNSIGNED NOT NULL,
    resultado enum("Reactivo", "No Reactivo", "Indeterminado") NOT NULL,
    valor_corte varchar(10) NOT NULL,
    valor_lectura varchar(10) NOT NULL,
    id_tecnica tinyint(3) UNSIGNED NOT NULL,
    fecha_determinacion date NOT NULL,
    comentarios text DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    id_verifico int(4) UNSIGNED DEFAULT NULL,
    id_firmo int(4) UNSIGNED DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_resultado),
    KEY id_muestra(id_muestra),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario),
    KEY id_verifico(id_verifico),
    KEY id_firmo(id_firmo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Resultados de las pruebas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_resultados;

-- creamos la tabla
CREATE TABLE auditoria_resultados (
    id_resultado int(8) UNSIGNED NOT NULL,
    id_muestra int(8) UNSIGNED NOT NULL,
    resultado enum("Reactivo", "No Reactivo", "Indeterminado") NOT NULL,
    valor_corte varchar(10) NOT NULL,
    valor_lectura varchar(10) NOT NULL,
    id_tecnica tinyint(3) UNSIGNED NOT NULL,
    fecha_determinacion date NOT NULL,
    comentarios text DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    id_verifico int(4) UNSIGNED DEFAULT NULL,
    id_firmo int(4) UNSIGNED DEFAULT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id_resultado(id_resultado),
    KEY id_muestra(id_muestra),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario),
    KEY id_verifico(id_verifico),
    KEY id_firmo(id_firmo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los resultados';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_resultados;

-- lo recreamos
CREATE TRIGGER edicion_resultados
AFTER UPDATE ON resultados
FOR EACH ROW
INSERT INTO auditoria_resultados
    (id_resultado,
     id_muestra,
     resultado,
     valor_corte,
     valor_lectura,
     id_tecnica,
     fecha_determinacion,
     comentarios,
     id_usuario,
     id_verifico,
     id_firmo,
     fecha,
     evento)
    VALUES
    (OLD.id_resultado,
     OLD.id_muestra,
     OLD.resultado,
     OLD.valor_corte,
     OLD.valor_lectura,
     OLD.id_tecnica,
     OLD.fecha_determinacion,
     OLD.comentarios,
     OLD.id_usuario,
     OLD.id_verifico,
     OLD.id_firmo,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_resultados;

-- lo recreamos
CREATE TRIGGER eliminacion_resultados
AFTER DELETE ON resultados
FOR EACH ROW
INSERT INTO auditoria_resultados
    (id_resultado,
     id_muestra,
     resultado,
     valor_corte,
     valor_lectura,
     id_tecnica,
     fecha_determinacion,
     comentarios,
     id_usuario,
     id_verifico,
     id_firmo,
     fecha,
     evento)
    VALUES
    (OLD.id_resultado,
     OLD.id_muestra,
     OLD.resultado,
     OLD.valor_corte,
     OLD.valor_lectura,
     OLD.id_tecnica,
     OLD.fecha_determinacion,
     OLD.comentarios,
     OLD.id_usuario,
     OLD.id_verifico,
     OLD.id_firmo,
     OLD.fecha,
     "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                       Toma de Muestras                                */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla de muestras
 id_muestra entero(8) autonumerico
 id_protocolo entero(8) clave del paciente o protocolo
 id_laboratorio entero(6) laboratorio donde se tomó la muestra
 comentarios texto comentarios y observaciones
 id_genero entero(4) clave del usuario que generó el registro
 fecha_alta date fecha en que fue tomada la muestra
 id_tomo entero(4) clave del usuario que tomó la muestra
 fecha_toma date fecha en que fue tomada la muestra

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS muestras;

-- la recreamos desde cero
CREATE TABLE muestras (
    id_muestra int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    comentarios TEXT,
    id_genero int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_tomo int(4) UNSIGNED DEFAULT NULL,
    fecha_toma date DEFAULT NULL,
    PRIMARY KEY(id_muestra),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_genero(id_genero),
    KEY id_tomo(id_tomo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Muestras de los pacientes';

-- eliminamos la auditoria de las muestras
DROP TABLE IF EXISTS auditoria_muestras;

-- la recreamos desde cero
CREATE TABLE auditoria_muestras (
    id_muestra int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    comentarios TEXT,
    id_genero int(4) UNSIGNED NOT NULL,
    fecha_alta date,
    id_tomo int(4) DEFAULT NULL,
    fecha_toma date,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id_muestra(id_muestra),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_genero(id_genero),
    KEY id_tomo(id_tomo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las muestras';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_muestras;

-- lo recreamos
CREATE TRIGGER edicion_muestras
AFTER UPDATE ON muestras
FOR EACH ROW
INSERT INTO auditoria_muestras
    (id_muestra,
     id_protocolo,
     id_laboratorio,
     comentarios,
     id_genero,
     fecha_alta,
     id_tomo,
     fecha_toma,
     evento)
    VALUES
    (OLD.id_muestra,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.comentarios,
     OLD.id_genero,
     OLD.fecha_alta,
     OLD.id_tomo,
     OLD.fecha_toma,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_muestras;

-- lo recreamos
CREATE TRIGGER eliminacion_muestras
AFTER DELETE ON muestras
FOR EACH ROW
INSERT INTO auditoria_muestras
    (id_muestra,
     id_protocolo,
     id_laboratorio,
     comentarios,
     id_genero,
     fecha_alta,
     id_tomo,
     fecha_toma,
     evento)
    VALUES
    (OLD.id_muestra,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.comentarios,
     OLD.id_genero,
     OLD.fecha_alta,
     OLD.id_tomo,
     OLD.fecha_toma,
     "Eliminacion");


/************************************************************************/
/*                                                                      */
/*                  Entrevistas a Pacientes                             */
/*                                                                      */
/************************************************************************/

/*

 Estructura de la tabla de entrevistas
 id_entrevista entero(6) autonumerico, clave del registro
 id_protocolo entero(8) clave del paciente
 id_laboratorio int(6) laboratorio donde se realizó la entrevista
 resultado enum(positivo / negativo / otro) resultado de la entrevista
 actitud enum (positiva / negativa / otra) actitud del paciente
 concurrio enum (si / no) indica si concurrio a la entrevista
 tipo (presencial / telefonica) modalidad de la entrevista
 fecha date fecha de la entrevista
 comentarios text comentarios y observaciones del entrevistador
 id_usuario entero(4) clave del usuario que entrevisto

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS entrevistas;

-- la recreamos
CREATE TABLE entrevistas (
    id_entrevista int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    resultado enum("Positivo", "Negativo", "Otro") NOT NULL,
    actitud enum("Positiva", "Negativa", "Indiferente") NOT NULL,
    concurrio enum("Si", "No") NOT NULL,
    tipo enum("Presencial", "Telefonica") NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id_entrevista),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Entrevistas a pacientes';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_entrevistas;

-- la recreamos
CREATE TABLE auditoria_entrevistas (
    id_entrevista int(6) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    resultado enum("Positivo", "Negativo", "Otro") NOT NULL,
    actitud enum("Positiva", "Negativa", "Indiferente") NOT NULL,
    concurrio enum("Si", "No") NOT NULL,
    tipo enum("Presencial", "Telefonica") NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text,
    id_usuario int(4) UNSIGNED NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id_entrevista(id_entrevista),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de entrevistas a pacientes';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_entrevistas;

-- lo recreamos
CREATE TRIGGER edicion_entrevistas
AFTER UPDATE ON entrevistas
FOR EACH ROW
INSERT INTO auditoria_entrevistas
    (id_entrevista,
     id_protocolo,
     id_laboratorio,
     resultado,
     actitud,
     concurrio,
     tipo,
     comentarios,
     id_usuario,
     evento)
    VALUES
    (OLD.id_entrevista,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.resultado,
     OLD.actitud,
     OLD.concurrio,
     OLD.tipo,
     OLD.comentarios,
     OLD.id_usuario,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_entrevistas;

-- lo recreamos
CREATE TRIGGER eliminacion_entrevistas
AFTER DELETE ON entrevistas
FOR EACH ROW
INSERT INTO auditoria_entrevistas
    (id_entrevista,
     id_protocolo,
     id_laboratorio,
     resultado,
     actitud,
     concurrio,
     tipo,
     comentarios,
     id_usuario,
     evento)
    VALUES
    (OLD.id_entrevista,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.resultado,
     OLD.actitud,
     OLD.concurrio,
     OLD.tipo,
     OLD.comentarios,
     OLD.id_usuario,
     "Eliminacion");


/************************************************************************/
/*                                                                      */
/*                     Motivos de Consulta                              */
/*                                                                      */
/************************************************************************/

/*

 Estructura de la tabla de motivos de consulta
 id_motivo entero pequeño (2) autonumerico, clave del registro
 motivo varchar(50) descripcion del motivo de la consulta
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS motivos;

-- la recreamos
CREATE TABLE motivos (
    id_motivo tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    motivo varchar(50) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_motivo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Motivos de consulta';

-- insertamos los valores iniciales
INSERT INTO motivos (motivo, id_usuario) VALUES ("Atención Médica", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Banco de Sangre", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Embarazo", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("HIV", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Niño", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Pre ocupacional", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Transplante", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Laboral", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Migraciones", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Otro", 1);


/***************************************************************************/
/*                                                                         */
/*                       Transfusiones Recibidas                           */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de transfusiones
 id entero(8) autonumerico, clave del registro
 id_protocolo entero(8) clave del protocolo
 localidad varchar localidad de la transfusión
 fecha_transfusion date fecha de la transfusion
 motivo varchar(200) descripcion del motivo
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS transfusiones;

-- la recreamos
CREATE TABLE transfusiones (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    fecha_transfusion date NOT NULL,
    localidad varchar(9) NOT NULL,
    motivo varchar(200) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Transfusiones recibidas';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_transfusiones;

-- la recreamos
CREATE TABLE auditoria_transfusiones (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    fecha_transfusion date NOT NULL,
    localidad varchar(9) NOT NULL,
    motivo varchar(200) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las Transfusiones';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_transfusiones;

-- lo recreamos
CREATE TRIGGER edicion_transfusiones
AFTER UPDATE ON transfusiones
FOR EACH ROW
INSERT INTO auditoria_transfusiones
    (id,
     id_protocolo,
     fecha_transfusion,
     localidad,
     motivo,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.fecha_transfusion,
     OLD.localidad,
     OLD.motivo,
     OLD.id_usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_transfusiones;

-- lo recreamos
CREATE TRIGGER eliminacion_transfusiones
AFTER DELETE ON transfusiones
FOR EACH ROW
INSERT INTO auditoria_transfusiones
    (id,
     id_protocolo,
     fecha_transfusion,
     localidad,
     motivo,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.fecha_transfusion,
     OLD.localidad,
     OLD.motivo,
     OLD.id_usuario,
     OLD.fecha,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                            Transplantes Recibidos                      */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de transplantes
 id entero(8) autonumerico, clave del registro
 id_protocolo entero(8) clave del protocolo
 organo entero pequeño (2) clave del organo recibido
 positivo enum (si / no / no sabe) indica si el organo recibido era
          positivo para chagas
 fecha_transplante date fecha del transplante
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS transplantes;

-- la creamos
CREATE TABLE transplantes (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    organo tinyint(2) UNSIGNED NOT NULL,
    positivo enum("Si", "No", "No sabe") NOT NULL,
    fecha_transplante date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY organo(organo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Transplantes recibidos';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_transplantes;

-- la creamos
CREATE TABLE auditoria_transplantes (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    organo tinyint(2) UNSIGNED NOT NULL,
    positivo enum("Si", "No", "No Sabe") NOT NULL,
    fecha_transplante date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY organo(organo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Transplantes';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_transplantes;

-- lo recreamos
CREATE TRIGGER edicion_transplantes
AFTER UPDATE ON transplantes
FOR EACH ROW
INSERT INTO auditoria_transplantes
    (id,
     id_protocolo,
     organo,
     positivo,
     fecha_transplante,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.organo,
     OLD.positivo,
     OLD.fecha_transplante,
     OLD.id_usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_transplantes;

-- lo recreamos
CREATE TRIGGER eliminacion_transplantes
AFTER DELETE ON transplantes
FOR EACH ROW
INSERT INTO auditoria_transplantes
    (id,
     id_protocolo,
     organo,
     positivo,
     fecha_transplante,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.organo,
     OLD.positivo,
     OLD.fecha_transplante,
     OLD.id_usuario,
     OLD.fecha,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                       Diccionario de Organos                           */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de organos
 id entero pequeño (2) clave del registro
 organo varchar(100) nombre del organo
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS organos;

-- la creamos
CREATE TABLE organos (
    id tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    organo varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de Organos';

-- cargamos los registros iniciales
INSERT INTO organos (organo, id_usuario) VALUES ("Corazon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Higado", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Riñon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Pulmon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Medula", 1);


/**************************************************************************/
/*                                                                        */
/*                         Usuarios Autorizados                           */
/*                                                                        */
/**************************************************************************/

/*
 Para evitar complicaciones, vamos a usar a tabla de usuarios de control
 de calidad y relacionarlo con la tabla de permisos

 Estructura de la tabla de usuarios

 id, entero(4) clave de la tabla de usuarios sincronizada con la tabla
     de usuarios del sistema cce
 autorizo, clave del usuario que autorizo
 matricula varchar(20) número de matrícula para los profesionales
 profesion varchar(50) profesión o especialidad (para los certificados)
 laboratorio, entero clave del laboratorio al que pertenece (es distinto
              de la clave laboratorio del sistema cce)
 departamento, entero clave del departamento
 administrador enum (es o no administrador)
 personas enum(si o no puede editar personas)
 congenito enum(si / no) si puede editar chagas congenito
 resultados enum(si / no) si puede cargar resultados
 muestras enum(si / no) si puede cargar muestras
 entrevistas enum(si / no) si puede cargar entrevistas
 auxiliares enum(si / no) si puede editar tablas auxiliares
 protocolos enum(si / no) si puede firmar protocolos
 stock enum (si / no) si puede editar el stock
 usuarios enum (si / no) si puede editar usuarios del sistema
 clinica enum (si / no) si puede editar datos de historia clínica
 firma blob contenido de la firma (si puede firmar protocolos)

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS usuarios;

-- la recreamos desde cero
CREATE TABLE usuarios(
    id int(4) UNSIGNED NOT NULL,
    autorizo int(4) UNSIGNED NOT NULL,
    matricula varchar(20) DEFAULT NULL,
    profesion varchar(50) DEFAULT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    administrador enum("Si", "No") DEFAULT "No",
    personas enum("Si", "No") DEFAULT "No",
    congenito enum("Si", "No") DEFAULT "No",
    resultados enum("Si", "No") DEFAULT "No",
    muestras enum("Si", "No") DEFAULT "No",
    entrevistas enum("Si", "No") DEFAULT "No",
    auxiliares enum("Si", "No") DEFAULT "No",
    protocolos enum("Si", "No") DEFAULT "No",
    stock enum("Si", "No") DEFAULT "No",
    usuarios enum("Si", "No") DEFAULT "No",
    clinica enum("Si", "No") DEFAULT "No",
    firma mediumblob DEFAULT NULL,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY departamento(departamento),
    KEY autorizo(autorizo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Usuarios autorizados del sistema';

-- insertamos el primer registro
INSERT INTO usuarios
    (id,
     autorizo,
     matricula,
     profesion,
     laboratorio,
     departamento,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     clinica)
    VALUES
    ('1',
     '1',
     'M.N. 12811',
     'Psicólogo',
     '397',
     '1',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si');

-- insertamos el resto de los registros
INSERT INTO `usuarios` (`id`, `autorizo`, `laboratorio`, `departamento`, `administrador`, `personas`, `congenito`, `resultados`, `muestras`, `entrevistas`, `auxiliares`, `protocolos`, `stock`, `usuarios`, `clinica`, `firma`) VALUES
(30, 1, 397, 1, 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'No', 0x2e2e2f696d6167656e65732f696d6167656e5f6e6f5f646973706f6e69626c652e676966),
(56, 1, 397, 1, 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'No', 0x2e2e2f696d6167656e65732f696d6167656e5f6e6f5f646973706f6e69626c652e676966),
(59, 1, 397, 1, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Si', 'No', 'No', 0x2e2e2f696d6167656e65732f696d6167656e5f6e6f5f646973706f6e69626c652e676966),
(58, 1, 397, 1, 'No', 'Si', 'Si', 'Si', 'Si', 'No', 'No', 'No', 'Si', 'No', 'No', 0x2e2e2f696d6167656e65732f696d6167656e5f6e6f5f646973706f6e69626c652e676966);

-- elimina la tabla de auditoria de usuarios si existe
DROP TABLE IF EXISTS auditoria_usuarios;

-- creacion de la tabla de auditoria de usuarios
CREATE TABLE auditoria_usuarios(
    id int(4) UNSIGNED NOT NULL,
    autorizo int(4) UNSIGNED NOT NULL,
    matricula varchar(20) DEFAULT NULL,
    profesion varchar(50) DEFAULT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    administrador enum("Si", "No") DEFAULT "No",
    personas enum("Si", "No") DEFAULT "No",
    congenito enum("Si", "No") DEFAULT "No",
    resultados enum("Si", "No") DEFAULT "No",
    muestras enum("Si", "No") DEFAULT "No",
    entrevistas enum("Si", "No") DEFAULT "No",
    auxiliares enum("Si", "No") DEFAULT "No",
    protocolos enum("Si", "No") DEFAULT "No",
    stock enum("Si", "No") DEFAULT "No",
    usuarios enum("Si", "No") DEFAULT "No",
    clinica enum("Si", "No") DEFAULT "No",
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY autorizo(autorizo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de usuarios del sistema';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_usuarios;

-- lo recreamos
CREATE TRIGGER edicion_usuarios
AFTER UPDATE ON usuarios
FOR EACH ROW
INSERT INTO auditoria_usuarios
    (id,
     autorizo,
     matricula,
     profesion,
     laboratorio,
     departamento,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     clinica,
     evento)
    VALUES
    (OLD.id,
     OLD.autorizo,
     OLD.matricula,
     OLD.profesion,
     OLD.laboratorio,
     OLD.departamento,
     OLD.administrador,
     OLD.personas,
     OLD.congenito,
     OLD.resultados,
     OLD.muestras,
     OLD.entrevistas,
     OLD.auxiliares,
     OLD.protocolos,
     OLD.stock,
     OLD.usuarios,
     OLD.clinica,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_usuarios;

-- lo recreamos
CREATE TRIGGER eliminacion_usuarios
AFTER DELETE ON usuarios
FOR EACH ROW
INSERT INTO auditoria_usuarios
    (id,
     autorizo,
     matricula,
     profesion,
     laboratorio,
     departamento,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     clinica,
     evento)
    VALUES
    (OLD.id,
     OLD.autorizo,
     OLD.matricula,
     OLD.profesion,
     OLD.laboratorio,
     OLD.departamento,
     OLD.administrador,
     OLD.personas,
     OLD.congenito,
     OLD.resultados,
     OLD.muestras,
     OLD.entrevistas,
     OLD.auxiliares,
     OLD.protocolos,
     OLD.stock,
     OLD.usuarios,
     OLD.clinica,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                      Pruebas Reconocidas                               */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de pruebas o tecnicas
 id entero pequeño(3) clave del registro
 tecnica varchar(30) nombre genérico de la técnica
 nombre varchar(30) nombre completo de la técnica
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tecnicas;

-- la recreamos
CREATE TABLE tecnicas (
    id int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    tecnica VARCHAR(30) DEFAULT NULL,
    nombre VARCHAR(60) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY tecnica(tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de técnicas';

-- cargamos los valores iniciales
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (1, 'AP', 'Aglutinacion de Particulas', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (2, 'ELISA', 'Ensayo por Inmunoadsorción Ligado a Enzimas', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (3, 'HAI', 'Hemaglutinacion Indirecta', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (4, 'IFI', 'Inmunofluorescencia', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (5, 'Quimioluminiscencia', 'Quimioluminiscencia', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (6, 'OTRA', 'Otra', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (7, 'PCR', 'Reaccion en Cadena de la Polimerasa', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (8, 'Micrometodo', 'Micrometodo', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (9, 'XENO', 'Xenodiagnostico', 1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tecnicas;

-- la recreamos
CREATE TABLE auditoria_tecnicas (
    id int(3) UNSIGNED NOT NULL,
    tecnica VARCHAR(30) DEFAULT NULL,
    nombre VARCHAR(60) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY tecnica(tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de técnicas';

-- eliminamos el trigger de edicion
DROP TRIGGER IF EXISTS edicion_tecnicas;

-- lo recreamos
CREATE TRIGGER edicion_tecnicas
AFTER UPDATE ON tecnicas
FOR EACH ROW
INSERT INTO auditoria_tecnicas
       (id,
        tecnica,
        nombre,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.tecnica,
        OLD.nombre,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_tecnicas;

-- lo recreamos
CREATE TRIGGER eliminacion_tecnicas
AFTER DELETE ON tecnicas
FOR EACH ROW
INSERT INTO auditoria_tecnicas
       (id,
        tecnica,
        nombre,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.tecnica,
        OLD.nombre,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                         Marcas de Reactivos                              */
/*                                                                          */
/****************************************************************************/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `marcas`;

-- recreamos desde cero
CREATE TABLE IF NOT EXISTS `marcas` (
  `ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de las marcas utilizadas';

-- agregamos los registros iniciales
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (1,'Serodia','1','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (3,'Biomerieux','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (4,'Lemos','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (5,'Bioschile','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (6,'Biozima','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (7,'Lisado de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (8,'Polychaco','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (9,'Fatalakit','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (10,'Hemave','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (11,'Biocientifica','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (12,'Inmunofluor','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (13,'Ag Fatala','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (14,'Otra','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (15,'Otra','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (19,'Otra','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (21,'Otra','1','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (24,'Recombinante 3.0 de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (25,'Recombinante 4.0 de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (26,'Wiener','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (27,'ABBOT Architect','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (29,'Otra','6','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (30,'Lemos Recombinante','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (31,'ABBOT Architect','5','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (32,'Otra','5','1');

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_marcas;

-- creamos la tabla de auditoría marcas
CREATE TABLE IF NOT EXISTS `auditoria_marcas` (
  `ID` tinyint(2) UNSIGNED NOT NULL,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_EVENTO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `EVENTO` enum("Edicion", "Eliminacion"),
  KEY `ID`(`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las marcas utilizadas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_marcas;

-- lo recreamos
CREATE TRIGGER edicion_marcas
AFTER UPDATE ON marcas
FOR EACH ROW
INSERT INTO auditoria_marcas
       (ID,
        MARCA,
        TECNICA,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.MARCA,
        OLD.TECNICA,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Edicion");

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS eliminacion_marcas;

-- lo recreamos
CREATE TRIGGER eliminacion_marcas
AFTER DELETE ON marcas
FOR EACH ROW
INSERT INTO auditoria_marcas
       (ID,
        MARCA,
        TECNICA,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.MARCA,
        OLD.TECNICA,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                         Valores de Corte                                 */
/*                                                                          */
/****************************************************************************/

/*

 Estructura de la tabla de valores de corte de cada prueba, si una técnica
 tiene varios valores el sistema debe presentar un desplegable, si tiene
 solo un valor entiende que se trata de una máscara de entrada
 id entero(4) autonumerico, clave unica del registro
 id_tecnica entero pequeño(3) clave de la tecnica
 valor varchar(10) uno de los valores aceptados o la mascara de entrada
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS valores_tecnicas;

-- la recreamos
CREATE TABLE valores_tecnicas (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_tecnica smallint(3) UNSIGNED NOT NULL,
    valor varchar(10) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de valores de las técnicas';

-- insertamos los valores iniciales
INSERT INTO valores_tecnicas
        (id_tecnica, valor, id_usuario) VALUES
        (1,"1/4",1),
        (1,"1/8",1),
        (1,"1/16",1),
        (1,"1/32",1),
        (1,"1/64",1),
        (1,"1/128",1),
        (1,"1/256",1),
        (1,"1/512",1),
        (1,"1/1024",1),
        (1,"1/2048",1),
        (1,"1/4096",1),
        (3,"1/4",1),
        (3,"1/8",1),
        (3,"1/16",1),
        (3,"1/32",1),
        (3,"1/64",1),
        (3,"1/128",1),
        (3,"1/256",1),
        (3,"1/512",1),
        (3,"1/1024",1),
        (3,"1/2048",1),
        (3,"1/4096",1),
        (4,"1/4",1),
        (4,"1/8",1),
        (4,"1/16",1),
        (4,"1/32",1),
        (4,"1/64",1),
        (4,"1/128",1),
        (4,"1/256",1),
        (4,"1/512",1),
        (4,"1/1024",1),
        (4,"1/2048",1),
        (4,"1/4096",1),
        (2,"9.999",1),
        (5,"99.999",1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_valores;

-- la recreamos
CREATE TABLE auditoria_valores (
    id int(4) UNSIGNED NOT NULL,
    id_tecnica smallint(3) UNSIGNED NOT NULL,
    valor varchar(10) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de valores de las técnicas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_valores;

-- lo recreamos
CREATE TRIGGER edicion_valores
AFTER UPDATE ON valores_tecnicas
FOR EACH ROW
INSERT INTO auditoria_valores
       (id,
        id_tecnica,
        valor,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.id_tecnica,
        OLD.valor,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_valores;

-- lo recreamos
CREATE TRIGGER eliminacion_valores
AFTER DELETE ON valores_tecnicas
FOR EACH ROW
INSERT INTO auditoria_valores
       (id,
        id_tecnica,
        valor,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.id_tecnica,
        OLD.valor,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                  Instituciones Asistenciales                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de instituciones asistenciales
 id_institucion entero(6) autonumerico, clave del registro
 siisa varchar(20) clave siisa de la institución
 nombre varchar(250) nombre del centro asistencial
 id_localidad varchar(9) clave de la localidad del centro (a traves de la
              localidad obtenemos la jurisdiccion y el pais)
 direccion varchar(250) direccion postal de la institucion
 codigo_postal varchar(20) codigo postal de la direccion
 telefono varchar(20) telefono de la institucion
 mail varchar(50) direccion de correo de la institucion
 responsable varchar(150) nombre del responsable o director
 dependencia entero pequeño (1) clave de la dependencia
 coordenadas varchar(100) coordenadas gps
 comentarios texto comentarios del usuario
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS centros_asistenciales;

-- creamos la tabla
CREATE TABLE `centros_asistenciales` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `siisa` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `localidad` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_postal` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `responsable` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(4) unsigned NOT NULL,
  `dependencia` tinyint(1) unsigned NOT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentarios` text COLLATE utf8_spanish_ci,
  `coordenadas` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siisa` (`siisa`),
  KEY `responsable` (`responsable`),
  KEY `id_usuario` (`id_usuario`),
  KEY `dependencia` (`dependencia`),
  KEY `nombre` (`nombre`),
  KEY `localidad` (`localidad`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Centros Asistenciales';

-- eliminamos la tabla
DROP TABLE IF EXISTS auditoria_centros_asistenciales;

-- creamos la tabla
CREATE TABLE `auditoria_centros_asistenciales` (
  `id` int(6) unsigned NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `siisa` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `localidad` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_postal` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `responsable` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(4) unsigned NOT NULL,
  `dependencia` tinyint(1) unsigned NOT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_evento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentarios` text COLLATE utf8_spanish_ci,
  `coordenadas` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `evento` enum('Edicion', 'Eliminacion') NOT NULL,
  KEY `id`(`id`),
  KEY `siisa` (`siisa`),
  KEY `responsable` (`responsable`),
  KEY `id_usuario` (`id_usuario`),
  KEY `dependencia` (`dependencia`),
  KEY `nombre` (`nombre`),
  KEY `localidad` (`localidad`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Centros Asistenciales';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_instituciones;

-- lo recreamos
CREATE TRIGGER edicion_centros_asistenciales
AFTER UPDATE ON centros_asistenciales
FOR EACH ROW
INSERT INTO auditoria_centros_asistenciales
    (id,
     nombre,
     siisa,
     localidad,
     direccion,
     codigo_postal,
     telefono,
     mail,
     responsable,
     id_usuario,
     dependencia,
     fecha_alta,
     comentarios,
     coordenadas,
     evento)
    VALUES
    (OLD.id,
     OLD.nombre,
     OLD.siisa,
     OLD.localidad,
     OLD.direccion,
     OLD.codigo_postal,
     OLD.telefono,
     OLD.mail,
     OLD.responsable,
     OLD.id_usuario,
     OLD.dependencia,
     OLD.fecha_alta,
     OLD.comentarios,
     OLD.coordenadas,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_instituciones;

-- lo recreamos
CREATE TRIGGER eliminacion_centros_asistenciales
AFTER DELETE ON centros_asistenciales
FOR EACH ROW
INSERT INTO auditoria_centros_asistenciales
    (id,
     nombre,
     siisa,
     localidad,
     direccion,
     codigo_postal,
     telefono,
     mail,
     responsable,
     id_usuario,
     dependencia,
     fecha_alta,
     comentarios,
     coordenadas,
     evento)
    VALUES
    (OLD.id,
     OLD.nombre,
     OLD.siisa,
     OLD.localidad,
     OLD.direccion,
     OLD.codigo_postal,
     OLD.telefono,
     OLD.mail,
     OLD.responsable,
     OLD.id_usuario,
     OLD.dependencia,
     OLD.fecha_alta,
     OLD.comentarios,
     OLD.coordenadas,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                    Protocolos o Certificados                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de protocolos
 id_informe entero(8) autonumérico clave del registro
 id_protocolo entero(8) clave del registro de pacientes
 informe blob contenido del resultado en pdf
 id_usuario entero(4) usuario que imprimió el registro
 fecha date fecha de impresión o entrega

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS protocolos;

-- la creamos
CREATE TABLE protocolos (
    id_informe int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    informe blob,
    id_usuario int(8) UNSIGNED NOT NULL,
    fecha_impresion date DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_informe),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Protocolos de pacientes';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_protocolos;

-- la creamos
CREATE TABLE auditoria_protocolos (
    id_informe int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    informe blob,
    id_usuario int(8) UNSIGNED NOT NULL,
    fecha_impresion date DEFAULT NULL,
    fecha date NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id_informe(id_informe),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Protocolos de pacientes';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_protocolos;

-- lo recreamos
CREATE TRIGGER edicion_protocolos
AFTER UPDATE ON protocolos
FOR EACH ROW
INSERT INTO auditoria_protocolos
    (id_informe,
     id_protocolo,
     informe,
     id_usuario,
     fecha_impresion,
     fecha,
     evento)
    VALUES
    (OLD.id_informe,
     OLD.id_protocolo,
     OLD.informe,
     OLD.id_usuario,
     OLD.fecha_impresion,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_protocolos;

-- lo recreamos
CREATE TRIGGER eliminacion_protocolos
AFTER DELETE ON protocolos
FOR EACH ROW
INSERT INTO auditoria_protocolos
    (id_informe,
     id_protocolo,
     informe,
     id_usuario,
     fecha_impresion,
     fecha,
     evento)
    VALUES
    (OLD.id_informe,
     OLD.id_protocolo,
     OLD.informe,
     OLD.id_usuario,
     OLD.fecha_impresion,
     OLD.fecha,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Tablas de No Conformidades                                     */
/*                                                                                          */
/********************************************************************************************/

/*

    Estructura de la tabla
    id int(6) clave del registro
    usuario int(4) clave del usuario que generó la no conformidad
    titulo varchar(100) descripción breve de la no conformidad
    fecha date fecha de emisión de la no conformidad
    detalle blob descripción de la no conformidad
    causa blob análisis de las causas
    respuesta date fecha de la respuesta
    accion blob detalle de las acciones correctivas
    ejecucion date fecha límite para la ejecución de la acción correctiva

*/

-- eliminamos si existe
DROP TABLE IF EXISTS noconformidad;

-- creamos la tabla
CREATE TABLE noconformidad(
    id int(6) unsigned NOT NULL AUTO_INCREMENT,
    usuario int(4) unsigned NOT NULL,
    titulo varchar(100) NOT NULL,
    fecha date DEFAULT NULL,
    detalle blob DEFAULT NULL,
    causa blob DEFAULT NULL,
    respuesta date DEFAULT NULL,
    accion blob DEFAULT NULL,
    ejecucion date DEFAULT NULL,
    PRIMARY KEY (id),
    KEY usuario(usuario),
    KEY fecha(fecha),
    KEY respuesta(respuesta),
    KEY ejecucion(ejecucion)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Incidencias y no conformidades';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_conformidades;

-- la recreamos
CREATE TABLE auditoria_conformidades(
    id int(6) unsigned NOT NULL,
    usuario int(4) unsigned NOT NULL,
    titulo varchar(100) NOT NULL,
    fecha date DEFAULT NULL,
    detalle blob DEFAULT NULL,
    causa blob DEFAULT NULL,
    respuesta date DEFAULT NULL,
    accion blob DEFAULT NULL,
    ejecucion date DEFAULT NULL,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY usuario(usuario),
    KEY fecha(fecha),
    KEY respuesta(respuesta),
    KEY ejecucion(ejecucion)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de no conformidades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_conformidades;

-- lo recreamos
CREATE TRIGGER edicion_conformidades
AFTER UPDATE ON noconformidad
FOR EACH ROW
INSERT INTO auditoria_conformidades
    (id,
     usuario,
     fecha,
     titulo,
     detalle,
     causa,
     respuesta,
     accion,
     ejecucion,
     evento)
    VALUES
    (OLD.id,
     OLD.usuario,
     OLD.fecha,
     OLD.titulo,
     OLD.detalle,
     OLD.causa,
     OLD.respuesta,
     OLD.accion,
     OLD.ejecucion,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_conformidades;

-- lo recreamos
CREATE TRIGGER eliminacion_conformidades
AFTER DELETE ON noconformidad
FOR EACH ROW
INSERT INTO auditoria_conformidades
    (id,
     usuario,
     fecha,
     titulo,
     detalle,
     causa,
     respuesta,
     accion,
     ejecucion,
     evento)
    VALUES
    (OLD.id,
     OLD.usuario,
     OLD.fecha,
     OLD.titulo,
     OLD.detalle,
     OLD.causa,
     OLD.respuesta,
     OLD.accion,
     OLD.ejecucion,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Tablas de Control de Stock                                     */
/*                                                                                          */
/********************************************************************************************/

/*

  Vamos a crear tablas para
  1. Items y su descripción
  2. Entradas al depósito
  3. Salidas del Depósito
  4. Inventario en existencia (que se actualiza en tiempo real)
  5. La tabla de fuentes de financiamiento (diccionario)

  Se crean triggers para los eventos y el inventario es controlado directamente por los
  triggers (un ingreso al depósito incrementará el inventario existente y una salida
  lo actualizará) esto permite simplificar el código (el inventario solo se consultará)
  y evitar errores de diseño

*/


/********************************************************************************************/
/*                                                                                          */
/*                               Items del Depósito                                         */
/*                                                                                          */
/********************************************************************************************/

/*
   id entero(4) autonumérico
   id_laboratorio entero(6) laboratorio que utiliza este item
   departamento entero(4) clave del departamento
   descripcion varchar(200)
   valor decimal valor de la unidad
   codigosop varchar(20) código del sistema de la apn
   imagen blob imagen del artículo
   critico int(4) valor mínimo crítico
   fecha date
   id_usuario int(4) usuario que editó el registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS items ;

-- la creamos
CREATE TABLE items (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    descripcion varchar(200) NOT NULL,
    valor decimal(10,2) UNSIGNED DEFAULT NULL,
    codigosop varchar(20) DEFAULT NULL,
    imagen blob DEFAULT NULL,
    critico int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY laboratorio(id_laboratorio),
    KEY departamento(departamento),
    KEY descripcion(descripcion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Elementos del inventario';

-- eliminamos la tabla
DROP TABLE IF EXISTS auditoria_items ;

-- la creamos
CREATE TABLE auditoria_items (
    id int(4) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    descripcion varchar(200) NOT NULL,
    valor decimal(10,2) UNSIGNED DEFAULT NULL,
    codigosop varchar(20) DEFAULT NULL,
    critico int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    KEY id(id),
    KEY laboratorio(id_laboratorio),
    KEY descripcion(descripcion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los items';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_items;

-- lo recreamos
CREATE TRIGGER edicion_items
AFTER UPDATE ON items
FOR EACH ROW
INSERT INTO auditoria_items
    (id,
     id_laboratorio,
     departamento,
     descripcion,
     valor,
     codigosop,
     critico,
     fecha,
     evento,
     id_usuario)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.departamento,
     OLD.descripcion,
     OLD.valor,
     OLD.codigosop,
     OLD.critico,
     OLD.fecha,
     "Edicion",
     OLD.id_usuario);

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_items;

-- lo recreamos
CREATE TRIGGER eliminacion_items
AFTER DELETE ON items
FOR EACH ROW
INSERT INTO auditoria_items
    (id,
     id_laboratorio,
     departamento,
     descripcion,
     valor,
     codigosop,
     critico,
     fecha,
     evento,
     id_usuario)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.departamento,
     OLD.descripcion,
     OLD.valor,
     OLD.codigosop,
     OLD.critico,
     OLD.fecha,
     "Eliminacion",
     OLD.id_usuario);

-- cargamos los registros iniciales
INSERT INTO `items` (`id`, `id_laboratorio`, `departamento`, `descripcion`, `valor`, `codigosop`, `imagen`, `critico`, `fecha`, `id_usuario`) VALUES
(283, 397, 1, 'ALGODON; TIPO HIDROFILADO - PRESENTACION ENVASE X 1000GR -,', '50.00', '295-00470-0023', NULL, 10, '2019-01-28 13:42:44', 59),
(284, 397, 1, 'ALCOHOL USO MEDICINAL, PUREZA 96? - PRESENTACION ENVASE X 1000CM3', '80.00', '252-00711-0002', 0x6e756c6c, 12, '2019-01-28 15:46:10', 59),
(285, 397, 1, 'Alcohol- uso medicinal; purez 96 % Observaciones: Gel - alcohol para lavado de manos envase x 500 CM 3', '35.00', '251-00711-0001', NULL, 2, '2019-01-28 13:45:21', 59),
(286, 397, 1, 'ANTEOJO DE SEGURIDAD; MARCO POLICARBONATO - CRISTAL POLICARBONATO -,', '50.00', '262-00268-0014', NULL, 2, '2019-01-28 13:48:31', 59),
(287, 397, 1, 'AGUA BIDESTILADA USO BIOLOGIA MOLECULAR OBSERVACIONES: LIBRE DE RNASA Y DNASA. X50ML', '150.00', '251-02283-0006', NULL, 2, '2019-01-28 13:50:30', 59),
(288, 397, 1, 'AGUJAS QUIRURGICAS; TIPO HIPODERMICA - ESPECIFICACION 22G1 - PRESENTACION C (BB)', '200.00', '295-0441-0070', NULL, 5, '2019-01-28 13:51:27', 59),
(289, 397, 1, 'AGUJA QUIRURGICA; TIPO HIPODERMICA - ESPECIFICACION 23G1 - PRESENTACION CAJA X 100 -.(BB)', '200.00', '295-00441-0069', 0x6e756c6c, 5, '2019-02-08 12:37:41', 59),
(290, 397, 1, 'AGUJAS ;TIPO BUTTERFLY- 23 G CAJA X 50 UNIDADES', '300.00', '295-00441-0212', NULL, 2, '2019-01-28 13:54:15', 59),
(291, 397, 1, 'BARBIJO; TIPO PROTECTOR C/M. TUBERCULOSIS.', '100.00', '295-00428-0013', NULL, 5, '2019-01-28 14:16:07', 59),
(292, 397, 1, 'BARBIJOS, TIPO TABLEADO TRIPLE CON ELASTICO', '20.00', '295-00428-00010', 0x6e756c6c, 15, '2019-01-28 14:55:52', 59),
(293, 397, 1, 'CAJAS P/CRIOTUBOS; MATERIAL POLICARBONATO - TIPO P/TUBOS DE 1,5 A 2mL - CAP', '100.00', '295-05717-0019', NULL, 10, '2019-01-28 14:33:02', 59),
(294, 397, 1, 'CAJAS P/CRIOTUBOS; MATERIAL PLASTICO - TIPO POLICARBONATO - CAPACIDAD 50-10', '120.00', '295-05717-0005', NULL, 5, '2019-01-28 14:34:08', 59),
(295, 397, 1, 'CEPILLOS P/LABORATORIO; TIPO LIMPIEZA DE TUBOS - MATERIAL CERDA - TAMA?O CH', '70.00', '295-01908-0003', NULL, 2, '2019-01-28 14:35:27', 59),
(296, 397, 1, 'CEPILLOS P/LABORATORIO; TIPO LIMPIEZA DE TUBOS - MATERIAL CERDA - TAMA?O GR', '85.00', '295-01908-0001', NULL, 2, '2019-01-28 14:36:15', 59),
(297, 397, 1, 'CEPILLOS P/LABORATORIO; TIPO LIMPIEZA MATERIAL DE VIDRIO - MATERIAL NYLON -', '90.00', '295-01908-0006', NULL, 2, '2019-01-28 14:48:43', 59),
(298, 397, 1, 'COLUMNAS Y SIST.DE PURIFICACIO; ELEMENTO KIT PURIFICAC. DE ADN GENOMICO - A', '1000.00', '251-09046-0034', NULL, 2, '2019-01-28 14:56:52', 59),
(299, 397, 1, 'CRIOTUBOS; TIPO 2mL - MATERIAL POLIPROPILENO - PRESENTACION UNIDAD -.', '25.00', '295-04106-0016', NULL, 100, '2019-01-28 15:00:58', 59),
(300, 397, 1, 'MICROTUBO P/LABORATORIO; MATERIAL POLIPROPILENO - PRESENTACION UNIDAD - MEDIDA 2mL - TIPO LIBRE DE RNASA Y DNASA -,', '25.00', '295-04633-0031', NULL, 100, '2019-01-28 15:06:27', 59),
(301, 397, 1, 'CRIOTUBOS; TIPO ESTERIL X 2mL - MAT. POLIPROPILENO - PRESENTACION CAJA POR 500 UNIDADES (CON O-RING, CIERRE A ROSCA                    ', '500.00', '295-04106-0004', NULL, 2, '2019-01-28 15:34:25', 59),
(302, 397, 1, 'CUBETAS PLÁSTICAS ( RESERVORIOS PLASTICOS) ', '100.00', '258-05469-0008', NULL, 10, '2019-01-28 15:39:55', 59),
(303, 397, 1, 'CUBREOBJETOS; MATERIAL VIDRIO - DIMENSION 22 X 22mm - PRESENTACION CAJA X 1', '300.00', '295-02557-0007', NULL, 5, '2019-01-28 15:44:17', 59),
(304, 397, 1, 'CUBREOBJETOS, MATERIAL VIDRIO, DIEMNSION 24X50MM, CAJA X 1000', '300.00', '295-02557-0025', NULL, 4, '2019-01-28 15:45:17', 59),
(305, 397, 1, 'DESCARTADORES DE AGUJAS; BOCA 2 - CAPACIDAD 11L - SISTEMA DE CIERRE P/AGUJA', '200.00', '295-00216-00009', NULL, 4, '2019-01-28 16:01:43', 59),
(306, 397, 1, 'DESCARTADORES DE AGUJAS; BOCA 1 - CAPACIDAD 1L - SISTEMA DE CIERRE C/TAPA -. ', '150.00', '295-02160-0023', NULL, 5, '2019-01-28 16:05:34', 59),
(307, 397, 1, 'DESCARTADORES DE AGUJAS; BOCA 2 - CAPACIDAD 2L - SISTEMA DE CIERRE P/AGUJAS', '150.00', '295-02160-0010', NULL, 5, '2019-01-28 16:07:25', 59),
(308, 397, 1, 'DESINFECTANTES Y DETERGENTES; PRINCIPIO ACTIVO DETERGENTE - CALIDAD NO IONICO', '500.00', '251-09065-0019', NULL, 5, '2019-01-28 16:08:22', 59),
(309, 397, 1, 'EMBUDO P/MEDICINA; DIAMETRO 47mm - FORMA DE PRESENTACION SOLIDO -.', '200.00', '295-04523-0002', NULL, 1, '2019-01-28 16:09:27', 59),
(310, 397, 1, 'EMBUDOS DE VIDRIO DE 10 CM DE DIAMETRO Y VÁSTAGO DE 6 ML DE LARGO', '250.00', '295-04523-0007', NULL, 1, '2019-01-28 16:10:19', 59),
(311, 397, 1, 'MICROTUBO P/LABORATORIO; MATERIAL POLIPROPILENO - PRESENTACION BOLSA X 1000UN - MEDIDA 1,5mL( EPPENDORF).', '1000.00', '295-04633-0002', NULL, 20, '2019-01-28 16:13:47', 59),
(312, 397, 1, 'FILTROS DE FIBRA DE VIDRIO; MATERIAL VIDRIO BOROSILICATO - DIAMETRO 47mm -', '500.00', '295-04078-0006', NULL, 5, '2019-01-28 16:17:15', 59),
(313, 397, 1, 'FILTROS P/JERINGAS/CANULAS; MEDIDA 0,22Mn - DIAMETRO 25mm - MATERIAL POLIET', '550.00', '295-03416-0039', NULL, 5, '2019-01-28 16:18:01', 59),
(314, 397, 1, 'FILTROS P/JERINGAS/CANULAS; MEDIDA 0,45?mol - DIAMETRO 33mm - MATERIAL PVDF', '550.00', '295-03416-0042', NULL, 4, '2019-01-28 16:18:40', 59),
(315, 397, 1, 'FILTROS P/JERINGAS/CANULAS; MEDIDA 25mm - DIAMETRO 0,22?m - MATERIAL CELULO CAJA POR 250 UNIDADES', '700.00', '295-03416-0003', NULL, 2, '2019-01-28 16:19:24', 59),
(316, 397, 1, 'GASAS; TIPO TUBULAR COMPACTO - MEDIDA 40 X 0,90M - PRESENTACION PIEZA -.', '700.00', '295-0473-0031', NULL, 1, '2019-01-28 16:21:04', 59),
(317, 397, 1, 'GRADILLA P/TUBO DE ENSAYO; TAMAÑO TUBO DIAM. EXT. 25mm - CANTIDAD DE TUBOS 50 - MATERIAL ALAMBRE PLASTIFICADO -.', '500.00', '295-03017-0025', NULL, 1, '2019-01-28 16:22:18', 59),
(318, 397, 1, 'GRADILLAS P/LABORATORIO; MATERIAL ACERO INOXIDABLE - CANT. DE ELEMENTOS 36', '400.00', '295-03017-0020', NULL, 1, '2019-01-28 16:26:21', 59),
(319, 397, 1, 'GRADILLAS P/LABORATORIO; MATERIAL POLIPROPILENO - CANT. DE ELEMENTOS 96 - M(con tapa)', '700.00', '295-03017-0016', 0x6e756c6c, 1, '2019-02-15 15:47:04', 59),
(320, 397, 1, 'GUANTES USO MEDICINAL; TIPO EXAMINACION - TAMAÑO S 6/7 - MATERIAL NITRILO -', '500.00', '295-01909-0172', 0x6e756c6c, 10, '2019-03-12 13:24:01', 59),
(321, 397, 1, 'GUANTES USO MEDICINAL; TIPO EXAMINACION - TAMAÑO M 7/8 - MATERIAL NITRILO -', '500.00', '295-01909-0173', 0x6e756c6c, 10, '2019-03-12 13:23:21', 59),
(322, 397, 1, 'JERINGAS DESCARTABLES; CAPACIDAD 5mL - AGUJA 25/08 - USO EXTRACCION -.', '25.00', '295-02157-0001', NULL, 100, '2019-01-28 16:49:20', 59),
(323, 397, 1, 'GUANTES USO MEDICINAL; TIPO EXAMINACION - TAMAÑO L 8/9 - MATERIAL NITRILO -', '600.00', '295-01909-0174', 0x6e756c6c, 5, '2019-03-12 13:23:06', 59),
(324, 397, 1, 'JERINGAS DESCARTABLES; CAPACIDAD 10mL - AGUJA 25/08 - USO EXTRACCION -.', '500.00', '295-02157-0003', NULL, 10, '2019-01-29 13:42:03', 59),
(325, 397, 1, 'JERINGAS DESCARTABLES; CAPACIDAD 20mL - AGUJA SIN -.', '550.00', '295-02157-0008', NULL, 2, '2019-01-29 13:43:53', 59),
(326, 397, 1, 'LIMPIADORES, TIPO DETERGENTE PRESENTACION ENVASE X 1Kg -.(LAVADO DE IMPRONTAS)', '300.00', '291-06951-0083', NULL, 1, '2019-01-29 13:55:40', 59),
(327, 397, 1, 'LIMPIADOR ;TIPO LAVANDINA- PRESENTACION ENVASE X 1 L', '40.00', '291-06951-0028', NULL, 6, '2019-01-29 13:58:16', 59),
(328, 397, 1, 'LIMPIADOR ;TIPO LAVANDINA- PRESENTACION ENVASE X 5 L', '80.00', '291-06951-0030', NULL, 2, '2019-01-29 14:00:15', 59),
(329, 397, 1, 'MATRAZ AFORADO; ESTADO SOLIDO - MATERIAL VIDRIO BOROSILICATO - PRESENTACION 500 ML', '150.00', '295-04539-0013', NULL, 2, '2019-01-29 14:01:21', 59),
(330, 397, 1, 'MATRAZ AFORADO; ESTADO SOLIDO - MATERIAL VIDRIO BOROSILICATO - PRESENTACION 1000 ML', '200.00', '295-04539-0005', NULL, 2, '2019-01-29 14:02:13', 59),
(331, 397, 1, 'MATRAZ AFORADO; ESTADO SOLIDO - MATERIAL VIDRIO BOROSILICATO - PRESENTACION 2000 ML ', '230.00', '295-04539-0009', NULL, 2, '2019-01-29 14:03:36', 59),
(332, 397, 1, 'MICROPLACAS; TIPO P/PCR EN TIEMPO REAL - PRESENTACION ENVASE X 10UN - CAP.', '1000.00', '295-07227-0012', 0x6e756c6c, 1, '2019-01-29 14:07:14', 59),
(333, 397, 1, 'MICROTUBOS P/LABORATORIO; MATERIAL POLIPROPILENO - PRESENTACION CAJA POR 1000 UNIDADES - 2 ML', '600.00', '295-04633-0002', NULL, 1, '2019-01-29 14:08:01', 59),
(334, 397, 1, 'MICROTUBOS P/LABORATORIO; MATERIAL POLIPROPILENO - PRESENTACION ENVASE X 500 1,5 ML', '600.00', '295-04633-0011', NULL, 2, '2019-01-29 14:25:16', 59),
(335, 397, 1, 'PARAFILM M (38 MTS. X 10 CM.)', '1000.00', '231-6586-0192', 0x6e756c6c, 1, '2019-02-08 13:29:27', 59),
(336, 397, 1, 'PAPEL FILTRO; FORMA CUADRADA - DIMENSION 60 X 60Cm - PRESENTACION HOJAS', '20.00', '295-03024-0007', NULL, 50, '2019-01-29 14:28:51', 59),
(337, 397, 1, 'GASAS; TIPO ESTERIL IQP CAJAS N°5 - MEDIDA 10 X 10Cm - PRESENTACION CAJA -.', '50.00', '295-0473-0006', 0x6e756c6c, 2, '2019-02-11 15:54:18', 59),
(338, 397, 1, 'TELAS ADHESIVAS; ANCHO 2.50Cm - LONG.DE CUERPO 9M - TIPO HIPOALERGENICA -.', '15.00', '295-00931-0002', NULL, 20, '2019-01-29 14:34:40', 59),
(339, 397, 1, 'TELAS ADHESIVAS; ANCHO 1,25Cm - TIPO HIPOALERGENICA -(BB).', '15.00', '', NULL, 20, '2019-01-29 14:42:54', 59),
(340, 397, 1, 'POLICUBETAS 96 POCILLOS FONDO EN U POLIESTIRENO TRANSPARENTE PAQUETE X 10 UNIDADES', '200.00', '295-02583-0009', NULL, 3, '2019-01-29 14:52:28', 59),
(341, 397, 1, 'PIPETAS DE VIDRIO; USO LABORATORIO - TIPO AFORO SIMPLE 10 ML -. ', '30.00', '295-00469-0069', NULL, 10, '2019-02-06 14:52:01', 59),
(342, 397, 1, 'Propipetas ; tipo 3 valvulas-largo standard-ancho standard', '200.00', '295-03491-0005', NULL, 1, '2019-02-06 14:53:01', 59),
(343, 397, 1, 'Probeta de vidrio borosilicato con tapon x 25 ml de base plastica', '250.00', '295-02763-0003', NULL, 2, '2019-02-06 14:53:49', 59),
(344, 397, 1, 'AGUJAS QUIRURGICAS; TIPO DESCARTABLE - ESPECIFICACION 21G X 1 1/2 - PRESENT', '40.00', '295-0441-0071', NULL, 10, '2019-02-08 12:43:00', 59),
(345, 397, 1, 'CUBRECAMILLA; TIPO DESCARTABLE SIN TIRAS', '30.00', '295-00466-0001', NULL, 5, '2019-02-08 12:48:44', 59),
(346, 397, 1, 'Probeta de vidrio borosilicato con tapon x 10 ml de base plastica', '225.00', '295-02763-0044', NULL, 2, '2019-02-08 14:35:13', 59),
(347, 397, 1, 'Probeta de vidrio borosilicato con tapon x 50 ml de base plastica', '300.00', '295-02763-0059', NULL, 2, '2019-02-08 14:36:08', 59),
(348, 397, 1, 'Probeta de vidrio borosilicato con tapon x 100 ml de base plastica ', '325.00', '295-02763-0002', NULL, 2, '2019-02-08 14:37:02', 59),
(349, 397, 1, 'Probeta de vidrio borosilicato con tapon x 250 ml de base plastica ', '350.00', '295-02763-0009', NULL, 3, '2019-02-08 14:38:14', 59),
(350, 397, 1, 'Probeta de vidrio borosilicato con tapon x 500 ml de base plastica ', '350.00', '295-02763-0056', NULL, 2, '2019-02-08 14:38:51', 59),
(351, 397, 1, 'Probeta de vidrio borosilicato con tapon x 1000 ml de base plastica', '400.00', '295-02763-0057', NULL, 2, '2019-02-08 14:39:46', 59),
(352, 397, 1, 'Probeta de vidrio borosilicato con tapon de 2000 ml de base plastica', '450.00', '295-02763-0058', NULL, 2, '2019-02-08 14:40:19', 59),
(353, 397, 1, 'BIDONES PARA DESCARTAR LIQUIDOS CHICO', '80.00', '', NULL, 2, '2019-02-11 15:58:17', 59),
(354, 397, 1, 'BIDONES PARA DESCARTAR LIQUIDOS MEDIANOS', '100.00', '', NULL, 2, '2019-02-11 16:03:39', 59),
(355, 397, 1, 'BIDONES PARA DESCARTAR LIQUIDOS GRANDES', '120.00', '', NULL, 2, '2019-02-11 16:04:14', 59),
(356, 397, 1, 'BOLSAS AMARRILLAS', '10.00', '', NULL, 20, '2019-02-11 16:53:40', 59),
(357, 397, 1, 'BOLSAS ROJAS CHICAS ', '10.00', '', NULL, 20, '2019-02-11 16:54:17', 59),
(358, 397, 1, 'BOLSAS ROJAS GRANDES', '15.00', '', NULL, 20, '2019-02-11 16:54:57', 59),
(359, 397, 1, 'CAJAS P/CRIOTUBOS; MATERIAL PLASTICO - TIPO POLICARBONATO -CAPACIDAD 81 TUBOS DE 0,2mL - USO PARA LABORATORIO -. ', '150.00', '295-05717-0012', NULL, 5, '2019-02-11 16:58:37', 59),
(360, 397, 1, 'CAJAS PARA RESIDUOS BIOPATOGENICOS  ( ROJAS)', '20.00', '', NULL, 10, '2019-02-11 17:06:32', 59),
(361, 397, 1, 'CAJAS PARA RESIDUOS QUIMICOS  ( AZUL)', '20.00', '', NULL, 10, '2019-02-11 17:07:29', 59),
(362, 397, 1, 'ACIDO CITRICO, ANHIDRO PUREZA 99,5% PROANALISIS ENVASE POR 500 GR', '1000.00', '251-09009-0263', NULL, 1, '2019-02-12 18:55:32', 59),
(363, 397, 1, 'PROTEINAS; COMPUESTO QUIMICO ALBUMINA BOVINA FRACCION V -ESTADO SOLIDO - CALIDAD BIOLOGIA MOLECULAR -CONCENTRACION/PUREZA 99,5% - PRESENTACION ENVASE X 100GR -. ', '2000.00', '251-09037-0035', NULL, 1, '2019-02-12 19:04:27', 59),
(364, 397, 1, 'ALDEHIDOS Y CETONAS; COMPUESTO QUIMICO FORMALDEHIDO - ESTADO SOLUCION - CALIDAD BIOLOGIA MOLECULAR - CONCENTRACION/PUREZA 37% - PRESENTACION ENVASE X 1L.-', '2500.00', '251-09013-0038', NULL, 1, '2019-02-12 19:05:36', 59),
(365, 397, 1, 'AMINAS; COMPUESTO QUIMICO OPD DIHIDROCLORURO - ESTADO SOLIDO COMPRIMIDO - CALIDAD PRO ANALISIS -CONCENTRACION/PUREZA 10mGr - PRESENTACION ENVASE X 100UN -,', '1000.00', '251-09014-0046', NULL, 2, '2019-02-12 19:14:03', 59),
(366, 397, 1, 'ANTICUERPOS P/LABORATORIO; NOMBRE ANTI-IGG HUMANA - ESTADO LIQUIDO - MARCADO CON ISOTIOCIANATO DE FLUORESCEINA - ORIGEN CABRA - PRESENTACION ENVASE X 1mL - TIPO POLICLONAL (IFI)', '3000.00', '251-09017-0313', NULL, 1, '2019-02-12 19:14:59', 59),
(367, 397, 1, 'ANTICOAGULANTE P/PERSONA; TIPO MONO DROGA. DROGA GENÉRICA HEPARINA SÓDICA. FORMA FARMACEUTIC. CONCENTRACIÓN 5000 UI/ML. USO LABORATORIO - PRESENTACION: FRASCO X 5 ML', '1000.00', '252-01873-0036', NULL, 3, '2019-02-12 19:16:08', 59),
(368, 397, 1, 'Cloruro de Sodio estado solido -calidad biologia molecular-hidratación anhidro-', '1000.00', '251-09004-0408', NULL, 1, '2019-02-12 19:16:50', 59),
(369, 397, 1, 'Cloruro de potasio. Calidad biologia molecular 99,9% pureza mínima. Contenido máximo de impurezas: ioduros <= 0.002%, sodio <=0.005%, compuestos de nitrógeno <=0.001%, fosfatos <= 5 ppm. Envase por 1 ', '1000.00', '251-09004-0407', NULL, 1, '2019-02-12 19:19:05', 59),
(370, 397, 1, 'DESINFECTANTE; PRINCIPIO ACTIVO CLOROXILENOL - CONTROL DESINFECTANTE - PRESENTACION LIQUIDO X 1L -.', '800.00', '254-00505-0047', NULL, 2, '2019-02-12 19:19:49', 59),
(371, 397, 1, 'COMP, HIDROXILADOS Y ALCOHOLES; COMPUESTO QUIMICO ETANOL - ESTADO LIQUIDO - CALIDAD HPLC - CONCENTRACION/PUREZA 100% - PRESENTACION ENVASE X 1L,-', '1000.00', '251-09007-0093', NULL, 1, '2019-02-12 19:22:46', 59),
(372, 397, 1, 'COMP. HIDROXILADOS Y ALCOHOLES, COMPUESTO QUIMICO GLICERINA NEUTRA - ESTADO LIQUIDO - CALIDAD PRO ANALISIS - CONCENTRACION/PUREZA 99% - PRESENTACION ENVASE X 1L', '600.00', '251-09007-0129', NULL, 3, '2019-02-12 19:48:43', 59),
(373, 397, 1, 'DESINFECTANTES Y DETERGENTES; PRINCIPIO ACTIVO POLIOXIET, SORBITAN MONOLAUR, - CALIDAD PRO ANALISIS - CONCENTRACION/PUREZA 99 9% - PRESENTACION ENVASE X 1L ( TWEEN 20)', '500.00', '251-09065-0013', NULL, 2, '2019-02-13 16:11:56', 59),
(374, 397, 1, 'ELECTRODO P/PH; TIPO COMBINADO DE VIDRIO - CONECTOR BNC -,', '10001.00', '295-04883-0001', NULL, 1, '2019-03-06 14:07:17', 59),
(375, 397, 1, 'CEPILLOS P/LABORATORIO; TIPO LIMPIEZA DE TUBOS - MATERIAL NYLON - TAMAÑO 1C', '85.00', '295-01908-0004', NULL, 1, '2019-03-12 13:06:55', 59),
(376, 397, 1, 'CRIOTUBOS ; TIPO 2mL - MATERIAL POLIPROPILENO -PRESENTACION UNIDAD .', '30.00', '295-04106-0016', NULL, 500, '2019-03-12 13:08:18', 59),
(377, 397, 1, 'DESCARTADOR AMARILLO (Balde grande )', '200.00', '', NULL, 5, '2019-03-12 13:09:20', 59),
(378, 397, 1, 'GUANTE P/LIMPIEZA; TAMAÑO MEDIANO - MATERIAL LATEX C/FELPA INTERIOR -.', '60.00', '291-00761-0006', NULL, 5, '2019-03-12 13:17:19', 59),
(379, 397, 1, 'GUANTE P/LIMPIEZA; TAMAÑO CHICO - MATERIAL LATEX C/FELPA INTERIOR -.', '60.00', '291-00761-0007', NULL, 5, '2019-03-12 13:18:11', 59),
(380, 397, 1, 'GUANTE USO MEDICINAL; TIPO SIN POLVO - TAMAÑO CHICO - MATERIAL LATEX - PRESENTACION ENVASE X 100', '250.00', '295-01909-0090', 0x6e756c6c, 10, '2019-03-12 13:21:28', 59),
(381, 397, 1, 'GUANTE USO MEDICINAL; TIPO SIN POLVO - TAMAÑO MEDIUM - MATERIAL LATEX - PRESENTACION ENVASE X 100', '250.00', '295-01909-0089', 0x6e756c6c, 10, '2019-03-12 13:22:41', 59),
(382, 397, 1, 'GUANTE USO MEDICINAL; TIPO SIN POLVO - TAMAÑO LARGE - MATERIAL LATEX - PRESENTACION ENVASE X 100', '250.00', '295-01909-0088', 0x6e756c6c, 5, '2019-03-12 13:23:40', 59),
(383, 397, 1, 'GUANTE USO MEDICINAL; TIPO EXAMINACIÓN - TAMAÑO CHICO - MATERIAL LATEX - PRESENTACION ENVASE X 100 U', '250.00', '295-01909-0091', NULL, 10, '2019-03-12 13:28:15', 59),
(384, 397, 1, 'GUANTE USO MEDICINAL;TIPO EXAMINACIÓN- TAMAÑO MEDIANO - MATERIAL LATEX - PRESENTACION ENVASE X 100', '250.00', '295-01909-0080', NULL, 9, '2019-03-12 13:28:55', 59),
(385, 397, 1, 'GUANTE USO MEDICINAL; TIPO EXAMINACION - TAMAÑO GRANDE - MATERIAL LATEX - PRESENTACION ENVASE X 100UN,-', '250.00', '295-01909-0078', NULL, 5, '2019-03-12 13:30:04', 59),
(386, 397, 1, 'PORTAOBJETOS P/MICROSCOPIO; MATERIAL VIDRIO - PRESENTACIONENVASE X 100UN - TAMAÑO 26 X 76mm (Improntas)', '400.00', '295-02562-0018', NULL, 5, '2019-03-12 13:32:19', 59),
(387, 397, 1, 'Placas ópticas de 96 pocillos para PCR en tiempo real compatibles con el sistema CFX 96 realtime System, marca BioRad PRESENTACION 25 un', '500.00', '251-09044-0023', NULL, 1, '2019-03-12 13:38:43', 59),
(388, 397, 1, 'Selladora de placas Realtime P.C.R x 100', '1000.00', '295-05754-0006', NULL, 1, '2019-03-12 14:43:29', 59),
(389, 397, 1, 'TAPAS P/TUBOS; TIPO KHAN - USO CIERRE DE TUBOS - MATERIAL GOMA - PRESENTACI', '25.00', '295-05753-0015', NULL, 50, '2019-03-12 14:45:47', 59),
(390, 397, 1, 'Termometros; tipo p/agua caliente-rango de temperatura 0 A 100 °C ', '350.00', '433-03675-0022', NULL, 1, '2019-03-12 15:41:11', 59),
(391, 397, 1, 'TERMÓMETRO TIPO DIGITAL MULTIPROPÓSITO - ESCALA GRADOS- RANGO DE TEMPERATURA 0 A 100', '350.00', '433-03675-0045', NULL, 1, '2019-03-12 15:42:24', 59),
(392, 397, 1, 'Timer/cronometro-tamaño bolsillo- mecanismo digital', '500.00', '437-01880-0020', NULL, 1, '2019-03-12 15:42:59', 59),
(393, 397, 1, 'TIP; TIPO MULTICANAL - PRESENTACION BOLSA X 1000UN - CAP. USO 250µL -.(Blancos)', '800.00', '295-03492-0109', NULL, 2, '2019-03-12 15:45:09', 59),
(394, 397, 1, 'TIP; TIPO AZULES - PRESENTACION BOLSA X 1000UN - CAP. USO 1000µL -.', '800.00', '295-03492-0015', NULL, 3, '2019-03-12 15:45:52', 59),
(395, 397, 1, 'TIP; TIPO C/FILTRO ART10 ESTERILES - PRESENTACION CAJA X 96UN - CAPACIDAD 0,5 A 10µL', '800.00', '295-03492-0020', NULL, 3, '2019-03-12 15:46:39', 59),
(396, 397, 1, 'TIP; TIPO C/FILTRO ART 200 ESTERILES - PRESENTACION CAJA X 96UN ¬CAPACIDAD 50 A 200µL -. ', '800.00', '295-03492-0022', NULL, 2, '2019-03-12 16:51:35', 59),
(397, 397, 1, 'TIP; TIPO C/FILTRO ART - PRESENTACION ENVASE X 96UN - CAPACIDAD 1000µL -.', '800.00', '295-03492-0076', NULL, 2, '2019-03-12 17:39:44', 59),
(398, 397, 1, 'TIP; TIPO C/FILTRO ART 20 ESTERILES - PRESENTACION CAJA X 96UN - CAPACIDAD 2 A 20µL', '800.00', '295-03492-0021', NULL, 2, '2019-03-12 17:40:27', 59),
(399, 397, 1, 'TIP; TIPO AMARILLO - PRESENTACION BOLSA X 1000UN - CAPACIDAD 200µL (Enrrasados)', '600.00', '295-03492-0014', NULL, 2, '2019-03-12 17:42:20', 59),
(400, 397, 1, 'TIP; TIPO  AMARILLOS P/PIPETA GILSON - CAPACIDAD DE PIPETA 20 A 200µ- PRESENTACION BOLSA X 1000UN S/E', '600.00', '295-06471-0001', NULL, 2, '2019-03-12 17:43:10', 59),
(401, 397, 1, 'TUBO DE EXTRACCIÓN AL VACIO CON GEL SEPARADOR X 10 ML', '40.00', '295-02576-0008', NULL, 3, '2019-03-12 17:44:40', 59),
(402, 397, 1, 'TUBOS DE POLIPROPILENO; ESTADO ESTERIL - PRESENTACION BOLSA X 50UN - CAPACIDAD 15mL.', '30.00', '295-04534-0023', NULL, 2, '2019-03-12 17:45:35', 59),
(403, 397, 1, 'TUBOS DE POLIPROPILENO; ESTADO ESTERIL - PRESENTACION UNIDAD ¬CAPACIDAD 50mL -. ', '80.00', '295-04534-0036', NULL, 50, '2019-03-12 17:46:27', 59),
(404, 397, 1, 'TUBO DE ENSAYO; TIPO DESCARTABLE - MATERIAL POLIPROPILENO - DIMENSION 16 X 100mm - TAPA PLASTICA A PRESION.-', '50.00', '295-00468-0124', NULL, 500, '2019-03-12 17:48:09', 59),
(405, 397, 1, 'VASO DE PRECIPITADO; MATERIAL BOROSILICATO - TIPO GRADUADO - CAPACIDAD 50mL - GRADUACION SI', '200.00', '295-02764-0052', NULL, 1, '2019-03-13 14:53:16', 59),
(406, 397, 1, 'VASO DE PRECIPITADO; MATERIAL VIDRIO BOROSILICATO - TIPO PYREX - CAPACIDAD 100mL - GRADUACION SI -,', '250.00', '295-02764-0029', NULL, 1, '2019-03-13 14:54:20', 59),
(407, 397, 1, 'VASO DE PRECIPITADO; MATERIAL VIDRIO BOROSILICATO - TIPO PYREX - CAPACIDAD 200 ml', '300.00', '295-02764-0030', NULL, 1, '2019-03-14 12:53:31', 59),
(408, 397, 1, 'VASOS DE PRECIPITADO; MATERIAL VIDRIO BOROSILICATO - CAPACIDAD 500mL - ', '350.00', '295-02515-0004', NULL, 1, '2019-03-14 12:54:23', 59),
(409, 397, 1, 'VASO DE PRECIPITADO; MATERIAL VIDRIO BOROSILICATO - TIPO RESISTENTE AL CALOR - 1000 ML', '400.00', '295-02764-0032', NULL, 1, '2019-03-14 12:54:54', 59),
(410, 397, 1, 'ENZIMAS; COMPUESTO QUIMICO URACIL DNA GLICOSILASA - ESTADO LIQUIDO - CALIDAD BIOLOGIA MOLECULAR - PRESENTACION ENVASE X 500DETERM. -.(UDG)', '1000.00', '251-09028-0403', NULL, 1, '2019-03-14 13:18:46', 59),
(411, 397, 1, 'NUCLEOTIDOS Y NUCLEOSIDOS; COMPUESTO QUIMICO TAQMAN P/GEN RNASAP - ESTADO LIQUIDO - CALIDAD BIOLOGIA MOLECULAR - PRESENTACION ENVASE X 1000 DETERM. -', '1500.00', '251-09045-0129', NULL, 1, '2019-03-14 13:20:07', 59),
(412, 397, 1, 'PAPEL INDICADOR PH; TIPO VARILLA - PRESENTACION CAJA X 100UN - DIMENSION 5 (TIRAS P-H )', '500.00', '295-06529-0007', NULL, 2, '2019-03-14 13:21:35', 59),
(413, 397, 1, 'PATRON/ESTANDAR P/CALIBRACION; EQUIPO MEDIDOR DE PH - MARCA EQUIPO GENERICO( BUFFER)', '800.00', '251-08937-0530', NULL, 1, '2019-03-14 13:22:40', 59),
(414, 397, 1, 'PATRON/ESTANDAR P/CALIBRACION; EQUIPO MEDIDOR DE PH - MARCA EQUIPO GENERICO(BUFFER)', '800.00', '251-08937-0531', NULL, 1, '2019-03-14 13:23:09', 59),
(415, 397, 1, 'PATRON/ESTANDAR P/CALIBRACION; EQUIPO MEDIDOR DE PH - MARCA EQUIPO GENERICO(BUFFER)', '800.00', '251-08937-0532', 0x6e756c6c, 1, '2019-03-18 17:06:47', 59),
(416, 397, 1, 'Primers- nucleotidos y nucleosidos;compuesto quimico oligonucleotido de base 20 - estado solido ', '1500.00', '251-09045-0054', NULL, 1, '2019-03-14 14:30:33', 59),
(417, 397, 1, 'REACTIVO; TIPO ANTI IGG HUM, CONJ, C/PEROXID - USO MICROBIOLOGIA - PRESENTACION ENVASE X 1mGr (ELISA)', '3500.00', '251-09017-0448', NULL, 1, '2019-03-14 14:32:57', 59),
(418, 397, 1, 'SALES; COMPUESTO QUIMICO BICARBONATO DE SODIO - ESTADO SOLIDO - CALIDAD REA', '1000.00', '251-09004-0897', NULL, 1, '2019-03-14 14:33:55', 59),
(419, 397, 1, 'SALES; COMPUESTO QUIMICO CARBONATO DE SODIO - ESTADO SOLIDO - CALIDAD PRO ANALISIS - CONCENTRACION/PUREZA 99% - PRESENTACION ENVASE X 500GR - GRADO HIDRATACION ANHIDRO.-***', '1000.00', '251-09004-0374', NULL, 1, '2019-03-14 14:35:33', 59),
(420, 397, 1, 'SALES; COMPUESTO QUIMICO CARBONATO ACIDO DE SODIO - ESTADO SOLIDO - CALIDAD PRO ANALISIS - CONCENTRACION/PUREZA 99% ¬PRESENTACION ENVASE X 1Kg - GRADO HIDRATACION ANHIDRO -. ', '1000.00', '251-09004-0321', NULL, 1, '2019-03-14 14:37:32', 59),
(421, 397, 1, 'SALES, COMPUESTO QUIMICO FOSFATO DIACIDO DE SODIO - ESTADO SOLIDO - CALIDAD PRO ANALISIS - CONCENTRACION/PUREZA 99% - PRESENTACION ENVASE X 1Kg - GRADO HIDRATACION MONOHIDRATADO.', '1000.00', '251-09004-0613', NULL, 1, '2019-03-14 14:38:06', 59),
(422, 397, 1, 'SALES; COMPUESTO QUIMICO FOSFATO DISODICO - ESTADO POLVO - CALIDAD BIOLOGIA MOLECULAR CONCENTRACION/PUREZA 99% -PRESENTACION ENVASE X 1Kg - GRADO HIDRATACION DIHIDRATADO -,', '1000.00', '251-09004-0424', NULL, 1, '2019-03-14 14:40:07', 59),
(423, 397, 1, 'SALES; COMPUESTO QUIMICO CLORURO DE SODIO - ESTADO SOLIDO - CALIDAD BIOLOGI', '1000.00', '251-09004-0408', NULL, 1, '2019-03-14 14:40:40', 59),
(424, 397, 1, 'SALES; COMPUESTO QUIMICO CLORURO DE CALCIO - ESTADO SOLIDO - CALIDAD PRO ANALISIS - CONCENTRACION/PUREZA 100% - PRESENTACION ENVASE X 1Kg - GRADO HIDRATACION ANHIDRO', '1300.00', '251-09004-0191', NULL, 1, '2019-03-14 14:41:30', 59),
(425, 397, 1, 'SALES; COMPUESTO QUIMICO EDTA DISODICO - ESTADO SOLIDO - CALIDAD BIOLOGIA M', '1800.00', '251-09004-0591', NULL, 1, '2019-03-14 14:42:04', 59),
(426, 397, 1, 'SALES; COMPUESTO QUIMICO GUANIDINA CLORHIDRATO - ESTADO SOLIDO - CALIDAD BI', '2000.00', '251-09004-0551', NULL, 1, '2019-03-14 14:42:39', 59),
(427, 397, 1, 'SISTEMA TIPO TAQMAN: MEZCLA DE REACCI?N CON TODOS LOS COMPONENTES OPTIMIZADOS PARA LA AMPLIFICACI?N POR PCR EN TIEMPO REAL, CON ENZIMA ADN POLIMERASA TIPO HOT START CON ACTIVIDAD EXONUCLEASA 3` 5` Y ', '4000.00', '251-08988-0681', 0x6e756c6c, 1, '2019-03-14 14:47:02', 59),
(428, 397, 1, 'SONDA TAQMAN MGB MARCA VIC EN 5 Y QUENCHER MGB EN EL EXTREMO 3 CANT DESDE 6000 PMOLES.( CRUZI 3)', '1000.00', '251-09045-0077', NULL, 1, '2019-03-14 14:44:22', 59),
(429, 397, 1, 'Abrochadora de papel 21/6 21/8 tipo pinza mat metal cromado', '300.00', '292-00261-0012', 0x6e756c6c, 0, '2019-03-19 13:54:17', 59),
(430, 397, 1, 'ADHESIVOS P/PAPEL, PRESENTACION 50mL - TIPO SINTETICO TRANSPARENTE', '35.00', '292-00262-0040', NULL, 1, '2019-03-14 14:58:30', 59),
(431, 397, 1, 'ALMACENAMIENTO PORTATIL 32 GB-CONEXIÓN USB', '250.00', '436-08113-0009', NULL, 1, '2019-03-14 15:04:51', 59),
(432, 397, 1, 'BANDA ELASTICA; DIAMETRO 5Cm - PRESENTACION CAJA DE 100GR - ESPESOR 2mm -,', '150.00', '292-01428-0001', NULL, 1, '2019-03-14 15:05:21', 59),
(433, 397, 1, 'BIBLIORATO; ANCHO 85mm - ANILLO 2 Y PALANCA - TAMA?O A4 - MATERIAL CARTON -,', '180.00', '292-00274-0024', NULL, 2, '2019-03-14 15:05:53', 59),
(434, 397, 1, 'BOLIGRAFO; TRAZO MEDIO , AZUL', '20.00', '292-00317-0031', NULL, 7, '2019-03-14 15:06:32', 59),
(435, 397, 1, 'BOLSA DE POLIETILENO DE ALTA DENSIDAD CON CIERRE HERMETICO TIPO ZIPLOCK TRANSPARENTE,MEDIDA10X14 CM BOLSA X 500 U', '200.00', '258-03977-0092', NULL, 1, '2019-03-14 15:09:34', 59),
(436, 397, 1, 'BROCHE P/ABROCHADORA, NUMERO 21/6 - PRESENTACION CAJA X 5000 UNIDADES -.', '50.00', '292-00700-0020', NULL, 2, '2019-03-14 15:10:09', 59),
(437, 397, 1, 'BROCHE P/ABROCHADORA; NUMERO 24/6 -,', '50.00', '292-00700-0009', NULL, 2, '2019-03-14 15:11:11', 59),
(438, 397, 1, 'CARPETAS; MATERIAL FIBRA - TAMAÑO OFICIO -.', '60.00', '292-01358-0359', NULL, 1, '2019-03-14 15:11:40', 59),
(439, 397, 1, 'CINTAS ADHESIVAS; TIPO EMBALAR, COLOR MARRON - ANCHO 5Cm - LARGO 50M - MATERIAL CELOFAN - ACCESORIO SIN', '100.00', '292-01153-0035', NULL, 3, '2019-03-14 15:12:36', 59),
(440, 397, 1, 'CINTA ADHESIVA TRANSPARENTE 49 mm MAT. CELOFAN', '50.00', '292-01153-0006', NULL, 2, '2019-03-18 14:30:39', 59),
(441, 397, 1, 'CLIP P/PAPEL; NUMERO 3 - MATERIAL PLASTICO - PRESENTACION CAJA X 100.-', '150.00', '292-00660-0002', NULL, 2, '2019-03-18 15:07:32', 59),
(442, 397, 1, 'CORRECTOR DE ESCRITURA; CAPACIDAD 10mL - ENVASE LAPIZ (PUNTA METAL) -,', '80.00', '292-00323-0003', NULL, 2, '2019-03-18 15:33:03', 59),
(443, 397, 1, 'CUADERNO DE 80 HOJAS RAYADAS TAMAÑO OFICIO CON ESPIRAL', '100.00', '231-06587-0028', NULL, 1, '2019-03-18 15:34:10', 59),
(444, 397, 1, 'CUTTER 18 mm  TIPO HOJA ACERADA MAT. MANGO PLASTICO', '120.00', '292-00790-0024', NULL, 1, '2019-03-18 15:36:28', 59),
(445, 397, 1, 'ETIQUETA AUTOADHESIVA;MEDIDA 215,9 X 279,4 mm- PRECENTACIÓN CAJA X 100 U. (hoja entera)', '30.00', '292-00675-0030', NULL, 2, '2019-03-18 15:41:31', 59),
(446, 397, 1, 'FICHAS P/ARCHIVOS; MATERIAL CARTULINA - FONDO BLANCO -IMPRESION RAYADA - DIMENSION 12,5 X 20Cm - PAQUETE X 100 HOJAS -,', '200.00', '231-00860-0008', NULL, 2, '2019-03-18 15:42:17', 59),
(447, 397, 1, 'FICHEROS DE ESCRITORIO; CAPACIDAD 300UN - TAMAÑO FICHA 125 X 200 (Nº 3)mm - MATERIAL METAL - SEPARADOR ALFABETICO -.', '350.00', '292-01955-0003', NULL, 0, '2019-03-18 15:43:08', 59),
(448, 397, 1, 'FOLIO TRANSPARENTE; TAMAÑO A-4 - PERFORACIONES 5 - MATERIAL POLIPROPILENO,', '100.00', '292-01611-0010', NULL, 1, '2019-03-18 15:45:27', 59),
(449, 397, 1, 'FOLIO TRANSPARENTE; TAMAÑO OFICIO - PERFORACIONES 5 - MATERIAL POLIPROPILENO -,', '100.00', '292-01611-0011', NULL, 1, '2019-03-18 15:46:17', 59),
(450, 397, 1, 'GOMA DE BORRAR', '20.00', '292-00661-0016', NULL, 1, '2019-03-18 15:47:22', 59),
(451, 397, 1, 'HILO P/ATAR; TIPO POLIPROPILENO - PESO 400GR -,', '150.00', '292-00851-0019', NULL, 1, '2019-03-18 15:48:37', 59),
(452, 397, 1, 'LAPIZ NEGRO TIPO 2B', '35.00', '292-00848-0015', NULL, 1, '2019-03-18 15:49:38', 59),
(453, 397, 1, 'LIBRO DE ACTAS X 400 HOJAS RAYADAS NUMERADAS', '200.00', '231-06587-0080', NULL, 1, '2019-03-18 15:50:16', 59),
(454, 397, 1, 'MARCADORES; TIPO AL SOLVENTE - TRAZO 2mm - PUNTA REDONDA - COLOR ROJO - RECARGABLE SI -.', '50.00', '292-01616-0041', NULL, 2, '2019-03-18 15:59:04', 59),
(455, 397, 1, 'MARCADORES; TIPO AL SOLVENTE - TRAZO 2mm - PUNTA REDONDA - COLOR AZUL - RECARGABLE SI -.', '80.00', '292-01616-0042', NULL, 2, '2019-03-18 16:46:28', 59),
(456, 397, 1, 'MARCADORES; TIPO AL SOLVENTE - TRAZO 2mm - PUNTA REDONDA - COLOR NEGRO - RECARGABLE SI -.', '80.00', '292-01616-0043', NULL, 2, '2019-03-18 17:09:23', 59),
(457, 397, 1, 'PAPEL EN HOJA, TIPO DE PAPEL PAPEL OBRA - DIMENSION 21 X 29 7Cm - GRAMAJE 80GR/M - PRESENTACION RESMA X 500HOJAS ( RESMA )', '250.00', '231-06563-0129', NULL, 2, '2019-03-18 17:20:18', 59),
(458, 397, 1, 'PAPEL ENCUADERNADO; CANT. DE HOJAS 200 - TAMAÑO 21,5 X 35,5Cm - TIPO LIBRO DE ACTAS -.', '150.00', '231-06587-0078', NULL, 2, '2019-03-18 17:21:23', 59),
(459, 397, 1, 'PAPEL MADERA de 120 x 87 cm EN HOJAS INDIVIDUALES', '50.00', '231-06563-0319', NULL, 10, '2019-03-19 13:49:51', 59),
(460, 397, 1, 'PERFORADORA GRANDE N° PERFORACIONES 2 DIST E/AGUJ FIJO GUIA POSICION PAPEL CON MAT FUNDICIÓN HIERRO BASE MADERA', '500.00', '292-00858-0007', NULL, 1, '2019-03-19 13:54:03', 59),
(461, 397, 1, 'PINCHES PARA PAPELES ALT 170 mm BASE METAL', '150.00', '292-00873-0007', NULL, 0, '2019-03-19 13:55:11', 59),
(462, 397, 1, 'REPUESTO PAPEL MULTICOLOR; MEDIDA 9 X 9Cm - PRESENTACION 400HOJAS -,', '100.00', '292-01466-0002', NULL, 0, '2019-03-19 13:55:58', 59),
(463, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO MODULO PRIMARIO -MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0034', NULL, 1, '2019-03-19 14:07:56', 59),
(464, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO MODULO POLISHER -MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0035', NULL, 1, '2019-03-19 14:08:30', 59),
(465, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO ULTRAFILTRO DE PUNTO FINAL - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0036', NULL, 1, '2019-03-19 14:09:02', 59),
(466, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO LAMPARA UV - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0037', NULL, 1, '2019-03-19 14:09:36', 59),
(467, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO CARTUCHO CON MICROFILTRO - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0038', NULL, 1, '2019-03-19 14:10:11', 59),
(468, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO SIST, BASICO PREFILTRACION - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0039', NULL, 1, '2019-03-19 14:10:38', 59),
(469, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO FILTRO DE CARBON ACTIVADO - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0041', NULL, 1, '2019-03-19 14:11:04', 59),
(470, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO CARTUCHO LAVABLE ?MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0042', NULL, 1, '2019-03-19 14:11:34', 59),
(471, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO MODULO DE PRETRATAMIENTO - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0044', NULL, 1, '2019-03-19 14:12:04', 59),
(472, 397, 1, 'REP, Y ACC, P/PURIFICADOR AGUA; REPUESTO MODULO DE OSMOSIS INVERSA - MODELO EQUIPO SERIE NW15 - MARCA EQUIPO HEAL FORCE -,', '2500.00', '296-07208-0045', NULL, 1, '2019-03-19 14:12:32', 59),
(473, 397, 1, 'MARCADORES; TIPO INDELEBLE - TRAZO MEDIO - PUNTA REDONDA -RECARGABLE NO.COLOR ROJO  ', '50.00', '292-01616-0140', 0x696d6167656e65732f696d6167656e5f6e6f5f646973706f6e69626c652e676966, 3, '2019-04-08 14:43:32', 59);


/********************************************************************************************/
/*                                                                                          */
/*                               Entradas del Depósito                                      */
/*                                                                                          */
/********************************************************************************************/

/*

    Ingresos al depósito
    id entero(6) clave del registro
    item entero(6) clave del item
    id_laboratorio entero(6) clave del laboratorio
    departamento entero(4) clave del departamento
    remito varchar(20) número o identificación del remito de entrada
    cantidad entero(5) número de unidades
    importe decimal(10,2) importe de la factura
    vencimiento fecha de vencimiento del producto
    lote varchar(20) número de lote del artículo
    ubicacion varchar(30) ubicación física del artículo
    fecha date fecha de entrada al depósito
    id_usuario int(4) usuario que ingresó el registro
    comentarios text observaciones

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS ingresos;

-- la recreamos
CREATE TABLE ingresos (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    item int(6) UNSIGNED NOT NULL,
    importe decimal(10,2) UNSIGNED DEFAULT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    id_financiamiento int(1) UNSIGNED NOT NULL,
    remito varchar(20) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    vencimiento date DEFAULT NULL,
    lote varchar(20) DEFAULT NULL,
    ubicacion varchar(30) DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY departamento(departamento),
    KEY id_financiamiento(id_financiamiento),
    KEY remito(remito),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Ingresos al Depósito';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_ingresos;

-- la recreamos
CREATE TABLE auditoria_ingresos (
    id int(6) UNSIGNED NOT NULL,
    item int(6) UNSIGNED NOT NULL,
    importe decimal(10,2) UNSIGNED DEFAULT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    id_financiamiento int(1) UNSIGNED NOT NULL,
    remito varchar(20) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    vencimiento date DEFAULT NULL,
    lote varchar(20) DEFAULT NULL,
    ubicacion varchar(30) DEFAULT NULL,
    fecha date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    comentarios text DEFAULT NULL,
    KEY id(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY departamento(departamento),
    KEY id_financiamiento(id_financiamiento),
    KEY remito(remito),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Ingresos al Depósito';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_ingresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER edicion_ingresos
AFTER UPDATE ON ingresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_ingresos
        (id,
        item,
        importe,
        id_laboratorio,
        departamento,
        id_financiamiento,
        remito,
        cantidad,
        vencimiento,
        lote,
        ubicacion,
        fecha,
        id_usuario,
        evento,
        comentarios)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.importe,
        OLD.id_laboratorio,
        OLD.departamento,
        OLD.id_financiamiento,
        OLD.remito,
        OLD.cantidad,
        OLD.vencimiento,
        OLD.lote,
        OLD.ubicacion,
        OLD.fecha,
        OLD.id_usuario,
        "Edicion",
        OLD.comentarios);

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia + NEW.cantidad - OLD.cantidad,
                                    inventario.valor = (inventario.existencia + NEW.cantidad - OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
          inventario.departamento = NEW.departamento AND
          inventario.item = NEW.item;

END; //

-- cambiamos el delimitador
DELIMITER ;

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ingresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER eliminacion_ingresos
AFTER DELETE ON ingresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_ingresos
        (id,
        item,
        importe,
        id_laboratorio,
        departamento,
        id_financiamiento,
        remito,
        cantidad,
        vencimiento,
        lote,
        ubicacion,
        fecha,
        id_usuario,
        evento,
        comentarios)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.importe,
        OLD.id_laboratorio,
        OLD.departamento,
        OLD.id_financiamiento,
        OLD.remito,
        OLD.cantidad,
        OLD.vencimiento,
        OLD.lote,
        OLD.ubicacion,
        OLD.fecha,
        OLD.id_usuario,
        "Eliminacion",
        OLD.comentarios);

    -- actualizamos el inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia - OLD.cantidad,
                                    inventario.valor = (inventario.existencia - OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = OLD.id_laboratorio AND
          inventario.departamento = OLD.departamento AND
          inventario.item = OLD.item;

END; //

-- actualizamos el delimitador
DELIMITER ;


/********************************************************************************************/
/*                                                                                          */
/*                               Salidas del Depósito                                       */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla con la información de salidas de material del depósito
    id entero(6) clave del registro
    item int(6) clave del item
    id_laboratorio entero(6) clave del laboratorio
    departamento entero(4) clave del departamento
    remito varchar(20) remito de salida
    cantidad int(5) numero de unidades salientes
    fecha date fecha de la operación
    id_entrego int(4) id del usuario que entregó el material
    id_recibio int(4) id del usuario que recibió el material
    comentarios text

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS egresos;

-- la recreamos
CREATE TABLE egresos (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    item int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    remito int(6) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_entrego int(4) UNSIGNED NOT NULL,
    id_recibio int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY departamento(departamento),
    KEY remito(remito),
    KEY id_entrego(id_entrego),
    KEY id_recibio(id_recibio)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Egresos del Depósito';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_egresos;

CREATE TABLE auditoria_egresos (
    id int(6) UNSIGNED NOT NULL,
    item int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    remito int(6) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    fecha date DEFAULT NULL,
    id_entrego int(4) UNSIGNED NOT NULL,
    id_recibio int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY departamento(departamento),
    KEY remito(remito),
    KEY id_entrego(id_entrego),
    KEY id_recibio(id_recibio)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Egresos del Depósito';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_egresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER edicion_egresos
AFTER UPDATE ON egresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_egresos
        (id,
        item,
        id_laboratorio,
        departamento,
        remito,
        cantidad,
        fecha,
        id_entrego,
        id_recibio,
        comentarios,
        evento)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.id_laboratorio,
        OLD.departamento,
        OLD.remito,
        OLD.cantidad,
        OLD.fecha,
        OLD.id_entrego,
        OLD.id_recibio,
        OLD.comentarios,
        "Edicion");

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia - NEW.cantidad + OLD.cantidad,
                                    inventario.valor = (inventario.existencia - NEW.cantidad + OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
          inventario.departamento = NEW.departamento AND
          inventario.item = NEW.item;

END; //

-- actualizamos el delimitador
DELIMITER ;

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_egresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER eliminacion_egresos
AFTER DELETE ON egresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_egresos
        (id,
        item,
        id_laboratorio,
        departamento,
        remito,
        cantidad,
        fecha,
        id_entrego,
        id_recibio,
        comentarios,
        evento)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.id_laboratorio,
        OLD.departamento,
        OLD.remito,
        OLD.cantidad,
        OLD.fecha,
        OLD.id_entrego,
        OLD.id_recibio,
        OLD.comentarios,
        "Eliminacion");

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia + OLD.cantidad,
                                    inventario.valor = (inventario.existencia + OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = OLD.id_laboratorio AND
          inventario.departamento = OLD.departamento AND
          inventario.item = OLD.item;

END; //

-- actualizamos el delimitador
DELIMITER ;


/********************************************************************************************/
/*                                                                                          */
/*                                      Inventario                                          */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de inventario, contiene el resumen y a través de la auditoría el historial de
    elementos en depósito
    id entero(6) clave del registro
    id_laboratorio entero(6) clave del laboratorio
    valor decimal(10,2) valor en existencia (calculado al momento de actualizar la tabla)
    item entero(6) clave del item
    existencia entero(5) número de unidades en existencia

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS inventario;

-- la creamos
CREATE TABLE inventario (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    valor decimal(10,2) DEFAULT NULL,
    item int(6) UNSIGNED NOT NULL,
    existencia int(5) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY id_laboratorio(id_laboratorio),
    KEY departamento(departamento),
    KEY item(item)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Inventario en existencia';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_inventario;

-- la recreamos (la clave primaria aquí es el registro de la
-- transacción)
CREATE TABLE auditoria_inventario (
    id int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    valor decimal(10,2) DEFAULT NULL,
    item int(6) UNSIGNED NOT NULL,
    existencia int(5) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Ingreso", "Egreso", "Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_laboratorio(id_laboratorio),
    KEY item(item)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del Inventario';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_inventario;

-- lo recreamos
CREATE TRIGGER edicion_inventario
AFTER UPDATE ON inventario
FOR EACH ROW
INSERT INTO auditoria_inventario
    (id,
     id_laboratorio,
     departamento,
     valor,
     item,
     existencia,
     evento)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.departamento,
     OLD.valor,
     OLD.item,
     OLD.existencia,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_inventario;

-- lo recreamos
CREATE TRIGGER eliminacion_inventario
AFTER DELETE ON inventario
FOR EACH ROW
INSERT INTO auditoria_inventario
    (id,
     id_laboratorio,
     departamento,
     valor,
     item,
     existencia,
     evento)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.departamento,
     OLD.valor,
     OLD.item,
     OLD.existencia,
     "Eliminacion");

-- cargamos el inventario inicial
INSERT INTO `inventario` (`id`, `id_laboratorio`, `departamento`, `valor`, `item`, `existencia`) VALUES
(283, 397, 1, '50.00', 283, 0),
(284, 397, 1, '30.00', 284, 0),
(285, 397, 1, '35.00', 285, 0),
(286, 397, 1, '50.00', 286, 0),
(287, 397, 1, '150.00', 287, 0),
(288, 397, 1, '200.00', 288, 0),
(289, 397, 1, '200.00', 289, 0),
(290, 397, 1, '300.00', 290, 0),
(291, 397, 1, '100.00', 291, 0),
(292, 397, 1, '20.00', 292, 0),
(293, 397, 1, '100.00', 293, 0),
(294, 397, 1, '120.00', 294, 0),
(295, 397, 1, '70.00', 295, 0),
(296, 397, 1, '85.00', 296, 0),
(297, 397, 1, '90.00', 297, 0),
(298, 397, 1, '1000.00', 298, 0),
(299, 397, 1, '25.00', 299, 0),
(300, 397, 1, '25.00', 300, 0),
(301, 397, 1, '500.00', 301, 0),
(302, 397, 1, '100.00', 302, 0),
(303, 397, 1, '300.00', 303, 0),
(304, 397, 1, '300.00', 304, 0),
(305, 397, 1, '200.00', 305, 0),
(306, 397, 1, '150.00', 306, 0),
(307, 397, 1, '150.00', 307, 0),
(308, 397, 1, '500.00', 308, 0),
(309, 397, 1, '200.00', 309, 0),
(310, 397, 1, '250.00', 310, 0),
(311, 397, 1, '1000.00', 311, 0),
(312, 397, 1, '500.00', 312, 0),
(313, 397, 1, '550.00', 313, 0),
(314, 397, 1, '550.00', 314, 0),
(315, 397, 1, '700.00', 315, 0),
(316, 397, 1, '700.00', 316, 0),
(317, 397, 1, '500.00', 317, 0),
(318, 397, 1, '400.00', 318, 0),
(319, 397, 1, '700.00', 319, 0),
(320, 397, 1, '500.00', 320, 0),
(321, 397, 1, '500.00', 321, 0),
(322, 397, 1, '25.00', 322, 0),
(323, 397, 1, '600.00', 323, 0),
(324, 397, 1, '500.00', 324, 0),
(325, 397, 1, '550.00', 325, 0),
(326, 397, 1, '300.00', 326, 0),
(327, 397, 1, '40.00', 327, 0),
(328, 397, 1, '80.00', 328, 0),
(329, 397, 1, '150.00', 329, 0),
(330, 397, 1, '200.00', 330, 0),
(331, 397, 1, '230.00', 331, 0),
(332, 397, 1, '1000.00', 332, 0),
(333, 397, 1, '600.00', 333, 0),
(334, 397, 1, '600.00', 334, 0),
(335, 397, 1, '1000.00', 335, 0),
(336, 397, 1, '20.00', 336, 0),
(337, 397, 1, '30.00', 337, 0),
(338, 397, 1, '15.00', 338, 0),
(339, 397, 1, '15.00', 339, 0),
(340, 397, 1, '200.00', 340, 0),
(341, 397, 1, '30.00', 341, 0),
(342, 397, 1, '200.00', 342, 0),
(343, 397, 1, '250.00', 343, 0),
(344, 397, 1, '40.00', 344, 0),
(345, 397, 1, '30.00', 345, 0),
(346, 397, 1, '225.00', 346, 0),
(347, 397, 1, '300.00', 347, 0),
(348, 397, 1, '325.00', 348, 0),
(349, 397, 1, '350.00', 349, 0),
(350, 397, 1, '350.00', 350, 0),
(351, 397, 1, '400.00', 351, 0),
(352, 397, 1, '450.00', 352, 0),
(353, 397, 1, '80.00', 353, 0),
(354, 397, 1, '100.00', 354, 0),
(355, 397, 1, '120.00', 355, 0),
(356, 397, 1, '10.00', 356, 0),
(357, 397, 1, '10.00', 357, 0),
(358, 397, 1, '15.00', 358, 0),
(359, 397, 1, '150.00', 359, 0),
(360, 397, 1, '20.00', 360, 0),
(361, 397, 1, '20.00', 361, 0),
(362, 397, 1, '1000.00', 362, 0),
(363, 397, 1, '2000.00', 363, 0),
(364, 397, 1, '2500.00', 364, 0),
(365, 397, 1, '1000.00', 365, 0),
(366, 397, 1, '3000.00', 366, 0),
(367, 397, 1, '1000.00', 367, 0),
(368, 397, 1, '1000.00', 368, 0),
(369, 397, 1, '1000.00', 369, 0),
(370, 397, 1, '800.00', 370, 0),
(371, 397, 1, '1000.00', 371, 0),
(372, 397, 1, '600.00', 372, 0),
(373, 397, 1, '500.00', 373, 0),
(374, 397, 1, '10001.00', 374, 0),
(375, 397, 1, '85.00', 375, 0),
(376, 397, 1, '30.00', 376, 0),
(377, 397, 1, '200.00', 377, 0),
(378, 397, 1, '60.00', 378, 0),
(379, 397, 1, '60.00', 379, 0),
(380, 397, 1, '50.00', 380, 0),
(381, 397, 1, '250.00', 381, 0),
(382, 397, 1, '250.00', 382, 0),
(383, 397, 1, '250.00', 383, 0),
(384, 397, 1, '250.00', 384, 0),
(385, 397, 1, '250.00', 385, 0),
(386, 397, 1, '400.00', 386, 0),
(387, 397, 1, '500.00', 387, 0),
(388, 397, 1, '1000.00', 388, 0),
(389, 397, 1, '25.00', 389, 0),
(390, 397, 1, '350.00', 390, 0),
(391, 397, 1, '350.00', 391, 0),
(392, 397, 1, '500.00', 392, 0),
(393, 397, 1, '800.00', 393, 0),
(394, 397, 1, '800.00', 394, 0),
(395, 397, 1, '800.00', 395, 0),
(396, 397, 1, '800.00', 396, 0),
(397, 397, 1, '800.00', 397, 0),
(398, 397, 1, '800.00', 398, 0),
(399, 397, 1, '600.00', 399, 0),
(400, 397, 1, '600.00', 400, 0),
(401, 397, 1, '40.00', 401, 0),
(402, 397, 1, '30.00', 402, 0),
(403, 397, 1, '80.00', 403, 0),
(404, 397, 1, '50.00', 404, 0),
(405, 397, 1, '200.00', 405, 0),
(406, 397, 1, '250.00', 406, 0),
(407, 397, 1, '300.00', 407, 0),
(408, 397, 1, '350.00', 408, 0),
(409, 397, 1, '400.00', 409, 0),
(410, 397, 1, '1000.00', 410, 0),
(411, 397, 1, '1500.00', 411, 0),
(412, 397, 1, '500.00', 412, 0),
(413, 397, 1, '800.00', 413, 0),
(414, 397, 1, '800.00', 414, 0),
(415, 397, 1, '800.00', 415, 0),
(416, 397, 1, '1500.00', 416, 0),
(417, 397, 1, '3500.00', 417, 0),
(418, 397, 1, '1000.00', 418, 0),
(419, 397, 1, '1000.00', 419, 0),
(420, 397, 1, '1000.00', 420, 0),
(421, 397, 1, '1000.00', 421, 0),
(422, 397, 1, '1000.00', 422, 0),
(423, 397, 1, '1000.00', 423, 0),
(424, 397, 1, '1300.00', 424, 0),
(425, 397, 1, '1800.00', 425, 0),
(426, 397, 1, '2000.00', 426, 0),
(427, 397, 1, '4000.00', 427, 0),
(428, 397, 1, '1000.00', 428, 0),
(429, 397, 1, '300.00', 429, 0),
(430, 397, 1, '35.00', 430, 0),
(431, 397, 1, '250.00', 431, 0),
(432, 397, 1, '150.00', 432, 0),
(433, 397, 1, '180.00', 433, 0),
(434, 397, 1, '20.00', 434, 0),
(435, 397, 1, '200.00', 435, 0),
(436, 397, 1, '50.00', 436, 0),
(437, 397, 1, '50.00', 437, 0),
(438, 397, 1, '60.00', 438, 0),
(439, 397, 1, '100.00', 439, 0),
(440, 397, 1, '50.00', 440, 0),
(441, 397, 1, '150.00', 441, 0),
(442, 397, 1, '80.00', 442, 0),
(443, 397, 1, '100.00', 443, 0),
(444, 397, 1, '120.00', 444, 0),
(445, 397, 1, '30.00', 445, 0),
(446, 397, 1, '200.00', 446, 0),
(447, 397, 1, '350.00', 447, 0),
(448, 397, 1, '100.00', 448, 0),
(449, 397, 1, '100.00', 449, 0),
(450, 397, 1, '20.00', 450, 0),
(451, 397, 1, '150.00', 451, 0),
(452, 397, 1, '35.00', 452, 0),
(453, 397, 1, '200.00', 453, 0),
(454, 397, 1, '50.00', 454, 0),
(455, 397, 1, '80.00', 455, 0),
(456, 397, 1, '80.00', 456, 0),
(457, 397, 1, '250.00', 457, 0),
(458, 397, 1, '150.00', 458, 0),
(459, 397, 1, '50.00', 459, 0),
(460, 397, 1, '500.00', 460, 0),
(461, 397, 1, '150.00', 461, 0),
(462, 397, 1, '100.00', 462, 0),
(463, 397, 1, '2500.00', 463, 0),
(464, 397, 1, '2500.00', 464, 0),
(465, 397, 1, '2500.00', 465, 0),
(466, 397, 1, '2500.00', 466, 0),
(467, 397, 1, '2500.00', 467, 0),
(468, 397, 1, '2500.00', 468, 0),
(469, 397, 1, '2500.00', 469, 0),
(470, 397, 1, '2500.00', 470, 0),
(471, 397, 1, '2500.00', 471, 0),
(472, 397, 1, '2500.00', 472, 0),
(473, 397, 1, '50.00', 473, 0);


/********************************************************************************************/
/*                                                                                          */
/*                           La tabla de pedidos                                            */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de pedidos al depósito, esta tabla permite que los usuarios realicen pedidos de
    material al depósito y luego el usuario autorizado pueda ver la nómina de pedidos
    y aprobarlos (con lo que genera un egreso automáticamente) o rechazarlos
    id int(8) clave del registro
    id_item int(4) clave del item solicitado
    id_usuario int(4) clave del usuario que solicitó
    cantidad int(4) número de unidades solicitadas
    fecha date fecha de la solicitud

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS pedidos;

-- la recreamos
CREATE TABLE pedidos(
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_item int(4) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    cantidad int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_item(id_item),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Pedidos al depósito';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_pedidos;

-- la recreamos
CREATE TABLE auditoria_pedidos(
    id int(8) UNSIGNED NOT NULL,
    id_item int(4) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    cantidad int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_item(id_item),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de pedidos al depósito';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_pedidos;

-- lo recreamos
CREATE TRIGGER edicion_pedidos
AFTER UPDATE ON pedidos
FOR EACH ROW
INSERT INTO auditoria_pedidos
       (id,
        id_item,
        id_usuario,
        cantidad,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_item,
        OLD.id_usuario,
        OLD.cantidad,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_pedidos;

-- lo recreamos
CREATE TRIGGER eliminacion_pedidos
AFTER DELETE ON pedidos
FOR EACH ROW
INSERT INTO auditoria_pedidos
       (id,
        id_item,
        id_usuario,
        cantidad,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_item,
        OLD.id_usuario,
        OLD.cantidad,
        OLD.fecha_alta,
        "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Fuentes de Financiamiento                                      */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de fuentes de financiamiento, no es necesario que sea exclusiva del stock pero
    aquí lo utilizamos para indicar la fuente de financiamiento de los ingresos al
    depósito, este diccionario es individual para cada laboratorio
    id int(1) clave del registro
    fuente varchar(50) descripcion del financiamiento
    id_laboratorio int(6) clave del laboratorio
    id_usuario int(4) clave del usuario
    fecha_alta date fecha del registro

*/

-- la eliminamos si no existe
DROP TABLE IF EXISTS financiamiento;

-- la recreamos
CREATE TABLE financiamiento(
    id int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    fuente varchar(50) NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario con las fuentes de financiamiento';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_financiamiento;

-- la recreamos
CREATE TABLE auditoria_financiamiento(
    id int(1) UNSIGNED NOT NULL,
    fuente varchar(50) NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las fuentes de financiamiento';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_financiamiento;

-- lo recreamos
CREATE TRIGGER edicion_financiamiento
AFTER UPDATE ON financiamiento
FOR EACH ROW
INSERT INTO auditoria_financiamiento
       (id,
        fuente,
        id_laboratorio,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.fuente,
        OLD.id_laboratorio,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_financiamiento;

-- lo recreamos
CREATE TRIGGER eliminacion_financiamiento
AFTER DELETE ON financiamiento
FOR EACH ROW
INSERT INTO auditoria_financiamiento
       (id,
        fuente,
        id_laboratorio,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.fuente,
        OLD.id_laboratorio,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Eliminacion");

-- cargamos los valores iniciales
INSERT INTO `financiamiento`
(`id`, `fuente`, `id_laboratorio`, `id_usuario`, `fecha_alta`) VALUES
(1, 'Tesoro Nacional', 397, 59, '2018-04-16 17:37:25'),
(2, 'Mundo Sano', 397, 59, '2018-04-16 17:38:40'),
(3, 'Proyecto de Investigación', 397, 59, '2018-04-16 17:38:57'),
(4, 'DNDI', 397, 59, '2018-06-05 17:33:44'),
(5, 'Donación', 397, 59, '2018-06-15 14:44:08');


/*

    Aquí creamos los triggers que se van a encargar de controlar el inventario, usamos el
    disparador BEFORE (antes) para usar el otro evento ya que MySQL solo soporta un trigger
    por evento, el concepto es
    - Cuando se elimina una entrada al depósito, resta esa entrada del existente
    - Cuando se elimina una salida al depósito, suma esa salida al existente
    - Cuando se inserta una entrada al depósito, suma esa entrada al existente
    - Cuando se inserta una salida del depósito, resta esa entrada al existente
    - Cuando se edita una entrada, resta la entrada anterior y suma la nueva
    - Cuando se edita una salida, suma la salida anterior y resta la nueva
    Tendremos entonces seis triggers (tres para la tabla de entradas y tres para la tabla
    de salidas)

    Creamos también un trigger para la inserción de items en el diccionario que inicializa
    a cero la tabla de inventario, de tal forma nos aseguramos que todos los items del
    diccionario tienen su correspondencia en el inventario

*/

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS insercion_items;

-- lo recreamos desde cero
CREATE TRIGGER insercion_items
AFTER INSERT ON items
FOR EACH ROW
INSERT INTO inventario
    (id_laboratorio,
     valor,
     item,
     existencia)
    VALUES
    (NEW.id_laboratorio,
     NEW.valor,
     NEW.id,
     '0');

-- eliminamos el trigger de inserción de entradas al depósito
DROP TRIGGER IF EXISTS nueva_entrada_inventario;

-- lo recreamos
CREATE TRIGGER nueva_entrada_inventario
AFTER INSERT ON ingresos
FOR EACH ROW
UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                inventario.item = items.id
                             SET inventario.existencia = inventario.existencia + NEW.cantidad,
                                 inventario.valor = (inventario.existencia + NEW.cantidad) * items.valor
WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
      inventario.item = NEW.item;

-- eliminamos el trigger de inserción de salidas al depósito
DROP TRIGGER IF EXISTS nueva_salida_inventario;

-- lo recreamos
CREATE TRIGGER nueva_salida_inventario
AFTER INSERT ON egresos
FOR EACH ROW
UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                inventario.item = items.id
                             SET inventario.existencia = inventario.existencia - NEW.cantidad,
                                 inventario.valor = (inventario.existencia - NEW.cantidad) * items.valor
WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
      inventario.item = NEW.item;


/***********************************************************************************************/
/*                                                                                             */
/*                              Freezers y Heladeras                                           */
/*                                                                                             */
/***********************************************************************************************/

/*

    Tabla que contiene la información sobre las heladeras a controlar
    id int(6) clave del registro
    laboratorio int(6) clave del laboratorio de la heladera
    departamento int(4) clave del departamento
    marca varchar(50) marca de la heladera
    ubicación varchar(100) descripción de donde se encuentra
    patrimonio varchar(100) cadena con la id de patrimonio
    temperatura int(3) temperatura que que debe estar (con signo)
    temperatura2 int(3) otra temperatura por defecto (para heladeras de dos)
    tolerancia int(2) porcentaje de tolerancia en la temperatura
    usuario int(4) clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS heladeras;

-- la recreamos
CREATE TABLE heladeras (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    laboratorio int(6) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    marca varchar(50) NOT NULL,
    ubicacion varchar(100) DEFAULT NULL,
    patrimonio varchar(100) DEFAULT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    tolerancia int(2) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY laboratorio(laboratorio),
    KEY departamento(departamento),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de heladeras';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_heladeras;

-- la recreamos
CREATE TABLE auditoria_heladeras (
    id int(6) UNSIGNED NOT NULL,
    laboratorio int(6) NOT NULL,
    departamento int(4) NOT NULL,
    marca varchar(50) NOT NULL,
    ubicacion varchar(100) DEFAULT NULL,
    patrimonio varchar(100) DEFAULT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    tolerancia int(2) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY departamento(departamento),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de auditoría de heladeras';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_heladeras;

-- lo recreamos
CREATE TRIGGER edicion_heladeras
AFTER UPDATE ON heladeras
FOR EACH ROW
INSERT INTO auditoria_heladeras
       (id,
        laboratorio,
        departamento,
        marca,
        ubicacion,
        patrimonio,
        temperatura,
        temperatura2,
        tolerancia,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.laboratorio,
        OLD.departamento,
        OLD.marca,
        OLD.ubicacion,
        OLD.patrimonio,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.tolerancia,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_heladeras;

CREATE TRIGGER eliminacion_heladeras
AFTER DELETE ON heladeras
FOR EACH ROW
INSERT INTO auditoria_heladeras
       (id,
        laboratorio,
        departamento,
        marca,
        ubicacion,
        patrimonio,
        temperatura,
        temperatura2,
        tolerancia,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.laboratorio,
        OLD.departamento,
        OLD.marca,
        OLD.ubicacion,
        OLD.patrimonio,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.tolerancia,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");

-- insertamos los registros
INSERT INTO `heladeras`
        (`id`, `laboratorio`, `departamento`, `marca`, `ubicacion`, `patrimonio`, `temperatura`, `temperatura2`, `tolerancia`, `usuario`, `fecha_alta`) VALUES
        (1, 397, 1, '09 - Silken', 'Control de Calidad', 's/Datos', -15, 0, 5, 1, '2019-03-25 19:04:47'),
        (2, 397, 1, '10 - Gafa', 'Control de Calidad', 's/Datos', -20, 0, 7, 1, '2019-03-22 20:13:01'),
        (3, 397, 1, '12 - Electrolux', 'Control de Calidad', 's/Datos', 5, 0, 5, 1, '2019-03-25 17:18:49'),
        (4, 397, 1, '13 - Sanyo', 'Control de Calidad', 's/Datos', -80, 0, 5, 1, '2019-03-25 17:33:11'),
        (5, 397, 1, '02- Sanyo Ultralow', 'Diagnóstico', 's/Datos', -80, 0, 5, 1, '2019-03-25 19:04:29'),
        (6, 397, 1, '03 - Sfera Line', 'Diagnostico', 's/Datos', -25, 0, 5, 1, '2019-03-25 19:04:38'),
        (7, 397, 1, '04 - Siam', 'Diagóstico ', 's/Datos', 5, 0, 5, 1, '2019-03-25 19:04:20'),
        (8, 397, 1, '05 - Silken', 'Diagnóstico', 's/Datos', 5, -15, 5, 1, '2019-03-25 19:52:21'),
        (9, 397, 1, '14 - Righi', 'Diagnóstico', 's/Datos', -60, 0, 5, 1, '2019-03-26 17:06:16');


/***********************************************************************************************/
/*                                                                                             */
/*                                     Temperaturas                                            */
/*                                                                                             */
/***********************************************************************************************/

/*

    Tabla con la información de las temperaturas de las heladeras
    id int(6) clave del registro
    heladera int(2) clave de la heladera
    fecha date fecha y hora del registro
    temperatura int(3) temperatura medida con signo
    temperatura2 int(3) segunda temperatura
    controlado int(4) clave del usuario

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS temperaturas;

-- la recreamos
CREATE TABLE temperaturas(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    heladera int(2) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    controlado int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY heladera(heladera),
    KEY controlado(controlado)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de temperaturas';

-- eliminamos la tabla de auditorías si existe
DROP TABLE IF EXISTS auditoria_temperaturas;

-- la recreamos
CREATE TABLE auditoria_temperaturas(
    id int(6) UNSIGNED NOT NULL,
    heladera int(2) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    controlado int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY heladera(heladera),
    KEY controlado(controlado)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditorìa de la tabla de temperaturas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_temperaturas;

-- lo recreamos
CREATE TRIGGER edicion_temperaturas
AFTER UPDATE ON temperaturas
FOR EACH ROW
INSERT INTO auditoria_temperaturas
       (id,
        heladera,
        fecha,
        temperatura,
        temperatura2,
        controlado,
        evento)
       VALUES
       (OLD.id,
        OLD.heladera,
        OLD.fecha,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.controlado,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_temperaturas;

-- lo recreamos
CREATE TRIGGER eliminacion_temperaturas
AFTER DELETE ON temperaturas
FOR EACH ROW
INSERT INTO auditoria_temperaturas
       (id,
        heladera,
        fecha,
        temperatura,
        temperatura2,
        controlado,
        evento)
       VALUES
       (OLD.id,
        OLD.heladera,
        OLD.fecha,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.controlado,
        "Eliminacion");

-- insertamos los registros
INSERT INTO `temperaturas` (`id`, `heladera`, `fecha`, `temperatura`, `temperatura2`, `controlado`) VALUES
(1, 1, '2019-01-31 18:29:00', '-18.0', '0.0', 1),
(2, 1, '2019-02-01 18:30:00', '-15.9', '0.0', 1),
(3, 1, '2019-02-04 18:30:00', '-7.5', '0.0', 1),
(4, 1, '2019-02-05 18:30:00', '-17.4', '0.0', 1),
(5, 1, '2019-02-06 18:31:00', '-19.3', '0.0', 1),
(6, 1, '2019-02-07 18:31:00', '-9.1', '0.0', 1),
(7, 1, '2019-02-08 18:31:00', '-8.6', '0.0', 1),
(8, 1, '2019-02-01 18:32:00', '-3.8', '0.0', 1),
(9, 1, '2019-02-12 18:32:00', '-4.7', '0.0', 1),
(10, 1, '2019-02-13 18:32:00', '-5.3', '0.0', 1),
(11, 1, '2019-02-14 18:32:00', '-14.4', '0.0', 1),
(12, 1, '2019-02-15 18:33:00', '-18.6', '0.0', 1),
(13, 1, '2019-02-18 18:33:00', '-7.7', '0.0', 1),
(14, 1, '2019-02-19 18:33:00', '-18.6', '0.0', 1),
(15, 1, '2019-02-20 18:34:00', '-16.6', '0.0', 1),
(16, 1, '2019-02-21 18:36:00', '-9.0', '0.0', 1),
(17, 1, '2019-02-25 18:37:00', '-6.1', '0.0', 1),
(18, 1, '2019-02-26 18:37:00', '-8.8', '0.0', 1),
(19, 1, '2019-02-27 18:37:00', '-15.0', '0.0', 1),
(20, 1, '2019-02-28 18:38:00', '-9.0', '0.0', 1),
(21, 1, '2019-03-01 18:39:00', '-9.5', '0.0', 1),
(22, 1, '2019-03-06 18:39:00', '-10.3', '0.0', 1),
(23, 1, '2019-03-07 18:39:00', '-10.8', '0.0', 1),
(24, 1, '2019-03-08 18:39:00', '-10.7', '0.0', 1),
(25, 1, '2019-03-11 18:40:00', '-9.8', '0.0', 1),
(26, 1, '2019-03-12 18:40:00', '-10.2', '0.0', 1),
(27, 1, '2019-03-13 18:41:00', '-9.0', '0.0', 1),
(28, 1, '2019-03-14 18:41:00', '-8.7', '0.0', 1),
(29, 1, '2019-03-15 18:41:00', '-9.1', '0.0', 1),
(30, 1, '2019-03-18 18:41:00', '-10.0', '0.0', 1),
(31, 1, '2019-03-19 18:42:00', '-9.2', '0.0', 1),
(32, 1, '2019-03-20 18:42:00', '-5.9', '0.0', 1),
(33, 1, '2019-03-20 18:44:00', '-6.2', '0.0', 1),
(34, 1, '2019-03-22 18:44:00', '-7.1', '0.0', 1),
(35, 1, '2019-01-04 18:44:00', '-20.1', '0.0', 1),
(36, 1, '2019-01-07 18:45:00', '-11.0', '0.0', 1),
(37, 1, '2019-01-08 18:46:00', '-15.2', '0.0', 1),
(38, 1, '2019-01-09 18:46:00', '-8.6', '0.0', 1),
(39, 1, '2019-01-10 18:47:00', '-10.1', '0.0', 1),
(40, 1, '2019-01-11 18:47:00', '-7.5', '0.0', 1),
(41, 1, '2019-01-14 18:47:00', '-10.5', '0.0', 1),
(42, 1, '2019-01-15 18:47:00', '-11.0', '0.0', 1),
(43, 1, '2019-01-16 18:48:00', '-10.0', '0.0', 1),
(44, 1, '2019-01-17 18:48:00', '-8.9', '0.0', 1),
(45, 1, '2019-01-18 18:48:00', '-8.3', '0.0', 1),
(46, 1, '2019-01-21 18:48:00', '-8.1', '0.0', 1),
(47, 1, '2019-01-22 18:49:00', '-6.3', '0.0', 1),
(48, 1, '2019-01-23 18:49:00', '-11.3', '0.0', 1),
(49, 1, '2019-01-24 18:49:00', '-14.0', '0.0', 1),
(50, 1, '2019-01-25 18:50:00', '-11.9', '0.0', 1),
(51, 1, '2019-01-28 18:50:00', '-9.4', '0.0', 1),
(52, 1, '2019-01-29 18:50:00', '-14.6', '0.0', 1),
(53, 1, '2019-01-30 18:51:00', '-13.9', '0.0', 1),
(54, 2, '2019-01-31 20:13:00', '-24.2', '0.0', 1),
(55, 2, '2019-02-01 20:13:00', '-26.1', '0.0', 1),
(56, 2, '2019-02-04 20:13:00', '-23.7', '0.0', 1),
(57, 2, '2019-02-05 20:14:00', '-25.2', '0.0', 1),
(58, 2, '2019-02-06 20:14:00', '-26.8', '0.0', 1),
(59, 2, '2019-02-07 20:14:00', '-24.5', '0.0', 1),
(60, 2, '2019-02-08 20:14:00', '-24.1', '0.0', 1),
(61, 2, '2019-02-11 20:14:00', '-26.1', '0.0', 1),
(62, 2, '2019-02-12 20:15:00', '-25.8', '0.0', 1),
(63, 2, '2019-02-13 20:15:00', '-23.7', '0.0', 1),
(64, 2, '2019-02-14 20:15:00', '-25.2', '0.0', 1),
(65, 2, '2019-02-15 20:15:00', '-23.8', '0.0', 1),
(66, 2, '2019-02-18 20:16:00', '-26.6', '0.0', 1),
(67, 2, '2019-02-19 20:16:00', '-26.3', '0.0', 1),
(68, 2, '2019-02-20 20:16:00', '-25.1', '0.0', 1),
(69, 2, '2019-02-21 20:16:00', '-25.2', '0.0', 1),
(70, 2, '2019-02-22 20:17:00', '-26.2', '0.0', 1),
(71, 2, '2019-02-25 20:17:00', '-25.3', '0.0', 1),
(72, 2, '2019-02-26 20:17:00', '-26.6', '0.0', 1),
(73, 2, '2019-02-27 20:17:00', '-24.5', '0.0', 1),
(74, 2, '2019-02-28 20:18:00', '-26.2', '0.0', 1),
(75, 2, '2019-03-01 20:18:00', '-25.3', '0.0', 1),
(76, 2, '2019-03-06 20:18:00', '-23.8', '0.0', 1),
(77, 2, '2019-03-07 20:21:00', '-26.6', '0.0', 1),
(78, 2, '2019-03-08 20:21:00', '-24.7', '0.0', 1),
(79, 2, '2019-03-11 20:21:00', '-24.5', '0.0', 1),
(80, 2, '2019-03-12 20:22:00', '-25.0', '0.0', 1),
(81, 2, '2019-03-13 20:22:00', '-23.8', '0.0', 1),
(82, 2, '2019-03-14 20:22:00', '-23.8', '0.0', 1),
(83, 2, '2019-03-15 20:22:00', '-25.5', '0.0', 1),
(84, 2, '2019-03-18 20:22:00', '-24.0', '0.0', 1),
(85, 2, '2019-03-19 20:23:00', '-25.6', '0.0', 1),
(86, 2, '2019-03-20 20:23:00', '-26.5', '0.0', 1),
(87, 2, '2019-03-21 20:23:00', '-24.9', '0.0', 1),
(88, 2, '2019-03-22 20:23:00', '-24.0', '0.0', 1),
(89, 2, '2019-01-03 20:23:00', '-26.0', '0.0', 1),
(90, 2, '2019-01-04 20:24:00', '-24.5', '0.0', 1),
(91, 2, '2019-01-07 20:24:00', '-26.0', '0.0', 1),
(92, 2, '2019-01-08 20:24:00', '-25.5', '0.0', 1),
(93, 2, '2019-01-09 20:25:00', '-26.0', '0.0', 1),
(94, 2, '2019-01-10 20:25:00', '-26.0', '0.0', 1),
(95, 2, '2019-01-11 20:25:00', '-25.0', '0.0', 1),
(96, 2, '2019-01-14 20:25:00', '-25.1', '0.0', 1),
(97, 2, '2019-01-15 20:25:00', '-26.5', '0.0', 1),
(98, 2, '2019-01-16 20:25:00', '-26.0', '0.0', 1),
(99, 2, '2019-01-17 20:26:00', '-25.8', '0.0', 1),
(100, 2, '2019-01-18 20:26:00', '-24.5', '0.0', 1),
(101, 2, '2019-01-21 20:26:00', '-24.1', '0.0', 1),
(102, 2, '2019-01-22 20:26:00', '-26.5', '0.0', 1),
(103, 2, '2019-01-23 20:26:00', '-26.0', '0.0', 1),
(104, 2, '2019-01-24 20:27:00', '-26.6', '0.0', 1),
(105, 2, '2019-01-25 20:27:00', '-26.6', '0.0', 1),
(106, 2, '2019-01-28 20:27:00', '-26.3', '0.0', 1),
(107, 2, '2019-01-29 20:27:00', '-26.1', '0.0', 1),
(108, 2, '2019-01-30 20:28:00', '-24.5', '0.0', 1),
(109, 3, '2019-02-06 17:18:00', '5.5', '0.0', 1),
(110, 3, '2019-03-25 17:22:00', '4.5', '0.0', 1),
(111, 3, '2019-03-12 17:23:00', '5.5', '0.0', 1),
(112, 3, '2019-03-13 17:23:00', '6.3', '0.0', 1),
(113, 3, '2019-03-14 17:23:00', '4.4', '0.0', 1),
(114, 3, '2019-03-15 17:23:00', '4.2', '0.0', 1),
(115, 3, '2019-03-18 17:23:00', '4.0', '0.0', 1),
(116, 3, '2019-03-19 17:24:00', '4.2', '0.0', 1),
(117, 3, '2019-03-20 17:24:00', '6.2', '0.0', 1),
(118, 3, '2019-03-21 17:24:00', '5.8', '0.0', 1),
(119, 3, '2019-03-22 17:24:00', '4.7', '0.0', 1),
(120, 3, '2019-03-25 17:24:00', '4.2', '0.0', 1),
(121, 3, '2019-01-03 17:25:00', '5.8', '0.0', 1),
(122, 3, '2019-02-01 17:25:00', '6.5', '0.0', 1),
(123, 3, '2019-02-04 17:25:00', '6.2', '0.0', 1),
(124, 3, '2019-02-05 17:26:00', '6.0', '0.0', 1),
(125, 3, '2019-02-07 17:27:00', '5.9', '0.0', 1),
(126, 3, '2019-02-08 17:28:00', '5.2', '0.0', 1),
(127, 3, '2019-02-11 17:28:00', '6.0', '0.0', 1),
(128, 3, '2019-02-12 17:28:00', '6.6', '0.0', 1),
(129, 3, '2019-02-13 17:29:00', '6.1', '0.0', 1),
(130, 3, '2019-02-14 17:29:00', '7.0', '0.0', 1),
(131, 3, '2019-02-15 17:29:00', '7.5', '0.0', 1),
(132, 3, '2019-02-18 17:29:00', '3.2', '0.0', 1),
(133, 3, '2019-02-19 17:30:00', '2.4', '0.0', 1),
(134, 3, '2019-02-20 17:30:00', '3.0', '0.0', 1),
(135, 3, '2019-02-21 17:30:00', '3.6', '0.0', 1),
(136, 3, '2019-02-22 17:30:00', '3.2', '0.0', 1),
(137, 3, '2019-02-25 17:31:00', '3.6', '0.0', 1),
(138, 3, '2019-02-26 17:31:00', '5.8', '0.0', 1),
(139, 3, '2019-02-27 17:31:00', '6.0', '0.0', 1),
(140, 3, '2019-03-01 17:31:00', '6.8', '0.0', 1),
(141, 4, '2019-01-31 17:33:00', '-78.0', '0.0', 1),
(142, 4, '2019-02-01 17:35:00', '-78.0', '0.0', 1),
(143, 4, '2019-02-04 17:35:00', '-79.0', '0.0', 1),
(144, 4, '2019-02-05 17:36:00', '-79.0', '0.0', 1),
(145, 4, '2019-02-06 17:36:00', '-79.0', '0.0', 1),
(146, 4, '2019-02-07 17:36:00', '-79.0', '0.0', 1),
(147, 4, '2019-02-08 17:36:00', '-80.0', '0.0', 1),
(148, 4, '2019-02-11 17:37:00', '-80.0', '0.0', 1),
(149, 4, '2019-02-12 17:37:00', '-80.0', '0.0', 1),
(150, 4, '2019-02-13 17:37:00', '-78.0', '0.0', 1),
(151, 4, '2019-02-14 17:37:00', '-80.0', '0.0', 1),
(152, 4, '2019-02-15 17:37:00', '-80.0', '0.0', 1),
(153, 4, '2019-02-18 17:38:00', '-80.0', '0.0', 1),
(154, 4, '2019-02-19 17:38:00', '-80.0', '0.0', 1),
(155, 4, '2019-02-20 17:38:00', '-79.0', '0.0', 1),
(156, 4, '2019-02-21 17:38:00', '-78.0', '0.0', 1),
(157, 4, '2019-02-22 17:39:00', '-78.0', '0.0', 1),
(158, 4, '2019-02-25 17:39:00', '-78.0', '0.0', 1),
(159, 4, '2019-02-26 17:39:00', '-78.0', '0.0', 1),
(160, 4, '2019-02-27 17:39:00', '-79.0', '0.0', 1),
(161, 4, '2019-02-28 17:40:00', '-79.0', '0.0', 1),
(162, 4, '2019-03-01 17:40:00', '-78.0', '0.0', 1),
(163, 4, '2019-03-06 17:40:00', '-80.0', '0.0', 1),
(164, 4, '2019-03-07 17:40:00', '-79.0', '0.0', 1),
(165, 4, '2019-03-08 17:41:00', '-79.0', '0.0', 1),
(166, 4, '2019-03-11 17:41:00', '-79.0', '0.0', 1),
(167, 4, '2019-03-12 17:41:00', '-80.0', '0.0', 1),
(168, 4, '2019-03-13 17:41:00', '-80.0', '0.0', 1),
(169, 4, '2019-03-14 17:41:00', '-80.0', '0.0', 1),
(170, 4, '2019-03-15 17:42:00', '-79.0', '0.0', 1),
(171, 4, '2019-03-18 17:42:00', '-79.0', '0.0', 1),
(172, 4, '2019-03-19 17:42:00', '-79.0', '0.0', 1),
(173, 4, '2019-03-21 17:43:00', '-80.0', '0.0', 1),
(174, 4, '2019-03-20 17:43:00', '-79.0', '0.0', 1),
(175, 4, '2019-03-22 17:43:00', '-79.0', '0.0', 1),
(176, 4, '2019-03-25 17:43:00', '-80.0', '0.0', 1),
(177, 4, '2019-01-02 17:44:00', '-79.0', '0.0', 1),
(178, 4, '2019-01-03 17:44:00', '-80.0', '0.0', 1),
(179, 4, '2019-01-04 17:45:00', '-79.0', '0.0', 1),
(180, 4, '2019-01-07 17:45:00', '-78.0', '0.0', 1),
(181, 4, '2019-01-08 17:45:00', '-80.0', '0.0', 1),
(182, 4, '2019-01-09 17:45:00', '-80.0', '0.0', 1),
(183, 4, '2019-01-10 17:45:00', '-78.0', '0.0', 1),
(184, 4, '2019-01-11 17:46:00', '-79.0', '0.0', 1),
(185, 4, '2019-01-14 17:46:00', '-80.0', '0.0', 1),
(186, 4, '2019-01-15 17:46:00', '-79.0', '0.0', 1),
(187, 4, '2019-01-16 17:46:00', '-79.0', '0.0', 1),
(188, 4, '2019-01-17 17:47:00', '-78.0', '0.0', 1),
(189, 4, '2019-01-18 17:47:00', '-80.0', '0.0', 1),
(190, 4, '2019-01-21 17:47:00', '-79.0', '0.0', 1),
(191, 4, '2019-01-22 17:47:00', '-80.0', '0.0', 1),
(192, 4, '2019-01-23 17:48:00', '-78.0', '0.0', 1),
(193, 4, '2019-01-24 17:48:00', '-80.0', '0.0', 1),
(194, 4, '2019-01-25 17:48:00', '-79.0', '0.0', 1),
(195, 4, '2019-01-28 17:48:00', '-80.0', '0.0', 1),
(196, 4, '2019-01-29 17:49:00', '-79.0', '0.0', 1),
(197, 4, '2019-01-30 17:49:00', '-78.0', '0.0', 1),
(198, 5, '2019-03-22 18:21:00', '-60.0', '0.0', 1),
(199, 5, '2019-03-25 18:21:00', '-60.0', '0.0', 1),
(200, 5, '2019-02-07 18:21:00', '-83.0', '0.0', 1),
(201, 5, '2019-02-08 18:25:00', '-84.0', '0.0', 1),
(202, 5, '2019-02-11 18:25:00', '-83.0', '0.0', 1),
(203, 5, '2019-02-12 18:25:00', '-84.0', '0.0', 1),
(204, 5, '2019-02-13 18:25:00', '-84.0', '0.0', 1),
(205, 5, '2019-02-14 18:26:00', '-85.0', '0.0', 1),
(206, 5, '2019-02-15 18:26:00', '-84.0', '0.0', 1),
(207, 5, '2019-02-18 18:26:00', '-85.0', '0.0', 1),
(208, 5, '2019-02-19 18:26:00', '-84.0', '0.0', 1),
(209, 5, '2019-02-20 18:27:00', '-84.0', '0.0', 1),
(210, 5, '2019-02-21 18:27:00', '-84.0', '0.0', 1),
(211, 5, '2019-02-22 18:27:00', '-83.0', '0.0', 1),
(212, 5, '2019-02-25 18:27:00', '-83.0', '0.0', 1),
(213, 5, '2019-02-26 18:28:00', '-84.0', '0.0', 1),
(214, 5, '2019-02-27 18:28:00', '-84.0', '0.0', 1),
(215, 5, '2019-02-28 18:28:00', '-84.0', '0.0', 1),
(216, 5, '2019-03-01 18:28:00', '-84.0', '0.0', 1),
(217, 5, '2019-02-06 18:28:00', '-84.0', '0.0', 1),
(218, 5, '2019-02-07 18:29:00', '-84.0', '0.0', 1),
(219, 5, '2019-02-08 18:29:00', '-83.0', '0.0', 1),
(220, 5, '2019-03-11 18:29:00', '-84.0', '0.0', 1),
(221, 5, '2019-02-12 18:29:00', '-83.0', '0.0', 1),
(222, 5, '2019-03-13 18:29:00', '-83.0', '0.0', 1),
(223, 5, '2019-02-14 18:30:00', '-83.0', '0.0', 1),
(224, 5, '2019-03-15 18:30:00', '-59.0', '0.0', 1),
(225, 5, '2019-03-18 18:30:00', '-84.0', '0.0', 1),
(226, 5, '2019-03-19 18:30:00', '-84.0', '0.0', 1),
(227, 5, '2019-03-20 18:31:00', '-60.0', '0.0', 1),
(228, 5, '2019-03-21 18:31:00', '-59.0', '0.0', 1),
(229, 5, '2019-01-02 18:31:00', '-81.0', '0.0', 1),
(230, 5, '2019-01-03 18:33:00', '-84.0', '0.0', 1),
(231, 5, '2019-01-04 18:33:00', '-80.0', '0.0', 1),
(232, 5, '2019-01-07 18:33:00', '-84.0', '0.0', 1),
(233, 5, '2019-01-08 18:33:00', '-83.0', '0.0', 1),
(234, 5, '2019-01-09 18:34:00', '-82.0', '0.0', 1),
(235, 5, '2019-01-10 18:34:00', '-85.0', '0.0', 1),
(236, 5, '2019-01-14 18:34:00', '-84.0', '0.0', 1),
(237, 5, '2019-01-15 18:34:00', '-85.0', '0.0', 1),
(238, 5, '2019-01-17 18:35:00', '-84.0', '0.0', 1),
(239, 5, '2019-01-18 18:35:00', '-84.0', '0.0', 1),
(240, 5, '2019-01-21 18:35:00', '-85.0', '0.0', 1),
(241, 5, '2019-01-22 18:35:00', '-82.0', '0.0', 1),
(242, 5, '2019-01-23 18:35:00', '-84.0', '0.0', 1),
(243, 5, '2019-01-24 18:36:00', '-85.0', '0.0', 1),
(244, 5, '2019-01-25 18:36:00', '-84.0', '0.0', 1),
(245, 5, '2019-01-28 18:37:00', '-84.0', '0.0', 1),
(246, 5, '2019-01-29 18:37:00', '-85.0', '0.0', 1),
(247, 5, '2019-01-30 18:37:00', '-85.0', '0.0', 1),
(248, 5, '2019-01-31 18:38:00', '-85.0', '0.0', 1),
(249, 5, '2019-02-01 18:38:00', '-83.0', '0.0', 1),
(250, 5, '2019-02-04 18:38:00', '-84.0', '0.0', 1),
(251, 5, '2019-02-05 18:38:00', '-82.0', '0.0', 1),
(252, 5, '2019-02-06 18:39:00', '-84.0', '0.0', 1),
(253, 6, '2019-02-25 18:43:00', '-21.4', '0.0', 1),
(254, 6, '2019-02-26 18:44:00', '-21.4', '0.0', 1),
(255, 6, '2019-02-27 18:44:00', '-21.6', '0.0', 1),
(256, 6, '2019-03-28 18:44:00', '-22.4', '0.0', 1),
(257, 6, '2019-03-01 18:45:00', '-21.4', '0.0', 1),
(258, 6, '2019-03-06 18:45:00', '-23.5', '0.0', 1),
(259, 6, '2019-03-07 18:45:00', '-20.3', '0.0', 1),
(260, 6, '2019-03-08 18:45:00', '-22.6', '0.0', 1),
(261, 6, '2019-03-08 18:45:00', '-24.0', '0.0', 1),
(262, 6, '2019-03-11 18:46:00', '-23.1', '0.0', 1),
(263, 6, '2019-03-12 18:46:00', '-20.2', '0.0', 1),
(264, 6, '2019-03-13 18:46:00', '-21.1', '0.0', 1),
(265, 6, '2019-03-14 18:46:00', '-21.2', '0.0', 1),
(266, 6, '2019-03-15 18:46:00', '-21.9', '0.0', 1),
(267, 6, '2019-03-18 18:47:00', '-25.2', '0.0', 1),
(268, 6, '2019-03-19 18:47:00', '-25.0', '0.0', 1),
(269, 6, '2019-03-19 18:47:00', '-24.0', '0.0', 1),
(270, 6, '2019-03-20 18:48:00', '-25.4', '0.0', 1),
(271, 6, '2019-03-21 18:48:00', '-23.3', '0.0', 1),
(272, 6, '2019-03-21 18:48:00', '-25.0', '0.0', 1),
(273, 6, '2019-03-22 18:48:00', '-23.7', '0.0', 1),
(274, 6, '2019-03-25 18:48:00', '-22.1', '0.0', 1),
(275, 6, '2019-01-17 18:49:00', '-22.1', '0.0', 1),
(276, 6, '2019-01-18 18:50:00', '-6.3', '0.0', 1),
(277, 6, '2019-01-21 18:50:00', '-23.3', '0.0', 1),
(278, 6, '2019-01-21 18:50:00', '-23.0', '0.0', 1),
(279, 6, '2019-01-22 18:51:00', '-26.4', '0.0', 1),
(280, 6, '2019-01-23 18:51:00', '-26.0', '0.0', 1),
(281, 6, '2019-01-24 18:51:00', '-25.9', '0.0', 1),
(282, 6, '2019-01-25 18:51:00', '-23.5', '0.0', 1),
(283, 6, '2019-01-28 18:52:00', '-22.3', '0.0', 1),
(284, 6, '2019-01-28 18:52:00', '-22.6', '0.0', 1),
(285, 6, '2019-01-29 18:52:00', '-27.4', '0.0', 1),
(286, 6, '2019-01-30 18:52:00', '-22.8', '0.0', 1),
(287, 6, '2019-01-30 18:53:00', '-21.0', '0.0', 1),
(288, 6, '2019-01-31 18:53:00', '-26.9', '0.0', 1),
(289, 6, '2019-02-01 18:53:00', '-26.9', '0.0', 1),
(290, 6, '2019-02-01 18:53:00', '-24.6', '0.0', 1),
(291, 6, '2019-02-04 18:54:00', '-23.4', '0.0', 1),
(292, 6, '2019-02-05 18:54:00', '-24.7', '0.0', 1),
(293, 6, '2019-02-05 18:54:00', '-28.0', '0.0', 1),
(294, 6, '2019-02-06 18:54:00', '-23.4', '0.0', 1),
(295, 6, '2019-02-06 18:54:00', '-10.4', '0.0', 1),
(296, 6, '2019-02-07 18:55:00', '-21.4', '0.0', 1),
(297, 6, '2019-02-08 18:55:00', '-23.7', '0.0', 1),
(298, 6, '2019-02-11 18:55:00', '-25.1', '0.0', 1),
(299, 6, '2019-02-12 18:55:00', '-24.2', '0.0', 1),
(300, 6, '2019-02-13 18:56:00', '-25.1', '0.0', 1),
(301, 6, '2019-02-13 18:56:00', '-26.6', '0.0', 1),
(302, 6, '2019-02-14 18:56:00', '-23.9', '0.0', 1),
(303, 6, '2019-02-15 18:56:00', '-22.8', '0.0', 1),
(304, 6, '2019-02-18 18:57:00', '-24.4', '0.0', 1),
(305, 6, '2019-02-19 18:57:00', '-23.5', '0.0', 1),
(306, 6, '2019-02-20 18:57:00', '-21.2', '0.0', 1),
(307, 6, '2019-02-21 18:58:00', '-21.8', '0.0', 1),
(308, 6, '2019-02-22 18:58:00', '-23.0', '0.0', 1),
(309, 6, '2019-01-02 18:58:00', '-23.9', '0.0', 1),
(310, 6, '2019-01-03 19:00:00', '-23.5', '0.0', 1),
(311, 6, '2019-01-04 19:00:00', '-20.6', '0.0', 1),
(312, 6, '2019-01-07 19:00:00', '-24.3', '0.0', 1),
(313, 6, '2019-01-08 19:00:00', '-22.4', '0.0', 1),
(314, 6, '2019-01-09 19:01:00', '-26.8', '0.0', 1),
(315, 6, '2019-01-10 19:01:00', '-25.8', '0.0', 1),
(316, 6, '2019-01-11 19:01:00', '-24.4', '0.0', 1),
(317, 7, '2019-02-22 19:33:00', '5.6', '0.0', 1),
(318, 7, '2019-02-25 19:34:00', '3.9', '0.0', 1),
(319, 7, '2019-02-26 19:34:00', '3.1', '0.0', 1),
(320, 7, '2019-02-26 19:34:00', '3.1', '0.0', 1),
(321, 7, '2019-02-27 19:34:00', '3.0', '0.0', 1),
(322, 7, '2019-02-28 19:35:00', '3.6', '0.0', 1),
(323, 7, '2019-03-01 19:35:00', '3.2', '0.0', 1),
(324, 7, '2019-03-06 19:35:00', '3.5', '0.0', 1),
(325, 7, '2019-03-07 19:35:00', '3.9', '0.0', 1),
(326, 7, '2019-03-08 19:35:00', '3.9', '0.0', 1),
(327, 7, '2019-03-08 19:36:00', '3.4', '0.0', 1),
(328, 7, '2019-03-11 19:36:00', '5.1', '0.0', 1),
(329, 7, '2019-03-12 19:36:00', '3.6', '0.0', 1),
(330, 7, '2019-03-13 19:36:00', '3.6', '0.0', 1),
(331, 7, '2019-03-14 19:36:00', '3.6', '0.0', 1),
(332, 7, '2019-03-15 19:36:00', '3.4', '0.0', 1),
(333, 7, '2019-03-19 19:37:00', '6.9', '0.0', 1),
(334, 7, '2019-03-19 19:37:00', '6.3', '0.0', 1),
(335, 7, '2019-03-20 19:37:00', '-7.3', '0.0', 1),
(336, 7, '2019-03-21 19:37:00', '-8.3', '0.0', 1),
(337, 7, '2019-03-21 19:38:00', '-7.9', '0.0', 1),
(338, 7, '2019-03-22 19:38:00', '-7.9', '0.0', 1),
(339, 7, '2019-03-25 19:38:00', '-6.4', '0.0', 1),
(340, 7, '2019-01-17 19:39:00', '4.1', '0.0', 1),
(341, 7, '2019-01-18 19:40:00', '3.8', '0.0', 1),
(342, 7, '2019-01-18 19:40:00', '5.7', '0.0', 1),
(343, 7, '2019-01-21 19:40:00', '9.3', '0.0', 1),
(344, 7, '2019-01-22 19:40:00', '6.9', '0.0', 1),
(345, 7, '2019-01-23 19:40:00', '2.3', '0.0', 1),
(346, 7, '2019-01-24 19:41:00', '2.9', '0.0', 1),
(347, 7, '2019-01-25 19:41:00', '2.1', '0.0', 1),
(348, 7, '2019-01-28 19:41:00', '2.9', '0.0', 1),
(349, 7, '2019-01-28 19:41:00', '7.9', '0.0', 1),
(350, 7, '2019-01-29 19:42:00', '1.2', '0.0', 1),
(351, 7, '2019-01-30 19:42:00', '3.6', '0.0', 1),
(352, 7, '2019-01-30 19:42:00', '3.1', '0.0', 1),
(353, 7, '2019-01-31 19:42:00', '2.0', '0.0', 1),
(354, 7, '2019-02-01 19:42:00', '2.4', '0.0', 1),
(355, 7, '2019-02-01 19:43:00', '1.6', '0.0', 1),
(356, 7, '2019-02-04 19:43:00', '3.8', '0.0', 1),
(357, 7, '2019-02-05 19:43:00', '1.0', '0.0', 1),
(358, 7, '2019-02-05 19:43:00', '2.1', '0.0', 1),
(359, 7, '2019-02-06 19:44:00', '4.3', '0.0', 1),
(360, 7, '2019-02-06 19:44:00', '4.3', '0.0', 1),
(361, 7, '2019-02-07 19:44:00', '5.5', '0.0', 1),
(362, 7, '2019-02-08 19:44:00', '3.1', '0.0', 1),
(363, 7, '2019-02-11 19:44:00', '4.1', '0.0', 1),
(364, 7, '2019-02-12 19:45:00', '3.1', '0.0', 1),
(365, 7, '2019-02-13 19:45:00', '3.4', '0.0', 1),
(366, 7, '2019-02-13 19:45:00', '2.8', '0.0', 1),
(367, 7, '2019-02-14 19:46:00', '2.0', '0.0', 1),
(368, 7, '2019-02-15 19:46:00', '3.5', '0.0', 1),
(369, 7, '2019-02-18 19:46:00', '3.2', '0.0', 1),
(370, 7, '2019-02-19 19:46:00', '3.1', '0.0', 1),
(371, 7, '2019-02-20 19:46:00', '4.1', '0.0', 1),
(372, 7, '2019-02-21 19:47:00', '4.6', '0.0', 1),
(373, 7, '2019-01-02 19:47:00', '6.1', '0.0', 1),
(374, 7, '2019-01-03 19:47:00', '4.7', '0.0', 1),
(375, 7, '2019-01-04 19:48:00', '5.6', '0.0', 1),
(376, 7, '2019-01-07 19:48:00', '5.0', '0.0', 1),
(377, 7, '2019-01-08 19:48:00', '6.2', '0.0', 1),
(378, 7, '2019-01-09 19:48:00', '3.2', '0.0', 1),
(379, 7, '2019-01-10 19:48:00', '3.1', '0.0', 1),
(380, 7, '2019-02-11 19:49:00', '4.1', '0.0', 1),
(381, 8, '2019-03-22 20:09:00', '2.9', '-13.0', 1),
(382, 8, '2019-03-25 20:10:00', '-2.3', '-17.0', 1),
(383, 8, '2019-02-08 20:10:00', '6.3', '-10.0', 1),
(384, 8, '2019-02-12 20:12:00', '7.2', '-10.0', 1),
(385, 8, '2019-02-13 20:12:00', '6.7', '-11.0', 1),
(386, 8, '2019-02-13 20:12:00', '8.5', '-14.0', 1),
(387, 8, '2019-02-14 20:12:00', '7.8', '-5.0', 1),
(388, 8, '2019-02-15 20:13:00', '7.9', '-11.0', 1),
(389, 8, '2019-02-18 20:13:00', '5.7', '-10.0', 1),
(390, 8, '2019-02-19 20:13:00', '5.8', '-10.0', 1),
(391, 8, '2019-02-20 20:14:00', '3.9', '-11.0', 1),
(392, 8, '2019-02-20 20:14:00', '10.5', '-14.0', 1),
(393, 8, '2019-02-21 20:14:00', '6.2', '-8.0', 1),
(394, 8, '2019-02-22 20:15:00', '5.4', '-10.0', 1),
(395, 8, '2019-02-25 20:15:00', '5.3', '-10.0', 1),
(396, 8, '2019-02-26 20:15:00', '7.6', '-8.0', 1),
(397, 8, '2019-02-26 20:16:00', '9.2', '-10.1', 1),
(398, 8, '2019-02-27 20:16:00', '4.5', '-11.0', 1),
(399, 8, '2019-02-28 20:16:00', '5.3', '-11.0', 1),
(400, 8, '2019-03-01 20:17:00', '8.1', '-12.0', 1),
(401, 8, '2019-03-06 20:17:00', '4.0', '-13.0', 1),
(402, 8, '2019-03-07 20:17:00', '8.7', '-12.0', 1),
(403, 8, '2019-03-08 20:17:00', '6.7', '-11.0', 1),
(404, 8, '2019-03-08 20:18:00', '4.1', '-19.0', 1),
(405, 8, '2019-03-11 20:18:00', '3.5', '-13.0', 1),
(406, 8, '2019-03-12 20:18:00', '6.1', '-12.0', 1),
(407, 8, '2019-03-13 20:19:00', '2.5', '-13.0', 1),
(408, 8, '2019-03-14 20:19:00', '3.1', '-19.0', 1),
(409, 8, '2019-03-15 20:19:00', '1.3', '-15.0', 1),
(410, 8, '2019-03-18 20:19:00', '-0.6', '-15.0', 1),
(411, 8, '2019-03-19 20:20:00', '2.3', '-12.0', 1),
(412, 8, '2019-03-19 20:20:00', '3.2', '-10.0', 1),
(413, 8, '2019-03-20 20:20:00', '4.1', '-15.0', 1),
(414, 8, '2019-03-21 20:21:00', '0.8', '-12.0', 1),
(415, 8, '2019-03-22 20:21:00', '5.8', '-10.0', 1),
(416, 8, '2019-01-02 20:21:00', '8.5', '-9.0', 1),
(417, 8, '2019-01-03 20:22:00', '5.2', '-8.0', 1),
(418, 8, '2019-01-04 20:22:00', '0.7', '-10.0', 1),
(419, 8, '2019-01-07 20:22:00', '6.1', '-11.0', 1),
(420, 8, '2019-01-08 20:23:00', '3.6', '-12.0', 1),
(421, 8, '2019-01-09 20:23:00', '7.3', '-5.0', 1),
(422, 8, '2019-01-10 20:23:00', '4.4', '-12.0', 1),
(423, 8, '2019-01-11 20:23:00', '6.5', '-7.0', 1),
(424, 8, '2019-01-14 20:24:00', '7.1', '-15.0', 1),
(425, 8, '2019-01-17 20:24:00', '5.8', '-15.0', 1),
(426, 8, '2019-01-18 20:24:00', '6.4', '-14.0', 1),
(427, 8, '2019-01-18 20:25:00', '5.4', '-15.0', 1),
(428, 8, '2019-01-21 20:25:00', '6.3', '-8.0', 1),
(429, 8, '2019-01-22 20:26:00', '4.5', '-11.0', 1),
(430, 8, '2019-01-23 20:26:00', '4.8', '-11.0', 1),
(431, 8, '2019-01-24 20:26:00', '5.3', '-10.0', 1),
(432, 8, '2019-01-25 20:26:00', '6.0', '-8.0', 1),
(433, 8, '2019-01-28 20:27:00', '6.5', '-9.0', 1),
(434, 8, '2018-12-28 20:27:00', '8.3', '-10.0', 1),
(435, 8, '2019-01-29 20:27:00', '6.3', '-10.0', 1),
(436, 8, '2019-01-30 20:28:00', '6.7', '-15.0', 1),
(437, 8, '2019-01-30 20:28:00', '7.8', '-14.0', 1),
(438, 8, '2019-01-31 20:28:00', '6.4', '-14.0', 1),
(439, 8, '2019-02-01 20:29:00', '7.3', '-6.0', 1),
(440, 8, '2019-02-01 20:29:00', '7.5', '-10.0', 1),
(441, 8, '2019-02-04 20:29:00', '5.5', '-15.0', 1),
(442, 8, '2019-02-05 20:29:00', '8.2', '-7.0', 1),
(443, 8, '2019-02-05 20:30:00', '9.0', '-8.0', 1),
(444, 8, '2019-02-06 20:30:00', '6.9', '-10.0', 1),
(445, 8, '2019-02-06 20:30:00', '8.4', '-16.0', 1),
(446, 8, '2019-02-07 20:30:00', '7.0', '-7.0', 1),
(447, 9, '2019-03-25 17:06:00', '-61.0', '0.0', 1),
(448, 9, '2019-03-26 17:06:00', '-60.0', '0.0', 1),
(449, 9, '2019-02-08 17:07:00', '-61.0', '0.0', 1),
(450, 9, '2019-02-11 17:07:00', '-61.0', '0.0', 1),
(451, 9, '2019-02-12 17:07:00', '-59.0', '0.0', 1),
(452, 9, '2019-02-13 17:07:00', '-61.0', '0.0', 1),
(453, 9, '2019-02-13 17:07:00', '-60.0', '0.0', 1),
(454, 9, '2019-02-14 17:08:00', '-63.0', '0.0', 1),
(455, 9, '2019-02-15 17:08:00', '-55.0', '0.0', 1),
(456, 9, '2019-02-18 17:08:00', '-61.0', '0.0', 1),
(457, 9, '2019-02-19 17:08:00', '-60.0', '0.0', 1),
(458, 9, '2019-02-20 17:08:00', '-62.0', '0.0', 1),
(459, 9, '2019-02-20 17:08:00', '-62.0', '0.0', 1),
(460, 9, '2019-02-21 17:09:00', '-62.0', '0.0', 1),
(461, 9, '2019-02-22 17:09:00', '-62.0', '0.0', 1),
(462, 9, '2019-02-25 17:09:00', '-61.0', '0.0', 1),
(463, 9, '2019-02-26 17:09:00', '-59.0', '0.0', 1),
(464, 9, '2019-02-26 17:09:00', '-61.0', '0.0', 1),
(465, 9, '2019-03-27 17:10:00', '-62.0', '0.0', 1),
(466, 9, '2019-02-28 17:10:00', '-53.0', '0.0', 1),
(467, 9, '2019-03-01 17:10:00', '-61.0', '0.0', 1),
(468, 9, '2019-03-06 17:10:00', '-60.0', '0.0', 1),
(469, 9, '2019-03-07 17:11:00', '-60.0', '0.0', 1),
(470, 9, '2019-03-08 17:11:00', '-62.0', '0.0', 1),
(471, 9, '2019-03-08 17:11:00', '-61.0', '0.0', 1),
(472, 9, '2019-02-11 17:11:00', '-62.0', '0.0', 1),
(473, 9, '2019-03-12 17:11:00', '-61.0', '0.0', 1),
(474, 9, '2019-03-13 17:12:00', '-63.0', '0.0', 1),
(475, 9, '2019-03-14 17:12:00', '-61.0', '0.0', 1),
(476, 9, '2019-03-15 17:12:00', '-61.0', '0.0', 1),
(477, 9, '2019-03-18 17:12:00', '-61.0', '0.0', 1),
(478, 9, '2019-03-19 17:12:00', '-62.0', '0.0', 1),
(479, 9, '2019-03-19 17:12:00', '-60.0', '0.0', 1),
(480, 9, '2019-03-20 17:12:00', '-59.0', '0.0', 1),
(481, 9, '2019-03-21 17:13:00', '-62.0', '0.0', 1),
(482, 9, '2019-03-21 17:13:00', '-59.0', '0.0', 1),
(483, 9, '2019-03-22 17:13:00', '-62.0', '0.0', 1),
(484, 9, '2019-01-02 17:13:00', '-61.0', '0.0', 1),
(485, 9, '2019-01-03 17:16:00', '-61.0', '0.0', 1),
(486, 9, '2019-01-04 17:16:00', '-60.0', '0.0', 1),
(487, 9, '2019-01-07 17:16:00', '-61.0', '0.0', 1),
(488, 9, '2019-02-08 17:16:00', '-59.0', '0.0', 1),
(489, 9, '2019-01-09 17:16:00', '-62.0', '0.0', 1),
(490, 9, '2019-01-10 17:17:00', '-63.0', '0.0', 1),
(491, 9, '2019-01-11 17:17:00', '-63.0', '0.0', 1),
(492, 9, '2019-01-14 17:17:00', '-61.0', '0.0', 1),
(493, 9, '2019-01-17 17:17:00', '-60.0', '0.0', 1),
(494, 9, '2019-01-18 17:17:00', '-62.0', '0.0', 1),
(495, 9, '2019-01-21 17:18:00', '-62.0', '0.0', 1),
(496, 9, '2019-01-21 17:18:00', '-61.0', '0.0', 1),
(497, 9, '2019-01-22 17:18:00', '-60.0', '0.0', 1),
(498, 9, '2019-01-23 17:18:00', '-60.0', '0.0', 1),
(499, 9, '2019-01-24 17:18:00', '-60.0', '0.0', 1),
(500, 9, '2019-01-25 17:19:00', '-59.0', '0.0', 1),
(501, 9, '2019-01-28 17:19:00', '-63.0', '0.0', 1),
(502, 9, '2019-01-28 17:19:00', '-63.0', '0.0', 1),
(503, 9, '2019-01-29 17:19:00', '-63.0', '0.0', 1),
(504, 9, '2019-01-30 17:19:00', '-61.0', '0.0', 1),
(505, 9, '2019-01-30 17:20:00', '-62.0', '0.0', 1),
(506, 9, '2019-01-31 17:20:00', '-61.0', '0.0', 1),
(507, 9, '2019-02-01 17:20:00', '-60.0', '0.0', 1),
(508, 9, '2019-02-01 17:20:00', '-62.0', '0.0', 1),
(509, 9, '2019-02-04 17:20:00', '-61.0', '0.0', 1),
(510, 9, '2019-02-05 17:21:00', '-59.0', '0.0', 1),
(511, 9, '2019-02-05 17:21:00', '-61.0', '0.0', 1),
(512, 9, '2019-02-06 17:21:00', '-61.0', '0.0', 1),
(513, 9, '2019-02-06 17:21:00', '-60.0', '0.0', 1),
(514, 9, '2019-02-07 17:22:00', '-61.0', '0.0', 1);


/***************************************************************************/
/*                                                                         */
/*                       Tipos de Derivación                               */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla es el diccionario de derivaciones su edición quedará
    restringida a los usuarios del nivel central, no tiene auditoría
    id entero pequeño, clave del registro
    derivacion texto descripción del motivo
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla de motivos si existe
DROP TABLE IF EXISTS derivacion;

-- la recreamos
CREATE TABLE derivacion (
    id tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    derivacion varchar(100) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tipos de derivación';

-- insertamos los valores iniciales
INSERT INTO derivacion (derivacion, usuario) VALUES ("Institución", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Profesional", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Personal", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Familiar", 1);


/***************************************************************************/
/*                                                                         */
/*                              Chagas Agudo                               */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene los antecedentes de chagas agudo, solo podrá ser
    editada por administradores del nivel central, debería haber un solo
    registro por cada paciente

    id int(6) clave del registro
    paciente int(8) clave del paciente
    sintomas si ha tenido síntomas y signos anteriormente
    observaciones texto, observaciones del usuario
    usuario clave del usuario
    fecha date, fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS agudo;

-- la recreamos
CREATE TABLE agudo (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    sintomas tinyint(1) UNSIGNED DEFAULT 0,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de chagas agudo';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_agudo;

-- la recreamos
CREATE TABLE auditoria_agudo (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    sintomas tinyint(1) UNSIGNED DEFAULT 0,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum ("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de chagas agudo';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_agudo;

-- lo recreamos
CREATE TRIGGER edicion_agudo
AFTER UPDATE ON agudo
FOR EACH ROW
INSERT INTO auditoria_agudo
    (id,
     paciente,
     sintomas,
     observaciones,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.paciente,
     OLD.sintomas,
     OLD.observaciones,
     OLD.usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_agudo;

-- lo recreamos
CREATE TRIGGER eliminacion_agudo
AFTER DELETE ON agudo
FOR EACH ROW
INSERT INTO auditoria_agudo
    (id,
     paciente,
     sintomas,
     observaciones,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.paciente,
     OLD.sintomas,
     OLD.observaciones,
     OLD.usuario,
     OLD.fecha,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Drogas                                     */
/*                                                                         */
/***************************************************************************/

/*

    Diccionario de drogas utilizadas para el tratamiento

    id int clave del registro
    droga varchar nombre de la droga
    dosis int dosis en miligramos
    comercial nombre comercial del medicamento
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS drogas;

-- la recreamos
CREATE TABLE drogas (
    id int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    droga varchar(200) DEFAULT NULL,
    dosis int(3) UNSIGNED NOT NULL,
    comercial varchar(200) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de drogas utilizadas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_drogas;

-- la recreamos
CREATE TABLE auditoria_drogas (
    id int(2) UNSIGNED NOT NULL,
    droga varchar(200) DEFAULT NULL,
    dosis int(3) UNSIGNED NOT NULL,
    comercial varchar(200) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de drogas utilizadas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_drogras;

-- lo recreamos
CREATE TRIGGER edicion_drogas
AFTER UPDATE ON drogas
FOR EACH ROW
INSERT INTO auditoria_drogas
       (id,
        droga,
        dosis,
        comercial,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.droga,
        OLD.dosis,
        OLD.comercial,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_drogas;

-- lo recreamos
CREATE TRIGGER eliminacion_drogas
AFTER DELETE ON drogas
FOR EACH ROW
INSERT INTO auditoria_drogas
       (id,
        droga,
        dosis,
        comercial,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.droga,
        OLD.dosis,
        OLD.comercial,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                          Efectos Adversos                               */
/*                                                                         */
/***************************************************************************/

/*

    Diccionario de efectos adversos al tratamiento

    id clave del registro
    descripcion texto descripción del efecto
    usuario entero clave del registro
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS adversos;

-- la recreamos
CREATE TABLE adversos (
    id int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    descripcion varchar(250) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de efectos adversos';


/***************************************************************************/
/*                                                                         */
/*                              Tratamiento                                */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene el esquema de tratamiento de cada paciente, puede
    tener varias entradas por cada paciente

    id clave del registro
    paciente clave del paciente
    droga clave de la droga utilizada
    comprimidos cantidad de comprimidos diarios
    inicio date fecha de inicio del tratamiento
    fin date fecha de finalización del tratamiento
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tratamiento;

-- la recreamos
CREATE TABLE tratamiento (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    droga int(2) UNSIGNED DEFAULT NULL,
    comprimidos tinyint(1) UNSIGNED DEFAULT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY paciente(paciente),
    KEY droga(droga),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del tratamiento';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tratamiento;

-- la recreamos
CREATE TABLE auditoria_tratamiento (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    droga int(2) UNSIGNED DEFAULT NULL,
    comprimidos tinyint(1) UNSIGNED DEFAULT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del tratamiento';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_tratamiento;

-- lo recreamos
CREATE TRIGGER edicion_tratamiento
AFTER UPDATE ON tratamiento
FOR EACH ROW
INSERT INTO auditoria_tratamiento
            (id,
             paciente,
             droga,
             comprimidos,
             inicio,
             fin,
             usuario,
             fecha,
             evento)
            VALUES
            (OLD.id,
             OLD.paciente,
             OLD.droga,
             OLD.comprimidos,
             OLD.inicio,
             OLD.fin,
             OLD.usuario,
             OLD.fecha,
             'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_tratamiento;

-- lo recreamos
CREATE TRIGGER eliminacion_tratamiento
AFTER DELETE ON tratamiento
FOR EACH ROW
INSERT INTO auditoria_tratamiento
            (id,
             paciente,
             droga,
             comprimidos,
             inicio,
             fin,
             usuario,
             fecha,
             evento)
            VALUES
            (OLD.id,
             OLD.paciente,
             OLD.droga,
             OLD.comprimidos,
             OLD.inicio,
             OLD.fin,
             OLD.usuario,
             OLD.fecha,
             'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*                         Adversos  Paciente                              */
/*                                                                         */
/***************************************************************************/

/*

    Tabla pivot de los efectos adversos del tratamiento
    id entero clave del registro
    tratamiento entero clave de la tabla de tratamiento
    adverso entero clave de la tabla de efectos adversos
    fecha date fecha en que se registró el efecto
    usuario entero clave del usuario
    fecha timestamp fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS adversos_paciente;

-- la recreamos
CREATE TABLE adversos_paciente (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    tratamiento int(6) UNSIGNED NOT NULL,
    adverso int(2) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY tratamiento(tratamiento),
    KEY adverso(adverso)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Efectos adversos del tratamiento';


/***************************************************************************/
/*                                                                         */
/*                              Antecedentes                               */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes tóxicos del paciente esta tendría que haber
    un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    fuma entero (0 si no fuma)
    cigarrillos entero número de cigarrillos por día
    exfumador entero (0 falso - 1 verdadero)
    anios entero cantidad de años que fumó
    dejo fecha en que dejó de fumar
    alcohol entero (o si no toma)
    litros entero litros a la semana de bebida
    bebida enum tipo de bebida
    adicciones texto descripción de las adicciones
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS antecedentes;

-- la recreamos
CREATE TABLE antecedentes(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    fuma tinyint(1) UNSIGNED DEFAULT 0,
    cigarrillos int(3) UNSIGNED DEFAULT 0,
    exfumador tinyint(1) UNSIGNED DEFAULT 0,
    anios int(2) UNSIGNED UNSIGNED DEFAULT 0,
    dejo date DEFAULT NULL,
    alcohol tinyint(1) UNSIGNED DEFAULT 0,
    litros int(2) UNSIGNED DEFAULT 0,
    bebida enum('Fermentada', 'Vino', 'Destilada', 'Otra') DEFAULT NULL,
    adicciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Antecedentes del paciente';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_antecedentes;

-- la recreamos
CREATE TABLE auditoria_antecedentes(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    fuma tinyint(1) UNSIGNED DEFAULT 0,
    cigarrillos int(3) UNSIGNED DEFAULT 0,
    exfumador tinyint(1) UNSIGNED DEFAULT 0,
    anios int(2) UNSIGNED UNSIGNED DEFAULT 0,
    dejo date DEFAULT NULL,
    alcohol tinyint(1) UNSIGNED DEFAULT 0,
    litros int(2) UNSIGNED DEFAULT 0,
    bebida enum('Fermentada', 'Vino', 'Destilada', 'Otra') DEFAULT NULL,
    adicciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de antecedentes del paciente';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_antecedentes;

-- lo recreamos
CREATE TRIGGER edicion_antecedentes
AFTER UPDATE ON antecedentes
FOR EACH ROW
INSERT INTO auditoria_antecedentes
       (id,
        paciente,
        fuma,
        cigarrillos,
        exfumador,
        anios,
        dejo,
        alcohol,
        litros,
        bebida,
        adicciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fuma,
        OLD.cigarrillos,
        OLD.exfumador,
        OLD.anios,
        OLD.dejo,
        OLD.alcohol,
        OLD.litros,
        OLD.bebida,
        OLD.adicciones,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_antecedentes;

-- lo recreamos
CREATE TRIGGER eliminacion_antecedentes
AFTER DELETE ON antecedentes
FOR EACH ROW
INSERT INTO auditoria_antecedentes
       (id,
        paciente,
        fuma,
        cigarrillos,
        exfumador,
        anios,
        dejo,
        alcohol,
        litros,
        bebida,
        adicciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fuma,
        OLD.cigarrillos,
        OLD.exfumador,
        OLD.anios,
        OLD.dejo,
        OLD.alcohol,
        OLD.litros,
        OLD.bebida,
        OLD.adicciones,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                                 Síntomas                                */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los síntomas del paciente, esta tabla debería tener una entrada
    por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    disnea enum tipo de disnea
    palpitaciones enum tipo de palpitaciones
    precordial enum si tiene dolor precordial
    conciencia int 0 false 1 verdadero
    presincope int 0 false 1 verdadero
    edema int 0 false 1 verdadero si tiene edema de miembros inferiores
    observaciones texto observaciones del usuario
    usuario entero clave del registro
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS sintomas;

-- la recreamos
CREATE TABLE sintomas (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    disnea enum('Esfuerzos Leves', 'Esfuerzos Moderados', 'Grandes Esfuerzos', 'En Reposo', 'DPN', 'No Tiene') DEFAULT 'No Tiene',
    palpitaciones enum('Raramente', 'Frecuentes', 'No Tiene') DEFAULT 'No Tiene',
    precordial enum('Tipico', 'Atipico', 'No Tiene') DEFAULT 'No Tiene',
    conciencia tinyint(1) UNSIGNED DEFAULT 0,
    presincope tinyint(1) UNSIGNED DEFAULT 0,
    edema tinyint(1) UNSIGNED DEFAULT 0,
    observaciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Síntomas del paciente';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_sintomas;

-- la recreamos
CREATE TABLE auditoria_sintomas (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    disnea enum('Esfuerzos Leves', 'Esfuerzos Moderados', 'Grandes Esfuerzos', 'En Reposo', 'DPN', 'No Tiene') DEFAULT 'No Tiene',
    palpitaciones enum('Raramente', 'Frecuentes', 'No Tiene') DEFAULT 'No Tiene',
    precordial enum('Tipico', 'Atipico', 'No Tiene') DEFAULT 'No Tiene',
    conciencia tinyint(1) UNSIGNED DEFAULT 0,
    presincope tinyint(1) UNSIGNED DEFAULT 0,
    edema enum('Si', 'No') DEFAULT 'No',
    observaciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum('Edicion', 'Eliminacion'),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de síntomas del paciente';

-- eliminamos el trigger
DROP TRIGGER IF EXISTS edicion_sintomas;

-- lo recreamos
CREATE TRIGGER edicion_sintomas
AFTER UPDATE ON sintomas
FOR EACH ROW
INSERT INTO auditoria_sintomas
       (id,
        paciente,
        visita,
        disnea,
        palpitaciones,
        precordial,
        conciencia,
        presincope,
        edema,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.disnea,
        OLD.palpitaciones,
        OLD.precordial,
        OLD.conciencia,
        OLD.presincope,
        OLD.edema,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_sintomas;

-- lo recreamos
CREATE TRIGGER eliminacion_sintomas
AFTER DELETE ON sintomas
FOR EACH ROW
INSERT INTO auditoria_sintomas
       (id,
        paciente,
        visita,
        disnea,
        palpitaciones,
        precordial,
        conciencia,
        presincope,
        edema,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.disnea,
        OLD.palpitaciones,
        OLD.precordial,
        OLD.conciencia,
        OLD.presincope,
        OLD.edema,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*                              disautonomia                               */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de disautonía del paciente, esta también debería
    haber un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    hipotensión tinyint si tiene hipotensión (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    astenia tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS disautonomia;

-- la recreamos
CREATE TABLE disautonomia(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    hipotension tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    astenia tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de disautonía';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_disautonomia;

-- la recreamos
CREATE TABLE auditoria_disautonomia(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    hipotension tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    astenia tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de disautonía';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_disautonomia;

-- lo recreamos
CREATE TRIGGER edicion_disautonomia
AFTER UPDATE ON disautonomia
FOR EACH ROW
INSERT INTO auditoria_disautonomia
       (id,
        paciente,
        hipotension,
        bradicardia,
        astenia,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.hipotension,
        OLD.bradicardia,
        OLD.astenia,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_disautonomia;

-- lo recreamos
CREATE TRIGGER eliminacion_disautonomia
AFTER DELETE ON disautonomia
FOR EACH ROW
INSERT INTO auditoria_disautonomia
       (id,
        paciente,
        hipotension,
        bradicardia,
        astenia,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.hipotension,
        OLD.bradicardia,
        OLD.astenia,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Digestivo                                  */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes del compromiso digestivo, esta tabla tendría
    una entrada por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    disfagia tinyint (0 falso - 1 verdadero)
    pirosis tinyint (0 falso - 1 verdadero)
    regurgitacion tinyint (0 falso - 1 verdadero)
    constipacion tinyint (0 falso - 1 verdadero)
    bolo tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS digestivo;

-- la recreamos
CREATE TABLE digestivo(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    disfagia tinyint(1) DEFAULT 0,
    pirosis tinyint(1) DEFAULT 0,
    regurgitacion tinyint(1) DEFAULT 0,
    constipacion tinyint(1) DEFAULT 0,
    bolo tinyint(1) DEFAULT 0,
    notiene tinyint(1) DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de compromiso digestivo';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_digestivo;

-- la recreamos
CREATE TABLE auditoria_digestivo(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    disfagia tinyint(1) DEFAULT 0,
    pirosis tinyint(1) DEFAULT 0,
    regurgitacion tinyint(1) DEFAULT 0,
    constipacion tinyint(1) DEFAULT 0,
    bolo tinyint(1) DEFAULT 0,
    notiene tinyint(1) DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del compromiso digestivo';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_digestivo;

-- lo recreamos
CREATE TRIGGER edicion_digestivo
AFTER UPDATE ON digestivo
FOR EACH ROW
INSERT INTO auditoria_digestivo
       (id,
        paciente,
        disfagia,
        pirosis,
        regurgitacion,
        constipacion,
        bolo,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disfagia,
        OLD.pirosis,
        OLD.regurgitacion,
        OLD.constipacion,
        OLD.bolo,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_digestivo;

-- lo recreamos
CREATE TRIGGER eliminacion_digestivo
AFTER DELETE ON digestivo
FOR EACH ROW
INSERT INTO auditoria_digestivo
       (id,
        paciente,
        disfagia,
        pirosis,
        regurgitacion,
        constipacion,
        bolo,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disfagia,
        OLD.pirosis,
        OLD.regurgitacion,
        OLD.constipacion,
        OLD.bolo,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                          Examan Físico                                  */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con el resultado del examen físico

    id entero clave del registro
    protocolo entero clave del paciente
    visita clave de la visita
    ta string presión arterial
    peso entero peso en kilos
    fc entero frecuencia cardíaca
    spo entero saturación de oxígeno en porcentaje
    talla flotante con dos decimales altura
    bmi flotante índice de masa corporal
    edema entero pequeño si hay edema de miembros inferiores
          0 falso - 1 verdadero
    sp entero pequeño 0 falso - 1 verdadero
    mvdisminuido entero pequeño 0 falso - 1 verdadero
    crepitantes entero pequeño 0 falso - 1 verdadero
    sibilancias entero pequeño 0 falso - 1 verdadero

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS fisico;

-- la recreamos
CREATE TABLE fisico(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ta varchar(7) NOT NULL,
    peso decimal(5,2) UNSIGNED NOT NULL,
    fc int(3) UNSIGNED NOT NULL,
    spo int(3) UNSIGNED NOT NULL,
    talla decimal(3,2) UNSIGNED NOT NULL,
    bmi decimal(4,2) UNSIGNED NOT NULL,
    edema tinyint(1) UNSIGNED DEFAULT 0,
    sp tinyint(1) UNSIGNED DEFAULT 0,
    mvdisminuido tinyint(1) UNSIGNED DEFAULT 0,
    crepitantes tinyint(1) UNSIGNED DEFAULT 0,
    sibilancias tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY protocolo(protocolo),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Resultados del examen fìsico';

-- eliminamos la tabla de auditorìa si existe
DROP TABLE IF EXISTS auditoria_fisico;

-- la recreamos
CREATE TABLE auditoria_fisico(
    id int(6) UNSIGNED NOT NULL,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ta varchar(7) NOT NULL,
    peso decimal(5,2) UNSIGNED NOT NULL,
    fc int(3) UNSIGNED NOT NULL,
    spo int(3) UNSIGNED NOT NULL,
    talla decimal(3,2) UNSIGNED NOT NULL,
    bmi decimal(4,2) UNSIGNED NOT NULL,
    edema tinyint(1) UNSIGNED DEFAULT 0,
    sp tinyint(1) UNSIGNED DEFAULT 0,
    mvdisminuido tinyint(1) UNSIGNED DEFAULT 0,
    crepitantes tinyint(1) UNSIGNED DEFAULT 0,
    sibilancias tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY visita(visita),
    KEY protocolo(protocolo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del examen fìsico';

-- eliminamos el trigger de ediciòn si existe
DROP TRIGGER IF EXISTS edicion_fisico;

-- lo recreamos
CREATE TRIGGER edicion_fisico
AFTER UPDATE ON fisico
FOR EACH ROW
INSERT INTO auditoria_fisico
    (id,
     protocolo,
     visita,
     ta,
     peso,
     fc,
     spo,
     talla,
     bmi,
     edema,
     sp,
     mvdisminuido,
     crepitantes,
     sibilancias,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.protocolo,
     OLD.visita,
     OLD.ta,
     OLD.peso,
     OLD.fc,
     OLD.spo,
     OLD.talla,
     OLD.bmi,
     OLD.edema,
     OLD.sp,
     OLD.mvdisminuido,
     OLD.crepitantes,
     OLD.sibilancias,
     OLD.usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_fisico;

-- lo recreamos
CREATE TRIGGER eliminacion_fisico
AFTER DELETE ON fisico
FOR EACH ROW
INSERT INTO auditoria_fisico
    (id,
     protocolo,
     visita,
     ta,
     peso,
     fc,
     spo,
     talla,
     bmi,
     edema,
     sp,
     mvdisminuido,
     crepitantes,
     sibilancias,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.protocolo,
     OLD.visita,
     OLD.ta,
     OLD.peso,
     OLD.fc,
     OLD.spo,
     OLD.talla,
     OLD.bmi,
     OLD.edema,
     OLD.sp,
     OLD.mvdisminuido,
     OLD.crepitantes,
     OLD.sibilancias,
     OLD.usuario,
     OLD.fecha,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                         Cardiovascular                                  */
/*                                                                         */
/***************************************************************************/

/*

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    ausnormal tinyint(1) (0 falso - 1 verdadero) auscultación normal
    irregular tinyint(1) (0 falso - 1 verdadero) auscultación irregular
    3er tinyint(1) (0 falso - 1 verdadero)
    4to tinyint(1) (0 falso - 1 verdadero)
    eyectivo tinyint(1) (0 falso - 1 verdadero) soplo eyectivo
    regurgitativo tinyint(1) (0 falso - 1 verdadero) soplo mitral
    sinsistolico tinyint(1) (0 falso - 1 verdadero) no tiene soplo sistólico
    aortico tinyint(1) (0 falso - 1 verdadero) soplo diastólico aórtico
    diastolicomitral tinyint(1) (0 falso - 1 verdadero) soplo diastólico mitral
    sindiastolico tinyint(1) (0 falso - 1 verdadero)
    hepatomegalia tinyint (0 falso - 1 verdadero)
    esplenomegalia tinyint (0 falso - 1 verdadero)
    ingurgitacion tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS cardiovascular;

-- la recreamos
CREATE TABLE cardiovascular(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ausnormal tinyint(1) UNSIGNED DEFAULT 0,
    irregular tinyint(1) UNSIGNED DEFAULT 0,
    3er tinyint(1) UNSIGNED DEFAULT 0,
    4to tinyint(1) UNSIGNED DEFAULT 0,
    eyectivo tinyint(1) UNSIGNED DEFAULT 0,
    regurgitativo tinyint(1) UNSIGNED DEFAULT 0,
    sinsistolico tinyint(1) UNSIGNED DEFAULT 0,
    aortico tinyint(1) UNSIGNED DEFAULT 0,
    diastolicomitral tinyint(1) UNSIGNED DEFAULT 0,
    sindiastolico tinyint(1) UNSIGNED DEFAULT 0,
    hepatomegalia tinyint(1) UNSIGNED DEFAULT 0,
    esplenomegalia tinyint(1) UNSIGNED DEFAULT 0,
    ingurgitacion tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del aparato cardiovascular';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_cardiovascular;

-- la recreamos
CREATE TABLE auditoria_cardiovascular(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ausnormal tinyint(1) UNSIGNED DEFAULT 0,
    irregular tinyint(1) UNSIGNED DEFAULT 0,
    3er tinyint(1) UNSIGNED DEFAULT 0,
    4to tinyint(1) UNSIGNED DEFAULT 0,
    eyectivo tinyint(1) UNSIGNED DEFAULT 0,
    regurgitativo tinyint(1) UNSIGNED DEFAULT 0,
    sinsistolico tinyint(1) UNSIGNED DEFAULT 0,
    aortico tinyint(1) UNSIGNED DEFAULT 0,
    diastolicomitral tinyint(1) UNSIGNED DEFAULT 0,
    sindiastolico tinyint(1) UNSIGNED DEFAULT 0,
    hepatomegalia tinyint(1) UNSIGNED DEFAULT 0,
    esplenomegalia tinyint(1) UNSIGNED DEFAULT 0,
    ingurgitacion tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del aparato cardiovascular';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS auditoria_cardiovascular;

-- lo recreamos
CREATE TRIGGER edicion_cardiovascular
AFTER UPDATE ON cardiovascular
FOR EACH ROW
INSERT INTO auditoria_cardiovascular
       (id,
        paciente,
        visita,
        ausnormal,
        irregular,
        3er,
        4to,
        eyectivo,
        regurgitativo,
        sinsistolico,
        aortico,
        diastolicomitral,
        sindiastolico,
        hepatomegalia,
        esplenomegalia,
        ingurgitacion,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.ausnormal,
        OLD.irregular,
        OLD.3er,
        OLD.4to,
        OLD.eyectivo,
        OLD.regurgitativo,
        OLD.sinsistolico,
        OLD.aortico,
        OLD.diastolicomitral,
        OLD.sindiastolico,
        OLD.hepatomegalia,
        OLD.esplenomegalia,
        OLD.ingurgitacion,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_cardivascular;

-- lo recreamos
CREATE TRIGGER eliminacion_cardiovascular
AFTER DELETE ON cardiovascular
FOR EACH ROW
INSERT INTO auditoria_cardiovascular
       (id,
        paciente,
        visita,
        ausnormal,
        irregular,
        3er,
        4to,
        eyectivo,
        regurgitativo,
        sinsistolico,
        aortico,
        diastolicomitral,
        sindiastolico,
        hepatomegalia,
        esplenomegalia,
        ingurgitacion,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.ausnormal,
        OLD.irregular,
        OLD.3er,
        OLD.4to,
        OLD.eyectivo,
        OLD.regurgitativo,
        OLD.sinsistolico,
        OLD.aortico,
        OLD.diastolicomitral,
        OLD.sindiastolico,
        OLD.hepatomegalia,
        OLD.esplenomegalia,
        OLD.ingurgitacion,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                               Rx                                        */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene información sobre las radiografías y puede haber
    mas de una entrada por paciente

    id clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de toma de la radiografía
    ict tinyint (0 falso - 1 verdadero)
    cardiomegalia tinyint (0 falso - 1 verdadero)
    pleuro tinyint (0 falso - 1 verdadero)
    epoc tinyint (0 falso - 1 verdadero)
    calcificaciones tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS rx;

-- la recreamos
CREATE TABLE rx (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    ict tinyint(1) UNSIGNED DEFAULT 0,
    cardiomegalia tinyint(1) UNSIGNED DEFAULT 0,
    pleuro tinyint(1) UNSIGNED DEFAULT 0,
    epoc tinyint(1) UNSIGNED DEFAULT 0,
    calcificaciones tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Información de las radiografías de torax';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_rx;

-- la recreamos
CREATE TABLE auditoria_rx (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    ict tinyint(1) UNSIGNED DEFAULT 0,
    cardiomegalia tinyint(1) UNSIGNED DEFAULT 0,
    pleuro tinyint(1) UNSIGNED DEFAULT 0,
    epoc tinyint(1) UNSIGNED DEFAULT 0,
    calcificaciones tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las radiografías';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_rx;

-- lo recreamos
CREATE TRIGGER edicion_rx
AFTER UPDATE ON rx
FOR EACH ROW
INSERT INTO auditoria_rx
       (id,
        paciente,
        visita,
        fecha,
        ict,
        cardiomegalia,
        pleuro,
        epoc,
        calcificaciones,
        notiene,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.ict,
        OLD.cardiomegalia,
        OLD.pleuro,
        OLD.epoc,
        OLD.calcificaciones,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_rx;

-- lo recreamos
CREATE TRIGGER eliminacion_rx
AFTER DELETE ON rx
FOR EACH ROW
INSERT INTO auditoria_rx
       (id,
        paciente,
        visita,
        fecha,
        ict,
        cardiomegalia,
        pleuro,
        epoc,
        calcificaciones,
        notiene,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.ict,
        OLD.cardiomegalia,
        OLD.pleuro,
        OLD.epoc,
        OLD.calcificaciones,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                         Electrocardiograma                              */
/*                                                                         */
/***************************************************************************/

/*

    Datos de los electrocardiograma, esta tabla debe tener una entrada
    por cada visita

    id entero clave del registro
    protocolo entero clave del paciente
    visita clave de la visita
    fecha date fecha de toma del electro
    normal tinyint (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    fc entero frecuencia cardìaca
    arritmia enum
    qrs decimal 3,2 fracción de segundo
    ejeqrs entero (desviación en grados)
    pr decimal 3,2 fracción de segundo
    brd tinyint (0 falso - 1 verdadero)
    brdmoderado tinyint (0 falso - 1 verdadero)
    bcrd tinyint (0 falso - 1 verdadero)
    tendenciahbai tinyint (0 falso - 1 verdadero)
    hbai tinyint (0 falso - 1 verdadero)
    tciv tinyint (0 falso - 1 verdadero)
    bcri tinyint (0 falso - 1 verdadero)
    bav1g tinyint (0 falso - 1 verdadero)
    bav2g tinyint (0 falso - 1 verdadero)
    bav3g tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    fibrosis tinyint (0 falso - 1 verdadero)
    aumentoai tinyint (0 falso - 1 verdadero)
    wpv tinyint (0 falso - 1 verdadero)
    hvi tinyint (0 falso - 1 verdadero)
    sintranstornos tinyint(1) (0 falso - 1 verdadero)
    repolarizacion enum
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS electrocardiograma;

-- la recreamos
CREATE TABLE electrocardiograma (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    fc int(3) UNSIGNED NOT NULL,
    arritmia enum("FA", "EVF", "Pares", "TV", "No Tiene") DEFAULT "No Tiene",
    qrs decimal(3,2) UNSIGNED NOT NULL,
    ejeqrs int(2) UNSIGNED NOT NULL,
    pr decimal(3,2) UNSIGNED NOT NULL,
    brd tinyint(1) UNSIGNED DEFAULT 0,
    brdmoderado tinyint(1) UNSIGNED DEFAULT 0,
    bcrd tinyint(1) UNSIGNED DEFAULT 0,
    tendenciahbai tinyint(1) UNSIGNED DEFAULT 0,
    hbai tinyint(1) UNSIGNED DEFAULT 0,
    tciv tinyint(1) UNSIGNED DEFAULT 0,
    bcri tinyint(1) UNSIGNED DEFAULT 0,
    bav1g tinyint(1) UNSIGNED DEFAULT 0,
    bav2g tinyint(1) UNSIGNED DEFAULT 0,
    bav3g tinyint(1) UNSIGNED DEFAULT 0,
    fibrosis tinyint(1) UNSIGNED DEFAULT 0,
    aumentoai tinyint(1) UNSIGNED DEFAULT 0,
    wpv tinyint(1) UNSIGNED DEFAULT 0,
    hvi tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    sintranstornos tinyint(1) UNSIGNED DEFAULT 0,
    repolarizacion enum("Transt. Labil", "Transt. Isquemico", "Difusos Primarios", "No Tiene") DEFAULT "No Tiene",
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY protocolo(protocolo),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los electrocardiogramas';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_electro;

-- la recreamos
CREATE TABLE auditoria_electro (
    id int(6) UNSIGNED NOT NULL ,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    fc int(3) UNSIGNED NOT NULL,
    arritmia enum("FA", "EVF", "Pares", "TV", "No Tiene") DEFAULT "No Tiene",
    qrs decimal(3,2) UNSIGNED NOT NULL,
    ejeqrs int(2) UNSIGNED NOT NULL,
    pr decimal(3,2) UNSIGNED NOT NULL,
    brd tinyint(1) UNSIGNED DEFAULT 0,
    brdmoderado tinyint(1) UNSIGNED DEFAULT 0,
    bcrd tinyint(1) UNSIGNED DEFAULT 0,
    tendenciahbai tinyint(1) UNSIGNED DEFAULT 0,
    hbai tinyint(1) UNSIGNED DEFAULT 0,
    tciv tinyint(1) UNSIGNED DEFAULT 0,
    bcri tinyint(1) UNSIGNED DEFAULT 0,
    bav1g tinyint(1) UNSIGNED DEFAULT 0,
    bav2g tinyint(1) UNSIGNED DEFAULT 0,
    bav3g tinyint(1) UNSIGNED DEFAULT 0,
    fibrosis tinyint(1) UNSIGNED DEFAULT 0,
    aumentoai tinyint(1) UNSIGNED DEFAULT 0,
    wpv tinyint(1) UNSIGNED DEFAULT 0,
    hvi tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    sintranstornos tinyint(1) UNSIGNED DEFAULT 0,
    repolarizacion enum("Transt. Labil", "Transt. Isquemico", "Difusos Primarios", "No Tiene") DEFAULT "No Tiene",
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY protocolo(protocolo),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los electrocardiogramas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_electro;

-- lo recreamos
CREATE TRIGGER edicion_electro
AFTER UPDATE ON electrocardiograma
FOR EACH ROW
INSERT INTO auditoria_electro
       (id,
        protocolo,
        visita,
        fecha,
        normal,
        bradicardia,
        fc,
        arritmia,
        ars,
        ejeqrs,
        pr,
        brd,
        brdmoderado,
        bcrd,
        tendenciahbai,
        hbai,
        tciv,
        bcri,
        bav1g,
        bav2,
        bav3,
        fibrosis,
        aumentoai,
        wpv,
        hvi,
        notiene,
        sintranstornos,
        repolarizacion,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.protocolo,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.bradicardia,
        OLD.fc,
        OLD.arritmia,
        OLD.qrs,
        OLD.ejeqrs,
        OLD.pr,
        OLD.brd,
        OLD.brdmoderado,
        OLD.bcrd,
        OLD.tendenciahbai,
        OLD.hbai,
        OLD.tciv,
        OLD.bcri,
        OLD.bav1g,
        OLD.bav2g,
        OLD.bav3g,
        OLD.fibrosis,
        OLD.aumentoai,
        OLD.wpv,
        OLD.hvi,
        OLD.notiene,
        OLD.sintranstornos,
        OLD.repolarizacion,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_electro;

-- lo recreamos
CREATE TRIGGER eliminacion_electro
AFTER DELETE ON electrocardiograma
FOR EACH ROW
INSERT INTO auditoria_electro
       (id,
        protocolo,
        visita,
        fecha,
        normal,
        bradicardia,
        fc,
        arritmia,
        ars,
        ejeqrs,
        pr,
        brd,
        brdmoderado,
        bcrd,
        tendenciahbai,
        hbai,
        tciv,
        bcri,
        bav1g,
        bav2,
        bav3,
        fibrosis,
        aumentoai,
        wpv,
        hvi,
        notiene,
        sintranstornos,
        repolarizacion,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.protocolo,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.bradicardia,
        OLD.fc,
        OLD.arritmia,
        OLD.qrs,
        OLD.ejeqrs,
        OLD.pr,
        OLD.brd,
        OLD.brdmoderado,
        OLD.bcrd,
        OLD.tendenciahbai,
        OLD.hbai,
        OLD.tciv,
        OLD.bcri,
        OLD.bav1g,
        OLD.bav2g,
        OLD.bav3g,
        OLD.fibrosis,
        OLD.aumentoai,
        OLD.wpv,
        OLD.hvi,
        OLD.notiene,
        OLD.sintranstornos,
        OLD.repolarizacion,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                           Ecocardiograma                                */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de los ecocardiogramas, los trae el paciente y pueden
    haber entradas (varias) o ninguna por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de toma
    normal tinyint (0 falso - 1 verdadero)
    ddvi entero 2 en milímetros
    dsvi (verificar tipo de dato)
    fac decimal
    siv entero milímetros
    pp entero milímetros
    ai entero milímetros
    ao entero milímetros
    fey decimal
    ddvd (verificar tipo de dato)
    ad entero milímetros
    movilidad varchar(200) texto libre
    fsvi varchar(200) texto libre
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS ecocardiograma;

-- la recreamos
CREATE TABLE ecocardiograma (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    ddvi int(2) UNSIGNED DEFAULT 0,
    dsvi int(2) UNSIGNED DEFAULT 0,
    fac decimal(3,2) UNSIGNED DEFAULT 0.00,
    siv int(2) UNSIGNED DEFAULT 0,
    pp int(2) UNSIGNED DEFAULT 0,
    ai int(2) UNSIGNED DEFAULT 0,
    ao int(2) UNSIGNED DEFAULT 0,
    fey decimal(3,2) UNSIGNED DEFAULT 0.00,
    ddvd int(2) UNSIGNED DEFAULT 0,
    ad int(2) UNSIGNED DEFAULT 0,
    movilidad varchar(200) DEFAULT NULL,
    fsvi varchar(200) DEFAULT NULL,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los ecocardiogramas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_ecocardiograma;

-- creamos la tabla de auditoría
CREATE TABLE auditoria_ecocardiograma (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    ddvi int(2) UNSIGNED DEFAULT 0,
    dsvi int(2) UNSIGNED DEFAULT 0,
    fac decimal(3,2) UNSIGNED DEFAULT 0.00,
    siv int(2) UNSIGNED DEFAULT 0,
    pp int(2) UNSIGNED DEFAULT 0,
    ai int(2) UNSIGNED DEFAULT 0,
    ao int(2) UNSIGNED DEFAULT 0,
    fey decimal(3,2) UNSIGNED DEFAULT 0.00,
    ddvd int(2) UNSIGNED DEFAULT 0,
    ad int(2) UNSIGNED DEFAULT 0,
    movilidad varchar(200) DEFAULT NULL,
    fsvi varchar(200) DEFAULT NULL,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta DATE NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los ecocardiogramas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_ecocardiograma;

-- lo recreamos
CREATE TRIGGER edicion_ecocardiograma
AFTER UPDATE ON ecocardiograma
FOR EACH ROW
INSERT INTO auditoria_ecocardiograma
       (id,
        paciente,
        visita,
        fecha,
        normal,
        ddvi,
        dsvi,
        fac,
        siv,
        pp,
        ai,
        ao,
        fey,
        ddvd,
        ad,
        movilidad,
        fsvi,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.ddvi,
        OLD.dsvi,
        OLD.fac,
        OLD.siv,
        OLD.pp,
        OLD.ai,
        OLD.ao,
        OLD.fey,
        OLD.ddvd,
        OLD.ad,
        OLD.movilidad,
        OLD.fsvi,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ecocardiograma;

-- lo recreamos
CREATE TRIGGER eliminacion_ecocardiograma
AFTER DELETE ON ecocardiograma
FOR EACH ROW
INSERT INTO auditoria_ecocardiograma
       (id,
        paciente,
        visita,
        fecha,
        normal,
        ddvi,
        dsvi,
        fac,
        siv,
        pp,
        ai,
        ao,
        fey,
        ddvd,
        ad,
        movilidad,
        fsvi,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.ddvi,
        OLD.dsvi,
        OLD.fac,
        OLD.siv,
        OLD.pp,
        OLD.ai,
        OLD.ao,
        OLD.fey,
        OLD.ddvd,
        OLD.ad,
        OLD.movilidad,
        OLD.fsvi,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                                 Holter                                  */
/*                                                                         */
/***************************************************************************/

/*

    Esta es la tabla con los datos del holter, puede traerlo el paciente
    se le puede tomar en el instituto o puede no tener

    id clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de adminstración
    media entero frecuencia cardìaca media
    minima entero frecuencia cardíaca mínima
    máxima entero frecuencia cardíaca máxima
    latidos entero número total de latidos
    ev tinyint si tuvo episodios ventriculares
    nev entero número de episodios
    tasaev decimal porcentaje de episodios
    esv tinyint si tuvo episodios extraventriculares
    nesv entero número de episodios extraventriculares
    tasa esv decimal, porcentaje de episodios
    normal tinyint (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    rtacronotropica tinyint (0 falso - 1 verdadero)
    arritmiasevera tinyint (0 falso - 1 verdadero)
    arritmiasimple tinyint (0 falso - 1 verdadero)
    arritmiasupra tinyint (0 falso - 1 verdadero)
    bderama tinyint (0 falso - 1 verdadero)
    bav2g tinyint (0 falso - 1 verdadero)
    disociacion tinyint (0 falso - 1 verdadero)
    comentarios texto comentarios del usuario
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS holter;

-- la recreamos
CREATE TABLE holter (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    media int(3) UNSIGNED NOT NULL,
    minima int(3) UNSIGNED NOT NULL,
    maxima int(3) UNSIGNED NOT NULL,
    latidos int(5) UNSIGNED NOT NULL,
    ev tinyint(1) UNSIGNED DEFAULT 0,
    nev int(3) UNSIGNED DEFAULT 0,
    tasaev decimal(4,2) UNSIGNED DEFAULT 0.00,
    esv tinyint(1) UNSIGNED DEFAULT 0,
    nesv int(3) UNSIGNED DEFAULT 0,
    tasaesv decimal(4,2) UNSIGNED DEFAULT 0.00,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(0) UNSIGNED DEFAULT 0,
    rtacronotropica tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasevera tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasimple tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasupra tinyint(0) UNSIGNED DEFAULT 0,
    bderama tinyint(0) UNSIGNED DEFAULT 0,
    bav2g tinyint(0) UNSIGNED DEFAULT 0,
    disociacion tinyint(0) UNSIGNED DEFAULT 0,
    comentarios mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del holter';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_holter;

-- creamos la tabla de auditoría
CREATE TABLE auditoria_holter (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    media int(3) UNSIGNED NOT NULL,
    minima int(3) UNSIGNED NOT NULL,
    maxima int(3) UNSIGNED NOT NULL,
    latidos int(5) UNSIGNED NOT NULL,
    ev tinyint(1) UNSIGNED DEFAULT 0,
    nev int(3) UNSIGNED DEFAULT 0,
    tasaev decimal(4,2) UNSIGNED DEFAULT 0.00,
    esv tinyint(1) UNSIGNED DEFAULT 0,
    nesv int(3) UNSIGNED DEFAULT 0,
    tasaesv decimal(4,2) UNSIGNED DEFAULT 0.00,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(0) UNSIGNED DEFAULT 0,
    rtacronotropica tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasevera tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasimple tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasupra tinyint(0) UNSIGNED DEFAULT 0,
    bderama tinyint(0) UNSIGNED DEFAULT 0,
    bav2g tinyint(0) UNSIGNED DEFAULT 0,
    disociacion tinyint(0) UNSIGNED DEFAULT 0,
    comentarios mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del holter';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_holter;

-- lo recreamos
CREATE TRIGGER edicion_holter
AFTER UPDATE ON holter
FOR EACH ROW
INSERT INTO auditoria_holter
       (id,
        paciente,
        visita,
        fecha,
        media,
        minima,
        maxima,
        latidos,
        ev,
        nev,
        tasaev,
        esv,
        nesv,
        tasanev,
        normal,
        bradicardia,
        rtacronotropica,
        arritmiasevera,
        arritmiasimple,
        arritmiasupra,
        bderama,
        bav2g,
        disociacion,
        comentarios,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.media,
        OLD.minima,
        OLD.maxima,
        OLD.latidos,
        OLD.ev,
        OLD.nev,
        OLD.tasaev,
        OLD.esv,
        OLD.nesv,
        OLD.tasaesv,
        OLD.normal,
        OLD.bradicardia,
        OLD.rtacronotropica,
        OLD.arritmiasevera,
        OLD.arritmiasimple,
        OLD.arritmiasupra,
        OLD.bderama,
        OLD.bav2g,
        OLD.disociacion,
        OLD.comentarios,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_holter;

-- lo recreamos
CREATE TRIGGER eliminacion_holter
AFTER DELETE ON holter
FOR EACH ROW
INSERT INTO auditoria_holter
       (id,
        paciente,
        visita,
        fecha,
        media,
        minima,
        maxima,
        latidos,
        ev,
        nev,
        tasaev,
        esv,
        nesv,
        tasanev,
        normal,
        bradicardia,
        rtacronotropica,
        arritmiasevera,
        arritmiasimple,
        arritmiasupra,
        bderama,
        bav2g,
        disociacion,
        comentarios,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.media,
        OLD.minima,
        OLD.maxima,
        OLD.latidos,
        OLD.ev,
        OLD.nev,
        OLD.tasaev,
        OLD.esv,
        OLD.nesv,
        OLD.tasaesv,
        OLD.normal,
        OLD.bradicardia,
        OLD.rtacronotropica,
        OLD.arritmiasevera,
        OLD.arritmiasimple,
        OLD.arritmiasupra,
        OLD.bderama,
        OLD.bav2g,
        OLD.disociacion,
        OLD.comentarios,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                            Ergometría                                   */
/*                                                                         */
/***************************************************************************/

/*

    Estos son los datos de la ergometría, los trae el paciente y puede
    haber mas de un registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de administración
    fcbasal entero frecuencia basal
    fcmax entero frecuencia màxima
    tabasal varchar con màscara tensión basal
    tamax varchar con máscara tensión máxima
    kpm entero km recorridos
    itt varchar(200) por ahora texto libre
    observaciones texto comentarios del usuario
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS ergometria;

-- la recreamos
CREATE TABLE ergometria(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fcbasal int(3) UNSIGNED DEFAULT 0,
    fcmax int(3) UNSIGNED DEFAULT 0,
    tabasal varchar(20) NOT NULL,
    tamax varchar(20) NOT NULL,
    kpm int(4) UNSIGNED NOT NULL,
    itt varchar(200) DEFAULT NULL,
    observaciones mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del Ergometría';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_ergometria;

-- la recreamos
CREATE TABLE auditoria_ergometria(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fcbasal int(3) UNSIGNED DEFAULT 0,
    fcmax int(3) UNSIGNED DEFAULT 0,
    tabasal varchar(20) NOT NULL,
    tamax varchar(20) NOT NULL,
    kpm int(4) UNSIGNED NOT NULL,
    itt varchar(200) DEFAULT NULL,
    observaciones mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de la Ergometría';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_ergometria;

-- lo recreamos
CREATE TRIGGER edicion_ergometria
AFTER UPDATE ON ergometria
FOR EACH ROW
INSERT INTO auditoria_ergometria
       (id,
        paciente,
        visita,
        fecha,
        fcbasal,
        fcmax,
        tabasal,
        tamax,
        kpm,
        itt,
        observaciones,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.fcbasal,
        OLD.fcmax,
        OLD.tabasal,
        OLD.tamax,
        OLD.kpm,
        OLD.itt,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ergometria;

-- lo recreamos
CREATE TRIGGER eliminacion_ergometria
AFTER DELETE ON ergometria
FOR EACH ROW
INSERT INTO auditoria_ergometria
       (id,
        paciente,
        visita,
        fecha,
        fcbasal,
        fcmax,
        tabasal,
        tamax,
        kpm,
        itt,
        observaciones,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.fcbasal,
        OLD.fcmax,
        OLD.tabasal,
        OLD.tamax,
        OLD.kpm,
        OLD.itt,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                           Visitas del Paciente                          */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla funciona como pivot de las visitas del paciente
    id entero clave del registro
    fecha date fecha de la visita
    usuario usuario que ingresó el registro
    alta timestamp fecha de alta del registro

    No tenemos registros de auditoría de esta tabla

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS visitas;

-- la recreamos
CREATE TABLE visitas (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    fecha date NOT NULL,
    usuario int(6) UNSIGNED NOT NULL,
    alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Pivot de visitas';


/***************************************************************************/
/*                                                                         */
/*                            Clasificación                                */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene los datos de la clasificación de la enfermedad de
    Chagas según Kuscknir debería haber una entrada por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    estadio enum estadio en que se encuentra
    observaciones comentarios del usuario
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS clasificacion;

-- la recreamos
CREATE TABLE clasificacion (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    estadio enum("Estadio 0", "Estadio I", "Estadio II", "Estadio III") DEFAULT NULL,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Clasificacion según Kuscknir';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_clasificacion;

-- la recreamos
CREATE TABLE auditoria_clasificacion (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    estadio enum("Estadio 0", "Estadio I", "Estadio II", "Estadio III") DEFAULT NULL,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de la clasificación';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_clasificacion;

-- lo recreamos
CREATE TRIGGER edicion_clasificacion
AFTER UPDATE ON clasificacion
FOR EACH ROW
INSERT INTO auditoria_clasificacion
       (id,
        paciente,
        visita,
        estadio,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.estadio,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_clasificacion;

-- lo recreamos
CREATE TRIGGER eliminacion_clasificacion
AFTER DELETE ON clasificacion
FOR EACH ROW
INSERT INTO auditoria_clasificacion
       (id,
        paciente,
        visita,
        estadio,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.estadio,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Familiares                                 */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes familiares de cada paciente, esta tendría
    que haber un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    subita tinyint si hay muerte súbita (0 falso - 1 verdadero)
    cardiopatia tinyint si hay cardiopatía (0 falso - 1 verdadero)
    disfagia tinyint si ha disfagia, constipación o evidencia de megas
    marcapaso tinyint si algún familiar tiene marcapaso
    nosabe tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    otra texto descripción de la enfermedad
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS familiares;

-- la recreamos
CREATE TABLE familiares (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    subita tinyint(1) UNSIGNED DEFAULT 0,
    cardiopatia tinyint(1) UNSIGNED DEFAULT 0,
    disfagia tinyint(1) UNSIGNED DEFAULT 0,
    marcapaso tinyint(1) UNSIGNED DEFAULT 0,
    nosabe tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    otra text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Antecedentes familiares';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_familiares;

-- la recreamos
CREATE TABLE auditoria_familiares (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    subita tinyint(1) UNSIGNED DEFAULT 0,
    cardiopatia tinyint(1) UNSIGNED DEFAULT 0,
    disfagia tinyint(1) UNSIGNED DEFAULT 0,
    marcapaso tinyint(1) UNSIGNED DEFAULT 0,
    nosabe tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    otra text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de antecedentes familiares';

-- eliminamos el trigger
DROP TRIGGER IF EXISTS edicion_familiares;

-- lo recreamos
CREATE TRIGGER edicion_familiares
AFTER UPDATE ON familiares
FOR EACH ROW
INSERT INTO auditoria_familiares
       (id,
        paciente,
        subita,
        cardiopatia,
        disfagia,
        marcapaso,
        nosabe,
        notiene,
        otra,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.subita,
        OLD.cardiopatia,
        OLD.disfagia,
        OLD.marcapaso,
        OLD.nosabe,
        OLD.notiene,
        OLD.otra,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_familiares;

-- lo recreamos
CREATE TRIGGER eliminacion_familiares
AFTER DELETE ON familiares
FOR EACH ROW
INSERT INTO auditoria_familiares
       (id,
        paciente,
        subita,
        cardiopatia,
        disfagia,
        marcapaso,
        nosabe,
        notiene,
        otra,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.subita,
        OLD.cardiopatia,
        OLD.disfagia,
        OLD.marcapaso,
        OLD.nosabe,
        OLD.notiene,
        OLD.otra,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Citas                                      */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de la agenda de citas de los profesionales
    id entero clave del registro
    profesional entero clave del profesional que tiene la cita
    protocolo entero clave del protocolo del paciente
    fecha date fecha de la entrevista
    hora time hora de la entrevista
    concurrio entero si concurrió a la entrevista
    usuario entero clave del usuario que ingresó el registro
    fecha date time, fecha de alta del registro

    No usamos tablas de auditoría en esta

*/

-- eliminamos la tabla si existe
DROP TABLE IF exists citas;

-- la recreamos
CREATE TABLE citas (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    profesional int(4) UNSIGNED NOT NULL,
    protocolo int(8) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    hora time NOT NULL,
    concurrio tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY profesional(profesional),
    KEY protocolo(protocolo),
    KEY usurio(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Agenda con citas de los pacientes';


/***************************************************************************/
/*                                                                         */
/*                            Horarios                                     */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los horarios de atención de los profesionales
    id int(4) clave del registro
    profesional int(4) clave del profesional
    lunes martes miercoles jueves viernes dias de atención
    inicio time hora de inicio de atención
    fin time hora de finalización de la atención
    usuario int(4) clave del usuario
    fecha_alta datetime fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS horarios;

-- la recreamos
CREATE TABLE horarios (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    profesional int(4) UNSIGNED NOT NULL,
    inicio_lunes time DEFAULT NULL,
    fin_lunes time DEFAULT NULL,
    inicio_martes time DEFAULT NULL,
    fin_martes time DEFAULT NULL,
    inicio_miercoles time DEFAULT NULL,
    fin_miercoles time DEFAULT NULL,
    inicio_jueves time DEFAULT NULL,
    fin_jueves time DEFAULT NULL,
    inicio_viernes time DEFAULT NULL,
    fin_viernes time DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY profesional(profesional),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Horarios de atención de los profesionales';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_horarios;

-- la recreamos
CREATE TABLE auditoria_horarios (
    id int(4) UNSIGNED NOT NULL,
    profesional int(4) UNSIGNED NOT NULL,
    inicio_lunes time DEFAULT NULL,
    fin_lunes time DEFAULT NULL,
    inicio_martes time DEFAULT NULL,
    fin_martes time DEFAULT NULL,
    inicio_miercoles time DEFAULT NULL,
    fin_miercoles time DEFAULT NULL,
    inicio_jueves time DEFAULT NULL,
    fin_jueves time DEFAULT NULL,
    inicio_viernes time DEFAULT NULL,
    fin_viernes time DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum('Edicion', 'Eliminacion'),
    PRIMARY KEY(id),
    KEY profesional(profesional),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los horarios de atención';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_horarios;

-- lo recreamos
CREATE TRIGGER edicion_horarios
AFTER UPDATE ON horarios
FOR EACH ROW
INSERT INTO auditoria_horarios
    (id,
     profesional,
     inicio_lunes,
     fin_lunes,
     inicio_martes,
     fin_martes,
     inicio_miercoles,
     fin_miercoles,
     inicio_jueves,
     fin_jueves,
     inicio_viernes,
     fin_viernes,
     usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.profesional,
     OLD.inicio_lunes,
     OLD.fin_lunes,
     OLD.inicio_martes,
     OLD.fin_martes,
     OLD.inicio_miercoles,
     OLD.fin_miercoles,
     OLD.inicio_jueves,
     OLD.fin_jueves,
     OLD.inicio_viernes,
     OLD.fin_viernes,
     OLD.usuario,
     OLD.fecha_alta,
     'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_horarios;

-- lo recreamos
CREATE TRIGGER eliminacion_horarios
AFTER DELETE ON horarios
FOR EACH ROW
INSERT INTO auditoria_horarios
    (id,
     profesional,
     inicio_lunes,
     fin_lunes,
     inicio_martes,
     fin_martes,
     inicio_miercoles,
     fin_miercoles,
     inicio_jueves,
     fin_jueves,
     inicio_viernes,
     fin_viernes,
     usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.profesional,
     OLD.inicio_lunes,
     OLD.fin_lunes,
     OLD.inicio_martes,
     OLD.fin_martes,
     OLD.inicio_miercoles,
     OLD.fin_miercoles,
     OLD.inicio_jueves,
     OLD.fin_jueves,
     OLD.inicio_viernes,
     OLD.fin_viernes,
     OLD.usuario,
     OLD.fecha_alta,
     'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*                           Proyectos                                     */
/*                                                                         */
/***************************************************************************/

/*
    Estructura de la tabla
    id entero clave del registro
    inicio fecha de inicio del proyecto
    fin fecha de finalización del proyecto
    titulo varchar titulo del proyecto
    descripcion texto con el abstract del proyecto
    consentimiento texto con el texto del consentimiento informado
    alta fecha de alta del registro
    usuario usuario que ingresó el registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS proyectos;

-- la recreamos
CREATE TABLE proyectos (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    institucion int(4) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    titulo varchar(200) NOT NULL,
    descripcion text,
    consentimiento longtext,
    alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY institucion(institucion),
    KEY departamento(departamento),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los proyectos de investigación';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_proyectos;

-- la recreamos
CREATE TABLE auditoria_proyectos (
    id int(4) UNSIGNED NOT NULL,
    institucion int(4) UNSIGNED NOT NULL,
    departamento int(4) UNSIGNED NOT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    titulo varchar(200) NOT NULL,
    descripcion text,
    consentimiento longtext,
    alta date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    PRIMARY KEY(id),
    KEY institucion(institucion),
    KEY departamento(departamento),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los proyectos de investigación';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_proyectos;

-- lo recreamos
CREATE TRIGGER edicion_proyectos
AFTER UPDATE ON proyectos
FOR EACH ROW
INSERT INTO auditoria_proyectos
       (id,
        institucion,
        departamento,
        inicio,
        fin,
        titulo,
        descripcion,
        consentimiento,
        alta,
        usuario,
        evento)
       VALUES
       (OLD.id,
        OLD.institucion,
        OLD.departamento,
        OLD.inicio,
        OLD.fin,
        OLD.titulo,
        OLD.descripcion,
        OLD.consentimiento,
        OLD.alta,
        OLD.usuario,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_proyectos;

-- lo recreamos
CREATE TRIGGER eliminacion_proyectos
AFTER DELETE ON proyectos
FOR EACH ROW
INSERT INTO auditoria_proyectos
       (id,
        inicio,
        fin,
        titulo,
        descripcion,
        consentimiento,
        alta,
        usuario,
        evento)
       VALUES
       (OLD.id,
        OLD.inicio,
        OLD.fin,
        OLD.titulo,
        OLD.descripcion,
        OLD.consentimiento,
        OLD.alta,
        OLD.usuario,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                          PacProyectos                                   */
/*                                                                         */
/***************************************************************************/

/*
    Tabla pivot con la información de los pacientes y que proyecto
    participan
    id entero, clave del registro
    proyecto entero clave del proyecto
    protocolo entero clave del paciente
    usuario entero clave del usuario
    alta fecha, fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS pacproyectos;

-- la recreamos
CREATE TABLE pacproyectos(
    id int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
    proyecto int(4) UNSIGNED NOT NULL,
    protocolo int(8) UNSIGNED NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY proyecto(proyecto),
    KEY protocolo(protocolo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Pivot de los proyectos y los pacientes';
