<?php

/**
 *
 * Class Familiares | familiares/familiares.class.php
 *
 * @package     Diagnostico
 * @subpackage  Familiares
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (0/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * antecedentes familiares
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Familiares {

    // declaración de variables
    protected $Link;              // puntero a la base de datos
    protected $Id;                // clave del registro
    protected $Paciente;          // clave del paciente
    protected $Subita;            // muerte súbita
    protected $Cardiopatia;       // 0 no 1 si
    protected $Disfagia;          // 0 no 1 si
    protected $Marcapaso;         // 0 no 1 si
    protected $NoSabe;            // 0 no 1 si
    protected $NoTiene;           // 0 no 1 si
    protected $Otra;              // descripción de otro antecedente
    protected $IdUsuario;         // clave del usuario
    protected $Usuario;           // nombre del usuario
    protected $Fecha;             // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase, llamado
     * desde el constructor o desde la consulta cuando
     * el registro está vacío
     */
    protected function initFamiliares(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Subita = 0;
        $this->Cardiopatia = 0;
        $this->Disfagia = 0;
        $this->Marcapaso = 0;
        $this->NoSabe = 0;
        $this->NoTiene = 0;
        $this->Otra = "";
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setSubita($subita){
        $this->Subita = $subita;
    }
    public function setCardiopatia($cardio){
        $this->Cardiopatia = $cardio;
    }
    public function setDisfagia($disfagia){
        $this->Disfagia = $disfagia;
    }
    public function setMarcapaso($marcapaso){
        $this->Marcapaso = $marcapaso;
    }
    public function setNoSabe($nosabe){
        $this->NoSabe = $nosabe;
    }
    public function setNoTiene($notiene){
        $this->NoTiene = $notiene;
    }
    public function setOtra($otra){
        $this->Otra = $otra;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getSubita(){
        return $this->Subita;
    }
    public function getCardiopatia(){
        return $this->Cardiopatia;
    }
    public function getDisfagia(){
        return $this->Disfagia;
    }
    public function getMarcapaso(){
        return $this->Marcapaso;
    }
    public function getNoSabe(){
        return $this->NoSabe;
    }
    public function getNoTiene(){
        return $this->NoTiene;
    }
    public function getOtra(){
        return $this->Otra;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave del paciente
     * Método que recibe como parámetro la clave del paciente
     * y asigna en las variables de clase los valores del registro
     */
    public function getDatosFamiliares($idpaciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_familiares.id AS id,
                            diagnostico.v_familiares.paciente AS paciente,
                            diagnostico.v_familiares.subita AS subita,
                            diagnostico.v_familiares.cardiopatia AS cardiopatia,
                            diagnostico.v_familiares.disfagia AS disfagia,
                            diagnostico.v_familiares.marcapaso AS marcapaso,
                            diagnostico.v_familiares.nosabe AS nosabe,
                            diagnostico.v_familiares.notiene AS notiene,
                            diagnostico.v_familiares.otra AS otra,
                            diagnostico.v_familiares.usuario AS usuario,
                            diagnostico.v_familiares.fecha AS fecha
                     FROM diagnostico.v_familiares
                     WHERE diagnostico.v_familiares.paciente = '$idpaciente'; ";
        $resultado = $this->Link->query($consulta);

        // si tiene registros
        if ($resultado->rowCount() != 0) {

            // obtenemos el registro y lo asignamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Subita = $subita;
            $this->Cardiopatia = $cardiopatia;
            $this->Disfagia = $disfagia;
            $this->Marcapaso = $marcapaso;
            $this->NoSabe = $nosabe;
            $this->NoTiene = $notiene;
            $this->Otra = $otra;
            $this->Usuario = $usuario;
            $this->Fecha = $fecha;

        // si no encontró
        } else {

            // inicializamos las variables
            $this->initFamiliares();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public function grabaFamiliares(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoFamiliares();
        } else {
            $this->editaFamiliares();
        }

        // retorna la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevoFamiliares(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.familiares
                                 (paciente,
                                  subita,
                                  cardiopatia,
                                  disfagia,
                                  marcapaso,
                                  nosabe,
                                  notiene,
                                  otra,
                                  usuario)
                                 VALUES
                                 (:paciente,
                                  :subita,
                                  :cardiopatia,
                                  :disfagia,
                                  :marcapaso,
                                  :nosabe,
                                  :notiene,
                                  :otra,
                                  :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",    $this->Paciente);
        $psInsertar->bindParam(":subita",      $this->Subita);
        $psInsertar->bindParam(":cardiopatia", $this->Cardiopatia);
        $psInsertar->bindParam(":disfagia",    $this->Disfagia);
        $psInsertar->bindParam(":marcapaso",   $this->Marcapaso);
        $psInsertar->bindParam(":nosabe",      $this->NoSabe);
        $psInsertar->bindParam(":notiene",     $this->NoTiene);
        $psInsertar->bindParam(":otra",        $this->Otra);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaFamiliares(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.familiares SET
                            subita = :subita,
                            cardiopatia = :cardiopatia,
                            disfagia = :disfagia,
                            marcapaso = :marcapaso,
                            nosabe = :nosabe,
                            notiene = :notiene,
                            otra = :otra,
                            usuario = :usuario
                     WHERE diagnostico.familiares.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":subita",      $this->Subita);
        $psInsertar->bindParam(":cardiopatia", $this->Cardiopatia);
        $psInsertar->bindParam(":disfagia",    $this->Disfagia);
        $psInsertar->bindParam(":marcapaso",   $this->Marcapaso);
        $psInsertar->bindParam(":nosabe",      $this->NoSabe);
        $psInsertar->bindParam(":notiene",     $this->NoTiene);
        $psInsertar->bindParam(":otra",        $this->Otra);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - entero con la clave del protocolo
     * @return boolean si existen antecedentes
     * Método utilizado al cargar el formulario que verifica
     * si ya se declararon los datos del familiar, retorna
     * verdadero si ya fueron declarados o falso si no
     * hay datos declarados
     */
    public function existeFamiliar($protocolo){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.familiares.id) AS registros
                     FROM diagnostico.familiares
                     WHERE diagnostico.familiares.paciente = '$protocolo'; ";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si hay resultados
        if ($regiostros != 0){

                // ya cargamos el registro en memoria
                // y retornamos
                $this->getDatosFamiliares(protocolo);
                return true;

        // si no hay datos
        } else {

            // retornamos
            return false;

        }

    }

}
?>