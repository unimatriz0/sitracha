/*

    Nombre: proyectos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 13/08/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de la tabla de
                 proyectos de investigación

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de proyectos
 */
class Proyectos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // definimos los layers
        this.layerProyectos = "";

        // inicializamos las variables
        this.initProyectos();

        // cargamos en el contenedor el formulario
        $("#form_proyectos").load("proyectos/proyectos.html");

        // reiniciamos la sesión
        sesion.reiniciar();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de la clase
     */
    initProyectos(){

        // inicializamos las variables
        this.Id = 0;                    // clave del registro
        this.Inicio = "";               // fecha de inicio
        this.Fin = "";                  // fecha de finalización
        this.Titulo = "";               // título del proyecto
        this.Descripcion = "";          // descripción del proyecto
        this.Consentimiento = "";       // consentimiento informado
        this.Usuario = "";              // nombre del usuario
        this.Alta = "";                 // fecha de alta del proyecto
        this.proyectoCorrecto = false;  // switch de título repetido

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setInicio(fecha){
        this.Inicio = fecha;
    }
    setFin(fecha){
        this.Fin = fecha;
    }
    setTitulo(titulo){
        this.Titulo = titulo;
    }
    setDescripcion(descripcion){
        this.Descripcion = descripcion;
    }
    setConsentimiento(consentimiento){
        this.Consentimiento = consentimiento;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setAlta(fecha){
        this.Alta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir del texto en el campo título
     * busca un proyecto en la base
     */
    buscaProyecto(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // presentamos el diálogo emergente
        var contenido = "Ingrese parte del título del proyecto.<br>";
        contenido += "<form name='busca_proyecto'>";
        contenido += "<p><b>Título:</b> ";
        contenido += "<input type='text' name='texto' size='30'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Aceptar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='proyectos.grillaProyectos()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerProyectos = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    content: contenido,
                                    title: 'Buscar Proyecto',
                                    theme: 'TooltipBorder',
                                    width: 400,
                            });
        this.layerProyectos.open();

        // fijamos el foco
        document.busca_proyecto.texto.focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el click del botón buscar proyecto
     * verifica se halla ingresado un texto y carga la
     * grilla de resultados
     */
    grillaProyectos(){

        // reiniciamos sesión
        sesion.reiniciar();

        // declaración de variables
        var texto = document.getElementById("texto").value;
        var mensaje;

        // si no ingresó el texto
        if ( texto == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese un texto a buscar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("texto").focus();
            return false;

        }

        // destruimos el layer
        this.layerProyectos.destroy();

        // cargamos la grilla
        this.layerProyectos = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        title: 'Resultados de la Búsqueda',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        draggable: 'title',
                        width: 600,
                        ajax: {
                            url: 'proyectos/buscar.php?texto='+texto,
                            reload: 'strict'
                    }
                });
        this.layerProyectos.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idproyecto - clave del proyecto
     * Método llamado al pulsar sobre la grilla de resultados
     * que presenta en el formulario principal los datos
     * del proyecto y luego carga la grilla de pacientes
     */
    encuentraProyecto(idproyecto){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos la clave en la clase
        this.Id = idproyecto;

        // obtenemos los datos del registro
        $.ajax({
            url: "proyectos/getdatosproyecto.php?id=" + this.Id,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // asignamos en la grilla
                document.getElementById("titulo_proyecto").value = data.Titulo;
                document.getElementById("inicio_proyecto").value = data.Inicio;
                document.getElementById("fin_proyecto").value = data.Fin;

        }});

        // cargamos la grilla de pacientes
        pacproyectos.cargaPacientes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el layer emergente para la
     * inserción de un nuevo proyecto
     */
    nuevoProyecto(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // cargamos el formulario
        this.layerProyectos = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: false,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        title: 'Proyectos de Investigación',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        width: 800,
                        draggable: 'title',
                        ajax: {
                            url: 'proyectos/form_proyectos.html',
                            reload: 'strict'
                        }
                });
        this.layerProyectos.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que obtiene los datos del registro actual
     */
    getDatosProyecto(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "proyectos/getdatosproyecto.php?id=" + this.Id,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                proyectos.cargaDatosProyecto(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector json con el registro
     * Método que recibe como parámetro el vector json con
     * los datos del registro y los carga en las variables
     * de clase
     */
    cargaDatosProyecto(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en la clase (la clave ya está
        // asignada)
        this.setInicio(datos.Inicio);
        this.setFin(datos.Fin);
        this.setTitulo(datos.Titulo);
        this.setDescripcion(datos.Descripcion);
        this.setConsentimiento(datos.Consentimiento);
        this.setUsuario(datos.Usuario);
        this.setAlta(datos.Alta);

        // mostramos el registro
        this.muestraProyecto();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase,
     * muestra los datos del registro en el formulario
     */
    muestraProyecto(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en el formulario
        document.getElementById("proyecto_titulo").value = this.Titulo;
        document.getElementById("proyecto_inicio").value = this.Inicio;
        document.getElementById("proyecto_fin").value = this.Fin;
        document.getElementById("proyecto_alta").value = this.Alta;
        document.getElementById("proyecto_usuario").value = this.Usuario;
        CKEDITOR.instances['descripcion_proyecto'].setData(this.Descripcion);
        CKEDITOR.instances['consentimiento_proyecto'].setData(this.Descripcion);

        // fijamos el foco
        document.getElementById("proyecto_titulo").focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de pedir confirmación para eliminar
     * el registro actual
     */
    eliminaProyecto(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Proyecto',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('proyectos/puedeborrar.php?id='+this.Id,
                      function(data){

                        // si no encontró registros
                        if (data.Registros == 0){

                            // llama la rutina de eliminación
                            proyectos.borraProyecto();

                        // si encontró hijos
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "El proyecto tiene pacientes ...", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de pedir confirmación que
     * elimina el registro actual
     */
    borraProyecto(idproyecto){

        // borramos el registro
        $.ajax({
            url: "proyectos/borrar.php?id=" + this.Id,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salió bien
                if (data.Error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro eliminado ... ", color: 'green'});

                    // inicializamos las variables de clase
                    proyectos.initProyectos();

                    // limpiamos el formulario
                    proyectos.limpiaFormulario();

                // si algo salió mal
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error ... ", color: 'red'});

                }

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de la grilla de
     * proyectos llamado luego de eliminar el registro
     */
    limpiaFormulario(){

        // inicializamos los campos
        document.getElementById("titulo_proyecto").value = "";
        document.getElementById("inicio_proyecto").value = "";
        document.getElementById("fin_proyecto").value = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario antes
     * de enviarlo al servidor
     */
    verificaProyecto(){

        // reiniciamos sesion
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no indicó el título
        if (document.getElementById("proyecto_titulo").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el título del proyecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("proyecto_titulo").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Titulo = document.getElementById("proyecto_titulo").value;

        }

        // si no ingresó la fecha de inicio
        if (document.getElementById("proyecto_inicio").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de inicio del proyecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Inicio = document.getElementById("proyecto_inicio").value;

        }

        // si no ingresó la fecha de finalización
        if (document.getElementById("proyecto_fin").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de finalización del proyecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Fin = document.getElementById("proyecto_fin").value;

        }

        // si no ingresó la descripción
        this.Descripcion = CKEDITOR.instances['descripcion_proyecto'].getData();
        if (this.Descripcion == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique los objetivos del proyecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // el consentimiento lo permite en blanco pero
        // lanza una alerta
        this.Consentimiento = CKEDITOR.instances['consentimiento_proyecto'].getData();
        if (this.Consentimiento == ""){

            // presenta el alerta
            mensaje = "No hay información del consentimiento";
            new jBox('Notice', {content: mensaje, color: 'red'});

        }

        // verificamos por callback que no esté repitiendo
        // el pedido
        this.valida(function(proyectoCorrecto){

            // si está repetido
            if (!proyectoCorrecto){

                // presenta el mensaje
                mensaje = "Ya se encuentra declarado un\n";
                mensaje += "proyecto con el mismo título";
                new jBox('Notice', {content: mensaje, color: 'red'});

            }

        });

        // grabamos el registro
        this.grabaProyecto();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de verificar el formulario de
     * proyectos, envía los datos al servidor
     */
    grabaProyecto(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no validó el título
        if (!proyectoCorrecto){
            return false;
        }

        // declaración de variables
        var datosProyecto = new FormData();

        // asignamos los valores
        datosProyecto.append("Id", this.Id);
        datosProyecto.append("Titulo", this.Titulo);
        datosProyecto.append("Inicio", this.Inicio);
        datosProyecto.append("Fin", this.Fin);
        datosProyecto.append("Descripcion", this.Descripcion);
        datosProyecto.append("Consentimiento", this.Consentimiento);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "proyectos/graba_proyecto.php",
            type: "POST",
            data: datosProyecto,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // cargamos el proyecto
                    proyectos.encuentraProyecto(data.Id);

                    // cerramos el layer
                    proyectos.layerProyectos.destroy();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que simplemente
     * elimina el layer emergente
     */
    cancelaProyecto(){

        // eliminamos el layer
        this.layerProyectos.destroy();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente y presenta los
     * tags utilizados en el consentimiento informado
     */
    mostrarTags(){

        // cargamos la grilla
        layerTags = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Tags Disponibles',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    draggable: 'title',
                    width: 600,
                    ajax: {
                        url: 'proyectos/tags.html',
                        reload: 'strict'
                    }
                });
        layerTags.open();

    }

}