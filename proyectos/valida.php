<?php

/**
 *
 * valida | proyectos/valida.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una cadena con el título del proyecto
 * y verifica que no se encuentre declarado, retorna la cantidad
 * de registros encontrados
 *
*/

// incluimos e instanciamos las clases
require_once ("proyectos.class.php");
$proyecto = new Proyectos();

// obtenemos los registros
$registros = $proyecto->validaProyecto($_GET["titulo"]);

// retornamos
echo json_encode(array("Registros" => $registros));

?>