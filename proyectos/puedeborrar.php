<?php

/**
 *
 * puedeborrar | proyectos/puedeborrar.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un proyecto y verifica
 * si tiene hijos, retorna la cantidad de registros encontrados
 *
*/

// incluimos e instanciamos las clases
require_once ("proyectos.class.php");
$proyecto = new Proyectos();

// obtenemos los registros
$registros = $proyecto->puedeBorrar($_GET["id"]);

// retornamos
echo json_encode(array("Registros" => $registros));

?>