<?php

/**
 *
 * nuevopaciente | proyectos/nuevopaciente.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (1/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post la clave de un proyecto y el protocolo
 * de un paciente, ejecuta la consulta de inserción y retorna la
 * clave del registro
 *
*/

// incluimos e instanciamos las clases
require_once("pacproyectos.class.php");
$paciente = new PacProyectos();

// asignamos los valores
$paciente->setProtocolo($_POST["Paciente"]);
$paciente->setProyecto($_POST["Proyecto"]);

// ejecutamos la consulta
$id = $paciente->grabaPaciente();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>