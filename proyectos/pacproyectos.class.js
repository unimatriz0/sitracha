/*

    Nombre: pacproyectos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 13/08/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de pacientes incluidos
                 en los proyectos de investigación

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre la tabla de pacientes
 *        participantes de los proyectos de investigación
 */
class PacProyectos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.layerPacProyectos = "";
        this.initPacProyectos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initPacProyectos(){

        // inicializamos las variables
        this.Id = 0;               // clave del registro
        this.Proyecto = 0;         // clave del proyecto
        this.Protocolo = 0;        // clave del paciente
        this.Usuario = "";         // nombre del usuario
        this.Alta = "";            // fecha de alta

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setProyecto(proyecto){
        this.Proyecto = proyecto;
    }
    setProtocolo(protocolo){
        this.Protocolo = protocolo;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setAlta(fecha){
        this.Alta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de los valores del formulario de
     * proyectos (documento, nombre, etc.) presenta el layer
     * emergente con los pacientes coincidentes para ser
     * agregados al proyecto
     */
    buscaPaciente(){

        // si no hay proyecto activo
        if (proyectos.Id == 0){

            // presenta el mensaje y retorna
            var mensaje = "Seleccione un proyecto primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // reiniciamos la sesión
        sesion.reiniciar();

        // presentamos el diálogo emergente
        var contenido = "Ingrese parte del nombre del paciente, .<br>";
        contenido = "su número de documento completo o el <br>";
        contenido = "número de protocolo, el sistema presentará <br>";
        contenido = "los registros coincidentes.";
        contenido += "<form name='busca_paciente'>";
        contenido += "<p><b>Título:</b> ";
        contenido += "<input type='text' name='texto' size='30'>";
        contenido += "</p>";
        contenido += "<p align='center'>";
        contenido += "<input type='button' name='btnBuscar' ";
        contenido += "       value='Aceptar' ";
        contenido += "       class='boton_confirmar' ";
        contenido += "       onClick='proyectos.grillaPacientes()'>";
        contenido += "</p>";
        contenido += "</form>";

        // abrimos el formulario
        this.layerPacProyectos = new jBox('Modal', {
                                    animation: 'flip',
                                    closeOnEsc: true,
                                    closeOnClick: false,
                                    closeOnMouseleave: false,
                                    closeButton: true,
                                    overlay: false,
                                    repositionOnContent: true,
                                    onCloseComplete: function(){
                                        this.destroy();
                                    },
                                    draggable: 'title',
                                    content: contenido,
                                    title: 'Buscar Paciente',
                                    theme: 'TooltipBorder',
                                    width: 400,
                            });
        this.layerProyectos.open();

        // fijamos el foco
        document.busca_paciente.texto.focus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de pulsar la búsqueda de
     * pacientes que presenta la nómina de los registros
     * coincidentes y permite agregarlos al proyecto
     */
    grillaPacientes(){

        // si no ingresó texto
        if (document.busca_paciente.texto.value == ""){

            // presenta el mensaje y retorna
            var mensaje = "Ingrese algún texto a buscar";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.busca_paciente.texto.focus();
            return false;

        }

        // destruimos el layer
        this.layerPacProyectos.destroy();

        // cargamos la grilla
        this.layerPacProyectos = new jBox('Modal', {
                        animation: 'flip',
                        closeOnEsc: true,
                        closeOnClick: false,
                        closeOnMouseleave: false,
                        closeButton: true,
                        overlay: false,
                        repositionOnContent: true,
                        title: 'Resultados de la Búsqueda',
                        theme: 'TooltipBorder',
                        onCloseComplete: function(){
                            this.destroy();
                        },
                        draggable: 'title',
                        width: 600,
                        ajax: {
                            url: 'proyectos/buscarpaciente.php?texto='+texto+'&proyecto='+proyectos.Id,
                            reload: 'strict'
                    }
                });
        this.layerPacProyectos.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de la variable de clase de los
     * proyectos, carga la grilla de pacientes del
     * proyecto activo
     */
    cargaPacientes(){

        // si no hay proyecto activo
        if (proyectos.Id == 0){
            return false;
        }

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la nómina de pacientes asignamos los datos
        $.ajax({
            url: "proyectos/getpacientes.php?id=" + proyectos.Id,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // cargue en la grilla
                pacproyectos.cargaGrilla(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos vector con los pacientes
     * Método llamado luego de obtener el vector de pacientes
     * que participan del proyecto y carga la grilla de
     * los mismos
     */
    cargaGrilla(datos){

        // declaramos las variables
        var tabla = $('#grilla_pacproyectos').DataTable();
        var imprimir = "";         // enlace de impresión
        var borrar = "";           // enlace de eliminación

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // armamos el enlace de eliminación
            borrar =  "<input type='button' ";
            borrar += "        name='btnBorrar' ";
            borrar += "        class='botonborrar' ";
            borrar += "        title='Pulse para eliminar el registro' ";
            borrar += "        onClick='pacproyectos.eliminaPaciente(" + datos[i].id + ")';>";

            // armamos el enlace de impresión
            imprimir =  "<input type='button' ";
            imprimir += "        name='btnImprimir' ";
            imprimir += "        class='botonimprimir' ";
            imprimir += "        title='Pulse para imprimir el consentimiento' ";
            imprimir += "        onClick='pacproyectos.imprimeConsentimiento(" + datos[i].id + ")';>";

            // agregamos el registro
            tabla.row.add( [
                    datos[i].protocolo,
                    datos[i].paciente,
                    datos[i].tratamiento,
                    datos[i].sexo,
                    datos[i].hijos,
                    datos[i].provincia,
                    datos[i].localidad,
                    datos[i].alta,
                    datos[i].usuario,
                    imprimir,
                    borrar
                ]).draw( false );

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpaciente - clave del paciente
     * Método que recibe como parámetro la clave del paciente
     * y lo agrega a la tabla pivot, luego recarga la grilla
     * y cierra el layer emergente
    */
    nuevoPaciente(idpaciente){

        // declaración de variables
        var datosPaciente = new FormData();

        // agregamos los valores
        datosPaciente.append("Proyecto", proyectos.Id);
        datosPaciente.append("Paciente", idpaciente);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "proyectos/nuevopaciente.php",
            type: "POST",
            data: datosPaciente,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // actualizamos la grilla
                    pacproyectos.limpiaGrilla();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

   /**
    * @author Claudio Invernizzi <cinvernizzi@gmail.com>
    * @param {int} idregistro - clave del registro
    * Método llamado al pulsar el botón borrar que
    * pide confirmación antes de eliminar el registro
    */
   eliminaPaciente(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Paciente',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el paciente?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llamamos la rutina
                pacproyectos.borraPaciente(id);

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

   }

   /**
    * @author Claudio Invernizzi <cinvernizzi@gmail.com>
    * @param {int} idregistro - clave del registro
    * Método que recibe como parámetro la clave del
    * registro y ejecuta la consulta de eliminación
    * luego recarga la grilla
    */
    borraPaciente(id){

        // ejecuta la consulta de eliminación pasando también la
        // id del proyecto (porque un paciente puede participar
        // de mas de un proyecto)
        $.get('proyectos/borrapaciente.php?id='+id+'&proyecto='+proyectos.Id,
                function(data){

                // si no encontró registros
                if (data.Error != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Paciente eliminado ...", color: 'red'});

                    // recarga la grilla
                    pacproyectos.limpiaGrilla();

                // si encontró hijos
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error ...", color: 'red'});

                }

            }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idpaciente - clave del paciente
     * Método que recibe como parámetro la clave del paciente
     * y genera el documento con el consentimiento informado
     * a partir de la plantilla del proyecto
     */
    imprimeConsentimiento(idpaciente){

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de grabar o borrar un registro que
     * recarga la grilla
     */
    limpiaGrilla(){

        // limpiamos la tabla y luego recargamos
        var tabla = $('#grilla_pacproyectos').DataTable();
        tabla.clear();
        this.cargaPacientes();

    }

}