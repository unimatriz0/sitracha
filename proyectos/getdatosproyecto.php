<?php

/**
 *
 * getdatosproyecto | proyectos/getdatosproyecto.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y retorna
 * un array json con los datos del mismo
 *
*/

// incluimos e instanciamos la clase
require_once("proyectos.class.php");
$proyecto = new Proyectos();

// obtenemos el registro
$proyecto->getDatosProyecto($_GET["id"]);

// retornamos el array json
echo json_encode(array("Inicio" => $proyecto->getInicio(),
                       "Fin"    => $proyecto->getFin(),
                       "Titulo" => $proyecto->getTitulo(),
                       "Descripcion" => $proyecto->getDescripcion(),
                       "Consentimiento" => $proyecto->getConsentimiento(),
                       "Usuario"        => $proyecto->getUsuario(),
                       "Alta"           => $proyecto->getAlta()));

?>