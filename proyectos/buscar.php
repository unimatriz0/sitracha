<?php

/**
 *
 * buscar | proyectos/buscar.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una cadena de texto y presenta una
 * grilla con los registros encontrados
 *
*/

// incluimos e instanciamos las clases
require_once ("proyectos.class.php");
$proyecto = new Proyectos();

// buscamos la ocurrencia
$nomina = $proyecto->buscaProyecto($_GET["texto"]);

// si no hubo resultados
if (count($nomina) == 0){

    // presenta el mensaje
    echo "<h2>No se encontraron registros coincidentes</h2>";

// si encontró
} else {

    // define la tabla
    echo "<table width='95%' align='center' border='0'>";

    // define los títulos
    echo "<thead>";
    echo "<tr>";
    echo "<th>Título</th>";
    echo "<th>Inicio</th>";
    echo "<th>Fin</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "<thead>";

    // define el cuerpo
    echo "<tbody>";

    // recorremos el vector
    foreach ($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abre la fila
        echo "<tr>";

        // lo presentamos
        echo "<td>$titulo</td>";
        echo "<td>$inicio</td>";
        echo "<td>$fin</td>";

        // presenta el botón ver
        echo "<td>";
        echo "<input type='button'
                     name='btnVerProyecto'
                     id='btnVerProyecto'
                     class='botoneditar'
                     title='Pulse para seleccionar el proyecto'
                     onClick='proyectos.encuentraProyecto($id)'>";
        echo "</td>";

        // cierra la fila
        echo "</tr>";

    }

    // cierra el cuerpo y la tabla
    echo "</body></table>";

}
?>
