<?php

/**
 *
 * graba_proyecto | proyectos/graba_proyecto.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de un registro y ejecuta
 * la consulta en la base de datos, retorna la clave del
 * registro afectado
 *
*/

// incluimos e instanciamos la clase
require_once("proyectos.class.php");
$proyecto = new Proyectos();

// fijamos los valores
$proyecto->setId($_POST["Id"]);
$proyecto->setTitulo($_POST["Titulo"]);
$proyecto->setInicio($_POST["Inicio"]);
$proyecto->setFin($_POST["Fin"]);
$proyecto->setDescripcion($_POST["Descripcion"]);
$proyecto->setConsentimiento($_POST["Consentimiento"]);

// ejecutamos la consulta
$id = $proyecto->grabaProyecto();

// retornamos el resultado
echo json_encode(array("Id" => $id));

?>