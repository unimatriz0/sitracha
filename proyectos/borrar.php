<?php

/**
 *
 * borrar | proyectos/borrar.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un proyecto y ejecuta
 * la consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once ("proyectos.class.php");
$proyecto = new Proyectos();

// ejecutamos la consulta
$proyecto->borraProyecto($_GET["id"]);

// por ahora retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>