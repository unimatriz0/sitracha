<?php

/**
 *
 * Class Proyectos | proyectos/proyectos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla pivot de pacientes
 * participantes de los proyectos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class PacProyectos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Id;                    // clave del registro
    protected $Proyecto;              // clave del proyecto
    protected $Protocolo;             // clave del paciente
    protected $Paciente;              // nombre del paciente
    protected $TipoDocumento;         // tipo de documento del paciente
    protected $Documento;             // número de documento del paciente
    protected $Sexo;                  // sexo del paciente
    protected $EstadoCivil;           // estado civil del paciente
    protected $Hijos;                 // número de hijos del paciente
    protected $Localidad;             // nombre de la localidad de residencia
    protected $Provincia;             // nombre de la provincia de residencia
    protected $Pais;                  // país de residencia del paciente
    protected $Tratamiento;           // si recibió o no tratamiento
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $Alta;                  // fecha de alta del registro

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

        // inicializamos las variables de clase
        $this->initPacProyectos();

    }

    /**
     * Método que inicializa las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
    */
    protected function initPacProyectos(){

        $this->Id = 0;
        $this->Proyecto = 0;
        $this->Protocolo = 0;
        $this->Paciente = "";
        $this->TipoDocumento = "";
        $this->Documento = "";
        $this->Sexo = "";
        $this->EstadoCivil = "";
        $this->Hijos = 0;
        $this->Localidad = "";
        $this->Provincia = "";
        $this->Pais = "";
        $this->Tratamiento = "";
        $this->Usuario = "";
        $this->Alta = "";

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setProyecto($proyecto){
        $this->Proyecto = $proyecto;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getProyecto(){
        return $this->Proyecto;
    }
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getTipoDocumento(){
        return $this->TipoDocumento;
    }
    public function getDocumento(){
        return $this->Documento;
    }
    public function getSexo(){
        return $this->Sexo;
    }
    public function getEstadoCivil(){
        return $this->EstadoCivil;
    }
    public function getHijos(){
        return $this->Hijos;
    }
    public function getLocalidad(){
        return $this->Localidad;
    }
    public function getProvincia(){
        return $this->Provincia;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getTratamiento(){
        return $this->Tratamiento;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getAlta(){
        return $this->Alta;
    }

    /**
     * Método que recibe como parámetros la clave del paciente y la
     * clave del proyecto y verifica que no estemos ingresando un
     * paciene duplicado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param [int] $idproyecto - clave del proyecto
     * @param [int] $idpaciente - clave del paciente
     * @return [int] $registros - número de registros encontrados
     */
    public function validaPaciente($idproyecto, $idpaciente){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.pacproyectos.id) AS registros
                     FROM diagnostico.pacproyectos
                     WHERE diagnostico.pacproyectos.proyecto = '$idproyecto' AND
                           diagnostico.pacproyectos.protocolo = '$idpaciente'; ";
        $resultado = $this->Link->query($consulta);

        // retornamos los registros
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        return $registros;

    }

    /**
     * Método que recibe como parámetro la clave de un proyecto y
     * retorna el vector con la nómina de pacientes
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idproyecto - clave del proyecto
     * @return $nomina - vector de resultados
     */
    public function nominaPacientes($idproyecto){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_pacproyectos.id AS id,
                            diagnostico.v_pacproyectos.idproyecto AS idproyecto,
                            diagnostico.v_pacproyectos.protocolo AS protocolo,
                            diagnostico.v_pacproyectos.paciente AS paciente,
                            diagnostico.v_pacproyectos.tipo_documento AS tipo_documento,
                            diagnostico.v_pacproyectos.documento AS documento,
                            diagnostico.v_pacproyectos.sexo AS sexo,
                            diagnostico.v_pacproyectos.estado_civil AS estado_civil,
                            diagnostico.v_pacproyectos.hijos AS hijos,
                            diagnostico.v_pacproyectos.localidad AS localidad,
                            diagnostico.v_pacproyectos.provincia AS provincia,
                            diagnostico.v_pacproyectos.pais AS pais,
                            diagnostico.v_pacproyectos.tratamiento AS tratamiento,
                            diagnostico.v_pacproyectos.usuario AS usuario,
                            diagnostico.v_pacproyectos.alta AS alta
                     FROM diagnostico.v_pacproyectos
                     WHERE diagnostico.v_pacproyectos.idproyecto = '$idproyecto'
                     ORDER BY diagnostico.v_pacproyectos.paciente; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * Método que recibe como parámetros un texto y la clave de un
     * proyecto, busca en la base de datos de pacientes aquellos
     * que coinciden con el criterio de búsqueda pero que no
     * se encuentran agregados todavía al proyecto (utilizado
     * en las altas de pacientes)
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param [string] $texto - texto a buscar
     * @param [int] $proyecto - clave del proyecto
     * @return [array] vector de resultados
     */
    public function buscaPaciente($texto, $proyecto){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_personas.protocolo AS protocolo,
                            CONCAT(diagnostico.v_personas.nombre, ', ', diagnostico.v_personas.apellido) AS nombre,
                            diagnostico.v_personas.documento AS documento,
                            diagnostico.v_personas.edad AS edad,
                            diagnostico.v_personas.sexo AS sexo,
                            diagnostico.v_personas.localidad_residencia AS localidad,
                            diagnostico.v_personas.jurisdiccion_residencia AS jurisdiccion,
                            diagnostico.v_personas.tratamiento AS tratamiento,
                            diagnostico.v_pacproyectos.idproyecto AS idproyecto
                     FROM diagnostico.v_personas LEFT JOIN diagnostico.v_pacproyectos ON diagnostico.v_personas.protocolo = diagnostico.v_pacproyectos.protocolo
                     WHERE (diagnostico.v_pacproyectos.idproyecto != '$proyecto' OR
                            ISNULL(diagnostico.v_pacproyectos.idproyecto)) AND
                           (diagnostico.v_personas.protocolo = '$texto' OR
                            diagnostico.v_personas.documento = '$texto' OR
                            diagnostico.v_personas.apellido LIKE '%texto%' OR
                            diagnostico.v_personas.nombre LIKE '%texto%')
                     ORDER BY diagnostico.v_personas.nombre,
                              diagnostico.v_personas.apellido;";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los datos del mismo
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave del registro
     */
    public function getDatosPaciente($id){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_pacproyectos.id AS id,
                            diagnostico.v_pacproyectos.idproyecto AS idproyecto,
                            diagnostico.v_pacproyectos.protocolo AS protocolo,
                            diagnostico.v_pacproyectos.paciente AS paciente,
                            diagnostico.v_pacproyectos.tipo_documento AS tipo_documento,
                            diagnostico.v_pacproyectos.documento AS documento,
                            diagnostico.v_pacproyectos.sexo AS sexo,
                            diagnostico.v_pacproyectos.estado_civil AS estado_civil,
                            diagnostico.v_pacproyectos.hijos AS hijos,
                            diagnostico.v_pacproyectos.localidad AS localidad,
                            diagnostico.v_pacproyectos.provincia AS provincia,
                            diagnostico.v_pacproyectos.pais AS pais,
                            diagnostico.v_pacproyectos.tratamiento AS tratamiento,
                            diagnostico.v_pacproyectos.usuario AS usuario,
                            diagnostico.v_pacproyectos.alta AS alta
                     FROM diagnostico.v_pacproyectos
                     WHERE diagnostico.v_pacproyectos.id = '$id';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y asignamos en la clase
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Proyecto = $idproyecto;
        $this->Protocolo = $protocolo;
        $this->Paciente = $paciente;
        $this->TipoDocumento = $tipo_documento;
        $this->Documento = $documento;
        $this->Sexo = $sexo;
        $this->EstadoCivil = $estado_civil;
        $this->Hijos = $hijos;
        $this->Localidad = $localidad;
        $this->Provincia = $provincia;
        $this->Pais = $pais;
        $this->Tratamiento = $tratamiento;
        $this->Usuario = $usuario;
        $this->Alta = $alta;

    }

    /**
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda, retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $id - clave del registro
     */
    public function grabaPaciente(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoPaciente();
        } else {
            $this->editaPaciente();
        }

        // retornamos la clave
        return $this->Id;

    }

    /**
     * Método que inserta un nuevo paciente en el proyecto
     * actual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoPaciente(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.pacproyectos
                            (proyecto,
                             protocolo,
                             usuario)
                            VALUES
                            (:proyecto,
                             :protocolo,
                             :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":proyecto",    $this->Proyecto);
        $psInsertar->bindParam(":protocolo",   $this->Protocolo);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la id del protocolo
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de actualización
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaPaciente(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.pacproyectos SET
                            proyecto = :proyecto,
                            protocolo = :protocolo,
                            usuario = :usuario
                     WHERE diagnostico.pacproyectos.id = :id;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":proyecto",    $this->Proyecto);
        $psInsertar->bindParam(":protocolo",   $this->Protocolo);
        $psInsertar->bindParam(":usuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave del registro
     * @param $proyecto - clave del proyecto
     */
    public function borraPaciente($id, $proyecto){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.pacproyectos
                     WHERE diagnostico.pacproyectos.id = '$id' AND
                           diagnostico.pacproyectos.proyecto = '$proyecto'; ";
        $this->Link->exec($consulta);

    }

}