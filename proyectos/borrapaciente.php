<?php

/**
 *
 * borrapaciente | proyectos/borrapaciente.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (20/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un paciente y la clave
 * de un proyecto y ejecuta la consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once ("pacproyectos.class.php");
$paciente = new PacProyectos();

// ejecutamos la consulta
$paciente->borraPaciente($_GET["id"], $_GET["proyecto"]);

// retornamos siempre verdadero
echo json_encode((array("Error" => 1)));

?>