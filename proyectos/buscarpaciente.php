<?php

/**
 *
 * buscarpaciente | proyectos/buscarpaciente.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (14/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get un texto a buscar y la clave del
 * proyecto y presenta la nómina de pacientes que coinciden
 * con el texto y que no figuran en el mismo
 *
*/

// incluimos e instanciamos las clases
require_once ("pacproyectos.class.php");
$paciente = new PacProyectos();

// obtenemos los registros
$nomina = $proyecto->puedeBorrar($_GET["texto"], $_GET["proyecto"]);

// si no hubo registros
if (count($nomina) == 0){

    // presentamos el mensaje
    echo "<h2>No se han encontrado registros</h2>";

// si encontró
} else {

    // define la tabla
    echo "<table id='nomina_pacientes'>";

    // define los encabezados
    echo "<thead>";
    echo "<tr>";
    echo "<th>Protocolo</th>";
    echo "<th>Nombre</th>";
    echo "<th>Documento</th>";
    echo "<th>Trat.</th>";
    echo "<th>Edad</th>";
    echo "<th>Sexo</th>";
    echo "<th>Provincia</th>";
    echo "<th>Localidad</th>";
    echo "</tr>";
    echo "</thead>";

    // define el cuerpo de la tabla
    echo "<tbody>";

    // recorremos el vector
    foreach($nomina AS $registro){

        // obtenemos el registro
        extract($registro);

        // abrimos la fila
        echo "<tr>";

        // agregamos el registro
        echo "<td>$protocolo</td>";
        echo "<td>$nombre</td>";
        echo "<td>$documento</td>";
        echo "<td>$tratamiento</td>";
        echo "<td>$edad</td>";
        echo "<td>$sexo</td>";
        echo "<td>$jurisdiccion</td>";
        echo "<td>$localidad</td>";

        // presenta el botón agregar
        echo "<td>";
        echo "<input type='button'
                     name='btnAgregaPaciente'
                     id='btnAgregaPaciente'
                     class='botonagregar'
                     title='Agrega el paciente al proyecto'
                     onClick='pacproyectos.nuevoPaciente($protocolo)'>";
        echo "</td>";

        // cierra la fila
        echo "</tr>";

    }

    // cierra el cuerpo y la tabla
    echo "</tbody>";
    echo "</table>";

}
?>
<script>

    // instanciamos aquí la tabla
    $('#nomina_pacientes').DataTable({
        responsive: true,
        "language":{
            "url": "script/dataTables.spanish.lang"
        }});

</script>