<?php

/**
 *
 * getpacientes | proyectos/getpacientes.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un proyecto retorna
 * el vector con los pacientes participantes
 *
*/

// incluimos e instanciamos las clases
require_once ("pacproyectos.class.php");
$pacientes = new PacProyectos();

// obtenemos los registros
$registros = $pacientes->nominaPacientes($_GET["id"]);

// retornamos el vector
echo json_encode($registros);

?>