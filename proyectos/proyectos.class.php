<?php

/**
 *
 * Class Proyectos | proyectos/proyectos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Proyectos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (09/08/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de proyectos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Proyectos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $Id;                    // clave del registro
    protected $IdDepartamento;        // clave del departamento
    protected $Departamento;          // nombre del departamento
    protected $IdInstitucion;         // clave de la institucion
    protected $Institucion;           // nombre de la institucion
    protected $Inicio;                // fecha de inicio del proyecto
    protected $Fin;                   // fecha de finalización del proyecto
    protected $Titulo;                // título del proyecto
    protected $Descripcion;           // descripción del proyecto
    protected $Consentimiento;        // texto del consentimiento informado
    protected $Alta;                  // fecha de alta del proyecto
    protected $IdUsuario;             // clave del usuario del proyecto
    protected $Usuario;               // nombre del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario y del laboratorio
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdInstitucion = $_SESSION["IdLaboratorio"];
            $this->IdDepartamento = $_SESSION["IdDepartamento"];

        }

        // cerramos sesión
        session_write_close();

        // inicializamos las variables de clase
        $this->initProyecto();

    }

    /**
     * Método que inicializa las variables de clase
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function initProyecto(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Institucion = "";
        $this->Departamento = "";
        $this->Inicio = "";
        $this->Fin = "";
        $this->Titulo = "";
        $this->Descripcion = "";
        $this->Consentimiento = "";
        $this->Alta = "";
        $this->Usuario = "";

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setInicio($inicio){
        $this->Inicio = $inicio;
    }
    public function setFin($fin){
        $this->Fin = $fin;
    }
    public function setTitulo($titulo){
        $this->Titulo = $titulo;
    }
    public function setDescripcion($descripcion){
        $this->Descripcion = $descripcion;
    }
    public function setConsentimiento($consentimiento){
        $this->Consentimiento = $consentimiento;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getDepartamento(){
        return $this->Departamento;
    }
    public function getInicio(){
        return $this->Inicio;
    }
    public function getFin(){
        return $this->Fin;
    }
    public function getTitulo(){
        return $this->Titulo;
    }
    public function getDescripcion(){
        return $this->Descripcion;
    }
    public function getConsentimiento(){
        return $this->Consentimiento;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getAlta(){
        return $this->Alta;
    }

    /**
     * Método público que retorna el vector con todos los proyectos
     * que pertenecen a la institución del usuario actual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $nomina - vector con los registros
     */
    public function nominaProyectos(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_proyectos.id AS id,
                            diagnostico.v_proeyctos.titulo AS titulo,
                            diagnostico.v_proyectos.inicio AS inicio,
                            diagnostico.v_proyectos.fin AS fin,
                            diagnostico.v_proyectos.descripcion AS descripcion,
                            diagnostico.v_proyectos.alta AS alta,
                            diagnostico.v_proyectos.usuario AS usuario
                     FROM diagnostico.v_proyectos
                     WHERE diagnostico.v_proyectos.idinstitucion = '$this->IdInstitucion' AND
                           diagnostico.v_proyectos.iddepartamento = '$this->IdDepartamento'
                     ORDER BY diagnostico.v_proyectos.titulo; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * Método público que retorna el vector con los proyectos que
     * se encuentran comprendidos entre las fechas límite que
     * recibe como parámetro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $inicio - fecha inicial a reportar
     * @param $fin - fecha final a reportar
     * @return $nomina - vector con los registros
     */
    public function listaProyectos($inicio, $fin){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_proyectos.id AS id,
                            diagnostico.v_proeyctos.titulo AS titulo,
                            diagnostico.v_proyectos.inicio AS inicio,
                            diagnostico.v_proyectos.fin AS fin,
                            diagnostico.v_proyectos.descripcion AS descripcion,
                            diagnostico.v_proyectos.alta AS alta,
                            diagnostico.v_proyectos.usuario AS usuario
                     FROM diagnostico.v_proyectos
                     WHERE STR_TO_DATE(diagnostico.v_proyectos.inicio, '%d/%m/%Y') >= STR_TO_DATE($inicio, '%d/%m/%Y') AND
                           STR_TO_DATE(diagnostico.v_proyectos.fin, '%d/%m/%Y') <= STR_TO_DATE($fin, '%d/%m/%Y') AND
                           diagnostico.v_proyectos.idinstitucion = '$this->IdInstitucion' AND
                           diagnostico.v_proyectos.iddepartamento = '$this->IdDepartamento' AND
                     ORDER BY diagnostico.v_proyectos.titulo; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * Método que recibe como parámetro una cadena y retorna un vector
     * con los proyectos coincidentes en el título o la descripción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $texto - cadena a buscar
     * @return $nomina - vector de resultados
     */
    public function buscaProyecto($texto){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_proyectos.id AS id,
                            diagnostico.v_proeyctos.titulo AS titulo,
                            diagnostico.v_proyectos.inicio AS inicio,
                            diagnostico.v_proyectos.fin AS fin,
                            diagnostico.v_proyectos.descripcion AS descripcion,
                            diagnostico.v_proyectos.alta AS alta,
                            diagnostico.v_proyectos.usuario AS usuario
                     FROM diagnostico.v_proyectos
                     WHERE diagnostico.v_proyectos.titulo LIKE '%texto%' OR
                           diagnostico.v_proyectos.descripcion LIKE '%texto%' AND
                           diagnostico.v_proyectos.idinstitucion = '$this->IdInstitucion' AND
                           diagnostico.v_proyectos.iddepartamento = '$this->IdDepartamento'
                     ORDER BY diagnostico.v_proyectos.titulo; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * Método público que recibe como parámetro la clave de un
     * registro y asigna en las variables de clase los valores
     * del registro
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave del registro
     */
    public function getDatosProyecto($id){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_proyectos.id AS id,
                            diagnostico.v_proeyctos.inicio AS inicio,
                            diagnostico.v_proyectos.fin AS fin,
                            diagnostico.v_proyectos.titulo AS titulo,
                            diagnostico.v_proyectos.descripcion AS descripcion,
                            diagnostico.v_proyectos.consentimiento AS consentimiento,
                            diagnostico.v_proyectos.alta AS alta,
                            diagnostico.v_proyectos.usuario AS usuario
                     FROM diagnostico.v_proyectos
                     WHERE diagnostico.v_proyectos.id = '$id';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro y lo asignamos en la clase
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);
        $this->Id = $id;
        $this->Inicio = $inicio;
        $this->Fin = $fin;
        $this->Titulo = $titulo;
        $this->Descripcion = $descripcion;
        $this->Consentimiento = $consentimiento;
        $this->Alta = $alta;
        $this->Usuario = $usuario;

    }

    /**
     * Método público que ejecuta la consulta de edición o inserción
     * según corresponda, retorna la id del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return $id - clave del registro
     */
    public function grabaProyecto(){

        // si está insertando
        if ($this->Id = 0){
            $this->nuevoProyecto();
        } else {
            $this->editaProyecto();
        }

        // retornamos la clave
        return $this->Id;

    }

    /**
     * Método proyegido que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoProyecto(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.proyectos
                            (institucion,
                             departamento,
                             inicio,
                             fin,
                             titulo,
                             descripcion,
                             consentimiento,
                             usuario)
                            VALUES
                            (:institucion,
                             :departamento,
                             STR_TO_DATE(:inicio, '%d/%m/%Y'),
                             STR_TO_DATE(:fin, '%d/%m/%Y'),
                             :titulo,
                             :descripcion,
                             :consentimiento,
                             :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":institucion",    $this->Institucion);
        $psInsertar->bindParam(":departamento",   $this->Departamento);
        $psInsertar->bindParam(":inicio",         $this->Inicio);
        $psInsertar->bindParam(":fin",            $this->Fin);
        $psInsertar->bindParam(":titulo",         $this->Titulo);
        $psInsertar->bindParam(":descripcion",    $this->Descripcion);
        $psInsertar->bindParam(":consentimiento", $this->Consentimiento);
        $psInsertar->bindParam(":usuario",        $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la id del protocolo
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaProyecto(){

        // componemos la consulta (no actualizamos ni la instituciòn
        // ni el departamento)
        $consulta = "UPDATE diagnostico.proyectos SET
                            inicio = STR_TO_DATE(:inicio, '%d/%m/%Y'),
                            fin = STR_TO_DATE(:fin, '%d/%m/%Y'),
                            titulo = :titulo,
                            descripcion = :descripcion,
                            consentimiento = :consentimiento,
                            usuario = :usuario
                     WHERE diagnostico.proyectos.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":inicio",         $this->Inicio);
        $psInsertar->bindParam(":fin",            $this->Fin);
        $psInsertar->bindParam(":titulo",         $this->Titulo);
        $psInsertar->bindParam(":descripcion",    $this->Descripcion);
        $psInsertar->bindParam(":consentimiento", $this->Consentimiento);
        $psInsertar->bindParam(":usuario",        $this->IdUsuario);
        $psInsertar->bindParam(":id",             $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método público que recibe como parámetro el título de
     * un proyecto y verifica que no se encuentre repetido,
     * retorna la cantidad de registros encontrados
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $titulo - titulo del proyecto
     * @return $registros - número de registros
     */
    public function validaProyecto($titulo){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.proyectos.id) AS registros
                     FROM diagnostico.proyectos
                     WHERE diagnostico.proyectos.titulo = '$titulo'; ";

        // ejecutamos la consulta y obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // retornamos los registros encontrados
        return $registros;

    }

    /**
     * Método público que recibe como parámetro la clave de un
     * proyecto y verifica que este no tenta hijos (para poder
     * ser eliminado)
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave del registro
     * @return $registros - los hijos encontrados
     */
    public function puedeBorrar($id){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.pacproyectos.id) AS registros
                     FROM diagnostico.pacproyectos
                     WHERE diagnostico.pacproyectos.proyecto = '$id'; ";

        // ejecutamos la consulta y obtenemos el registro
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // retornamos los registros encontrados
        return $registros;

    }

    /**
     * Método público que recibe como parámetro la clave de
     * un registro y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $id - clave del registro
     */
    public function borraProyecto($id){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.proyectos
                     WHERE diagnostico.proyectos.id = '$id';";
        $this->Link->exec($consulta);

    }

}