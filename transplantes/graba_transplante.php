<?php

/**
 *
 * transplantes/graba_transplante.php
 *
 * @package     Diagnostico
 * @subpackage  Transplantes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y
 * ejecuta la consulta de inserción en el servidor
*/

// incluimos e instanciamos las clases
require_once("transplantes.class.php");
$transplantes = new Transplantes();

// fijamos los valores
$transplantes->setProtocolo($_POST["Protocolo"]);
$transplantes->setIdOrgano($_POST["Organo"]);
$transplantes->setPositivo($_POST["Positivo"]);
$transplantes->setFechaTransplante($_POST["Fecha"]);

// grabamos el registro
$id = $transplantes->grabaTransplante();

// retornamsos el resultado de la operación
echo json_encode(array("Id" => $id));

?>