<?php

/**
 *
 * transplantes/borra_transplante.php
 *
 * @package     Diagnostico
 * @subpackage  Transplantes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/07/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
 * Método que recibe por get la clave de un registro y ejecuta la
 * consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once ("transplantes.class.php");
$transplante = new Transplantes();

// ejecutamos la consulta
$transplante->borraTransplante($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>