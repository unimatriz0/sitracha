<?php

/**
 *
 * Class Transplantes | transplantes/transplantes.class.php
 *
 * @package     Diagnostico
 * @subpackage  Transplantes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (21/03/2018)
 * @copyright   Copyright (c) 2017, INP/
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Esta clase controla las operaciones sobre la tabla de
 * transplantes realizados en los pacientes
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Transplantes{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Link;                  // puntero a la base de datos
    protected $IdTransplante;         // clave del registro
    protected $Protocolo;             // protocolo del paciente
    protected $Organo;                // nombre del órgano
    protected $IdOrgano;              // clave del órgano
    protected $Positivo;              // organo positivo para chagas
    protected $FechaTransplante;      // fecha del transplante
    protected $Usuario;               // nombre del usuario
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdTransplante = 0;
        $this->Protocolo = 0;
        $this->Organo = "";
        $this->IdOrgano = 0;
        $this->Positivo = "";
        $this->FechaTransplante = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de inicialización de variables
    public function setIdTransplante($idtransplante){
        $this->IdTransplante = $idtransplante;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setIdOrgano($idorgano){
        $this->IdOrgano = $idorgano;
    }
    public function setPositivo($positivo){
        $this->Positivo = $positivo;
    }
    public function setFechaTransplante($fecha){
        $this->FechaTransplante = $fecha;
    }

    // métodos de retorno de valores
    public function getIdTransplante(){
        return $this->IdTransplante;
    }
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getOrgano(){
        return $this->Organo;
    }
    public function getIdOrgano(){
        return $this->IdOrgano;
    }
    public function getPositivo(){
        return $this->Positivo;
    }
    public function getFechaTransplante(){
        return $this->FechaTransplante;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * Método que recibe como parámetro la clave del paciente y retorna un
     * array con la nómina de transplantes recibidos por el paciente
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $protocolo - clave del protocolo del paciente
     * @return array
     */
    public function nominaTransplantes($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_transplantes.id_transplante AS id_transplante,
                            diagnostico.v_transplantes.protocolo AS protocolo,
                            diagnostico.v_transplantes.id_organo AS id_organo,
                            diagnostico.v_transplantes.organo AS organo,
                            diagnostico.v_transplantes.positivo AS positivo,
                            diagnostico.v_transplantes.fecha_transplante AS fecha_transplante,
                            diagnostico.v_transplantes.usuario AS usuario,
                            diagnostico.v_transplantes.laboratorio AS laboratorio,
                            diagnostico.v_transplantes.id_laboratorio AS id_laboratorio,
                            diagnostico.v_transplantes.fecha_alta AS fecha_alta
                     FROM diagnostico.v_transplantes
                     WHERE diagnostico.v_transplantes.protocolo = '$protocolo';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * Método que ejecuta la consulta de inserción o edición
     * retorna la clave del registro afectado
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function grabaTransplante(){

        // si está insertando
        if ($this->IdTransplante == 0){
            $this->nuevoTransplante();
        } else {
            $this->editaTransplante();
        }

        // retornamos la clave
        return $this->IdTransplante;

    }

    /**
     * Método protegido que ejecuta la consulta de inserción
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoTransplante(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.transplantes
                            (id_protocolo,
                             organo,
                             positivo,
                             fecha_transplante,
                             id_usuario)
                            VALUES
                            (:id_protocolo,
                             :id_organo,
                             :positivo,
                             STR_TO_DATE(:fecha_transplante, '%d/%m/%Y'),
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_organo", $this->IdOrgano);
        $psInsertar->bindParam(":positivo", $this->Positivo);
        $psInsertar->bindParam(":fecha_transplante", $this->FechaTransplante);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        $this->IdTransplante = $this->Link->lastInsertId();

    }

    /**
     * Método protegido que ejecuta la consulta de edición
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaTransplante(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.transplantes SET
                            id_protocolo = :id_protocolo,
                            organo = :id_organo,
                            positivo = :positivo,
                            fecha_transplante = STR_TO_DATE(:fecha_transplante, '%d/%m/%Y'),
                            id_usuario = :id_usuario
                     WHERE diagnostico.transplantes.id = :id_transplante;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":id_protocolo", $this->Protocolo);
        $psInsertar->bindParam(":id_organo", $this->IdOrgano);
        $psInsertar->bindParam(":positivo", $this->Positivo);
        $psInsertar->bindParam(":fecha_transplante", $this->FechaTransplante);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_transplante", $this->IdTransplante);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método público que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idtransplante - clave del registro
     */
    public function borraTransplante($idtransplante){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.transplantes WHERE id = '$idtransplante';";
        $this->Link->query($consulta);

    }

}