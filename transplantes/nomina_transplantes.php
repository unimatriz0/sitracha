<?php

/**
 *
 * transplantes/nomina_transplantes.php
 *
 * @package     Diagnostico
 * @subpackage  Transplantes
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Metodo que retorna un array json con los transplantes 
 * recibidos por el paciente
 * 
*/

// incluimos e instanciamos las clases
require_once("transplantes.class.php");
$transplantes = new Transplantes();

// obtenemos los transplantes
$nomina = $transplantes->nominaTransplantes($_GET["id"]);

// retornamos el vector
echo json_encode($nomina);

?>