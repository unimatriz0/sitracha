/*

    Nombre: transplantes.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 21/03/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones sobre los transplantes
                 de los pacientes, utiliza como tabla auxiliar el
                 diccionario de órganos

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de transplantes
 */
class Transplantes {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initTransplantes();

        // el layer emergente
        this.layerTransplantes = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initTransplantes(){

        // inicializamos las variables de clase
        this.IdTransplante = 0;            // clave del registro
        this.Protocolo = 0;                // protocolo del paciente
        this.Organo = 0;                   // clave del órgano
        this.Positivo = "";                // órgano positivo para chagas
        this.FechaTransplante = "";        // fecha de realización
        this.Usuario = "";                 // nombre del usuario
        this.FechaAlta = "";               // fecha de alta del registro

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta la grilla con los transplantes del paciente activo
     */
    grillaTransplantes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // verifica que exista un paciente activo
        if (document.getElementById("protocolo_paciente").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe tener en pantalla un paciente antes<br>";
            mensaje += "de declarar los transplantes";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // seteamos la variable control
        pacientes.setTransplantes();

        // cargamos el formulario en una ventana emergente
        this.layerTransplantes = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    repositionOnContent: true,
                    overlay: false,
                    title: 'Transplantes del Paciente',
                    draggable: 'title',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                    this.destroy();
                    },
                    ajax: {
                        url: 'transplantes/grilla_transplantes.html',
                        reload: 'strict'
                    }
      });
      this.layerTransplantes.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir del protocolo del paciente obtiene la
     * nómina de transplantes del mismo
     */
    cargaGrilla(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos la nómina
        $.get('transplantes/nomina_transplantes.php?id=' + document.getElementById("protocolo_paciente").value,
        function(data){

            // cargamos la grilla
            transplantes.nominaTransplantes(data);

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} vector con los datos de los transplantes
     * Método que recibe como parámetro los datos de los
     * transplantes y carga la tabla con los mismos
     */
    nominaTransplantes(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // inicializamos las variables
        var texto = "";

        // limpia el cuerpo de la tabla
        $("#cuerpo_transplantes").html('');

        // recorre el vector de resultados
        for(var i=0; i < datos.length; i++){

            // abrimos la fila
            texto = "<tr>";

            // agregamos el órgano
            texto += "<td>";
            texto += datos[i].organo;
            texto += "</td>";

            // agregamos si era positivo
            texto += "<td>";
            texto += datos[i].positivo;
            texto += "</td>";

            // agregamos la fecha
            texto += "<td>";
            texto += datos[i].fecha_transplante;
            texto += "</td>";

            // agregamos el usuario
            texto += "<td>";
            texto += datos[i].usuario;
            texto += "</td>";

            // agregamos la fecha de alta
            texto += "<td>";
            texto += datos[i].fecha_alta;
            texto += "</td>";

            // agrega el botón eliminar
            texto += "<td>";
            texto += "<input type='button' ";
            texto += "       name='btnEliminaTransplante' ";
            texto += "       id='btnEliminaTransplante' ";
            texto += "       title='Pulse para eliminar el registro' ";
            texto += "       class='botonborrar' ";
            texto += "       onClick='transplantes.borraTransplante(" + datos[i].id_transplante + ")'>";
            texto += "</td>";

            // cerramos la fila
            texto += "</tr>";

            // agregamos la fila a la tabla
            $("#cuerpo_transplantes").append(texto);

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de alta de un nuevo
     * transplante antes de enviarlo al servidor
     */
    verificaTransplante(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si no seleccionó el órgano
        if (document.getElementById("organo_transplante").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el órgano transplantado";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("organo_transplante").focus();
            return false;

        // si seleccionó
        } else {

            // asigna en la variable de clase
            this.Organo = document.getElementById("organo_transplante").value;

        }

        // si no indicó si el órgano era positivo
        if (document.getElementById("organo_positivo").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique si el órgano era positivo para Chagas";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("organo_positivo").focus();
            return false;

        // si indicó
        } else {

            // asigna en la variable de clase
            this.Positivo = document.getElementById("organo_positivo").value;

        }

        // si no ingresó la fecha del transplante
        if (document.getElementById("fecha_transplante").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha del transplante";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_transplante").focus();
            return false;

        // si asignó
        } else {

            // asigna en la variable de clase
            this.FechaTransplante = document.getElementById("fecha_transplante").value;

        }

        // grabamos el registro
        this.grabaTransplantes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase envía la
     * consulta de actualización al servidor
     */
    grabaTransplantes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosTransplante = new FormData();

        // obtenemos el protocolo
        var protocolo = document.getElementById("protocolo_paciente").value;

        // asignamos los valores
        datosTransplante.append("Protocolo", protocolo);
        datosTransplante.append("Organo", this.Organo);
        datosTransplante.append("Positivo", this.Positivo);
        datosTransplante.append("Fecha", this.FechaTransplante);

        // enviamos el formulario por ajax
        $.ajax({
            type: "POST",
            url: "transplantes/graba_transplante.php",
            data: datosTransplante,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                // si grabó
                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // limpiamos el formulario
                    transplantes.limpiaTransplantes();

                    // recargamos la grilla
                    transplantes.cargaGrilla();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idtransplante del registro
     * Método que recibe como parámetro la clave de un registro
     * pide confirmación antes de llamar la consulta de eliminación
     */
    borraTransplante(idtransplante){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Transplante',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el órgano?',
            theme: 'TooltipBorder',
            confirm: function() {

                 // llama la rutina php
                 $.get('transplantes/borra_transplante.php?id='+idtransplante,
                     function(data){

                         // si retornó error
                         if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // limpiamos el formulario
                            transplantes.limpiaTransplantes();

                            // recargamos la grilla
                            transplantes.cargaGrilla();

                         // si no pudo eliminar
                         } else {

                             // presenta el mensaje
                             new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                         }

                     }, "json");

             },
             cancel: function(){
                 Confirmacion.destroy();
             },
             confirmButton: 'Aceptar',
             cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario luego de grabar o eliminar
     */
    limpiaTransplantes(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos los elementos del formulario
        document.getElementById("organo_transplante").value = 0;
        document.getElementById("organo_positivo").value = "";
        document.getElementById("fecha_transplante").value = "";
        document.getElementById("usuario_transplante").value = sessionStorage.getItem("Usuario");
        document.getElementById("alta_transplante").value = fechaActual();

    }

}