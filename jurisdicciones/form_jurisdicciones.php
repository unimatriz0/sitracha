<?php

/**
 *
 * jurisdicciones/form_jurisdicciones.php
 *
 * @package     Diagnostico
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario de edición de jurisdicciones
 * 
*/

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["Usuario"])){
    $usuario_sesion = $_SESSION["Usuario"];
}
session_write_close();

// incluimos e instanciamos las clases
require_once ("Jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// obtenemos la nómina
$lista_provincias = $jurisdicciones->listaProvincias($_GET["idpais"]);

// definimos la tabla
echo "<table width='70%' align='center' border='0'>";

// definimos el encabezado
echo "<thead>";
echo "<tr>";
echo "<th align='left'>COD.</th>";
echo "<th align='left'>Jurisdiccion</th>";
echo "<th>Población</th>";
echo "<th>Usuario</th>";
echo "<th>Alta</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($lista_provincias AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro

    // la clave indec
    echo "<td>";
    echo "<span class='tooltip'
                title='Clave de la Jurisdicción'>";
    echo "<input type='text' name='codprov'
           id='codprov_$idprovincia'
           size='5'
           value='$idprovincia'>";
    echo "</span>";
    echo "</td>";

    // el nombre de la jurisdicción
    echo "<td>";
    echo "<span class='tooltip'
                title='Nombre de la Jurisdicción'>";
    echo "<input type='text' name='provincia'
           id='provincia_$idprovincia'
           size='25'
           value='$nombreprovincia'>";
    echo "</span>";
    echo "</td>";

    // la población
    echo "<td align='center'>";
    echo "<span class='tooltip'
                title='Número de habitantes'>";
    echo "<input type='number' name='poblacion'
           id='poblacion_$idprovincia'
           size='10'
           class='numerogrande'
           value='$poblacion'>";
    echo "</span>";
    echo "</td>";

    // el resto de los datos
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // el enlace de edición
    echo "<td align='center'>";
    echo "<input type='button' name='btnGrabaProvincia'
           id='btnGrabaProvincia'
           title='Pulse para grabar el registro'
           onClick='provincias.verificaJurisdiccion(" . chr(34) . $idprovincia . chr(34) . ")'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "</tr>";

}

// abrimos la fila para el nuevo registro
echo "<tr>";

// presentamos el registro

// la clave indec
echo "<td>";
echo "<span class='tooltip'
            title='Clave de la Jurisdicción'>";
echo "<input type='text' name='codprov'
       id='codprov_nueva'
       size='5'>";
echo "</span>";
echo "</td>";

// el nombre de la provincia
echo "<td>";
echo "<span class='tooltip'
            title='Nombre de la Jurisdicción'>";
echo "<input type='text' name='provincia'
       id='provincia_nueva'
       size='25'>";
echo "</span>";
echo "</td>";

// la población
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Número de habitantes'>";
echo "<input type='number' name='poblacion'
       id='poblacion_nueva'
       class='numerogrande'
       size='10'>";
echo "</span>";
echo "</td>";

// obtenemos la fecha de alta
$fecha_alta = date('d/m/Y');

// el resto de los datos
echo "<td align='center'>$usuario_sesion</td>";
echo "<td align='center'>$fecha_alta</td>";

// el enlace de edición
echo "<td align='center'>";
echo "<input type='button' name='btnGrabaProvincia'
       id='btnGrabaProvincia'
       onClick='provincias.verificaJurisdiccion()'
       title='Graba el registro en la base'
       class='botonagregar'>";
echo "</td>";

// cerramos la fila
echo "</tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>
<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>
