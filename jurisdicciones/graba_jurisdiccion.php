<?php

/**
 *
 * jurisdicciones/graba_jurisdiccion.php
 *
 * @package     Diagnostico
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario de 
 * jurisdicciones y ejecuta la consulta en el servidor, 
 * retorna la id del registro afectado (o cero en caso de error)
 * 
*/

// incluimos e instanciamos las clases
require_once ("Jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// asigna los valores
$jurisdicciones->setIdPais($_POST["IdPais"]);
$jurisdicciones->setIdProvincia($_POST["CodProv"]);
$jurisdicciones->setNombreProvincia($_POST["Provincia"]);
$jurisdicciones->setPoblacionProvincia($_POST["Poblacion"]);

// graba el registro y obtiene la clave
$resultado = $jurisdicciones->grabaProvincia($_POST["evento"]);

// retorna el resultado
echo json_encode(array("error" => $resultado));

?>