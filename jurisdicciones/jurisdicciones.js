/*

 * Nombre: jurisdicciones.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 10/01/2017
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de la base de datos de
 *              jurisdicciones
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones sobre las jurisdicciones
 */
class Jurisdicciones {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor (){

        // inicializamos las variables
        this.initJurisdicciones();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initJurisdicciones(){

        // declaración de variables
        this.Id = 0;
        this.CodProv = "";
        this.Provincia = "";
        this.Poblacion = 0;
        this.Pais = "";
        this.IdPais = 0;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en contenido el formulario de jurisdicciones
     */
    muestraJurisdicciones(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("jurisdicciones/form_jurisdicciones.html");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave de la jurisdicción
     * @return {boolean} - resultado de la operación
     * Método que verifica el formulario de jurisdicciones
     */
    verificaJurisdiccion(id){

        // declaración de variables
        var mensaje;

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no recibió id
        if (typeof(id) == "undefined"){

            // está dando un alta
            id = 'nueva';

        }

        // asigna
        this.Id = id;

        // obtenemos los valores
        this.CodProv = document.getElementById("codprov_" + id).value;
        this.Provincia = document.getElementById("provincia_" + id).value;
        this.Poblacion = document.getElementById("poblacion_" + id).value;
        this.IdPais = document.getElementById("lista_paises").value;

        // si no ingresó el código de localidad
        if (this.CodProv == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el Código de Georreferenciación";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("codprov_" + id).focus();
            return false;

        }

        // verificamos si ingresó provincia
        if (this.Provincia == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la Jurisdicción";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("provincia_" + id).focus();
            return false;

        }

        // verificamos si ingresó la población
        if (this.Poblacion == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el número de habitantes";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("poblacion_" + id).focus();
            return false;

        }

        // si está insertando
        if (this.Id == "nueva"){

            // si está repetida
            if (this.validaJurisdiccion()){

                // presenta el mensaje y retorna
                mensaje = "Esa Jurisdicción ya está declarada";
                new jBox('Notice', {content: mensaje, color: 'red'});
                return false;

            }

        }

        // llama la rutina de grabado
        this.grabaJurisdiccion();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} - resultado de la operación
     * Método que después de verificado el formulario, arma el objeto
     * formdata y lo envía por ajax al servidor
     */
    grabaJurisdiccion(){

        // implementa el formulario usando objetos html5
        var formulario = new FormData();

        // si está editando
        if (this.Id === "nueva"){
            formulario.append("evento", "insertar");
        } else {
            formulario.append("evento", "editar");
        }

        // agrega la marca y la técnica
        formulario.append("IdPais", this.IdPais);
        formulario.append("CodProv", this.CodProv);
        formulario.append("Provincia", this.Provincia);
        formulario.append("Poblacion", this.Poblacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "jurisdicciones/graba_jurisdiccion.php",
            type: "POST",
            data: formulario,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro Grabado", color: 'green'});

                    // reiniciamos las variables de clase
                    provincias.initJurisdicciones();

                    // recarga el formulario para reflejar los cambios
                    provincias.formJurisdicciones();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return {boolean} - resultado de la operación
     * Método que en las altas verifica que la provincia no se encuentre
     * repetida
     */
    validaJurisdiccion(){

        // declaración de variables
        var resultado;

        // llamamos la rutina php
        $.ajax({
            url: 'jurisdicciones/verifica_jurisdiccion.php?provincia='+this.Provincia,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            async: false,
            success: function(data) {

                // si retornó verdadero
                if (data.error === true){
                    resultado = true;
                } else {
                    resultado = false;
                }

            }});

        // retornamos
        return resultado;

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado por el formulario de abm de jurisdicciones que actualiza
     * la nómina de países
     */
    cargaPaises(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llamamos la clase javascript
        paises.listaPaises("lista_paises");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado en el evento onchange de la nómina de países del formulario
     * de ABM de jurisdicciones, determina el valor y carga en el div correspondiente
     * la nómina de jurisdicciones del país seleccionado
     */
    formJurisdicciones(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos el país seleccionado
        var idpais = document.getElementById("lista_paises").value;

        // si no seleccionó ninguno
        if (idpais == 0){
            return;
        }

        // cargamos en el div la nómina
        // carga el formulario
        $("#nominajurisdicciones").load("jurisdicciones/form_jurisdicciones.php?idpais="+idpais);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idpais - clave del valor preseleccionado
     * @param {string} idjurisdiccion (puede ser nulo)
     * Método que recibe como parámetro la id de un elemento del formulario,
     * la clave del país y (optativo) la clave de la jurisdicción preseleccionada
     * carga en el select recibido como parámetro la nómina de jurisdicciones
     * de ese país y en todo caso preselecciona uno
     */
    listaJurisdicciones(idelemento, idpais, idjurisdiccion){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // si hay algún país seleccionado
        if (idpais != 0){

            // lo llamamos asincrónico
            $.ajax({
                url: "jurisdicciones/lista_provincias.php?Pais="+idpais,
                type: "GET",
                cahe: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    // recorre el vector de resultados
                    for(var i=0; i < data.length; i++){

                        // si recibió la clave de la jurisdicción
                        if (typeof(idjurisdiccion) != "undefined"){

                            // si coincide
                            if (data[i].Id == idjurisdiccion){

                                // lo agrega seleccionado
                                $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Provincia + "</option>");

                            // si no coinciden
                            } else {

                                // simplemente lo agrega
                                $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Provincia + "</option>");

                            }

                        // si no recibió jurisdicción
                        } else {
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Provincia + "</option>");
                        }

                    }

            }});

        }

    }

}
