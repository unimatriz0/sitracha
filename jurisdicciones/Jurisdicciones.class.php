<?php

/**
 *
 * jurisdicciones/jurisdicciones.class.php
 *
 * @package     Diagnostico
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones sobre la tabla de jurisdicciones
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Jurisdicciones{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $NombrePais;            // nombre del país
    protected $IdPais;                // clave del país
    protected $IdProvincia;           // clave indec de la provincia
    protected $NombreProvincia;       // nombre de la provincia
    protected $PoblacionProvincia;    // población de la provincia
    protected $FechaAlta;             // fecha de alta del registro
    protected $Link;                  // puntero a la base de datos

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->NombrePais = "";
        $this->IdPais = 0;
        $this->IdProvincia = "";
        $this->NombreProvincia = "";
        $this->PoblacionProvincia = 0;

        // inicializamos la fecha de alta en formato mysql
        $this->FechaAlta = date('Y/m/d');

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setNombrePais($pais){
        $this->NombrePais = $pais;
    }
    public function setIdPais($idpais){
        $this->IdPais = $idpais;
    }
    public function setNombreProvincia($provincia){
        $this->NombreProvincia = $provincia;
    }
    public function setIdProvincia($idprovincia){
        $this->IdProvincia = $idprovincia;
    }
    public function setPoblacionProvincia($poblacion){

        // verifica si es un número
        if (!is_numeric($poblacion)){

            // abandona por error
            echo "La población de la Jurisdicción debe ser un número";
            exit;

            // si es correcto
        } else {

            // lo asigna
            $this->PoblacionProvincia = $poblacion;

        }

    }

    /**
     * Método que retorna un vector con la nómina de provincias y sus
     * claves, recibe como parámetro la clave del país
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $pais - clave del país
     * @return array
     */
    public function listaProvincias($pais){

        // componemos la consulta
        $consulta = "SELECT diccionarios.provincias.COD_PROV AS idprovincia,
                            diccionarios.provincias.NOM_PROV AS nombreprovincia,
                            diccionarios.paises.nombre AS nombrepais,
                            diccionarios.provincias.POBLACION AS poblacion,
                            cce.responsables.USUARIO AS usuario,
                            DATE_FORMAT(diccionarios.provincias.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta
                     FROM diccionarios.provincias INNER JOIN cce.responsables ON diccionarios.provincias.USUARIO = cce.responsables.ID
                                                  INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.ID
                     WHERE diccionarios.provincias.NOM_PROV != 'Indeterminado' AND
                           diccionarios.paises.id = '$pais'
                     ORDER BY diccionarios.provincias.NOM_PROV;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $nominaProvincias = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $nominaProvincias;

    }

    /**
     * Método que recibe como parámetro el nombre de una jurisdicción y
     * la busca en la base, retorna verdadero si existe o falso en caso
     * contrario, usada para evitar registros duplicados en el abm
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $provincia - nombre de la provincia
     * @return boolean
     */
    public function existeJurisdiccion($provincia){

        // inicializamos las variables
        $registros = 0;

        // compone y ejecuta la consulta
        $consulta = "SELECT COUNT(diccionarios.provincias.COD_PROV) AS registros
                     FROM diccionarios.provincias
                     WHERE diccionarios.provincias.NOM_PROV = '$provincia';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si encontró
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método que ejecuta la consulta de actualización de las jurisdicciones
     * y retorna la id del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $evento - acción a ejecutar
     * @return int - clave del registro insertado / editado
     */
    public function grabaProvincia($evento){

        // si está insertando
        if ($evento == "insertar"){

            // insertamos el registro
            $resultado = $this->nuevaProvincia();

            // si está editando
        } else {

            // editamos el registro
            $resultado = $this->editaProvincia();

        }

        // retorna el resultado de la operación
        return $resultado;

    }

    /**
     * Método protegido que graba un nuevo registro de provincia
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean - resultado de la operación
     */
    protected function nuevaProvincia(){

        // crea la consulta de inserción
        $consulta = "INSERT INTO diccionarios.provincias
                               (PAIS,
                                NOM_PROV,
                                COD_PROV,
                                POBLACION,
                                USUARIO,
                                FECHA_ALTA)
                               VALUES
                               (:pais,
                                :provincia,
                                :cod_prov,
                                :poblacion,
                                :usuario,
                                :fecha_alta);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asigna los valores
        $psInsertar->bindParam(":pais", $this->IdPais);
        $psInsertar->bindParam(":provincia", $this->NombreProvincia);
        $psInsertar->bindParam(":cod_prov", $this->IdProvincia);
        $psInsertar->bindParam(":poblacion", $this->PoblacionProvincia);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);

        // ejecuta la consulta
        $resultado = $psInsertar->execute();

        // retornamos el número de registros
        return $resultado;

    }

    /**
     * Método protegido que edita el registro de la provincia
     * retorna resultado de la operación
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean - resultado de la operación
     */
    protected function editaProvincia(){

        // crea la consulta de edición
        $consulta = "UPDATE diccionarios.provincias SET
                            PAIS = :idpais,
                            NOM_PROV = :provincia,
                            POBLACION = :poblacion,
                            USUARIO = :usuario,
                            FECHA_ALTA = :fecha_alta
                     WHERE diccionarios.provincias.COD_PROV = :cod_prov;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asigna los valores
        $psInsertar->bindParam(":idpais", $this->IdPais);
        $psInsertar->bindParam(":provincia", $this->NombreProvincia);
        $psInsertar->bindParam(":poblacion", $this->PoblacionProvincia);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);
        $psInsertar->bindParam(":fecha_alta", $this->FechaAlta);
        $psInsertar->bindParam(":cod_prov", $this->IdProvincia);

        // ejecuta la consulta
        $resultado = $psInsertar->execute();

        // retorna el número de registros
        return $resultado;

    }

}

?>