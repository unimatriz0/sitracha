<?php

/**
 *
 * jurisdicciones/verifica_jurisdiccio.php
 *
 * @package     Diagnostico
 * @subpackage  Jurisdicciones
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/01/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el nombre de una jurisdicción y 
 * verifica si esta existe, retorna el número de registros 
 * encontrados, utilizado en el alta para evitar la 
 * repetición 
 * 
*/

// incluimos e instanciamos las clases
require_once ("Jurisdicciones.class.php");
$jurisdicciones = new Jurisdicciones();

// verificamos si existe
$resultado = $jurisdicciones->existeJurisdiccion($_GET["provincia"]);

// retornamos el resultado de la operación
echo json_encode(array("error" => $resultado));

?>