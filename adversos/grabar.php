<?php

/**
 *
 * grabar | adversos/grabar.php
 *
 * @package     Diagnostico
 * @subpackage  Adversos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta de actualización en la base
*/

// incluimos e instanciamos las clases
require_once("adversos.class.php");
$adverso = new Adversos();

// fijamos las variables
$adverso->setId($_POST["Id"]);
$adverso->setDescripcion($_POST["Descripcion"]);

// ejecutamos la consulta
$adverso->grabaAdverso();

?>