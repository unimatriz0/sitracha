<?php

/**
 *
 * nominaadversos | adversos/nominaadversos.php
 *
 * @package     Diagnostico
 * @subpackage  Adversos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna el array json con la nómina de efectos
 * adversos utilizado para cargar los combos
*/

// incluimos e instanciamos las clases
require_once("adversos.class.php");
$adversos = new Adversos();

// ejecutamos la consulta
$nomina = $adversos->nominaAdversos();

// encodeamos el array y retornamos
echo json_encode($nomina);

?>