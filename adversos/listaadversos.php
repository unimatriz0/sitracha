<?php

/**
 *
 * listaadversos | adversos/listaadversos.php
 *
 * @package     Diagnostico
 * @subpackage  Adversos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta la grilla con los tipos efectos adversos
 * del tratamiento y permite el abm de ellos
*/

// incluimos e instanciamos las clases
require_once("adversos.class.php");
$adversos = new Adversos();

// obtenemos la matriz
$nomina = $adversos->nominaAdversos();

// presentamos el título
echo "<h2>Efectos Adversos</h2>";

// definimos la tabla
echo "<table id='adversos'
             whidth='60%'
             align='center'
             border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Efecto</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abre la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>";
    echo "<span class='tooltip'
                title='Descripción del efecto'>";
    echo "<input type='text'
                 size='20'
                 name='descripcion_$id'
                 id='descripcion_$id'
                 value = '$descripcion'>";
    echo "</span>";
    echo "</td>";

    // presenta el usuario y la fecha
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha</td>";

    // presenta el enlace de edición
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaEfecto'
           id='btnGrabaEfecto'
           title='Pulse para grabar el registro'
           onClick='adversos.verificaAdverso($id)'
           class='botongrabar'>";
    echo "</td>";

    // presenta el enlace de borrar
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnBorraEfecto'
           id='btnBorraEfecto'
           title='Pulse para borrar el registro'
           onClick='adversos.borraAdverso($id)'
           class='botonborrar'>";
    echo "</td>";

    // cierra la fila
    echo "</tr>";

}

// agregamos la última fila
echo "<tr>";
echo "<td>";
echo "<span class='tooltip'
            title='Descripción del efecto'>";
echo "<input type='text'
             size='20'
             name='descripcion_nuevo'
             id='descripcion_nuevo'>";
echo "</span>";
echo "</td>";

// presenta el usuario
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Usuario que ingresó el registro'>";
echo "<input type='text'
             size='10'
             name='usuario_adverso'
             id='usuario_adverso'>";
echo "</span>";
echo "</td>";

// presenta la fecha
echo "<td align='center'>";
echo "<span class='tooltip'
            title='Fecha de alta del registro'>";
echo "<input type='text'
             size='10'
             name='fecha_adverso'
             id='fecha_adverso'>";
echo "</span>";
echo "</td>";

// presenta el enlace de edición
echo "<td align='center'>";
echo "<input type='button'
       name='btnGrabaEfecto'
       id='btnGrabaEfecto'
       title='Pulse para grabar el registro'
       onClick='adversos.verificaAdverso()'
       class='botongrabar'>";
echo "</td>";

// presenta la última celda y cierra
echo "<td align='center'></td>";
echo "<tr>";

// cerramos la tabla
echo "</table>";

?>

<script>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

    // carga el usuario y la fecha de alta
    document.getElementById("usuario_adverso").value = sessionStorage.getItem("Usuario");
    document.getElementById("fecha_adverso").value = fechaActual();

</script>