/*

    Nombre: adversos.class.js
    Autor: Lic. Claudio Invernizzi
    Fecha: 10/06/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de efectos adversos

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones efectos adversos
 */
class Adversos {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initAdversos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initAdversos(){

        // inicializamos las variables
        this.Id = 0;                 // clave del registro
        this.Descripcion = "";       // descripción de la derivación

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga el formulario de datos en el contenedor
     */
    muestraAdversos(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_administracion").load("adversos/listaadversos.php");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que verifica los datos del formulario antes
     * de enviarlo al servidor
     */
    verificaAdverso(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;

        // si está dando un alta
        if (typeof(id) == "undefined"){
            id = "nuevo";
        } else {
            this.Id = id;
        }

        // verifica que halla ingresado el nombre
        if (document.getElementById("descripcion_" + id).value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar descripción del efecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("descripcion_" + id).focus();
            return false;

        }

        // asigna en la variable de clase y graba
        this.Descripcion = document.getElementById("descripcion_" + id).value;
        this.grabaAdverso();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que graba el registro en la base y luego
     * recarga la grilla
     */
    grabaAdverso(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosAdverso = new FormData();

        // agregamos los datos al formulario
        datosAdverso.append("Id", this.Id);
        datosAdverso.append("Descripcion", this.Descripcion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "adversos/grabar.php",
            type: "POST",
            data: datosAdverso,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    adversos.initAdversos();

                    // recarga el formulario para reflejar los cambios
                    adversos.muestraAdversos();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método que llamado al pulsar el botón borrar que
     * verifica si puede borrar el registro
     */
    borraDerivacion(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // llama la rutina php
        $.get('adversos/puedeborrar.php?id='+id,
            function(data){

            // si retornó correcto
            if (data.Registros != 0){

                // llamamos la rutina de eliminación
                Adversos.eliminaAdverso(id);

            // si no puede borrar
            } else {

                // presenta el mensaje
                new jBox('Notice', {content: "El registro tiene pacientes", color: 'red'});

            }

        }, "json");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} id - clave del registro
     * Método llamado luego de verificar que puede
     * borrar el registro y luego de pedir confirmación
     * elimina el registro y recarga la grilla
     */
    eliminaAdverso(id){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Efectos Adversos',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('adversos/borraadverso.php?id='+id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // inicializamos las variables
                            adversos.initAdversos();

                            // recargamos la grilla
                            adversos.muestraAdversos();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un elemento html
     * @param {int} idderivacion - clave del registro preseleccionado
     * Método que recibe como parámetro la id de un elemento del
     * formulario y carga en ese elemento la nómina de tipos de
     * derivacion
     */
    nominaAdversos(idelemento, idderivacion){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "adversos/nominaadversos.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la derivacion
                    if (typeof(idderivacion) != "undefined"){

                        // si coincide
                        if (data[i].Id == idderivacion){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].id + ">" + data[i].descripcion + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].descripcion + "</option>");

                        }

                    // si no recibió la clave
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].id + ">" + data[i].descripcion + "</option>");
                    }

                }

        }});

    }

}