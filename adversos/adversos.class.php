<?php

/**
 *
 * Class Adversos | adversos/adversos.class.php
 *
 * @package     Diagnostico
 * @subpackage  Adversos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * efectos adversos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Adversos {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;             // puntero a la base de datos
    protected $Id;               // clave del registro
    protected $Descripcion;      // descripción del efecto
    protected $IdUsuario;        // clave del usuario
    protected $Usuario;          // nombre del usuario
    protected $Alta;             // fecha de alta

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();
        $this->Id = 0;
        $this->Descripcion = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setId($id){
        $this->Id = $id;
    }
    public function setDescripcion($descripcion){
        $this->Descripcion = $descripcion;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getDescripcion(){
        return $this->Descripcion;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getAlta(){
        return $this->Alta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de síntomas
     * Método que retorna un resultset con la nómina de síntomas
     */
    public function nominaAdversos(){

        // componemos y ejecutamos la consulta
        $consulta = "SELECT diagnostico.v_adversos.id AS id,
                            diagnostico.v_adversos.descripcion AS descripcion,
                            diagnostico.v_adversos.usuario AS usuario,
                            diagnostico.v_adversos.fecha AS fecha
                     FROM diagnostico.v_adversos
                     ORDER BY diagnostico.v_adversos.descripcion; ";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - entero con la clave del registro
     * Método que recibe la clave del registro como parámetro
     * y asigna los valores del mismo en las variables de clase
     */
    public function getDatosAdverso($clave){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_adversos.id AS id,
                            diagnostico.v_adversos.descripcion AS descripcion,
                            diagnostico.v_adversos.usuario AS usuario,
                            diagnostico.v_adversos.fecha AS fecha
                     FROM diagnostico.v_adversos
                     WHERE diagnostico.v_adversos.id = '$clave';";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y asignamos en las
        // variables de clase
        extract($fila);
        $this->Id = $id;
        $this->Descripcion = $descripcion;
        $this->Usuario = $usuario;
        $this->Alta = $fecha;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int la clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * en la base y retorna la id del registro afectado
     */
    public function grabaAdverso(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoAdverso();
        } else {
            $this->editaAdverso();
        }

        // retornamos la clave
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un
     * nuevo síntoma adverso al tratamiento
     */
    protected function nuevoAdverso(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.adversos
                           (descripcion,
                            usuario)
                           VALUES
                           (:descripcion,
                            :idusuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":descripcion", $this->Descripcion);
        $psInsertar->bindParam(":idusuario",   $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición de un
     * síntoma adverso
     */
    protected function editaAdverso(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.adversos SET
                            descripcion = :descripcion,
                            usuario = :idusuario
                     WHERE diagnostico.adversos.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":descripcion", $this->Descripcion);
        $psInsertar->bindParam(":idusuario",   $this->IdUsuario);
        $psInsertar->bindParam(":id",          $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idadverso clave del registro
     * Método que recibe como parámetro la clave del registro
     * y elimina el efecto de la tabla
     */
    public function borraAdverso($idadverso){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.adversos
                    WHERE diagnostico.adversos.id = '$idadverso';";
        $this->Link->exec($consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param descripcion - string con la descripcion
     * @return boolean
     * Método llamado en las altas que verifica no se
     * encuentre declarado el efecto adverso
     */
    public function validaAdverso($descripcion){

        // componemos la consulta
        $consulta = "SELECT COUNT(diagnostico.v_adversos.id) AS registros
                     FROM diagnostico.v_adversos
                     WHERE diagnostico.v_adversos.descripcion = '$descripcion'; ";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si encontró registros
        if ($registros != 0){
            $correcto = false;
        } else {
            $correcto = true;
        }

        // retornamos
        return $correcto;

    }

}
?>