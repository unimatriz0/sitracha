<?php

/**
 *
 * borrar | adversos/borrar.php
 *
 * @package     Diagnostico
 * @subpackage  Adversos
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (10/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta
 * la consulta de eliminación
*/

// incluimos e instanciamos las clases
require_once("adversos.class.php");
$adverso = new Adversos();

// ejecutamos la consulta
$adverso->borraAdverso($_GET["id"]);

?>