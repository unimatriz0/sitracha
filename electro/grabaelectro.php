<?php

/**
 *
 * Class Electro | electro/electro.class.php
 *
 * @package     Diagnostico
 * @subpackage  Electro
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (27/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y ejecuta
 * la consulta en el servidor, retorna por json el resultado de
 * la operación
 *
*/

// incluimos e instanciamos las clases
require_once("electro.class.php");
$electro = new Electro();

// asignamos los valores
$electro->setId($_POST["Id"]);
$electro->setProtocolo($_POST["Protocolo"]);
$electro->setVisita($_POST["Visita"]);
$electro->setFecha($_POST["Fecha"]);
$electro->setNormal($_POST["Normal"]);
$electro->setBradicardia($_POST["Bradicardia"]);
$electro->setFc($_POST["Fc"]);
$electro->setArritmia($_POST["Arritmia"]);
$electro->setQrs($_POST["Qrs"]);
$electro->setEjeqrs($_POST["EjeQrs"]);
$electro->setPr($_POST["Pr"]);
$electro->setBrd($_POST["Brd"]);
$electro->setBrdModerado($_POST["BrdModerado"]);
$electro->setBcrd($_POST["Bcrd"]);
$electro->setTendenciaHbai($_POST["TendenciaHbai"]);
$electro->setHbai($_POST["Hbai"]);
$electro->setTciv($_POST["Tciv"]);
$electro->setBcri($_POST["Bcri"]);
$electro->setBav1g($_POST["Bav1g"]);
$electro->setBav2g($_POST["Bav2g"]);
$electro->setBav3g($_POST["Bav3g"]);
$electro->setFibrosis($_POST["Fibrosis"]);
$electro->setAumentoAi($_POST["AumentoAi"]);
$electro->setWpv($_POST["Wpw"]);
$electro->setHvi($_POST["Hvi"]);
$electro->setNoTiene($_POST["NoTiene"]);
$electro->setSinTranstornos($_POST["SinTranstornos"]);
$electro->setRepolarizacion($_POST["Repolarizacion"]);

// grabamos el registro
$id = $electro->grabaElectro();

// retornamos
echo json_encode(array("Id" => $id));

?>