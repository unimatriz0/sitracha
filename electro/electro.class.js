/*
 * Nombre: electro.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 11/06/2019
 * Licencia/: GPL
 * Comentarios: Clase que controla las operaciones del formulario
 *              de electrocardiograma
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/* jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de electrocardiogramas
 */
class Electro {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initElectro();

        // inicializamos el layer
        this.layerElectro = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables
     */
    initElectro(){

        // inicializamos las variables de clase
        this.Id = 0;                       // clave del registro
        this.Protocolo = 0;                // protocolo del paciente
        this.Visita = 0;                   // clave de la visita
        this.Fecha = "";                   // fecha de administración
        this.Normal = 0;                   // 0 no - 1 si
        this.Bradicardia = 0;              // 0 no - 1 si
        this.Fc = 0;                       // frecuencia cardíaca
        this.Arritmia = "";                // tipo de arritmia
        this.Qrs = 0;                      // decimal
        this.EjeQrs = 0;                   // desviación en grados
        this.Pr = 0;                       // decimal
        this.Brd = 0;                      // 0 no - 1 si
        this.BrdModerado = 0;              // 0 no - 1 si
        this.Bcrd = 0;                     // 0 no - 1 si
        this.TendenciaHbai = 0;            // 0 no - 1 si
        this.Hbai = 0;                     // 0 no - 1 si
        this.Tciv = 0;                     // 0 no - 1 si
        this.Bcri = 0;                     // 0 no - 1 si
        this.Bav1g = 0;                    // 0 no - 1 si
        this.Bav2g = 0;                    // 0 no - 1 si
        this.Bav3g = 0;                    // 0 no - 1 si
        this.Fibrosis = 0;                 // 0 no - 1 si
        this.AumentoAi = 0;                // 0 no - 1 si
        this.Wpw = 0;                      // 0 no - 1 si
        this.Hvi = 0;                      // 0 no - 1 si
        this.NoTiene = 0;                  // 0 no - 1 si
        this.SinTranstornos = 0;           // 0 no - 1 si
        this.Repolarizacion = "";          // tipo de transtorno
        this.Usuario = "";                 // usuario que ingresó el registro
        this.FechaAlta = "";               // fecha de alta del registro

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setProtocolo(protocolo){
        this.Protocolo = protocolo;
    }
    setVisita(visita){
        this.Visita = visita;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setNormal(normal){
        this.Normal = normal;
    }
    setBradicardia(bradicardia){
        this.Bradicardia = bradicardia;
    }
    setFc(fc){
        this.Fc = fc;
    }
    setArritmia(arritmia){
        this.Arritmia = arritmia;
    }
    setQrs(qrs){
        this.Qrs = qrs;
    }
    setEjeQrs(ejeqrs){
        this.EjeQrs = ejeqrs;
    }
    setPr(pr){
        this.Pr = pr;
    }
    setBrd(brd){
        this.Brd = brd;
    }
    setBrdModerado(brdmoderado){
        this.BrdModerado = brdmoderado;
    }
    setBcrd(bcrd){
        this.Bcrd = bcrd;
    }
    setTendenciaHbai(tendencia){
        this.TendenciaHbai = tendencia;
    }
    setHbai(hbai){
        this.Hbai = hbai;
    }
    setTciv(tciv){
        this.Tciv = tciv;
    }
    setBcri(bcri){
        this.Bcri = bcri;
    }
    setBav1g(bav){
        this.Bav1g = bav;
    }
    setBav2g(bav){
        this.Bav2g = bav;
    }
    setBav3g(bav){
        this.Bav3g = bav;
    }
    setFibrosis(fibrosis){
        this.Fibrosis = fibrosis;
    }
    setAumentoAi(aumento){
        this.AumentoAi = aumento;
    }
    setWpw(wpw){
        this.Wpw = wpw;
    }
    setHvi(hvi){
        this.Hvi = hvi;
    }
    setNoTiene(notiene){
        this.NoTiene = notiene;
    }
    setSinTranstornos(sintrastornos){
        this.SinTranstornos = sintrastornos;
    }
    setRepolarizacion(repolarizacion){
        this.Repolarizacion = repolarizacion;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el layer emergente el formulario
     * de electrocardiogramas
     */
    verElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no grabó la visita
        if (visitas.IdVisita == 0){

            // presenta el mensaje
            mensaje = "Debe grabar los datos de la visita primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos el protocolo y la visita del formulario padre
        this.Protocolo = document.getElementById("protocolo_paciente").value;
        this.Visita = visitas.IdVisita;

        // cargamos la página
        this.layerElectro = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Electrocardiogramas',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:900,
                    draggable: 'title',
                    zIndex: 20000,
                    ajax: {
                        url: 'electro/form_electro.html',
                        reload: 'strict'
                    }
            });
        this.layerElectro.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase obtiene
     * los datos del registro en formato json
     */
    getDatosElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "electro/getelectro.php?idvisita=" + this.Visita,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                electro.cargaDatosElectro(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector con el registro
     * Método que recibe como parámetro un array json con
     * los datos del registro y los asigna a las variables
     * de clase
     */
    cargaDatosElectro(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en las variables de clase
        // protocolo y visita no porque los tomamos
        // del formulario padre
        this.setId(datos.Id);
        this.setFecha(datos.Fecha);
        this.setNormal(datos.Normal);
        this.setBradicardia(datos.Bradicardia);
        this.setFc(datos.Fc);
        this.setArritmia(datos.Arritmia);
        this.setQrs(datos.Qrs);
        this.setEjeQrs(datos.EjeQrs);
        this.setPr(datos.Pr);
        this.setBrd(datos.Brd);
        this.setBrdModerado(datos.BrdModerado);
        this.setBcrd(datos.Bcrd);
        this.setTendenciaHbai(datos.TendenciaHbai);
        this.setHbai(datos.Hbai);
        this.setTciv(datos.Tciv);
        this.setBcri(datos.Bcri);
        this.setBav1g(datos.Bav1g);
        this.setBav2g(datos.Bav2g);
        this.setBav3g(datos.Bav3g);
        this.setFibrosis(datos.Fibrosis);
        this.setAumentoAi(datos.AumentoAi);
        this.setWpw(datos.Wpw);
        this.setHvi(datos.Hvi);
        this.setNoTiene(datos.NoTiene);
        this.setSinTranstornos(datos.SinTranstornos);
        this.setRepolarizacion(datos.Repolarizacion);
        this.setUsuario(datos.Usuario);
        this.setFechaAlta(datos.FechaAlta);

        // mostramos el registro
        this.muestraElectro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase muestra
     * el registro en el formulario
     */
    muestraElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en el formulario
        document.getElementById("fecha_electro").value = this.Fecha;

        // si es normal
        if (this.Normal == 0){
            document.getElementById("electro_normal").checked = false;
        } else {
            document.getElementById("electro_normal").checked = true;
        }

        // si presenta bradicardia
        if (this.Bradicardia == 0){
            document.getElementById("electro_bradicardia").checked = false;
        } else {
            document.getElementById("electro_bradicardia").checked = true;
        }

        // carga la frecuencia
        document.getElementById("frecuenciaelectro").value = this.Fc;

        // el tipo de arritmia
        document.getElementById("arritmiaelectro").value = this.Arritmia;

        // el qrs
        document.getElementById("qrs").value = this.Qrs;

        // el eje qrs
        document.getElementById("ejeqrs").value = this.EjeQrs;

        // el pr (en segundos)
        document.getElementById("pr").value = this.Pr;

        // si tiene brd
        if (this.Brd == 0){
            document.getElementById("brd").checked = false;
        } else {
            document.getElementById("brd").checked = true;
        }

        // si tiene brd moderado
        if (this.BrdModerado == 0){
            document.getElementById("brdmoderado").checked = false;
        } else {
            document.getElementById("brdmoderado").checked = true;
        }

        // si tiene bcrd
        if (this.Bcrd == 0){
            document.getElementById("bcrd").checked = false;
        } else {
            document.getElementById("bcrd").checked = true;
        }

        // si tiene tendencia hbai
        if (this.TendenciaHbai == 0){
            document.getElementById("tendenciahbai").checked = false;
        } else {
            document.getElementById("tendenciahbai").checked = true;
        }

        // si tiene hbai
        if (this.Hbai == 0){
            document.getElementById("hbai").checked = false;
        } else {
            document.getElementById("hbai").checked = true;
        }

        // si tiene tciv
        if (this.Tciv == 0){
            document.getElementById("tciv").checked = false;
        } else {
            document.getElementById("tciv").checked = false;
        }

        // si tiene bcri
        if (this.Bcri == 0){
            document.getElementById("bcri").checked = false;
        } else {
            document.getElementById("bcri").checked = true;
        }

        // si tiene bav1g
        if (this.Bav1g == 0){
            document.getElementById("bav1g").checked = false;
        } else {
            document.getElementById("bav1g").checked = true;
        }

        // si tiene bav2g
        if (this.Bav2g == 0){
            document.getElementById("bav2g").checked = false;
        } else {
            document.getElementById("bav2g").checked = true;
        }

        // si tiene bav3g
        if (this.Bav3g == 0){
            document.getElementById("bav3g").checked = false;
        } else {
            document.getElementById("bav3g").checked = true;
        }

        // si no tiene transtornos
        if (this.SinTranstornos == 0){
            document.getElementById("sintranstornos").checked = false;
        } else {
            document.getElementById("sintranstornos").checked = true;
        }

        // si tiene fibrosis
        if (this.Fibrosis == 0){
            document.getElementById("fibrosis").checked = false;
        } else {
            document.getElementById("fibrosis").checked = true;
        }

        // si tiene aumento ai
        if (this.AumentoAi == 0){
            document.getElementById("aumentoai").checked = false;
        } else {
            document.getElementById("aumentoai").checked = true;
        }

        // si tiene Wpw
        if (this.Wpw == 0){
            document.getElementById("wpw").checked = false;
        } else {
            document.getElementById("wpw").checked = true;
        }

        // si tiene hvi
        if (this.Hvi == 0){
            document.getElementById("hvi").checked = false;
        } else {
            document.getElementById("hvi").checked = true;
        }

        // si no tiene otros transtornos
        if (this.NoTiene == 0){
            document.getElementById("sinotros").value = false;
        } else {
            document.getElementById("sinotros").value = false;
        }

        // si tiene transtornos de repolarización
        document.getElementById("repolarizacion").value = this.Repolarizacion;

        // fijamos la fecha de alta y el usuario
        if (this.Id != 0){
            document.getElementById("usuarioelectro").value = this.Usuario;
            document.getElementById("altaelectro").value = this.FechaAlta;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que valida
     * los datos del formulario antes de enviarlos al
     * servidor
     */
    validaElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var mensaje;
        var correcto = false;

        // si no ingresó la fecha
        if (document.getElementById("fecha_electro").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha del estudio";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_electro").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Fecha = document.getElementById("fecha_electro").value;

        }

        // según el valor de normal
        if (document.getElementById("electro_normal").checked){
            this.Normal = 1;
        } else {
            this.Normal = 0;
        }

        // si preenta bradicardia
        if (document.getElementById("electro_bradicardia").checked){
            this.Bradicardia = 1;
        } else {
            this.Bradicardia = 0;
        }

        // si no ingresó la frecuencia
        if (document.getElementById("frecuenciaelectro").value == 0 || document.getElementById("frecuenciaelectro").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese la frecuencia cardíaca";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("frecuenciaelectro").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Fc = document.getElementById("frecuenciaelectro").value;

        }

        // si no seleccionó el tipo de arritmia
        if (document.getElementById("arritmiaelectro").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el tipo de arritmia";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("arritmiaelectro").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Arritmia = document.getElementById("arritmiaelectro").value;

        }

        // si no ingresó el qrs
        if (document.getElementById("qrs").value == 0 || document.getElementById("qrs").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el valor del QRS";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("qrs").focus();
            return false;

        // si ingresó
        } else {

            // lo asigna en la clase
            this.Qrs = document.getElementById("qrs").value;

        }

        // si no ingresó el eje qrs
        if (document.getElementById("ejeqrs").value == 0 || document.getElementById("ejeqrs").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el eje QRS en grados";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ejeqrs").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.EjeQrs = document.getElementById("ejeqrs").value;

        }

        // si no ingresó el pr
        if (document.getElementById("pr").value == 0 || document.getElementById("pr").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Indique el PR en segundos";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("pr").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Pr = document.getElementById("pr").value;

        }

        // transtornos de la conducción debe marcar al menos uno
        correcto = false;

        // si tiene brd
        if (document.getElementById("brd").checked){
            correcto = true;
            this.Brd = 1;
        } else {
            this.Brd = 0;
        }

        // si marcó brdmoderado
        if (document.getElementById("brdmoderado").checked){
            correcto = true;
            this.BrdModerado = 1;
        } else {
            this.BrdModerado = 0;
        }

        // si marcó bcrd
        if (document.getElementById("bcrd").checked){
            correcto = true;
            this.Bcrd = 1;
        } else {
            this.Bcrd = 0;
        }

        // si marcó tendencia
        if (document.getElementById("tendenciahbai").checked){
            correcto = true;
            this.TendenciaHbai = 1;
        } else {
            this.TendenciaHbai = 0;
        }

        // si marcó hbai
        if (document.getElementById("hbai").checked){
            correcto = true;
            this.Hbai = 1;
        } else {
            this.Hbai = 0;
        }

        // si marcó tciv
        if (document.getElementById("tciv").checked){
            correcto = true;
            this.Tciv = 1;
        } else {
            this.Tciv = 0;
        }

        // si marcó bcri
        if (document.getElementById("bcri").checked){
            correcto = true;
            this.Bcri = 1;
        } else {
            this.Bcri = 0;
        }

        // si marcó bav1g
        if (document.getElementById("bav1g").checked){
            correcto = true;
            this.Bav1g = 1;
        } else {
            this.Bav1g = 0;
        }

        // si marcó bav2g
        if (document.getElementById("bav2g").checked){
            correcto = true;
            this.Bav2g = 1;
        } else {
            this.Bav2g = 0;
        }

        // si marcó bav3g
        if (document.getElementById("bav3g").checked){
            correcto = true;
            this.Bav3g = 1;
        } else {
            this.Bav3g = 0;
        }

        // si marcó sin transtornos
        if (document.getElementById("sintranstornos").checked){
            correcto = true;
            this.SinTranstornos = 1;
        } else {
            this.SinTranstornos = 0;
        }

        // si no marcó ningún elemento
        if (!correcto){

            // presenta el mensaje y retorna
            mensaje = "Debe marcar al menos un transtorno de la<br>";
            mensaje += "conducción (puede marcar Sin Transtornos)";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica que halla marcado al menos un elemento
        // de los otros transtornos
        correcto = false;

        // si marcó fibrosis
        if (document.getElementById("fibrosis").checked){
            correcto = true;
            this.Fibrosis = 1;
        } else {
            this.Fibrosis = 0;
        }

        // si marcó aumento ai
        if (document.getElementById("aumentoai").checked){
            correcto = true;
            this.AumentoAi = 1;
        } else {
            this.AumentoAi = 0;
        }

        // si marcó wpw
        if (document.getElementById("wpw").checked){
            correcto = true;
            this.Wpw = 1;
        } else {
            this.Wpw = 0;
        }

        // si marcó hvi
        if (document.getElementById("hvi").checked){
            correcto = true;
            this.Hvi = 1;
        } else {
            this.Hvi = 0;
        }

        // si marcó sin otros
        if (document.getElementById("sinotros").checked){
            correcto = true;
            this.NoTiene = 1;
        } else {
            this.NoTiene = 0;
        }

        // si no marcó ningún elemento
        if (!correcto){

            // presenta el mensaje y retorna
            mensaje = "Debe marcar al menos un transtorno del tipo<br>";
            mensaje += "Otros Transtornos (puede marcar No Tiene)";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // si no seleccionó transtornos de la repolarización
        if (document.getElementById("repolarizacion").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Seleccione de la lista el valor <br>";
            mensaje += "correcto de Transtornos de la Repolatización";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("repolarizacion").focus();
            return false;

        // si seleccionó
        } else {

            // asignamos en la clase
            this.Repolarizacion = document.getElementById("repolarizacion").value;

        }

        // grabamos el registro
        this.grabaElectro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * ejecuta la consulta en el servidor
     */
    grabaElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaracion de variables
        var datosElectro = new FormData();

        // asignamos los valores
        datosElectro.append("Id", this.Id);
        datosElectro.append("Protocolo", this.Protocolo);
        datosElectro.append("Visita", this.Visita);
        datosElectro.append("Fecha", this.Fecha);
        datosElectro.append("Normal", this.Normal);
        datosElectro.append("Bradicardia", this.Bradicardia);
        datosElectro.append("Fc", this.Fc);
        datosElectro.append("Arritmia", this.Arritmia);
        datosElectro.append("Qrs", this.Qrs);
        datosElectro.append("EjeQrs", this.EjeQrs);
        datosElectro.append("Pr", this.Pr);
        datosElectro.append("Brd", this.Brd);
        datosElectro.append("BrdModerado", this.BrdModerado);
        datosElectro.append("Bcrd", this.Bcrd);
        datosElectro.append("TendenciaHbai", this.TendenciaHbai);
        datosElectro.append("Hbai", this.Hbai);
        datosElectro.append("Tciv", this.Tciv);
        datosElectro.append("Bcri", this.Bcri);
        datosElectro.append("Bav1g", this.Bav1g);
        datosElectro.append("Bav2g", this.Bav2g);
        datosElectro.append("Bav3g", this.Bav3g);
        datosElectro.append("Fibrosis", this.Fibrosis);
        datosElectro.append("AumentoAi", this.AumentoAi);
        datosElectro.append("Wpw", this.Wpw);
        datosElectro.append("Hvi", this.Hvi);
        datosElectro.append("NoTiene", this.NoTiene);
        datosElectro.append("SinTranstornos", this.SinTranstornos);
        datosElectro.append("Repolarizacion", this.Repolarizacion);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "electro/grabaelectro.php",
            type: "POST",
            data: datosElectro,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    electro.setId(data.Id);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * según corresponda limpia el formulario o recarga
     * el registro
     */
    cancelaElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está editando
        if (this.Id != 0){
            this.getDatosElectro();
        } else {
            this.limpiaElectro();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario de datos
     */
    limpiaElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos los campos del formulario
        document.getElementById("fecha_electro").value = "";
        document.getElementById("electro_normal").checked = false;
        document.getElementById("electro_bradicardia").checked = false;
        document.getElementById("frecuenciaelectro").value = "";
        document.getElementById("arritmiaelectro").value = "";
        document.getElementById("qrs").value = "";
        document.getElementById("ejeqrs").value = "";
        document.getElementById("pr").value = "";
        document.getElementById("brd").checked = false;
        document.getElementById("brdmoderado").checked = false;
        document.getElementById("bcrd").checked = false;
        document.getElementById("tendenciahbai").checked = false;
        document.getElementById("hbai").checked = false;
        document.getElementById("tciv").checked = false;
        document.getElementById("bcri").checked = false;
        document.getElementById("bav1g").checked = false;
        document.getElementById("bav2g").checked = false;
        document.getElementById("bav3g").checked = false;
        document.getElementById("sintranstornos").checked = false;
        document.getElementById("fibrosis").checked = false;
        document.getElementById("aumentoai").checked = false;
        document.getElementById("wpw").checked = false;
        document.getElementById("hvi").checked = false;
        document.getElementById("sinotros").checked = false;
        document.getElementById("repolarizacion").value = "";

        // fijamos la fecha de alta y el usuario
        document.getElementById("usuarioelectro").value = sessionStorage.getItem("Usuario");
        document.getElementById("altaelectro").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de pedir confirmación elimina el
     * registro actual y cierra el layer
     */
    borraElectro(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Electrocardiograma',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('electro/borraelectro.php?id='+electro.Id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // cerramos el layer
                            electro.layerElecto.destroy();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}