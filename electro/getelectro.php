<?php

/**
 *
 * borraelectro | electro/borraelectro.php
 *
 * @package     Diagnostico
 * @subpackage  Electro
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (27/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una visita y retorna el 
 * array json con los datos del registro
 * 
*/

// incluimos e instanciamos las clases
require_once("electro.class.php");
$electro = new Electro();

// obtenemos el registro
$electro->getDatosElectro($_GET["idvisita"]);

// retornamos
echo json_encode(array("Id" =>             $electro->getId(),
                       "Fecha" =>          $electro->getFecha(),
                       "Normal" =>         $electro->getNormal(),
                       "Bradicardia" =>    $electro->getBradicardia(),
                       "Fc" =>             $electro->getFc(),
                       "Arritmia" =>       $electro->getArritmia(),
                       "Qrs" =>            $electro->getQrs(),
                       "EjeQrs" =>         $electro->getEjeqrs(),
                       "Pr" =>             $electro->getPr(),
                       "Brd" =>            $electro->getBrd(),
                       "BrdModerado" =>    $electro->getBrdModerado(),
                       "Bcrd" =>           $electro->getBcrd(),
                       "TendenciaHbai" =>  $electro->getTendenciaHbai(),
                       "Hbai" =>           $electro->getHbai(),
                       "Tciv" =>           $electro->getTciv(),
                       "Bcri" =>           $electro->getBcri(),
                       "Bav1g" =>          $electro->getBav1g(),
                       "Bav2g" =>          $electro->getBav2g(),
                       "Bav3g" =>          $electro->getBav3g(),
                       "Fibrosis" =>       $electro->getFibrosis(),
                       "AumentoAi" =>      $electro->getAumentoAi(),
                       "Wpw" =>            $electro->getWpv(),
                       "Hvi" =>            $electro->getHvi(),
                       "NoTiene" =>        $electro->getNotiene(),
                       "SinTranstornos" => $electro->getSinTranstornos(),
                       "Repolarizacion" => $electro->getRepolarizacion(),
                       "Usuario" =>        $electro->getUsuario(),
                       "FechaAlta" =>      $electro->getFechaAlta()));

?>