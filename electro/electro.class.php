<?php

/**
 *
 * Class Electro | electro/electro.class.php
 *
 * @package     Diagnostico
 * @subpackage  Electro
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * efectos adversos
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Electro {

    // declaramos las variables
    protected $Link;                         // puntero a la base de datos
    protected $Id;                           // clave del registro
    protected $Protocolo;                    // clave del paciente
    protected $Visita;                       // clave de la visita
    protected $Fecha;                        // fecha de administración
    protected $Normal;                       // 0 false - 1 verdadero
    protected $Bradicardia;                  // 0 false - 1 verdadero
    protected $Fc;                           // la frecuencia cardíaca
    protected $Arritmia;                     // tipo de arritmia
    protected $Qrs;                          // eje qrs
    protected $Ejeqrs;                       // desviación del eje en grados
    protected $Pr;                           // ver que significa
    protected $Brd;                          // 0 false - 1 verdadero
    protected $BrdModerado;                  // 0 false - 1 verdadero
    protected $Bcrd;                         // 0 false - 1 verdadero
    protected $TendenciaHbai;                // 0 false - 1 verdadero
    protected $Hbai;                         // 0 false - 1 verdadero
    protected $Tciv;                         // 0 false - 1 verdadero
    protected $Bcri;                         // 0 false - 1 verdadero
    protected $Bav1g;                        // 0 false - 1 verdadero
    protected $Bav2g;                        // 0 false - 1 verdadero
    protected $Bav3g;                        // 0 false - 1 verdadero
    protected $Fibrosis;                     // 0 false - 1 verdadero
    protected $AumentoAi;                    // 0 false - 1 verdadero
    protected $Wpv;                          // 0 false - 1 verdadero
    protected $Hvi;                          // 0 false - 1 verdadero
    protected $NoTiene;                      // 0 false - 1 verdadero
    protected $SinTanstornos;                // 0 false - 1 verdadero
    protected $Repolarizacion;               // tipo de transtorno de repolarización
    protected $Usuario;                      // nombre del usuario
    protected $IdUsuario;                    // clave del usuario
    protected $FechaAlta;                    // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){
        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initElectro();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase cuando
     * es llamado desde el constructor o cuando no existe
     * un registro
     */
    protected function initElectro() {

    	// inicializamos las variables
        $this->Id = 0;
        $this->Protocolo = 0;
        $this->Visita = 0;
        $this->Fecha = "";
        $this->Normal = 0;
        $this->Bradicardia = 0;
        $this->Fc = 0;
        $this->Arritmia = "";
        $this->Qrs = 0;
        $this->Ejeqrs = 0;
        $this->Pr = 0;
        $this->Brd = 0;
        $this->BrdModerado = 0;
        $this->Bcrd = 0;
        $this->TendenciaHbai = 0;
        $this->Hbai = 0;
        $this->Tciv = 0;
        $this->Bcri = 0;
        $this->Bav1g = 0;
        $this->Bav2g = 0;
        $this->Bav3g = 0;
        $this->Fibrosis = 0;
        $this->AumentoAi = 0;
        $this->Wpv = 0;
        $this->Hvi = 0;
        $this->NoTiene = 0;
        $this->SinTanstornos = 0;
        $this->Repolarizacion = "";
        $this->Usuario = "";
        $this->FechaAlta = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setProtocolo($protocolo){
        $this->Protocolo = $protocolo;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setFecha($fecha){
        $this->Fecha = $fecha;
    }
    public function setNormal($normal){
        $this->Normal = $normal;
    }
    public function setBradicardia($bradicardia){
        $this->Bradicardia = $bradicardia;
    }
    public function setFc($fc){
        $this->Fc = $fc;
    }
    public function setArritmia($arritmia){
        $this->Arritmia = $arritmia;
    }
    public function setQrs($qrs){
        $this->Qrs = $qrs;
    }
    public function setEjeqrs($eje){
        $this->Ejeqrs = $eje;
    }
    public function setPr($pr){
        $this->Pr = $pr;
    }
    public function setBrd($brd){
        $this->Brd = $brd;
    }
    public function setBrdModerado($brdmoderado){
        $this->BrdModerado = $brdmoderado;
    }
    public function setBcrd($bcrd){
        $this->Brcd = $bcrd;
    }
    public function setTendenciaHbai($tendencia){
        $this->TendenciaHbai = $tendencia;
    }
    public function setHbai($hbai){
        $this->Hbai = $hbai;
    }
    public function setTciv($tciv){
        $this->Tciv = $tciv;
    }
    public function setBcri($bcri){
        $this->Bcri = $bcri;
    }
    public function setBav1g($bav){
        $this->Bav1g = $bav;
    }
    public function setBav2g($bav){
        $this->Bav2g = $bav;
    }
    public function setBav3g($bav){
        $this->Bav3g = $bav;
    }
    public function setFibrosis($fibrosis){
        $this->Fibrosis = $fibrosis;
    }
    public function setAumentoAi($aumento){
        $this->AumentoAi = $aumento;
    }
    public function setWpv($wpv){
        $this->Wpv = $wpv;
    }
    public function setHvi($hvi){
        $this->Hvi = $hvi;
    }
    public function setNoTiene($notiene){
        $this->NoTiene = $notiene;
    }
    public function setSinTranstornos($sintranstornos){
        $this->SinTanstornos = $sintranstornos;
    }
    public function setRepolarizacion($repolarizacion){
        $this->Repolarizacion = $repolarizacion;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getProtocolo(){
        return $this->Protocolo;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getFecha(){
        return $this->Fecha;
    }
    public function getNormal(){
        return $this->Normal;
    }
    public function getBradicardia(){
        return $this->Bradicardia;
    }
    public function getFc(){
        return $this->Fc;
    }
    public function getArritmia(){
        return $this->Arritmia;
    }
    public function getQrs(){
        return $this->Qrs;
    }
    public function getEjeqrs(){
        return $this->Ejeqrs;
    }
    public function getPr(){
        return $this->Pr;
    }
    public function getBrd(){
        return $this->Brd;
    }
    public function getBrdModerado(){
        return $this->BrdModerado;
    }
    public function getBcrd(){
        return $this->Bcrd;
    }
    public function getTendenciaHbai(){
        return $this->TendenciaHbai;
    }
    public function getHbai(){
        return $this->Hbai;
    }
    public function getTciv(){
        return $this->Tciv;
    }
    public function getBcri(){
        return $this->Bcri;
    }
    public function getBav1g(){
        return $this->Bav1g;
    }
    public function getBav2g(){
        return $this->Bav2g;
    }
    public function getBav3g(){
        return $this->Bav3g;
    }
    public function getFibrosis(){
        return $this->Fibrosis;
    }
    public function getAumentoAi(){
        return $this->AumentoAi;
    }
    public function getWpv(){
        return $this->Wpv;
    }
    public function getHvi(){
        return $this->Hvi;
    }
    public function getNotiene(){
        return $this->NoTiene;
    }
    public function getSinTranstornos(){
        return $this->SinTanstornos;
    }
    public function getRepolarizacion(){
        return $this->Repolarizacion;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFechaAlta(){
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - clave del protocolo del paciente
     * @return resultset - vector con los registros encontrados
     * Método utilizado en la impresión de historias clínicas
     * clasificadas por estudio, que recibe como parámetro el
     * protocolo y retorna todos los electros realizados
     */
    public function nominaElectro($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_electro.id AS id,
                           diagnostico.v_electro.protocolo AS protocolo,
                           diagnostico.v_electro.visita AS visita,
                           diagnostico.v_electro.fecha AS fecha,
                           diagnostico.v_electro.normal AS normal,
                           diagnostico.v_electro.bradicardia AS bradicardia,
                           diagnostico.v_electro.fc AS fc,
                           diagnostico.v_electro.arritmia AS arritmia,
                           diagnostico.v_electro.qrs AS qrs,
                           diagnostico.v_electro.ejeqrs AS ejeqrs,
                           diagnostico.v_electro.pr AS pr,
                           diagnostico.v_electro.brd AS brd,
                           diagnostico.v_electro.brdmoderado AS brdmoderado,
                           diagnostico.v_electro.bcrd AS bcrd,
                           diagnostico.v_electro.tendenciahbai AS tendenciahbai,
                           diagnostico.v_electro.hbai AS hbai,
                           diagnostico.v_electro.tciv AS tciv,
                           diagnostico.v_electro.bcri AS bcri,
                           diagnostico.v_electro.bav1g AS bav1g,
                           diagnostico.v_electro.bav2g AS bav2g,
                           diagnostico.v_electro.bav3g AS bav3g,
                           diagnostico.v_electro.fibrosis AS fibrosis,
                           diagnostico.v_electro.aumentoai AS aumentoai,
                           diagnostico.v_electro.wpv AS wpv,
                           diagnostico.v_electro.hvi AS hvi,
                           diagnostico.v_electro.notiene AS notiene,
                           diagnostico.v_electro.sintranstornos AS sintranstornos,
                           diagnostico.v_electro.repolarizacion AS repolarizacion,
                           diagnostico.v_electro.usuario AS usuario,
                           diagnostico.v_electro.fecha_alta AS fecha_alta
                     FROM diagnostico.v_electro
                     WHERE diagnostico.v_electro.protocolo = '$protocolo'
                     ORDER BY STR_TO_DATE(diagnostico.v_electro.fecha, '%d/%m/%Y'); ";

        // obtenemos el vector y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita - entero con la clave de la visita
     * Método que recibe como parámetro la clave del registro
     * y asigna en las variables de clase los valores de la
     * base
     */
    public function getDatosElectro($idvisita){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_electro.id AS id,
                            diagnostico.v_electro.protocolo AS protocolo,
                            diagnostico.v_electro.visita AS visita,
                            diagnostico.v_electro.fecha AS fecha,
                            diagnostico.v_electro.normal AS normal,
                            diagnostico.v_electro.bradicardia AS bradicardia,
                            diagnostico.v_electro.fc AS fc,
                            diagnostico.v_electro.arritmia AS arritmia,
                            diagnostico.v_electro.qrs AS qrs,
                            diagnostico.v_electro.ejeqrs AS ejeqrs,
                            diagnostico.v_electro.pr AS pr,
                            diagnostico.v_electro.brd AS brd,
                            diagnostico.v_electro.brdmoderado AS brdmoderado,
                            diagnostico.v_electro.bcrd AS bcrd,
                            diagnostico.v_electro.tendenciahbai AS tendenciahbai,
                            diagnostico.v_electro.hbai AS hbai,
                            diagnostico.v_electro.tciv AS tciv,
                            diagnostico.v_electro.bcri AS bcri,
                            diagnostico.v_electro.bav1g AS bav1g,
                            diagnostico.v_electro.bav2g AS bav2g,
                            diagnostico.v_electro.bav3g AS bav3g,
                            diagnostico.v_electro.fibrosis AS fibrosis,
                            diagnostico.v_electro.aumentoai AS aumentoai,
                            diagnostico.v_electro.wpv AS wpv,
                            diagnostico.v_electro.hvi AS hvi,
                            diagnostico.v_electro.notiene AS notiene,
                            diagnostico.v_electro.sintranstornos AS sintranstornos,
                            diagnostico.v_electro.repolarizacion AS repolarizacion,
                            diagnostico.v_electro.usuario AS usuario,
                            diagnostico.v_electro.fecha_alta AS fecha_alta
                     FROM diagnostico.v_electro
                     WHERE diagnostico.v_electro.visita = '$idvisita'; ";

        // obtenemos el vector
        $resultado = $this->Link->query($consulta);

        // si hubo registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro y lo asignamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Protocolo = $protocolo;
            $this->Visita = $visita;
            $this->Fecha = $fecha;
            $this->Normal = $normal;
            $this->Bradicardia = $bradicardia;
            $this->Fc = $fc;
            $this->Arritmia = $arritmia;
            $this->Qrs = $qrs;
            $this->Ejeqrs = $ejeqrs;
            $this->Pr = $pr;
            $this->Brd = $brd;
            $this->BrdModerado = $brdmoderado;
            $this->Bcrd = $bcrd;
            $this->TendenciaHbai = $tendenciahbai;
            $this->Hbai = $hbai;
            $this->Tciv = $tciv;
            $this->Bcri = $bcri;
            $this->Bav1g = $bav1g;
            $this->Bav2g = $bav2g;
            $this->Bav3g = $bav2g;
            $this->Fibrosis = $fibrosis;
            $this->AumentoAi = $aumentoai;
            $this->Wpv = $wpv;
            $this->Hvi = $hvi;
            $this->NoTiene = $notiene;
            $this->SinTanstornos = $sintranstornos;
            $this->Repolarizacion = $repolarizacion;
            $this->Usuario = $usuario;
            $this->FechaAlta = $fecha_alta;

        // si no encontró
        } else {

            // inicializa las variables
            $this->initElectro();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero la clave del registro afectado
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda
     */
    public function grabaElectro(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoElectro();
        } else {
            $this->editaElectro();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    public function nuevoElectro(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.electrocardiograma
                            (protocolo,
                             visita,
                             fecha,
                             normal,
                             bradicardia,
                             fc,
                             arritmia,
                             qrs,
                             ejeqrs,
                             pr,
                             brd,
                             brdmoderado,
                             bcrd,
                             tendenciahbai,
                             hbai,
                             tciv,
                             bcri,
                             bav1g,
                             bav2g,
                             bav3g,
                             fibrosis,
                             aumentoai,
                             wpv,
                             hvi,
                             notiene,
                             sintranstornos,
                             repolarizacion,
                             usuario)
                            VALUES
                           (:protocolo,
                            :visita,
                            STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            :normal,
                            :bradicardia,
                            :fc,
                            :arritmia,
                            :qrs,
                            :ejeqrs,
                            :pr,
                            :brd,
                            :brdmoderado,
                            :bcrd,
                            :tendenciahbai,
                            :hbai,
                            :tciv,
                            :bcri,
                            :bav1g,
                            :bav2g,
                            :bav3g,
                            :fibrosis,
                            :aumentoai,
                            :wpv,
                            :hvi,
                            :notiene,
                            :sintranstornos,
                            :repolarizacion,
                            :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":protocolo",      $this->Protocolo);
        $psInsertar->bindParam(":visita",         $this->Visita);
        $psInsertar->bindParam(":fecha",          $this->Fecha);
        $psInsertar->bindParam(":normal",         $this->Normal);
        $psInsertar->bindParam(":bradicardia",    $this->Bradicardia);
        $psInsertar->bindParam(":fc",             $this->Fc);
        $psInsertar->bindParam(":arritmia",       $this->Arritmia);
        $psInsertar->bindParam(":qrs",            $this->Qrs);
        $psInsertar->bindParam(":ejeqrs",         $this->Ejeqrs);
        $psInsertar->bindParam(":pr",             $this->Pr);
        $psInsertar->bindParam(":brd",            $this->Brd);
        $psInsertar->bindParam(":brdmoderado",    $this->BrdModerado);
        $psInsertar->bindParam(":bcrd",           $this->Brcd);
        $psInsertar->bindParam(":tendenciahbai",  $this->TendenciaHbai);
        $psInsertar->bindParam(":hbai",           $this->Hbai);
        $psInsertar->bindParam(":tciv",           $this->Tciv);
        $psInsertar->bindParam(":bcri",           $this->Bcri);
        $psInsertar->bindParam(":bav1g",          $this->Bav1g);
        $psInsertar->bindParam(":bav2g",          $this->Bav2g);
        $psInsertar->bindParam(":bav3g",          $this->Bav3g);
        $psInsertar->bindParam(":fibrosis",       $this->Fibrosis);
        $psInsertar->bindParam(":aumentoai",      $this->AumentoAi);
        $psInsertar->bindParam(":wpv",            $this->Wpv);
        $psInsertar->bindParam(":hvi",            $this->Hvi);
        $psInsertar->bindParam(":notiene",        $this->NoTiene);
        $psInsertar->bindParam(":sintranstornos", $this->SinTanstornos);
        $psInsertar->bindParam(":repolarizacion", $this->Repolarizacion);
        $psInsertar->bindParam(":usuario",        $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    public function editaElectro(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.electrocardiograma SET
                            protocolo = :protocolo,
                            visita = :visita,
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            normal = :normal,
                            bradicardia = :bradicardia,
                            fc = :fc,
                            arritmia = :arritmia,
                            qrs = :qrs,
                            ejeqrs = :ejeqrs,
                            pr = :pr,
                            brd = :brd,
                            brdmoderado = :brdmoderado,
                            bcrd = :bcrd,
                            tendenciahbai = :tendenciahbai,
                            hbai = :hbai,
                            tciv = :tciv,
                            bcri = :bcri,
                            bav1g = :bav1g,
                            bav2g = :bav2g,
                            bav3g = :bav3g,
                            fibrosis = :fibrosis,
                            aumentoai = :aumentoai,
                            wpv = :wpv,
                            hvi = :hvi,
                            notiene = :notiene,
                            sintranstornos = :sintranstornos,
                            repolarizacion = :repolarizacion,
                            usuario = :usuario
                     WHERE diagnostico.electrocardiograma.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":protocolo",      $this->Protocolo);
        $psInsertar->bindParam(":visita",         $this->Visita);
        $psInsertar->bindParam(":fecha",          $this->Fecha);
        $psInsertar->bindParam(":normal",         $this->Normal);
        $psInsertar->bindParam(":bradicardia",    $this->Bradicardia);
        $psInsertar->bindParam(":fc",             $this->Fc);
        $psInsertar->bindParam(":arritmia",       $this->Arritmia);
        $psInsertar->bindParam(":qrs",            $this->Qrs);
        $psInsertar->bindParam(":ejeqrs",         $this->Ejeqrs);
        $psInsertar->bindParam(":pr",             $this->Pr);
        $psInsertar->bindParam(":brd",            $this->Brd);
        $psInsertar->bindParam(":brdmoderado",    $this->BrdModerado);
        $psInsertar->bindParam(":bcrd",           $this->Brcd);
        $psInsertar->bindParam(":tendenciahbai",  $this->TendenciaHbai);
        $psInsertar->bindParam(":hbai",           $this->Hbai);
        $psInsertar->bindParam(":tciv",           $this->Tciv);
        $psInsertar->bindParam(":bcri",           $this->Bcri);
        $psInsertar->bindParam(":bav1g",          $this->Bav1g);
        $psInsertar->bindParam(":bav2g",          $this->Bav2g);
        $psInsertar->bindParam(":bav3g",          $this->Bav3g);
        $psInsertar->bindParam(":fibrosis",       $this->Fibrosis);
        $psInsertar->bindParam(":aumentoai",      $this->AumentoAi);
        $psInsertar->bindParam(":wpv",            $this->Wpv);
        $psInsertar->bindParam(":hvi",            $this->Hvi);
        $psInsertar->bindParam(":notiene",        $this->NoTiene);
        $psInsertar->bindParam(":sintranstornos", $this->SinTanstornos);
        $psInsertar->bindParam(":repolarizacion", $this->Repolarizacion);
        $psInsertar->bindParam(":usuario",        $this->IdUsuario);
        $psInsertar->bindParam(":id",             $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idelectro - clave del registro
     */
    public function borraElectro($idelectro){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.electrocardiograma
                     WHERE diagnostico.electrocardiograma.id = '$idelectro';";
        $this->Link->exec($consulta);

    }
}
?>