<?php

/**
 *
 * borraelectro | electro/borraelectro.php
 *
 * @package     Diagnostico
 * @subpackage  Electro
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (27/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta
 * la consulta de eliminación
 * 
*/

// incluimos e instanciamos las clases
require_once("electro.class.php");
$electro = new Electro();

// eliminamos el registro
$electro->borraElectro($_GET["id"]);

// retornamos
echo json_encode(array("Id" => 1));

?>