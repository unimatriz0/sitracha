/*

    Nombre: fechas.class.js
    Autor: Lic. Claudio Innizzi
    Fecha: 26/06/2018
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de fechas de la
                 plataforma (entrega de resultados, etc.)

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de fechas de la
 *        plataforma (por ejemplo, obtiene las fecha de
 *        entrega de resultados)
 */
class Fechas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initFechas();

        // variables de la clase

    }

    /**
     * Método que inicializa las variables de clase
     */
    initFechas(){

        // el layer
        this.layerFechas = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que llama la rutina php para verificar si está definida
     * la fecha de entrega de resultados
     */
    entregaResultados(){

        // llama la rutina php
        $.get('fechas/fecha_resultados.php',
        function(data){

            // si retornó error
            if (data.Error != 1){

                // abrimos el layer pidiendo la fecha
                // de entrega de resultados
                fechas.layerFechas = new jBox('Modal', {
                            animation: 'flip',
                            closeOnEsc: false,
                            closeOnClick: false,
                            closeOnMouseleave: false,
                            closeButton: false,
                            overlay: false,
                            onCloseComplete: function(){
                                this.destroy();
                            },
                            title: 'Entrega de Resultados',
                            draggable: 'title',
                            repositionOnContent: true,
                            theme: 'TooltipBorder',
                            ajax: {
                                url: 'fechas/definir_fecha.php',
                            reload: 'strict'
                            }
                    });
                fechas.layerFechas.open();

            }

        }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón definir fecha en
     * la entrega de resultados, verifica que la fecha sea
     * correcta y ejecuta la consulta en el servidor, luego
     * destruye el layer
     */
    definirFecha(){

        // declaración de variables
        var mensaje;
        var fecha = document.getElementById("fecha_entrega").value;

        // verificamos que la fecha sea correcta
        if (fecha == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la fecha de entrega";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_entrega").focus();
            return false;

        }

        // ejecutamos la consulta en el servidor
        $.get('fechas/graba_fecha.php?fecha='+fecha,
        function(data){

            // si retornó error
            if (data.Error !== 1){

                // presenta el mensaje
                mensaje = "Ha ocurrido un error";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("fecha_entrega").focus();
                return false;

            // si grabó
            } else {

                // presenta el mensaje y destruye el layer
                mensaje = "Fecha de entrega definida";
                new jBox('Notice', {content: mensaje, color: 'green'});
                fechas.layerFechas.destroy();

            }

        }, "json");

    }

}