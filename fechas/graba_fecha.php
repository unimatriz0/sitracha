<?php

/**
 *
 * fechas/graba_fecha.php
 *
 * @package     Diagnostico
 * @subpackage  Fechas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (26/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una fecha en formato dd/mm/YYYY y ejecuta
 * la consulta en el servidor actualizando la fecha de entrega de
 * resultados para los registros ingresados en la fecha actual
*/

// inclusión de archivos
require_once ("fechas.class.php");
$fecha = new Fechas();

// grabamos la fecha
$fecha->fechaEntrega($_GET["fecha"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>