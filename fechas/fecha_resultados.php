<?php

/**
 *
 * fechas/fecha_resultados.php
 *
 * @package     Diagnostico
 * @subpackage  Fechas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que verifica si ya se ha definido una fecha de entrega de
 * resultados para el día actual, retorna 1 si ya está definida,
 * en caso contrario retorna 0 (falso)
*/

// inclusión de archivos
require_once ("fechas.class.php");
$fecha = new Fechas();

// verificamos si está definida
$registros = $fecha->fechaResultados();

// retornamos
echo json_encode(array("Error" => $registros));

?>