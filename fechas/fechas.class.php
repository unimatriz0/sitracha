<?php

/**
 *
 * Class Fechas | fechas/fechas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Fechas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (26/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de fecha de la plataforma
 * (por ejemplo, la fecha de entrega de resultados)
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Fechas {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del usuario
    protected $IdLaboratorio;         // laboratorio del usuario

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que verifica la tabla de fechas de entrega y retorna si
     * ya está definida la fecha para el día actual
     * @autor Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int
     */
    public function fechaResultados(){

        // declaración de variables
        $registros = 0;
        $fecha_actual = date('Y/m/d');

        // definimos la consulta
        $consulta = "SELECT COUNT(diagnostico.entregas.id) AS registros
                     FROM diagnostico.entregas
                     WHERE diagnostico.entregas.recepcion = '$fecha_actual' AND
                           diagnostico.entregas.laboratorio = '$this->IdLaboratorio';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // retornamos el número de registros
        return $registros;

    }

    /**
     * Método que recibe como parámetro una cadena en el formato dd/mm/YYYY
     * que es la fecha de entrega de resultados y graba para la fecha actual
     * la fecha de entrega
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $fecha_entrega
     */
    public function fechaEntrega($fecha_entrega){

        // obtenemos la fecha actual
        $fecha_actual = date('Y/m/d');

        // componemos la consulta de inserción
        $consulta = "INSERT INTO diagnostico.entregas
                            (recepcion,
                             entrega,
                             laboratorio,
                             usuario)
                            VALUES
                            (:recepcion,
                             STR_TO_DATE(:entrega, '%d/%m/%Y'),
                             :laboratorio,
                             :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":recepcion", $fecha_actual);
        $psInsertar->bindParam(":entrega", $fecha_entrega);
        $psInsertar->bindParam(":laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":usuario", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

    }

    /**
     * Método que retorna el día de la semana actual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string
     */
    public function diaSemana(){

        // obtenemos el día en inglés
        $dia = date('l');

        // según el día
        switch($dia){

            // según el valor
            case "Monday":
                $dia = "Lunes";
                break;
            case "Tuesday":
                $dia = "Martes";
                break;
            case "Wednesday":
                $dia = "Miércoles";
                break;
            case "Thursday":
                $dia = "Jueves";
                break;
            case "Friday":
                $dia = "Viernes";
                break;
        }

        // retornamos la cadena
        return $dia;

    }

    /**
     * Método público que retorna el mes en castellano de la
     * fecha actual
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string
     */
    public function mesFecha(){

        // obtenemos el mes en formato númerico (1 dígito)
        $mes = date('n');

        // según el mes
        switch ($mes){

            // asignamos la variables
            case "1":
                $mes = "Enero";
                break;
            case "2":
                $mes = "Febrero";
                break;
            case "3":
                $mes = "Marzo";
                break;
            case "4":
                $mes = "Abril";
                break;
            case "5":
                $mes = "Mayo";
                break;
            case "6":
                $mes = "Junio";
                break;
            case "7":
                $mes = "Julio";
                break;
            case "8":
                $mes = "Agosto";
                break;
            case "9":
                $mes = "Septiembre";
                break;
            case "10":
                $mes = "Octubre";
                break;
            case "11":
                $mes = "Noviembre";
                break;
            case "12":
                $mes = "Diciembre";
                break;

        }

        // retornamos la cadena
        return $mes;

    }

}