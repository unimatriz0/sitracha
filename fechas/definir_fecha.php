<?php

/**
 *
 * fechas/definir_fecha.php
 *
 * @package     Diagnostico
 * @subpackage  Fechas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (22/06/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que presenta el cuadro de confirmación solicitando la
 * fecha de entrega de resultados
*/

// inclusión de archivos
require_once ("fechas.class.php");
$resultados = new Fechas();

// presentamos el título
echo "<h2>Indique la fecha de entrega</h2>";

// obtenemos la fecha actual
$fecha_actual = date('m');
$dia = $resultados->diaSemana();
$mes = $resultados->mesFecha();
$anio = date('Y');

// presenta la fecha actual
echo "<p>Hoy es $dia $fecha_actual de $mes de $anio,
         confirme la fecha de entrega <br>de resultados
         para los pacientes ingresados</p>";

// pide confirmación
echo "<label>Entrega: ";
echo "<input type='text'
             name='fecha_entrega'
             id='fecha_entrega'
             size='12'
             class='datepicker'
             title='Seleccione la fecha de entrega'>";

// inserta un separador
echo "&nbsp; &nbsp; &nbsp; &nbsp; ";

// presenta el botón confirmar
echo "<input type='button'
       name='btnCofirmaFecha'
       id='btnConfirmaFecha'
       class='boton_confirmar'
       value='Confirmar'
       title='Pulse para confirmar la fecha'
       onClick='fechas.definirFecha()'>";
?>
<script>

    // fijamos los datepicker, desabilitamos los sábados y
    // domingos y configuramos el valor de retorno
    $('#fecha_entrega').Zebra_DatePicker({
        disabled_dates: ['* * * 0,6'],
        direction: 7,
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        format: "d/m/Y",
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    });

</script>