<?php

/**
 *
 * financiamiento/form_financiamiento.class.php
 *
 * @package     Diagnostico
 * @subpackage  Financiamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que arma el formulario para la edición de 
 * fuentes de financiamiento 
 * 
*/

// incluimos e instanciamos la clase
require_once ("financiamiento.class.php");
$financiamiento = new Financiamiento();

// abrimos la sesión y obtenemos el usuario
session_start();
if (!empty($_SESSION["ID"])){
    $usuario_actual = $_SESSION["Usuario"];
}
session_write_close();

// presentamos el título
echo "<h2>Nómina de Fuentes de Financiamiento</h2>";

// definimos la tabla
echo "<table width='40%' align='center' border='0'>";

// definimos los encabezados
echo "<thead>";
echo "<tr>";
echo "<th align='left'>Fuente</th>";
echo "<th>Usuario</th>";
echo "<th>Fecha</th>";
echo "<th></th>";
echo "</tr>";
echo "</thead>";

// obtenemos la nómina de dependencias
$nomina = $financiamiento->nominaFinanciamiento();

// definimos el cuerpo
echo "<tbody>";

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // abrimos la fila
    echo "<tr>";

    // presentamos el registro
    echo "<td>";
    echo "<span class='tooltip'
                title='Descripción de la fuente de financiamiento'>";
    echo "<input type='text'
           name='financiamiento'
           id='$id_financiamiento'
           size='25'
           value='$financiamiento'>";
    echo "</span>";
    echo "</td>";

    // presentamos el usuario y el alta
    echo "<td align='center'>$usuario</td>";
    echo "<td align='center'>$fecha_alta</td>";

    // presentamos el botón grabar
    echo "<td align='center'>";
    echo "<input type='button'
           name='btnGrabaFinanciamiento'
           id='btnGrabaFinanciamiento'
           onClick='financiamiento.verificaFinanciamiento($id_financiamiento)'
           title='Pulse para grabar el registro'
           class='botongrabar'>";
    echo "</td>";

    // cerramos la fila
    echo "<tr>";

}

// abrimos la fila para insertar una nueva fuente
echo "<tr>";

// presentamos el registro
echo "<td>";
echo "<span class='tooltip'
            title='Descripción de la fuente de financiamiento'>";
echo "<input type='text'
       name='financiamiento'
       id='nuevo'
       size='25'>";
echo "</span>";
echo "</td>";

// obtenemos la fecha actual
$fecha_alta = date('d/m/Y');

// presentamos la fecha de alta y el usuario actual
echo "<td align='center'>$usuario_actual</td>";
echo "<td align='center'>$fecha_alta</td>";

// presentamos el botón grabar
echo "<td align='center'>";
echo "<input type='button'
       name='btnNuevFinanciamiento'
       id='btnNuevoFinanciamiento'
       onClick='financiamiento.verificaFinanciamiento()'
       title='Pulse para agregar una fuente'
       class='botonagregar'>";
echo "</span>";
echo "</td>";

// cerramos la fila
echo "<tr>";

// cerramos la tabla
echo "</tbody>";
echo "</table>";
?>

<SCRIPT>

    // instanciamos los tooltips
    new jBox('Tooltip', {
        attach: '.tooltip',
        theme: 'TooltipBorder'
    });

</SCRIPT>