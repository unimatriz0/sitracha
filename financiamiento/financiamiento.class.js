/*

 * Nombre: financiamiento.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 07/03/2018
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de la base de datos de
 *              fuentes de financiamiento
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de la tabla de financiamiento
 */
class Financiamiento {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor (){

        // inicializamos las variables de clase
        this.initFinanciamiento();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initFinanciamiento(){

        // declaración de variables
        this.IdFinanciamiento = 0;
        this.Financiamiento = "";
        this.IdUsuario = "";
        this.IdLaboratorio = 0;
        this.FechaAlta = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el contenido el formulario de fuentes de financiamiento
     */
    muestraFinanciamiento(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga el formulario
        $("#form_stock").load("financiamiento/form_financiamiento.php");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {int} idfinanciamiento - clave de la fuente de financiamiento
     * Método que recibe como parámetro la clave del registro que es también
     * la id del elemento del formulario, verifica los datos del formulario
     * y lo envía al método de grabación, si no recibe ningún parámetro
     * asume que se trata de un alta
     */
    verificaFinanciamiento(idfinanciamiento){

        // declaración de variables
        var mensaje;

        // si no recibió la id
        if (typeof(idfinanciamiento) == "undefined"){
            idfinanciamiento = "nuevo";
        } else {
            this.IdFinanciamiento = idfinanciamiento;
        }

        // verifica si ingresó texto
        if (document.getElementById(idfinanciamiento).value == ""){

            // presenta el mensaje y retorna
            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de la fuente";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("idfinanciamiento").focus();
            return false;

        }

        // asigna en la variable de clase y grabamos
        this.Financiamiento = document.getElementById(idfinanciamiento).value;
        this.grabaFinanciamiento();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de las variables de clase ejecuta la consulta
     * en el servidor
     */
    grabaFinanciamiento(){

        // declaración de variables
        var datosFinanciamiento = new FormData();

        // si está editando
        if (this.IdCompania != 0){
            datosFinanciamiento.append("Id", this.IdFinanciamiento);
        }

        // agregamos el nombre
        datosFinanciamiento.append("Financiamiento", this.Financiamiento);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "financiamiento/graba_financiamiento.php",
            type: "POST",
            data: datosFinanciamiento,
            cahe: false,
            contentType: false,
            processData: false,
            success: function(data) {

                // si salío todo bien
                if (data.Error !== false){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // reiniciamos las variables de clase para no
                    // arrastrar basura
                    financiamiento.initFinanciamiento();

                    // recarga el formulario para reflejar los cambios
                    financiamiento.muestraFinanciamiento();

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }
            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento - id de un objeto html
     * @param {int} idfinanciamiento - clave preseleccionada
     * Método que recibe como parámetro la id de un elemento del
     * formulario y carga en ese elemento la nómina de compañias de
     * celular
     */
    nominaFinanciamiento(idelemento, idfinanciamiento){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el texto
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "financiamiento/lista_financiamiento.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // si recibió la clave de la jurisdicción
                    if (typeof(idfinanciamiento) != "undefined"){

                        // si coincide
                        if (data[i].Id == idfinanciamiento){

                            // lo agrega seleccionado
                            $("#" + idelemento).append("<option selected value=" + data[i].Id + ">" + data[i].Financiamiento + "</option>");

                        // si no coinciden
                        } else {

                            // simplemente lo agrega
                            $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Financiamiento + "</option>");

                        }

                    // si no recibió jurisdicción
                    } else {
                        $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Financiamiento + "</option>");
                    }

                }

        }});

    }

}