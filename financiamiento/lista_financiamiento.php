<?php

/**
 *
 * financiamiento/lista_financiamiento.class.php
 *
 * @package     Diagnostico
 * @subpackage  Financiamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina 
 * de fuentes de financiamiento del laboratorio 
 * del usuario activo
 * 
*/

// incluimos e instanciamos la clase
require_once("financiamiento.class.php");
$financiamiento = new Financiamiento();

// obtenemos la nómina
$nomina = $financiamiento->nominaFinanciamiento();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" => $id_financiamiento,
                        "Financiamiento" => $financiamiento);

}

// retornamos el vector
echo json_encode($jsondata);

?>