<?php

/**
 *
 * financiamiento/graba_financiamiento.php
 *
 * @package     Diagnostico
 * @subpackage  Financiamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del formulario y 
 * ejecuta la consulta en la base de datos, retorna el 
 * resultado de la operación 
 * 
*/

// incluimos e instanciamos la clase
require_once("financiamiento.class.php");
$financiamiento = new Financiamiento();

// si recibió la id
if (!empty($_POST["Id"])){
    $financiamiento->setIdFinanciamiento($_POST["Id"]);
}

// asigna el nombre
$financiamiento->setFinanciamiento($_POST["Financiamiento"]);

// grabamos el registro
$resultado = $financiamiento->grabaFinanciamiento();

// retornamos el resultado
echo json_encode(array("Error" => $resultado));
?>