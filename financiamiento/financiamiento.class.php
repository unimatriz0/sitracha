<?php

/**
 *
 * Class Financiamiento | financiamiento/financiamiento.class.php
 *
 * @package     Diagnostico
 * @subpackage  Financiamiento
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (07/03/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla la tabla de fuentes de financiamiento
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Financiamiento{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $IdUsuario;             // clave del usuario
    protected $Link;                  // puntero a la base de datos
    protected $IdFinanciamiento;      // clave del registro
    protected $Financiamiento;        // descripción del financiamiento
    protected $IdLaboratorio;         // clave del laboratorio

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdFinanciamiento = 0;
        $this->Financiamiento = "";

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
            $this->IdLaboratorio = $_SESSION["IdLaboratorio"];
        }
        session_write_close();

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setIdFinanciamiento($idfinanciamiento){
        $this->IdFinanciamiento = $idfinanciamiento;
    }
    public function setFinanciamiento($financiamiento){
        $this->Financiamiento = $financiamiento;
    }

    /**
     * Método que retorna un array asociativo con las fuentes de
     * financiamiento para el laboratorio del usuario activo
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaFinanciamiento(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_financiamiento.id_financiamiento AS id_financiamiento,
                            diagnostico.v_financiamiento.fuente AS financiamiento,
                            diagnostico.v_financiamiento.usuario AS usuario,
                            diagnostico.v_financiamiento.fecha_alta AS fecha_alta
                     FROM diagnostico.v_financiamiento
                     WHERE diagnostico.v_financiamiento.id_laboratorio = '$this->IdLaboratorio'
                     ORDER BY diagnostico.v_financiamiento.fuente;";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que ejecuta la consulta de edición o inserción según el caso
     * retorna la clave del registro afectado
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int - clave del registro insertado / modificado
     */
    public function grabaFinanciamiento(){

        // si está insertando
        if ($this->IdFinanciamiento == 0){
            $this->nuevoFinanciamiento();
        } else {
            $this->editaFinanciamiento();
        }

        // retornamos la clave
        return $this->IdFinanciamiento;

    }

    /**
     * Método que ejecuta la consulta de inserción
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function nuevoFinanciamiento(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.financiamiento
                            (fuente,
                             id_laboratorio,
                             id_usuario)
                            VALUES
                            (:financiamiento,
                             :id_laboratorio,
                             :id_usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":financiamiento", $this->Financiamiento);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->IdFinanciamiento = $this->Link->lastInsertId();

    }

    /**
     * Método que ejecuta la consulta de edición
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function editaFinanciamiento(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.financiamiento SET
                            fuente = :financiamiento,
                            id_laboratorio = :id_laboratorio,
                            id_usuario = :id_usuario
                     WHERE diagnostico.financiamiento.id = :id_financiamiento;";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":financiamiento", $this->Financiamiento);
        $psInsertar->bindParam(":id_laboratorio", $this->IdLaboratorio);
        $psInsertar->bindParam(":id_usuario", $this->IdUsuario);
        $psInsertar->bindParam(":id_financiamiento", $this->IdFinanciamiento);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}