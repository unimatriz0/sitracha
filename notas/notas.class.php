<?php

    /*

    Archivo: notas.class.php
    Proyecto: CCE
    Autor: Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Institución:INP - Dr. Mario Fatala Chaben
    Fecha: 23-02-2017
    Version: 1.0
    Licencia: GPL
    Comentarios: Clase que genera las notas de los responsables jurisdiccionales
                 al nivel central informando los laboratorios participantes

    */

/*

    Atención, esta clase extiende la clase tfpdf que a su vez extiende
    la clase fpdf, la diferencia es que permite definir y generar documentos
    pdf con página de códigos UTF8, el funcionamiento es el mismo que en
    la clase original.

    Sin embargo, la clase para ahorrar espacio regenera las fuentes que
    son utilizadas en el documento, si migramos el sistema y cambia el
    path de la aplicación arroja un error señalando que no encuentra las
    fuentes.

    Para obligar a que regenere las fuentes basta con eliminar todos los
    archivos dat y aquellos php que tengan nombres de fuentes del directorio
    /font/unifont/ dejando solamente los archivos ttf y el archivo ttfonts.php
    que es el que se encarga de generar las fuentes

    Si se desean incluir otras fuentes en el documento, bastaría con
    copiar los archivos ttf en este directorio y luego al incluirlos en
    el documento el sistema se encarga automáticamente de generar los dat

*/

// define la ruta a las fuentes pdf
define('FPDF_FONTPATH',$_SERVER["DOCUMENT_ROOT"] . '/CCE3/clases/fpdf/font');

// inclusión de archivos
require_once ("conexion.class.php");
require_once ("herramientas.class.php");
require_once ("fpdf/tfpdf.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

// definición de la clase
class Notas extends tFPDF{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                // puntero a la base de datos
    protected $Jurisdiccion;        // jurisdicción del usuario
    protected $IdUsuario;           // id del usuario activo
    protected $Nombre;              // nombre del usuario
    protected $Cargo;               // cargo que ocupa el usuario
    protected $nominaLaboratorios;  // vector con los datos de los laboratorios
    protected $Alto;                // altura de las líneas y celdas
    protected $Herramientas;        // clase de utilidades

    // variables utilizadas por la extensión write tag que permite
    // definir tags de tipos de párrafo e identado
    protected $wLine;                     // Maximum width of the line
    protected $hLine;                     // Height of the line
    protected $Text;                      // Text to display
    protected $border;
    protected $align;                     // Justification of the text
    protected $fill;
    protected $Padding;
    protected $lPadding;
    protected $tPadding;
    protected $bPadding;
    protected $rPadding;
    protected $TagStyle;                  // Style for each tag
    protected $Indent;
    protected $Space;                     // Minimum space between words
    protected $PileStyle;
    protected $Line2Print;                // Line to display
    protected $NextLineBegin;             // Buffer between lines
    protected $TagName;
    protected $Delta;                     // Maximum width minus width
    protected $StringLength;
    protected $LineLength;
    protected $wTextLine;                 // Width minus paddings
    protected $nbSpace;                   // Number of spaces in the line
    protected $Xini;                      // Initial position
    protected $href;                      // Current URL
    protected $TagHref;                   // URL for a cell

    // constructor de la clase
    function __construct (){

        // instanciamos la clase de utilidades
        $this->Herramientas = new Herramientas();

        // inicializamos las variables
        $this->nominaLaboratorios = "";
        $this->Alto = 8;

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // iniciamos sesión
        session_start();

        // si existe la sesión
        if(isset($_SESSION["ID"])){

            // obtenemos los datos de la sesión
            $this->IdUsuario = $_SESSION["ID"];
            $this->Jurisdiccion = $_SESSION["Jurisdiccion"];

        // si no existe la sesión
        } else {

            // abandona por error
            echo "No existen datos de la sesión actual";
            exit;

        }

        // cerramos sesión
        session_write_close();

        // llamamos al constructor de la clase padre
        parent::__construct();

        // establecemos las propiedades
        $this->SetAuthor("Claudio Invernizzi");
        $this->SetCreator("Sistema CCE");
        $this->SetSubject("Notas de los Responsables", true);
        $this->SetTitle("Notas de los Responsables", true);
        $this->SetAutoPageBreak(true, 10);

        // setea los márgenes (left, top, right)
        $this->SetMargins(3,3,1.5);

        // agrega una fuente unicode
        $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf', true);

        // definimos los tags que vamos a usar de tipo de párrafo
        // y de fuentes
        $this->SetStyle("p", "DejaVu","N", $this->Fuente, "0,0,0", "55");
        $this->SetStyle("n", "DejaVu","B", $this->Fuente, "0,0,0");

    }

    // destructor de la clase
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * @param nominaLaboratorios vector con los datos
     * Asigna el vector a la variable de clase
     */
    public function setNominaLaboratorios($nomina){
        $this->nominaLaboratorios = $nomina;
    }

    // @return cadena con el documento
    // método principal y público que genera la nota y luego la
    // tabla adjunta, retorna el objeto pdf como una cadena
    public function imprimirNota(){

        // inicializamos las variables
        $nombre = "";
        $cargo = "";
        
        // obtenemos los datos del usuario
        $consulta = "SELECT cce.responsables.NOMBRE AS nombre,
                            cce.responsables.CARGO AS cargo
                     FROM cce.responsables
                     WHERE cce.responsables.ID = '$this->IdUsuario'; ";

        // obtenemos el registro y lo asignamos
        $resultado = $this->Link->query($consulta);
        $registro = $resultado->fetch(PDO::FETCH_ASSOC);
        $datosResponsable = array_change_key_case($registro, CASE_LOWER);
        extract($datosResponsable);
        $this->Nombre = $nombre;
        $this->Cargo = $cargo;

        // agregamos la nota dirigida al nivel central
        $this->encabezadoNota();

        // agregamos la nómina de laboratorios
        $this->listarLaboratorios();

        // asignamos el documento a una variable
        $documento = $this->Output("","S");

        // insertamos la nota en la base de datos

        // retornamos el documento
        return $documento;

    }

    // método protegido que agrega la página inicial con el texto
    // dirigiendo la nota al nivel central
    protected function encabezadoNota(){

        // agrega la página
        $this->AddPage("P", "A4");

        // seteamos la fuente
        $this->SetFont("DejaVu", "", 12);

        // presenta los logos no usamos la función header porque
        // como las páginas tienen distinto formato debemos
        // cambiar la posición
        $this->Image("../imagenes/logo_msal.jpg", 10, 10, 40, 25);
        $this->Image("../imagenes/logo.png", 150, 10, 40, 25);

        // fijamos la posición de impresión
        $this->SetY(50);

        // obtenemos la fecha en letras
        $fechaLetras = $this->Herramientas->fechaLetras(date('d/m/Y'));

        // presenta la provincia y la fecha
        $texto = $this->Jurisdiccion . " " . $fechaLetras;
        $this->Cell(100, $this->Alto, $texto, 0, 1, "R");

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el encabezado
        $texto = "Sra. Jefa del\n";
        $texto .= "Departamento de Diagnóstico del\n";
        $texto .= "Instituto Nacional de Parasitología\n";
        $texto .= "Dr. Mario Fatala Chaben\n";
        $texto .= "Dra. Karenina Scollo";
        $this->MultiCell(0, $this->Alto, $texto, 0, "L");

        // inserta un separador
        $this->Ln($this->Alto);

        // inserta el texto
        $texto = "<p>Por intermedio de la presente me es grado dirigirme a la ";
        $texto .= "Señora Jefa del Departamento de Diagnóstico, a fin de ";
        $texto .= "adjuntar a la presente la nómina de laboratorios de la ";
        $texto .= "Jurisdicción de $this->Jurisdiccion que participarán ";
        $texto .= "del Control de Calidad Externo durante el año " . date('Y') . "</p>";
        $texto .= "<p>Sin otro particular le saluda atte.</p>";
        $this->WriteTag(0, $this->Alto, $texto,0 ,"J", 0, 0);

        // inserta un separador
        $this->Ln($this->Alto);

        // presenta el nombre y el cargo
        $texto = $this->Nombre . "\n";
        $texto .= $this->Cargo;
        $this->MultiCell(0, $this->Alto, $texto, 0, "C");

    }

    // método protegido que agrega la nómina de laboratorios como adjunto
    protected function listarLaboratorios(){

        // presenta el encabezado de la tabla
        $this->encabezadoTabla();

        // recorre el vector
        foreach($this->nominaLaboratorios AS $registro){

            // imprimimos la línea
            $this->imprimeLaboratorio($registro);

        }

    }

    /**
     * @param registro, matriz con los datos de un laboratorio
     * Método que recibe los datos del laboratorio y los presenta
     * verificando la posición del cabezal y la longitud de los textos
     */
    protected function imprimeLaboratorio($registro){

        // inicializamos las variables
        $laboratorio = "";
        $direccion = "";
        $localidad = "";
        $recibe = "";
        
        // obtenemos el registro
        extract($registro);

        // seteamos la fuente por las dudas
        $this->SetFont("DejaVu", "", 10);

        // verifica si alguna de las celdas supera el tamaño
        if ($this->GetStringWidth($laboratorio) > 79 || $this->GetStringWidth($direccion) > 79 || $this->GetStringWidth($localidad) > 59){

            // setea el switch de multilinea
            $multilinea = 2;

        // si ninguno supera
        } else {

            // setea el switch
            $multilinea = 1;

        }

        // verifica la posición de impresión y en todo caso
        // llama una nueva página
        if ($this->GetY() > 190 && $multilinea == 1){
            $this->encabezadoTabla();
        } elseif ($this->GetY() > 180 && $multilinea == 2){
            $this->encabezadoTabla();
        }

        // presentamos los datos verificando la longitud

        // si el laboratorio es mayor
        if ($this->GetStringWidth($laboratorio) > 79){

            // llama el multirow
            $this->MultiRow($laboratorio, 80);

        // si entra
        } else {

            // lo imprime en una celda
            $this->Cell(80, $this->Alto * $multilinea, $laboratorio, 1, 0, "L");

        }

        // verifica la longitud de la localidad
        $this->Cell(60, $this->Alto * $multilinea, $localidad, 1, 0, "L");

        // verifica la longitud de la dirección
        $this->Cell(80, $this->Alto * $multilinea, $direccion, 1, 0, "L");

        // si recibe muestras directamente
        if ($recibe == "Si"){
            $recibe_muestras = "Laboratorio";
        }

        // presenta quien recibe las muestras
        $this->Cell(60, $this->Alto * $multilinea, $recibe_muestras, 1, 1, "L");

    }

    /**
     * Método protegido que imprime los encabezados de la tabla
     */
    protected function encabezadoTabla(){

        // agrega una página apaisada
        $this->AddPage("L", "A4");

        // presenta los logos
        $this->Image("../imagenes/logo_msal.jpg", 10, 10, 40, 30);
        $this->Image("../imagenes/logo.png", 230, 10, 40, 30);

        // fijamos la posición
        $this->SetY(50, true);

        // seteamos la fuente
        $this->SetFont("DejaVu", "B", 12);

        // imprime los encabezados
        $this->Cell(80, $this->Alto, "Laboratorio", 1, 0, "L");
        $this->Cell(60, $this->Alto, "Localidad", 1, 0, "L");
        $this->Cell(80, $this->Alto, "Dirección", 1, 0, "L");
        $this->Cell(60, $this->Alto, "Recibe", 1, 1, "L");

        // setea la fuente
        $this->SetFont("DejaVu", "", 10);

    }

    /**
     * @param texto, cadena con el texto a imprimir
     * método protegido llamado cuando una texto no entra en un renglón
     * en realidad usa un multicell y dibuja el marco para luego posicionar
     * el puntero de impresión en la posición original, recibe como parámetro
     * el texto a imprimir, recibe como parámetro el texto y el ancho de la
     * columna
     */
    protected function MultiRow($texto, $ancho_columna){

        // almacenamos la posición actual
        $x = $this->GetX();
        $y = $this->GetY();

        // imprime el texto
        $this->MultiCell($ancho_columna, $this->Alto, $texto, 1, "L");

        // posiciona el puntero de impresión a la derecha
        // de la celda
        $this->SetXY($x + $ancho_columna, $y);

    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // extensión tags tomada de la página de fpdf la insertamos aquí porque //
    // php no soporta multiherencia                                         //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////

    // funciones públicas
    public function WriteTag($w, $h, $txt, $border=0, $align="J", $fill=false, $padding=0){
        $this->wLine=$w;
        $this->hLine=$h;
        $this->Text=trim($txt);
        $this->Text=preg_replace("/\n|\r|\t/","",$this->Text);
        $this->border=$border;
        $this->align=$align;
        $this->fill=$fill;
        $this->Padding=$padding;

        $this->Xini=$this->GetX();
        $this->href="";
        $this->PileStyle=array();
        $this->TagHref=array();
        $this->LastLine=false;

        $this->SetSpace();
        $this->Padding();
        $this->LineLength();
        $this->BorderTop();

        while($this->Text!=""){
            $this->MakeLine();
            $this->PrintLine();
        }

        $this->BorderBottom();

    }

    // funciones privadas (que declaramos como protegidas)

    // Minimal space between words
    protected function SetSpace(){
        $tag=$this->Parser($this->Text);
        $this->FindStyle($tag[2],0);
        $this->DoStyle(0);
        $this->Space=$this->GetStringWidth(" ");
    }

    protected function SetStyle($tag, $family, $style, $size, $color, $indent=-1){
         $tag=trim($tag);
         $this->TagStyle[$tag]['family']=trim($family);
         $this->TagStyle[$tag]['style']=trim($style);
         $this->TagStyle[$tag]['size']=trim($size);
         $this->TagStyle[$tag]['color']=trim($color);
         $this->TagStyle[$tag]['indent']=$indent;
    }

    protected function Padding(){
        if(preg_match("/^.+,/",$this->Padding)) {
            $tab=explode(",",$this->Padding);
            $this->lPadding=$tab[0];
            $this->tPadding=$tab[1];
            if(isset($tab[2]))
                $this->bPadding=$tab[2];
            else
                $this->bPadding=$this->tPadding;
            if(isset($tab[3]))
                $this->rPadding=$tab[3];
            else
                $this->rPadding=$this->lPadding;
        } else {
            $this->lPadding=$this->Padding;
            $this->tPadding=$this->Padding;
            $this->bPadding=$this->Padding;
            $this->rPadding=$this->Padding;
        }
        if($this->tPadding<$this->LineWidth)
            $this->tPadding=$this->LineWidth;
    }

    protected function LineLength(){
        if($this->wLine==0)
            $this->wLine=$this->w - $this->Xini - $this->rMargin;

        $this->wTextLine = $this->wLine - $this->lPadding - $this->rPadding;
    }

    protected function BorderTop(){
        $border=0;
        if($this->border==1)
            $border="TLR";
        $this->Cell($this->wLine,$this->tPadding,"",$border,0,'C',$this->fill);
        $y=$this->GetY()+$this->tPadding;
        $this->SetXY($this->Xini,$y);
    }


    protected function BorderBottom(){
        $border=0;
        if($this->border==1)
            $border="BLR";
        $this->Cell($this->wLine,$this->bPadding,"",$border,0,'C',$this->fill);
    }

    protected function DoStyle($tag){ // Applies a style
        $tag=trim($tag);
        $this->SetFont($this->TagStyle[$tag]['family'],
            $this->TagStyle[$tag]['style'],
            $this->TagStyle[$tag]['size']);

        $tab=explode(",",$this->TagStyle[$tag]['color']);
        if(count($tab)==1)
            $this->SetTextColor($tab[0]);
        else
            $this->SetTextColor($tab[0],$tab[1],$tab[2]);
    }

    protected function FindStyle($tag, $ind){ // Inheritance from parent elements
        $tag=trim($tag);

        // Family
        if($this->TagStyle[$tag]['family']!="")
            $family=$this->TagStyle[$tag]['family'];
        else
        {
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                if($this->TagStyle[$val]['family']!="") {
                    $family=$this->TagStyle[$val]['family'];
                    break;
                }
            }
        }

        // Style
        $style="";
        $style1=strtoupper($this->TagStyle[$tag]['style']);
        if($style1!="N")
        {
            $bold=false;
            $italic=false;
            $underline=false;
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                $style1=strtoupper($this->TagStyle[$val]['style']);
                if($style1=="N")
                    break;
                else
                {
                    if(strpos($style1,"B")!==false)
                        $bold=true;
                    if(strpos($style1,"I")!==false)
                        $italic=true;
                    if(strpos($style1,"U")!==false)
                        $underline=true;
                }
            }
            if($bold)
                $style.="B";
            if($italic)
                $style.="I";
            if($underline)
                $style.="U";
        }

        // Size
        if($this->TagStyle[$tag]['size']!=0)
            $size=$this->TagStyle[$tag]['size'];
        else
        {
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                if($this->TagStyle[$val]['size']!=0) {
                    $size=$this->TagStyle[$val]['size'];
                    break;
                }
            }
        }

        // Color
        if($this->TagStyle[$tag]['color']!="")
            $color=$this->TagStyle[$tag]['color'];
        else
        {
            reset($this->PileStyle);
            while(list($k,$val)=each($this->PileStyle))
            {
                $val=trim($val);
                if($this->TagStyle[$val]['color']!="") {
                    $color=$this->TagStyle[$val]['color'];
                    break;
                }
            }
        }

        // Result
        $this->TagStyle[$ind]['family']=$family;
        $this->TagStyle[$ind]['style']=$style;
        $this->TagStyle[$ind]['size']=$size;
        $this->TagStyle[$ind]['color']=$color;
        $this->TagStyle[$ind]['indent']=$this->TagStyle[$tag]['indent'];
    }


    protected function Parser($text){
        $tab=array();
        // Closing tag
        if(preg_match("|^(</([^>]+)>)|",$text,$regs)) {
            $tab[1]="c";
            $tab[2]=trim($regs[2]);
        }
        // Opening tag
        else if(preg_match("|^(<([^>]+)>)|",$text,$regs)) {
            $regs[2]=preg_replace("/^a/","a ",$regs[2]);
            $tab[1]="o";
            $tab[2]=trim($regs[2]);

            // Presence of attributes
            if(preg_match("/(.+) (.+)='(.+)'/",$regs[2])) {
                $tab1=preg_split("/ +/",$regs[2]);
                $tab[2]=trim($tab1[0]);
                while(list($i,$couple)=each($tab1))
                {
                    if($i>0) {
                        $tab2=explode("=",$couple);
                        $tab2[0]=trim($tab2[0]);
                        $tab2[1]=trim($tab2[1]);
                        $end=strlen($tab2[1])-2;
                        $tab[$tab2[0]]=substr($tab2[1],1,$end);
                    }
                }
            }
        }
         // Space
         else if(preg_match("/^( )/",$text,$regs)) {
            $tab[1]="s";
            $tab[2]=' ';
        }
        // Text
        else if(preg_match("/^([^< ]+)/",$text,$regs)) {
            $tab[1]="t";
            $tab[2]=trim($regs[1]);
        }

        $begin=strlen($regs[1]);
        $end=strlen($text);
        $text=substr($text, $begin, $end);
        $tab[0]=$text;

        return $tab;
    }

    protected function MakeLine(){
        $this->Text.=" ";
        $this->LineLength=array();
        $this->TagHref=array();
        $Length=0;
        $this->nbSpace=0;

        $i=$this->BeginLine();
        $this->TagName=array();

        if($i==0) {
            $Length=$this->StringLength[0];
            $this->TagName[0]=1;
            $this->TagHref[0]=$this->href;
        }

        while($Length<$this->wTextLine){
            $tab=$this->Parser($this->Text);
            $this->Text=$tab[0];
            if($this->Text=="") {
                $this->LastLine=true;
                break;
            }

            if($tab[1]=="o") {
                array_unshift($this->PileStyle,$tab[2]);
                $this->FindStyle($this->PileStyle[0],$i+1);

                $this->DoStyle($i+1);
                $this->TagName[$i+1]=1;
                if($this->TagStyle[$tab[2]]['indent']!=-1) {
                    $Length+=$this->TagStyle[$tab[2]]['indent'];
                    $this->Indent=$this->TagStyle[$tab[2]]['indent'];
                }
                if($tab[2]=="a")
                    $this->href=$tab['href'];
            }

            if($tab[1]=="c") {
                array_shift($this->PileStyle);
                if(isset($this->PileStyle[0]))
                {
                    $this->FindStyle($this->PileStyle[0],$i+1);
                    $this->DoStyle($i+1);
                }
                $this->TagName[$i+1]=1;
                if($this->TagStyle[$tab[2]]['indent']!=-1) {
                    $this->LastLine=true;
                    $this->Text=trim($this->Text);
                    break;
                }
                if($tab[2]=="a")
                    $this->href="";
            }

            if($tab[1]=="s") {
                $i++;
                $Length+=$this->Space;
                $this->Line2Print[$i]="";
                if($this->href!="")
                    $this->TagHref[$i]=$this->href;
            }

            if($tab[1]=="t") {
                $i++;
                $this->StringLength[$i]=$this->GetStringWidth($tab[2]);
                $Length+=$this->StringLength[$i];
                $this->LineLength[$i]=$Length;
                $this->Line2Print[$i]=$tab[2];
                if($this->href!="")
                    $this->TagHref[$i]=$this->href;
             }

        }

        trim($this->Text);
        if($Length>$this->wTextLine || $this->LastLine==true)
            $this->EndLine();
    }

    protected function BeginLine(){
        $this->Line2Print=array();
        $this->StringLength=array();

        if(isset($this->PileStyle[0])){
            $this->FindStyle($this->PileStyle[0],0);
            $this->DoStyle(0);
        }

        if(count($this->NextLineBegin)>0){
            $this->Line2Print[0]=$this->NextLineBegin['text'];
            $this->StringLength[0]=$this->NextLineBegin['length'];
            $this->NextLineBegin=array();
            $i=0;
        } else {
            preg_match("/^(( *(<([^>]+)>)* *)*)(.*)/",$this->Text,$regs);
            $regs[1]=str_replace(" ", "", $regs[1]);
            $this->Text=$regs[1].$regs[5];
            $i=-1;
        }

        return $i;
    }


    protected function EndLine(){
        if(end($this->Line2Print)!="" && $this->LastLine==false) {
            $this->NextLineBegin['text']=array_pop($this->Line2Print);
            $this->NextLineBegin['length']=end($this->StringLength);
            array_pop($this->LineLength);
        }

        while(end($this->Line2Print)==="")
            array_pop($this->Line2Print);

        $this->Delta=$this->wTextLine-end($this->LineLength);

        $this->nbSpace=0;
        for($i=0; $i<count($this->Line2Print); $i++) {
            if($this->Line2Print[$i]=="")
                $this->nbSpace++;
        }
    }

    protected function PrintLine(){
        $border=0;
        if($this->border==1)
            $border="LR";
        $this->Cell($this->wLine,$this->hLine,"",$border,0,'C',$this->fill);
        $y=$this->GetY();
        $this->SetXY($this->Xini+$this->lPadding,$y);

        if($this->Indent!=-1) {
            if($this->Indent!=0)
                $this->Cell($this->Indent,$this->hLine);
            $this->Indent=-1;
        }

        $space=$this->LineAlign();
        $this->DoStyle(0);
        for($i=0; $i<count($this->Line2Print); $i++){
            if(isset($this->TagName[$i]))
                $this->DoStyle($i);
            if(isset($this->TagHref[$i]))
                $href=$this->TagHref[$i];
            else
                $href='';
            if($this->Line2Print[$i]=="")
                $this->Cell($space,$this->hLine,"         ",0,0,'C',false,$href);
            else
                $this->Cell($this->StringLength[$i],$this->hLine,$this->Line2Print[$i],0,0,'C',false,$href);
        }

        $this->LineBreak();
        if($this->LastLine && $this->Text!="")
            $this->EndParagraph();
        $this->LastLine=false;
    }

    protected function LineAlign(){
        $space=$this->Space;
        if($this->align=="J") {
            if($this->nbSpace!=0)
                $space=$this->Space + ($this->Delta/$this->nbSpace);
            if($this->LastLine)
                $space=$this->Space;
        }

        if($this->align=="R")
            $this->Cell($this->Delta,$this->hLine);

        if($this->align=="C")
            $this->Cell($this->Delta/2,$this->hLine);

        return $space;
    }

    protected function LineBreak(){
        $x=$this->Xini;
        $y=$this->GetY()+$this->hLine;
        $this->SetXY($x,$y);
    }

    protected function EndParagraph(){
        $border=0;
        if($this->border==1)
            $border="LR";
        $this->Cell($this->wLine,$this->hLine/2,"",$border,0,'C',$this->fill);
        $x=$this->Xini;
        $y=$this->GetY()+$this->hLine/2;
        $this->SetXY($x,$y);
    }

}
?>
