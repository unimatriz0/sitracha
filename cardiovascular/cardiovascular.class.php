<?php

/**
 *
 * Class Cardiovascular | cardiovascular/cardiovascular.class.php
 *
 * @package     Diagnostico
 * @subpackage  Cardiovascular
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * antecedentes cardiovasculares
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Cardiovascular {

    // declaración de variables
    protected $Link;               // puntero a la base de datos
    protected $Id;                 // clave del registro
    protected $Paciente;           // clave del paciente
    protected $Visita;             // clave de la visita
    protected $AuscNormal;         // 0 falso 1 verdadero
    protected $Irregular;          // 0 falso 1 verdadero
    protected $Tercer;             // 0 falso 1 verdadero
    protected $Cuarto;             // 0 falso 1 verdadero
    protected $Eyectivo;           // 0 falso 1 verdadero
    protected $Regurgitativo;      // 0 falso 1 verdadero
    protected $SinSistolico;       // 0 falso 1 verdadero
    protected $Aortico;            // 0 falso 1 verdadero
    protected $DiastolicoMitral;   // 0 falso 1 verdadero
    protected $SinDiastolico;      // 0 falso 1 verdadero
    protected $Hepatomegalia;      // 0 falso 1 verdadero
    protected $Esplenomegalia;     // 0 falso 1 verdadero
    protected $Ingurgitacion;      // 0 falso 1 verdadero
    protected $IdUsuario;          // clave del usuario
    protected $Usuario;            // nombre del usuario
    protected $Fecha;              // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initCardiovascular();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que inicializa las variables de clase, llamado
     * desde el constructor o desde la consulta cuando no
     * encuentra registros
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    protected function initCardiovascular(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Visita = 0;
        $this->AuscNormal = 0;
        $this->Irregular = 0;
        $this->Tercer = 0;
        $this->Cuarto = 0;
        $this->Eyectivo = 0;
        $this->Regurgitativo = 0;
        $this->SinSistolico = 0;
        $this->Aortico = 0;
        $this->DiastolicoMitral = 0;
        $this->SinDiastolico = 0;
        $this->Esplenomegalia = 0;
        $this->Hepatomegalia = 0;
        $this->Ingurgitacion = 0;
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setAuscultacion($auscultacion){
        $this->AuscNormal = $auscultacion;
    }
    public function setIrregular($irregular){
        $this->Irregular = $irregular;
    }
    public function setTercer($tercer){
        $this->Tercer = $tercer;
    }
    public function setCuarto($cuarto){
        $this->Cuarto = $cuarto;
    }
    public function setEyectivo($eyectivo){
        $this->Eyectivo = $eyectivo;
    }
    public function setRegurgitativo($regurgitativo){
        $this->Regurgitativo = $regurgitativo;
    }
    public function setSinSistolico($sistolico){
        $this->SinSistolico = $sistolico;
    }
    public function setAortico($aortico){
        $this->Aortico = $aortico;
    }
    public function setDiastolicoMitral($diastolico){
        $this->DiastolicoMitral = $diastolico;
    }
    public function setSinDiastolico($diastolico){
        $this->SinDiastolico = $diastolico;
    }
    public function setEsplenomegalia($esplenomegalia){
        $this->Esplenomegalia = $esplenomegalia;
    }
    public function setHepatomegalia($hepatomegalia){
        $this->Hepatomegalia = $hepatomegalia;
    }
    public function setIngurtitacion($ingurgitacion){
        $this->Ingurgitacion = $ingurgitacion;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getAuscultacion(){
        return $this->AuscNormal;
    }
    public function getIrregular(){
        return $this->Irregular;
    }
    public function getTercer(){
        return $this->Tercer;
    }
    public function getCuarto(){
        return $this->Cuarto;
    }
    public function getEyectivo(){
        return $this->Eyectivo;
    }
    public function getRegurgitativo(){
        return $this->Regurgitativo;
    }
    public function getSinSistolico(){
        return $this->SinDiastolico;
    }
    public function getAortico(){
        return $this->Aortico;
    }
    public function getDiastolicoMitral(){
        return $this->DiastolicoMitral;
    }
    public function getSinDiastolico(){
        return $this->SinDiastolico;
    }
    public function getEsplenomegalia(){
        return $this->Esplenomegalia;
    }
    public function getHepatomegalia(){
        return $this->Hepatomegalia;
    }
    public function getIngurgitacion(){
        return $this->Ingurgitacion;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave de un paciente
     * @return resultset con los registros
     * Método que retorna el array de resultados cardiológicos
     * correspondientes a un paciente
     */
    public function nominaCardiovascular($idpaciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_cardiovascular.id AS id,
                            diagnostico.v_cardiovascular.paciente AS paciente,
                            diagnostico.v_cardiovascular.idvisita AS visita,
                            diagnostico.v_cardiovascular.ausnormal AS ausnormal,
                            diagnostico.v_cardiovascular.irregular AS irregular,
                            diagnostico.v_cardiovascular.3er AS tercer,
                            diagnostico.v_cardiovascular.4to AS cuarto,
                            diagnostico.v_cardiovascular.eyectivo AS eyectivo,
                            diagnostico.v_cardiovascular.regurgitativo AS regurgitativo,
                            diagnostico.v_cardiovascular.sinsistolico AS sinsistolico,
                            diagnostico.v_cardiovascular.aortico AS aortico,
                            diagnostico.v_cardiovascular.diastolicomitral AS diastolicomitral,
                            diagnostico.v_cardiovascular.sindiastolico AS sindiastolico,
                            diagnostico.v_cardiovascular.hepatomegalia AS hepatomegalia,
                            diagnostico.v_cardiovascular.esplenomegalia AS esplenomegalia,
                            diagnostico.v_cardiovascular.ingurgitacion AS ingurgitacion,
                            diagnostico.v_cardiovascular.usuario AS usuario,
                            diagnostico.v_cardiovascular.fecha AS fecha
                     FROM diagnostico.v_cardiovascular
                     WHERE diagnostico.v_cardiovascular.paciente = '$idpaciente'; ";
        $resultado = $this->Link->query($consulta);

        // retornamos el vector
        return $resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idvisita clave de la visita
     * Método que recibe como parámetro la clave de un
     * registro y asigna en las variables de clase los
     * valores de la entrevista
     */
    public function getDatosCardiovascular($idvisita){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_cardiovascular.id AS id,
                            diagnostico.v_cardiovascular.paciente AS paciente,
                            diagnostico.v_cardiovascular.idvisita AS visita,
                            diagnostico.v_cardiovascular.ausnormal AS ausnormal,
                            diagnostico.v_cardiovascular.irregular AS irregular,
                            diagnostico.v_cardiovascular.3er AS tercer,
                            diagnostico.v_cardiovascular.4to AS cuarto,
                            diagnostico.v_cardiovascular.eyectivo AS eyectivo,
                            diagnostico.v_cardiovascular.regurgitativo AS regurgitativo,
                            diagnostico.v_cardiovascular.sinsistolico AS sinsistolico,
                            diagnostico.v_cardiovascular.aortico AS aortico,
                            diagnostico.v_cardiovascular.diastolicomitral AS diastolicomitral,
                            diagnostico.v_cardiovascular.sindiastolico AS sindiastolico,
                            diagnostico.v_cardiovascular.hepatomegalia AS hepatomegalia,
                            diagnostico.v_cardiovascular.esplenomegalia AS esplenomegalia,
                            diagnostico.v_cardiovascular.ingurgitacion AS ingurgitacion,
                            diagnostico.v_cardiovascular.usuario AS usuario,
                            diagnostico.v_cardiovascular.fecha AS fecha
                     FROM diagnostico.v_cardiovascular
                     WHERE diagnostico.v_cardiovascular.idvisita = '$idvisita'; ";
        $resultado = $this->Link->query($consulta);

        // si hubo registros
        if ($resultado->rowCount() != 0){

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // obtenemos el registro y asignamos en las
            // variables de clase
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Visita = $visita;
            $this->AuscNormal = $ausnormal;
            $this->Irregular = $irregular;
            $this->Tercer = $tercer;
            $this->Cuarto = $cuarto;
            $this->Eyectivo = $eyectivo;
            $this->Regurgitativo = $regurgitativo;
            $this->SinSistolico = $sinsistolico;
            $this->Aortico = $aortico;
            $this->DiastolicoMitral = $diastolicomitral;
            $this->SinDiastolico = $sindiastolico;
            $this->Hepatomegalia = $hepatomegalia;
            $this->Esplenomegalia = $esplenomegalia;
            $this->Ingurgitacion = $ingurgitacion;
            $this->Usuario = $usuario;
            $this->Fecha = $fecha;

        // si no hubo registros
        } else {

            // inicializamos las variables
            $this->initCardiovascular();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id entero con la clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la id del registro
     * afectado
     */
    public function grabaCardiovascular(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoCardiovascular();
        } else {
            $this->editaCardiovascular();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un
     * nuevo registro
     */
    protected function nuevoCardiovascular(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.cardiovascular
                                (paciente,
                                 visita,
                                 ausnormal,
                                 irregular,
                                 3er,
                                 4to,
                                 eyectivo,
                                 regurgitativo,
                                 sinsistolico,
                                 aortico,
                                 diastolicomitral,
                                 sindiastolico,
                                 hepatomegalia,
                                 esplenomegalia,
                                 ingurgitacion,
                                 usuario)
                                VALUES
                                (:paciente,
                                 :visita,
                                 :ausnormal,
                                 :irregular,
                                 :tercer,
                                 :cuarto,
                                 :eyectivo,
                                 :regurgitativo,
                                 :sinsistolico,
                                 :aortico,
                                 :diastolicomitral,
                                 :sindiastolico,
                                 :hepatomegalia,
                                 :esplenomegalia,
                                 :ingurgitacion,
                                 :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",         $this->Paciente);
        $psInsertar->bindParam(":visita",           $this->Visita);
        $psInsertar->bindParam(":ausnormal",        $this->AuscNormal);
        $psInsertar->bindParam(":irregular",        $this->Irregular);
        $psInsertar->bindParam(":tercer",           $this->Tercer);
        $psInsertar->bindParam(":cuarto",           $this->Cuarto);
        $psInsertar->bindParam(":eyectivo",         $this->Eyectivo);
        $psInsertar->bindParam(":regurgitativo",    $this->Regurgitativo);
        $psInsertar->bindParam(":sinsistolico",     $this->SinSistolico);
        $psInsertar->bindParam(":aortico",          $this->Aortico);
        $psInsertar->bindParam(":diastolicomitral", $this->DiastolicoMitral);
        $psInsertar->bindParam(":sindiastolico",    $this->SinDiastolico);
        $psInsertar->bindParam(":hepatomegalia",    $this->Hepatomegalia);
        $psInsertar->bindParam(":esplenomegalia",   $this->Esplenomegalia);
        $psInsertar->bindParam(":ingurgitacion",    $this->Ingurgitacion);
        $psInsertar->bindParam(":usuario",          $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición del
     * registro activo
     */
    protected function editaCardiovascular(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.cardiovascular SET
                            ausnormal = :ausnormal,
                            irregular = :irregular,
                            3er = :tercer,
                            4to = :cuarto,
                            eyectivo = :eyectivo,
                            regurgitativo = :regurgitativo,
                            sinsistolico = :sinsistolico,
                            aortico = :aortico,
                            diastolicomitral = :diastolicomitral,
                            sindiastolico = :sindiastolico,
                            hepatomegalia = :hepatomegalia,
                            esplenomegalia = :esplenomegalia,
                            ingurgitacion = :ingurgitacion,
                            usuario = :idusuario
                     WHERE diagnostico.cardiovascular.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":ausnormal",        $this->AuscNormal);
        $psInsertar->bindParam(":irregular",        $this->Irregular);
        $psInsertar->bindParam(":tercer",           $this->Tercer);
        $psInsertar->bindParam(":cuarto",           $this->Cuarto);
        $psInsertar->bindParam(":eyectivo",         $this->Eyectivo);
        $psInsertar->bindParam(":regurgitativo",    $this->Regurgitativo);
        $psInsertar->bindParam(":sinsistolico",     $this->SinSistolico);
        $psInsertar->bindParam(":aortico",          $this->Aortico);
        $psInsertar->bindParam(":diastolicomitral", $this->DiastolicoMitral);
        $psInsertar->bindParam(":sindiastolico",    $this->SinDiastolico);
        $psInsertar->bindParam(":hepatomegalia",    $this->Hepatomegalia);
        $psInsertar->bindParam(":esplenomegalia",   $this->Esplenomegalia);
        $psInsertar->bindParam(":ingurgitacion",    $this->Ingurgitacion);
        $psInsertar->bindParam(":idusuario",        $this->IdUsuario);
        $psInsertar->bindParam(":id",               $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idcardio entero con la clave del registro
     * Método que ejecuta la consulta de eliminación de una
     * entrevista
     */
    public function borraCardivascular($idcardio){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE FROM diagnostico.cardiovascular " +
                    "WHERE diagnostico.cardiovascular = '$idcardio '; ";
        $this->Link->exec($consulta);

    }

}
?>