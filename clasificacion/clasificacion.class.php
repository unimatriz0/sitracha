<?php

/**
 *
 * Class Clasificacion | clasificacion/clasificacion.class.php
 *
 * @package     Diagnostico
 * @subpackage  Clasificacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * clasificación del estadío
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Clasificacion {

    // declaración de las variables
    protected $Link;                   // puntero a la base de datos
    protected $Id;                     // clave del registro
    protected $Paciente;               // clave del paciente
    protected $Visita;                 // clave de la visita
    protected $Estadio;                // estadío de la enfermedad
    protected $Observaciones;          // observaciones del usuario
    protected $IdUsuario;              // clave del usuario
    protected $Usuario;                // nombre del usuario
    protected $Fecha;                  // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initClasificacion();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase, tanto
     * llamado desde el constructor como desde la consulta
     * cuando no encontró resultados
     */
    protected function initClasificacion(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Visita = 0;
        $this->Estadio = "";
        $this->Observaciones = "";
        $this->Usuario = "";
        $this->Fecha = "";

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setVisita($visita){
        $this->Visita = $visita;
    }
    public function setEstadio($estadio){
        $this->Estadio = $estadio;
    }
    public function setObservaciones($observaciones){
        $this->Observaciones = $observaciones;
    }

    // métodos de devolución de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getVisita(){
        return $this->Visita;
    }
    public function getEstadio(){
        return $this->Estadio;
    }
    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * Método que recibe como parámetro el protocolo de un
     * paciente y retorna el vector con los datos de las
     * clasificaciones en las distingas visitas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $protocolo - clave del paciente
     */
    public function nominaClasificacion($protocolo){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_clasificacion.id AS id,
                            diagnostico.v_clasificacion.fechavisita AS fecha,
                            diagnostico.v_clasificacion.estadio AS estadio,
                            diagnostico.v_clasificacion.observaciones AS observaciones,
                            diagnostico.v_clasificacion.usuario AS usuario
                     FROM diagnostico.v_clasificacion
                     WHERE diagnostici.v_clasificacion.paciente = '$protocolo';";
        $resultado = $this->Link->query($consulta);

        // obtenemos y retornamos el vector
        $nomina = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $nomina;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idvisita clave de la visita
     * Método que recibe como parámetro la clave de un paciente
     * y asigna en las variables de clase los valores del
     * registro (se supone que solo puede haber una entrada
     * para cada paciente)
     */
    public function getDatosClasificacion($idvisita){

        // componemos la consulta sobre la vista
        $consulta = "SELECT diagnostico.v_clasificacion.id AS id,
                            diagnostico.v_clasificacion.paciente AS paciente,
                            diagnostico.v_clasificacion.estadio AS estadio,
                            diagnostico.v_clasificacion.observaciones AS observaciones,
                            diagnostico.v_clasificacion.usuario AS usuario,
                            diagnostico.v_clasificacion.fechavisita AS fecha
                     FROM diagnostico.v_clasificacion
                     WHERE diagnostico.v_clasificacion.idvisita = '$idvisita'; ";
        $resultado = $this->Link->query($consulta);

        // verifica que halla registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);

            // asignamos los valores
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Visita = $idvisita;
            $this->Estadio = $estadio;
            $this->Observaciones = $observaciones;
            $this->Usuario = $usuario;
            $this->Fecha = $fecha;

        // si no hubo registros
        } else {

            // inicializamos las variables
            $this->initClasificacion();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero, clave del registro afectado
     * Método que ejecuta la consulta de inserción o
     * edición según corresponda
     */
    public function grabaClasificacion(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevaClasificacion();
        } else {
            $this->editaClasificacion();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevaClasificacion(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.clasificacion
                            (paciente,
                             visita,
                             estadio,
                             observaciones,
                             usuario)
                            VALUES
                            (:paciente,
                             :visita,
                             :estadio,
                             :observaciones,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":visita",        $this->Visita);
        $psInsertar->bindParam(":estadio",       $this->Estadio);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaClasificacion(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.clasificacion SET
                            estadio = :estadio,
                            observaciones = :observaciones,
                            usuario = :usuario
                     WHERE diagnostico.clasificacion.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":estadio",       $this->Estadio);
        $psInsertar->bindParam(":observaciones", $this->Observaciones);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);
        $psInsertar->bindParam(":id",            $this->Id);

        // ejecutamos la edición
        $psInsertar->execute();

    }

}
?>