<?php

/**
 *
 * borraeco | ecocardio/borraeco.php
 *
 * @package     Diagnostico
 * @subpackage  Ecocardio
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de un registro y ejecuta
 * la consulta de eliminación
 *
*/

// incluimos e instanciamos las clases
require_once("ecocardio.class.php");
$ecocardio = new Ecocardio();

// ejecutamos la consulta
$ecocardio->borraEco($_GET["id"]);

// retornamos siempre verdadero
echo json_encode(array("Error" => 1));

?>