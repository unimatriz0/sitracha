<?php

/**
 *
 * Class Ecocardio | ecocardio/ecocardio.class.php
 *
 * @package     Diagnostico
 * @subpackage  Ecocardio
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (06/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * ecocardiogramas
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Ecocardio {

    // definimos las variables de clase
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del usuario activo
    protected $Id;                    // clave del registro
    protected $Paciente;              // clave del paciente
    protected $Visita;                // clave de la visita
    protected $Fecha;                 // fecha de la administración
    protected $Normal;                // 0 - falso 1 - verdadero
    protected $Ddvi;                  // entero en milímetros
    protected $Dsvi;                  // entero en milímetros
    protected $Fac;                   // decimal
    protected $Siv;                   // en milímetros
    protected $Pp;                    // en milímetros
    protected $Ai;                    // en milímetros
    protected $Ao;                    // en milímetros
    protected $Fey;                   // dos decimales
    protected $Ddvd;                  // entero
    protected $Ad;                    // en milímetros
    protected $Movilidad;             // texto libre
    protected $Fsvi;                  // texto libre
    protected $Usuario;               // nombre del usuario
    protected $FechaAlta;             // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initEcocardio();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa las variables de clase, llamado
	 * desde el constructor o al no encontrar el registro
	 */
	protected function initEcocardio() {

		// inicializamos las variables
		$this->Id = 0;
		$this->Paciente = 0;
		$this->Visita = 0;
		$this->Fecha = "";
		$this->Normal = 0;
		$this->Ddvi = 0;
		$this->Dsvi = 0;
		$this->Fac = 0;
		$this->Siv = 0;
		$this->Pp = 0;
		$this->Ai = 0;
		$this->Ao = 0;
		$this->Fey = 0;
		$this->Ddvd = 0;
		$this->Ad = 0;
		$this->Movilidad = "";
		$this->Fsvi = "";
		$this->Usuario = "";
		$this->FechaAlta = "";

    }

    // métodos de asignación de valores
    public function setId($id) {
        $this->Id = $id;
    }
    public function setPaciente($paciente) {
        $this->Paciente = $paciente;
    }
    public function setVisita($visita) {
        $this->Visita = $visita;
    }
    public function setFecha($fecha) {
        $this->Fecha = $fecha;
    }
    public function setNormal($normal) {
        $this->Normal = $normal;
    }
    public function setDdvi($ddvi) {
        $this->Ddvi = $ddvi;
    }
    public function setDsvi($dsvi) {
        $this->Dsvi = $dsvi;
    }
    public function setFac($fac) {
        $this->Fac = $fac;
    }
    public function setSiv($siv) {
        $this->Siv = $siv;
    }
    public function setPp($pp) {
        $this->Pp = $pp;
    }
    public function setAi($ai) {
        $this->Ai = $ai;
    }
    public function setAo($ao) {
        $this->Ao = $ao;
    }
    public function setFey($fey) {
        $this->Fey = $fey;
    }
    public function setDdvd($ddvd) {
        $this->Ddvd = $ddvd;
    }
    public function setAd($ad) {
        $this->Ad = $ad;
    }
    public function setMovilidad($movilidad) {
        $this->Movilidad = $movilidad;
    }
    public function setFsvi($fsvi) {
        $this->Fsvi = $fsvi;
    }

    // métodos de retorno de valores
    public function getId() {
        return $this->Id;
    }
    public function getPaciente() {
        return $this->Paciente;
    }
    public function getVisita() {
        return $this->Visita;
    }
    public function getFecha() {
        return $this->Fecha;
    }
    public function getNormal() {
        return $this->Normal;
    }
    public function getDdvi() {
        return $this->Ddvi;
    }
    public function getDsvi() {
        return $this->Dsvi;
    }
    public function getFac() {
        return $this->Fac;
    }
    public function getSiv() {
        return $this->Siv;
    }
    public function getPp() {
        return $this->Pp;
    }
    public function getAi() {
        return $this->Ai;
    }
    public function getAo() {
        return $this->Ao;
    }
    public function getFey() {
        return $this->Fey;
    }
    public function getDdvd() {
        return $this->Ddvd;
    }
    public function getAd() {
        return $this->Ad;
    }
    public function getMovilidad() {
        return $this->Movilidad;
    }
    public function getFsvi() {
        return $this->Fsvi;
    }
    public function getUsuario() {
        return $this->Usuario;
    }
    public function getFechaAlta() {
        return $this->FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param paciente - entero con la clave del paciente
     * @return resultado - vector con los registros
     * Método que recibe como parámetro la clave de un paciente
     * y retorna el vector con todos los ecocardio de ese
     * paciente (utilizado en la impresión de historias clínica
     * clasificadas por estudio)
     */
    public function nominaEcocardio($paciente) {

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_ecocardio.id AS id,
                            diagnostico.v_ecocardio.paciente AS paciente,
                            diagnostico.v_ecocardio.visita AS visita,
                            diagnostico.v_ecocardio.fecha AS fecha,
                            diagnostico.v_ecocardio.normal AS normal,
                            diagnostico.v_ecocardio.ddvi AS ddvi,
                            diagnostico.v_ecocardio.dsvi AS dsvi,
                            diagnostico.v_ecocardio.fac AS fac,
                            diagnostico.v_ecocardio.siv AS siv,
                            diagnostico.v_ecocardio.pp AS pp,
                            diagnostico.v_ecocardio.ai AS ai,
                            diagnostico.v_ecocardio.ao AS ao,
                            diagnostico.v_ecocardio.fey AS fey,
                            diagnostico.v_ecocardio.ddvd AS ddvd,
                            diagnostico.v_ecocardio.ad AS ad,
                            diagnostico.v_ecocardio.movilidad AS movilidad,
                            diagnostico.v_ecocardio.fsvi AS fsvi,
                            diagnostico.v_ecocardio.usuario AS usuario,
                            diagnostico.v_ecocardio.fecha_alta AS fecha_alta
                     FROM diagnostico.v_ecocardio
                     WHERE diagnostico.v_ecocardio.paciente = '$paciente '; ";

        // obtenemos el vector y retornamos
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);
        return $fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita - clave de la visita
     * Método que recibe como parámetro la clave del registro
     * y asigna los valores de la base en las variables de
     * clase
     */
    public function getDatosEcocardio($idvisita) {

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_ecocardio.id AS id,
                            diagnostico.v_ecocardio.paciente AS paciente,
                            diagnostico.v_ecocardio.visita AS visita,
                            diagnostico.v_ecocardio.fecha AS fecha,
                            diagnostico.v_ecocardio.normal AS normal,
                            diagnostico.v_ecocardio.ddvi AS ddvi,
                            diagnostico.v_ecocardio.dsvi AS dsvi,
                            diagnostico.v_ecocardio.fac AS fac,
                            diagnostico.v_ecocardio.siv AS siv,
                            diagnostico.v_ecocardio.pp AS pp,
                            diagnostico.v_ecocardio.ai AS ai,
                            diagnostico.v_ecocardio.ao AS ao,
                            diagnostico.v_ecocardio.fey AS fey,
                            diagnostico.v_ecocardio.ddvd AS ddvd,
                            diagnostico.v_ecocardio.ad AS ad,
                            diagnostico.v_ecocardio.movilidad AS movilidad,
                            diagnostico.v_ecocardio.fsvi AS fsvi,
                            diagnostico.v_ecocardio.usuario AS usuario,
                            diagnostico.v_ecocardio.fecha_alta AS fecha_alta
                     FROM diagnostico.v_ecocardio
                     WHERE diagnostico.v_ecocardio.visita = '$idvisita'; ";

        // obtenemos el registro
        $resultado = $this->Link->query($consulta);

        // si hay registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro y asignamos
            $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
            extract($fila);
            $this->Id = $id;
            $this->Paciente = $paciente;
            $this->Visita = $visita;
            $this->Fecha = $fecha;
            $this->Normal = $normal;
            $this->Ddvi = $ddvi;
            $this->Dsvi = $dsvi;
            $this->Fac = $fac;
            $this->Siv = $siv;
            $this->Pp = $pp;
            $this->Ai = $ai;
            $this->Ao = $ao;
            $this->Fey = $fey;
            $this->Ddvd = $ddvd;
            $this->Ad = $ad;
            $this->Movilidad = $movilidad;
            $this->Fsvi = $fsvi;
            $this->Usuario = $usuario;
            $this->FechaAlta = $fecha_alta;

        // si no hay registros
        } else {

            // inicializamos las variables
            $this->initEcocardio();

        }

    }

    /**
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * @return id - clave del registro
      * Método que ejecuta la consulta de edición o inserción
      * según corresponda, retorna la clave del registro afectado
    */
    public function grabaEcocardio(){

        // si está insertando
        if ($this->Id == 0){
        $this->nuevoEcocardio();
        } else {
        $this->editaEcocardio();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    public function nuevoEcocardio(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.ecocardiograma
                            (paciente,
                             visita,
                             fecha,
                             normal,
                             ddvi,
                             dsvi,
                             fac,
                             siv,
                             pp,
                             ai,
                             ao,
                             fey,
                             ddvd,
                             ad,
                             movilidad,
                             fsvi,
                             usuario)
                            VALUES
                            (:paciente,
                             :visita,
                             STR_TO_DATE(:fecha, '%d/%m/%Y'),
                             :normal,
                             :ddvi,
                             :dsvi,
                             :fac,
                             :siv,
                             :pp,
                             :ai,
                             :ao,
                             :fey,
                             :ddvd,
                             :ad,
                             :movilidad,
                             :fsvi,
                             :usuario); ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",   $this->Paciente);
        $psInsertar->bindParam(":visita",     $this->Visita);
        $psInsertar->bindParam(":fecha",      $this->Fecha);
        $psInsertar->bindParam(":normal",     $this->Normal);
        $psInsertar->bindParam(":ddvi",       $this->Ddvi);
        $psInsertar->bindParam(":dsvi",       $this->Dsvi);
        $psInsertar->bindParam(":fac",        $this->Fac);
        $psInsertar->bindParam(":siv",        $this->Siv);
        $psInsertar->bindParam(":pp",         $this->Pp);
        $psInsertar->bindParam(":ai",         $this->Ai);
        $psInsertar->bindParam(":ao",         $this->Ao);
        $psInsertar->bindParam(":fey",        $this->Fey);
        $psInsertar->bindParam(":ddvd",       $this->Ddvd);
        $psInsertar->bindParam(":ad",         $this->Ad);
        $psInsertar->bindParam(":movilidad",  $this->Movilidad);
        $psInsertar->bindParam(":fsvi",       $this->Fsvi);
        $psInsertar->bindParam(":usuario",    $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // obtenemos la clave del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    public function editaEcocardio(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.ecocardiograma SET
                            paciente = :paciente,
                            visita = :visita,
                            fecha = STR_TO_DATE(:fecha, '%d/%m/%Y'),
                            normal = :normal,
                            ddvi = :ddvi,
                            dsvi = :dsvi,
                            fac = :fac,
                            siv = :siv,
                            pp = :pp,
                            ai = :ai,
                            ao = :ao,
                            fey = :fey,
                            ddvd = :ddvd,
                            ad = :ad,
                            movilidad = :movilidad,
                            fsvi = :fsvi,
                            usuario = :usuario
                     WHERE diagnostico.ecocardiograma.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",   $this->Paciente);
        $psInsertar->bindParam(":visita",     $this->Visita);
        $psInsertar->bindParam(":fecha",      $this->Fecha);
        $psInsertar->bindParam(":normal",     $this->Normal);
        $psInsertar->bindParam(":ddvi",       $this->Ddvi);
        $psInsertar->bindParam(":dsvi",       $this->Dsvi);
        $psInsertar->bindParam(":fac",        $this->Fac);
        $psInsertar->bindParam(":siv",        $this->Siv);
        $psInsertar->bindParam(":pp",         $this->Pp);
        $psInsertar->bindParam(":ai",         $this->Ai);
        $psInsertar->bindParam(":ao",         $this->Ao);
        $psInsertar->bindParam(":fey",        $this->Fey);
        $psInsertar->bindParam(":ddvd",       $this->Ddvd);
        $psInsertar->bindParam(":ad",         $this->Ad);
        $psInsertar->bindParam(":movilidad",  $this->Movilidad);
        $psInsertar->bindParam(":fsvi",       $this->Fsvi);
        $psInsertar->bindParam(":usuario",    $this->IdUsuario);
        $psInsertar->bindParam(":id",         $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

    /**
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $ideco - clave del registro
     */
    public function borraEco($ideco){

        // componemos y ejecutamos la consulta
        $consulta = "DELETE diagnostico.ecocardiograma
                     WHERE diagnostico.ecocardiograma.id = '$ideco';";
        $this->Link->exec($consulta);

    }
}
?>