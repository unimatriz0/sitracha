<?php

/**
 *
 * grabaeco | ecocardio/grabaeco.php
 *
 * @package     Diagnostico
 * @subpackage  Ecocardio
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos del registro y ejecuta
 * la consulta en el servidor
 *
*/

// incluimos e instanciamos las clases
require_once("ecocardio.class.php");
$ecocardio = new Ecocardio();

// asignamos en la clase
$ecocardio->setId($_POST["Id"]);
$ecocardio->setPaciente($_POST["Paciente"]);
$ecocardio->setVisita($_POST["Visita"]);
$ecocardio->setFecha($_POST["Fecha"]);
$ecocardio->setNormal($_POST["Normal"]);
$ecocardio->setDdvi(($_POST["Ddvi"]));
$ecocardio->setDsvi($_POST["Dsvi"]);
$ecocardio->setFac($_POST["Fac"]);
$ecocardio->setSiv($_POST["Siv"]);
$ecocardio->setPp($_POST["Pp"]);
$ecocardio->setAi($_POST["Ai"]);
$ecocardio->setAo($_POST["Ao"]);
$ecocardio->setFey($_POST["Fey"]);
$ecocardio->setDdvd($_POST["Ddvd"]);
$ecocardio->setAd($_POST["Ad"]);
$ecocardio->setMovilidad($_POST["Movilidad"]);
$ecocardio->setFsvi($_POST["Fsvi"]);

// grabamos el registro
$id = $ecocardio->grabaEcocardio();

// retornamos siempre verdadero
echo json_encode(array("Id" => $id));

?>