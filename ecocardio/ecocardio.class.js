/*
 * Nombre: ecocardio.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 11/06/2019
 * Licencia: GPL
 * Comentarios: Clase que controla las operaciones de los ecocardiogramas
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla las operaciones de los ecocardiogramas
 */
class Ecocardio {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initEcocardio();

        // inicializamos el layer
        this.layerEcocardio = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initEcocardio(){

        // inicializamos las variables
        this.Id = 0;                    // clave del registro
        this.Paciente = 0;              // clave del paciente
        this.Visita = 0;                // clave de la visita
        this.Fecha = "";                // fecha de administración
        this.Normal = 0;                // 0 no 1 si
        this.Ddvi = 0;                  // entero
        this.Dsvi = 0;                  // entero
        this.Fac = 0;                   // decimal dos lugares
        this.Siv = 0;                   // entero
        this.Pp = 0;                    // entero
        this.Ai = 0;                    // entero
        this.Ao = 0;                   // entero
        this.Fey = 0;                   // decimal
        this.Ddvd = 0;                  // entero
        this.Ad = 0;                    // entero
        this.Movilidad = "";            // texto libre
        this.Fsvi = "";                 // texto libre
        this.Usuario = "";              // usuario que ingresó el registro
        this.FechaAlta = "";            // fecha de alta del registro

    }

    // métodos de asignación de valores
    setId(id){
        this.Id = id;
    }
    setPaciente(paciente){
        this.Paciente = paciente;
    }
    setVisita(visita){
        this.Visita = visita;
    }
    setFecha(fecha){
        this.Fecha = fecha;
    }
    setNormal(normal){
        this.Normal = normal;
    }
    setDdvi(ddvi){
        this.Ddvi = ddvi;
    }
    setDsvi(dsvi){
        this.Dsvi = dsvi;
    }
    setFac(fac){
        this.Fac = fac;
    }
    setSiv(siv){
        this.Siv = siv;
    }
    setPp(pp){
        this.Pp = pp;
    }
    setAi(ai){
        this.Ai = ai;
    }
    setAo(ao){
        this.Ao = ao;
    }
    setFey(fey){
        this.Fey = fey;
    }
    setDdvd(ddvd){
        this.Ddvd = ddvd;
    }
    setAd(ad){
        this.Ad = ad;
    }
    setMovilidad(movilidad){
        this.Movilidad = movilidad;
    }
    setFsvi(fsvi){
        this.Fsvi = fsvi;
    }
    setUsuario(usuario){
        this.Usuario = usuario;
    }
    setFechaAlta(fecha){
        this.FechaAlta = fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta en el layer emergente el
     * formulario de ecocardiograma
     */
    verEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si no grabó la visita
        if (visitas.IdVisita == 0){

            // presenta el mensaje
            mensaje = "Debe grabar los datos de la visita primero";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // obtenemos la visita y el protocolo de los formularios
        // padres
        this.Visita = visitas.IdVisita;
        this.Paciente = document.getElementById("protocolo_paciente").value;

        // cargamos la página
        this.layerEcocardio = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: true,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Ecocardiogramas',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    width:1000,
                    draggable: 'title',
                    zIndex: 20000,
                    ajax: {
                        url: 'ecocardio/form_ecocardio.html',
                        reload: 'strict'
                    }
            });
        this.layerEcocardio.open();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de la variable de clase visita
     * obtiene los datos del registro
     */
    getDatosEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // obtenemos y asignamos los datos
        $.ajax({
            url: "ecocardio/getecocardio.php?idvisita=" + this.Visita,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // le pasamos el vector para que
                // asigne en las variables de clase
                ecocardio.cargaDatosEcocardio(data);

        }});

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {array} datos - vector con los datos
     * Método que recibe como parámetro un array json con
     * los datos del registro y asigna los valores a las
     * variables de clase
     */
    cargaDatosEcocardio(datos){

        // reiniciamos la sesión
        sesion.reiniciar();

        // asignamos en las variables de clase, paciente y
        // visita no porque dependen del formulario padre
        this.setId(datos.Id);
        this.setFecha(datos.Fecha);
        this.setNormal(datos.Normal);
        this.setDdvi(datos.Ddvi);
        this.setDsvi(datos.Dsvi);
        this.setFac(datos.Fac);
        this.setSiv(datos.Siv);
        this.setPp(datos.Pp);
        this.setAi(datos.Ai);
        this.setAo(datos.Ao);
        this.setFey(datos.Fey);
        this.setDdvd(datos.Ddvd);
        this.setAd(datos.Ad);
        this.setMovilidad(datos.Movilidad);
        this.setFsvi(datos.Fsvi);
        this.setUsuario(datos.Usuario);
        this.setFechaAlta(datos.FechaAlta);

        // mostramos el registro
        this.muestraDatosEcocardio();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de los valores de las variables
     * carga el formulario de datos
     */
    muestraDatosEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // carga la fecha
        document.getElementById("fecha_eco").value = this.Fecha;

        // según el resultado
        if (this.Normal == 0){
            document.getElementById("econormal").checked = false;
        } else {
            document.getElementById("econormal").checked = true;
        }

        // carga el resto de los valores
        document.getElementById("ecoddvi").value = this.Ddvi;
        document.getElementById("ecodsvi").value = this.Dsvi;
        document.getElementById("ecofac").value = this.Fac;
        document.getElementById("ecosiv").value = this.Siv;
        document.getElementById("ecopp").value = this.Pp;
        document.getElementById("ecoai").value = this.Ai;
        document.getElementById("ecoao").value = this.Ao;
        document.getElementById("ecofey").value = this.Fey;
        document.getElementById("ecoddvd").value = this.Ddvd;
        document.getElementById("ecoad").value = this.Ad;
        document.getElementById("ecomovilidad").value = this.Movilidad;
        document.getElementById("ecofsvi").value = this.Fsvi;

        // fijamos la fecha de alta y el usuario
        if (this.Id != 0){
            document.getElementById("usuarioeco").value = this.Usuario;
            document.getElementById("altaeco").value = this.FechaAlta;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que verifica
     * los datos del formulario
     */
    validaEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // verifica que halla ingresado la fecha
        if (document.getElementById("fecha_eco").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la fecha de administración";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("fecha_eco").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Fecha = document.getElementById("fecha_eco").value;

        }

        // si el resultado es normal
        if (document.getElementById("econormal").checked){
            this.Normal = 1;
        } else {
            this.Normal = 0;
        }

        // si no cargó el ddvi
        if (document.getElementById("ecoddvi").value == 0 || document.getElementById("ecoddvi").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el DDVI en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecoddvi").focus();
            return false;

        // si cargó
        } else {

            // asigna en la clase
            this.Ddvi = document.getElementById("ecoddvi").value;

        }

        // si no cargó el dsvi
        if (document.getElementById("ecodsvi").value == 0 || document.getElementById("ecodsvi").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el DSVI en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecodsvi").focus();
            return false;

        // si cargó
        } else {

            // asigna en la clase
            this.Dsvi = document.getElementById("ecodsvi").value;

        }

        // si no cargó el fac
        if (document.getElementById("ecofac").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el FAC en porcentaje";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecofac").focus();
            return false;

        // si declaró
        } else {

            // asigna en la clase
            this.Fac = document.getElementById("ecofac").value;

        }

        // si no ingresó el siv
        if (document.getElementById("ecosiv").value == 0 || document.getElementById("ecosiv").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese el SIV en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecosiv").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Siv = document.getElementById("ecosiv").value;

        }

        // si no ingresó el PP
        if (document.getElementById("ecopp").value == 0 || document.getElementById("ecopp").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el PP en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecopp").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Pp = document.getElementById("ecopp").value;

        }

        // si no ingresó el AI
        if (document.getElementById("ecoai").value == 0 || document.getElementById("ecoai").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el AI en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecoai").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Ai = document.getElementById("ecoai").value;

        }

        // si no indicó el AO
        if (document.getElementById("ecoao").value == 0 || document.getElementById("ecoao").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el valor de AO en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecoao").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Ao = document.getElementById("ecoao").value;

        }

        // si no ingresó el FEY
        if (document.getElementById("ecofey").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el FEY como un porcentaje";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecofey").focus();
            return false;

        // si declaró
        } else {

            // asigna en la clase
            this.Fey = document.getElementById("ecofey").value;

        }

        // si no declaró el ddvd
        if (document.getElementById("ecoddvd").value == 0 || document.getElementById("ecoddvd").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el DDVD en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecoddvd").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Ddvd = document.getElementById("ecoddvd").value;
        }

        // si no declaró el ad
        if (document.getElementById("ecoad").value == 0 || document.getElementById("ecoad").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el valor de AD en milímetros";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecoad").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Ad = document.getElementById("ecoad").value;

        }

        // si no ingresó la movilidad
        if (document.getElementById("ecomovilidad").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique la movilidad";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecomovilidad").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Movilidad = document.getElementById("ecomovilidad").value;

        }

        // si no ingresó el fsvi
        if (document.getElementById("ecofsvi").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Indique el FSVI";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("ecofsvi").focus();
            return false;

        // si ingresó
        } else {

            // asigna en la clase
            this.Fsvi = document.getElementById("ecofsvi").value;

        }

        // grabamos el registro
        this.grabaEcocardio();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * envía los datos al servidor
     */
    grabaEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var datosEco = new FormData();

        // agregamos los datos del formulario
        datosEco.append("Id", this.Id);
        datosEco.append("Paciente", this.Paciente);
        datosEco.append("Visita", this.Visita);
        datosEco.append("Fecha", this.Fecha);
        datosEco.append("Normal", this.Normal);
        datosEco.append("Ddvi", this.Ddvi);
        datosEco.append("Dsvi", this.Dsvi);
        datosEco.append("Fac", this.Fac);
        datosEco.append("Siv", this.Siv);
        datosEco.append("Pp", this.Pp);
        datosEco.append("Ai", this.Ai);
        datosEco.append("Ao", this.Ao);
        datosEco.append("Fey", this.Fey);
        datosEco.append("Ddvd", this.Ddvd);
        datosEco.append("Ad", this.Ad);
        datosEco.append("Movilidad", this.Movilidad);
        datosEco.append("Fsvi", this.Fsvi);

        // serializa el formulario y lo envía, recibe
        // como respuesta el resultado de la operación
        $.ajax({
            url: "ecocardio/grabaecocardio.php",
            type: "POST",
            data: datosEco,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si salío todo bien
                if (data.Id != 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ...", color: 'green'});

                    // asigna en las variables de clase los
                    ecocardio.setId(data.Id);

                // si hubo algún error
                } else {

                    // presenta la alerta
                    new jBox('Notice', {content: "Ha ocurrido un error, intente nuevamente", color: 'red'});

                }

            },

        });

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * recarga el registro o limpia el formulario según
     * el caso
     */
    cancelaEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // si está editando
        if (this.Id != 0){
            this.getDatosEcocardio();
        } else {
            this.limpiaEcocardio();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario
     */
    limpiaEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // limpiamos el formulario
        document.getElementById("fecha_eco").value = "";
        document.getElementById("econormal").checked = false;
        document.getElementById("ecoddvi").value = "";
        document.getElementById("ecodsvi").value = "";
        document.getElementById("ecofac").value = "";
        document.getElementById("ecosiv").value = "";
        document.getElementById("ecopp").value = "";
        document.getElementById("ecoai").value = "";
        document.getElementById("ecoAo").value = "";
        document.getElementById("ecofey").value = "";
        document.getElementById("ecoddvd").value = "";
        document.getElementById("ecoad").value = "";
        document.getElementById("ecomovilidad").value = "";
        document.getElementById("ecofsvi").value = "";

        // fijamos la fecha de alta y el usuario
        document.getElementById("usuarioeco").value = sessionStorage.getItem("Usuario");
        document.getElementById("altaeco").value = fechaActual();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que luego de pedir confirmación elimina el
     * regstro y cierra el formulario
     */
    borraEcocardio(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el cuadro de diálogo de confirmación
        var Confirmacion = new jBox('Confirm', {
            animation: 'flip',
            title: 'Eliminar Ecocardiograma',
            closeOnEsc: false,
            closeOnClick: false,
            closeOnMouseleave: false,
            closeButton: true,
            onCloseComplete: function(){
                this.destroy();
            },
            overlay: false,
            content: 'Está seguro que desea eliminar el registro?',
            theme: 'TooltipBorder',
            confirm: function() {

                // llama la rutina php
                $.get('ecocardio/borraecocardio.php?id='+ecocardio.Id,
                      function(data){

                        // si retornó correcto
                        if (data.Error != 0){

                            // presenta el mensaje
                            new jBox('Notice', {content: "Registro eliminado", color: 'green'});

                            // cerramos el layer
                            ecocardio.layerEcocardio.destroy();

                        // si no pudo eliminar
                        } else {

                            // presenta el mensaje
                            new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                        }

                    }, "json");

            },
            cancel: function(){
                Confirmacion.destroy();
            },
            confirmButton: 'Aceptar',
            cancelButton: 'Cancelar'

        });

        // mostramos el diálogo
        Confirmacion.open();

    }

}