<?php

/**
 *
 * getecocardio | ecocardio/getecocardio.php
 *
 * @package     Diagnostico
 * @subpackage  Ecocardio
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (28/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get la clave de una visita y retorna el
 * array json con los datos del registro
 *
*/

// incluimos e instanciamos las clases
require_once("ecocardio.class.php");
$ecocardio = new Ecocardio();

// obtenemos el registro
$ecocardio->getDatosEcocardio($_GET["idvisita"]);

// retornamos los datos
echo json_encode(array("Id" =>        $ecocardio->getId(),
                       "Fecha" =>     $ecocardio->getFecha(),
                       "Normal" =>    $ecocardio->getNormal(),
                       "Ddvi" =>      $ecocardio->getDdvi(),
                       "Dsvi" =>      $ecocardio->getDsvi(),
                       "Fac" =>       $ecocardio->getFac(),
                       "Siv" =>       $ecocardio->getSiv(),
                       "Pp" =>        $ecocardio->getPp(),
                       "Ai" =>        $ecocardio->getAi(),
                       "Ao" =>        $ecocardio->getAo(),
                       "Fey" =>       $ecocardio->getFey(),
                       "Ddvd" =>      $ecocardio->getDdvd(),
                       "Ad" =>        $ecocardio->getAd(),
                       "Movilidad" => $ecocardio->getMovilidad(),
                       "Fsvi" =>      $ecocardio->getFsvi(),
                       "Usuario" =>   $ecocardio->getUsuario(),
                       "FechaAlta" => $ecocardio->getFechaAlta()));

?>