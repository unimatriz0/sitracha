<?php

/**
 *
 * Class Digestivo | digestivo/digestivo.class.php
 *
 * @package     Diagnostico
 * @subpackage  Derivacion
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/06/2019)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla las operaciones de la tabla de
 * antecedentes digestivos del paciente
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Digestivo {

    // declaración de variables
    protected $Link;                 // puntero a la base de datos
    protected $Id;                   // clave del registro
    protected $Paciente;             // clave del paciente
    protected $Disfagia;             // 0 no 1 si
    protected $Pirosis;              // 0 no 1 si
    protected $Regurgitacion;        // 0 no 1 si
    protected $Constipacion;         // o no 1 si
    protected $Bolo;                 // 0 no 1 si
    protected $NoTiene;              // 0 no 1 si
    protected $IdUsuario;            // clave del usuario
    protected $Usuario;              // nombre del usuario
    protected $Fecha;                // fecha de alta del registro

    /**
     * Constructor de la clase, inicializa la conexión con
     * la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->Link = new Conexion();

        // abrimos la sesión y obtenemos el usuario
        session_start();
        if (!empty($_SESSION["ID"])){
            $this->IdUsuario = $_SESSION["ID"];
        }
        session_write_close();

        // inicializamos las variables
        $this->initDigestivo();

    }

    /**
     * Destructor de la clase, básicamente cierra la conexión
     * con la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // metodo que inicializa las variables de clase
    private function initDigestivo(){

        // inicializamos las variables
        $this->Id = 0;
        $this->Paciente = 0;
        $this->Disfagia = 0;
        $this->Pirosis = 0;
        $this->Regurgitacion = 0;
        $this->Constipacion = 0;
        $this->Bolo = 0;
        $this->NoTiene = 0;
        $this->Usuario = "";
        $this->Fecha = "";

    }

    // métodos de asignación de valores
    public function setId($id){
        $this->Id = $id;
    }
    public function setPaciente($paciente){
        $this->Paciente = $paciente;
    }
    public function setDisfagia($disfagia){
        $this->Disfagia = $disfagia;
    }
    public function setPirosis($pirosis){
        $this->Pirosis = $pirosis;
    }
    public function setRegurgitacion($regurgitacion){
        $this->Regurgitacion = $regurgitacion;
    }
    public function setConstipacion($constipacion){
        $this->Constipacion = $constipacion;
    }
    public function setBolo($bolo){
        $this->Bolo = $bolo;
    }
    public function setNoTiene($tiene){
        $this->NoTiene = $tiene;
    }

    // métodos de retorno de valores
    public function getId(){
        return $this->Id;
    }
    public function getPaciente(){
        return $this->Paciente;
    }
    public function getDisfagia(){
        return $this->Disfagia;
    }
    public function getPirosis(){
        return $this->Pirosis;
    }
    public function getRegurgitacion(){
        return $this->Regurgitacion;
    }
    public function getConstipacion(){
        return $this->Constipacion;
    }
    public function getBolo(){
        return $this->Bolo;
    }
    public function getNoTiene(){
        return $this->NoTiene;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getFecha(){
        return $this->Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param $idpaciente entero con la clave del paciente
     * Método que recibe como parámetro la clave del paciente y
     * asigna en las variables de clase los datos del registro,
     * se asume que un paciente solo puede tener una entrada
     * en los antecedentes digestivos
     */
    public function getDatosDigestivo($idpaciente){

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_digestivo.id AS id,
                            diagnostico.v_digestivo.paciente AS paciente,
                            diagnostico.v_digestivo.disfagia AS disfagia,
                            diagnostico.v_digestivo.pirosis AS pirosis,
                            diagnostico.v_digestivo.regurgitacion AS regurgitacion,
                            diagnostico.v_digestivo.constipacion AS constipacion,
                            diagnostico.v_digestivo.bolo AS bolo,
                            diagnostico.v_digestivo.notiene AS notiene,
                            diagnostico.v_digestivo.usuario AS usuario,
                            diagnostico.v_digestivo.fecha AS fecha
                     FROM diagnostico.v_digestivo
                     WHERE diagnostico.v_digestivo.paciente = '$idpaciente';";
        $resultado = $this->Link->query($consulta);

        // obtenemos el registro
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // asignamos los valores
        $this->Id = $id;
        $this->Paciente = $paciente;
        $this->Disfagia = $disfagia;
        $this->Pirosis = $pirosis;
        $this->Regurgitacion = $regurgitacion;
        $this->Constipacion = $constipacion;
        $this->Bolo = $bolo;
        $this->NoTiene = $notiene;
        $this->Usuario = $usuario;
        $this->Fecha = $fecha;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public function grabaDigestivo(){

        // si está insertando
        if ($this->Id == 0){
            $this->nuevoDigestivo();
        } else {
            $this->editaDigestivo();
        }

        // retornamos la id
        return $this->Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected function nuevoDigestivo(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.digestivo
                                (paciente,
                                 disfagia,
                                 pirosis,
                                 regurgitacion,
                                 constipacion,
                                 bolo,
                                 notiene,
                                 usuario)
                                VALUES
                                (:paciente,
                                 :disfagia,
                                 :pirosis,
                                 :regurgitacion,
                                 :constipacion,
                                 :bolo,
                                 :notiene,
                                 :usuario);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":paciente",      $this->Paciente);
        $psInsertar->bindParam(":disfagia",      $this->Disfagia);
        $psInsertar->bindParam(":pirosis",       $this->Pirosis);
        $psInsertar->bindParam(":regurgitacion", $this->Regurgitacion);
        $psInsertar->bindParam(":constipacion",  $this->Constipacion);
        $psInsertar->bindParam(":bolo",          $this->Bolo);
        $psInsertar->bindParam(":notiene",       $this->NoTiene);
        $psInsertar->bindParam(":usuario",       $this->IdUsuario);

        // ejecutamos la consulta
        $psInsertar->execute();

        // asigna la id del registro
        $this->Id = $this->Link->lastInsertId();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected function editaDigestivo(){

        // componemos la consulta
        $consulta = "UPDATE diagnostico.digestivo SET
                            disfagia = :disfagia,
                            pirosis = :pirosis,
                            regurgitacion = :regurgitacion,
                            constipacion = :constipacion,
                            bolo = :bolo,
                            notiene = :notiene,
                            usuario = :idusuario
                     WHERE diagnostico.digestivo.id = :id; ";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los valores
        $psInsertar->bindParam(":disfagia",      $this->Disfagia);
        $psInsertar->bindParam(":pirosis",       $this->Pirosis);
        $psInsertar->bindParam(":regurgitacion", $this->Regurgitacion);
        $psInsertar->bindParam(":constipacion",  $this->Constipacion);
        $psInsertar->bindParam(":bolo",          $this->Bolo);
        $psInsertar->bindParam(":notiene",       $this->NoTiene);
        $psInsertar->bindParam(":idusuario",     $this->IdUsuario);
        $psInsertar->bindParam(":id",            $this->Id);

        // ejecutamos la consulta
        $psInsertar->execute();

    }

}
?>