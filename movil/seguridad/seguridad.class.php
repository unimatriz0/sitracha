<?php

/**
 *
 * Classs Seguridad | movil/seguridad/seguridad.class.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla el acceso al sistema móvil y establece
 * las cookies del usuario
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Seguridad{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del usuario
    protected $Usuario;               // nombre del usuario
    protected $IdLaboratorio;         // clave del laboratorio

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de retorno de valores
    public function getIdUsuario(){
        return $this->IdUsuario;
    }
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getUsuario(){
        return $this->Usuario;
    }

    /**
     * Método que recibe como parámetro el nombre de usuario y su
     * contraseña sin encriptar, verifica que esté activo y que coincida
     * retorna la id del registro, previamente setea las cookies
     * del sitio
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $usuario - nombre del usuario
     * @param string $contrasenia - contraseña sin encriptar
     * @return int $idusuario - clave del usuario encontrado
     */
    public function validaUsuario($usuario, $contrasenia){

        // inicializamos las variables
        $idusuario = 0;
        $idlaboratorio = 0;

        // componemos la consulta
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.usuario AS usuario,
                            diagnostico.v_usuarios.idlaboratorio AS idlaboratorio
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.usuario = '$usuario' AND
                           diagnostico.v_usuarios.password = MD5('$contrasenia') AND
                           diagnostico.v_usuarios.activo = 'Si';";
        $resultado = $this->Link->query($consulta);

        // si encontró registros
        if ($resultado->rowCount() != 0){

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $registro = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

            // lo pasamos a variables
            extract($registro);

            // en el sitio movil usamos cookies para que el usuario
            // se encuentre acreditado durante un año
            setcookie("IdUsuario", $idusuario, time() + 60 * 60 * 24 * 365, "/");
            setcookie("Usuario", $usuario, time() + 60 * 60 * 24 * 365, "/");
            setcookie("IdLaboratorio", $idlaboratorio, time() + 60 * 60 * 24 * 365, "/");

            // asignamos en las variables de clase
            $this->IdUsuario = $idusuario;
            $this->Usuario = $usuario;
            $this->IdLaboratorio = $idlaboratorio;

            // retornamos verdadero
            return true;

        // si no encontró al usuario
        } else {

            // retorna falso
            return false;

        }

    }

}