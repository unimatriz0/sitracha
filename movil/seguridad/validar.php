<?php

/**
 *
 * movil/seguridad/validar.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que verifica el usuario se halla acreditado, obtiene
 * los valores de las cookies y retorna los mismos en un
 * array json
 *
 *
*/

// verifica si existe la cookie
if (isset($_COOKIE["IdUsuario"])){

    // retornamos los valores
    echo json_encode(array("IdUsuario" =>     $_COOKIE["IdUsuario"]));

// si no hay sesión
} else {

    // retornamos cero
    echo json_encode(array("IdUsuario" => 0));

}
?>