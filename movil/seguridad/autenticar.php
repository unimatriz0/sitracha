<?php

/**
 *
 * movil/seguridad/autenticar.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (17/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get los datos de usuario y contraseña
 * verifica que sean correctos y retorna el resultado mediante
 * json
 *
*/

// incluimos e instanciamos las clases
require_once("seguridad.class.php");
$seguridad = new Seguridad();

// verifica si está correcto
$resultado = $seguridad->validaUsuario($_GET["usuario"], $_GET["contrasenia"]);

// si autenticó
if ($resultado){

    // retornamos los valores
    echo json_encode(array("IdUsuario" => $seguridad->getIdUsuario()));

// si no lo encontró o no coincide
} else {

    // retornamos cero
    echo json_encode(array("IdUsuario" => 0));

}
?>