/*
 * Nombre: seguridad.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/05/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad móvil
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el acceso al sitio móvil
 */
class Seguridad {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initSeguridad();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initSeguridad(){

        // el layer emergente
        this.layerSeguridad = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de ingreso
     */
    formIngreso(){

        // mostramos el formulario
        this.layerSeguridad = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: false,
                    overlay: false,
                    repositionOnContent: true,
                    title: '<b>Acreditación</b>',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    draggable: 'title',
                    ajax: {
                        url: 'seguridad/form_ingreso.html',
                    reload: 'strict'
                }
            });
        this.layerSeguridad.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el formulario de ingreso que
     * verifica las credenciales
     */
    validarUsuario(){

        // declaración de variables
        var mensaje;
        var usuario = document.getElementById("usuario").value;
        var contrasenia = document.getElementById("contrasenia").value;

        // verifica que halla ingresado la contraseña
        if (usuario == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nombre de usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("usuario").focus();
            return false;

        }

        // verifica que halla ingresado el password
        if(contrasenia == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el password de acceso";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("contrasenia").focus();
            return false;

        }

        // llamamos la rutina php
        $.get('seguridad/autenticar.php',
                {usuario: usuario,
                 contrasenia: contrasenia},
                function(data){

                // si no se encuentra acreditado
                if (data.IdUsuario == 0){

                    // presenta el mensaje de error
                    new jBox('Notice', {content: "No coincide el usuario o la contraseña", color: 'red'});

                // si está acreditado
                } else {

                    // destruimos el layer
                    usuarios.layerSeguridad.destroy();

                }

            }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica si el usuario está acreditado
     */
    verificaUsuario(){

        // llamamos la rutina php
        $.get('seguridad/validar.php',
            function(data){

                // si no se encuentra acreditado
                if (data.IdUsuario == 0){

                    // presenta el formulario de ingreso
                    usuarios.formIngreso();

                }

            }, "json");

    }

}