<?php

/**
 *
 * movil/temperaturas/ultima_lectura.class.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Metodo que recibe por get la clave de una heladera y
 * retorna la última lectura de esa heladera, utilizado
 * en el sistema móvil de temperaturas
 *
*/

// incluimos e instanciamos la clase
require_once("temperaturas.class.php");
$heladera = new Temperaturas();

// obtenemos el registro
$heladera->ultimaLectura($_GET["heladera"]);

// retornamos el array json
echo json_encode(array("Lectura1" => $heladera->getTemperatura1(),
                       "Lectura2" => $heladera->getTemperatura2()));

?>