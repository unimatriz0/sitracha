<?php

/**
 *
 * movil/temperaturas/lista_temperaturas.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que retorna un array json con la nómina de heladeras
 *
*/

// incluimos e instanciamos la clase
require_once("temperaturas.class.php");
$heladera = new Temperaturas();

// obtenemos la nómina
$nomina = $heladera->nominaHeladeras();

// declaramos la matriz
$jsondata = array();

// recorremos el vector
foreach($nomina AS $registro){

    // obtenemos el registro
    extract($registro);

    // lo agregamos a la matriz
    $jsondata[] = array("Id" =>    $idheladera,
                        "Marca" => $marca);

}

// retornamos el vector
echo json_encode($jsondata);

?>