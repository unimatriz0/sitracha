<?php

/**
 *
 * movil/temperaturas/graba_lectura.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post los datos de una lectura y
 * ejecuta la insersión en la base
 *
*/

// incluimos e instanciamos la clase
require_once("temperaturas.class.php");
$lectura = new Temperaturas();

// fijamos los valores
$lectura->setIdHeladera($_POST["Heladera"]);
$lectura->setTemperatura1($_POST["Lectura1"]);
$lectura->setTemperatura2($_POST["Lectura2"]);

// grabamos el registro
$resultado = $lectura->grabaLectura();

// retornamos el resultado
echo json_encode(array("Id" => $resultado));

?>