<?php

/**
 *
 * Class Temperaturas | movil/temperaturas/temperaturas.class.php
 *
 * @package     Diagnostico
 * @subpackage  Temperaturas
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (16/05/2018)
 * @copyright   Copyright (c) 2017, INP
 *
*/

// inclusión de archivos
require_once ("../../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que controla la carga de lecturas de temperaturas de
 * heladeras para el sistema móvil
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Temperaturas{

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del usuario
    protected $IdLaboratorio;         // clave del laboratorio
    protected $IdHeladera;            // clave de la heladera
    protected $Fecha;                 // timestamp de la lectura
    protected $Temperatura1;          // primera temperatura
    protected $Temperatura2;          // segunda heladera

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // inicializamos las variables
        $this->IdHeladera = 0;
        $this->Fecha = date('Y/m/d H:i');
        $this->Temperatura1 = 0;
        $this->Temperatura2 = 0;

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // obtenemos el usuario y el laboratorio, usando cookies
        // para no tener que autenticarse en cada ingreso
        if (isset($_COOKIE["IdUsuario"])){
            $this->IdUsuario = $_COOKIE["IdUsuario"];
            $this->IdLaboratorio = $_COOKIE["IdLaboratorio"];
        } else {
            echo "Error en la sesión";
            exit;
        }

    }

    /**
     * Dstructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    // métodos de asignación de variables
    public function setIdHeladera($idheladera){
        $this->IdHeladera = $idheladera;
    }
    public function setTemperatura1($temperatura){
        $this->Temperatura1 = $temperatura;
    }
    public function setTemperatura2($temperatura){
        $this->Temperatura2 = $temperatura;
    }

    // métodos de retorno de valores
    public function getTemperatura1(){
        return $this->Temperatura1;
    }
    public function getTemperatura2(){
        return $this->Temperatura2;
    }

    /**
     * Método que retorna la nómina de heladeras del laboratorio del
     * usuario activo, utilizado en el sistema móvil
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array
     */
    public function nominaHeladeras(){

        // componemos la consulta
        $consulta = "SELECT diagnostico.heladeras.id AS idheladera,
                            diagnostico.heladeras.marca AS marca
                     FROM diagnostico.heladeras
                     WHERE diagnostico.heladeras.laboratorio = '$this->IdLaboratorio'
                     ORDER BY diagnostico.heladeras.marca;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetchAll(PDO::FETCH_ASSOC), CASE_LOWER);

        // retornamos el vector
        return $registros;

    }

    /**
     * Método que recibe como parámetro la id de una heladera y
     * asigna en las variables de clase los últimos valores
     * de lectura obtenidos, utilizado en el sistema móvil para
     * precargar la última lectura
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idheladera - clave de la heladera
     */
    public function ultimaLectura($idheladera){

        // inicializamos las variables
        $temperatura1 = 0;
        $temperatura2 = 0;

        // componemos la consulta
        $consulta = "SELECT diagnostico.temperaturas.temperatura AS temperatura1,
                            diagnostico.temperaturas.temperatura2 AS temperatura2
                     FROM diagnostico.temperaturas
                     WHERE diagnostico.temperaturas.heladera = '$idheladera'
                     ORDER BY diagnostico.temperaturas.fecha DESC
                     LIMIT 1;";
        $resultado = $this->Link->query($consulta);

        // lo pasamos a minúsculas porque según la versión de
        // pdo lo devuelve en mayúsculas o minúsculas
        $registros = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);

        // obtenemos el registro y lo asignamos a las variables
        extract($registros);
        $this->Temperatura1 = $temperatura1;
        $this->Temperatura2 = $temperatura2;

    }

    /**
     * Método que ejecuta la consulta de grabación y retorna la id
     * del registro
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int $idtemperatura - clave del registro insertado / editado
     */
    public function grabaLectura(){

        // componemos la consulta
        $consulta = "INSERT INTO diagnostico.temperaturas
                            (heladera,
                             fecha,
                             temperatura,
                             temperatura2,
                             controlado)
                            VALUES
                            (:heladera,
                             :fecha,
                             :temperatura,
                             :temperatura2,
                             :controlado);";

        // asignamos la consulta
        $psInsertar = $this->Link->prepare($consulta);

        // asignamos los parámetros de la consulta
        $psInsertar->bindParam(":heladera", $this->IdHeladera);
        $psInsertar->bindParam(":fecha", $this->Fecha);
        $psInsertar->bindParam(":temperatura", $this->Temperatura1);
        $psInsertar->bindParam(":temperatura2", $this->Temperatura2);
        $psInsertar->bindParam(":controlado", $this->IdUsuario);

        // ejecutamos la edición
        $psInsertar->execute();

        // obtiene la id del registro insertado
        return $this->Link->lastInsertId();

    }

}