/*
 * Nombre: temperaturas.class.js
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Fecha: 16/05/2018
 * Licencia: GPL
 * Comentarios: Serie de funciones y procedimientos utilizados por el
 *              sistema de control de calidad móvil en el registro de
 *              temperaturas
 */

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el registro de temperaturas en el
 * sitio móvil
 */
class Temperaturas {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initTemperaturas();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initTemperaturas(){

        // las variables de clase
        this.Lectura1 = 0;
        this.Lectura2 = "";
        this.Heladera = 0;

        // el layer de las temperaturas
        this.layerTemperaturas = "";

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el layer emergente para la carga de
     * registros
     */
    formTemperaturas(){

        // mostramos el formulario
        this.layerTemperaturas = new jBox('Modal', {
                    animation: 'flip',
                    closeOnEsc: false,
                    closeOnClick: false,
                    closeOnMouseleave: false,
                    closeButton: true,
                    overlay: false,
                    repositionOnContent: true,
                    title: 'Carga de Temperaturas',
                    theme: 'TooltipBorder',
                    onCloseComplete: function(){
                        this.destroy();
                    },
                    draggable: 'title',
                    ajax: {
                        url: 'temperaturas/form_temperaturas.html',
                    reload: 'strict'
                }
            });
        this.layerTemperaturas.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que simplemente cierra
     * el layer de temperaturas
     */
    cancelaTemperatura(){

        // destruimos el layer
        this.layerTemperaturas.destroy();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cambiar el select de heladeras, actualiza
     * los campos numero con las últimas lecturas realizadas
     */
    actualizaHeladera(){

        // verifica que halla una heladera seleccionada
        if (document.getElementById("heladera_temperaturas").value == 0){
            return false;
        }

        // llamamos la rutina php
        $.ajax({
            url: "temperaturas/ultima_lectura.php?heladera="+document.getElementById("heladera_temperaturas").value,
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // asignamos los valores en el formulario
                document.getElementById("heladera_temperatura1").value = data.Lectura1;
                document.getElementById("heladera_temperatura2").value = data.Lectura2;

                // seteamos el foco
                document.getElementById("heladera_temperatura1").focus();

        }});

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar, verifica que se
     * halla cargado al menos la primer temperatura y una heladera
     */
    verificaTemperatura(){

        // declaración de variables
        var mensaje;

        // si no hay una heladera seleccionada
        if (document.getElementById("heladera_temperaturas").value == 0){

            // presenta el mensaje y retorna
            mensaje = "Debe seleccionar una heladera";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("heladera_temperatura").focus();
            return false;

        // si seleccionó
        } else {

            // almacena en la variable de clase
            this.Heladera = document.getElementById("heladera_temperaturas").value;

        }

        // si no cargo la lectura 1
        if (document.getElementById("heladera_temperatura1").value == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar al menos una lectura";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("heladera_temperatura2").focus();
            return false;

        // si cargó
        } else {

            // asignamos en la variable de clase
            this.Lectura1 = document.getElementById("heladera_temperatura1").value;

        }

        // si cargó la lectura 2
        if (document.getElementById("heladera_temperatura2").value != ""){

            // asigna en la variable de clase
            this.Lectura2 = document.getElementById("heladera_temperatura2").value;

        }

        // llamamos la rutina de grabación
        this.grabaTemperatura();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario que ejecuta
     * la consulta de grabación
     */
    grabaTemperatura(){

        // declaración de variables
        var datosLectura = new FormData();

        // asignamos los valores
        datosLectura.append("Heladera", this.Heladera);
        datosLectura.append("Lectura1", this.Lectura1);
        datosLectura.append("Lectura2", this.Lectura2);

        // envía el formulario por ajax en forma sincrónica
        $.ajax({
            type: "POST",
            url: "temperaturas/graba_lectura.php",
            data: datosLectura,
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // si ocurrió un error
                if (data.Id == 0){

                    // presenta el mensaje
                    new jBox('Notice', {content: "Ha ocurrido un error", color: 'red'});

                } else {

                    // presenta el mensaje
                    new jBox('Notice', {content: "Registro grabado ... ", color: 'green'});

                    // inicializamos las variables de clase
                    temperaturas.initTemperaturas();

                    // reiniciamos el formulario
                    document.getElementById("heladera_temperaturas").value = 0;
                    document.getElementById("heladera_temperatura1").value = "";
                    document.getElementById("heladera_temperatura2").value = "";

                    // seteamos el foco
                    document.getElementById("heladera_temperaturas").focus();

                }

            },

        });

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} idelemento id de un elemento html
     * Método que recibe como parámetro la id de un elemento html
     * y carga en el la nómina de heladeras
     */
    nominaHeladeras(idelemento){

        // limpia el combo
        $("#" + idelemento).html('');

        // agrega el primer elemento en blanco
        $("#" + idelemento).append("<option value=0>Seleccione</option>");

        // lo llamamos asincrónico
        $.ajax({
            url: "temperaturas/lista_temperaturas.php",
            type: "GET",
            cahe: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {

                // recorre el vector de resultados
                for(var i=0; i < data.length; i++){

                    // agregamos el elemento
                    $("#" + idelemento).append("<option value=" + data[i].Id + ">" + data[i].Marca + "</option>");

                }

        }});

    }

}