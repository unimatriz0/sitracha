<?php

/**
 *
 * seguridad/verificar.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (05/05/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que verifica si el usuario tiene sesión activa, en cuyo
 * caso retorna verdadero
 *
*/

// iniciamos las variables
$estado = "";

// iniciamos la sesión
session_start();

// si existe la sesión
if (isset($_SESSION["ID"])){
    $estado = 1;
} else {
    $estado = 0;
}

// retorna el estado
echo json_encode(array("Error" => $estado));
?>
