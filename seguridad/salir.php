<?php

/**
 *
 * seguridad/salir.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que elimina las variables de sesión del usuario actual
 *
*/

// continuamos con la sesión
session_start();

// destruye la sesión
session_destroy();

// retorna siempre verdadero
echo json_encode(array("Error" => true));
?>
