<?php

/**
 *
 * seguridad/verificamail.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (19/10/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get una dirección de correo y
 * retorna un array jjson indicando si existe en la base
 * Utilizado para evitar la repetición de correos
 *
*/

// incluímos e instanciamos las clases
require_once("seguridad.class.php");
$seguridad = new Seguridad();

// verificamos si el mail existe
$resultado = $seguridad->verificaMail($_GET["mail"]);

// según el estado de la operación
if ($resultado){
    $error = 1;
} else {
    $error = 0;
}

// retornamos en formato json
echo json_encode(array("Error" => $error));
?>