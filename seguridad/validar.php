<?php

/**
 *
 * seguridad/validar.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por get el nombre de usuario y contraseña,
 * instancia la clase y obtiene los datos del usuario (la clase
 * fija las variables de sesión) retorna falso si hubo algún
 * error y caso contrario retorna en un array json el nivel
 * de seguridad del usuario
 *
*/

// obtenemos las variables recibidas
$usuario =  $_GET["usuario"];
$password = $_GET["password"];

// incluímos e instanciamos la clase
require_once ("seguridad.class.php");
$seguridad = new Seguridad();

// asignamos los valores
$seguridad->setUsuario($usuario);
$seguridad->setPassword($password);

// verificamos el acceso
$estado = $seguridad->Validar();

// si no validó
if (!$estado){

    // retorna el error
    echo json_encode(array("Error" => false));

// si validó
} else {

    // retornamos los valores
    echo json_encode(array("ID"             => $seguridad->getIdUsuario(),
                           "NivelCentral"   => $seguridad->getNivelCentral(),
                           "Laboratorio"    => $seguridad->getLaboratorio(),
                           "IdLaboratorio"  => $seguridad->getIdLaboratorio(),
                           "Departamento"   => $seguridad->getDepartamento(),
                           "IdDepartamento" => $seguridad->getIdDepartamento(),
                           "Pais"           => $seguridad->getPais(),
                           "IdPais"         => $seguridad->getIdPais(),
                           "Administrador"  => $seguridad->getAdministrador(),
                           "Personas"       => $seguridad->getPersonas(),
                           "Congenito"      => $seguridad->getCongenito(),
                           "Resultados"     => $seguridad->getResultados(),
                           "Muestras"       => $seguridad->getMuestras(),
                           "Entrevistas"    => $seguridad->getEntrevistas(),
                           "Auxiliares"     => $seguridad->getAuxiliares(),
                           "Protocolos"     => $seguridad->getProtocolos(),
                           "Stock"          => $seguridad->getStock(),
                           "Clinica"        => $seguridad->getClinica(),
                           "Usuarios"       => $seguridad->getUsuarios(),
                           "Jurisdiccion"   => $seguridad->getJurisdiccion(),
                           "CodProv"        => $seguridad->getCodProv(),
                           "Usuario"        => $seguridad->getUsuario()));

}
?>
