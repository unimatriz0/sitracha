<?php

/**
 *
 * Class Seguridad | seguridad/seguridad.class.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 *
*/

// inclusión de archivos
require_once ("../clases/conexion.class.php");

// convención para la nomenclatura de las propiedades, comienzan con una
// letra mayúscula, de tener mas de una palabra no se utilizan separadores
// y la inicial de cada palabra va en mayúscula
// para las variables recibidas como parámetro el criterio es todas en
// minúscula

// convención para la nomenclatura de los metodos, comienzan con set o get
// según asignen un valor o lo lean y luego el nombre del valor a obtener

/**
 * Clase que gestiona el acceso de los usuarios autorizados
 * al sistema
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 */
class Seguridad {

    // declaración de variables de la clase, las declaramos
    // como protected para que puedan ser heredadas pero
    // para asignarle el valor debemos crear los métodos
    protected $Link;                  // puntero a la base de datos
    protected $IdUsuario;             // clave del registro
    protected $Usuario;               // nombre del usuario
    protected $Nombre;                // nombre completo del usuario
    protected $Activo;                // indica si está activo
    protected $Password;              // contraseña de acceso sin encriptar
    protected $NuevoPass;             // nuevo password del usuario
    protected $Jurisdiccion;          // jurisdicción del usuario
    protected $Institucion;           // institución a la que pertenece
    protected $CodProv;               // código indec de la jurisdicción
    protected $Pais;                  // nombre del pais del usuario
    protected $IdPais;                // clave del país
    protected $NivelCentral;          // indica si es de nivel central
    protected $Laboratorio;           // laboratorio autorizado
    protected $IdLaboratorio;         // clave del laboratorio
    protected $Departamento;          // nombre del departamento
    protected $IdDepartamento;        // clave del departamento
    protected $Administrador;         // si es administrador
    protected $Personas;              // si puede editar personas
    protected $Congenito;             // si puede editar chagas congénito
    protected $Resultados;            // si puede cargar resultados
    protected $Muestras;              // si puede cargar muestras
    protected $Entrevistas;           // si puede cargar entrevistas
    protected $Auxiliares;            // si puede editar tablas auxiliares
    protected $Protocolos;            // si puede firmar protocolos
    protected $Stock;                 // si puede editar el stock
    protected $Clinica;               // si tiene permisos de clìnica mèdica
    protected $Usuarios;              // si puede editar usuarios

    /**
     * Constructor de la clase, establece la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __construct (){

        // nos conectamos a la base de datos
        $this->Link = new Conexion();

        // inicializamos las variables
        $this->Nombre = "";
        $this->Usuario = "";
        $this->Password = "";
        $this->NuevoPass = "";
        $this->Jurisdiccion = "";
        $this->Institucion = "";
        $this->Activo = "";
        $this->CodProv = "";
        $this->Pais = "";
        $this->IdPais = 0;
        $this->NivelCentral = "";
        $this->Laboratorio = "";
        $this->IdLaboratorio = 0;
        $this->IdDepartamento = 0;
        $this->Departamento = "";
        $this->Administrador = "No";
        $this->Personas = "No";
        $this->Congenito = "No";
        $this->Resultados = "No";
        $this->Muestras = "No";
        $this->Entrevistas = "No";
        $this->Auxiliares = "No";
        $this->Protocolos = "No";
        $this->Stock = "No";
        $this->Clinica = "No";
        $this->Usuarios = "No";

        // si inició sesión
        session_start();
        if(isset($_SESSION["ID"])){

            // obtenemos la id del usuario
            $this->IdUsuario = $_SESSION["ID"];

        }

        // cerramos sesión
        session_write_close();

    }

    /**
     * Destructor de la clase, cierra la conexión con la base
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     */
    function __destruct(){

        // elimina el enlace a la base
        $this->Link = null;

    }

    /**
     * Método que asigna la id de usuario en la clase
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int $idusuario - clave del usuario
     */
    public function setIdUsuario($idusuario){

        // verificamos que sea un número
        if (!is_numeric($idusuario)){

            // presenta el mensaje y abandona
            echo "La clave del usuario debe ser un número";
            exit;

        // si es correcto
        } else {

            // lo asigna
            $this->IdUsuario = $idusuario;

        }

    }

    /**
     * Método que asigna el nombre del usuario en la clase
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $usuario - nombre del usuario
     */
    public function setUsuario($usuario){
        $this->Usuario = $usuario;
    }

    /**
     * Método que asigna la contraseña sin encriptar en la clase
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $password - contraseña sin encriptar
     */
    public function setPassword($password){
        $this->Password = $password;
    }

    /**
     * Método que asigna la nueva contraseña (sin encriptar) en la clase
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $nuevopass - nuevo password del usuario
     */
    public function setNuevoPass($nuevopass){
        $this->NuevoPass = $nuevopass;
    }

    // métodos que retornan los valores
    public function getNombre(){
        return $this->Nombre;
    }
    public function getIdUsuario(){
        return $this->IdUsuario;
    }
    public function getNivelCentral(){
        return $this->NivelCentral;
    }
    public function getUsuario(){
        return $this->Usuario;
    }
    public function getJurisdiccion(){
        return $this->Jurisdiccion;
    }
    public function getInstitucion(){
        return $this->Institucion;
    }
    public function getActivo(){
        return $this->Activo;
    }
    public function getCodProv(){
        return $this->CodProv;
    }
    public function getPais(){
        return $this->Pais;
    }
    public function getIdPais(){
        return $this->IdPais;
    }
    public function getLaboratorio(){
        return $this->Laboratorio;
    }
    public function getIdLaboratorio(){
        return $this->IdLaboratorio;
    }
    public function getDepartamento(){
        return $this->Departamento;
    }
    public function getIdDepartamento(){
        return $this->IdDepartamento;
    }
    public function getAdministrador(){
        return $this->Administrador;
    }
    public function getPersonas(){
        return $this->Personas;
    }
    public function getCongenito(){
        return $this->Congenito;
    }
    public function getResultados(){
        return $this->Resultados;
    }
    public function getMuestras(){
        return $this->Muestras;
    }
    public function getEntrevistas(){
        return $this->Entrevistas;
    }
    public function getAuxiliares(){
        return $this->Auxiliares;
    }
    public function getProtocolos(){
        return $this->Protocolos;
    }
    public function getStock(){
        return $this->Stock;
    }
    public function getClinica(){
        return $this->Clinica;
    }
    public function getUsuarios(){
        return $this->Usuarios;
    }

    /**
     * Método que efectúa el cambio de contraseña en la base de datos
     * de usuarios, asume que ya fueron inicializadas las variables
     * de clase, verifica si los valores son correctos y actualiza
     * en la base de datos
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function nuevoPassword(){

        // encriptamos el nuevo password usando la función de mysql Y
        // componemos la consulta de actualización
        $consulta = "UPDATE cce.responsables SET
                            cce.responsables.password = MD5('$this->NuevoPass')
                     WHERE cce.responsables.id = '$this->IdUsuario';";
        $resultado = $this->Link->exec($consulta);

        // seteamos el estado de la operación
        return $resultado;

    }

    /**
     * Método que verifica que exista el usuario en la base y que la
     * contraseña de ingreso sea correcta, en cuyo caso retorna
     * verdadero
     * Al mismo tiempo asigna en las variables de clase los valores
     * del registro (cuando la autenticación es correcta)
     * Fija con los valores del registro las variables de sesión
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return boolean
     */
    public function Validar(){

        // buscamos el usuario en la base controlando que coincida la
        // contraseña encriptada y que esté activo
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario,
                            diagnostico.v_usuarios.usuario AS usuario,
                            diagnostico.v_usuarios.provincia AS jurisdiccion,
                            diagnostico.v_usuarios.codprov AS codprov,
                            diagnostico.v_usuarios.pais AS pais,
                            diagnostico.v_usuarios.idpais AS idpais,
                            diagnostico.v_usuarios.nivelcentral AS nivelcentral,
                            diagnostico.v_usuarios.nombrelaboratorio AS laboratorio,
                            diagnostico.v_usuarios.idlaboratorio AS idlaboratorio,
                            diagnostico.v_usuarios.departamento AS departamento,
                            diagnostico.v_usuarios.id_departamento AS iddepartamento,
                            diagnostico.v_usuarios.administrador AS administrador,
                            diagnostico.v_usuarios.personas AS personas,
                            diagnostico.v_usuarios.congenito AS congenito,
                            diagnostico.v_usuarios.resultados AS resultados,
                            diagnostico.v_usuarios.muestras AS muestras,
                            diagnostico.v_usuarios.entrevistas AS entrevistas,
                            diagnostico.v_usuarios.auxiliares AS auxiliares,
                            diagnostico.v_usuarios.protocolos AS protocolos,
                            diagnostico.v_usuarios.stock AS stock,
                            diagnostico.v_usuarios.clinica AS clinica,
                            diagnostico.v_usuarios.usuarios AS usuarios
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.usuario = '$this->Usuario' AND
                           diagnostico.v_usuarios.password = MD5('$this->Password') AND
                           diagnostico.v_usuarios.activo = 'Si';";
        $resultado = $this->Link->query($consulta);

        // si encontró registros
        if ($resultado->rowCount() != 0){

            // obtenemos el registro
            $fila = $resultado->fetch(PDO::FETCH_ASSOC);

            // lo pasamos a minúsculas porque según la versión de
            // pdo lo devuelve en mayúsculas o minúsculas
            $registro = array_change_key_case($fila, CASE_LOWER);

            // lo pasamos a variables
            extract($registro);

            // asígnamos en las variables de clase
            $this->IdUsuario = $idusuario;
            $this->Usuario = $usuario;
            $this->Jurisdiccion = $jurisdiccion;
            $this->CodProv = $codprov;
            $this->Pais = $pais;
            $this->IdPais = $idpais;
            $this->NivelCentral = $nivelcentral;
            $this->Laboratorio = $laboratorio;
            $this->IdLaboratorio = $idlaboratorio;
            $this->Departamento = $departamento;
            $this->IdDepartamento = $iddepartamento;
            $this->Administrador = $administrador;
            $this->Personas = $personas;
            $this->Congenito = $congenito;
            $this->Resultados = $resultados;
            $this->Muestras = $muestras;
            $this->Entrevistas = $entrevistas;
            $this->Auxiliares = $auxiliares;
            $this->Protocolos = $protocolos;
            $this->Stock = $stock;
            $this->Clinica = $clinica;
            $this->Usuarios = $usuarios;

            // inicia la sesión
            session_start();

            // almacena la información
            $_SESSION["ID"] = $this->IdUsuario;
            $_SESSION["Usuario"] = $this->Usuario;
            $_SESSION["Laboratorio"] = $this->Laboratorio;
            $_SESSION["IdLaboratorio"] = $this->IdLaboratorio;
            $_SESSION["Departamento"] = $this->Departamento;
            $_SESSION["IdDepartamento"] = $this->IdDepartamento;
            $_SESSION["Jurisdiccion"] = $this->Jurisdiccion;
            $_SESSION["CodProv"] = $this->CodProv;
            $_SESSION["Pais"] = $this->Pais;
            $_SESSION["IdPais"] = $this->IdPais;
            $_SESSION["NivelCentral"] = $this->NivelCentral;
            $_SESSION["Administrador"] = $this->Administrador;
            $_SESSION["Personas"] = $this->Personas;
            $_SESSION["Congenito"] = $this->Congenito;
            $_SESSION["Resultados"] = $this->Resultados;
            $_SESSION["Muestras"] = $this->Muestras;
            $_SESSION["Entrevistas"] = $this->Entrevistas;
            $_SESSION["Auxiliares"] = $this->Auxiliares;
            $_SESSION["Protocolos"] = $this->Protocolos;
            $_SESSION["Stock"] = $this->Stock;
            $_SESSION["Clinica"] = $this->Clinica;
            $_SESSION["Usuarios"] = $this->Usuarios;

            // cerramos la sesión
            session_write_close();

            // retornamos verdadero
            return true;

        // si no encontró al usuario
        } else {

            // retorna falso
            return false;

        }

    }

    /**
     * Método que verifica si existe un nombre de usuario en la base de
     * datos en cuyo caso retorna verdadero
     * Utilizado para evitar el registro duplicado de usuarios
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $nombreusuario - nombre de usuario a verificar
     * @return boolean
     */
    public function VerificaUsuario($nombreusuario){

        // inicializamos las variables
        $registros = 0;

        // primero verifica que el nombre de usuario no se encuentre
        // repetido en la base de usuarios
        $consulta = "SELECT COUNT(*) AS registros
                     FROM cce.responsables
                     WHERE cce.responsables.USUARIO = '$nombreusuario'; ";
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si encontró
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método utilizado en las altas y la recuperación de contraseña
     * recibe como parámetro una dirección de mail y retorna
     * verdadero si ya se encuentra declarada
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - correo a verificar
     * @return boolean - si existe o no el correo en la base
     */
    public function verificaMail($mail){

        // inicializamos las variables
        $registros = 0;

        // componemos la consulta
        $consulta = "SELECT COUNT(*) AS registros
                     FROM cce.responsables
                     WHERE cce.responsables.e_mail = '$mail';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // si encontró
        if ($registros != 0){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método que recibe una dirección de correo (la cual es única)
     * y asigna en las variables de clase los valores del usuario
     * para remitirlos por mail
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param string $mail - correo a buscar en la base
     */
    public function getIdCorreo($mail){

        // componemos la consulta, que exista el mail ya lo
        // verificamos antes
        $consulta = "SELECT diagnostico.v_usuarios.idusuario AS id,
                            diagnostico.v_usuarios.nombreusuario AS responsable,
                            diagnostico.v_usuarios.institucion AS institucion,
                            diagnostico.v_usuarios.activo AS activo,
                            diagnostico.v_usuarios.usuario AS usuario
                     FROM diagnostico.v_usuarios
                     WHERE diagnostico.v_usuarios.e_mail = '$mail';";

        // ejecutamos la consulta
        $resultado = $this->Link->query($consulta);
        $fila = array_change_key_case($resultado->fetch(PDO::FETCH_ASSOC), CASE_LOWER);
        extract($fila);

        // asignamos en las variables de clase
        $this->IdUsuario = $id;
        $this->Nombre = $responsable;
        $this->Institucion = $institucion;
        $this->Activo = $activo;
        $this->Usuario = $usuario;

    }

}