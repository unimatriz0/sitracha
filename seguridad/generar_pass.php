<?php

/**
 *
 * seguridad/generar_pass.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (12/06/2017)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe como parámetro una dirección de correo,
 * verifica que esta exista en la base y reinicia la
 * contraseña de ingreso, envía luego un mail con información
 * para el ingreso
 *
*/

// incluimos e instanciamos las clases
require_once ("seguridad.class.php");
$usuario = new Seguridad();

// inclusión de las clases de correo
require_once ("../clases/PHPMailerAutoload.php");

// obtenemos los datos del usuario y ya asigna en
// las variables de clase
$usuario->getIdCorreo($_GET["mail"]);

// si está activo
if ($usuario->getActivo() == "Si"){

    // actualizamos la contraseña
    $usuario->setNuevoPass($usuario->getUsuario());
    $usuario->nuevoPassword();

    // lee el archivo con el mensaje del disco y lo escapa
    $mensaje = file_get_contents("correo.html");

    // reemplaza en el mensaje por los valores de usuario
    // y contraseña, nombre e institucion
    $mensaje = str_replace("nombre_responsable", $usuario->getNombre(), $mensaje);
    $mensaje = str_replace("institucion_responsable", $usuario->getInstitucion(), $mensaje);
    $mensaje = str_replace("nombre_usuario", $usuario->getUsuario(), $mensaje);
    $mensaje = str_replace("password_usuario", $usuario->getUsuario(), $mensaje);

    // usamos el servidor de google porque usando el servidor del inp
    // al no tener un dominio la mayor parte de los mensajes son filtrados

    // instanciamos el objeto
    $mail = new PHPMailer;

    // indicamos que usamos smtp
    $mail->isSMTP();

    // Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;

    // Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';

    // Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';

    // seteamos el puerto smtp
    $mail->Port = 587;

    // configuramos la encripción
    $mail->SMTPSecure = 'tls';

    // usamos autenticación
    $mail->SMTPAuth = true;

    // usuario de gmail
    $mail->Username = "cinvernizzi@gmail.com";

    // password de gmail
    $mail->Password = "pickard47alfatango";

    // from del mensaje
    $mail->setFrom('cinvernizzi@gmail.com', 'Lic. Claudio Invernizzi');

    // responder a del mensaje
    $mail->addReplyTo('cinvernizzi@gmail.com', 'Lic. Claudio Invernizzi');

    // destinatario del mensaje
    $mail->addAddress($_GET["mail"], $responsable->getNombre());

    // el tema del mensaje
    $mail->Subject = "Acceso al Sistema de Diagnóstico";

    // cargamos el contenido del texto
    $mail->msgHTML($mensaje);

    // enviamos el mensaje
    $mail->send();

    // fijamos el error
    $error = 1;

// si el responsable no está activo
} else {

    // fijamos el error
    $error = 0;

}

// retornamos el resultado de la operación
echo json_encode(array("Error" => $error));
?>