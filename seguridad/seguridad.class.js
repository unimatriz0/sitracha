/*

    Nombre: seguridad.class.js
    Autor: Lic. Claudio Innizzi
    Fecha: 17/10/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Licencia: GPL
    Comentarios: Clase que controla las operaciones de seguridad del sistema

*/

/**
 *
 * Atención, utilizamos el nuevo estandard definido en ECMAScript 2015 (ES6)
 * este permite definir clases con el mètodo ya utilizado en php y java
 * como creo que es mas claro lo utilizaremos, la compatibilidad está
 * anunciada para firefox y chrome así que no deberìa haber problemas
 *
 */

/*jshint esversion: 6 */

/**
 * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @class Clase que controla el acceso al sistema
 */
class Seguridad {

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @constructor
     */
    constructor(){

        // inicializamos las variables
        this.initSeguridad();

        // objetos de la clase
        this.Ingreso = "";            // formulario de ingreso
        this.formRecuperar = "";      // formulario de recuperación

        // en el constructor verificamos si el usuario está
        // autenticado (instanciamos la clase desde el inicio)
        this.verificaIngreso();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    initSeguridad(){

        // definimos las variables de clase
        this.ID = 0;                  // clave del usuario
        this.Pais = "";               // nombre del país
        this.IdPais = 0;              // clave del país
        this.Provincia = "";          // nombre de la provincia
        this.IdProvincia = "";        // clave indec de la provincia
        this.Localidad = "";          // nombre de la localidad
        this.IdLocalidad = "";        // clave indec de la localidad
        this.Usuario = "";            // nombre del usuario
        this.Laboratorio = 0;         // clave del laboratorio al que pertenece
        this.Administrador = "No";    // indica si es administrador
        this.Personas = "No";         // si puede editar personas
        this.Congenito = "No";        // si puede editar chagas congenito
        this.Resultados = "No";       // si puede cargar resultados
        this.Muestras = "No";         // si puede tomar muestras
        this.Entrevistas = "No";      // si puede realizar entrevistas
        this.Auxiliares = "No";       // si puede editar tablas auxiliares
        this.Protocolos = "No";       // si puede firmar protocolos
        this.Stock = "No";            // si puede editar el stock
        this.Clinica = "No";          // si puede editar historias clìnicas
        this.Usuarios = "No";         // si puede editar la tabla de usuarios

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica si el usuario ya inició sesión
     */
    verificaIngreso(){

        // llama la rutina php
        $.get('seguridad/verificar.php',
        function(data){

            // si retornó error
            if (data.Error !== 1){

                // pedimos se identifique
                seguridadUsuarios.formIngreso();

            // si ya inición sesión
            } else {

                // seteamos la sesión
                seguridadUsuarios.fijarAcceso();

            }

        }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el formulario de ingreso
     */
    formIngreso(){

        // definimos el diálogo y cargamos el formulario
        this.Ingreso = new jBox('Modal', {
                                 animation: 'flip',
                                 closeOnEsc: false,
                                 closeOnClick: false,
                                 closeOnMouseleave: false,
                                 closeButton: false,
                                 repositionOnContent: true,
                                 overlay: true,
                                 draggable: 'title',
                                 theme: 'TooltipBorder',
                                 zIndex: 25000,
                                 ajax: {
                                    url: 'seguridad/form_ingreso.html',
                                    reload: 'strict'
                                 }
                            });
        this.Ingreso.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica se halla completado correctamente
     * el formulario de ingreso
     */
    validarIngreso(){

        // declaración de variables
        var usuario = document.getElementById("usuario").value;
        var password = document.getElementById("password").value;
        var mensaje;

        // verifica halla ingresado el usuario
        if ( usuario === ""){

            // presenta el mensaje de error y retorna
            mensaje = "Debe ingresar el nombre de usuario";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("usuario").focus();
            return false;

        }

        // verifica halla ingresado la contraseña
        if ( password === ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar la contraseña";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("password").focus();
            return false;

        }

        // ahora enviamos por get los datos
        $.get('seguridad/validar.php','usuario='+usuario+'&password='+password,
        function(data){

            // si retornó error
            if (data.Error === false){

                // presenta el mensaje de error
                mensaje = "El nombre de usuario o la contraseña\n";
                mensaje += "son incorrectas. Por favor verifique.";
                new jBox('Notice', {content: mensaje, color: 'red'});
                document.getElementById("usuario").focus();
                return false;

            // si está autenticado
            } else {

                // usamos la propiedad localstorage de HTML5 para
                // almacenar las variables de sesión
                sessionStorage.setItem("ID", data.ID);
                sessionStorage.setItem("NivelCentral", data.NivelCentral);
                sessionStorage.setItem("Laboratorio", data.Laboratorio);
                sessionStorage.setItem("IdLaboratorio", data.IdLaboratorio);
                sessionStorage.setItem("Departamento", data.Departamento);
                sessionStorage.setItem("IdDepartamento", data.IdDepartamento);
                sessionStorage.setItem("Jurisdiccion", data.Jurisdiccion);
                sessionStorage.setItem("CodProv", data.CodProv);
                sessionStorage.setItem("Pais", data.Pais);
                sessionStorage.setItem("IdPais", data.IdPais);
                sessionStorage.setItem("Administrador", data.Administrador);
                sessionStorage.setItem("Personas", data.Personas);
                sessionStorage.setItem("Congenito", data.Congenito);
                sessionStorage.setItem("Resultados", data.Resultados);
                sessionStorage.setItem("Muestras", data.Muestras);
                sessionStorage.setItem("Entrevistas", data.Entrevistas);
                sessionStorage.setItem("Auxiliares", data.Auxiliares);
                sessionStorage.setItem("Protocolos", data.Protocolos);
                sessionStorage.setItem("Stock", data.Stock);
                sessionStorage.setItem("Usuarios", data.Usuarios);
                sessionStorage.setItem("Clinica", data.Clinica);
                sessionStorage.setItem("Usuario", data.Usuario);

                // oculta el formulario y carga los datos
                seguridadUsuarios.Ingreso.destroy();

                // fijamos el nivel de acceso
                seguridadUsuarios.fijarAcceso();

                // si el usuario puede ingresar pacientes
                if (data.Personas == "Si"){

                    // verifica si está definida la fecha de entrega
                    fechas.entregaResultados();

                }

            }

        }, "json");

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que cambia el estado del password
     */
    muestraPassword(){

        // obtenemos el puntero al elemento
        var x = document.getElementById("password");

        // alterna el tipo de elemento
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que cierra la sesión de usuario
     */
    salir(){

        // llama la rutina php de elimación de la sesión, llamamos
        // así para que ejecute en forma sincrónica
        $.get('seguridad/salir.php',
        function(data){

            // si retornó correcto (que siempre retorna correcto)
            if (data.Error == true){

                // destruye las variables de sesión javascript
                sessionStorage.removeItem("ID");
                sessionStorage.removeItem("NivelCentral");
                sessionStorage.removeItem("Laboratorio");
                sessionStorage.removeItem("IdLaboratorio");
                sessionStorage.removeItem("Jurisdiccion");
                sessionStorage.removeItem("CodProv");
                sessionStorage.removeItem("Pais");
                sessionStorage.removeItem("IdPais");
                sessionStorage.removeItem("Administrador");
                sessionStorage.removeItem("Personas");
                sessionStorage.removeItem("Congenito");
                sessionStorage.removeItem("Resultados");
                sessionStorage.removeItem("Muestras");
                sessionStorage.removeItem("Entrevistas");
                sessionStorage.removeItem("Auxiliares");
                sessionStorage.removeItem("Protocolos");
                sessionStorage.removeItem("Stock");
                sessionStorage.removeItem("Clinica");
                sessionStorage.removeItem("Usuarios");
                sessionStorage.removeItem("Usuario");

            }

        }, "json");

        // llama la rutina de verificación de usuario
        this.verificaIngreso();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el formulario de cambio de password
     */
    nuevoPassword(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // definimos el diálogo y cargamos el formulario
        this.Ingreso = new jBox('Modal', {
                                 animation: 'flip',
                                 closeOnEsc: true,
                                 closeOnClick: false,
                                 closeOnMouseleave: false,
                                 closeButton: true,
                                 overlay: false,
                                 onCloseComplete: function(){
                                    this.destroy();
                                 },
                                 title: 'Nueva Contraseña',
                                 draggable: 'title',
                                 repositionOnContent: true,
                                 theme: 'TooltipBorder',
                                 ajax: {
                                    url: 'seguridad/form_password.html',
                                    reload: 'strict'
                                 }
                        });
        this.Ingreso.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica el formulario de cambio de password
     */
    validarPassword(){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var passOriginal = document.getElementById("passwordactual").value;
        var nuevoPassword = document.getElementById("passwordnuevo").value;
        var repeticion = document.getElementById("verificapassword").value;
        var mensaje;

        // verifica si ingresó el password original
        if (passOriginal == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el password actual";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("passwordactual").focus();
            return false;

        }

        // verifica si ingresó el nuevo password
        if (nuevoPassword == ""){

            // presenta el mensaje y retorna
            mensaje = "Debe ingresar el nuevo password";
            new Mensaje(mensaje, "red");
            document.getElementById("passwordnuevo").focus();
            return false;

        // verifica la longitud del nuevo password
        } else if (nuevoPassword.length < 6 || nuevoPassword.length > 15){

            // presenta el mensaje y retorna
            mensaje = "La contraseña debe tener una longitud no<br>";
            mensaje += "menor de 6 caracteres y no mayor de 15";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // verifica si ambos coinciden
        if (nuevoPassword != repeticion){

            // presenta el mensaje y retorna
            mensaje = "El nuevo password y su repetición<br>";
            mensaje += "no coinciden. Verifique por favor";
            new jBox('Notice', {content: mensaje, color: 'red'});
            return false;

        }

        // llamamos la rutina de cambio de password
        this.cambiarPassword(passOriginal, nuevoPassword);

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} password - contraseña sin encriptar
     * @param {string} nuevopassword - nueva contraseña
     * Método llamado luego de validar el formulario de cambio
     * de password, ejecuta por ajax la consulta en el servidor
     */
    cambiarPassword(password, nuevopassword){

        // reiniciamos la sesión
        sesion.reiniciar();

        // declaración de variables
        var estado = false;

        // llamamos la rutina de cambio de contraseña
        $.get('seguridad/nuevo_pass.php?password='+password+'&nuevo_password='+nuevopassword,
            function(data){

            // si retornó correcto (que siempre retorna correcto)
            if (data.Error === 0){
                estado = false;
            } else {
                estado = true;
            }

        }, "json");

        // presenta el mensaje
        if (estado){
            new Mensaje("Contraseña actualizada", 'green');
            mensaje = "Contraseña Actualizada";
            new jBox('Notice', {content: mensaje, color: 'green'});
            this.Ingreso.destroy();
        } else {
            mensaje = "Ha ocurrido un arror";
            new jBox('Notice', {content: mensaje, color: 'red'});
        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el formulario para recuperación de
     * contraseña
     */
    recuperarPassword(){

        // definimos el layer
        this.formRecuperar = new jBox('Modal', {
                                 animation: 'flip',
                                 closeOnEsc: true,
                                 closeOnClick: false,
                                 closeOnMouseleave: false,
                                 closeButton: true,
                                 overlay: false,
                                 onCloseComplete: function(){
                                    this.destroy();
                                 },
                                 title: 'Recuperar Usuario / Contraseña',
                                 draggable: 'title',
                                 repositionOnContent: true,
                                 theme: 'TooltipBorder',
                                 ajax: {
                                    url: 'seguridad/recuperar.html',
                                    reload: 'strict'
                                 }
                            });
        this.formRecuperar.open();

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario de recuperación
     * de contraseña
     */
    generarUsuario(){

        // declaración de variables
        var estado = false;

        // obtenemos el valor ingresado
        var mail = document.getElementById("recupera_mail").value;

        // verifica que se halla ingresado un mail
        if ( mail === ""){

            // presenta el mensaje y retorna
            mensaje = "Ingrese un mail válido";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("recupera_mail").focus();
            return false;

        // verifica que el mail esté correctamente conformado
        } else if (!echeck(mail)){

            // presenta el mensaje y retorna
            mensaje = "El mail parece incorrecto";
            new jBox('Notice', {content: mensaje, color: 'red'});
            document.getElementById("recupera_mail").focus();
            return false;

        }

        // verifica que el mail exista
        $.get('seguridad/verificamail.php?mail='+mail,
            function(data){

                // si retornó error
                if (data.Error === 0){
                    estado = false;
                } else {
                    estado = true;
                }

            }, "json");

        // si no encontró
        if (!estado){

            // presenta el mensaje
            mensaje = "No encuentro el mail en la base<br>";
            mensaje += "por favor, verifique";
            new jBox('Notice', {content: mensaje, color: 'red'});
            new Mensaje(mensaje, 'red');

        // si lo encontró
        } else {

            // envía el correo
            this.enviaCorreo(mail);

            // destruye la ventana
            this.formRecuperar.destroy();

        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param {string} mail - correo a verificar
     * Método que recibe como parámetro una dirección de correo, llama
     * la rutina php para enviar el mensaje de recuperación al usuario
     */
    enviaCorreo(mail){

        // declaración de variables
        var estado = false;

        // le pasamos por get el correo
        $.get('seguridad/generar_pass.php?mail='+mail,
        function(data){

            // si retornó error
            if (data.Error === 0){
                estado = false;
            } else {
                estado = true;
            }

        }, "json");

        // según el estado de la operación
        if (estado){

            // presenta el mensaje
            mensaje = "Se ha enviado un correo a: " + mail + "<br>";
            mensaje += "por favor, Verifique";
            new jBox('Notice', {content: mensaje, color: 'red'});
            this.formRecuperar.destruir();

        // si hubo un error
        } else {

            // presenta el mensaje
            mensaje = "El usuario no se encuentra activo";
            new jBox('Notice', {content: mensaje, color: 'red'});


        }

    }

    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de ingresar el usuario, muestra u oculta
     * los items del menú según el nivel de acceso del usuario
     */
    fijarAcceso(){

        // si no puede administrar el sistema
        if (sessionStorage.getItem("Administrador") !== "Si"){

        }

        // si no puede editar las tablas auxiliares
        if (sessionStorage.getItem("Auxiliares") !== "Si"){

            // oculta el tabulador

        }

        // si no puede editar el stock
        if (sessionStorage.getItem("Stock") !== "Si"){

            // oculta el menu
            $("#stockpedidos").hide();
            $("#stockingresos").hide();
            $("#stockegresos").hide();
            $("#stockitems").hide();
            $("#stockfinanciamiento").hide();
            $("#stockreportes").hide();

        // si es responsable de stock
        } else {

            // verificamos si hay artículos críticos
            inventario.verificaCritico();

        }

        // si no puede editar personas
        if (sessionStorage.getItem("Personas") !== "Si"){

            // oculta el menú nuevo

        }

        // si no puede cargar resultados
        if (sessionStorage.getItem("Resultados") !== "Si"){

            // oculta el menú carga

        }

        // si no puede tomar muestras
        if (sessionStorage.getItem("Muestras") !== "Si"){

            // oculta el menú tomar muestras

        }

        // si no puede realizar entrevistas
        if (sessionStorage.getItem("Entrevistas") !== "Si"){

            // oculta el menú nueva

        }


        // si no puede firmar protocolos
        if (sessionStorage.getItem("Protocolos") !== "Si"){

            // oculta el menú de la firma

        }

        // si no puede editar historias clìnicas
        if (sessionStorage.getItem("Clinica") !== "Si"){

            // ocultamos los menùes

        }

        // además en los formularios debe ocultar el botón grabar

        // por último mostramos el usuario actual
        usuarios.obtenerUsuario(sessionStorage.getItem("ID"));

    }

}
