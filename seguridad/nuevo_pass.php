<?php

/**
 *
 * seguridad/nuevo_pass.php
 *
 * @package     Diagnostico
 * @subpackage  Seguridad
 * @author      Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
 * @version     v.1.0 (15/12/2016)
 * @copyright   Copyright (c) 2017, INP
 *
 * Método que recibe por post la contraseña actual y la nueva
 * contraseña, toma la id del usuario de la sesión y actualiza
 * la contraseña, luego retorna en un array json el resultado
 * de la operación
 *
*/

// inclusión de archivos
require_once ("seguridad.class.php");

// instancia la clase
$seguridad = new Seguridad();

// setea los valores recibidos por post
$seguridad->setPassword($_GET["password"]);
$seguridad->setNuevoPass($_GET["nuevo_password"]);

// cambia el password
$resultado = $seguridad->NuevoPassword();

// si cambió con éxito
if ($resultado){
    $estado = 1;
} else {
    $estado = 0;
}

// retorna el estado
echo json_encode(array("Error" => $estado));
?>
